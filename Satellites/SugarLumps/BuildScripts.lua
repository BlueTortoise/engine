--[AutoExec]

--Always build the common functions.
LM_ExecuteScript("Data/CommonFuncs.lua")

--Build Scripts
SLF_Open("Output/Scripts.slf")
LuaTar_BuildTarball("../../Data/Scripts/")
SLF_Close()