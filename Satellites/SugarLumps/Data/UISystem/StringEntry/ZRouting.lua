--[ ===================================== String Entry UI ======================================= ]
--This appears when editing the note on a save file.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "AdvString|"
local saNames = {"Base", "Pressed"}
local saPaths = {"Base", "Pressed"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end
