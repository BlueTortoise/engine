--[Combat Animations]
--These are the animations that play over the character portraits in combat. There are variations
-- for all the major element types, as well as miscellaneous cases like healing, buffs, debuff removal,
-- and so on. They come in 4x4 grids of fixed sizes.

--[Ripping Function]
--Rips the 4x4 grid provided.
local fnRipAnim = function(psPrefix, psImgPath)
	
	--Arg check.
	if(psPrefix == nil)  then return end
	if(psImgPath == nil) then return end
	
	--Constants.
	local ciXSize = 384
	local ciYSize = 384

	--Iterate.
	for i = 0, 15, 1 do
		local iX = i % 4
		local iY = math.floor(i / 4)
		local sName = string.format("%s%02i", psPrefix, i)
		ImageLump_Rip(sName, psImgPath, ciXSize * iX, ciYSize * iY, ciXSize, ciYSize, 0)
	end
end

--[Execution]
--Rip the animations.
local sBasePath = fnResolvePath()
--fnRipAnim("ComFX|Electricity", sBasePath .. "Electricity.png")
--fnRipAnim("ComFX|Healing",     sBasePath .. "Healing.png")
--fnRipAnim("ComFX|Pierce",      sBasePath .. "Pierce.png")
--fnRipAnim("ComFX|SlashClaw",   sBasePath .. "SlashClaw.png")
--fnRipAnim("ComFX|SlashCross",  sBasePath .. "SlashCross.png")
--fnRipAnim("ComFX|Strike",      sBasePath .. "Strike.png")

--[Image Ripping Series]
local saFolderNames = {       "Buff",        "Debuff",       "Electricity",  "GunShot",        "Healing", "LaserShot",          "Pierce",        "Slash1",        "Slash2",         "Strike"}
local iaFrameCounts = {           24,              24,                  24,         20,               24,          20,                24,              24,              24,                10}
local saInternName  = {"buffa-color", "debuffa-color", "electricitya-color", "gunshot", "healinga-color", "lasershot", "piercinga-color", "slash1a-color", "slash2a-color", "strike2b-color"}

--For each folder:
local iFolder = 1
while(saFolderNames[iFolder] ~= nil) do
	
	--Iterate across the contents.
	for i = 1, iaFrameCounts[iFolder], 1 do
		local sName = string.format("ComFX|%s%02i", saFolderNames[iFolder], i-1)
		local sPath = sBasePath .. saFolderNames[iFolder] .. "/" .. string.format("%s%02i", saInternName[iFolder], i) .. ".png"
		
		ImageLump_Rip(sName, sPath, 0, 0, -1, -1, 0)
	end
	
	--Next.
	iFolder = iFolder + 1
end

