--[Sprites]
--Spritesheets used for characters in Adventure Mode. Sheets use a standardized format.
local sBasePath = fnResolvePath()
local sAnimationsPath = sBasePath .. "Animations/"
local sBearerPath = sBasePath .. "Bearers/"
local sNonBearerPath = sBasePath .. "NonBearerCharacters/"
local sGenericNPCPath = sBasePath .. "GenericNPCs/"
local sSystemPath = sBasePath .. "System/"

--Setup
SLF_Open("Output/Sprites.slf")
ImageLump_SetCompression(1)

--[Functions]
--Standard Sheet Rip
local fOffsetX = 0
local fOffsetY = 0
local fnRipSheet = function(sBaseName, sImagePath, bIsEightDir, bIsWide, bHasRunAnim)
	
	--Arg Check
	if(sBaseName == nil) then return end
	if(sImagePath == nil) then return end
	if(bIsEightDir == nil) then return end
	if(bIsWide == nil) then return end
	if(bHasRunAnim == nil) then return end
	
	--Constants
	local cfStartX = 0
	local cfStartY = 40
	local cfSizeX = 32
	local cfSizeY = 40
	local saSets =   {"South", "North", "West", "East"}
	local iaFrames = {      4,       4,      4,      4}
	if(bIsEightDir) then
		saSets =   {"South", "North", "West", "East", "NE", "NW", "SW", "SE"}
		iaFrames = {      4,       4,      4,      4,    4,    4,    4,    4}
	end
	
	--Wide case.
	if(bIsWide ~= nil and bIsWide == true) then
		cfStartX = 0
		cfStartY = 0
		cfSizeX = 50
		cfSizeY = 59
	end
	
	--Rip
	local i = 1
	while(saSets[i] ~= nil) do
		
		--Iterate.
		for p = 1, iaFrames[i], 1 do
			
			--Name.
			local sUseName = sBaseName .. "|" .. saSets[i] .. "|" .. p
			
			--Position.
			local fRipX = cfStartX + (cfSizeX * (p-1))
			local fRipY = cfStartY + (cfSizeY * (i-1))
			
			--Rip.
			ImageLump_Rip(sUseName, sImagePath, fRipX + fOffsetX, fRipY + fOffsetY, cfSizeX, cfSizeY, 0)
			
		end
	
		--Next.
		i = i + 1
	end
	
	--Running animation. Only used on some sprites, mostly party members.
	if(bHasRunAnim ~= nil and bHasRunAnim == true) then
		
		--Iterate again.
		i = 1
		while(saSets[i] ~= nil) do
			
			--Iterate.
			for p = 1, iaFrames[i], 1 do
				
				--Name.
				local sUseName = sBaseName .. "|" .. saSets[i] .. "|Run" .. p
				
				--Position.
				local fRipX = cfStartX + (cfSizeX * (p-1)) + (cfSizeX * 4)
				local fRipY = cfStartY + (cfSizeY * (i-1))
				
				--Rip.
				ImageLump_Rip(sUseName, sImagePath, fRipX + fOffsetX, fRipY + fOffsetY, cfSizeX, cfSizeY, 0)
				
			end
		
			--Next.
			i = i + 1
		end
		
	end
end

--Sheet rip with left and right movement only. Used for some animals.
local fnRipSheetHalf = function(sBaseName, sImagePath)
	
	--Arg Check
	if(sBaseName == nil) then return end
	if(sImagePath == nil) then return end
	
	--Constants
	local cfStartX = 0
	local cfStartY = 40
	local cfSizeX = 32
	local cfSizeY = 40
	local saSets =   {"West", "East"}
	local iaFrames = {     4,      4}
	
	--Rip
	local i = 1
	while(saSets[i] ~= nil) do
		
		--Iterate.
		for p = 1, iaFrames[i], 1 do
			
			--Name.
			local sUseName = sBaseName .. "|" .. saSets[i] .. "|" .. p
			
			--Position.
			local fRipX = cfStartX + (cfSizeX * (p-1))
			local fRipY = cfStartY + (cfSizeY * (i-1))
			
			--Rip.
			ImageLump_Rip(sUseName, sImagePath, fRipX + fOffsetX, fRipY + fOffsetY, cfSizeX, cfSizeY, 0)
			
		end
	
		--Next.
		i = i + 1
	end
end

--Rip with only facing directions.
local fnRipFacingSheet = function(sBaseName, sImagePath, iXStart, iYStart)
	
	--Arg Check
	if(sBaseName  == nil) then return end
	if(sImagePath == nil) then return end
	if(iXStart    == nil) then return end
	
	--Constants
	local cfStartX = iXStart
	local cfStartY = iYStart
	local cfSizeX = 32
	local cfSizeY = 40
	local saSets =   {"South", "North", "West", "East"}
	local iaFrames = {      1,       1,      1,      1}
	
	--Rip
	local i = 1
	while(saSets[i] ~= nil) do
		
		--Iterate.
		for p = 1, iaFrames[i], 1 do
			
			--Name.
			local sUseName = sBaseName .. "|" .. saSets[i] .. "|" .. p
			
			--Position.
			local fRipX = cfStartX + (cfSizeX * (p-1))
			local fRipY = cfStartY + (cfSizeY * (i-1))
			
			--Rip.
			ImageLump_Rip(sUseName, sImagePath, fRipX + fOffsetX, fRipY + fOffsetY, cfSizeX, cfSizeY, 0)
			
		end
	
		--Next.
		i = i + 1
	end
end

--[Characters]
--Playable characters, use 8-way movement.
fnRipSheet("55",             sNonBearerPath .. "55.png",             true, false, true)
fnRipSheet("55Sundress",     sNonBearerPath .. "55_Sundress.png",    true, false, true)
fnRipSheet("56",             sNonBearerPath .. "56.png",             true, false, true)
fnRipSheet("Aquillia",       sNonBearerPath .. "Aquillia.png",       true, false, true)
fnRipSheet("Breanne",        sNonBearerPath .. "Breanne_Human.png",  true, false, true)
fnRipSheet("Mei_Alraune",    sBearerPath .. "Mei_Alraune.png",    true, false, true)
fnRipSheet("Mei_Alraune_MC", sBearerPath .. "Mei_Alraune_MC.png", true, false, true)
fnRipSheet("Mei_Bee",        sBearerPath .. "Mei_Bee.png",        true, false, true)
fnRipSheet("Mei_Bee_MC",     sBearerPath .. "Mei_Bee_MC.png",     true, false, true)
fnRipSheet("Mei_Ghost",      sBearerPath .. "Mei_Ghost.png",      true, false, true)
fnRipSheet("Mei_Human",      sBearerPath .. "Mei_Human.png",      true, false, true)
fnRipSheet("Mei_Human_MC",   sBearerPath .. "Mei_Human_MC.png",   true, false, true)
fnRipSheet("Mei_Slime",      sBearerPath .. "Mei_Slime.png",      true, false, true)
fnRipSheet("Mei_Werecat",    sBearerPath .. "Mei_Werecat.png",    true, false, true)
fnRipSheet("Florentina",     sNonBearerPath .. "Florentina.png",     true, false, true)
	
fnRipSheet("Christine_Male",          sBearerPath .. "Chris_Human.png",             true, false, true)
fnRipSheet("Christine_Darkmatter",    sBearerPath .. "Christine_Darkmatter.png",    true, false, true)
fnRipSheet("Christine_DreamGirl",     sBearerPath .. "Christine_DreamGirl.png",     true, false, true)
fnRipSheet("Christine_Doll",          sBearerPath .. "Christine_Doll.png",          true, false, true)
fnRipSheet("Christine_Electrosprite", sBearerPath .. "Christine_Electrosprite.png", true, false, true)
fnRipSheet("Christine_Human",         sBearerPath .. "Christine_Human.png",         true, false, true)
fnRipSheet("Christine_Golem",         sBearerPath .. "Christine_Golem.png",         true, false, true)
fnRipSheet("Christine_GolemDress",    sBearerPath .. "Christine_GolemDress.png",    true, false, true)
fnRipSheet("Christine_Latex",         sBearerPath .. "Christine_Latex.png",         true, false, true)
fnRipSheet("Christine_Raibie",        sBearerPath .. "Christine_Raibie.png",        true, false, true)
fnRipSheet("Christine_Raiju",         sBearerPath .. "Christine_Raiju.png",         true, false, true)
fnRipSheet("Christine_RaijuClothes",  sBearerPath .. "Christine_RaijuClothes.png",  true, false, true)
fnRipSheet("Christine_SteamDroid",    sBearerPath .. "Christine_SteamDroid.png",    true, false, true)
fnRipSheet("Sophie",                  sNonBearerPath .. "Sophie.png",               true, false, true)
fnRipSheet("SophieDress",             sNonBearerPath .. "SophieDress.png",          true, false, true)
fnRipSheet("SophieLeather",           sNonBearerPath .. "SophieLeather.png",        true, false, true)
fnRipSheet("SX399Lord",               sNonBearerPath .. "SX-399Lord.png",           true, false, true)

fnRipSheet("Jeanne_Human", sBearerPath .. "Jeanne_Human.png",    true, false, true)
fnRipSheet("Lotta_Human",  sBearerPath .. "Lotta_Human.png",     true, false, true)
fnRipSheet("Sanya_Human",  sBearerPath .. "Sanya_Human.png",     true, false, true)

--Character Shadows
ImageLump_Rip("Shadow|Allchars|Neutral",  sSystemPath .. "CharacterShadowSheet.png", 0, 0, 32, 40, 0)

--Special frames for Mei and Florentina.
ImageLump_Rip("Spcl|MeiAlraune|Crouch",  sBearerPath .. "Mei_Alraune.png",      0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|MeiAlraune|Wounded", sBearerPath .. "Mei_Alraune.png",     32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|MeiBee|Crouch",      sBearerPath .. "Mei_Bee.png",          0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|MeiBee|Wounded",     sBearerPath .. "Mei_Bee.png",         32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|MeiBeeMC|Crouch",    sBearerPath .. "Mei_Bee_MC.png",       0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Mei|Crouch",         sBearerPath .. "Mei_Human.png",        0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Mei|Wounded",        sBearerPath .. "Mei_Human.png",       32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|MeiMC|Crouch",       sBearerPath .. "Mei_Human_MC.png",     0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|MeiGhost|Crouch",    sBearerPath .. "Mei_Ghost.png",        0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|MeiGhost|Wounded",   sBearerPath .. "Mei_Ghost.png",       32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|MeiSlime|Crouch",    sBearerPath .. "Mei_Slime.png",        0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|MeiSlime|Wounded",   sBearerPath .. "Mei_Slime.png",       32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|MeiWerecat|Crouch",  sBearerPath .. "Mei_Werecat.png",      0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|MeiWerecat|Wounded", sBearerPath .. "Mei_Werecat.png",     32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Florentina|Crouch",  sNonBearerPath .. "Florentina.png",       0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Florentina|Wounded", sNonBearerPath .. "Florentina.png",      32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Florentina|Sleep",   sNonBearerPath .. "Florentina.png",      64, 360, 32, 40, 0)

--Special frames for Chris and Christine
ImageLump_Rip("Spcl|Christine_Male|Crouch",          sBearerPath .. "Chris_Human.png",             0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Male|Wounded",         sBearerPath .. "Chris_Human.png",            32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_DreamGirl|Crouch",     sBearerPath .. "Christine_DreamGirl.png",     0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_DreamGirl|Wounded",    sBearerPath .. "Christine_DreamGirl.png",    32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Human|Crouch",         sBearerPath .. "Christine_Human.png",         0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Human|Wounded",        sBearerPath .. "Christine_Human.png",        32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Golem|Crouch",         sBearerPath .. "Christine_Golem.png",         0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Golem|Wounded",        sBearerPath .. "Christine_Golem.png",        32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Golem|Sad",            sBearerPath .. "Christine_Golem.png",        64, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Golem|Laugh0",         sBearerPath .. "Christine_Golem.png",         0, 400, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Golem|Laugh1",         sBearerPath .. "Christine_Golem.png",        32, 400, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Latex|Crouch",         sBearerPath .. "Christine_Latex.png",         0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Latex|Wounded",        sBearerPath .. "Christine_Latex.png",        32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Raibie|Crouch",        sBearerPath .. "Christine_Raibie.png",       32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Raibie|Wounded",       sBearerPath .. "Christine_Raibie.png",       32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Raiju|Crouch",         sBearerPath .. "Christine_Raiju.png",        32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Raiju|Wounded",        sBearerPath .. "Christine_Raiju.png",        32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_RaijuClothes|Crouch",  sBearerPath .. "Christine_RaijuClothes.png", 32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_RaijuClothes|Wounded", sBearerPath .. "Christine_RaijuClothes.png", 32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_SteamDroid|Crouch",    sBearerPath .. "Christine_SteamDroid.png",    0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_SteamDroid|Wounded",   sBearerPath .. "Christine_SteamDroid.png",   32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_DarkM|Crouch",         sBearerPath .. "Christine_Darkmatter.png",    0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_DarkM|Wounded",        sBearerPath .. "Christine_Darkmatter.png",   32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Doll|Crouch",          sBearerPath .. "Christine_Doll.png",          0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Doll|Wounded",         sBearerPath .. "Christine_Doll.png",         32, 360, 32, 40, 0)

ImageLump_Rip("Spcl|Christine_Human|BedSleep", sBearerPath .. "Christine_Human.png",  64, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Human|BedWake",  sBearerPath .. "Christine_Human.png",  96, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Human|BedFull",  sBearerPath .. "Christine_Human.png", 128, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Human|BedWakeR", sBearerPath .. "Christine_Human.png", 160, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Human|BedFullR", sBearerPath .. "Christine_Human.png", 192, 360, 32, 40, 0)

ImageLump_Rip("Spcl|Christine_Doll|FlatbackC", sBearerPath .. "Christine_Doll.png",  64, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Christine_Doll|FlatbackO", sBearerPath .. "Christine_Doll.png",  96, 360, 32, 40, 0)
	
--Sophie
ImageLump_Rip("Spcl|Sophie|Laugh0",  sNonBearerPath .. "Sophie.png",       0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Sophie|Laugh1",  sNonBearerPath .. "Sophie.png",      32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Sophie|Cry0",    sNonBearerPath .. "SophieDress.png",  0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|Sophie|Cry1",    sNonBearerPath .. "SophieDress.png", 32, 360, 32, 40, 0)

--Special frames for 2855.
ImageLump_Rip("Spcl|2855|Chair0", sNonBearerPath .. "55.png",   0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Chair1", sNonBearerPath .. "55.png",  32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Chair2", sNonBearerPath .. "55.png",  64, 360, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Chair3", sNonBearerPath .. "55.png",  96, 360, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Chair4", sNonBearerPath .. "55.png", 128, 360, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Draw0",  sNonBearerPath .. "55.png", 160, 360, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Draw1",  sNonBearerPath .. "55.png", 192, 360, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Draw2",  sNonBearerPath .. "55.png", 224, 360, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Crouch", sNonBearerPath .. "55.png",  32,   0, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Wounded",sNonBearerPath .. "55.png",  64,   0, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Downed", sNonBearerPath .. "55.png",  96,   0, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Stealth",sNonBearerPath .. "55.png", 128,   0, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Exec0",  sNonBearerPath .. "55.png",   0, 400, 64, 40, 0)
ImageLump_Rip("Spcl|2855|Exec1",  sNonBearerPath .. "55.png",  64, 400, 64, 40, 0)
ImageLump_Rip("Spcl|2855|Exec2",  sNonBearerPath .. "55.png", 128, 400, 64, 40, 0)
ImageLump_Rip("Spcl|2855|Exec3",  sNonBearerPath .. "55.png", 192, 400, 64, 40, 0)
ImageLump_Rip("Spcl|2855|Exec4",  sNonBearerPath .. "55.png", 256, 400, 64, 40, 0)
ImageLump_Rip("Spcl|2855|Exec5",  sNonBearerPath .. "55.png", 320, 400, 64, 40, 0)
ImageLump_Rip("Spcl|2855|Heavy0", sNonBearerPath .. "55.png",   0, 440, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Heavy1", sNonBearerPath .. "55.png",  32, 440, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Heavy2", sNonBearerPath .. "55.png",  64, 440, 32, 40, 0)
ImageLump_Rip("Spcl|2855|Heavy3", sNonBearerPath .. "55.png",  96, 440, 32, 40, 0)

--Special frames for SX-399
ImageLump_Rip("Spcl|SX399|Crouch",  sNonBearerPath .. "SX-399Lord.png",  0, 360, 32, 40, 0)
ImageLump_Rip("Spcl|SX399|Wounded", sNonBearerPath .. "SX-399Lord.png", 32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|SX399|Laugh0",  sNonBearerPath .. "SX-399Lord.png", 64, 360, 32, 40, 0)
ImageLump_Rip("Spcl|SX399|Laugh1",  sNonBearerPath .. "SX-399Lord.png", 96, 360, 32, 40, 0)

--Scraprat-splosion!
for i = 0, 11, 1 do
	local sName = "Spcl|Scraprat|Explode"
	if(i < 10) then sName = sName .. "0" end
	sName = sName .. i
	ImageLump_Rip(sName, sGenericNPCPath .. "Scraprat.png", i * 32, 200, 32, 64, 0)
end

--Special frames for NPCs.
ImageLump_Rip("Spcl|Aquillia|Wounded",    sNonBearerPath  .. "Aquillia.png",          32, 360, 32, 40, 0)
ImageLump_Rip("Spcl|GenericF1|Wounded",   sGenericNPCPath .. "Generic_NPC_F_1.png",   32,   0, 32, 40, 0)
ImageLump_Rip("Spcl|CultistF|Wounded",    sGenericNPCPath .. "Cultist_F.png",         32,   0, 32, 40, 0)
ImageLump_Rip("Spcl|Nadia|Sleep",         sNonBearerPath  .. "Nadia.png",              0, 200, 32, 40, 0)
ImageLump_Rip("Spcl|Werecat|Wounded",     sGenericNPCPath .. "Werecat.png",            0, 200, 32, 40, 0)
ImageLump_Rip("Spcl|CassandraH|Wounded",  sNonBearerPath  .. "Cassandra_Human.png",   32,   0, 32, 40, 0)
ImageLump_Rip("Spcl|CassandraW|Wounded",  sNonBearerPath  .. "Cassandra_Werecat.png", 32,   0, 32, 40, 0)
ImageLump_Rip("Spcl|GolemSlave|Wounded",  sGenericNPCPath .. "Golem_SlaveNoPack.png",  0, 200, 32, 40, 0)
ImageLump_Rip("Spcl|GolemSlave|Mutant",   sGenericNPCPath .. "Golem_SlaveNoPack.png", 32, 200, 32, 40, 0)
ImageLump_Rip("Spcl|GolemLord|Wounded",   sGenericNPCPath .. "Golem_LordA.png",        0, 200, 32, 40, 0)
ImageLump_Rip("Spcl|Doll|Wounded",        sGenericNPCPath .. "Doll.png",              32, 200, 32, 40, 0)
ImageLump_Rip("Spcl|GolemSlaveR|Wounded", sGenericNPCPath .. "Golem_SlaveRebel.png",   0, 200, 32, 40, 0)
ImageLump_Rip("Spcl|GolemSlaveR|Sitting", sGenericNPCPath .. "Golem_SlaveRebel.png",  32, 200, 32, 40, 0)
ImageLump_Rip("Spcl|Alraune|Crouch",      sGenericNPCPath .. "Alraune.png",           32,   0, 32, 40, 0)
ImageLump_Rip("Spcl|Alraune|Wounded",     sGenericNPCPath .. "Alraune.png",           64,   0, 32, 40, 0)

--Golem Explosion
for i = 0, 9, 1 do
    ImageLump_Rip("Spcl|GolemSlave|Explode" .. i, sGenericNPCPath .. "Golem_SlaveNoPack.png", 0 + (i*32), 240, 32, 40, 0)
end

--Special frames for Christine and Sophie at the same time.
ImageLump_Rip("Spcl|ChristineSophie|HoldHands0", sAnimationsPath .. "ChristineSophieSpecialAnim.png",  0,   0, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands1", sAnimationsPath .. "ChristineSophieSpecialAnim.png",  0,  40, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands2", sAnimationsPath .. "ChristineSophieSpecialAnim.png",  0,  80, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands3", sAnimationsPath .. "ChristineSophieSpecialAnim.png",  0, 120, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands4", sAnimationsPath .. "ChristineSophieSpecialAnim.png",  0, 160, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands5", sAnimationsPath .. "ChristineSophieSpecialAnim.png",  0, 200, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands6", sAnimationsPath .. "ChristineSophieSpecialAnim.png",  0, 240, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands7", sAnimationsPath .. "ChristineSophieSpecialAnim.png",  0, 280, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands8", sAnimationsPath .. "ChristineSophieSpecialAnim.png",  0, 320, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|HoldHands9", sAnimationsPath .. "ChristineSophieSpecialAnim.png",  0, 360, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss0",      sAnimationsPath .. "ChristineSophieSpecialAnim.png", 48,   0, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss1",      sAnimationsPath .. "ChristineSophieSpecialAnim.png", 48,  40, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss2",      sAnimationsPath .. "ChristineSophieSpecialAnim.png", 48,  80, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss3",      sAnimationsPath .. "ChristineSophieSpecialAnim.png", 48, 120, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss4",      sAnimationsPath .. "ChristineSophieSpecialAnim.png", 48, 160, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss5",      sAnimationsPath .. "ChristineSophieSpecialAnim.png", 48, 200, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss6",      sAnimationsPath .. "ChristineSophieSpecialAnim.png", 48, 240, 48, 40, 0)
ImageLump_Rip("Spcl|ChristineSophie|Kiss7",      sAnimationsPath .. "ChristineSophieSpecialAnim.png", 48, 280, 48, 40, 0)

--Static Burst Animation
for i = 0, 12, 1 do
    ImageLump_Rip("Spcl|StaticAnim" .. i, sAnimationsPath .. "Static" .. i .. ".png",  0,   0, -1, -1, 16)
end

--Latex Drone's arm falls off.
ImageLump_Rip("Spcl|LatexDrone|Arm0", sGenericNPCPath .. "LatexDrone.png",  0, 200, 32, 40, 0)
ImageLump_Rip("Spcl|LatexDrone|Arm1", sGenericNPCPath .. "LatexDrone.png", 32, 200, 32, 40, 0)
ImageLump_Rip("Spcl|LatexDrone|Arm2", sGenericNPCPath .. "LatexDrone.png", 64, 200, 32, 40, 0)
ImageLump_Rip("Spcl|LatexDrone|Arm3", sGenericNPCPath .. "LatexDrone.png", 96, 200, 32, 40, 0)
ImageLump_Rip("Spcl|LatexDrone|Arm4", sGenericNPCPath .. "LatexDrone.png",  0, 240, 32, 40, 0)

--NPCs, use 4-way movement.
fnRipSheet("20",                 sNonBearerPath  .. "20.png",                false, false, false)
fnRipSheet("609144",             sNonBearerPath  .. "609144.png",            false, false, false)
fnRipSheet("Adina",              sNonBearerPath  .. "Adina.png",             false, false, false)
fnRipSheet("Alraune",            sGenericNPCPath .. "Alraune.png",           false, false, false)
fnRipSheet("BandageImp",         sGenericNPCPath .. "BandageImp.png",        false, false, false)
fnRipSheet("BeeGirl",            sGenericNPCPath .. "Beegirl_A.png",         false, false, false)
fnRipSheet("BeeGirlZ",           sGenericNPCPath .. "Beegirl_Z.png",         false, false, false)
fnRipSheet("BestFriend",         sGenericNPCPath .. "BestFriend.png",        false, false, false)
fnRipSheet("Blythe",             sNonBearerPath  .. "Blythe.png",            false, false, false)
fnRipSheet("CassandraH",         sNonBearerPath  .. "Cassandra_Human.png",   false, false, false)
fnRipSheet("CassandraW",         sNonBearerPath  .. "Cassandra_Werecat.png", false, false, false)
fnRipSheet("CassandraG",         sNonBearerPath  .. "Cassandra_Golem.png",   false, false, false)
fnRipSheet("Claudia",            sNonBearerPath  .. "Claudia.png",           false, false, false)
fnRipSheet("CultistF",           sGenericNPCPath .. "Cultist_F.png",         false, false, false)
fnRipSheet("CultistM",           sGenericNPCPath .. "Cultist_M.png",         false, false, false)
fnRipSheet("DarkmatterGirl",     sGenericNPCPath .. "DarkMatterGirl.png",    true,  false, false) --Uses 8-way movement for some cutscenes.
fnRipSheet("DrMaisie",           sNonBearerPath  .. "DrMaisie.png",          false, false, false)
fnRipSheet("Doll",               sGenericNPCPath .. "Doll.png",              false, false, false)
fnRipSheet("DollGrn",            sGenericNPCPath .. "DollGrn.png",           false, false, false)
fnRipSheet("DollPrp",            sGenericNPCPath .. "DollPrp.png",           false, false, false)
fnRipSheet("DollInfluenced",     sGenericNPCPath .. "DollInfluenced.png",    false, false, false)
fnRipSheet("EldritchDream",      sGenericNPCPath .. "EldritchDream.png",     false, false, false)
fnRipSheet("Electrosprite",      sGenericNPCPath .. "Electrosprite.png",     false, false, false)
fnRipSheet("GenericF0",          sGenericNPCPath .. "Generic_NPC_F_0.png",   false, false, false)
fnRipSheet("GenericF1",          sGenericNPCPath .. "Generic_NPC_F_1.png",   false, false, false)
fnRipSheet("GenericM0",          sGenericNPCPath .. "Generic_NPC_M_0.png",   false, false, false)
fnRipSheet("GenericM1",          sGenericNPCPath .. "Generic_NPC_M_1.png",   false, false, false)
fnRipSheet("GolemLordA",         sGenericNPCPath .. "Golem_LordA.png",       false, false, false)
fnRipSheet("GolemLordB",         sGenericNPCPath .. "Golem_LordB.png",       false, false, false)
fnRipSheet("GolemLordC",         sGenericNPCPath .. "Golem_LordC.png",       false, false, false)
fnRipSheet("GolemLordD",         sGenericNPCPath .. "Golem_LordD.png",       false, false, false)
fnRipSheet("GolemSlaveP",        sGenericNPCPath .. "Golem_SlavePack.png",   false, false, false)
fnRipSheet("GolemSlave",         sGenericNPCPath .. "Golem_SlaveNoPack.png", false, false, false)
fnRipSheet("GolemSlaveR",        sGenericNPCPath .. "Golem_SlaveRebel.png",  false, false, false)
fnRipSheet("Hoodie",             sGenericNPCPath .. "Hoodie.png",            false, false, false)
fnRipSheet("Horrible",           sGenericNPCPath .. "Horrible.png",          false, false, false)
fnRipSheet("Imp",                sGenericNPCPath .. "Imp.png",               false, false, false)
fnRipSheet("InnGeisha",          sGenericNPCPath .. "InnGeisha.png",         false, false, false)
fnRipSheet("Isabel",             sNonBearerPath  .. "Isabel.png",            false, false, false)
fnRipSheet("LatexDrone",         sGenericNPCPath .. "LatexDrone.png",        false, false, false)
fnRipSheet("MaidGhost",          sGenericNPCPath .. "MaidGhost.png",         false, false, false)
fnRipSheet("Maisie",             sNonBearerPath ..  "DrMaisie.png",          false, false, false)
fnRipSheet("MirrorImage",        sGenericNPCPath .. "MirrorImage.png",       false, false, false)
fnRipSheet("MercF",              sGenericNPCPath .. "Merc_Female.png",       false, false, false)
fnRipSheet("MercM",              sGenericNPCPath .. "Merc_Male.png",         false, false, false)
fnRipSheet("Nadia",              sNonBearerPath  .. "Nadia.png",             false, false, false)
fnRipSheet("Raibie",             sGenericNPCPath .. "Raibie.png",            false, false, false)
fnRipSheet("Raiju",              sGenericNPCPath .. "Raiju.png",             false, false, false)
fnRipSheet("Rilmani",            sGenericNPCPath .. "Rilmani.png",           false, false, false)
fnRipSheet("Rochea",             sNonBearerPath  .. "Rochea.png",            false, false, false)
fnRipSheet("Sammy",              sNonBearerPath  .. "Sammy.png",             true,  false, true)
fnRipSheet("Septima",            sNonBearerPath  .. "Septima.png",           false, false, false)
fnRipSheet("Scraprat",           sGenericNPCPath .. "Scraprat.png",          false, false, false)
fnRipSheet("SecurityBot",        sGenericNPCPath .. "SecurityBot.png",       false, false, false)
fnRipSheet("SecurityBotBroke",   sGenericNPCPath .. "SecurityBotBroke.png",  false, false, false)
fnRipSheet("SkullCrawler",       sGenericNPCPath .. "SkullCrawler.png",      false, false, false)
fnRipSheet("Slime",              sGenericNPCPath .. "Slime.png",             false, false, false)
fnRipSheet("SlimeB",             sGenericNPCPath .. "SlimeBlue.png",         false, false, false)
fnRipSheet("SlimeG",             sGenericNPCPath .. "SlimeGreen.png",        false, false, false)
fnRipSheet("JX101",              sNonBearerPath  .. "JX-101.png",            false, false, false)
fnRipSheet("SteamDroid",         sGenericNPCPath .. "SteamDroid.png",        false, false, false)
fnRipSheet("SX399",              sNonBearerPath  .. "SX-399.png",            false, false, false)
fnRipSheet("Werecat",            sGenericNPCPath .. "Werecat.png",           false, false, false)
fnRipSheet("VoidRift",           sGenericNPCPath .. "VoidRift.png",          false, false, false)

--Latex Drones with hair!
fnRipSheet("LatexBlonde", sGenericNPCPath .. "LatexDrone_Blonde.png", false, false, false)
fnRipSheet("LatexBrown",  sGenericNPCPath .. "LatexDrone_Brown.png",  false, false, false)
fnRipSheet("LatexRed",    sGenericNPCPath .. "LatexDrone_Red.png",    false, false, false)

--NPCs using 2-way movement
fnRipSheetHalf("Chicken", sGenericNPCPath .. "Chicken.png")
fnRipSheetHalf("Sheep",   sGenericNPCPath .. "Sheep.png")

--[Sammy Davis, ACTION MOTH]
--She has some dancing frames
for i = 0, 7, 1 do
    ImageLump_Rip("SammyDanceA|" .. i, sNonBearerPath .. "SammyDance.png", 32 * i,   0, 32, 40, 0)
    ImageLump_Rip("SammyDanceB|" .. i, sNonBearerPath .. "SammyDance.png", 32 * i,  40, 32, 40, 0)
    ImageLump_Rip("SammyDanceC|" .. i, sNonBearerPath .. "SammyDance.png", 32 * i,  80, 32, 40, 0)
    ImageLump_Rip("SammyDanceD|" .. i, sNonBearerPath .. "SammyDance.png", 32 * i, 120, 32, 40, 0)
    ImageLump_Rip("SammyDanceE|" .. i, sNonBearerPath .. "SammyDance.png", 32 * i, 160, 32, 40, 0)
    ImageLump_Rip("SammyDanceF|" .. i, sNonBearerPath .. "SammyDance.png", 32 * i, 200, 32, 40, 0)
    ImageLump_Rip("SammyDanceG|" .. i, sNonBearerPath .. "SammyDance.png", 32 * i, 240, 32, 40, 0)
    ImageLump_Rip("SammyDanceH|" .. i, sNonBearerPath .. "SammyDance.png", 32 * i, 280, 32, 40, 0)
end

--Sammy's swimming frames.
ImageLump_Rip("SammySwimS", sNonBearerPath .. "Sammy.png", 32, 0, 32, 40, 0)
ImageLump_Rip("SammySwimW", sNonBearerPath .. "Sammy.png", 64, 0, 32, 40, 0)

--[Boat Animations]
local sBoatPath = sAnimationsPath .. "Boats.png"
for i = 0, 9, 1 do
    ImageLump_Rip("SammyBoatNormal|" .. i, sBoatPath, 120 * i,   0, 120, 40, 0)
    ImageLump_Rip("SammyBoatGuns|" .. i,   sBoatPath, 120 * i,  40, 120, 40, 0)
    ImageLump_Rip("SammyBoatShoot|" .. i,  sBoatPath, 120 * i,  80, 120, 40, 0)
    ImageLump_Rip("GolemBoat|" .. i,       sBoatPath, 120 * i, 120, 120, 40, 0)
end
for i = 0, 8, 1 do
    ImageLump_Rip("GolemBoatDestroyed|" .. i, sBoatPath, 120 * i, 160, 120, 40, 0)
end
for i = 0, 24, 1 do
    ImageLump_Rip("GolemBoatDestroying|" .. i, sBoatPath, 120 * i, 200, 120, 40, 0)
end
ImageLump_Rip("SammyBoatRock", sBoatPath, 0, 240, 120, 40, 0)

--NPCs with no movement frames.
fnRipFacingSheet("GolemFancyA", sGenericNPCPath .. "Golem_FancySheet.png",   0,   0)
fnRipFacingSheet("GolemFancyB", sGenericNPCPath .. "Golem_FancySheet.png",  32,   0)
fnRipFacingSheet("GolemFancyC", sGenericNPCPath .. "Golem_FancySheet.png",  64,   0)
fnRipFacingSheet("GolemFancyD", sGenericNPCPath .. "Golem_FancySheet.png",  96,   0)
fnRipFacingSheet("GolemFancyE", sGenericNPCPath .. "Golem_FancySheet.png", 128,   0)
fnRipFacingSheet("GolemFancyF", sGenericNPCPath .. "Golem_FancySheet.png", 160,   0)
fnRipFacingSheet("GolemFancyG", sGenericNPCPath .. "Golem_FancySheet.png",   0, 160)
fnRipFacingSheet("GolemFancyH", sGenericNPCPath .. "Golem_FancySheet.png",  32, 160)
fnRipFacingSheet("GolemFancyI", sGenericNPCPath .. "Golem_FancySheet.png",  64, 160)
fnRipFacingSheet("GolemFancyJ", sGenericNPCPath .. "Golem_FancySheet.png",  96, 160)
fnRipFacingSheet("GolemFancyK", sGenericNPCPath .. "Golem_FancySheet.png", 128, 160)
fnRipFacingSheet("GolemFancyL", sGenericNPCPath .. "Golem_FancySheet.png", 160, 160)
fnRipFacingSheet("GolemFancyM", sGenericNPCPath .. "Golem_FancySheet.png",   0, 320)
fnRipFacingSheet("GolemFancyN", sGenericNPCPath .. "Golem_FancySheet.png",  32, 320)
fnRipFacingSheet("GolemFancyO", sGenericNPCPath .. "Golem_FancySheet.png",  64, 320)
fnRipFacingSheet("GolemFancyP", sGenericNPCPath .. "Golem_FancySheet.png",  96, 320)
fnRipFacingSheet("GolemFancyQ", sGenericNPCPath .. "Golem_FancySheet.png", 128, 320)
fnRipFacingSheet("GolemFancyR", sGenericNPCPath .. "Golem_FancySheet.png", 160, 320)
fnRipFacingSheet("GolemFancyS", sGenericNPCPath .. "Golem_FancySheet.png",   0, 480)
fnRipFacingSheet("GolemFancyT", sGenericNPCPath .. "Golem_FancySheet.png",  32, 480)
fnRipFacingSheet("GolemFancyU", sGenericNPCPath .. "Golem_FancySheet.png",  64, 480)
fnRipFacingSheet("GolemFancyV", sGenericNPCPath .. "Golem_FancySheet.png",  96, 480)
fnRipFacingSheet("GolemFancyW", sGenericNPCPath .. "Golem_FancySheet.png", 128, 480)
fnRipFacingSheet("GolemFancyX", sGenericNPCPath .. "Golem_FancySheet.png", 160, 480)

--Biolabs NPCs. These use sheets on the same image.
fOffsetX = 0
fOffsetY = 0
fnRipSheet("GenericBiolabsFBlue",   sGenericNPCPath .. "BiolabsNPCsF.png", false, false, false)
fOffsetX = 128
fnRipSheet("GenericBiolabsFRed",    sGenericNPCPath .. "BiolabsNPCsF.png", false, false, false)
fOffsetX = 256
fnRipSheet("GenericBiolabsFGreen",  sGenericNPCPath .. "BiolabsNPCsF.png", false, false, false)
fOffsetX = 384
fnRipSheet("GenericBiolabsFYellow", sGenericNPCPath .. "BiolabsNPCsF.png", false, false, false)

--Males in the Biolabs.
fOffsetX = 0
fnRipSheet("GenericBiolabsMBlue",   sGenericNPCPath .. "BiolabsNPCsM.png", false, false, false)
fOffsetX = 128
fnRipSheet("GenericBiolabsMRed",    sGenericNPCPath .. "BiolabsNPCsM.png", false, false, false)
fOffsetX = 256
fnRipSheet("GenericBiolabsMGreen",  sGenericNPCPath .. "BiolabsNPCsM.png", false, false, false)
fOffsetX = 384
fnRipSheet("GenericBiolabsMYellow", sGenericNPCPath .. "BiolabsNPCsM.png", false, false, false)

--Clean.
fOffsetX = 0

--Special NPCs. Use wider frames.
fnRipSheet("Vivify",      sNonBearerPath .. "Vivify.png",      false, true, false)
fnRipSheet("VivifyBlack", sNonBearerPath .. "VivifyBlack.png", false, true, false)

--Vivify's special frames.
ImageLump_Rip("VivifyFreeze0", sNonBearerPath .. "Vivify.png",   0, 236, 50, 59, 0)
ImageLump_Rip("VivifyFreeze1", sNonBearerPath .. "Vivify.png",  50, 236, 50, 59, 0)
ImageLump_Rip("VivifyFreeze2", sNonBearerPath .. "Vivify.png", 100, 236, 50, 59, 0)
ImageLump_Rip("VivifyFreeze3", sNonBearerPath .. "Vivify.png", 150, 236, 50, 59, 0)
ImageLump_Rip("VivifyFreeze4", sNonBearerPath .. "Vivify.png", 200, 236, 50, 59, 0)

--[Tram]
--Not an NPC but uses the same system as one. It's *complicated*.
fnRipSheet("Tram", sSystemPath .. "Tram.png", false, false, false)

--[Goats]
--Baa.
ImageLump_Rip("Goat|0", sAnimationsPath .. "Goat.png",  0, 0, 32, 40, 0)
ImageLump_Rip("Goat|1", sAnimationsPath .. "Goat.png", 32, 0, 32, 40, 0)
ImageLump_Rip("Goat|2", sAnimationsPath .. "Goat.png", 64, 0, 32, 40, 0)

--[Doctor Bag Refill]
--Refills the doctor bag and despawns when examine.
ImageLump_Rip("BagRefill|0", sSystemPath .. "BagRefill.png",  0, 0, 32, 40, 0)
ImageLump_Rip("BagRefill|1", sSystemPath .. "BagRefill.png", 32, 0, 32, 40, 0)
ImageLump_Rip("BagRefill|2", sSystemPath .. "BagRefill.png", 64, 0, 32, 40, 0)

--[Catalysts]
--Health.
ImageLump_Rip("Catalyst|Heart",          sSystemPath .. "Catalyst.png",   0, 0, 32, 40, 0)
ImageLump_Rip("Catalyst|HeartFirework0", sSystemPath .. "Catalyst.png",  32, 0, 32, 40, 0)
ImageLump_Rip("Catalyst|HeartFirework1", sSystemPath .. "Catalyst.png",  64, 0, 32, 40, 0)
ImageLump_Rip("Catalyst|HeartFirework2", sSystemPath .. "Catalyst.png",  96, 0, 32, 40, 0)
ImageLump_Rip("Catalyst|HeartFirework3", sSystemPath .. "Catalyst.png", 128, 0, 32, 40, 0)

--Attack Power.
ImageLump_Rip("Catalyst|Sword",          sSystemPath .. "Catalyst.png",   0, 40, 32, 40, 0)
ImageLump_Rip("Catalyst|SwordFirework0", sSystemPath .. "Catalyst.png",  32, 40, 32, 40, 0)
ImageLump_Rip("Catalyst|SwordFirework1", sSystemPath .. "Catalyst.png",  64, 40, 32, 40, 0)
ImageLump_Rip("Catalyst|SwordFirework2", sSystemPath .. "Catalyst.png",  96, 40, 32, 40, 0)
ImageLump_Rip("Catalyst|SwordFirework3", sSystemPath .. "Catalyst.png", 128, 40, 32, 40, 0)

--Accuracy.
ImageLump_Rip("Catalyst|Target",          sSystemPath .. "Catalyst.png",   0, 80, 32, 40, 0)
ImageLump_Rip("Catalyst|TargetFirework0", sSystemPath .. "Catalyst.png",  32, 80, 32, 40, 0)
ImageLump_Rip("Catalyst|TargetFirework1", sSystemPath .. "Catalyst.png",  64, 80, 32, 40, 0)
ImageLump_Rip("Catalyst|TargetFirework2", sSystemPath .. "Catalyst.png",  96, 80, 32, 40, 0)
ImageLump_Rip("Catalyst|TargetFirework3", sSystemPath .. "Catalyst.png", 128, 80, 32, 40, 0)

--Initiative.
ImageLump_Rip("Catalyst|Boot",          sSystemPath .. "Catalyst.png",   0, 120, 32, 40, 0)
ImageLump_Rip("Catalyst|BootFirework0", sSystemPath .. "Catalyst.png",  32, 120, 32, 40, 0)
ImageLump_Rip("Catalyst|BootFirework1", sSystemPath .. "Catalyst.png",  64, 120, 32, 40, 0)
ImageLump_Rip("Catalyst|BootFirework2", sSystemPath .. "Catalyst.png",  96, 120, 32, 40, 0)
ImageLump_Rip("Catalyst|BootFirework3", sSystemPath .. "Catalyst.png", 128, 120, 32, 40, 0)

--Evasion.
ImageLump_Rip("Catalyst|Dodge",          sSystemPath .. "Catalyst.png",   0, 160, 32, 40, 0)
ImageLump_Rip("Catalyst|DodgeFirework0", sSystemPath .. "Catalyst.png",  32, 160, 32, 40, 0)
ImageLump_Rip("Catalyst|DodgeFirework1", sSystemPath .. "Catalyst.png",  64, 160, 32, 40, 0)
ImageLump_Rip("Catalyst|DodgeFirework2", sSystemPath .. "Catalyst.png",  96, 160, 32, 40, 0)
ImageLump_Rip("Catalyst|DodgeFirework3", sSystemPath .. "Catalyst.png", 128, 160, 32, 40, 0)

--[Bullet Impacts]
ImageLump_Rip("Impact|Bullet0", sSystemPath .. "BulletImpact.png",  0, 0, 32, 40, 0)
ImageLump_Rip("Impact|Bullet1", sSystemPath .. "BulletImpact.png", 32, 0, 32, 40, 0)
ImageLump_Rip("Impact|Bullet2", sSystemPath .. "BulletImpact.png", 64, 0, 32, 40, 0)
ImageLump_Rip("Impact|Bullet3", sSystemPath .. "BulletImpact.png", 96, 0, 32, 40, 0)

--[Crowbar Chan]
--Nyuu!
ImageLump_Rip("CrowbarChan|0", sNonBearerPath .. "CrowbarChan.png",  0, 0, 32, 40, 0)
ImageLump_Rip("CrowbarChan|1", sNonBearerPath .. "CrowbarChan.png", 32, 0, 32, 40, 0)
ImageLump_Rip("CrowbarChan|2", sNonBearerPath .. "CrowbarChan.png", 64, 0, 32, 40, 0)

--[Farm Icons]
--Used in the Salt Flats sequence.
ImageLump_Rip("FarmIcon|Water0", sSystemPath .. "FarmIcons.png",  0,   0, 32, 40, 0)
ImageLump_Rip("FarmIcon|Water1", sSystemPath .. "FarmIcons.png", 32,   0, 32, 40, 0)
ImageLump_Rip("FarmIcon|Ferti0", sSystemPath .. "FarmIcons.png",  0,  40, 32, 40, 0)
ImageLump_Rip("FarmIcon|Ferti1", sSystemPath .. "FarmIcons.png", 32,  40, 32, 40, 0)
ImageLump_Rip("FarmIcon|Grass0", sSystemPath .. "FarmIcons.png",  0,  80, 32, 40, 0)
ImageLump_Rip("FarmIcon|Grass1", sSystemPath .. "FarmIcons.png", 32,  80, 32, 40, 0)
ImageLump_Rip("FarmIcon|Polln0", sSystemPath .. "FarmIcons.png",  0, 120, 32, 40, 0)
ImageLump_Rip("FarmIcon|Polln1", sSystemPath .. "FarmIcons.png", 32, 120, 32, 40, 0)

--[Other]
--Hacksaw, used to rescue Claudia.
ImageLump_Rip("Hacksaw", sSystemPath .. "Hacksaw.png", 0, 0, 32, 40, 0)

--[Objects]
--Doors
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("Obj|DoorNS",          sSystemPath .. "Objects.png",  0,   0, 16, 32, 0)
ImageLump_Rip("Obj|DoorEW",          sSystemPath .. "Objects.png", 16,   0, 16, 32, 0)
ImageLump_Rip("Obj|DoorNSDungeonC",  sSystemPath .. "Objects.png",  0,  32, 16, 32, 0)
ImageLump_Rip("Obj|DoorNSDungeonO",  sSystemPath .. "Objects.png", 16,  32, 16, 32, 0)
ImageLump_Rip("Obj|DoorNSSilverC",   sSystemPath .. "Objects.png", 32,  32, 16, 32, 0)
ImageLump_Rip("Obj|DoorNSSilverO",   sSystemPath .. "Objects.png", 48,  32, 16, 32, 0)
ImageLump_Rip("Obj|DoorNSSpooky",    sSystemPath .. "Objects.png",  0, 112, 16, 32, 0)
ImageLump_Rip("Obj|DoorNSDungeonCV", sSystemPath .. "Objects.png", 48,  64, 16, 32, 0)
ImageLump_Rip("Obj|DoorNSDungeonOV", sSystemPath .. "Objects.png", 64,  64, 16, 32, 0)
ImageLump_Rip("Obj|DoorNSRegulusC",  sSystemPath .. "Objects.png", 48,  96, 16, 32, 0)
ImageLump_Rip("Obj|DoorNSRegulusO",  sSystemPath .. "Objects.png", 64,  96, 16, 32, 0)
ImageLump_Rip("Obj|DoorNSRegulusWC", sSystemPath .. "Objects.png", 48, 128, 32, 32, 0)
ImageLump_Rip("Obj|DoorNSRegulusWO", sSystemPath .. "Objects.png", 80, 128, 32, 32, 0)
ImageLump_Rip("Obj|DoorEWRegulusC",  sSystemPath .. "Objects.png", 80,  64, 16, 32, 0)
ImageLump_Rip("Obj|DoorEWRegulusFO", sSystemPath .. "Objects.png", 16, 128, 16, 32, 0)
ImageLump_Rip("Obj|DoorEWRegulusFC", sSystemPath .. "Objects.png", 32, 128, 16, 32, 0)
ImageLump_Rip("Obj|FallingRockRegA", sSystemPath .. "Objects.png",  0, 160, 7, 9, 0)
ImageLump_Rip("Obj|FallingRockRegB", sSystemPath .. "Objects.png", 16, 160, 6, 7, 0)
ImageLump_Rip("Obj|FallingRockRegC", sSystemPath .. "Objects.png", 32, 160, 6, 6, 0)
ImageLump_SetBlockTrimmingFlag(false)

--Other Objects
ImageLump_Rip("Obj|PDU",     sSystemPath .. "Objects.png", 80,  96, 16, 16, 0)
ImageLump_Rip("Obj|Lantern", sSystemPath .. "Objects.png", 80, 112, 16, 16, 0)

--Savepoint Fire
ImageLump_Rip("Obj|FireUnlit", sSystemPath .. "Objects.png", 48,  0, 16, 16, 0)
ImageLump_Rip("Obj|FireLit0",  sSystemPath .. "Objects.png", 64,  0, 16, 16, 0)
ImageLump_Rip("Obj|FireLit1",  sSystemPath .. "Objects.png", 80,  0, 16, 16, 0)

--Savepoint Coil
ImageLump_Rip("Obj|CoilUnlit", sSystemPath .. "Objects.png", 144,  0, 16, 16, 0)
ImageLump_Rip("Obj|CoilLit0",  sSystemPath .. "Objects.png", 160,  0, 16, 16, 0)
ImageLump_Rip("Obj|CoilLit1",  sSystemPath .. "Objects.png", 176,  0, 16, 16, 0)

--Savepoint Benches
ImageLump_Rip("Obj|BenchWood",  sSystemPath .. "Objects.png", 192,  0, 32, 16, 0)
ImageLump_Rip("Obj|BenchMetal", sSystemPath .. "Objects.png", 224,  0, 32, 16, 0)

--Stairs
ImageLump_Rip("Obj|StairUR",   sSystemPath .. "Objects.png", 48, 16, 16, 16, 0)
ImageLump_Rip("Obj|StairUL",   sSystemPath .. "Objects.png", 64, 16, 16, 16, 0)
ImageLump_Rip("Obj|StairDR",   sSystemPath .. "Objects.png", 80, 16, 16, 16, 0)
ImageLump_Rip("Obj|StairDL",   sSystemPath .. "Objects.png", 96, 16, 16, 16, 0)

--Treasure Chests
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("Obj|ChestC",  sSystemPath .. "Objects.png", 112,  0, 16, 32, 0)
ImageLump_Rip("Obj|ChestO",  sSystemPath .. "Objects.png", 128,  0, 16, 32, 0)
ImageLump_Rip("Obj|ChestBC", sSystemPath .. "Objects.png", 112, 48, 16, 32, 0)
ImageLump_Rip("Obj|ChestBO", sSystemPath .. "Objects.png", 128, 48, 16, 32, 0)
ImageLump_SetBlockTrimmingFlag(false)

--Futuristic Chests
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("Obj|ChestFC",  sSystemPath .. "Objects.png", 144, 48, 16, 32, 0)
ImageLump_Rip("Obj|ChestFO",  sSystemPath .. "Objects.png", 160, 48, 16, 32, 0)
ImageLump_Rip("Obj|ChestFBC", sSystemPath .. "Objects.png", 176, 48, 16, 32, 0)
ImageLump_Rip("Obj|ChestFBO", sSystemPath .. "Objects.png", 192, 48, 16, 32, 0)
ImageLump_SetBlockTrimmingFlag(false)

--Switches
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("Obj|SwitchUp", sSystemPath .. "Objects.png", 64, 32, 16, 32, 0)
ImageLump_Rip("Obj|SwitchDn", sSystemPath .. "Objects.png", 80, 32, 16, 32, 0)
ImageLump_SetBlockTrimmingFlag(false)

--Poof!
ImageLump_Rip("Obj|Poof0", sSystemPath .. "Poof.png",  0,  0, 32, 40, 0)
ImageLump_Rip("Obj|Poof1", sSystemPath .. "Poof.png", 32,  0, 32, 40, 0)
ImageLump_Rip("Obj|Poof2", sSystemPath .. "Poof.png", 64,  0, 32, 40, 0)
ImageLump_Rip("Obj|Poof3", sSystemPath .. "Poof.png", 96,  0, 32, 40, 0)

--Ladders, Ropes, Etc
ImageLump_Rip("Obj|LadderTop",  sSystemPath .. "Objects.png",  0, 64, 16, 16, 0)
ImageLump_Rip("Obj|LadderMid",  sSystemPath .. "Objects.png",  0, 80, 16, 16, 0)
ImageLump_Rip("Obj|LadderBot",  sSystemPath .. "Objects.png",  0, 96, 16, 16, 0)
ImageLump_Rip("Obj|RopeTop",    sSystemPath .. "Objects.png", 16, 64, 16, 16, 0)
ImageLump_Rip("Obj|RopeMid",    sSystemPath .. "Objects.png", 16, 80, 16, 16, 0)
ImageLump_Rip("Obj|RopeBot",    sSystemPath .. "Objects.png", 16, 96, 16, 16, 0)
ImageLump_Rip("Obj|RopeAnchor", sSystemPath .. "Objects.png", 32, 64, 16, 16, 0)

--[Combat Effects]
--Subfile handles this.
LM_ExecuteScript(sBasePath .. "CombatAnimations/ZRouting.lua")

--[Tilesets]
--Subfile.
LM_ExecuteScript(sBasePath .. "RandomMaps/ZRouting.lua")

--[Misc Effects]
--Enemy Spotting Effects
ImageLump_Rip("Spotted|Exclamation", sSystemPath .. "Spotted_Exclamation.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Spotted|Question",    sSystemPath .. "Spotted_Question.png",    0, 0, -1, -1, 0)

--[Apartment Objects]
--Used to customize Christine's quarters in Chapter 5.
ImageLump_Rip("AptObj|Tube0",    sSystemPath .. "ApartmentObjects.png",  0,   0, 32, 40, 0)
ImageLump_Rip("AptObj|Tube1",    sSystemPath .. "ApartmentObjects.png", 32,   0, 32, 40, 0)
ImageLump_Rip("AptObj|Tube2",    sSystemPath .. "ApartmentObjects.png", 64,   0, 32, 40, 0)
ImageLump_Rip("AptObj|Tube3",    sSystemPath .. "ApartmentObjects.png", 96,   0, 32, 40, 0)
ImageLump_Rip("AptObj|TubeLD",   sSystemPath .. "ApartmentObjects.png", 64,  80, 32, 40, 0)
ImageLump_Rip("AptObj|Tube55",   sSystemPath .. "ApartmentObjects.png", 96,  80, 32, 40, 0)
ImageLump_Rip("AptObj|CounterL", sSystemPath .. "ApartmentObjects.png",  0,  40, 32, 40, 0)
ImageLump_Rip("AptObj|CounterM", sSystemPath .. "ApartmentObjects.png", 32,  40, 32, 40, 0)
ImageLump_Rip("AptObj|CounterR", sSystemPath .. "ApartmentObjects.png", 64,  40, 32, 40, 0)
ImageLump_Rip("AptObj|Coffee",   sSystemPath .. "ApartmentObjects.png",  0,  80, 32, 40, 0)
ImageLump_Rip("AptObj|TV",       sSystemPath .. "ApartmentObjects.png",  0, 120, 32, 40, 0)
ImageLump_Rip("AptObj|ChairS",   sSystemPath .. "ApartmentObjects.png",  0, 160, 32, 40, 0)
ImageLump_Rip("AptObj|CouchSL",  sSystemPath .. "ApartmentObjects.png",  0, 200, 32, 40, 0)
ImageLump_Rip("AptObj|CouchSM",  sSystemPath .. "ApartmentObjects.png", 32, 200, 32, 40, 0)
ImageLump_Rip("AptObj|CouchSR",  sSystemPath .. "ApartmentObjects.png", 64, 200, 32, 40, 0)
ImageLump_Rip("AptObj|ChairN",   sSystemPath .. "ApartmentObjects.png",  0, 240, 32, 40, 0)
ImageLump_Rip("AptObj|CouchNL",  sSystemPath .. "ApartmentObjects.png",  0, 280, 32, 40, 0)
ImageLump_Rip("AptObj|CouchNM",  sSystemPath .. "ApartmentObjects.png", 32, 280, 32, 40, 0)
ImageLump_Rip("AptObj|CouchNR",  sSystemPath .. "ApartmentObjects.png", 64, 280, 32, 40, 0)

--Finish
SLF_Close()
