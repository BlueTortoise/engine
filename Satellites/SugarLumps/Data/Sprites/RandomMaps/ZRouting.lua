--[Tilemaps]
--Randomly generated levels need tilemaps to work. These don't use the same logic as the MapData.slf
-- versions, instead they are mapped by lua scripts to fill certain slots.
local sBasePath = fnResolvePath()
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("Level|RegulusCave",       sBasePath .. "RegulusCave.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Level|RegulusCaveFungus", sBasePath .. "RegulusCaveFungusFull.png", 0, 0, -1, -1, 0)
ImageLump_SetBlockTrimmingFlag(false)
