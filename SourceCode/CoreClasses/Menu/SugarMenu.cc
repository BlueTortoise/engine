//--Base
#include "SugarMenu.h"

//--Classes
#include "SugarButton.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

//=========================================== System ==============================================
SugarMenu::SugarMenu()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_MENU_ROOT;

    //--[SugarMenu]
    //--System
    mIsPendingClose = false;

    //--Buttons
    mButtonList = new SugarLinkedList(true);
}
SugarMenu::~SugarMenu()
{
    delete mButtonList;
}

//====================================== Property Queries =========================================
bool SugarMenu::HasPendingClose()
{
    return mIsPendingClose;
}

//========================================= Manipulators ==========================================
void SugarMenu::Clear()
{
    mButtonList->ClearList();
}
void SugarMenu::SetClosingFlag(bool pFlag)
{
    mIsPendingClose = pFlag;
}
void SugarMenu::RegisterButton(SugarButton *pButton)
{
    if(!pButton) return;
    mButtonList->AddElement(pButton->GetName(), pButton, SugarButton::DeleteThis);
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
