//--Base
#include "SugarButton.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

//=========================================== System ==============================================
SugarButton::SugarButton()
{
    //--[SugarButton]
    //--System
    mLocalName = InitializeString("Unnamed Button");
    mLocalPriority = 0;

    //--Location
    mCoordinates.Set(0.0f, 0.0f, 1.0f, 1.0f);

    //--Execution
    mExecutionList = new SugarLinkedList(true);
}
SugarButton::~SugarButton()
{
    free(mLocalName);
    delete mExecutionList;
}
void SugarButton::DeleteThis(void *pPtr)
{
    delete ((SugarButton *)pPtr);
}

//====================================== Property Queries =========================================
char *SugarButton::GetName()
{
    return mLocalName;
}
int SugarButton::GetPriority()
{
    return mLocalPriority;
}

//========================================= Manipulators ==========================================
void SugarButton::SetName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void SugarButton::SetPriority(int pPriority)
{
    mLocalPriority = pPriority;
}
void SugarButton::SetPosition(float pLft, float pTop)
{
    mCoordinates.SetPosition(pLft, pTop);
}
void SugarButton::SetDimensions(float pLft, float pTop, float pRgt, float pBot)
{
    mCoordinates.Set(pLft, pTop, pRgt, pBot);
}
void SugarButton::SetDimensionsWH(float pLft, float pTop, float pWid, float pHei)
{
    mCoordinates.SetWH(pLft, pTop, pWid, pHei);
}
void SugarButton::RegisterExecPack(VirtualButtonExecutionPack *pPack)
{
    //--Registration function. Can be used externally or internally, but assumes the pack is
    //  assembled before passage.
    if(!pPack) return;
    mExecutionList->AddElement("X", pPack, VirtualButtonExecutionPack::DeleteThis);
}
void SugarButton::RegisterLuaExecPack(int pPriority, const char *pScriptPath)
{
    //--Registers a Lua execution pack with no frills, just the script. The firing code defaults to 0.0f.
    RegisterLuaExecPack(pPriority, pScriptPath, 0.0f);
}
void SugarButton::RegisterLuaExecPack(int pPriority, const char *pScriptPath, float pFiringCode)
{
    //--Registers a Lua execution pack with a firing code. The script will execute with the
    //  firing code.
    if(!pScriptPath) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    VirtualButtonExecutionPack *nPack = (VirtualButtonExecutionPack *)starmemoryalloc(sizeof(VirtualButtonExecutionPack));
    nPack->Initialize();
    nPack->mPriority = pPriority;

    //--Lua Setup.
    nPack->mIsLua = true;
    ResetString(nPack->mLuaExec, pScriptPath);
    nPack->mFiringCode = pFiringCode;

    //--Register.
    RegisterExecPack(nPack);
}

//========================================= Core Methods ==========================================
void SugarButton::SortPackages()
{
    //--Sorts the packages. Duh. Packages are sorted by priority, with ties being undefined.
    mExecutionList->SortListUsing(SugarButton::CompareButtonPacks);
}
void SugarButton::Execute()
{
    //--Executes all the packages.

    //--Sort the packages. This way, they will execute in the correct order.
    SortPackages();

    //--Iterate and execute all packages.
    VirtualButtonExecutionPack *rPack = (VirtualButtonExecutionPack *)mExecutionList->PushIterator();
    while(rPack)
    {
        ExecutePackage(rPack);
        rPack = (VirtualButtonExecutionPack *)mExecutionList->AutoIterate();
    }
}
void SugarButton::ExecutePackage(VirtualButtonExecutionPack *pPack)
{
    //--Executes a provided package.
    if(!pPack) return;

    //--Lua Version.
    if(pPack->mIsLua && pPack->mLuaExec)
    {
        LuaManager::Fetch()->ExecuteLuaFile(pPack->mLuaExec, 1, "N", pPack->mFiringCode);
    }
}

//===================================== Private Core Methods ======================================
int SugarButton::CompareButtonPacks(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used to sort the packages. The Entries hold VirtualButtonExecutionPacks
    //  which have a comparable priority value.
    SugarLinkedListEntry **rEntryA = (SugarLinkedListEntry **)pEntryA;
    SugarLinkedListEntry **rEntryB = (SugarLinkedListEntry **)pEntryB;

    //--Get the data pointer.
    VirtualButtonExecutionPack *rPackA = (VirtualButtonExecutionPack *)((*rEntryA)->rData);
    VirtualButtonExecutionPack *rPackB = (VirtualButtonExecutionPack *)((*rEntryB)->rData);
    return rPackA->mPriority - rPackB->mPriority;
}

//============================================ Update =============================================
void SugarButton::Update(int pMouseX, int pMouseY)
{
    //--Handles an update. Update calls are optional, it's possible for the menu itself to handle
    //  all the updating necessary by itself.
    //--Base class does nothing.
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void SugarButton::HookToLuaState(lua_State *pLuaState)
{
    /* SugarButton_SetProperty("Name", sName)
       SugarButton_SetProperty("Priority", iPriority)
       Sets the requested property in the Active SugarButton or derived class. */
    lua_register(pLuaState, "SugarButton_SetProperty", &Hook_SugarButton_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_SugarButton_SetProperty(lua_State *L)
{
    //SugarButton_SetProperty("Name", sName)
    //SugarButton_SetProperty("Priority", iPriority)
    //SugarButton_SetProperty("NewLuaExec", iExecPriority, sLuaPath, iFiringCode)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("SugarButton_SetProperty");

    //--Type check.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid()) return LuaTypeError("SugarButton_SetProperty");
    SugarButton *rButton = (SugarButton *)rDataLibrary->rActiveObject;

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    //--Name of the button.
    if(!strcasecmp("Name", rSwitchType) && tArgs == 2)
    {
        rButton->SetName(lua_tostring(L, 2));
    }
    //--Priority. Used for ordering buttons when using an auto-ordered menu.
    else if(!strcasecmp("Priority", rSwitchType) && tArgs == 2)
    {
        rButton->SetPriority(lua_tointeger(L, 2));
    }
    //--Creates a new lua execution.
    else if(!strcasecmp("NewLuaExec", rSwitchType) && tArgs == 4)
    {
        rButton->RegisterLuaExecPack(lua_tointeger(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    //--Error case.
    else
    {
        return LuaPropertyError("SugarButton_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
