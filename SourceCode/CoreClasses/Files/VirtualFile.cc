//--Base
#include "VirtualFile.h"
#include <string.h>

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "OptionsManager.h"

//#define VF_DEBUG
#ifdef VF_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//=========================================== System ==============================================
VirtualFile::VirtualFile(const char *pPath, bool pMustBeMemoryMode)
{
    //--[Documentation and Setup]
    //--The VirtualFile can optionally be forced to Memory Mode. If this is the case, the static flag
    //  is bypassed, and drive-loaded files will fail to load properly. If you're going to write back
    //  into the file or access its data pointer directly, use the pMustBeMemoryMode flag.
    //--If you only intend to read from the file, it can be in either memory or drive mode.

    //--[VirtualFile]
    //--System
    mIsReady = false;
    mIsRAMLoading = xUseRAMLoading;
    mUseOneByteForStringLengths = true;

    //--Storage
    mFileLength = 0;
    mData = NULL;

    //--Reading
    fInfile = NULL;
    mFilePointer = 0;

    //--Public Variables
    mAssumeZeroIsMax = false;
    mDebugFlag = false;

    //--[Special Override]
    //--If the file is mandated to be in memory mode, set this flag.
    if(pMustBeMemoryMode) mIsRAMLoading = true;

    //--[File Checking]
    //--Error check (if this errors out, the file won't work)
    mIsReady = false;
    if(!pPath) return;

    //--Open the file and error check
    fInfile = fopen(pPath, "rb");
    if(!fInfile)
    {
        return;
    }

    //--Scan the file for its length (may need to optimize)
    fseek(fInfile, 0, SEEK_END);
    mFileLength = ftell(fInfile);
    rewind(fInfile);

    //--[RAM Loading Mode]
    //--Attempt to scan the entire file into memory. This speeds up loading.
    if(mIsRAMLoading)
    {
        //--Allocate.
        SetMemoryData(__FILE__, __LINE__);
        mData = (unsigned char *)starmemoryalloc(sizeof(unsigned char) * mFileLength);

        //--Error check. Some of the files can get pretty big, so it's possible the OS will just not allocate
        //  the memory for us. While this usually causes a program crash, we can at least handle it, and switch
        //  into normal file parsing.
        if(!mData)
        {
            //--If the file *must* be in memory mode, which is the case for writing a file to the hard drive, we
            //  have to fail here.
            if(pMustBeMemoryMode)
            {
                mIsReady = false;
                DebugManager::ForcePrint("VirtualFile ==> Warning, unable to allocate file size %i, forced to memory mode. Failing.\n", mFileLength);
                fclose(fInfile);
                fInfile = NULL;
                return;
            }
        }
        else
        {
            //--Set flags.
            mIsReady = true;
            fread(mData, sizeof(unsigned char) * mFileLength, 1, fInfile);

            //--File had no errors, it can now be used
            fclose(fInfile);
            fInfile = NULL;
            return;
        }
    }

    //--[Drive Loading Mode]
    //--If we got this far, the file is loading off the hard drive and not out of RAM.
    mIsReady = true;
    mIsRAMLoading = false;
}
VirtualFile::~VirtualFile()
{
    free(mData);
    if(fInfile) fclose(fInfile);
}

//--[Public Statics]
//--When set to true, the entire file in question is loaded into memory in a single read action, and
//  the data is then read out of memory. This is considerably faster but for larger files uses a lot
//  of memory and may cause the file to fail to load due to memory fragmentation.
//--If the memory allocation fails to work, this mode will activate anyway. The local variables
//  mIsRAMLoading determines this.
//--This option is set in the OptionsManager. It gets queried each time a new file is created.
bool VirtualFile::xUseRAMLoading = false;

//====================================== Property Queries =========================================
bool VirtualFile::IsReady()
{
    return mIsReady;
}
bool VirtualFile::IsInMemoryMode()
{
    return mIsRAMLoading;
}
long int VirtualFile::GetLength()
{
    return mFileLength;
}
long int VirtualFile::GetCurPosition()
{
    return mFilePointer;
}
void VirtualFile::Read(void *pDest, int pSizePerItem, int pItemsTotal)
{
    //--[Error Check]
    if(!pDest) return;

    //--[RAM Loading]
    //--Read the data out of the memory block.
    if(mIsRAMLoading)
    {
        memcpy(pDest, &mData[mFilePointer], pSizePerItem * pItemsTotal);
        mFilePointer += pSizePerItem * pItemsTotal;
    }
    //--[Drive Loading]
    //--Read off the hard drive.
    else if(fInfile)
    {
        fread(pDest, pSizePerItem, pItemsTotal, fInfile);
        mFilePointer += pSizePerItem * pItemsTotal;
    }
}
char *VirtualFile::ReadLenString()
{
    //--Assuming the file is in position, reads a length/string combo and returns it as a newly
    //  allocated string. The length is always assumed to be two bytes long unless a flag is set.
    uint32_t tLen = 0;
    if(mUseOneByteForStringLengths)
    {
        Read(&tLen, sizeof(uint8_t), 1);
    }
    else
    {
        Read(&tLen, sizeof(uint16_t), 1);
    }

    //--If the value comes back as zero, then assume it's 256 characters. This is only when doing
    //  repair work. 16-bit cases never have this problem.
    if(tLen == 0 && mAssumeZeroIsMax)
    {
        if(mUseOneByteForStringLengths)
        {
            tLen = 256;
        }
        else
        {
            tLen = 0;
        }
    }

    //--Allocate name.
    SetMemoryData(__FILE__, __LINE__);
    char *nString = (char *)starmemoryalloc(sizeof(char) * (tLen + 1));
    Read(nString, sizeof(char), tLen);
    nString[tLen] = '\0';
    return nString;
}
#include <stdint.h>
void VirtualFile::ReadImage(void *pDest, int pXOrig, int pYOrig, int pXNew, int pYNew, int pSizePerItem)
{
    //--Special case when reading an image which requires a specific size, but
    //  has been compressed by the EIM.  This function will "pad" the edges of
    //  the image with transparent pixels for you!
    uint8_t *rDest = (uint8_t *)pDest;
    if(!rDest) return;

    //--Setup.
    int i = 0;

    //--Pad the top of the image. Remember that it gets flipped, so this is technically the bottom.
    for(int y = pYNew-1; y >= pYOrig; y --)
    {
        for(int x = 0; x < pXNew * pSizePerItem; x ++)
        {
            rDest[i] = 0;
            i ++;
        }
    }

    //--Crossload, pad right edge.
    for(int y = pYOrig-1; y >= 0; y --)
    {
        //--Crossload.
        for(int x = 0; x < pXOrig * pSizePerItem; x ++)
        {
            //--Memory loader:
            if(mIsRAMLoading)
            {
                rDest[i] = mData[mFilePointer];
                mFilePointer ++;
            }
            //--Hard-drive loader:
            else
            {
                fread(&rDest[i], sizeof(uint8_t), 1, fInfile);
                mFilePointer ++;
            }

            //--Move the image's array pointer.
            i ++;
        }

        //--Pad the right edge of the image.
        for(int x = pXOrig * pSizePerItem; x < (pXNew * pSizePerItem); x ++)
        {
            rDest[i] = 0;
            i ++;
        }
    }
}

//========================================= Manipulators ==========================================
void VirtualFile::SetUseOneByteForStringLengths(bool pFlag)
{
    mUseOneByteForStringLengths = pFlag;
}
void VirtualFile::Seek(int pPosition)
{
    //--Note: Relatively slow when reading from the hard drive. Fast when in memory.
    mFilePointer = pPosition;
    if(!mIsRAMLoading && fInfile)
    {
        fseek(fInfile, mFilePointer, SEEK_SET);
    }
}
void VirtualFile::SeekRelative(int pChange)
{
    //--Reposition the memory cursor.
    mFilePointer += pChange;
    if(mFilePointer < 0) mFilePointer = 0;
    if(mFilePointer >= mFileLength) mFilePointer = mFileLength-1;

    //--Reposition the file cursor to wherever the pointer ended up.
    if(!mIsRAMLoading && fInfile)
    {
        fseek(fInfile, mFilePointer, SEEK_SET);
    }
}

//========================================= Core Methods ==========================================
void VirtualFile::SkipLenString()
{
    //--Same basic functionality as ReadLenString(), except doesn't allocate or return anything. It
    //  merely skips over the string since starmemoryalloc() is a fairly slow function.
    uint32_t tLen = 0;
    if(mUseOneByteForStringLengths)
    {
        Read(&tLen, sizeof(uint8_t), 1);
    }
    else
    {
        Read(&tLen, sizeof(uint16_t), 1);
    }
    SeekRelative(tLen);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
void VirtualFile::Write(const char *pFileDest)
{
    //--Re-write whatever data existed for the file. A warning is printed if this is attempted with
    //  a drive-reading file. Files must be memory-based.
    if(!mIsRAMLoading)
    {
        DebugManager::ForcePrint("VirtualFile Error: Unable to write file when in drive-loading mode.\n");
        return;
    }

    //--Open, write, close.
    FILE *fOutfile = fopen(pFileDest, "wb");
    if(!fOutfile) return;

    fwrite(mData, sizeof(unsigned char), mFileLength, fOutfile);

    fclose(fOutfile);
}

//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
unsigned char *VirtualFile::GetData()
{
    //--Does nothing when in drive-loading.
    if(!mIsRAMLoading)
    {
        DebugManager::ForcePrint("VirtualFile Error: Unable to return valid data pointer when in drive-loading mode.\n");
        return NULL;
    }

    //--Memory-loading works as normal.
    return mData;
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================

