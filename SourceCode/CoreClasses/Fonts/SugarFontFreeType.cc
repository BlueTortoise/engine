//--Base
#include "SugarFont.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "UString.h"

//--Definitions
#include "GlDfn.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"

//--SugarFont Implementation specific to Freetype.  Entire file will not be included if Freetype is not
//  the compilation style.
#if defined _FONTTYPE_FREETYPE_

//=========================================== System ==============================================
void SugarFont::ConstructWith(const char *pLoadPath, int pFontSize, uint8_t pFlags)
{
    //--Creates the SugarFont and manually loads the font according to the passed size. If size
    //  is zero or lower, the default size is used.
    if(!pLoadPath) return;

    //--Range check.
    if(pFontSize < 1) pFontSize = SUGARFONT_DEFAULTSIZE;

    //--Font storage
    mOwnsFont = true;
    mInternalSize = pFontSize;
    FT_New_Face(xFTLibrary, pLoadPath, 0, &rFont);
    FT_Set_Pixel_Sizes(rFont, 0, pFontSize);

    //--If the font is valid, we can be ready immediately.
    if(rFont) mIsReady = true;

    //--If in precache mode, and in Freetype mode, then precache the font here.
    PrecacheData(pFlags);
}

//--[Private Statics]
FT_Library SugarFont::xFTLibrary;

//====================================== Property Queries =========================================
int SugarFont::GetTextWidth(const char *pString)
{
    //--Returns how wide the given string would be on screen, in pixels. Note that this does not
    //  take font scaling into account. If a coefficient has been applied, multiply this result
    //  by that coefficient to get the final pixel size.
    if(!mIsReady || !pString) return 1;

    //--[Bitmap Font]
    if(mIsBitmapMode) return GetBitmapTextWidth(pString);

    //--One-letter.
    if(pString[0] == '\0') return 0;

    //--[Normal Case]
    //--Setup.
    float tRunningOffset = 0.0f;

    //--Precached data runs *considerably* faster since the advances are already stored.
    if(mPrecacheData)
    {
        //--For each letter...
        int tLen = (int)strlen(pString);
        for(int i = 0; i < tLen - 1; i ++)
        {
            tRunningOffset = tRunningOffset + mPrecacheData[(int)pString[i]].mKerningDistance[(int)pString[i+1]];
        }

        //--Last letter. Advance distance is just the width of the letter.
        tRunningOffset = tRunningOffset + mPrecacheData[(int)pString[tLen-1]].mAdvanceX;
    }
    //--Non-precached data is a lot slower. Don't use this unless you're experimenting.
    else
    {
        //--For each letter...
        int tLen = (int)strlen(pString);
        for(int i = 0; i < tLen; i ++)
        {
            //--Get the Glyph.  If it doesn't exist, 0 comes back.
            uint32_t tGlyphIndex = FT_Get_Char_Index(rFont, pString[i]);
            if(!tGlyphIndex) continue;

            FT_Load_Glyph(rFont, tGlyphIndex, FT_LOAD_RENDER);
            FT_GlyphSlot rGlyph = rFont->glyph;

            tRunningOffset = tRunningOffset + (float)(rGlyph->advance.x >> 6);
        }
    }

    //--Pass the result back.
    return tRunningOffset;
}
int SugarFont::GetTextHeight()
{
    //--Returns how tall the given string would be on screen, in pixels.
    if(mIsBitmapMode) return GetBitmapTextHeight();
    return mTallestCharacter;
}

//========================================= Manipulators ==========================================
void SugarFont::Bind()
{
    //--Binds the requested texture if precaching is enabled. Otherwise, does nothing.
    if(!mIsReady) return;
    if(mPrecachedHandle) glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);
}

//========================================= Core Methods ==========================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void SugarFont::DrawText(float pX, float pY, int pFlags, float pSize, const char *pText)
{
    //--[Documentation]
    //--Master draw function. Translate to the provided position and render the text.
    if(!mIsReady || !pText) return;

    //--[Bitmap]
    //--Independent of library type, uses a single bitmap for ASCII-only rendering.
    if(mIsBitmapMode) { DrawTextBitmap(pX, pY, pFlags, pSize, pText); return; }

    //--[Error Checking]
    if(!mPrecachedHandle || !mPrecacheData) return;
    if(pSize == 0.0f) pSize = 1.0f;

    //--[Positioning]
    //--Move cursor. If flagged, use this as the center point (mod the offset).
    if(pFlags & SUGARFONT_AUTOCENTER_X)
    {
        pX = pX - (GetTextWidth(pText) / 2.0f * pSize);
    }
    else if(pFlags & SUGARFONT_RIGHTALIGN_X)
    {
        pX = pX - (GetTextWidth(pText) * pSize);
    }
    if(pFlags & SUGARFONT_AUTOCENTER_Y)
    {
        pY = pY - (GetTextHeight() / 2.0f * pSize);
    }
    glTranslatef(pX, pY, 0.0f);

    //--Sizing. This is only used if the size is not 1.0f, since scaling is non-trivial.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(pSize, pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    //--[Rendering]
    //--Grab each glyph and render it in turn. First, bind the texture (precached mode).
    glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);

    //--For each letter...
    float tRunningOffset = 0.0f;
    int tLen = strlen(pText);
    for(int i = 0; i < tLen-1; i ++)
    {
        //--Get the index and store it as an integer. If the letter somehow does not have its
        //  glLists built, skip it.
        int8_t tLetter = pText[i];
        int8_t tNextLetter = pText[i+1];
        if(!mPrecacheData[tLetter].mGLList) continue;

        //--[Special Flag Cases]
        //--Italics.
        if(pFlags & SUGARFONT_ITALIC)
        {
            glCallList(mPrecacheData[tLetter].mGLListItalic);
        }
        //--Mirror around the X axis. Doesn't render the original glyph.
        else if(pFlags & SUGARFONT_MIRRORX)
        {
            glCallList(mPrecacheData[tLetter].mGLListXMirror);
        }
        //--Mirror around the Y axis. Doesn't render the original glyph.
        else if(pFlags & SUGARFONT_MIRRORY)
        {
            glCallList(mPrecacheData[tLetter].mGLListYMirror);
        }
        //--Double-render X. Renders the letter twice, once normally and once flipped on the X-axis.
        else if(pFlags & SUGARFONT_DOUBLERENDERX)
        {
            //--Normal.
            glCallList(mPrecacheData[tLetter].mGLList);

            //--Flipped.
            glTranslatef(mPrecacheData[tLetter].mAdvanceX, 0.0f, 0.0f);
            glScalef(-1.0f, 1.0f, 1.0f);
            glCallList(mPrecacheData[tLetter].mGLList);
            glScalef(-1.0f, 1.0f, 1.0f);
            glTranslatef(mPrecacheData[tLetter].mAdvanceX * -1.0f, 0.0f, 0.0f);
        }
        //--Double-render Y. Renders the letter twice, once normally and once flipped on the Y-axis.
        else if(pFlags & SUGARFONT_DOUBLERENDERY)
        {
            //--Normal.
            glCallList(mPrecacheData[tLetter].mGLList);

            //--Compute.
            float cYPosition = mPrecacheData[tLetter].mFlippedTopOffset * 1.0f;

            //--Flipped.
            glTranslatef(0.0f, cYPosition, 0.0f);
            glScalef(1.0f, -1.0f, 1.0f);
            glCallList(mPrecacheData[tLetter].mGLList);
            glScalef(1.0f, -1.0f, 1.0f);
            glTranslatef(0.0f, -cYPosition, 0.0f);
        }
        //--Normal rendering.
        else
        {
            glCallList(mPrecacheData[tLetter].mGLList);
        }

        //--Reposition the cursor.
        float cKernDistance = mPrecacheData[tLetter].mKerningDistance[tNextLetter];
        glTranslatef(cKernDistance, 0.0f, 0.0f);
        tRunningOffset = tRunningOffset + cKernDistance;
    }

    //--Last letter. Doesn't need to use the kerning.
    int8_t tLastLetter = pText[tLen-1];

    //--[Special Flag Cases]
    //--Italics.
    if(pFlags & SUGARFONT_ITALIC)
    {
        glCallList(mPrecacheData[tLastLetter].mGLListItalic);
    }
    //--Mirror around the X axis. Doesn't render the original glyph.
    else if(pFlags & SUGARFONT_MIRRORX)
    {
        glCallList(mPrecacheData[tLastLetter].mGLListXMirror);
    }
    //--Mirror around the Y axis. Doesn't render the original glyph.
    else if(pFlags & SUGARFONT_MIRRORY)
    {
        glCallList(mPrecacheData[tLastLetter].mGLListYMirror);
    }
    //--Double-render X. Renders the letter twice, once normally and once flipped on the X-axis.
    else if(pFlags & SUGARFONT_DOUBLERENDERX)
    {
        //--Normal.
        glCallList(mPrecacheData[tLastLetter].mGLList);

        //--Flipped.
        glTranslatef(mPrecacheData[tLastLetter].mAdvanceX, 0.0f, 0.0f);
        glScalef(-1.0f, 1.0f, 1.0f);
        glCallList(mPrecacheData[tLastLetter].mGLList);
        glScalef(-1.0f, 1.0f, 1.0f);
        glTranslatef(mPrecacheData[tLastLetter].mAdvanceX * -1.0f, 0.0f, 0.0f);
    }
    //--Double-render Y. Renders the letter twice, once normally and once flipped on the Y-axis.
    else if(pFlags & SUGARFONT_DOUBLERENDERY)
    {
        //--Normal.
        glCallList(mPrecacheData[tLastLetter].mGLList);

        //--Compute.
        float cYPosition = mPrecacheData[tLastLetter].mFlippedTopOffset * 1.0f;

        //--Flipped.
        glTranslatef(0.0f, cYPosition, 0.0f);
        glScalef(1.0f, -1.0f, 1.0f);
        glCallList(mPrecacheData[tLastLetter].mGLList);
        glScalef(1.0f, -1.0f, 1.0f);
        glTranslatef(0.0f, -cYPosition, 0.0f);
    }
    //--Normal rendering.
    else
    {
        glCallList(mPrecacheData[tLastLetter].mGLList);
    }

    //--[Clean Up]
    //--Remove running offset.
    glTranslatef(tRunningOffset * -1.0f, 0.0f, 0.0f);

    //--Undo the size change.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(1.0f / pSize, 1.0f / pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    //--Undo by cursor.
    glTranslatef(pX * -1.0f, pY * -1.0f, 0.0f);
}

//--[Single-Letter Rendering]
//--Overload. Passes 0 for the next letter, which ignores kerning.
float SugarFont::DrawLetter(float pX, float pY, int pFlags, float pSize, char pLetter)
{
    return DrawLetter(pX, pY, pFlags, pSize, pLetter, 0);
}

float SugarFont::DrawLetter(float pX, float pY, int pFlags, float pSize, char pLetter, char pNextLetter)
{
    //--Used for cases where you want to render a single letter at a time due to varying colors,
    //  styles, or other effects. This is often used when rendering complex dialogue cases.
    //--Returns the advance amount to render the next letter, including Kerning. Pass 0 to not compute
    //  kerning with the advance rate. Ignore the return value if you're using fixed-width cases.
    if(!mIsReady) return 0.0f;

    //--Buffer, used for some size checks.
    char tBuffer[2];
    tBuffer[0] = pLetter;
    tBuffer[1] = '\0';

    //--[Bitmap]
    //--Independent of library type, uses a single bitmap for ASCII-only rendering. Always returns a fixed width.
    if(mIsBitmapMode)
    {
        DrawTextBitmap(pX, pY, pFlags, pSize, tBuffer);
        return GetBitmapTextWidth("A");
    }

    //--[Error Checking]
    //--Data must be precached, size must be non-zero or scaling will crash the program.
    if(!mPrecachedHandle || !mPrecacheData) return 0.0f;
    if(pSize == 0.0f) pSize = 1.0f;

    //--[Positioning]
    //--Move cursor. If flagged, use this as the center point (mod the offset).
    if(pFlags & SUGARFONT_AUTOCENTER_X)
    {
        pX = pX - (GetTextWidth(tBuffer) / 2.0f * pSize);
    }
    else if(pFlags & SUGARFONT_RIGHTALIGN_X)
    {
        pX = pX - (GetTextWidth(tBuffer) * pSize);
    }
    if(pFlags & SUGARFONT_AUTOCENTER_Y)
    {
        pY = pY - (GetTextHeight() / 2.0f * pSize);
    }
    glTranslatef(pX, pY, 0.0f);

    //--Sizing. This is only used if the size is not 1.0f, since scaling is non-trivial.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(pSize, pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    //--[Rendering]
    //--Grab each glyph and render it in turn. First, bind the texture (precached mode).
    glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);

    //--Render the letter.
    if(mPrecacheData[(int)pLetter].mGLList)
    {
        glCallList(mPrecacheData[(int)pLetter].mGLList);
    }

    //--[Clean Up]
    //--Undo the size change.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(1.0f / pSize, 1.0f / pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    //--Undo by cursor.
    glTranslatef(pX * -1.0f, pY * -1.0f, 0.0f);

    //--[Kerning]
    //--If no next letter is provided, we return 0.0f.
    if(pNextLetter == 0) return 0.0f;

    //--A lookup table will provide the distance to the next letter.
    return mPrecacheData[(int)pLetter].mKerningDistance[(int)pNextLetter];
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================

#endif
