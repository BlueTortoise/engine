//--Base
#include "SugarFont.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"
#include "UString.h"

//--Definitions
#include "DeletionFunctions.h"
#include "GlDfn.h"
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"
#include "DebugManager.h"

//--[Notes]
//--SugarFonts are different from almost all other classes in that most of the code is library-specific.
//  For this reason, the SugarFonts have a .cc file for each, that handles all things - constructor,
//  destructor, implementation.  The only thing not handled is the Lua calls, plus a few parts which
//  are common between all implementations.
//--Also, Bitmap-Mode is handled here to some degree, as it ignores the font types and uses an image
//  to render glyphs.

//=========================================== System ==============================================
SugarFont::SugarFont()
{
    //--[Documentation]
    //--Makes the SugarFont a factory-zero font, will not crash or do anything when used.

    //--[RootObject]
    //--System
    mType = POINTER_TYPE_SUGARFONT;

    //--[SugarFont]
    mIsReady = false;
    mOwnsFont = false;

    //--Font storage
    #if defined _FONTTYPE_FREETYPE_
    rFont = NULL;
    #endif

    //--Font precaching
    mEndPadding = 3.0f;
    mPrecachedHandle = 0;
    mTallestCharacter = 15.0f;
    mPrecacheData = NULL;
    mPrecachedSizeX = 0.0f;
    mPrecachedSizeY = 0.0f;

    //--Offsets
    mInternalSize = SUGARFONT_DEFAULTSIZE;

    //--Kerning
    mKerningScaler = 1.0f;

    //--Bitmap Mode
    mOwnsBitmapFont = false;
    mIsBitmapMode = false;
    mIsBitmapFixedWidth = true;
    mIsBitmapFlipped = false;
    rAlphabetBitmap = NULL;
    mBitmapCharacters = NULL;
}
SugarFont::~SugarFont()
{
    //--Delete precached data.
    if(mPrecacheData)
    {
        glDeleteTextures(1, &mPrecachedHandle);
        for(int i = 0; i < SUGARFONT_PRECACHE_LETTERS; i ++)
        {
            if(mPrecacheData[i].mGLList)        glDeleteLists(mPrecacheData[i].mGLList, 1);
            if(mPrecacheData[i].mGLListItalic)  glDeleteLists(mPrecacheData[i].mGLListItalic, 1);
            if(mPrecacheData[i].mGLListXMirror) glDeleteLists(mPrecacheData[i].mGLListXMirror, 1);
            if(mPrecacheData[i].mGLListYMirror) glDeleteLists(mPrecacheData[i].mGLListYMirror, 1);
        }
        free(mPrecacheData);
        mPrecacheData = NULL;
    }
}

//--[Public Statics]
bool SugarFont::xScaleAffectsX = true;
bool SugarFont::xScaleAffectsY = true;

//====================================== Property Queries =========================================
bool SugarFont::IsReady()
{
    return mIsReady;
}
float SugarFont::GetKerningBetween(int pLetterLft, int pLetterRgt)
{
    //--Error checks.
    if(!mIsReady) return 0.0f;
    if(mIsBitmapMode) return GetBitmapTextWidth("A");
    if(!mPrecacheData) return 0.0f;

    //--If the second letter is NULL, then return the width of the character. This is meant for cases
    //  where the user is trying to compute the length of a string.
    if(pLetterRgt == 0) return mPrecacheData[pLetterLft].mWidth;

    //--Return from the lookup table.
    return mPrecacheData[pLetterLft].mKerningDistance[pLetterRgt];
}

//========================================= Manipulators ==========================================
void SugarFont::SetKerningScaler(float pAmount)
{
    mKerningScaler = pAmount;
}
void SugarFont::SetKerning(char pLft, char pRgt, float pAmount)
{
    //--Error check.
    if(!mIsReady || !mPrecacheData) return;

    //--Range check. -1 is legal, all others illegal.
    if(pLft < -1 || pLft >= SUGARFONT_PRECACHE_LETTERS) return;
    if(pRgt < -1 || pRgt >= SUGARFONT_PRECACHE_LETTERS) return;

    //--It is not legal for both left and right to be -1.
    if(pLft == -1 && pRgt == -1) return;

    //--If the left value is -1, it means "All letters on the left side".
    if(pLft == -1)
    {
        for(int i = 0; i < SUGARFONT_PRECACHE_LETTERS; i ++)
        {
            mPrecacheData[i].mKerningDistance[(int)pRgt] = mPrecacheData[i].mWidth + (pAmount * mKerningScaler);
        }
    }
    //--If the right value is -1, it means "All letters on the right side".
    else if(pRgt == -1)
    {
        for(int i = 0; i < SUGARFONT_PRECACHE_LETTERS; i ++)
        {
            mPrecacheData[(int)pLft].mKerningDistance[i] = mPrecacheData[(int)pLft].mWidth + (pAmount * mKerningScaler);
        }
    }
    //--Normal case.
    else
    {
        mPrecacheData[(int)pLft].mKerningDistance[(int)pRgt] = mPrecacheData[(int)pLft].mWidth + (pAmount * mKerningScaler);
    }
}

//========================================= Core Methods ==========================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//--Cascading overloads. Each of these assumes neutral properties for the next overload.
void SugarFont::DrawText(const char *pText)
{
    DrawText(0.0f, 0.0f, 0, 1.0f, pText);
}
void SugarFont::DrawText(float pX, float pY, const char *pText)
{
    DrawText(pX, pY, 0, 1.0f, pText);
}
void SugarFont::DrawText(float pX, float pY, int pFlags, const char *pText)
{
    DrawText(pX, pY, pFlags, 1.0f, pText);
}

//--[Color Overload]
//--Uses a boolean to switch colors.
void SugarFont::DrawTextColor(float pX, float pY, int pFlags, float pSize, bool pUseHighlight, StarlightColor pHighlightColor, StarlightColor pBaseColor, const char *pText)
{
    if(pUseHighlight)
    {
        pHighlightColor.SetAsMixer();
    }
    else
    {
        pBaseColor.SetAsMixer();
    }

    DrawText(pX, pY, pFlags, pSize, pText);
    StarlightColor::ClearMixer();
}

//--[Args Version]
//--These versions decode the variable argument list into a single buffer and send that for rendering.
void SugarFont::DrawTextArgs(float pX, float pY, int pFlags, float pSize, const char *pText, ...)
{
    //--Get arg list.
    va_list tArgList;
    va_start(tArgList, pText);

    //--Create a buffer.
    char tBuffer[256];
    vsprintf(tBuffer, pText, tArgList);
    va_end (tArgList);

    //--Call.
    DrawText(pX, pY, pFlags, pSize, tBuffer);
}

//--Color version.
void SugarFont::DrawTextColorArgs(float pX, float pY, int pFlags, float pSize, bool pUseHighlight, StarlightColor pHighlightColor, StarlightColor pBaseColor, const char *pText, ...)
{
    if(pUseHighlight)
    {
        pHighlightColor.SetAsMixer();
    }
    else
    {
        pBaseColor.SetAsMixer();
    }

    //--Get arg list.
    va_list tArgList;
    va_start(tArgList, pText);

    //--Create a buffer.
    char tBuffer[256];
    vsprintf(tBuffer, pText, tArgList);
    va_end (tArgList);

    //--Call.
    DrawText(pX, pY, pFlags, pSize, tBuffer);
    StarlightColor::ClearMixer();
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
SugarFont *SugarFont::FetchSystemFont()
{
    //--Fetches one of the system fonts. Which one is determined by the coder, as both a TTF and
    //  Bitmap version should exist. If not found, returns a factory-zero font that does nothing
    //  but will prevent crashes.
    //--Note that both types can be used, in which case the TTF will attempt to resolve and, if that
    //  fails, the Bitmap will try to resolve.
    SugarFont *rCheckFont = NULL;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--[TTF Version]
    #if defined _SYSTEMFONT_TTF_
        rCheckFont = (SugarFont *)rDataLibrary->GetEntry("Root/Fonts/System/TTF/SystemFont");
        if(rCheckFont) return rCheckFont;
    #endif

    //--[Bitmap Version]
    #if defined _SYSTEMFONT_BITMAP_
        rCheckFont = (SugarFont *)rDataLibrary->GetEntry("Root/Fonts/System/Bitmap/SystemFont");
        if(rCheckFont) return rCheckFont;
    #endif

    //--[Failure]
    //--Returns a static-init font.
    DebugManager::ForcePrint("SugarFont: Error, unable to resolve a system font!\n");
    static SugarFont *xStaticFont = NULL;
    if(!xStaticFont) xStaticFont = new SugarFont();
    return xStaticFont;
}

//========================================= Lua Hooking ===========================================
void SugarFont::HookToLuaState(lua_State *pLuaState)
{
    /* SugarFont_SetBitmapCharacter(iIndex, iLft, iTop, iRgt, iBot, iForcedWid)
       Sets a character's properties in Bitmap mode. iIndex refers to the ASCII value of the character
       and must be between 0 and 255. The iLft-iBot properties refer to the pixel positions of the
       letter's box, and iForcedWid is how many characters the letter occupies in a line. A space,
       for example, is wider than it appears since it has no pixels. */
    lua_register(pLuaState, "SugarFont_SetBitmapCharacter", &Hook_SugarFont_SetBitmapCharacter);

    /* SugarFont_SetKerning(fKerningScaler)
       SugarFont_SetKerning(iLftCharacter, iRgtCharacter, fKerning)
       Sets the kerning properties for TTF fonts. The font must be the active object. */
    lua_register(pLuaState, "SugarFont_SetKerning", &Hook_SugarFont_SetKerning);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_SugarFont_SetBitmapCharacter(lua_State *L)
{
    //SugarFont_SetBitmapCharacter(iIndex, iLft, iTop, iRgt, iBot, iForcedWid)
    int tArgs = lua_gettop(L);
    if(tArgs < 6) return LuaArgError("SugarFont_SetBitmapCharacter");

    //--Type check.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_SUGARFONT)) return LuaTypeError("SugarFont_SetBitmapCharacter");
    SugarFont *rFont = (SugarFont *)rDataLibrary->rActiveObject;

    //--Set.
    rFont->SetBitmapCharacterSizes(lua_tointeger(L, 1), lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6));

    return 0;
}
int Hook_SugarFont_SetKerning(lua_State *L)
{
    //SugarFont_SetKerning(fKerningScaler)
    //SugarFont_SetKerning(iLftCharacter, iRgtCharacter, fKerning)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("SugarFont_SetKerning");

    //--Type check.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_SUGARFONT)) return LuaTypeError("SugarFont_SetKerning");
    SugarFont *rFont = (SugarFont *)rDataLibrary->rActiveObject;

    //--Set the kerning scaler. Only affects calls made after the scaler is set.
    if(tArgs == 1)
    {
        rFont->SetKerningScaler(lua_tonumber(L, 1));
    }
    //--Set the kerning itself.
    else if(tArgs == 3)
    {
        rFont->SetKerning(lua_tointeger(L, 1), lua_tointeger(L, 2), lua_tonumber(L, 3));
    }

    return 0;
}
