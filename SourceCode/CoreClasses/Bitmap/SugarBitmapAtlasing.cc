//--Base
#include "SugarBitmap.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "GlDfn.h"

//--GUI
//--Libraries
//--Managers

//--[Atlas Statics]
//--The active atlas pointer. When atlases are active, texture information goes to the active
//  atlas instead of the current bitmap. When the atlas is full or the mode is disabled, the
//  atlas is pointed to instead of individual texture bindings.
SugarBitmap *SugarBitmap::xrActiveAtlas = NULL;

//--These are the data indicators for the atlas itself. Only useful when one is being built.
int SugarBitmap::xAtlasDataLen = 0;
int SugarBitmap::xAtlasStride = 1;
uint8_t *SugarBitmap::xAtlasData = NULL;

//--Iteration variables for the atlas.
int SugarBitmap::xAtlasCurrentX = 0;
int SugarBitmap::xAtlasCurrentY = 0;
int SugarBitmap::xAtlasCurrentTallest = 0;

//--[Public Atlas Statics]
//--These control whether or not to allow atlasing, and whether or not it is currently active.
//  Atlasing should manually be set to only work on small images like sprites and UI parts.
bool SugarBitmap::xDisallowAtlasing = true;
bool SugarBitmap::xIsAtlasModeActive = false;

//--Flag gets toggled on if a new atlas was created in the last bitmap upload.
bool SugarBitmap::xCreatedAtlas = false;

//--Used to track how long it takes between activation and deactivation of atlasing. Used to
//  test if the atlaser is optimized.
float SugarBitmap::xActivationTime = 0.0f;

//--Atlas maximum size. Usually the same as the texture maximum size. Minimum is 1024 by GL standards.
float SugarBitmap::xAtlasMaxSize = 4096.0f;

//--Passes position data back to whatever bitmap is going to use the uploaded atlas.
float SugarBitmap::xLastAtlasLft = 0.0f;
float SugarBitmap::xLastAtlasTop = 0.0f;
float SugarBitmap::xLastAtlasRgt = 1.0f;
float SugarBitmap::xLastAtlasBot = 1.0f;

//--[Manipulators]
void SugarBitmap::AddToAtlasList(SugarBitmap *pSubImage)
{
    //--When an image is part of an atlas, it needs to be known to the master image. If the master image
    //  gets deleted, all its sub-images lose their data.
    if(mDependencyList) mDependencyList->AddElement("X", pSubImage);
}

//--[Core Methods]
void SugarBitmap::PutDataIntoAtlas(uint8_t *pArray, int &sWidth, int &sHeight, int pCompressionType, int pDataSize)
{
    //--Given a set of data that is a bitmap in compressed or uncompressed format, places it into the atlas
    //  array. Edge checking is not done for speed reasons.
    if(!pArray || sWidth < 1 || sHeight < 1 || pDataSize < 1) return;

    //--Setup.
    int cOffsetX = 0;
    int cOffsetY = 0;

    //--[Compression Types]
    //--Flags.
    bool tIsCompressedType = false;
    bool tIsHorizontalType = false;
    bool tIsVerticalType = false;

    //--Error.
    if(pCompressionType == cInvalidCompression)
    {
        return;
    }
    //--R8G8B8A8 32-Bit unsigned.
    else if(pCompressionType == cNoCompression)
    {
    }
    //--PNG not supported yet.
    else if(pCompressionType == cPNGCompression)
    {
        return;
    }
    //--Pre-compiled compressed bitmap.
    else if(pCompressionType >= cDXT1Compression && pCompressionType <= cDXT5Compression)
    {
        tIsCompressedType = true;
    }
    //--Horizontal compression.
    else if(pCompressionType == cHorizontalLineCompression)
    {
        tIsHorizontalType = true;
    }
    //--Vertical compression.
    else if(pCompressionType == cVeritcalLineCompression)
    {
        tIsVerticalType = true;
    }
    //--Array contains a single R8G8B8A8 pixel.
    else if(pCompressionType == cIsMonoColor)
    {
        sWidth = 1;
        sHeight = 1;
    }
    //--Error.
    else
    {
        return;
    }

    //--[Deconstruction]
    //--Non-compressed types, or Starlight Engine compression. That is, not OpenGL on-card compression.
    if(!tIsCompressedType)
    {
        //--Horizontal compression. Subroutine does this.
        if(tIsHorizontalType)
        {
            UncompressHorizontalDataIntoAtlas(pArray, pDataSize, sWidth, sHeight);
            return;
        }
        //--Vertical compression. Subroutine does this.
        else if(tIsVerticalType)
        {
            UncompressVerticalDataIntoAtlas(pArray, pDataSize, sWidth, sHeight);
            return;
        }
        //--Basic data, but needs sprite padding. 2 pixels of transparency are added to all four sides.
        else if(xPadSpriteForEdging)
        {
            //cOffsetX = 2;
            //cOffsetY = 2;
        }
    }

    //--Copy the data over.
    int cStride = sWidth * sizeof(uint32_t);
    for(int x = 0; x < sWidth; x ++)
    {
        for(int y = 0; y < sHeight; y ++)
        {
            //--Positions.
            int cSrc =  (x                              * sizeof(uint32_t)) +  (y                              * cStride);
            int cDst = ((x + xAtlasCurrentX + cOffsetX) * sizeof(uint32_t)) + ((y + xAtlasCurrentY + cOffsetY) * xAtlasStride);

            //--Copy across.
            xAtlasData[cDst+0] = pArray[cSrc+0];
            xAtlasData[cDst+1] = pArray[cSrc+1];
            xAtlasData[cDst+2] = pArray[cSrc+2];
            xAtlasData[cDst+3] = pArray[cSrc+3];
        }
    }

    //--Padding case.
    /*if(xPadSpriteForEdging)
    {
        sWidth += 4;
        sHeight += 4;
    }*/
}
void SugarBitmap::FinishAtlas()
{
    //--If there's no more space in the atlas, or atlasing mode has finished, upload the atlas data
    //  and finalize the binding cases for all dependent bitmaps. This should be called by the xrActiveAtlas,
    //  so the atlas values should all be legal.
    if(!xAtlasData || !mDependencyList) return;

    //--Generate a handle.
    mOwnsHandle = true;
    glGenTextures(1, &mGLHandle);

    //--Set the flags associated with this image.
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, mGLHandle);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelZoom(1.0f, -1.0f);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, xStaticFlags.mUplMinFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, xStaticFlags.mUplMagFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, xStaticFlags.mUplBaseLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, xStaticFlags.mUplMaxLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, xStaticFlags.mUplWrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, xStaticFlags.mUplWrapT);

    //--Upload the data.
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, (int)xAtlasMaxSize, (int)xAtlasMaxSize, 0, GL_RGBA, GL_UNSIGNED_BYTE, xAtlasData);

    //--Tell all the dependent bitmaps that they should point to this handle now. They already know
    //  their UV coordinates.
    //fprintf(stderr, "There are %i bitmaps dependent on this atlas.\n", mDependencyList->GetListSize());
    SugarBitmap *rDependent = (SugarBitmap *)mDependencyList->PushIterator();
    while(rDependent)
    {
        rDependent->AssignHandle(mGLHandle, false, true);
        rDependent = (SugarBitmap *)mDependencyList->AutoIterate();
    }

    //--Clean up all the atlas data.
    xrActiveAtlas = NULL;
    xAtlasDataLen = 0;
    xAtlasStride = 1;
    xAtlasCurrentX = 0;
    xAtlasCurrentY = 0;
    xAtlasCurrentTallest = 0;
    free(xAtlasData);
}
