//--Base
#include "SugarBitmap.h"

//--Classes
//--Definitions
#include "GlDfn.h"

//--Generics
//--GUI
//--Libraries
//--Managers
#include "OptionsManager.h"

//--Functions dedicated to the uploading of data to the graphics card.  For the functions which
//  retrieve this data:
//  Hard drive:  SugarBitmapImageProcessing.cc
//  SLF Files:   SugarLumpManagerImages.cc

//--Static Image Loading Variables.  These variables can be manipulated by Lua or the program at any
//  time, they are public.
SugarBitmapFlags SugarBitmap::xStaticFlags  = {GL_NEAREST, GL_NEAREST, 0, 0, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE};
SugarBitmapFlags SugarBitmap::xDefaultFlags = {GL_NEAREST, GL_NEAREST, 0, 0, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE};

const int SugarBitmap::cGLLookups[4] = {GL_COMPRESSED_RGB_S3TC_DXT1_EXT,  GL_COMPRESSED_RGBA_S3TC_DXT1_EXT,
                                        GL_COMPRESSED_RGBA_S3TC_DXT3_EXT, GL_COMPRESSED_RGBA_S3TC_DXT5_EXT};

void SugarBitmap::SetPrecacheFlags(SugarBitmapPrecache &sDataPack)
{
    //--Sets a precache to use the standard flags.
    memcpy(&sDataPack.mFlags, &xStaticFlags, sizeof(SugarBitmapFlags));
}
uint32_t SugarBitmap::UploadData(uint8_t *pArray, int pWidth, int pHeight)
{
    //--Overload to automatically generate a new texture with uncompressed images.
    return SugarBitmap::UploadData(pArray, pWidth, pHeight, 0);
}
uint32_t SugarBitmap::UploadData(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle)
{
    //--Uploads the data assuming it is not a compressed type, and is not monocolor.
    //--Sends a dummy size through, as size is not needed when using uncompressed data.
    return UploadBitmap(pArray, pWidth, pHeight, pForceHandle, cNoCompression, sizeof(uint32_t));
}
uint32_t SugarBitmap::UploadCompressed(SugarBitmapPrecache *pDataPack)
{
    //--Overload using a pack of data. Neither the pack nor its data are deallocated here.
    if(!pDataPack) return 0;

    //--Set the flags according to the precache.  Store the old ones.
    SugarBitmapFlags tOldFlags;
    memcpy(&tOldFlags, &xStaticFlags, sizeof(SugarBitmapFlags));
    memcpy(&xStaticFlags, &pDataPack->mFlags, sizeof(SugarBitmapFlags));

    //--Upload.
    uint32_t tHandle = UploadCompressed(pDataPack->mArray, pDataPack->mWidth, pDataPack->mHeight, pDataPack->mForceHandle, pDataPack->mCompressionType, pDataPack->mDataSize);

    //--Reset the flags.
    memcpy(&xStaticFlags, &tOldFlags, sizeof(SugarBitmapFlags));

    //--Done.
    return tHandle;
}
uint32_t SugarBitmap::UploadCompressed(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle, int pCompressionType, int pDataSize)
{
    //--Synonym of UploadBitmap.
    return UploadBitmap(pArray, pWidth, pHeight, pForceHandle, pCompressionType, pDataSize);
}
uint32_t SugarBitmap::UploadMonoColor(uint8_t *pArray, uint32_t pForceHandle)
{
    //--The bitmap's data represents a single pixel color, which should be applied across the
    //  entire area represented by the bitmap.  A texture of a single pixel is uploaded, which
    //  should then be stretched.
    return UploadBitmap(pArray, 1, 1, pForceHandle, cIsMonoColor, sizeof(uint32_t));
}
uint8_t *SugarBitmap::GetDataFrom(uint8_t *pArray, int &sWidth, int &sHeight, int pCompressionType, int &sDataSize)
{
    //--Given a set of bitmap data, returns the finalized data set after line-compression or padding is handled. The original
    //  data may be returned, or a newly allocated set of data may be returned.
    //--NULL is returned on error.
    if(!pArray || sWidth < 1 || sHeight < 1 || sDataSize < 1) return NULL;

    //--[Compression Types]
    //--Flags.
    bool tIsCompressedType = false;
    bool tIsHorizontalType = false;
    bool tIsVerticalType = false;

    //--Error.
    if(pCompressionType == cInvalidCompression)
    {
        return NULL;
    }
    //--R8G8B8A8 32-Bit unsigned.
    else if(pCompressionType == cNoCompression)
    {
    }
    //--PNG not supported yet.
    else if(pCompressionType == cPNGCompression)
    {
        return NULL;
    }
    //--Pre-compiled compressed bitmap.
    else if(pCompressionType >= cDXT1Compression && pCompressionType <= cDXT5Compression)
    {
        tIsCompressedType = true;
    }
    //--Horizontal compression.
    else if(pCompressionType == cHorizontalLineCompression)
    {
        tIsHorizontalType = true;
    }
    //--Vertical compression.
    else if(pCompressionType == cVeritcalLineCompression)
    {
        tIsVerticalType = true;
    }
    //--Array contains a single R8G8B8A8 pixel.
    else if(pCompressionType == cIsMonoColor)
    {
        sWidth = 1;
        sHeight = 1;
    }
    //--Error.
    else
    {
        return NULL;
    }

    //--[Deconstruction]
    //--Non-compressed types, or Starlight Engine compression. That is, not OpenGL on-card compression.
    if(!tIsCompressedType)
    {
        //--Horizontal compression. We need to assemble a new data array.
        if(tIsHorizontalType)
        {
            //fprintf(stderr, " Horizontal.\n");
            return UncompressHorizontalData(pArray, sDataSize, sWidth, sHeight);
        }
        //--Vertical compression. We need to assemble a new data array.
        else if(tIsVerticalType)
        {
            //fprintf(stderr, " Vertical.\n");
            return UncompressVerticalData(pArray, sDataSize, sWidth, sHeight);
        }
        //--Basic data, but needs sprite padding. 2 pixels of transparency are added to all four sides.
        else if(xPadSpriteForEdging)
        {
            //--Store old values.
            int tOldWid = sWidth;
            int tOldHei = sHeight;
            sWidth += 4;
            sHeight += 4;

            //--Allocate a new array.
            SetMemoryData(__FILE__, __LINE__);
            uint8_t *nNewData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * sWidth * sHeight * 4);
            memset(nNewData, 0, sizeof(uint8_t) * sWidth * sHeight * 4);

            //--Copy the data across, indented by 2 in both directions.
            for(int x = 0; x < tOldWid; x ++)
            {
                for(int y = 0; y < tOldHei; y ++)
                {
                    //--Compute array position.
                    int tOldArrayPos = (( y    * tOldWid) +  x)    * 4;
                    int tNewArrayPos = (((y+2) * sWidth)  + (x+2)) * 4;

                    //--Copy over.
                    nNewData[tNewArrayPos+0] = pArray[tOldArrayPos+0];
                    nNewData[tNewArrayPos+1] = pArray[tOldArrayPos+1];
                    nNewData[tNewArrayPos+2] = pArray[tOldArrayPos+2];
                    nNewData[tNewArrayPos+3] = pArray[tOldArrayPos+3];
                }
            }

            //--Return the new data.
            sDataSize = (sizeof(uint8_t) * sWidth * sHeight * 4);
            //fprintf(stderr, " Padded.\n");
            return nNewData;
        }
    }

    //--Return the original data with no modifications.
    //fprintf(stderr, " Normal.\n");
    return pArray;
}
uint32_t SugarBitmap::UploadBitmap(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle, int pCompressionType, int pDataSize)
{
    //--Worker function, uploads the provided data using whatever state flags were set. Needs to
    //  know all the details about the image in question, including array type and size.
    //--Handles uncompressing data that is compressed, if necessary.
    //--Returns 0 on error or if the image didn't need any work done. Otherwise, returns the texture handle
    //  of the newly uploaded data.
    if(!pArray || pWidth < 1 || pHeight < 1 || pDataSize < 1) return 0;

    //--Clear the binding, just in case.
    glBindTexture(GL_TEXTURE_2D, 0);

    //--If the pForceHandle was 0, generate a new handle.  Otherwise, use the handle requested.
    uint32_t tTextureHandle = pForceHandle;
    if(!tTextureHandle) glGenTextures(1, &tTextureHandle);

    //--Get the raw data, after Starlight compression and/or padding. This may or may not be the same
    //  as pArray if no modifications were necessary.
    //fprintf(stderr, "Getting new data: %i %i %i\n", pWidth, pHeight, pDataSize);
    uint8_t *tNewData = GetDataFrom(pArray, pWidth, pHeight, pCompressionType, pDataSize);
    //fprintf(stderr, " New data was %p\n", tNewData);
    if(!tNewData) return 0;

    //--[Mipmapping Handler]
    //--Special: If the OptionsManager has disallowed bitmap mipmapping, flip flags off.
    bool tDisallowMipmapping = OptionsManager::Fetch()->GetOptionB("DisallowMipmapping");
    if(tDisallowMipmapping)
    {
        //--Minification filter.
        if(xStaticFlags.mUplMinFilter == GL_NEAREST_MIPMAP_LINEAR || xStaticFlags.mUplMinFilter == GL_NEAREST_MIPMAP_NEAREST)
        {
            xStaticFlags.mUplMinFilter = GL_NEAREST;
        }
        else if(xStaticFlags.mUplMinFilter == GL_LINEAR_MIPMAP_LINEAR || xStaticFlags.mUplMinFilter == GL_LINEAR_MIPMAP_NEAREST)
        {
            xStaticFlags.mUplMinFilter = GL_LINEAR;
        }

        //--Magnification filter.
        if(xStaticFlags.mUplMagFilter == GL_NEAREST_MIPMAP_LINEAR || xStaticFlags.mUplMagFilter == GL_NEAREST_MIPMAP_NEAREST)
        {
            xStaticFlags.mUplMagFilter = GL_NEAREST;
        }
        else if(xStaticFlags.mUplMagFilter == GL_LINEAR_MIPMAP_LINEAR || xStaticFlags.mUplMagFilter == GL_LINEAR_MIPMAP_NEAREST)
        {
            xStaticFlags.mUplMagFilter = GL_LINEAR;
        }
    }

    //--[Flag Setup]
    //--Set the flags associated with this image.
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tTextureHandle);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelZoom(1.0f, -1.0f);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, xStaticFlags.mUplMinFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, xStaticFlags.mUplMagFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, xStaticFlags.mUplBaseLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, xStaticFlags.mUplMaxLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, xStaticFlags.mUplWrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, xStaticFlags.mUplWrapT);

    //--[Upload]
    //--On-Card OpenGL compression cases.
    if(pCompressionType >= cDXT1Compression && pCompressionType <= cDXT5Compression)
    {
        uint32_t tCompression = cGLLookups[pCompressionType - cDXT1Compression];
        sglCompressedTexImage2D(GL_TEXTURE_2D, 0, tCompression, pWidth, pHeight, 0, pDataSize, tNewData);
    }
    //--Send the data to the graphics card in uncompressed formats.
    else
    {
        InterceptGlTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, pWidth, pHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, tNewData);
    }

    //--Mipmap generation. Only used if mipmaps are available and at least one of the filters was a
    //  mipmap filter.
    if(sglGenerateMipmap && !tDisallowMipmapping)
    {
        if(xStaticFlags.mUplMinFilter == GL_NEAREST_MIPMAP_LINEAR  || xStaticFlags.mUplMinFilter == GL_LINEAR_MIPMAP_LINEAR ||
           xStaticFlags.mUplMinFilter == GL_NEAREST_MIPMAP_NEAREST || xStaticFlags.mUplMinFilter == GL_LINEAR_MIPMAP_NEAREST ||
           xStaticFlags.mUplMagFilter == GL_NEAREST_MIPMAP_LINEAR  || xStaticFlags.mUplMagFilter == GL_LINEAR_MIPMAP_LINEAR ||
           xStaticFlags.mUplMagFilter == GL_NEAREST_MIPMAP_NEAREST || xStaticFlags.mUplMagFilter == GL_LINEAR_MIPMAP_NEAREST)
        {
            sglGenerateMipmap(GL_TEXTURE_2D);
        }
    }

    //--Issue a call to the LoadInterrupt.  Automatically fails if the interrupt was disabled.
    if(SugarBitmap::xInterruptCall && !xSuppressInterrupt) SugarBitmap::xInterruptCall();

    //--[Clean Up]
    //--If the new texture data after uncompression was not the same as the old data, deallocate it.
    if(tNewData != pArray) free(tNewData);

    //--Return the handle if all went well.
    return tTextureHandle;
}
uint8_t *CopyRegion(uint8_t *pArray, int pWid, int pHei, int pX1, int pY1, int pX2, int pY2, int &sSizeX, int &sSizeY, int &sDataSize)
{
    //--Given a data array of size pWid and pHei, create a new array with a region copy and return it.
    if(!pArray || pWid < 1 || pHei < 1) return NULL;

    //--Range clamp.
    if(pX1 < 0) pX1 = 0;
    if(pY1 < 0) pY1 = 0;
    if(pX2 >= pWid) pX2 = pWid - 1;
    if(pY2 >= pHei) pY2 = pHei - 1;

    //--Calc space of new array.
    sSizeX = pX2 - pX1;
    sSizeY = pY2 - pY1;

    sDataSize = sizeof(uint8_t) * sSizeY * sSizeX * 4;
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *nArray = (uint8_t *)starmemoryalloc(sDataSize);

    //--Begin copying.
    for(int x = pX1; x < pX2; x ++)
    {
        for(int y = pY1; y < pY2; y ++)
        {
            int tCurArray  = ((x-pX1) + ((y-pY1) * sSizeX)) * 4;
            int tOrigArray = (x + (y * pWid)) * 4;
            nArray[tCurArray+0] = pArray[tOrigArray+0];
            nArray[tCurArray+1] = pArray[tOrigArray+1];
            nArray[tCurArray+2] = pArray[tOrigArray+2];
            nArray[tCurArray+3] = pArray[tOrigArray+3];
        }
    }
    //fprintf(stderr, " Done\n");

    //--Pass it back.
    return nArray;
}

void SugarBitmap::UploadBitmapMultiTexture(uint8_t *pArray, int pWidth, int pHeight, int pCompressionType, int pDataSize)
{
    //--When the requested texture is too large for a single image to contain due to limitations of
    //  the graphics card, we can split it into multiple smaller textures and render them side by side.
    //  This function statically returns the data needed.
    //--This only works with uncompressed data right now.
    if(!pArray || pWidth < 1 || pHeight < 1 || pDataSize < 1 || !xMaxTextureSize) return;
    xMultiGLHandles = NULL;
    xThisIsMultiTexture = true;

    //--Figure out how many textures will be needed.
    xMultiTexX = pWidth / xMaxTextureSize;
    xMultiTexY = pHeight / xMaxTextureSize;
    xMultiTexX = 2;
    xMultiTexY = 1;
    if(xMultiTexX < 1 || xMultiTexY < 1)
    {
        xThisIsMultiTexture = false;
        return;
    }

    //--Allocate space.
    SetMemoryData(__FILE__, __LINE__);
    xMultiGLHandles = (uint32_t **)starmemoryalloc(sizeof(uint32_t *) * xMultiTexX);
    for(int x = 0; x < xMultiTexX; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        xMultiGLHandles[x] = (uint32_t *)starmemoryalloc(sizeof(uint32_t) * xMultiTexY);
        for(int y = 0; y < xMultiTexY; y ++)
        {
            glGenTextures(1, &xMultiGLHandles[x][y]);
        }
    }

    //--Size storage.
    xMultiTexSizeX = pWidth;
    xMultiTexSizeY = pHeight;

    //--With all the needed textures generated, split the data array up and call the upload for each one.
    int tSizeX, tSizeY, tDataSize;
    for(int x = 0; x < xMultiTexX; x ++)
    {
        for(int y = 0; y < xMultiTexY; y ++)
        {
            //--Generate a new data array.
            uint8_t *tDataArray = CopyRegion(pArray, pWidth, pHeight, x*xMaxTextureSize, y*xMaxTextureSize, ((x+1)*xMaxTextureSize), ((y+1)*xMaxTextureSize), tSizeX, tSizeY, tDataSize);

            //--Upload it and get the data handle back.
            UploadBitmap(tDataArray, tSizeX, tSizeY, xMultiGLHandles[x][y], pCompressionType, tDataSize);

            //--Clean.
            free(tDataArray);
        }
    }
}
