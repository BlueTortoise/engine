//--Base
#include "SugarBitmap.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers
#include "DisplayManager.h"

void SugarBitmap::DrawFullBlack(float pAlpha)
{
    //--Draws a full black overlay using the given alpha, which can be 0. Covers the whole screen implicitly.
    glColor4f(0.0f, 0.0f, 0.0f, pAlpha);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(            0.0f,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X,              0.0f);
        glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
        glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);
}
void SugarBitmap::DrawFullColor(StarlightColor pColor)
{
    //--Draws a full colored overlay using a given color. Covers the whole screen.
    pColor.SetAsMixer();
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(            0.0f,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X,              0.0f);
        glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
        glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);
}
void SugarBitmap::DrawRect(float pLft, float pTop, float pRgt, float pBot, StarlightColor pColor, float pThickness)
{
    //--Renders a rectangle at the provided location, in the provided color.
    glLineWidth(pThickness);
    glColor4f(pColor.r, pColor.g, pColor.b, pColor.a);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_LINE_LOOP);
        glVertex2f(pLft, pTop);
        glVertex2f(pRgt, pTop);
        glVertex2f(pRgt, pBot);
        glVertex2f(pLft, pBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor4i(255, 255, 255, 255);
}
void SugarBitmap::DrawRectFill(float pLft, float pTop, float pRgt, float pBot, StarlightColor pColor)
{
    //--Renders a rectangle at the provided location in the provided color, but the rectangle is
    //  filled in.
    glColor4f(pColor.r, pColor.g, pColor.b, pColor.a);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(pLft, pTop);
        glVertex2f(pRgt, pTop);
        glVertex2f(pRgt, pBot);
        glVertex2f(pLft, pBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);
}
void SugarBitmap::DrawPolyLines(float *pArray, StarlightColor pColor, float pScale)
{
    //--Renders a polygon that is not filled, as a series of line segments. The 0th entry of the
    //  array is how many sides to expect.
    if(!pArray) return;

    int tSides = (int)pArray[0];
    glColor4f(pColor.r, pColor.g, pColor.b, pColor.a);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_POLYGON);
        for(int i = 0; i < tSides; i ++)
        {
            glVertex2f(pArray[(i*2)+1] * pScale, pArray[(i*2)+2] * pScale);
        }
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor4i(255, 255, 255, 255);
}
void SugarBitmap::DrawHorizontalPercent(SugarBitmap *pBitmap, float pPercent)
{
    //--Renders the left X percent of a bitmap. This is usually used for rendering things like health
    //  bars, since the bar does not stretch as it gets smaller.
    if(!pBitmap || pPercent <= 0.0f) return;
    if(pPercent > 1.0f) pPercent = 1.0f;

    //--Rendering setup.
    float cLft = pBitmap->GetXOffset();
    float cTop = pBitmap->GetYOffset();
    float cRgt = pBitmap->GetXOffset() + (pBitmap->GetWidth() * pPercent);
    float cBot = pBitmap->GetYOffset() +  pBitmap->GetHeight();
    float cTxL = 0.0f;
    float cTxT = 0.0f;
    float cTxR = pPercent;
    float cTxB = 1.0f;

    //--Flip since OpenGL is upside-down.
    cTxT = 1.0f - cTxT;
    cTxB = 1.0f - cTxB;

    //--Render.
    pBitmap->Bind();
    glBegin(GL_QUADS);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
    glEnd();
}

