//--Base
#include "StarlightString.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

//=========================================== System ==============================================
StarlightString::StarlightString()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_STARLIGHTSTRING;

    //--[StarlightString]
    //--Breakpoints
    mBreakpointList = new SugarLinkedList(true);

    //--Images
    mImagesTotal = 0;
    mYOffsets = NULL;
    mrImages = NULL;
}
StarlightString::~StarlightString()
{
    delete mBreakpointList;
    free(mYOffsets);
    free(mrImages);
}

//====================================== Property Queries =========================================
float StarlightString::GetLength(SugarFont *pFont)
{
    //--Error check.
    if(!pFont) return 1.0f;

    //--Setup.
    float tTotalLength = 0.0f;

    //--Iterate.
    StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        if(rBreakpoint->mString) tTotalLength = tTotalLength + pFont->GetTextWidth(rBreakpoint->mString);
        if(rBreakpoint->rImage)
        {
            tTotalLength = tTotalLength + rBreakpoint->rImage->GetTrueWidth();
            tTotalLength = tTotalLength + STARLIGHT_STRING_BUFFER_PIXELS;
        }

        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }

    //--Done.
    return tTotalLength;
}
int StarlightString::GetTotalChars()
{
    //--Setup.
    int tTotalChars = 0;

    //--Iterate.
    StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        if(rBreakpoint->mString) tTotalChars = tTotalChars + (int)strlen(rBreakpoint->mString);
        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }

    //--Done.
    return tTotalChars;
}

//========================================= Manipulators ==========================================
void StarlightString::AllocateImages(int pTotal)
{
    //--Deallocate.
    free(mYOffsets);
    free(mrImages);

    //--Initialize;
    mImagesTotal = 0;
    mYOffsets = NULL;
    mrImages = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mImagesTotal = pTotal;
    mYOffsets = (float *)starmemoryalloc(sizeof(float) * mImagesTotal);
    mrImages = (SugarBitmap **)starmemoryalloc(sizeof(SugarBitmap *) * mImagesTotal);
    for(int i = 0; i < mImagesTotal; i ++)
    {
        mYOffsets[i] = 0.0f;
        mrImages[i] = NULL;
    }
}
void StarlightString::SetImageS(int pSlot, float pYOffset, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= mImagesTotal) return;
    mYOffsets[pSlot] = pYOffset;
    mrImages[pSlot] = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void StarlightString::SetImageP(int pSlot, float pYOffset, SugarBitmap *pImage)
{
    if(pSlot < 0 || pSlot >= mImagesTotal) return;
    mYOffsets[pSlot] = pYOffset;
    mrImages[pSlot] = pImage;
}

//========================================= Core Methods ==========================================
void StarlightString::SetString(const char *pString, ...)
{
    //--[Documentation and Setup]
    //--Sets the internals of the string. The string is parsed individually.
    SetMemoryData(__FILE__, __LINE__);

    //--[Initialize]
    //--Clear.
    mBreakpointList->ClearList();
    if(!pString) return;

    //--[String Resolve]
    //--Get the variable arg list.
    va_list tArgList;
    va_start(tArgList, pString);

    //--Print the args into a buffer.
    char tBuffer[STARLIGHT_STRING_BUFFER_LETTERS];
    int tLetters = vsprintf(tBuffer, pString, tArgList);
    va_end(tArgList);

    //--Warning:
    if(tLetters >= STARLIGHT_STRING_BUFFER_LETTERS) fprintf(stderr, "== Warning, StarlightString vsprintf() printed %i letters, buffer was %i.\n", tLetters, STARLIGHT_STRING_BUFFER_LETTERS);

    //--[First Pass: Break Up]
    //--Scan through the string and break it into pieces. Each piece contains a string and is ended with an image.
    bool tLettersRemaining = true;
    int tStart = 0;
    int tEnd = 0;
    int cLen = (int)strlen(tBuffer);
    for(int i = 0; i < cLen; i ++)
    {
        //--Found a match.
        if(!strncmp(&tBuffer[i], "[IMG", 4))
        {
            //--Create and register new breakpoint.
            StarlightStringBreakpoint *nPack = (StarlightStringBreakpoint *)starmemoryalloc(sizeof(StarlightStringBreakpoint));
            nPack->Initialize();
            mBreakpointList->AddElementAsTail("X", nPack, &StarlightStringBreakpoint::DeleteThis);

            //--Store the substring segment.
            tEnd = i;
            int cLetters = (tEnd - tStart) + 1;

            //--Zero-length string:
            if(cLetters <= 1)
            {
                nPack->mString = (char *)starmemoryalloc(sizeof(char) * 1);
                nPack->mString[0] = '\0';
            }
            //--Non-zero-length string:
            else
            {
                nPack->mString = (char *)starmemoryalloc(sizeof(char) * cLetters);
                strncpy(nPack->mString, &tBuffer[tStart], cLetters);
                nPack->mString[cLetters-1] = '\0';
            }

            //--Scan into a buffer until a ']' is reached.
            int n = 0;
            char tNumBuf[32];
            tNumBuf[0] = '\0';
            for(int p = i+4; p < cLen; p ++)
            {
                //--Stop on a ']'
                if(tBuffer[p] == ']')
                {
                    tStart = p+1;
                    i = p;
                    if(tStart == cLen) tLettersRemaining = false;
                    break;
                }
                //--Copy across.
                else
                {
                    tNumBuf[n+0] = tBuffer[p];
                    tNumBuf[n+1] = '\0';
                    n ++;
                }
            }

            //--Resolve the number.
            nPack->mIndex = atoi(tNumBuf);
        }
        //--No special string.
        else
        {
        }
    }

    //--First, if there were no breakpoints, this is just a normal string. Create a single
    //  breakpoint to represent the whole string.
    if(mBreakpointList->GetListSize() < 1)
    {
        //--Set.
        StarlightStringBreakpoint *nPack = (StarlightStringBreakpoint *)starmemoryalloc(sizeof(StarlightStringBreakpoint));
        nPack->Initialize();
        ResetString(nPack->mString, tBuffer);
        mBreakpointList->AddElementAsTail("X", nPack, &StarlightStringBreakpoint::DeleteThis);

        //--Report.
        if(false)
        {
            fprintf(stderr, "Report:\n");
            fprintf(stderr, " Original string: %s\n", tBuffer);
            fprintf(stderr, " String has no breakpoints.\n");
            fprintf(stderr, " String Segment 0: %s\n", nPack->mString);
        }
        return;
    }

    //--Otherwise, we need to add a breakpoint for the remainder of the string after the last image block.
    //  if tLettersRemaining is false, then the last letter coincides with an image breakpoint.
    if(tLettersRemaining)
    {
        StarlightStringBreakpoint *nPack = (StarlightStringBreakpoint *)starmemoryalloc(sizeof(StarlightStringBreakpoint));
        nPack->Initialize();
        ResetString(nPack->mString, &tBuffer[tStart]);
        mBreakpointList->AddElementAsTail("X", nPack, &StarlightStringBreakpoint::DeleteThis);
    }

    //--Report.
    if(false)
    {
        fprintf(stderr, "Report:\n");
        fprintf(stderr, " Original string: %s\n", tBuffer);
        fprintf(stderr, " Segments: %i\n", mBreakpointList->GetListSize());
        int t = 0;
        StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
        while(rBreakpoint)
        {
            fprintf(stderr, "  %i: IMG %3i - String: |%s|\n", t, rBreakpoint->mIndex, rBreakpoint->mString);
            t ++;
            rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
        }
    }
}
void StarlightString::CrossreferenceImages()
{
    //--Once all images are set and the string is parsed, call this to provide pointers to all
    //  the breakpoints.
    StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        if(rBreakpoint->mIndex >= 0 && rBreakpoint->mIndex < mImagesTotal)
        {
            rBreakpoint->pYOffset = mYOffsets[rBreakpoint->mIndex];
            rBreakpoint->rImage = mrImages[rBreakpoint->mIndex];
        }
        else
        {
            rBreakpoint->rImage = NULL;
        }

        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void StarlightString::DrawText(float pX, float pY, int pFlags, float pScale, SugarFont *pFont)
{
    //--[Documentation and Setup]
    //--Simple rendering. More complicated rendering will require getting each breakpoint package
    //  and rendering those manually.
    if(!pFont) return;

    //--[Length]
    //--Compute the total length.
    float cTotalLength = 0.0f;
    StarlightStringBreakpoint *rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        if(rBreakpoint->mString) cTotalLength = cTotalLength + pFont->GetTextWidth(rBreakpoint->mString);
        if(rBreakpoint->rImage)
        {
            cTotalLength = cTotalLength + rBreakpoint->rImage->GetTrueWidth();
            cTotalLength = cTotalLength + STARLIGHT_STRING_BUFFER_PIXELS;
        }

        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }

    //--[Flag Handling]
    //--Horizontal centering:
    if(pFlags & SUGARFONT_AUTOCENTER_X)
    {
        pX = pX - (cTotalLength * 0.50f);
        pFlags = pFlags ^ SUGARFONT_AUTOCENTER_X;
    }

    //--Horizontal right-align:
    if(pFlags & SUGARFONT_RIGHTALIGN_X)
    {
        pX = pX - (cTotalLength * 1.00f);
        pFlags = pFlags ^ SUGARFONT_RIGHTALIGN_X;
    }

    //--Vertical centering:
    if(pFlags & SUGARFONT_AUTOCENTER_Y)
    {
        pY = pY - (pFont->GetTextHeight() * 0.50f);
        pFlags = pFlags ^ SUGARFONT_AUTOCENTER_Y;
    }

    //--[Scaling]
    if(pScale != 1.0f)
    {
        //TODO
    }

    //--[Render]
    rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->PushIterator();
    while(rBreakpoint)
    {
        //--Render the text component.
        if(rBreakpoint->mString)
        {
            pFont->DrawText(pX, pY, pFlags, 1.0f, rBreakpoint->mString);
            pX = pX + pFont->GetTextWidth(rBreakpoint->mString);
        }

        //--Render the image component bottom-up.
        if(rBreakpoint->rImage)
        {
            rBreakpoint->rImage->Draw(pX, pY + rBreakpoint->pYOffset);
            pX = pX + rBreakpoint->rImage->GetTrueWidth();
            pX = pX + STARLIGHT_STRING_BUFFER_PIXELS;
        }

        //--Next.
        rBreakpoint = (StarlightStringBreakpoint *)mBreakpointList->AutoIterate();
    }

    //--[Clean]
    //--Clean scaling.
    //TODO
}

//======================================= Pointer Routing =========================================
StarlightStringBreakpoint *StarlightString::GetBreakpoint(int pIndex)
{
    return (StarlightStringBreakpoint *)mBreakpointList->GetElementBySlot(pIndex);
}

//====================================== Static Functions =========================================
//===================================== Structure Functions =======================================
void StarlightStringBreakpoint::Initialize()
{
    mIndex = -1;
    mString = NULL;
    pYOffset = 0.0f;
    rImage = NULL;
}
void StarlightStringBreakpoint::DeleteThis(void *pPtr)
{
    StarlightStringBreakpoint *rPtr = (StarlightStringBreakpoint *)pPtr;
    free(rPtr->mString);
    free(rPtr);
}
int StarlightStringBreakpoint::ComputeLength(SugarFont *pFont)
{
    if(!pFont) return 0;
    int tLength = 0;
    if(mString) tLength = tLength + pFont->GetTextWidth(mString);
    if(rImage)  tLength = tLength + rImage->GetTrueWidth();
    return tLength;
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
