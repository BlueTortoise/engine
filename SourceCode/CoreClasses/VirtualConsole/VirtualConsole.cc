//--Base
#include "VirtualConsole.h"

//--Classes
//--CoreClasses
#include "SugarFont.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

//=========================================== System ==============================================
VirtualConsole::VirtualConsole()
{
    //--System
    //--Cursor
    mCursorX = 0;
    mCursorY = 0;

    //--Storage.
    for(int y = 0; y < CONSOLE_SIZE_Y; y ++)
    {
        mGLLists[y] = 0;
    }
    for(int x = 0; x < CONSOLE_SIZE_X; x ++)
    {
        for(int y = 0; y < CONSOLE_SIZE_Y; y ++)
        {
            mStringData[y][x] = '\0';
        }
    }

    //--Flags
    mEdgeHandler = EDGE_SMARTWRAP;

    //--Rendering
    mOffsetX = 12.0f;
    mOffsetY = 8.0f;
    mCharW = 8.00f;
    mCharH = 10.0f;

    //--Decisions
    mIsDecisionMode = false;
    mDecisionCursor = 0;
    mDecisionCursorMax = 0;
    mDecisionPacks = NULL;

    //--Input Catching
    mUnderscoreTimer = 0;
    mIsCatchingKeyboard = false;
    strcpy(mInputBuffer, "\0");
}
VirtualConsole::~VirtualConsole()
{
    for(int y = 0; y < CONSOLE_SIZE_Y; y ++)
    {
        glDeleteLists(mGLLists[y], 1);
    }
    ClearDecisions();
}
VirtualConsole *VirtualConsole::xStaticConsole = new VirtualConsole();

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void VirtualConsole::SetCursor(int pX, int pY)
{
    //--Changes the position the cursor is currently at.  Auto-blocks out of range cases.
    if(pX >= 0 && pX < CONSOLE_SIZE_X)
    {
        mCursorX = pX;
    }
    if(pY >= 0 && pY < CONSOLE_SIZE_Y)
    {
        mCursorY = pY;
    }
}
void VirtualConsole::SetCharacter(int pX, int pY, char pLetter)
{
    //--Sets a character at a specified position, without affecting the cursor.
    if(pX < 0 || pY < 0 || pX >= CONSOLE_SIZE_X || pY >= CONSOLE_SIZE_Y) return;
    mStringData[pY][pX] = pLetter;
    glDeleteLists(mGLLists[pY], 1);
    mGLLists[pY] = 0;
}
void VirtualConsole::AppendAtCursor(const char *pString)
{
    //--Appends the specified string at the cursor position provided.  Does not use string functions
    //  for this purpose, because here we manually edge-check the console.
    if(!pString) return;

    //--Storage.
    uint32_t tLen = strlen(pString);

    //--Copying routine.  What we do after hitting the edge is based on the edge flag.
    for(uint32_t i = 0; i < tLen; i ++)
    {
        //--Set char.
        mStringData[mCursorY][mCursorX] = pString[i];
        glDeleteLists(mGLLists[mCursorY], 1);
        mGLLists[mCursorY] = 0;

        //--Increment the X position.
        mCursorX ++;

        //--Are we past the edge?  Determine what to do.
        if(mCursorX >= CONSOLE_SIZE_X)
        {
            //--Ignore:  All characters past this point are dropped, and the cursor is moved to the
            //  beginning of the next line.
            if(mEdgeHandler == EDGE_IGNORE)
            {
                NextLine();
                return;
            }
            //--Dumb Wrapping:  Go to the next line and continue copying.
            else if(mEdgeHandler == EDGE_DUMBWRAP)
            {
                NextLine();
            }
            //--Smart Wrapping:  Space all characters in the console, storing them going backwards,
            //  then append them to the next line.  Stop upon hitting a space.  This is what many
            //  programs that handle text editing do.
            else if(mEdgeHandler == EDGE_SMARTWRAP)
            {
                //--Buffer to store the line.  Only needs to be as large as the line is.
                char tBuffer[80];

                //--Count backwards to the last space.
                int tStartOfLastWord = 0;
                for(int p = CONSOLE_SIZE_X - 1; p >= 0; p --)
                {
                    if(mStringData[mCursorY][p] == ' ')
                    {
                        tStartOfLastWord = p + 1;
                        break;
                    }
                }

                //--Copy into the buffer.
                for(int p = tStartOfLastWord; p < CONSOLE_SIZE_X; p ++)
                {
                    tBuffer[p - tStartOfLastWord] = mStringData[mCursorY][p];
                    tBuffer[p - tStartOfLastWord + 1] = '\0';
                    mStringData[mCursorY][p] = ' ';
                }

                //--Print the letters to the new line.  Continue afterwards as normal.
                NextLine();
                AppendAtCursor(tBuffer);
            }
        }
    }
}
void VirtualConsole::NextLine()
{
    //--Moves the cursor to the start of the next line.  Auto-handles the bottom case.
    mCursorX = 0;
    mCursorY ++;
    if(mCursorY >= CONSOLE_SIZE_Y) mCursorY = CONSOLE_SIZE_Y - 1;
}
void VirtualConsole::AddLine(const char *pString)
{
    //--Adds the requested string and moves the cursor to the next line.  A macro, really.
    AppendAtCursor(pString);
    NextLine();
}
void VirtualConsole::BeginInput()
{
    for(int i = 0; i < CONSOLE_BUFFER; i ++) mInputBuffer[i] = '\0';
    mIsCatchingKeyboard = true;
    ControlManager::Fetch()->ClearInputBuffer();
    mUnderscoreTimer = 0;
}
void VirtualConsole::EndInput()
{
    mIsCatchingKeyboard = false;
    ControlManager::Fetch()->ClearInputBuffer();
}

//========================================= Core Methods ==========================================
void VirtualConsole::SetLine(int pY, const char *pString)
{
    //--Sets the line as requested.  Affects the cursor.
    SetCursor(0, pY);
    AppendAtCursor(pString);
    NextLine();
}
void VirtualConsole::SetLine(const char *pString)
{
    //--Sets the line as requested from wherever the cursor currently is.
    SetCursor(0, mCursorY);
    AppendAtCursor(pString);
    NextLine();
}
void VirtualConsole::Clear()
{
    //--Clears the VirtualConsole's letters, all to space (not NULL).
    for(int x = 0; x < CONSOLE_SIZE_X; x ++)
    {
        for(int y = 0; y < CONSOLE_SIZE_Y; y ++)
        {
            mStringData[y][x] = ' ';
        }
    }
    for(int y = 0; y < CONSOLE_SIZE_Y; y ++)
    {
        glDeleteLists(mGLLists[y], 1);
        mGLLists[y] = 0;
    }

    //--Reset the cursor.
    mCursorX = 0;
    mCursorY = 0;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void VirtualConsole::Update()
{
    //--If in decision mode, we both accept control requests and also render a '>' to represent the
    //  cursor.  Decision mode can be cancelled during this call.
    if(mIsDecisionMode)
    {
        //--Pass the update.
        ReceiveDecisionControl();
        if(!mIsDecisionMode) return;

        //--For each decision, remove the '>'.
        for(int i = 0; i < mDecisionCursorMax; i ++)
        {
            SetCharacter(0, mDecisionPacks[i].mLineToDisplayOn, ' ');
        }

        //--For the currently selected decision, place a '>'.
        SetCharacter(0, mDecisionPacks[mDecisionCursor].mLineToDisplayOn, '>');
    }

    //--If catching keyboard input, append the key to the end of the buffer (if applicable) or perform
    //  a special action (for things like Enter or Escape)
    ControlManager *rControlManager = ControlManager::Fetch();
    if(mIsCatchingKeyboard)
    {
        //--Timer.
        mUnderscoreTimer ++;

        //--Get the key.
        char tInKey = rControlManager->PopKeyboardInputBuffer();
        while(tInKey != '\0')
        {
            //--Standard keycodes.
            if(tInKey > 0)
            {
                char tBuffer[2];
                tBuffer[0] = tInKey;
                tBuffer[1] = '\0';
                strcat(mInputBuffer, tBuffer);
            }
            //--Backspace, remove the last letter.
            else if(tInKey == SPECIALCODE_BACKSPACE)
            {
                int tLen = strlen(mInputBuffer);
                if(tLen > 0) mInputBuffer[tLen-1] = '\0';
            }
            //--Enter.  Clears off the input buffer, and sends it to the console commands list.
            else if(tInKey == SPECIALCODE_RETURN)
            {
                OptionsManager::Fetch()->ExecuteCommand(mInputBuffer);
                EndInput();
            }
            //--Cancel.  Clears off the input buffer, doesn't execute command list.
            else if(tInKey == SPECIALCODE_CANCEL)
            {
                EndInput();
            }

            //--Next key.
            tInKey = rControlManager->PopKeyboardInputBuffer();
        }
    }
    else
    {
        //--Get the key.
        char tInKey = rControlManager->PopKeyboardInputBuffer();
        while(tInKey != '\0')
        {
            //--Enter.  Begins input mode, and scraps the rest of the buffer.
            if(tInKey == SPECIALCODE_RETURN)
            {
                BeginInput();
                rControlManager->ClearInputBuffer();
            }

            //--Next key.
            tInKey = rControlManager->PopKeyboardInputBuffer();
        }
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void VirtualConsole::Render()
{
    //--Renders all the data currently in mStringData to the window.
    DebugManager::PushPrint(false, "[Virtual Console] Begin Render\n");

    //--Resolve the font.  Don't render if there's no font, that'd be silly.
    SugarFont *rUseFont = DataLibrary::Fetch()->GetFont("Virtual Console Main");
    if(!rUseFont)
    {
        DebugManager::PopPrint("[Virtual Console] Ended render, no font resolved\n");
        return;
    }

    //--One-call pre-bind.
    rUseFont->Bind();

    //--Setup.
    DisplayManager::StdModelPush();
    glColor3f(1.0f, 1.0f, 1.0f);

    //--Translate a fixed offset if there is one.
    glTranslatef(mOffsetX, mOffsetY, 0.0f);
    glTranslatef(0.0f, 5.0f, 0.0f);

    //--Constants.
    const float ctScale = 0.18f;

    //--Render all letters at fixed positions.  Ignore spaces.  End a pulse on NULLs.
    for(int y = 0; y < CONSOLE_SIZE_Y; y ++)
    {
        //--Does a list exists?  If not, generate a new one.
        if(!mGLLists[y])
        {
            //--Build the list.
            mGLLists[y] = glGenLists(1);
            glNewList(mGLLists[y], GL_COMPILE_AND_EXECUTE);

            //--Position.
            glTranslatef(0.0f, y * mCharH, 0.0f);

            //--Scale down the console, and flip the Y coordinate.
            glScalef(ctScale, ctScale * -1.0f, ctScale);

            //--Render
            rUseFont->DrawText(mStringData[y]);

            //--Clean up.
            glScalef(1.0f / ctScale, -1.0f / ctScale, 1.0f / ctScale);
            glTranslatef(0.0f, y * mCharH * -1.0f, 0.0f);
            glEndList();
        }
        //--Use the existing list.
        else
        {
            glCallList(mGLLists[y]);
        }
    }

    //--Render the current input on the bottom.
    if(mIsCatchingKeyboard)
    {
        //--Position.
        glTranslatef(0.0f, (CONSOLE_SIZE_Y-1) * mCharH, 0.0f);

        //--Scale down the console, and flip the Y coordinate.
        glScalef(ctScale, ctScale * -1.0f, ctScale);

        //--Render an underscore at the last character.
        char tBuffer[256];
        strcpy(tBuffer, mInputBuffer);
        if(mUnderscoreTimer % 10 < 5)
        {
            strcat(tBuffer, "_");
        }

        //--Render
        rUseFont->DrawText(tBuffer);

        //--Clean up.
        glScalef(1.0f / ctScale, -1.0f / ctScale, 1.0f / ctScale);
        glTranslatef(0.0f, (CONSOLE_SIZE_Y-1) * mCharH * -1.0f, 0.0f);
    }

    //--Clean.
    DisplayManager::StdModelPop();
    DebugManager::PopPrint("[Virtual Console] Complete Render\n");
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void VirtualConsole::HookToLuaState(lua_State *pLuaState)
{
    //--[Standard Functions]
    /* VC_SetChar(iX, iY, sCharacter[])
       Sets the character at the specified position in the virtual console.  Does not affect the
       cursor.  sCharacter is a string, not a literal character.  Only the first letter is used.*/
    lua_register(pLuaState, "VC_SetChar", &Hook_VC_SetChar);

    /* VC_SetLine(sString[])
       VC_SetLine(iY, sString[])
       Sets the given line to be sString, affecting the cursor.  Letters on the line past the end
       of the string will be invisible.
       In the overload that does not provide the Y, the current cursor line is set. */
    lua_register(pLuaState, "VC_SetLine", &Hook_VC_SetLine);

    /* VC_Clear()
       Clears the VirtualConsole to all spaces. */
    lua_register(pLuaState, "VC_Clear", &Hook_VC_Clear);

    //--[Decision Controllers]
    /* VC_BeginDecision()
       Begins decision mode.  Please indent when using this. */
    lua_register(pLuaState, "VC_BeginDecision", &Hook_VC_BeginDecision);

    /* VC_AppendDecision(sString[], sExecScript[], fFiringCode)
       Adds a new decision, showing the provided line in the console, and executing the provided
       scripts if the decision is selected, with the provided firing code. */
    lua_register(pLuaState, "VC_AppendDecision", &Hook_VC_AppendDecision);

    /* VC_FinalizeDecisions()
       Prints the decisions to the console, and waits for the Player to select one. */
    lua_register(pLuaState, "VC_FinalizeDecisions", &Hook_VC_FinalizeDecisions);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
//--[Worker Functions]
VirtualConsole *AutoFetchConsole()
{
    return NULL;
}

//--[Standard Functions]
int Hook_VC_SetChar(lua_State *L)
{
    //VC_SetChar(iX, iY, sCharacter[])
    if(lua_gettop(L) != 3) return LuaArgError("VC_SetChar");

    //--Get the VC Object.
    VirtualConsole *rVirtualConsole = AutoFetchConsole();
    if(!rVirtualConsole) return LuaTypeError("VC_SetChar");

    //--Set it.
    const char *rBuffer = lua_tostring(L, 3);
    rVirtualConsole->SetCharacter(lua_tointeger(L, 1), lua_tointeger(L, 2), rBuffer[0]);

    //--Done.
    return 0;
}
int Hook_VC_SetLine(lua_State *L)
{
    //VC_SetLine(sString[])
    //VC_SetLine(iY, sString[])
    uint32_t tArgs = lua_gettop(L);
    if(tArgs != 1 && tArgs != 2) return LuaArgError("VC_SetLine");

    //--Get the VC Object.
    VirtualConsole *rVirtualConsole = AutoFetchConsole();
    if(!rVirtualConsole) return LuaTypeError("VC_SetLine");

    //--Set it.
    if(tArgs == 1)
    {
        rVirtualConsole->SetLine(lua_tostring(L, 1));
    }
    else if(tArgs == 2)
    {
        rVirtualConsole->SetLine(lua_tointeger(L, 1), lua_tostring(L, 2));
    }

    //--Done.
    return 0;
}
int Hook_VC_Clear(lua_State *L)
{
    //VC_Clear()

    //--Get the VC Object.
    VirtualConsole *rVirtualConsole = AutoFetchConsole();
    if(!rVirtualConsole) return LuaTypeError("VC_Clear");

    //--Clear it.
    rVirtualConsole->Clear();

    //--Done.
    return 0;
}

//--[Decision Controllers]
int Hook_VC_BeginDecision(lua_State *L)
{
    //VC_BeginDecision()

    //--Get the VC Object.
    VirtualConsole *rVirtualConsole = AutoFetchConsole();
    if(!rVirtualConsole) return LuaTypeError("VC_BeginDecision");

    //--Call.
    rVirtualConsole->BeginDecisionMode();

    //--Done.
    return 0;
}
int Hook_VC_AppendDecision(lua_State *L)
{
    //VC_AppendDecision(sString[], sExecScript[], fFiringCode)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs != 3) return LuaArgError("VC_AppendDecision");

    //--Get the VC Object.
    VirtualConsole *rVirtualConsole = AutoFetchConsole();
    if(!rVirtualConsole) return LuaTypeError("VC_BeginDecision");

    //--Call.
    rVirtualConsole->AddDecision(lua_tostring(L, 1), lua_tostring(L, 2), lua_tonumber(L, 3));

    //--Done.
    return 0;
}
int Hook_VC_FinalizeDecisions(lua_State *L)
{
    //VC_FinalizeDecisions()

    //--Get the VC Object.
    VirtualConsole *rVirtualConsole = AutoFetchConsole();
    if(!rVirtualConsole) return LuaTypeError("VC_BeginDecision");

    //--Call.
    rVirtualConsole->FinishAddingDecisions();

    //--Done.
    return 0;
}
