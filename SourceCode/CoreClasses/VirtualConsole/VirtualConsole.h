//--[VirtualConsole]
//--A console emulated in OpenGL, making it that much more potent.  Allows pretty much all the
//  capability that libraries like nCurses were meant to have, without all the stupid bullshit
//  caused by the morons that coded Windows.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--[Local Definitions]
#define EDGE_IGNORE 0
#define EDGE_DUMBWRAP 1
#define EDGE_SMARTWRAP 2

#define CONSOLE_SIZE_X 80
#define CONSOLE_SIZE_Y 15

#define CONSOLE_BUFFER 80

//--[Local Structures]
typedef struct
{
    int mLineToDisplayOn;
    char *mDisplayString;
    char *mExecScriptIfSelected;
    float mFiringCodeIfSelected;
}VirtualConsoleDecisionPack;

class VirtualConsole
{
    private:
    //--System
    //--Cursor
    int mCursorX, mCursorY;

    //--Storage.
    uint32_t mGLLists[CONSOLE_SIZE_Y];
    char mStringData[CONSOLE_SIZE_Y][CONSOLE_SIZE_X];

    //--Flags
    uint8_t mEdgeHandler;

    //--Rendering
    float mOffsetX, mOffsetY;
    float mCharW, mCharH;

    //--Decisions
    bool mIsDecisionMode;
    int mDecisionCursor;
    int mDecisionCursorMax;
    VirtualConsoleDecisionPack *mDecisionPacks;

    //--Input Catching
    int mUnderscoreTimer;
    bool mIsCatchingKeyboard;
    char mInputBuffer[CONSOLE_BUFFER];

    protected:

    public:
    //--System
    VirtualConsole();
    ~VirtualConsole();
    static VirtualConsole *xStaticConsole;

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void SetCursor(int pX, int pY);
    void SetCharacter(int pX, int pY, char pLetter);
    void AppendAtCursor(const char *pString);
    void NextLine();
    void AddLine(const char *pString);
    void BeginInput();
    void EndInput();

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    void SetLine(int pY, const char *pString);
    void SetLine(const char *pString);
    void Clear();

    //--Decisions Sub-routines
    void BeginDecisionMode();
    void AddDecision(const char *pString, const char *pExecScript, float pFiringCode);
    void FinishAddingDecisions();
    void EndDecisionMode();
    void ClearDecisions();
    void ReceiveDecisionControl();

    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_VC_SetChar(lua_State *L);
int Hook_VC_SetLine(lua_State *L);
int Hook_VC_Clear(lua_State *L);

//--Decision Controllers
int Hook_VC_BeginDecision(lua_State *L);
int Hook_VC_AppendDecision(lua_State *L);
int Hook_VC_FinalizeDecisions(lua_State *L);

