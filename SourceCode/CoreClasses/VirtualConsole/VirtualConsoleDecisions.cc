//--Base
#include "VirtualConsole.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "ControlManager.h"
#include "LuaManager.h"

void VirtualConsole::BeginDecisionMode()
{
    //--Begins decision mode, clearing off all the existing data and awaiting decision to be plugged in.
    ClearDecisions();

    //--Flag.
    mIsDecisionMode = true;
}
void VirtualConsole::AddDecision(const char *pString, const char *pExecScript, float pFiringCode)
{
    //--Appends a new decision onto the list.  Realloc is used, but there ought to be no dangling
    //  pointer issues since nothing is allowed to reference these directly.
    if(!pString || !pExecScript || !mIsDecisionMode) return;

    //--New space.
    int i = mDecisionCursorMax;
    mDecisionCursorMax ++;

    //--Alloc.
    mDecisionPacks = (VirtualConsoleDecisionPack *)realloc(mDecisionPacks, sizeof(VirtualConsoleDecisionPack) * mDecisionCursorMax);

    //--Set the data in the new pack.
    mDecisionPacks[i].mDisplayString = NULL;
    ResetString(mDecisionPacks[i].mDisplayString, pString);
    mDecisionPacks[i].mExecScriptIfSelected = NULL;
    ResetString(mDecisionPacks[i].mExecScriptIfSelected, pExecScript);
    mDecisionPacks[i].mFiringCodeIfSelected = pFiringCode;
    mDecisionPacks[i].mLineToDisplayOn = mCursorY;

    //--Move the line cursor.
    NextLine();
}
void VirtualConsole::FinishAddingDecisions()
{
    //--Prints all the decisions to the console, with a space before each one where the selection
    //  cursor will go.  This only needs to be called once, though multiple calls will not cause
    //  problems if logic requires that.
    if(!mIsDecisionMode) return;

    //--For each decision...
    for(int i = 0; i < mDecisionCursorMax; i ++)
    {
        //--Go to the requested line...
        SetCursor(1, mDecisionPacks[i].mLineToDisplayOn);

        //--Append the line.
        AppendAtCursor(mDecisionPacks[i].mDisplayString);
    }
}
void VirtualConsole::EndDecisionMode()
{
    //--Ends decision mode.  Whatever decision the cursor is currently on gets executed.
    if(!mIsDecisionMode) return;

    //--Store the decision pack temporarily.
    float tFiringCode = mDecisionPacks[mDecisionCursor].mFiringCodeIfSelected;
    char tExecScriptBuffer[512];
    strcpy(tExecScriptBuffer, mDecisionPacks[mDecisionCursor].mExecScriptIfSelected);

    //--Clear old decision mode data BEFORE executing the lua script.  This prevents conflicts with
    //  new decision data from the new script.
    ClearDecisions();

    //--Execute the script.
    LuaManager::Fetch()->ExecuteLuaFile(tExecScriptBuffer, 1, "N", tFiringCode);
}
void VirtualConsole::ClearDecisions()
{
    //--Cleaner subroutine, deallocates data associated with decision mode.
    mIsDecisionMode = false;
    mDecisionCursor = 0;

    for(int i = 0; i < mDecisionCursorMax; i ++)
    {
        free(mDecisionPacks[i].mDisplayString);
        free(mDecisionPacks[i].mExecScriptIfSelected);
    }
    mDecisionCursorMax = 0;
    free(mDecisionPacks);
    mDecisionPacks = NULL;
}
void VirtualConsole::ReceiveDecisionControl()
{
    //--Update function, called during the update tick if in decision mode.  Check for control
    //  presses and update accordingly.
    if(!mIsDecisionMode) return;
    ControlManager *rControlManager = ControlManager::Fetch();

    //--User pressed up.
    if(rControlManager->GetControlState("Up")->mIsFirstPress)
    {
        mDecisionCursor --;
        if(mDecisionCursor < 0) mDecisionCursor = mDecisionCursorMax - 1;
    }
    //--User pressed down.
    else if(rControlManager->GetControlState("Down")->mIsFirstPress)
    {
        mDecisionCursor ++;
        if(mDecisionCursor >= mDecisionCursorMax) mDecisionCursor = 0;
    }
    //--User pressed select.
    else if(rControlManager->GetControlState("Select")->mIsFirstPress)
    {
        EndDecisionMode();
    }
}
