//--Base
#include "SugarPalette.h"

//--Classes
//--CoreClasses
#include "GlDfn.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "DisplayManager.h"

//=========================================== System ==============================================
SugarPalette::SugarPalette()
{
    //--System
    //--Source
    mTotalColors = 0;
    mSrcColors = NULL;

    //--Palettes
    mLastPaletteName = NULL;
    rLastPalette = NULL;
    mPaletteList = new SugarLinkedList(true);
}
SugarPalette::~SugarPalette()
{
    free(mSrcColors);
    free(mLastPaletteName);
    delete mPaletteList;
}
void SugarPalette::DeleteThis(void *pPtr)
{
    delete ((SugarPalette *)pPtr);
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void SugarPalette::SetTotalColors(int pTotal)
{
    //--Clears off the old data.  A new total invalidates everything.
    Clear();

    //--Passing < 1 means we just clear, done.
    if(pTotal < 1) return;
    mTotalColors = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mSrcColors = (StarlightColor *)starmemoryalloc(sizeof(StarlightColor) * mTotalColors);
}
void SugarPalette::SetSourceColor(int pSlot, StarlightColor pColor)
{
    if(pSlot < 0 || pSlot >= mTotalColors) return;
    memcpy(&mSrcColors[pSlot], &pColor, sizeof(StarlightColor));
    //fprintf(stderr, "%2i: %3i %3i %3i\n", pSlot, (int)(mSrcColors[pSlot].r * 255.0f), (int)(mSrcColors[pSlot].g * 255.0f), (int)(mSrcColors[pSlot].b * 255.0f));
}
void SugarPalette::AddPalette(const char *pName)
{
    if(!pName || mTotalColors < 1) return;

    SetMemoryData(__FILE__, __LINE__);
    StarlightColor *nColorList = (StarlightColor *)starmemoryalloc(sizeof(StarlightColor) * mTotalColors);
    mPaletteList->AddElement(pName, nColorList, &FreeThis);

    ResetString(mLastPaletteName, pName);
    rLastPalette = nColorList;
}
void SugarPalette::SetPaletteColor(const char *pName, int pSlot, StarlightColor pColor)
{
    if(!pName || pSlot < 0 || pSlot >= mTotalColors) return;
    StarlightColor *rUsePalette = NULL;
    if(!strcmp(pName, mLastPaletteName))
    {
        rUsePalette = rLastPalette;
    }
    else
    {
        rUsePalette = (StarlightColor *)mPaletteList->GetElementByName(pName);
        if(rUsePalette) ResetString(mLastPaletteName, pName);
    }

    if(!rUsePalette) return;

    memcpy(&rUsePalette[pSlot], &pColor, sizeof(StarlightColor));
    //if(!strcmp(pName, "Palette2"))
    //    fprintf(stderr, "%2i: %3i %3i %3i %3i -> %3i %3i %3i %3i\n", pSlot, (int)(mSrcColors[pSlot].r * 255.0f), (int)(mSrcColors[pSlot].g * 255.0f), (int)(mSrcColors[pSlot].b * 255.0f), (int)(mSrcColors[pSlot].a * 255.0f),
    //                                                                        (int)(pColor.r * 255.0f), (int)(pColor.g * 255.0f), (int)(pColor.b * 255.0f), (int)(pColor.a * 255.0f));
}

//========================================= Core Methods ==========================================
void SugarPalette::Clear()
{
    //--Clears and deallocates all old data.
    mTotalColors = 0;
    free(mSrcColors);
    mSrcColors = NULL;
    mPaletteList->ClearList();
    free(mLastPaletteName);
    mLastPaletteName = NULL;
    rLastPalette = NULL;
}

//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void SugarPalette::ActivatePaletteShader(const char *pPaletteName)
{
    //--Turns on the requested palette, if it exists.  If not, the shader is not activated.
    if(!pPaletteName) return;

    //--Get the Palette.  Fail if not found.
    StarlightColor *rDstArray = (StarlightColor *)mPaletteList->GetElementByName(pPaletteName);
    if(!rDstArray) return;

    //--Activate the shader.
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    rDisplayManager->ActivateProgram("XPaletteSwap");
    GLint tShaderHandle = rDisplayManager->mLastProgramHandle;

    //--Upload how many colors we have.
    GLint tTotalColorsHandle = sglGetUniformLocation(tShaderHandle, "uTotalColors");
    sglUniform1i(tTotalColorsHandle, mTotalColors);

    //--Don't upload more colors than the shader supports!
    int tIterateTo = mTotalColors;
    if(tIterateTo > 128) tIterateTo = 128;

    //--Upload each color in turn.  Both source and dest can be uploaded at the same time.  We use
    //  sprintf for expandability (we could support 3-4 digit names someday...)
    char tName[32];
    for(int i = 0; i < tIterateTo; i ++)
    {
        sprintf(tName, "uSrcColor[%i]", i);
        GLint tSrcColorHandle = sglGetUniformLocation(tShaderHandle, tName);
        sglUniform4f(tSrcColorHandle, mSrcColors[i].r, mSrcColors[i].g, mSrcColors[i].b, 1.0f);
        //fprintf(stderr, "%3f %3f %3f", mSrcColors[i].r * 255.0f, mSrcColors[i].g * 255.0f, mSrcColors[i].b * 255.0f);

        sprintf(tName, "uDstColor[%i]", i);
        GLint tDstColorHandle = sglGetUniformLocation(tShaderHandle, tName);
        sglUniform4f(tDstColorHandle, rDstArray[i].r, rDstArray[i].g, rDstArray[i].b, 1.0f);
        //fprintf(stderr, " - > %3f %3f %3f\n", rDstArray[i].r * 255.0f, rDstArray[i].g * 255.0f, rDstArray[i].b * 255.0f);
    }
}
void SugarPalette::DeactivatePaletteShader()
{
    //--Turns off the palette swapping shader.  Remember to pair this with Activate!
    //  This call is static since which palette is in use doesn't matter.
    DisplayManager::Fetch()->ActivateProgram(NULL);
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
