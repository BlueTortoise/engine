//--[SugarPalette]
//--Represents a list of colors in memory and how they remap to another set of colors.  This is
//  done through shaders on the graphics card.
//--SLF Files have a lump for SugarPalettes, the aptly named "PALETTEXXX" lump.

#pragma once

#include "Definitions.h"
#include "Structures.h"

class SugarPalette
{
    private:
    //--System
    //--Source
    int mTotalColors;
    StarlightColor *mSrcColors;

    //--Palettes
    char *mLastPaletteName;
    StarlightColor *rLastPalette;
    SugarLinkedList *mPaletteList;

    protected:

    public:
    //--System
    SugarPalette();
    ~SugarPalette();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void SetTotalColors(int pTotal);
    void SetSourceColor(int pSlot, StarlightColor pColor);
    void AddPalette(const char *pName);
    void SetPaletteColor(const char *pName, int pSlot, StarlightColor pColor);

    //--Core Methods
    void Clear();

    //--Update
    //--File I/O
    //--Drawing
    void ActivatePaletteShader(const char *pPaletteName);
    static void DeactivatePaletteShader();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

//--Hooking Functions

