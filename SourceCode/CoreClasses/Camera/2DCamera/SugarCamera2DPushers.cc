//--Base
#include "SugarCamera2D.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers

void SugarCamera2D::ClearPushers()
{
    mPusherList->ClearList();
}
void SugarCamera2D::AddPusher(const char *pName, float pIdealLft, float pIdealTop, float pLft, float pTop, float pWid, float pHei)
{
    //--Appends a new Pusher to the camera's list.
    if(!pName) return;

    SetMemoryData(__FILE__, __LINE__);
    CameraPushZone *nZone = (CameraPushZone *)starmemoryalloc(sizeof(CameraPushZone));
    nZone->mIdealLft = pIdealLft;
    nZone->mIdealTop = pIdealTop;
    nZone->mHitbox.mLft = pLft;
    nZone->mHitbox.mTop = pTop;
    nZone->mHitbox.mRgt = pLft + pWid;
    nZone->mHitbox.mBot = pTop + pHei;
    mPusherList->AddElement(pName, nZone, &FreeThis);
}
void SugarCamera2D::CheckPushersAgainst(ThreeDimensionReal pDimensions)
{
    //--Checks the Pushers list against the provided dimensions.  If any are colliding, sets that
    //  Pusher as the active one.
    CameraPushZone *rZone = (CameraPushZone *)mPusherList->PushIterator();
    while(rZone)
    {
        if(IsCollision3DReal(pDimensions, rZone->mHitbox))
        {
            rCurrentPusher = rZone;
            mPusherList->PopIterator();
            return;
        }
        rZone = (CameraPushZone *)mPusherList->AutoIterate();
    }
}
