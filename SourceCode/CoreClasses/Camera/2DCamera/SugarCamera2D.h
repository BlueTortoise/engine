//--[SugarCamera2D]
//--A SugarCamera specifically designed for 2D gameplay. Moves orthographically and can respect
//  boundaries in 2D. Has no idea what 3D is.

#pragma once

#include "Definitions.h"
#include "Structures.h"
#include "SugarCamera.h"

//--Local Definitions
#define DO_NOT_ROTATE -500.0f

//--Local Structures
typedef struct
{
    bool mIsActive;
    float mLft, mTop, mRgt, mBot;
}CAM_ZONE;
typedef struct
{
    float mIdealLft, mIdealTop;
    ThreeDimensionReal mHitbox;
}CameraPushZone;

class SugarCamera2D : public SugarCamera
{
    private:
    //--System
    //--Zoom factor.
    float m2DScale;
    float mSmallestScale;
    float mLargestScale;

    //--Spatial positioning
    TwoDimensionReal mBoundaries;
    int mMaxMovesPerTick;

    //--Non-polygonal collision zones.
    bool mUsePositiveCamera;
    uint32_t mTotalCamZones;
    CAM_ZONE *mCamZoneList;

    //--Polygonal collision zones.
    bool mIs2DPolygonMode;
    SugarLinkedList *mPolygonList;

    //--Pushers.
    CameraPushZone *rCurrentPusher;
    SugarLinkedList *mPusherList;

    //--Locking and Clamping
    bool mLockCameraPipeline;
    bool mAllowMouseControl;
    bool mOldCameraFlag, mOldPolygonFlag;
    float mOldMouseX, mOldMouseY;
    float mCameraStackX, mCameraStackY;

    protected:
    //--Spatial positioning. This is the only variable used by the 3D camera.
    ThreeDimensionReal mCoordinates;

    public:
    //--System
    SugarCamera2D();
    virtual ~SugarCamera2D();

    //--Public Variables
    //--Property Queries
    virtual bool Is2DCamera();
    ThreeDimensionReal GetDimensions();
    TwoDimensionReal GetBoundaries();
    bool IsObjectInFrame(ThreeDimensionReal pDimensions);
    bool IsObjectInFrame(float pLft, float pTop, float pRgt, float pBot);

    //--Manipulators
    void SetScale(float pScale);
    void SetMaxMovesPerTick(int pMoves);
    void SetLockingState(bool pFlag);
    void SetMouseControlFlag(bool pFlag);

    //--Core Methods
    void OverrideX(float pX);
    void OverrideY(float pY);
    void OverridePosition(float pX, float pY);
    void Clear();
    void ReloadAndReset();
    virtual void PositionInWorld();
    virtual void UnpositionInWorld();

    //--2D Methods (Camera2DHandling.cc)
    float Get2DScale();
    void SetDimensions(int pWidth, int pHeight);
    void SetBoundaries(TwoDimensionReal pBoundaries);
    void SetBoundaries(int pLft, int pTop, int pRgt, int pBot);
    void Move(int pXDist, int pYDist);
    void CenterOnPlayer();
    void CenterOnBox(int pLft, int pTop, int pRgt, int pBot);
    void CenterOnBox(int pLft, int pTop, int pRgt, int pBot, bool pLockScale, bool pIgnoreX, bool pIgnoreY);
    void CenterOnPosition(int pXTarget, int pYTarget);
    void CenterOnMouse();
    void LockToBoundaries();
    void Translate2D();
    void Untranslate2D();
    void ForceTo(int pLft, int pTop);

    //--2D Zone Handling
    void SetZoneMode(bool pFlag);
    void AllocZones(uint32_t pZonesTotal);
    void ReallocZones(int pZonesTotal);
    void SetZoneActivity(uint32_t pSlot, bool pFlag);
    void SetZone(uint32_t pSlot, int pLft, int pTop, int pRgt, int pBot);
    bool IsPositionValid(ThreeDimensionReal pPosition, int pXMove, int pYMove);

    //--Polygon Mode
    void SetPolygonMode(bool pFlag);
    void AddPolygon(float *pArray);
    bool IsPolyPositionValid(ThreeDimensionReal pPosition);

    //--Pushers
    void ClearPushers();
    void AddPusher(const char *pName, float pIdealLft, float pIdealTop, float pLft, float pTop, float pWid, float pHei);
    void CheckPushersAgainst(ThreeDimensionReal pDimensions);

    //--Update
    void Update();

    //--File I/O
    //--Drawing
    virtual void Render2D();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);

    //--Command Hooking
    static void RegisterCommands();
};

//--[Lua Functions]
int Hook_Camera_GetProperty(lua_State *L);
int Hook_Camera_SetProperty(lua_State *L);
int Hook_Camera_CallFunc(lua_State *L);
int Hook_Camera_SetLockingState(lua_State *L);

//--[Commands]
void Com_Cam_MouseMode(const char *);
