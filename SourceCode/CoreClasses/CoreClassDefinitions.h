class SugarAnimation;
class SugarAutoBuffer;
class SugarBitmap;
class SugarButton;
class SugarCamera;
class SugarCamera2D;
class DataList;
struct StarlightColor;
class SugarEventQueue;
class SugarFileSystem;
class VirtualFile;
class SugarFont;
class SugarLinkedList;
class SugarMenu;
class LoadInterrupt;
class SugarPalette;
class SugarPuppet;
class StarlightPerlin;
class StarlightString;
class VirtualFile;
class StarlightSocket;
class UString;
class VirtualConsole;
class SugarTag;

class RootEvent;

struct SugarBitmapPrecache;
