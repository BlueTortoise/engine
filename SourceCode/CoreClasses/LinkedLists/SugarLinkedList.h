//--[SugarLinkedList]
//--General-purpose linked list, built for the Sugar^3 engine.
//--Features:
//  Iterator handling
//  Automatic garbage handling through deletion pointers
//  Random-pointer allows for easier pruning
//  Does not rely on the STL

//--Notes:
//--The Iterator stack is not 'stable' with the Random pointer. Ideally, do not delete components
//  if there is an iterator on the stack. If you need to prune something, do it at the end of a
//  'tick' when nothing is accessing it. In addition, do not add components while the list is
//  iterating as it may or may not respect them depending on where they were added.
//--This will eventually be handled by adding a pending addition and deletion list.

#pragma once

//--Definitions.
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

//--Deletion Function Pointer is a standardized pointer type used to delete objects or free them.
//  It should be declared elsewhere in the program surrounded by these #ifdef braces.
#ifndef _DeletionFunctionPtr_
#define _DeletionFunctionPtr_
typedef void(*DeletionFunctionPtr)(void *);
#endif

#ifndef _SortFunctionPtr_
#define _SortFunctionPtr_
typedef int(*SortFnPtr)(const void *, const void *);
#endif

typedef int(*StringCompareFuncPtr)(const char *, const char *);

//--The SLL is constructed of these entries, linked to one another. The entry itself is garbage
//  collected automatically, but can be retrieved if really necessary. 99% of RLL activity does
//  not require the programmer to access these entries.
typedef struct SugarLinkedListEntry SugarLinkedListEntry;
struct SugarLinkedListEntry
{
    char *mName;
    SugarLinkedListEntry *rNext;
    SugarLinkedListEntry *rPrev;
    void *rData;
    void (*rDeletionFunc)(void *);
};

//--A component of the Iterator stack.
typedef struct SLLIteratorEntry SLLIteratorEntry;
struct SLLIteratorEntry
{
    SugarLinkedListEntry *rCurrentEntry;
    SLLIteratorEntry *rNextStackObject;
};

class SugarLinkedList
{
    private:
    //--List Handling
    SugarLinkedListEntry *mListHead;
    SugarLinkedListEntry *mListTail;
    SugarLinkedListEntry *mListCurrent;

    //--List Properties
    int mListSize;
    bool mHandlesDeallocation;
    StringCompareFuncPtr rStringCompareFuncPtr;

    //--Iterator Stack
    int mStackSize;
    SLLIteratorEntry *mIteratorStack;

    //--Debug
    bool mDebugFlag;

    public:
    //--System
    SugarLinkedList(bool pHandleDeallocate);
    ~SugarLinkedList();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    int GetListSize();
    void *GetHead();
    void *GetTail();
    void *GetEntryByRandomRoll();

    //--Manipulators
    void SetDebugFlag(bool pFlag);
    void SetDeallocation(bool pFlag);
    void SetCaseSensitivity(bool pFlag);

    //--Sorting
    static int CompareEntries(const void *pEntryA, const void *pEntryB);
    static int CompareEntriesRev(const void *pEntryA, const void *pEntryB);
    void SortList(bool pReverseFlag);
    void SortListUsing(SortFnPtr pSortFunction);

    //--Core Methods
    void *DeleteHead();
    void *DeleteTail();
    void ClearList();
    void *DeleteEntry(SugarLinkedListEntry *pEntry);
    void CloneToList(SugarLinkedList *pList);

    //--Private Core Methods
    void *DeleteWholeList();
    void DecrementList();

    public:

    //--Additions
    static SugarLinkedListEntry *AllocateEntry(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void *AddElement(const char *pName, void *pElement);
    void *AddElement(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void *AddElementAsHead(const char *pName, void *pElement);
    void *AddElementAsHead(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void *AddElementAsTail(const char *pName, void *pElement);
    void *AddElementAsTail(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr);
    void *AddElementInSlot(const char *pName, void *pElement, int pSlot);
    void *AddElementInSlot(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr, int pSlot);

    //--Iterator Handling
    void *PushIterator();
    void *Iterate();
    void *AutoIterate();
    char *GetIteratorName();
    void PopIterator();
    void ClearStack();

    //--Random Pointer Handling
    void SetRandomPointerToHead();
    void SetRandomPointerToTail();
    bool IncrementRandomPointer();
    bool DecrementRandomPointer();
    void *GetRandomPointerEntry();
    char *GetRandomPointerName();
    void *RemoveRandomPointerEntry();
    void *LiberateRandomPointerEntry();
    void *SetToHeadAndReturn();
    void *SetToTailAndReturn();
    void *IncrementAndGetRandomPointerEntry();
    void *DecrementAndGetRandomPointerEntry();
    bool SetRandomPointerToThis(void *pPtr);
    SugarLinkedListEntry *GetRandomPointerWholeEntry();

    //--Removal
    void *RemoveElementS(const char *pSearch);
    void *RemoveElementI(int slot);
    bool RemoveElementP(void *pPtr);

    //--Searches (SugarLinkedListSearches.cc)
    const char *GetNameOfElementBySlot(int pSlot);
    int GetSlotOfElementByName(const char *pName);
    int GetSlotOfElementByPtr(void *pPtr);
    void *GetElementByName(const char *pSearch);
    void *GetElementByPartialName(const char *pSearch);
    void *GetElementBySlot(int slot);
    bool IsElementOnList(void *pPtr);

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

