//--Base
#include "SugarLinkedList.h"
#include <string.h>

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers

const char *SugarLinkedList::GetNameOfElementBySlot(int pSlot)
{
    //--Returns the name of the element at the given slot. That is, the name it was put into the list
    //  with, which may not be the same as the one the object may hold internally.
    //--Returns NULL on error.
    if(pSlot < 0 || pSlot >= mListSize) return NULL;
    if(pSlot == 0) return mListHead->mName;
    if(pSlot == mListSize-1) return mListTail->mName;

    //--General case
    SugarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < pSlot; i ++)
    {
        rEntry = rEntry->rNext;
    }
    return rEntry->mName;
}
int SugarLinkedList::GetSlotOfElementByName(const char *pName)
{
    //--Returns the slot that the named element is in. If multiple elements have the same name,
    //  this returns the first one found.
    //--Returns -1 on error (0 is the first slot).
    if(!pName) return -1;

    SugarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < mListSize; i ++)
    {
        if(!rStringCompareFuncPtr(rEntry->mName, pName))
        {
            return i;
        }
        rEntry = rEntry->rNext;
    }

    //--Indicate failure (element not in list) with -1.
    return -1;
}
int SugarLinkedList::GetSlotOfElementByPtr(void *pPtr)
{
    //--Returns the slot that holds the given data pointer. If multiple instances exist, this will
    //  return the first one found.
    //--Returns -1 if the entry was not on the list.
    if(!pPtr) return -1;

    SugarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < mListSize; i ++)
    {
        if(rEntry->rData == pPtr)
        {
            return i;
        }
        rEntry = rEntry->rNext;
    }

    //--Indicate failure (element not in list) with -1.
    return -1;
}
void *SugarLinkedList::GetElementByName(const char *pSearch)
{
    //--The simplest search, returns the first element found which matches the name.
    //--Returns NULL on failure.
    if(!pSearch) return NULL;

    SugarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < mListSize; i ++)
    {
        if(!rStringCompareFuncPtr(rEntry->mName, pSearch))
        {
            return rEntry->rData;
        }
        rEntry = rEntry->rNext;
    }
    return NULL;
}
void *SugarLinkedList::GetElementByPartialName(const char *pSearch)
{
    //--Returns the first element which matches the name partially.
    //--Returns NULL on error.
    if(!pSearch) return NULL;

    //--Valid length. Cannot be a zero-length string.
    uint32_t tLen = strlen(pSearch);
    if(tLen < 1) return NULL;

    //--Scan the list.
    SugarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < mListSize; i ++)
    {
        if(!strncmp(rEntry->mName, pSearch, tLen))
        {
            return rEntry->rData;
        }
        rEntry = rEntry->rNext;
    }
    return NULL;
}
void *SugarLinkedList::GetElementBySlot(int slot)
{
    //--Returns the element that is in the matching slot.
    //--Returns NULL if out of range.
    if(slot < 0 || slot >= mListSize) return NULL;

    SugarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < slot; i ++)
    {
        rEntry = rEntry->rNext;
    }
    return rEntry->rData;
}
bool SugarLinkedList::IsElementOnList(void *pPtr)
{
    //--Returns a boolean indicating if the given pointer is on the list or not. Always returns false
    //  if you pass it a NULL, you idiot.
    if(!pPtr) return false;

    SugarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < mListSize; i ++)
    {
        if(rEntry->rData == pPtr)
        {
            return true;
        }
        rEntry = rEntry->rNext;
    }
    return false;
}
