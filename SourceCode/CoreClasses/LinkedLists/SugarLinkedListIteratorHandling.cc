//--Base
#include "SugarLinkedList.h"

//--Classes
//--Definitions
#include "Definitions.h"

//--Generics
//--GUI
//--Libraries
//--Managers

void *SugarLinkedList::PushIterator()
{
    //--Creates a new iterator for the list, and puts it on the top of the stack. Returns the DataPtr
    //  from the first entry on the list.
    //--Returns NULL if the list is empty.
    if(mListHead == NULL) return NULL;

    SetMemoryData(__FILE__, __LINE__);
    SLLIteratorEntry *nEntry = (SLLIteratorEntry *)starmemoryalloc(sizeof(SLLIteratorEntry));
    nEntry->rCurrentEntry = mListHead;
    nEntry->rNextStackObject = mIteratorStack;
    mIteratorStack = nEntry;

    mStackSize ++;
    return mListHead->rData;
}
void *SugarLinkedList::Iterate()
{
    //--First Iterator on the stack increments itself.  Returns the rData pointer to the entry
    //  it stops at.
    if(!mIteratorStack || !mIteratorStack->rCurrentEntry) return NULL;

    mIteratorStack->rCurrentEntry = mIteratorStack->rCurrentEntry->rNext;
    if(!mIteratorStack->rCurrentEntry) return NULL;

    return mIteratorStack->rCurrentEntry->rData;
}
void *SugarLinkedList::AutoIterate()
{
    //--Same as above, except if the rCurrentEntry->rNext is NULL, automatically pops the iterator,
    //  saving a function call.
    if(!mIteratorStack || !mIteratorStack->rCurrentEntry) return NULL;

    mIteratorStack->rCurrentEntry = mIteratorStack->rCurrentEntry->rNext;
    if(!mIteratorStack->rCurrentEntry)
    {
        PopIterator();
        return NULL;
    }

    return mIteratorStack->rCurrentEntry->rData;
}
char *SugarLinkedList::GetIteratorName()
{
    //--Returns the name of the Entry on the top of the Iterator stack.
    if(!mIteratorStack || !mIteratorStack->rCurrentEntry) return NULL;

    return mIteratorStack->rCurrentEntry->mName;
}
void SugarLinkedList::PopIterator()
{
    //--Remove the current Iterator from the front of the stack.
    if(!mIteratorStack) return;

    SLLIteratorEntry *rOldHead = mIteratorStack;
    mIteratorStack = rOldHead->rNextStackObject;
    free(rOldHead);

    mStackSize --;
}
void SugarLinkedList::ClearStack()
{
    //--Remove all the Iterators, and clear the stack.
    SLLIteratorEntry *rCurEntry = mIteratorStack;
    SLLIteratorEntry *rNextEntry;
    while(rCurEntry)
    {
        rNextEntry = rCurEntry->rNextStackObject;
        free(rCurEntry);
        rCurEntry = rNextEntry;
    }

    mStackSize = 0;
    mIteratorStack = NULL;
}
