//--Base
#include "SugarLinkedList.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers

void SugarLinkedList::SetRandomPointerToHead()
{
    mListCurrent = mListHead;
}
void SugarLinkedList::SetRandomPointerToTail()
{
    mListCurrent = mListTail;
}
bool SugarLinkedList::IncrementRandomPointer()
{
    if(!mListCurrent)
    {
        mListCurrent = mListHead;
    }
    else
    {
        mListCurrent = mListCurrent->rNext;
    }
    return true;
}
bool SugarLinkedList::DecrementRandomPointer()
{
    if(!mListCurrent)
    {
        mListCurrent = mListTail;
    }
    else
    {
        mListCurrent = mListCurrent->rPrev;
    }
    return true;
}
void *SugarLinkedList::GetRandomPointerEntry()
{
    if(!mListCurrent) return NULL;
    return mListCurrent->rData;
}
char *SugarLinkedList::GetRandomPointerName()
{
    if(!mListCurrent) return NULL;
    return mListCurrent->mName;
}
void *SugarLinkedList::RemoveRandomPointerEntry()
{
    //--Removes the Entry currently pointed at by the RandomPointer.  If deletion is turned on, then
    //  the entry is summarily deleted.

    //--Special cases
    if(!mListCurrent) return NULL;
    void *rReturnPtr = NULL;

    //--Object is the head. See below for why this is NULL'd.
    if(mListCurrent == mListHead)
    {
        rReturnPtr = DeleteHead();
        mListCurrent = NULL;
    }
    //--Object is the tail.
    else if(mListCurrent == mListTail)
    {
        rReturnPtr = DeleteTail();
    }
    //--General case.
    else
    {
        //--Cross the pointers for the victim's neighbours.
        SugarLinkedListEntry *rPrevEntry = mListCurrent->rPrev;
        SugarLinkedListEntry *rNextEntry = mListCurrent->rNext;
        rPrevEntry->rNext = rNextEntry;
        rNextEntry->rPrev = rPrevEntry;

        //--Deletion.
        rReturnPtr = DeleteEntry(mListCurrent);
        DecrementList();

        //--Move the RLLPointer to the previous entry. Typically, a while() loop will iterate up
        //  at the end of the loop, so this will cause the next entry to remain unchanged.
        mListCurrent = rPrevEntry;
    }

    return rReturnPtr;
}
void *SugarLinkedList::LiberateRandomPointerEntry()
{
    //--The same as removal, except the entry is never deleted regardless of the deallocation flag.
    //  This is done by simply flipping it off for the duration of one deletion call.
    bool tOldFlag = mHandlesDeallocation;
    mHandlesDeallocation = false;
    void *rEntry = RemoveRandomPointerEntry();
    mHandlesDeallocation = tOldFlag;
    return rEntry;
}
void *SugarLinkedList::SetToHeadAndReturn()
{
    SetRandomPointerToHead();
    return GetRandomPointerEntry();
}
void *SugarLinkedList::SetToTailAndReturn()
{
    SetRandomPointerToTail();
    return GetRandomPointerEntry();
}
void *SugarLinkedList::IncrementAndGetRandomPointerEntry()
{
    IncrementRandomPointer();
    return GetRandomPointerEntry();
}
void *SugarLinkedList::DecrementAndGetRandomPointerEntry()
{
    DecrementRandomPointer();
    return GetRandomPointerEntry();
}
bool SugarLinkedList::SetRandomPointerToThis(void *pPtr)
{
    //--Given a specific data pointer, searches the list for that pointer and sets the RLLPointer
    //  to the first match. If the object exists multiple times on the list, the first one found
    //  will be the one the pointer sets itself to.
    //--Returns a boolean indicating if the object was found on the list. Note that, even if the
    //  object is not found, mListCurrent IS changed.
    mListCurrent = mListHead;
    while(mListCurrent)
    {
        if(mListCurrent->rData == pPtr) return true;
        mListCurrent = mListCurrent->rNext;
    }
    return false;
}
SugarLinkedListEntry *SugarLinkedList::GetRandomPointerWholeEntry()
{
    //--Returns the whole entry, that is, mName, rNext, rPrev, and all that jazz can be accessed.
    //  Only use this if you know what you're doing!
    return mListCurrent;
}
