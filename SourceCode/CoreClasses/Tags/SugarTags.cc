//--Base
#include "SugarTags.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

//=========================================== System ==============================================
SugarTag::SugarTag()
{
    //--System
    //--Value
    mValue = NULL;
    ResetString(mValue, "NULL");
}
SugarTag::~SugarTag()
{
    free(mValue);
}
void SugarTag::DeleteThis(void *pPtr)
{
    delete ((SugarTag *)pPtr);
}
SugarLinkedList *SugarTag::xTagList = new SugarLinkedList(true);

//====================================== Property Queries =========================================
char *SugarTag::GetValue()
{
    return mValue;
}
float SugarTag::GetAsFloat()
{
    //--Returns the value as a singular float.
    float tReturn = 0.0f;
    sscanf(mValue, "%f", &tReturn);
    return tReturn;
}
void SugarTag::GetXByY(float &sX, float &sY)
{
    //--The value must be in format "%f x %f". Note that the values are initted to zero, so they
    //  will return as 0 even if there's an error.
    sX = 0.0f;
    sY = 0.0f;
    sscanf(mValue, "%f x %f", &sX, &sY);
}
void SugarTag::GetDimensions(float &sX, float &sY, float &sW, float &sH)
{
    //--Format: "%f x %f, %f x %f"
    sX = 0.0f;
    sY = 0.0f;
    sW = 0.0f;
    sH = 0.0f;
    sscanf(mValue, "%f x %f, %f x %f", &sX, &sY, &sW, &sH);
}

//========================================= Manipulators ==========================================
void SugarTag::SetValue(const char *pValue)
{
    if(!pValue)
    {
        ResetString(mValue, "NULL");
    }
    else
    {
        ResetString(mValue, pValue);
    }
}
void SugarTag::SetValueF(float pValue)
{
    //--Value is set using a floating point. It's still stored as a string.
    char tBuffer[32];
    sprintf(tBuffer, "%f", pValue);
    ResetString(mValue, tBuffer);
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
SugarTag *SugarTag::Get(const char *pName)
{
    //--Returns the named tag, or a special "Dummy" tag if it wasn't found. If a tag is not found
    //  then a print is sent to the console. The program otherwise has no idea.
    SugarTag *rCheckTag = (SugarTag *)xTagList->GetElementByName(pName);
    if(rCheckTag) return rCheckTag;

    static SugarTag *xTag = new SugarTag();
    fprintf(stderr, "Error, tag %s does not exist! Returning dummy.\n", pName);
    return xTag;
}
void SugarTag::Set(const char *pName, const char *pValue)
{
    //--Static automated creator. If the tag exists, it is overwritten. If not, it is created.
    SugarTag *rTag = (SugarTag *)xTagList->GetElementByName(pName);
    if(rTag)
    {
        rTag->SetValue(pValue);
    }
    //--Create a new tag.
    else
    {
        SugarTag *nTag = new SugarTag();
        nTag->SetValue(pValue);
        SugarTag::xTagList->AddElement(pName, nTag, &SugarTag::DeleteThis);
    }
}
void SugarTag::SetF(const char *pName, float pValue)
{
    //--Overload of SugarTag::Set, uses a float instead.
    char tBuffer[32];
    sprintf(tBuffer, "%f", pValue);
    Set(pName, tBuffer);
}

//========================================= Lua Hooking ===========================================
void SugarTag::HookToLuaState(lua_State *pLuaState)
{
    /* Tag_Set(sName[], sValue[])
       Creates a new tag or replaces an existing one. */
    lua_register(pLuaState, "Tag_Set", &Hook_Tag_Set);

    /* Tag_Get(sNamep[)
       Returns the raw string of a tag. */
    lua_register(pLuaState, "Tag_Get", &Hook_Tag_Get);
}

//======================================= Command Hooking =========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_Tag_Set(lua_State *L)
{
    //Tag_Set(sName, sValue)
    int tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("Tag_Set");

    //--Get values
    const char *rName = lua_tostring(L, 1);
    const char *rValue = lua_tostring(L, 2);

    //--Use the static call.
    SugarTag::Set(rName, rValue);

    return 0;
}
int Hook_Tag_Get(lua_State *L)
{
    //Tag_Get(sName)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("Tag_Get");

    //--Get the tag. Remember that it can never return NULL.
    SugarTag *rTag = SugarTag::Get(lua_tostring(L, 1));
    lua_pushstring(L, rTag->GetValue());

    return 1;
}

//=================================================================================================
//                                       Command Functions                                       ==
//=================================================================================================
