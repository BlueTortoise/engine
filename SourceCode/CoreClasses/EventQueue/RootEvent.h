//--[RootEvent]
//--The basic type that can be inserted into the SugarEventQueue. Has a logic section that derived
//  classes can execute, and a query function for whether or not the event is complete.

#ifndef _ROOTEVENT_H_
#define _ROOTEVENT_H_

#include "Definitions.h"
#include "Structures.h"

class RootEvent
{
    private:

    protected:
    //--System
    bool mIsBlocker;
    bool mIsComplete;

    public:
    //--System
    RootEvent();
    virtual ~RootEvent();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    bool IsBlocking();
    bool IsComplete();

    //--Manipulators
    void SetIsBlocker(bool pFlag);

    //--Core Methods
    virtual void Execute();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

#endif
