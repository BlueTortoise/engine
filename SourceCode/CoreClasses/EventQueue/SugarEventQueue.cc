//--Base
#include "SugarEventQueue.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Event types
#include "DialogueAction.h"
#include "PlayerControlMod.h"
#include "WaitTicksEvent.h"
#include "ManualExecute.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"
#include "EntityManager.h"

//=========================================== System ==============================================
SugarEventQueue::SugarEventQueue()
{
    //--System
    mEventList = new SugarLinkedList(true);
}
SugarEventQueue::~SugarEventQueue()
{
    delete mEventList;
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void SugarEventQueue::RegisterEvent(const char *pName, RootEvent *pEvent)
{
    if(!pName || !pEvent) return;
    mEventList->AddElement(pName, pEvent, &RootEvent::DeleteThis);
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void SugarEventQueue::Update()
{
    //--Setup.
    bool tPurgeToBlocked = false;
    bool tAllCompleteSoFar = true;

    //--Begin execution of all event packs.
    RootEvent *rEvent = (RootEvent *)mEventList->PushIterator();
    while(rEvent)
    {
        //--Execute the event.
        if(!rEvent->IsComplete()) rEvent->Execute();

        //--Check if it's complete.
        if(!rEvent->IsComplete()) tAllCompleteSoFar = false;

        //--If this event is a blocker...
        if(rEvent->IsBlocking())
        {
            //--All events not completed, stop.
            if(!tAllCompleteSoFar)
            {
            }
            //--All events are completed, mark for purge and stop.
            else
            {
                tPurgeToBlocked = true;
            }
            mEventList->PopIterator();
            break;
        }

        //--Not a blocker, so continue execution.
        rEvent = (RootEvent *)mEventList->AutoIterate();
    }

    //--Purging time.
    if(!tPurgeToBlocked) return;
    //fprintf(stderr, "Purging\n");
    rEvent = (RootEvent *)mEventList->SetToHeadAndReturn();
    while(rEvent)
    {
        //--If the event is not a blocker...
        //fprintf(stderr, " Purge %s\n", mEventList->GetRandomPointerName());
        if(!rEvent->IsBlocking())
        {
            mEventList->RemoveElementI(0);
        }
        //--Event was a blocker. This is the last one we delete.
        else
        {
            mEventList->RemoveElementI(0);
            break;
        }

        //--Keep deleting.
        rEvent = (RootEvent *)mEventList->SetToHeadAndReturn();
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
#include "Global.h"
void SugarEventQueue::Render()
{
    //--For debug purposes only, renders the names of the events currently held by the queue.
    if(true) return;

    //--Constants
    const float cScale = 0.25f;

    //--Fail if there's no font.
    SugarFont *rSystemFont = Global::Shared()->gSystemFont;
    if(!rSystemFont) return;

    //--GL Setup.
    DisplayManager::StdModelPush();
    glScalef(cScale, -cScale, cScale);
    glTranslatef(0.0f, -40.0f, 0.0f);

    //--Render.
    RootEvent *rEvent = (RootEvent *)mEventList->PushIterator();
    while(rEvent)
    {
        //--Draw a black box underlying the text.
        float tWid = strlen(mEventList->GetIteratorName()) * 26.5f;
        SugarBitmap::DrawRectFill(0.0f, 0.0f, tWid, 33.0f, MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));

        //--Draw the text.
        rSystemFont->DrawText(0.0f, 0.0f, mEventList->GetIteratorName());

        //--Move down for the next entry.
        glTranslatef(0.0f, -40.0f, 0.0f);

        rEvent = (RootEvent *)mEventList->AutoIterate();
    }

    //--Clean.
    DisplayManager::StdModelPop();
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void SugarEventQueue::HookToLuaState(lua_State *pLuaState)
{
    /* SEQ_PushEvent(sName, "Blocker")
       SEQ_PushEvent(sName, "Timer", iTicks)
       SEQ_PushEvent(sName, "LockControls", sLockName)
       SEQ_PushEvent(sName, "UnlockControls", sLockName)
       SEQ_PushEvent(sName, "Dialogue")
       SEQ_PushEvent(sName, "ManualExecute")
       Pushes the specified event onto the active SEQ and also pushes it on the Activity Stack. Be
       sure to pop it when setup is completed. */
    lua_register(pLuaState, "SEQ_PushEvent", &Hook_SEQ_PushEvent);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_SEQ_PushEvent(lua_State *L)
{
    //SEQ_PushEvent(sName, "Blocker")
    //SEQ_PushEvent(sName, "Timer", iTicks)
    //SEQ_PushEvent(sName, "LockControls", sLockName)
    //SEQ_PushEvent(sName, "UnlockControls", sLockName)
    //SEQ_PushEvent(sName, "Dialogue")
    //SEQ_PushEvent(sName, "ManualExecute")
    DataLibrary::Fetch()->PushActiveEntity();
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("SEQ_PushEvent");

    //--Setup.
    RootEvent *nRegEvent = NULL;

    //--Switch type.
    const char *rName = lua_tostring(L, 1);
    const char *rSwitchType = lua_tostring(L, 2);

    //--Blocker. A root type with a flag set.
    if(!strcasecmp(rSwitchType, "Blocker") && tArgs == 2)
    {
        nRegEvent = new RootEvent();
        nRegEvent->SetIsBlocker(true);
    }
    //--Timer. Waits X ticks.
    else if(!strcasecmp(rSwitchType, "Timer") && tArgs == 3)
    {
        WaitTicksEvent *nEvent = new WaitTicksEvent();
        nEvent->SetTimer(lua_tointeger(L, 3));
        nRegEvent = nEvent;
    }
    //--Lockout player controls.
    else if(!strcasecmp(rSwitchType, "LockControls") && tArgs == 3)
    {
        PlayerControlMod *nEvent = new PlayerControlMod();
        nEvent->SetAddLockout(lua_tostring(L, 3));
        nRegEvent = nEvent;
    }
    //--Unlock player controls.
    else if(!strcasecmp(rSwitchType, "UnlockControls") && tArgs == 3)
    {
        PlayerControlMod *nEvent = new PlayerControlMod();
        nEvent->SetRemLockout(lua_tostring(L, 3));
        nRegEvent = nEvent;
    }
    //--Issue a dialogue action.
    else if(!strcasecmp(rSwitchType, "Dialogue") && tArgs == 2)
    {
        DialogueAction *nEvent = new DialogueAction();
        nRegEvent = nEvent;
    }
    //--Manual execution..
    else if(!strcasecmp(rSwitchType, "ManualExecute") && tArgs == 2)
    {
        ManualExecute *nEvent = new ManualExecute();
        nRegEvent = nEvent;
    }
    //--No type resolved.
    else
    {
        LuaPropertyError("SEQ_PushEvent", rSwitchType, tArgs);
    }

    //--If the event was set, register it. The current implementation has the SEQ on the EM, though
    //  it may also be possible to have it held dynamically later.
    if(nRegEvent)
    {
        DataLibrary::Fetch()->rActiveObject = nRegEvent;
        EntityManager::Fetch()->GetEventQueue()->RegisterEvent(rName, nRegEvent);
    }
    return 0;
}
