//--[SugarEventQueue]
//--A specialized version of the RandomLinkedList which is used for scripted events, such as
//  cutscenes. The list is only allowed to store SugarEvents and derived classes.
//--When called, all SugarEvents execute until a blocker pack is found. If all events flagged
//  themselves as complete, they are all removed and the next set of events can execute. Each event
//  has an internal logic module which can be overridden by derived classes.
//--For safety, always end the event list with a blocker event. This makes sure the list is always
//  cleaned up correctly before new events are added.

#ifndef _SUGAREVENTQUEUE_H_
#define _SUGAREVENTQUEUE_H_

#include "Definitions.h"
#include "Structures.h"

class SugarEventQueue
{
    private:
    //--System
    SugarLinkedList *mEventList;

    protected:

    public:
    //--System
    SugarEventQueue();
    ~SugarEventQueue();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void RegisterEvent(const char *pName, RootEvent *pEvent);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_SEQ_PushEvent(lua_State *L);

#endif
