//--Base
#include "GameGear.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "FlexMenu.h"
#include "VisualLevel.h"

//--CoreClasses
//--Definitions
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "ResetManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "MemoryManager.h"
#include "SteamManager.h"

#if defined _STEAM_API_
    #include "steam_api.h"
#endif

//--Code common to both gears regardless of library used.
void GameGear::PostLogicHandler()
{
    //--Setup.
    GLOBAL *rGlobal = Global::Shared();
    ControlManager *rControlManager = rGlobal->gControlManager;
    EntityManager *rEntityManager = rGlobal->gEntityManager;

    //--Run end-of-tick to drop duplicate keypresses.
    rControlManager->PostUpdate();
    rEntityManager->PostUpdate();

    //--Audio handling. Stops any music that's playing but at zero volume.
    rGlobal->gAudioManager->Update();
    AdvCombatAbility::HandleCombatActionSounds();

    //--Handle turn controls. This is not the same as the usual Update cycle. Turns concern the
    //  actions of entities, Update cycles concern things like animations on a per-tick basis.
    rEntityManager->UpdateTurns();

    //--Visual Level creates rendering packs for entities who are leaving the room.
    VisualLevel *rVisualLevel = VisualLevel::Fetch();
    if(rVisualLevel) rVisualLevel->UpdateVisiblityTimers();

    //--[Mouse Locking]
    //--DisplayManager may optionally keep the mouse within the display here.
    rGlobal->gDisplayManager->UpdateMouseLock();

    //--[Clear/Reset Controllers]
    //--To prevent instability, some objects can clear themselves after the tick is over. This way,
    //  an entity on the EM can clear the EM without deleting itself in the middle of its update.
    if(rEntityManager->mHasPendingClear)
    {
        rEntityManager->mHasPendingClear = false;
        rEntityManager->ClearAll();
    }

    //--If the global shows a pending game reset, do so here.
    if(rGlobal->gReset)
    {
        //--Flip the flag off.
        rGlobal->gReset = false;

        //--Issue the reset.
        rGlobal->gResetManager->IssueReset("Full Game Reset");
        rGlobal->gResetManager->IssueReset("Combat");
        rGlobal->gMapManager->BeginFade();

        //--Sound settings.
        rGlobal->gAudioManager->ChangeMusicVolume(0.75f - AudioManager::xMusicVolume);
        rGlobal->gAudioManager->ChangeSoundVolume(0.75f - AudioManager::xSoundVolume);
    }

    //--If the MapManager has this flag set, it's quitting out of Adventure Mode and returning to the title screen.
    if(rGlobal->gMapManager->mBackToTitle)
    {
        //--Unset the flag.
        rGlobal->gMapManager->mBackToTitle = false;

        //--Issue a full reset.
        rGlobal->gResetManager->IssueReset("Full Game Reset");
        rGlobal->gResetManager->IssueReset("Combat");
        rGlobal->gMapManager->BeginFade();

        //--Also clear the DataLibrary of any pending values.
        rGlobal->gDataLibrary->Purge("Root/Variables/");

        //--Sound settings.
        rGlobal->gAudioManager->ChangeMusicVolume(0.75f - AudioManager::xMusicVolume);
        rGlobal->gAudioManager->ChangeSoundVolume(0.75f - AudioManager::xSoundVolume);

        //--If the menu stack has nothing on it, push something.
        /*
        if(!rGlobal->gMapManager->MenuStackHasContents())
        {
            FlexMenu *nFlexMenu = new FlexMenu();
            MapManager::Fetch()->PushMenuStack(nFlexMenu);
            LuaManager::Fetch()->PushExecPop(nFlexMenu, "Data/Scripts/MainMenu/000 PopulateMainMenu.lua");
            //MapManager::Fetch()->BeginFade();
        }*/
    }

    //--Recompilation of shaders, if flagged.
    if(rGlobal->gControlManager->IsFirstPress("RecompileShaders"))
    {
        rGlobal->gDisplayManager->ClearAllShaderData();
        rGlobal->gDisplayManager->ShaderExec();
    }

    //--Recompilation of kernings, if flagged.
    if(rGlobal->gControlManager->IsFirstPress("RebuildKerning"))
    {
        DataLibrary::Fetch()->RebuildAllKerning();
    }

    //--Drop any and all events on the drop list.
    rGlobal->gCutsceneManager->DropListAtEndOfTick();

    //--Garbage handling.
    Memory::Tick();

    //--If using FMOD, it needs to periodically do sound upkeep.
    #if defined _FMOD_AUDIO_
        FMOD_System_Update(AudioManager::xFMODSystem);
    #endif

    //--Steam API.
    #if defined _STEAM_API_
    if(SteamManager::Fetch()->IsSteamRunning())
    {
        SteamManager::Fetch()->PumpCallbackStack();
        SteamManager::Fetch()->RequestStats();
    }
    #endif
}
