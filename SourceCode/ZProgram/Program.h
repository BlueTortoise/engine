//--[Program.h]
//--Header for the entry point file.

#pragma once

#include "Definitions.h"
#include "Structures.h"

int main(int pArgsTotal, char **pArgs);
void LogFPS(MainPackage &sMainPackage);

//--SDL Callback for the loop timer.
#if defined _SDL_PROJECT_
uint32_t Timer_Callback(uint32_t pInterval, void *pParameters);
#endif

