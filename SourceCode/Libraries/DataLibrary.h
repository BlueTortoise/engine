//--[DataLibrary]
//--General purpose archiver of pointers, by name.  Everything is stored in SugarLinkedLists as
//  void pointers, and the type is expected to be known by the caller.  If the object is known to
//  be a RootObject then the type can be resolved from there.
//--Supports up to four levels of indexing.  These are called, in order, Section Catalogue Heading
//  and then the pointer itself is in the Index.
//--Example:  Section/Catalogue/Heading/Pointer
//            Graphics/Ponies/Applejack/AJSmiling
//--Additional functionality is provided for storing variables within the library as Sysvars.  These
//  are Lua-ready variables which can be strings or floating-point.
//--The DL can also store pointers by activity in a stack.  Lua can push/pull to and from this stack
//  which allows for complex object manipulation in decentralized Lua scripts.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--Local Structures
typedef struct
{
    float mNumeric;
    char *mAlpha;
    bool mIsSaved;
}SysVar;
typedef struct
{
    char mSection[256];
    char mCatalogue[256];
    char mHeading[256];
    char mName[256];

    SugarLinkedList *rSection;
    SugarLinkedList *rCatalogue;
    SugarLinkedList *rHeading;
    void *rEntry;
}ResolvePack;

//--Local Definitions
#define DL_ROOT -1
#define DL_SECTION 0
#define DL_CATALOGUE 1
#define DL_HEADING 2
#define DL_NAME 3

class DataLibrary
{
    private:
    //--System
    bool mDebugMode;
    bool mErrorMode;

    //--Storage
    SugarLinkedList *mSectionList;
    SugarLinkedList *mActiveEntityStack;

    //--Counters
    int mRunningIndexCounter;

    //--Dummies
    RootObject *mDummyPtr;

    //--Font Storage
    SugarLinkedList *mMasterFontList; //SugarFont *, master
    SugarLinkedList *mFontKerningPathList; //char *, master, parallel with mMasterFontList
    SugarLinkedList *mrFontRefList;   //SugarFont *, ref

    protected:

    public:
    //--System
    DataLibrary();
    ~DataLibrary();
    static void DeleteSysVar(void *pPtr);

    //--Public Variables
    bool mFailSilently;
    SugarLinkedList *rCurrentSection;
    SugarLinkedList *rCurrentCatalogue;
    SugarLinkedList *rCurrentHeading;
    void *rActiveObject;
    int mErrorCode;
    static bool xBlockSpecialDeletion;
    static bool xGetEntryDebug;

    //--Property Queries
    SugarLinkedList *GetSection(const char *pResolveString);
    SugarLinkedList *GetCatalogue(const char *pResolveString);
    SugarLinkedList *GetHeading(const char *pResolveString);
    void *GetEntry(const char *pResolveString);
    bool DoesEntryExist(const char *pResolveString);
    bool IsActiveValid();
    bool IsActiveValid(int pTypeFlag);
    bool IsActiveValid(int pTypeStart, int pTypeEnd);
    int GetActiveObjectType();

    //--Manipulators
    void SetErrorFlag(bool pFlag);
    void SetErrorCode(int pCode);
    void AddPath(const char *pDLPath);
    void RegisterPointer(const char *pDLPath, void *pPointer);
    void RegisterPointer(const char *pDLPath, void *pPointer, DeletionFunctionPtr pDeletionFunction);
    void *RemovePointer(const char *pDLPath);

    void SetActiveObject(const char *pDLPath);
    void PushActiveEntity();
    void PushActiveEntity(void *pReplacer);
    void *PopActiveEntity();

    //--Fonts
    bool FontExists(const char *pName);
    bool FontAliasExists(const char *pName);
    void RegisterFont(const char *pName, SugarFont *pFont, const char *pKerningPath);
    void RegisterFontAlias(const char *pFontName, const char *pAliasName);
    void RebuildAllKerning();
    void RebuildFontKerning(const char *pName);
    SugarFont *GetFont(const char *pName);

    //--Core Methods
    //--Private Core Methods
    private:
    void AddSection(const char *pName);
    void AddCatalogue(const char *pName);
    void AddHeading(const char *pName);
    public:

    //--Purging (DataLibraryPurging.cc)
    void *Purge(const char *pDLPath);
    void *Purge(const char *pDLPath, bool pBlockDeallocation);

    //--Worker Functions
    void ErrorNotFound(const char *pCallerName, const char *pResolveString);
    ResolvePack *CreateResolvePack(const char *pResolveString);
    ResolvePack *CreateResolvePack(const char *pResolveString, bool pSkipLists);
    static void GetNames(const char *pResolveString, ResolvePack &sPack);
    void GetLists(ResolvePack &sResolvePack);
    static char *GetPathPart(const char *pResolveString, int pPartFlag);

    //--Variables
    void WriteToBuffer(SugarAutoBuffer *pBuffer);
    void ReadFromFile(VirtualFile *fInfile);

    //--Update
    //--File I/O
    void PrintToFile(const char *pFilePath);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static DataLibrary *Fetch();
    static const char *GetGamePath(const char *pVariablePath);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--[Hooking Functions]
//--Standard
int Hook_DL_AddPath(lua_State *L);
int Hook_DL_SetActiveObject(lua_State *L);
int Hook_DL_Exists(lua_State *L);

//--Purging
int Hook_DL_Purge(lua_State *L);

//--Activity Stack
int Hook_DL_PushActiveEntity(lua_State *L);
int Hook_DL_PopActiveEntity(lua_State *L);
int Hook_DL_ActiveIsValid(lua_State *L);
int Hook_DL_GetActiveObjectType(lua_State *L);
int Hook_DL_ClearActiveEntity(lua_State *L);

//--CoreClass Macros
int Hook_DL_LoadBitmap(lua_State *L);
int Hook_DL_ExtractBitmap(lua_State *L);
int Hook_DL_ExtractDummyBitmap(lua_State *L);
int Hook_DL_ReportBitmap(lua_State *L);
int Hook_DL_ExtractPalette(lua_State *L);

//--Variable Manager Handling
int Hook_VM_Exists(lua_State *L);
int Hook_VM_SetVar(lua_State *L);
int Hook_VM_RemVar(lua_State *L);
int Hook_VM_GetVar(lua_State *L);
int Hook_VM_SetSaveFlag(lua_State *L);

//--Fonts
int Hook_Font_Register(lua_State *L);
int Hook_Font_GetProperty(lua_State *L);
int Hook_Font_SetProperty(lua_State *L);

