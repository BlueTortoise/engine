//--Base
#include "DataLibrary.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarPalette.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "SugarLumpManager.h"

//========================================= Lua Hooking ===========================================
void DataLibrary::HookToLuaState(lua_State *pLuaState)
{
    ///--[Standard]
    /* DL_AddPath(sPath)
       Creates the path specified.  If any members don't exist, they are created as well.
       Format:  Root/Section/Catalogue/Heading/ */
    lua_register(pLuaState, "DL_AddPath",         &Hook_DL_AddPath);

    /* DL_SetActiveObject(sPath)
       Sets the specified object as the rActiveObject.  If the object is not found, sets NULL. */
    lua_register(pLuaState, "DL_SetActiveObject", &Hook_DL_SetActiveObject);

    /* DL_Exists(sPath) (1 boolean)
       Returns true if an entry exists at the requested path. If the path doesn't exist returns false,
       and if the entry does not exist also returns false. The entry can theoretically be a NULL or
       placeholder entry and the DataLibrary will not make a distinction. */
    lua_register(pLuaState, "DL_Exists", &Hook_DL_Exists);

    ///--[Purging]
    /* DL_Purge(sPath, bBlockDealloc)
       Deletes the specified DL divider. Pass true or false for BlockDealloc.  True will cause the
       divider to delete all its pieces, false will not.  Beware dangling pointers.
       Format:  Root/Section/Catalogue/Heading/
       Whatever the final piece (Heading, Catalogue, Section) is will be destroyed.
       The special path "ALL" will wipe the whole library. */
    lua_register(pLuaState, "DL_Purge", &Hook_DL_Purge);

    ///--[Activity Stack]
    /* DL_PushActiveEntity()
       Pushes the rActiveEntity onto the top of the Activity Stack.  Replaces it with NULL. */
    lua_register(pLuaState, "DL_PushActiveEntity", &Hook_DL_PushActiveEntity);

    /* DL_PopActiveEntity()
       Pops off the top of the Activity Stack onto rActiveEntity. */
    lua_register(pLuaState, "DL_PopActiveEntity",  &Hook_DL_PopActiveEntity);
    lua_register(pLuaState, "DL_PopActiveObject",  &Hook_DL_PopActiveEntity);

    /* DL_ActiveIsValid() (1 Boolean)
       Returns true if the rActiveObject is not NULL, false if it is. */
    lua_register(pLuaState, "DL_ActiveIsValid",  &Hook_DL_ActiveIsValid);

    /* DL_GetActiveObjectType()
       Requires that the current active object inherit RootObject or incorrect results will occur.
       Call at your own risk.
       Gets the type of the current active object. */
    lua_register(pLuaState, "DL_GetActiveObjectType",  &Hook_DL_GetActiveObjectType);

    /* DL_ClearActiveEntity()
       Sets the ActiveEntity to NULL */
    lua_register(pLuaState, "DL_ClearActiveEntity", &Hook_DL_ClearActiveEntity);

    ///--[CoreClass Macros]
    /* DL_LoadBitmap(sPath, sDLPath)
       Loads the image off the hard drive from the specified path.  Must be of a file format that
       is supported by SugarCube's file loaders. */
    lua_register(pLuaState, "DL_LoadBitmap", &Hook_DL_LoadBitmap);

    /* DL_ExtractBitmap(sInfileName, sDLPath)
       Extracts the image from the currently active SugarLumpFile (see SLF_Open and SLF_Close)
       and places it at the sDLPath's location. */
    lua_register(pLuaState, "DL_ExtractBitmap", &Hook_DL_ExtractBitmap);

    /* DL_ExtractDummyBitmap(sDLPath)
       Places a dummy bitmap in the requested path. This dummy bitmap will not stop later calls of
       DL_ExtractBitmap() from overwriting it, but will prevent NULL calls. */
    lua_register(pLuaState, "DL_ExtractDummyBitmap", &Hook_DL_ExtractDummyBitmap);

    /* DL_ReportBitmap(sDLPath)
       Prints out whether or not the bitmap exists at the given path and some simple properties. Used
       to check if a bitmap loaded correctly. */
    lua_register(pLuaState, "DL_ReportBitmap", &Hook_DL_ReportBitmap);

    /* DL_ExtractPalette(sInfileName, sDLPath)
       Extracts a palette from the currently active SugarLumpFile and places it at the sDLPath's
       location. */
    lua_register(pLuaState, "DL_ExtractPalette", &Hook_DL_ExtractPalette);

    ///--[Variable Manager]
    lua_register(pLuaState, "VM_Exists",      &Hook_VM_Exists);
    lua_register(pLuaState, "VM_SetVar",      &Hook_VM_SetVar);
    lua_register(pLuaState, "VM_RemVar",      &Hook_VM_RemVar);
    lua_register(pLuaState, "VM_GetVar",      &Hook_VM_GetVar);
    lua_register(pLuaState, "VM_SetSaveFlag", &Hook_VM_SetSaveFlag);

    ///--[Fonts]
    /* Font_Register(sName, sFontPath, sKerningPath, iSize, iFlags)
       Creates and registers a new font with the given name. sFontPath is a hard drive path. */
    lua_register(pLuaState, "Font_Register", &Hook_Font_Register);

    /* Font_GetProperty("") (dummy)
       Returns the requested property from the DataLibrary's font module. */
    lua_register(pLuaState, "Font_GetProperty", &Hook_Font_GetProperty);

    /* Font_SetProperty("")
       Sets the requested property in the DataLibrary's font module. */
    lua_register(pLuaState, "Font_SetProperty", &Hook_Font_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
//--[Standard]
int Hook_DL_AddPath(lua_State *L)
{
    //DL_AddPath(sDLPath)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("DL_AddPath");

    //--Setup
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->AddPath(lua_tostring(L, 1));
    return 0;
}
int Hook_DL_SetActiveObject(lua_State *L)
{
    //DL_SetActiveObject(sDLPath)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("DL_SetActiveObject");

    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->SetActiveObject(lua_tostring(L, 1));

    return 0;
}
int Hook_DL_Exists(lua_State *L)
{
    //DL_Exists(sDLPath) (1 boolean)
    int tArgs = lua_gettop(L);
    if(tArgs != 1)
    {
        LuaArgError("DL_Exists");
        lua_pushboolean(L, false);
        return 1;
    }

    //--Execute.
    lua_pushboolean(L, DataLibrary::Fetch()->DoesEntryExist(lua_tostring(L, 1)));

    return 1;
}

//---[Purging]
int Hook_DL_Purge(lua_State *L)
{
    //DL_Purge(sDLPath, bBlockDealloc)
    int tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("DL_Purge");

    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    bool tOldSuppress = rDataLibrary->mFailSilently;
    rDataLibrary->mFailSilently = true;
    rDataLibrary->Purge(lua_tostring(L, 1), lua_toboolean(L, 2));
    rDataLibrary->mFailSilently = tOldSuppress;

    return 0;
}

//--[Activity Stack]
int Hook_DL_PushActiveEntity(lua_State *L)
{
    //DL_PushActiveEntity()

    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->PushActiveEntity();
    return 0;
}
int Hook_DL_PopActiveEntity(lua_State *L)
{
    //DL_PopActiveEntity()

    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->PopActiveEntity();
    return 0;
}
int Hook_DL_ActiveIsValid(lua_State *L)
{
    //DL_ActiveIsValid()

    lua_pushboolean(L, (DataLibrary::Fetch()->rActiveObject != NULL));
    return 1;
}
int Hook_DL_GetActiveObjectType(lua_State *L)
{
    //DL_GetActiveObjectType()
    //No Arguments

    int tType = DataLibrary::Fetch()->GetActiveObjectType();
    lua_pushinteger(L, tType);
    return 1;
}
int Hook_DL_ClearActiveEntity(lua_State *L)
{
    //DL_ClearActiveEntity()
    //No arguments

    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->rActiveObject = NULL;
    return 0;
}

//--[CoreClass Macros]
int Hook_DL_LoadBitmap(lua_State *L)
{
    //DL_LoadBitmap(sPath, sLibPath)
    int tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("DL_LoadBitmap");

    //--Load the bitmap.
    SugarBitmap *nNewBitmap = new SugarBitmap(lua_tostring(L, 1));

    //--Reg it
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->RegisterPointer(lua_tostring(L, 2), nNewBitmap, &SugarBitmap::DeleteThis);

    return 0;
}
int Hook_DL_ExtractBitmap(lua_State *L)
{
    //DL_ExtractBitmap(sInfileName, sDLPath)
    //DL_ExtractBitmap(sInfileName, sDLPath, bIsTileCase)
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("DL_ExtractBitmap");

    //--If the entry already exists, we don't need to load it. Just stop.
    DataLibrary *rLibrary = DataLibrary::Fetch();
    void *rExistingEntry = rLibrary->GetEntry(lua_tostring(L, 2));
    if(rExistingEntry)
    {
        //--Check if the entry is the dummy white pixel. If it is, we can overwrite it.
        void *rDummyPixel = DataLibrary::Fetch()->GetEntry("Root/Images/System/System/DummyPixel");
        if(rExistingEntry == rDummyPixel)
        {

        }
        //--Otherwise, the duplicate is legitimate. Ignore it.
        else
        {
            //fprintf(stderr, "Not loading duplicate at %s\n", lua_tostring(L, 2));
            return 0;
        }
    }

    //--Setup.
    SugarLumpManager *rManager = SugarLumpManager::Fetch();
    SugarBitmap *nNewBitmap = NULL;

    //--Normal.
    if(tArgs <= 2)
    {
        nNewBitmap = rManager->GetImage(lua_tostring(L, 1));
    }
    //--Tilemaps.
    else
    {
        nNewBitmap = rManager->GetPaddedTileImage(lua_tostring(L, 1));
    }

    //--Error check.
    if(!nNewBitmap)
    {
        DebugManager::ForcePrint("DL_ExtractBitmap:  Failed, no Bitmap named %s found.\n", lua_tostring(L, 1));
    }
    //--Register.
    else
    {
        rLibrary->RegisterPointer(lua_tostring(L, 2), nNewBitmap, &SugarBitmap::DeleteThis);
        //DebugManager::ForcePrint("Regged %s to %s %p\n", lua_tostring(L, 1), lua_tostring(L, 2), nNewBitmap);
    }

    return 0;
}
int Hook_DL_ExtractDummyBitmap(lua_State *L)
{
    //DL_ExtractDummyBitmap(sDLPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("DL_ExtractDummyBitmap");

    //--If the entry already exists, we don't need to do anything. Stop.
    DataLibrary *rLibrary = DataLibrary::Fetch();
    if(rLibrary->GetEntry(lua_tostring(L, 1)))
    {
        //fprintf(stderr, "Not loading duplicate at %s\n", lua_tostring(L, 1));
        return 0;
    }

    //--Place a fake bitmap in the given slot.
    SugarBitmap *rDummyPixel = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/System/System/DummyPixel");
    rLibrary->RegisterPointer(lua_tostring(L, 1), rDummyPixel, &DontDeleteThis);

    return 0;
}
int Hook_DL_ReportBitmap(lua_State *L)
{
    //DL_ReportBitmap(sDLPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("DL_ReportBitmap");

    //--If the entry does not exist:
    SugarBitmap *rBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(lua_tostring(L, 1));
    if(!rBitmap)
    {
        DebugManager::ForcePrint("Bitmap %s does not exist.\n", lua_tostring(L, 1));
    }
    //--Entry exists, print:
    else
    {
        DebugManager::ForcePrint("Bitmap %s exists, %i x %i\n", lua_tostring(L, 1), rBitmap->GetWidth(), rBitmap->GetHeight());
    }

    //--Clean.
    return 0;
}
int Hook_DL_ExtractPalette(lua_State *L)
{
    //DL_ExtractPalette(sInfileName, sDLPath)
    int tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("DL_ExtractPalette");

    DataLibrary *rLibrary = DataLibrary::Fetch();
    SugarLumpManager *rManager = SugarLumpManager::Fetch();
    SugarPalette *nNewPalette = rManager->GetPalette(lua_tostring(L, 1));

    if(!nNewPalette)
    {
        DebugManager::ForcePrint("DL_ExtractPalette:  Failed, no Palette named %s found.\n", lua_tostring(L, 1));
    }
    else
    {
        rLibrary->RegisterPointer(lua_tostring(L, 2), nNewPalette, &SugarPalette::DeleteThis);
    }

    return 0;
}

