//--Base
#include "DataLibrary.h"

//--Classes
#include "PairanormalLevel.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--Variable Manager Handling. These are a series of Lua functions that are specially designed to
//  operate on script variables. The variables are of type SysVar, defined in DataLibrary.h.
//  By default, they are all flagged to be saved by the SaveManager, though you can switch this off.

//--The SaveManager will make a copy of the DataLibrary's Root/Variables/ section and reinit it on game
//  load. Any other variables stored on any other list will be ignored!

//--Note: VM_SetVar will create a var if it doesn't exist. If the existence of a variable matters,
//  you can check if it exists with VM_Exists.

//--[Debug]
//#define DLVARS_DEBUG
#ifdef DLVARS_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--[External]
void DataLibrary::WriteToBuffer(SugarAutoBuffer *pBuffer)
{
    ///--[Documentation and Setup]
    //--Writes all VM variables to the given buffer. VM variables are valid if they are in Root/Variables/
    //  and have their saving flag set.
    if(!pBuffer) return;
    DebugPush(true, "Writing SCRIPTVARS_ block to data buffer %p.\n", pBuffer);

    //--Get the variable section. If it doesn't exist, write 0 to indicate there were no script variables.
    SugarLinkedList *rVariableSection = GetSection("Root/Variables/");
    SugarLinkedList *rSavingSection   = GetSection("Root/Saving/");
    if(!rVariableSection && !rSavingSection)
    {
        pBuffer->AppendInt32(0);
        DebugPop("Error, no variables/saving section.\n");
        return;
    }

    //--Create a linked list that will store reference copies to all variables.
    char tBuffer[512];
    SugarLinkedList *tAllVarsList = new SugarLinkedList(false);
    DebugPrint("Created master list.\n");

    ///--[Variables Section]
    //--Now build a listing of all the script variables.
    if(rVariableSection)
    {
        SugarLinkedList *rCatalogueList = (SugarLinkedList *)rVariableSection->PushIterator();
        while(rCatalogueList)
        {
            //--Iterate across the headings.
            SugarLinkedList *rHeadingList = (SugarLinkedList *)rCatalogueList->PushIterator();
            while(rHeadingList)
            {
                //--Iterate across the variables in this heading. Store them in the tAllVarsList.
                void *rVariable = rHeadingList->PushIterator();
                while(rVariable)
                {
                    //--The name is the path. The value is the pointer.
                    sprintf(tBuffer, "Root/%s/%s/%s/%s", "Variables", rVariableSection->GetIteratorName(), rCatalogueList->GetIteratorName(), rHeadingList->GetIteratorName());
                    tAllVarsList->AddElement(tBuffer, rVariable);

                    //--Next variable.
                    rVariable = rHeadingList->AutoIterate();
                }

                //--Next heading.
                rHeadingList = (SugarLinkedList *)rCatalogueList->AutoIterate();
            }

            //--Next catalogue.
            rCatalogueList = (SugarLinkedList *)rVariableSection->AutoIterate();
        }
    }
    #if defined DLVARS_DEBUG
    int tAllVarsTotal = tAllVarsList->GetListSize();
    DebugPrint("Iterated across variables listing. %i variables found.\n", tAllVarsTotal);
    #endif

    ///--[Saving Section]
    //--Iterate across the saving section and write it as well.
    if(rSavingSection)
    {
        SugarLinkedList *rCatalogueList = (SugarLinkedList *)rSavingSection->PushIterator();
        while(rCatalogueList)
        {
            //--Iterate across the headings.
            SugarLinkedList *rHeadingList = (SugarLinkedList *)rCatalogueList->PushIterator();
            while(rHeadingList)
            {
                //--Iterate across the variables in this heading. Store them in the tAllVarsList.
                void *rVariable = rHeadingList->PushIterator();
                while(rVariable)
                {
                    //--The name is the path. The value is the pointer.
                    sprintf(tBuffer, "Root/%s/%s/%s/%s", "Saving", rSavingSection->GetIteratorName(), rCatalogueList->GetIteratorName(), rHeadingList->GetIteratorName());
                    tAllVarsList->AddElement(tBuffer, rVariable);

                    //--Next variable.
                    rVariable = rHeadingList->AutoIterate();
                }

                //--Next heading.
                rHeadingList = (SugarLinkedList *)rCatalogueList->AutoIterate();
            }

            //--Next catalogue.
            rCatalogueList = (SugarLinkedList *)rSavingSection->AutoIterate();
        }
    }
    #if defined DLVARS_DEBUG
    DebugPrint("Iterated across saving listing. %i variables found.\n", tAllVarsList->GetListSize() - tAllVarsTotal);
    #endif

    ///--[Write All Variables]
    //--Write how many variables there are.
    pBuffer->AppendInt32(tAllVarsList->GetListSize());
    DebugPrint("Stored variable count: %i.\n", tAllVarsList->GetListSize());

    //--Write the variables.
    int i = 0;
    SysVar *rVariable = (SysVar *)tAllVarsList->PushIterator();
    while(rVariable)
    {
        //--Name is the path.
        DebugPrint(" %03i: %s - ", i, tAllVarsList->GetIteratorName());
        pBuffer->AppendStringWithLen(tAllVarsList->GetIteratorName());

        //--If this is a numerical type, place an 'N' and the value as a float.
        if(!rVariable->mAlpha || !strcasecmp(rVariable->mAlpha, "NULL"))
        {
            DebugPrint("N: ");
            pBuffer->AppendCharacter('N');
            pBuffer->AppendFloat(rVariable->mNumeric);
            DebugPrint(" %f\n", rVariable->mNumeric);
        }
        //--String type, place an 'S' and the value as a string.
        else
        {
            DebugPrint("S: ");
            pBuffer->AppendCharacter('S');
            pBuffer->AppendStringWithLen(rVariable->mAlpha);
            DebugPrint(" %s\n", rVariable->mAlpha);
        }

        //--Next.
        i++;
        rVariable = (SysVar *)tAllVarsList->AutoIterate();
    }
    DebugPrint("Wrote variables.\n");

    ///--[Finish Up]
    //--Debug.
    DebugPop("Finished writing SCRIPTVARS_ block.\n");
}
void DataLibrary::ReadFromFile(VirtualFile *fInfile)
{
    //--Reads the script variables from the file, assuming the cursor is in position. Script variables that
    //  already exist are overwritten, new ones are instantiated as needed. Variables not in the file are
    //  left alone.
    if(!fInfile) return;
    fInfile->mAssumeZeroIsMax = true;

    //--Get how many variables are expected.
    int32_t tExpectedVariables = 0;
    fInfile->Read(&tExpectedVariables, sizeof(int32_t), 1);

    //--Read the variables.
    for(int i = 0; i < tExpectedVariables; i ++)
    {
        //--Get the path of the variable.
        char *tVariablePath = fInfile->ReadLenString();

        //--Check if the variable exists.
        mFailSilently = true;
        SysVar *rVar = (SysVar *)GetEntry(tVariablePath);
        mFailSilently = false;

        //--If the variable does not exist, create it.
        if(!rVar)
        {
            //--Create the variable.
            SetMemoryData(__FILE__, __LINE__);
            rVar = (SysVar *)starmemoryalloc(sizeof(SysVar));
            rVar->mIsSaved = true;
            rVar->mNumeric = 0.0f;
            SetMemoryData(__FILE__, __LINE__);
            rVar->mAlpha = (char *)starmemoryalloc(sizeof(char) * (strlen("NULL") + 1));
            strcpy(rVar->mAlpha, "NULL");

            //--Add the path.
            AddPath(tVariablePath);
            RegisterPointer(tVariablePath, rVar, &DataLibrary::DeleteSysVar);
        }

        //--Read a character. If it's N, this is a numeric value.
        char tLetter = 'N';
        fInfile->Read(&tLetter, sizeof(char), 1);
        if(tLetter == 'N')
        {
            fInfile->Read(&rVar->mNumeric, sizeof(float), 1);
        }
        //--String.
        else
        {
            free(rVar->mAlpha);
            rVar->mAlpha = fInfile->ReadLenString();
        }

        //--Clean.
        free(tVariablePath);
    }

    //--Clean.
    fInfile->mAssumeZeroIsMax = false;
}

//--[Lua Functions]
int Hook_VM_Exists(lua_State *L)
{
    //--Returns true if the entry exists. This doesn't check for a specific SysVar, it actually
    //  just returns true if the DL entry exists at all.
    //VM_Exists(sDLPath)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("VM_Exists", L);

    //--Check
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->mFailSilently = true;
    void *rCheckPtr = rLibrary->GetEntry(lua_tostring(L, 1));
    rLibrary->mFailSilently = false;

    lua_pushboolean(L, (rCheckPtr != NULL));
    return 1;
}
int Hook_VM_SetVar(lua_State *L)
{
    //--Sets the variable. If it doesn't exist, creates it.
    //VM_SetVar(sDLPath, "S", sString)
    //VM_SetVar(sDLPath, "N", fValue)
    int tArgs = lua_gettop(L);
    if(tArgs != 3)
    {
        if(tArgs < 1) return LuaArgError("VM_SetVar");
        fprintf(stderr, "VM_SetVar. Incorrect argument count. Variable: %s.\n", lua_tostring(L, 1));
        return 0;
    }

    //--Fail if running to a checkpoint. Used by the VisualNovel engine.
    if(PairanormalLevel::xIsRunningToCheckpoint) return 0;

    //--Nil check.
    if(lua_isnil(L, 3))
    {
        DebugManager::ForcePrint("VM_SetVar: Error, argument 3 was nil.\n");
        return 0;
    }

    //--Fetch the var.
    bool tNewReg = false;
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->mFailSilently = true;
    SysVar *rVar = (SysVar *)rLibrary->GetEntry(lua_tostring(L, 1));
    rLibrary->mFailSilently = false;

    //--Doesn't exist?  Create one.
    if(!rVar)
    {
        SetMemoryData(__FILE__, __LINE__);
        rVar = (SysVar *)starmemoryalloc(sizeof(SysVar));
        rVar->mIsSaved = true;
        rVar->mNumeric = 0.0f;
        SetMemoryData(__FILE__, __LINE__);
        rVar->mAlpha = (char *)starmemoryalloc(sizeof(char) * (strlen("NULL") + 1));
        strcpy(rVar->mAlpha, "NULL");
        tNewReg = true;
    }

    //--Set the variable appropriately.
    const char *rTypeString = lua_tostring(L, 2);
    if(!strcmp(rTypeString, "S"))
    {
        const char *rString = lua_tostring(L, 3);
        free(rVar->mAlpha);
        SetMemoryData(__FILE__, __LINE__);
        rVar->mAlpha = (char *)starmemoryalloc(sizeof(char) * (strlen(rString) + 1));
        strcpy(rVar->mAlpha, rString);
    }
    else if(!strcmp(rTypeString, "N"))
    {
        rVar->mNumeric = lua_tonumber(L, 3);
    }

    //--If this is a new var, reg it.
    if(tNewReg)
    {
        rLibrary->AddPath(lua_tostring(L, 1));
        rLibrary->RegisterPointer(lua_tostring(L, 1), rVar, &DataLibrary::DeleteSysVar);
    }
    return 0;
}
int Hook_VM_RemVar(lua_State *L)
{
    //--Removes and deallocates the variable.
    //VM_RemVar(sDLPath)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("VM_RemVar");

    //--Fetch the var.
    DataLibrary *rLibrary = DataLibrary::Fetch();
    SysVar *rVar = (SysVar *)rLibrary->RemovePointer(lua_tostring(L, 1));

    //--Doesn't exist?  Done!
    if(!rVar) return 0;

    //--Deallocate it.
    free(rVar->mAlpha);
    free(rVar);
    return 0;
}
int Hook_VM_GetVar(lua_State *L)
{
    //--Returns the value in the variable, based on the type passed.
    //VM_GetVar(sDLPath, "S")
    //VM_GetVar(sDLPath, "N")
    int tArgs = lua_gettop(L);
    if(tArgs != 2)
    {
        if(tArgs < 1) return LuaArgError("VM_GetVar");
        fprintf(stderr, "VM_GetVar. Incorrect argument count. Variable: %s.\n", lua_tostring(L, 1));
        return 0;
    }

    //--Fetch the var.
    DataLibrary *rLibrary = DataLibrary::Fetch();
    rLibrary->mFailSilently = true;
    SysVar *rVar = (SysVar *)rLibrary->GetEntry(lua_tostring(L, 1));
    rLibrary->mFailSilently = false;

    //--Doesn't exist?  Done.
    if(!rVar)
    {
        //fprintf(stderr, "VM_GetVar:  Failed, var not found\n");
        lua_pushnumber(L, 0.0f);
        return 1;
    }

    //--Return the variable appropriately.
    const char *rTypeString = lua_tostring(L, 2);
    if(!strcmp(rTypeString, "S"))
    {
        lua_pushstring(L, rVar->mAlpha);
        return 1;
    }
    else if(!strcmp(rTypeString, "N"))
    {
        lua_pushnumber(L, rVar->mNumeric);
        return 1;
    }

    fprintf(stderr, "VM_GetVar:  Failed, can't resolve %s\n", rTypeString);
    lua_pushnumber(L, 0.0f);
    return 1;
}
int Hook_VM_SetSaveFlag(lua_State *L)
{
    //--Edits the save flag.  If true, the game will save it and load it.  This is true by default.
    //VM_SetSaveFlag(sDLPath, bFlag)
    int tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("VM_SetSaveFlag", L);

    //--Fetch the var.
    DataLibrary *rLibrary = DataLibrary::Fetch();
    SysVar *rVar = (SysVar *)rLibrary->RemovePointer(lua_tostring(L, 1));

    //--Doesn't exist?  Done!
    if(!rVar) return 0;

    //--Flip the flag.
    rVar->mIsSaved = lua_toboolean(L, 2);
    return 0;
}
