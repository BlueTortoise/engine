//--Base
#include "DataLibrary.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "LuaManager.h"

//--[Property Queries]
bool DataLibrary::FontExists(const char *pName)
{
    return (mMasterFontList->GetSlotOfElementByName(pName) != -1);
}
bool DataLibrary::FontAliasExists(const char *pName)
{
    return (mrFontRefList->GetSlotOfElementByName(pName) != -1);
}

//--[Manipulators]
void DataLibrary::RegisterFont(const char *pName, SugarFont *pFont, const char *pKerningPath)
{
    //--Registers a new font to the master list. The kerning path should be "NULL" if no kerning is desired.
    if(!pName || !pFont || !pKerningPath) return;

    //--If the font already exists, do nothing.
    if(mMasterFontList->GetElementByName(pName))
    {
        return;
    }

    //--Register.
    mMasterFontList->AddElementAsTail(pName, pFont, &RootObject::DeleteThis);
    mFontKerningPathList->AddElementAsTail(pName, InitializeString(pKerningPath), &FreeThis);
}
void DataLibrary::RegisterFontAlias(const char *pFontName, const char *pAliasName)
{
    //--Takes an existing font and gives it an additional name. Internal managers use these names to find fonts rather
    //  than the registered name of the font.
    if(!pFontName || !pAliasName) return;

    //--If the alias already exists, do nothing.
    if(mrFontRefList->GetElementByName(pAliasName)) return;

    //--Locate the font.
    void *rFont = mMasterFontList->GetElementByName(pFontName);
    if(!rFont) return;

    //--Give it the alias.
    mrFontRefList->AddElement(pAliasName, rFont);
}

//--[Core Methods]
void DataLibrary::RebuildAllKerning()
{
    //--Orders all fonts to rebuild their kerning. Fonts with "Null" as their kerning path do nothing.
    LuaManager *rLuaManager = LuaManager::Fetch();

    //--Iterate.
    void *rFont = mMasterFontList->PushIterator();
    char *rPath = (char *)mFontKerningPathList->PushIterator();
    while(rFont)
    {
        //--If the path is "Null", skip it.
        if(strcasecmp(rPath, "Null"))
        {
            rLuaManager->PushExecPop(rFont, rPath);
        }

        //--Next.
        rPath = (char *)mFontKerningPathList->AutoIterate();
        rFont = mMasterFontList->AutoIterate();
    }
}
void DataLibrary::RebuildFontKerning(const char *pName)
{
    //--Rebuilds the kerning for the named font.
    LuaManager *rLuaManager = LuaManager::Fetch();

    //--Locate both the font and its kerning entry.
    void *rFont = mMasterFontList->GetElementByName(pName);
    char *rPath = (char *)mFontKerningPathList->GetElementByName(pName);

    //--Execute.
    rLuaManager->PushExecPop(rFont, rPath);
}

//--[Pointer Routing]
SugarFont *DataLibrary::GetFont(const char *pName)
{
    //--Returns the named font. This must be an alias, not the font's core name. Returns NULL on error.
    if(!pName) return NULL;
    SugarFont *rFont = (SugarFont *)mrFontRefList->GetElementByName(pName);
    if(!rFont) fprintf(stderr, "Warning: No font with alias %s was found!\n", pName);
    return rFont;
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_Font_Register(lua_State *L)
{
    //Font_Register(sName, sFontPath, sKerningPath, iSize, iFlags)
    int tArgs = lua_gettop(L);
    if(tArgs < 5) return LuaArgError("Font_Register");

    //--Arg check.
    int cFontSize = lua_tointeger(L, 4);
    if(cFontSize < 1)
    {
        fprintf(stderr, "%s: Failed, font has illegal size %i. %s.\n", "Font_Register", cFontSize, LuaManager::Fetch()->GetCallStack(0));
        return 0;
    }

    //--Font already exists.
    if(DataLibrary::Fetch()->FontExists(lua_tostring(L, 1)))
    {
        //fprintf(stderr, "%s: Failed, font already exists. %s.\n", "Font_Register", LuaManager::Fetch()->GetCallStack(0));
        return 0;
    }

    //--Create.
    SugarFont *nFont = new SugarFont();
    nFont->ConstructWith(lua_tostring(L, 2), cFontSize, lua_tointeger(L, 5));

    //--Build Kerning
    const char *rKerningPath = lua_tostring(L, 3);
    if(strcasecmp(rKerningPath, "Null"))
    {
        LuaManager::Fetch()->PushExecPop(nFont, rKerningPath);
    }

    //--Register.
    DataLibrary::Fetch()->RegisterFont(lua_tostring(L, 1), nFont, rKerningPath);

    //--Interrupt call.
    if(SugarBitmap::xInterruptCall) SugarBitmap::xInterruptCall();
    return 0;
}
int Hook_Font_GetProperty(lua_State *L)
{
    //--[List]
    //Font_GetProperty("Font Exists", sFontName) (1 Boolean)
    //Font_GetProperty("Alias Exists", sAliasName) (1 Boolean)
    //Font_GetProperty("Constant Precache With Nearest") (1 Integer)
    //Font_GetProperty("Constant Precache With Edge") (1 Integer)
    //Font_GetProperty("Constant Precache With Downfade") (1 Integer)
    //Font_GetProperty("Constant Precache With Special S") (1 Integer)
    //Font_GetProperty("Constant Precache With Special !") (1 Integer)

    //--[Execution]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Font_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Returns whether or not the named font already exists.
    if(!strcasecmp(rSwitchType, "Font Exists") && tArgs == 2)
    {
        lua_pushboolean(L, DataLibrary::Fetch()->FontExists(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns whether or not the named alias already exists.
    else if(!strcasecmp(rSwitchType, "Alias Exists") && tArgs == 2)
    {
        lua_pushboolean(L, DataLibrary::Fetch()->FontAliasExists(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns a construction constant. This one uses GL_NEAREST instead of GL_LINEAR.
    else if(!strcasecmp(rSwitchType, "Constant Precache With Nearest") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_NEAREST);
        tReturns = 1;
    }
    //--Returns a construction constant. This causes a black border to be created.
    else if(!strcasecmp(rSwitchType, "Constant Precache With Edge") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_EDGE);
        tReturns = 1;
    }
    //--Returns a construction constant. This causes a the letters to grey from top to bottom.
    else if(!strcasecmp(rSwitchType, "Constant Precache With Downfade") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_DOWNFADE);
        tReturns = 1;
    }
    //--Returns a construction constant. This causes a different algorithm to work on the 'S', needed for some fonts.
    else if(!strcasecmp(rSwitchType, "Constant Precache With Special S") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_SPECIAL_S);
        tReturns = 1;
    }
    //--Returns a construction constant. This modifies the '!' character used for MrPixel.
    else if(!strcasecmp(rSwitchType, "Constant Precache With Special !") && tArgs == 1)
    {
        lua_pushinteger(L, SUGARFONT_PRECACHE_WITH_SPECIAL_EXC);
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("Font_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_Font_SetProperty(lua_State *L)
{
    //--[List]
    //Font_SetProperty("Downfade", fStart, fFinish)
    //Font_SetProperty("Outline Width", iWidth)
    //Font_SetProperty("Add Alias", sFontName, sAliasName)

    //--[Execution]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Font_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Sets downfade for the next constructed font.
    if(!strcasecmp(rSwitchType, "Downfade") && tArgs == 3)
    {
        SugarFont::SetDownfade(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Sets the outline width for the next constructed font.
    else if(!strcasecmp(rSwitchType, "Outline Width") && tArgs == 2)
    {
        SugarFont::SetOutlineWidth(lua_tointeger(L, 2));
    }
    //--Registers a new alias for the named font.
    else if(!strcasecmp(rSwitchType, "Add Alias") && tArgs == 3)
    {
        DataLibrary::Fetch()->RegisterFontAlias(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Error.
    else
    {
        LuaPropertyError("Font_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
