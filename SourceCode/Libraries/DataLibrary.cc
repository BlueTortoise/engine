//--Base
#include "DataLibrary.h"

//--Classes
#include "RootEntity.h"
//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--Generics
#include "SugarLinkedList.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
bool DataLibrary::xBlockSpecialDeletion = false;
DataLibrary::DataLibrary()
{
    //--System
    mDebugMode = false;
    mErrorMode = true;

    //--Storage
    mSectionList = new SugarLinkedList(true);
    mActiveEntityStack = new SugarLinkedList(false);

    //--Counters
    mRunningIndexCounter = 1;

    //--Dummies
    mDummyPtr = new RootObject();

    //--Font Storage
    mMasterFontList = new SugarLinkedList(true);
    mFontKerningPathList = new SugarLinkedList(true);
    mrFontRefList = new SugarLinkedList(false);

    //--Public Variables
    mFailSilently = false;
    rCurrentSection = NULL;
    rCurrentCatalogue = NULL;
    rCurrentHeading = NULL;
    rActiveObject = NULL;
    mErrorCode = 0;
}
DataLibrary::~DataLibrary()
{
    delete mSectionList;
    delete mActiveEntityStack;
    DataLibrary::xBlockSpecialDeletion = true;
    delete mDummyPtr;
    delete mMasterFontList;
    delete mFontKerningPathList;
    delete mrFontRefList;
}
void DataLibrary::DeleteSysVar(void *pPtr)
{
    SysVar *rPtr = (SysVar *)pPtr;
    free(rPtr->mAlpha);
    free(rPtr);
}

//--[Public Statics]
//--When set to true, and the debug printer is on, prints GetEntry() calls as they happen and the address
//  of the result. This allows a debugger to find out which calls are failing. The debug define must be
//  turned on, otherwise it will unnecessarily slow data access.
//--Flip the flag to true over top of whatever section you want to analyze, and set it to false later.
#define GETENTRYDEBUG
bool DataLibrary::xGetEntryDebug = false;

//====================================== Property Queries =========================================
SugarLinkedList *DataLibrary::GetSection(const char *pResolveString)
{
    //--Finds the Section specified. Must be of format Root/Section/
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    SugarLinkedList *rSection = tResolvePack->rSection;
    free(tResolvePack);

    if(!tResolvePack) ErrorNotFound("GetSection(N)", pResolveString);
    return rSection;
}
SugarLinkedList *DataLibrary::GetCatalogue(const char *pResolveString)
{
    //--Finds the Catalogue specified. Must be of format Root/Section/Catalogue/
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    SugarLinkedList *rCatalogue = tResolvePack->rCatalogue;
    free(tResolvePack);

    if(!tResolvePack) ErrorNotFound("GetCatalogue(N)", pResolveString);
    return rCatalogue;
}
SugarLinkedList *DataLibrary::GetHeading(const char *pResolveString)
{
    //--Finds the Heading specified. Must be of format Root/Section/Catalogue/Heading/
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    SugarLinkedList *rHeading = tResolvePack->rHeading;
    free(tResolvePack);

    if(!tResolvePack) ErrorNotFound("GetHeading(N)", pResolveString);
    return rHeading;
}
void *DataLibrary::GetEntry(const char *pResolveString)
{
    //--Finds the Entry specified.  Must be of format Root/Section/Catalogue/Heading/Name
    ResolvePack *tResolvePack = CreateResolvePack(pResolveString);
    void *rEntry = tResolvePack->rEntry;
    free(tResolvePack);

    //--[Debug]
    #ifdef GETENTRYDEBUG
    if(xGetEntryDebug)
    {
        fprintf(stderr, "%s %p\n", pResolveString, rEntry);
        if(!rEntry) fprintf(stderr, "==> Null <==\n");
    }

    #endif

    if(!tResolvePack) ErrorNotFound("GetEntry(N)", pResolveString);
    return rEntry;
}
bool DataLibrary::DoesEntryExist(const char *pResolveString)
{
    //--Returns true if the entry exists, false if it does not.
    return (GetEntry(pResolveString) != NULL);
}
bool DataLibrary::IsActiveValid()
{
    //--Returns true if the rActiveObject is not NULL.
    return (rActiveObject != NULL);
}
bool DataLibrary::IsActiveValid(int pTypeFlag)
{
    //--Returns true if the rActiveObject is not NULL, AND has the matching type flag set.
    //  Otherwise, false.
    if(!rActiveObject) return false;
    RootObject *rRootObject = (RootObject *)rActiveObject;
    return (rRootObject->IsOfType(pTypeFlag));
}
bool DataLibrary::IsActiveValid(int pTypeStart, int pTypeEnd)
{
    //--Returns true if the rActiveObject is not NULL, AND has its type flag between the start and
    //  end flags passed.  Otherwise, false.
    if(!rActiveObject) return false;
    RootObject *rRootObject = (RootObject *)rActiveObject;
    return (rRootObject->GetType() >= pTypeStart && rRootObject->GetType() <= pTypeEnd);
}
int DataLibrary::GetActiveObjectType()
{
    //--Returns the type of the active object. If the object is NOT of the RootObject inheritance
    //  structure, the results will be undefined.
    //--If this object is NULL, POINTER_TYPE_FAIL will be returned.
    if(!rActiveObject) return POINTER_TYPE_FAIL;

    //--This is where the crash is going to occur if the object wasn't in the heirarchy.
    RootObject *rObj = (RootObject *)rActiveObject;
    return rObj->GetType();
}

//========================================= Manipulators ==========================================
void DataLibrary::SetErrorFlag(bool pFlag)
{
    mErrorMode = pFlag;
}
void DataLibrary::SetErrorCode(int pCode)
{
    mErrorCode = pCode;
}
void DataLibrary::AddPath(const char *pDLPath)
{
    //--Adds the required path to the DataLibrary. If any of the sections, catalogues, or headings
    //  specified already exist, then they are considered activated. If they don't exist they are
    //  created, and set as active.
    //--The below three functions (AddSection, AddCatalogue, AddHeading) were built to be called
    //  through this function.
    ResolvePack *tResolvePack = CreateResolvePack(pDLPath);
    rCurrentSection = tResolvePack->rSection;
    rCurrentCatalogue = tResolvePack->rCatalogue;
    rCurrentHeading = tResolvePack->rHeading;

    //--Create the parts.  If any pieces were already active, they fail.
    AddSection(tResolvePack->mSection);
    AddCatalogue(tResolvePack->mCatalogue);
    AddHeading(tResolvePack->mHeading);

    //--Clean
    free(tResolvePack);
}
void DataLibrary::RegisterPointer(const char *pDLPath, void *pPointer)
{
    //--Overload, doesn't use a deletion function.
    RegisterPointer(pDLPath, pPointer, &DontDeleteThis);
}
void DataLibrary::RegisterPointer(const char *pDLPath, void *pPointer, DeletionFunctionPtr pDeletionFunction)
{
    //--Registers a pointer at the given locations set. The DataLibrary does not support the
    //  registration of NULL pointers, and will fail if that is passed.
    if(!pDLPath || !pPointer) return;

    //--Resolve
    SugarLinkedList *rHeading = GetHeading(pDLPath);
    char *tName = DataLibrary::GetPathPart(pDLPath, DL_NAME);
    if(!rHeading || !tName) return;

    //--If the entry already exists, don't do anything.
    void *rCheckEntry = rHeading->GetElementByName(tName);
    if(rCheckEntry)
    {
        pDeletionFunction(pPointer);
        return;
    }

    //--Register the Pointer
    rHeading->AddElement(tName, pPointer, pDeletionFunction);
    free(tName);
}
void *DataLibrary::RemovePointer(const char *pDLPath)
{
    //--Removes a pointer from its list.  The pointer is returned, but if the list had called for
    //  it to be deallocated, then the pointer returned will be unstable.  If not, the pointer will
    //  be valid and you are responsible for deallocating it.
    //--Returns NULL if something went wrong, or the entry wasn't found.
    if(!pDLPath) return NULL;

    //--Resolve
    SugarLinkedList *rHeading = GetHeading(pDLPath);
    char *tName = DataLibrary::GetPathPart(pDLPath, DL_NAME);
    if(!rHeading || !tName) return NULL;

    //--Get the pointer itself.
    void *rElement = rHeading->RemoveElementS(tName);
    free(tName);
    return rElement;
}

//--Activity Stack
void DataLibrary::SetActiveObject(const char *pDLPath)
{
    //--Sets the rActiveObject based on the path passed.  If the object is not found, it is set to
    //  NULL instead.
    rActiveObject = GetEntry(pDLPath);
}
void DataLibrary::PushActiveEntity()
{
    //--Overload of below, pushes NULL.
    PushActiveEntity(NULL);
}
void DataLibrary::PushActiveEntity(void *pReplacer)
{
    //--Pushes the rActiveObject back one on the Activity Stack.  If the rActiveObject was NULL
    //  then a dummy pointer is pushed onto the stack instead, as RLL's do not support NULL entries.
    //--Passing NULL is acceptable, it will place NULL on top of the Activity Stack.
    if(!rActiveObject) rActiveObject = mDummyPtr;
    mActiveEntityStack->AddElementAsHead("X|DL_ActiveEntity", rActiveObject);
    rActiveObject = pReplacer;
}
void *DataLibrary::PopActiveEntity()
{
    //--Pops off the rActiveObject and replaces it with the 0th entry on the Activity Stack.  If
    //  the stack was empty, this can be NULL.  In addition, if the mDummyPtr is spotted, the
    //  rActiveObject also becomes NULL.
    void *rReturn = rActiveObject;
    rActiveObject = mActiveEntityStack->RemoveElementI(0);
    if(rActiveObject == mDummyPtr) rActiveObject = NULL;
    return rReturn;
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//--Note:  These are private for a reason!
void DataLibrary::AddSection(const char *pName)
{
    //--Adds the specified section, and sets it as active.  If there is already a section active
    //  then fail.
    if(!pName || rCurrentSection) return;

    SugarLinkedList *nSection = new SugarLinkedList(true);
    rCurrentSection = nSection;
    mSectionList->AddElement(pName, nSection, &SugarLinkedList::DeleteThis);
}
void DataLibrary::AddCatalogue(const char *pName)
{
    //--Adds the specified catalogue to the rCurrentSection.  As above, don't call this directly,
    //  call it through AddPath().
    if(!pName || !rCurrentSection || rCurrentCatalogue) return;

    SugarLinkedList *nCatalogue = new SugarLinkedList(true);
    rCurrentCatalogue = nCatalogue;
    rCurrentSection->AddElement(pName, nCatalogue, &SugarLinkedList::DeleteThis);
}
void DataLibrary::AddHeading(const char *pName)
{
    //--Adds the specified heading to the rCurrentCatalogue.  Call this through AddPath().
    if(!pName || !rCurrentCatalogue || rCurrentHeading) return;

    SugarLinkedList *nHeading = new SugarLinkedList(true);
    rCurrentHeading = nHeading;
    rCurrentCatalogue->AddElement(pName, nHeading, &SugarLinkedList::DeleteThis);
}

//============================================ Update =============================================
//=========================================== File I/O ============================================
void DataLibrary::PrintToFile(const char *pFilePath)
{
    //--Prints the contents of the DataLibrary's directory structure.
    if(!pFilePath) return;

    //--Open it.  Write mode, don't check overwrite.
    FILE *fOutfile = fopen(pFilePath, "w");

    //--Sections
    SugarLinkedList *rSection = (SugarLinkedList *)mSectionList->PushIterator();
    while(rSection)
    {
        //--Name
        const char *rSectionName = mSectionList->GetIteratorName();

        //--Catalogues
        SugarLinkedList *rCatalogue = (SugarLinkedList *)rSection->PushIterator();
        while(rCatalogue)
        {
            //--Name
            const char *rCatalogueName = rSection->GetIteratorName();

            //--Headings
            SugarLinkedList *rHeading = (SugarLinkedList *)rCatalogue->PushIterator();
            while(rHeading)
            {
                //--Name
                const char *rHeadingName = rCatalogue->GetIteratorName();

                //--Entries
                void *rEntry = rHeading->PushIterator();
                while(rEntry)
                {
                    //--Print it
                    fprintf(fOutfile, "%s/%s/%s/%s\n", rSectionName, rCatalogueName, rHeadingName, rHeading->GetIteratorName());

                    rEntry = rHeading->AutoIterate();
                }

                //--Iterate
                rHeading = (SugarLinkedList *)rCatalogue->AutoIterate();
            }

            //--Iterate
            rCatalogue = (SugarLinkedList *)rSection->AutoIterate();
        }

        //--Iterate
        rSection = (SugarLinkedList *)mSectionList->AutoIterate();
    }

    //--Clean
    fclose(fOutfile);
}

//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
DataLibrary *DataLibrary::Fetch()
{
    return Global::Shared()->gDataLibrary;
}
const char *DataLibrary::GetGamePath(const char *pVariablePath)
{
    //--Returns a non-mutable string that is derived from the SysVar* at the location provided. If
    //  the location does not exist or is not a SysVar*, then the string returned is "NO PATH" to
    //  aid in debugging. An error message will be logged as well.
    //--A list of paths:
    //  "Root/Paths/System/Startup/sClassicModePath"
    //  "Root/Paths/System/Startup/sClassicMode3DPath"
    //  "Root/Paths/System/Startup/sCorrupterModePath"
    //  "Root/Paths/System/Startup/sAdventurePath"
    //  "Root/Paths/System/Startup/sDollManorPath"
    //  "Root/Paths/System/Startup/sElectrospriteAdventurePath"
    //  "Root/Paths/System/Startup/sMOTFPath"
    //  "Root/Paths/System/Startup/sLevelGeneratorPath"
    //  "Root/Paths/System/Startup/sPairanormalPath"
    if(!pVariablePath) return "NO PATH";

    //--Verify the DataLibrary exists.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary) return "DATALIBRARY NOT BUILT";

    //--Find the variable.
    SysVar *rPathVariable = (SysVar *)rDataLibrary->GetEntry(pVariablePath);
    if(rPathVariable)
    {
        return (const char *)rPathVariable->mAlpha;
    }

    //--Return the error, print an error message.
    fprintf(stderr, "Attempting to acquire invalid path variable: %s\n", pVariablePath);
    sprintf(gxLastIssue, "Attempting to acquire invalid path variable: %s\n", pVariablePath);
    LogError();
    return "NO PATH";
}
