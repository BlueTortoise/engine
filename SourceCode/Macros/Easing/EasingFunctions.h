//--[Easing Functions]
//--Functions that return a value ranging from 0.0f to 1.0f (and sometimes beyond) which are used
//  when moving things around on the UI to make them more natural. They are all statically encapsulated
//  within the EasingFunc class.

#pragma once

#include <math.h>

class EasingFunction
{
    public:
    //--Baseline.
    static float Linear(float pPercent);
    static float QuadraticIn(float pPercent);
    static float QuadraticOut(float pPercent);
    static float QuadraticInOut(float pPercent);
    static float QuarticIn(float pPercent);
    static float QuarticOut(float pPercent);
    static float QuarticInOut(float pPercent);
    static float SinusoidalIn(float pPercent);
    static float SinusoidalOut(float pPercent);
    static float SinusoidalInOut(float pPercent);

    //--Overloads.
    static float Linear(float pNumerator, float pDenominator);
    static float QuadraticIn(float pNumerator, float pDenominator);
    static float QuadraticOut(float pNumerator, float pDenominator);
    static float QuadraticInOut(float pNumerator, float pDenominator);
    static float QuarticIn(float pNumerator, float pDenominator);
    static float QuarticOut(float pNumerator, float pDenominator);
    static float QuarticInOut(float pNumerator, float pDenominator);
    static float SinusoidalIn(float pNumerator, float pDenominator);
    static float SinusoidalOut(float pNumerator, float pDenominator);
    static float SinusoidalInOut(float pNumerator, float pDenominator);
};
