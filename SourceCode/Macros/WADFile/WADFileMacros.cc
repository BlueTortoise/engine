//--Base
#include "WADFileMacros.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

bool CompareEight(const char *pStringA, const char *pStringB)
{
    //--Function to be used instead of strcmp, compares just like it does except does NOT assume
    //  that the string is null-terminated. Instead, the string is always 8 chars long.
    //--Returns true if the strings are identical. Also, is not case sensitive.
    for(int i = 0; i < 8; i ++)
    {
        char tUseCharA = pStringA[i];
        char tUseCharB = pStringB[i];

        if(tUseCharA >= 'a' && tUseCharA <= 'z')
        {
            tUseCharA = tUseCharA + 'A' - 'a';
        }
        if(tUseCharB >= 'a' && tUseCharB <= 'z')
        {
            tUseCharB = tUseCharB + 'A' - 'a';
        }

        if(tUseCharA != tUseCharB) return false;
    }
    return true;
}
void CopyEight(char *pDest, const char *pSrc)
{
    //--Function used instead of strcpy, copies eight characters and ignored null termination.
    if(!pDest || !pSrc) return;

    for(int i = 0; i < 8; i ++)
    {
        pDest[i] = pSrc[i];
    }
}
