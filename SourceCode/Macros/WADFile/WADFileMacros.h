//--[WADFileMacros]
//--Some functions that are useful for coding with WAD files.

#pragma once

bool CompareEight(const char *pStringA, const char *pStringB);
void CopyEight(char *pDest, const char *pSrc);
