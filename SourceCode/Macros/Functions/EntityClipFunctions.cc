//--Base
#include "EntityClipFunctions.h"

//--Classes
#include "RootEntity.h"

//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers
#include "MapManager.h"

//--[Local Definitions]
//--These definitions are used to set the location of the top/bottom slope variable. For example, if
//  your program has them in RootEntity::xBotSlope, set this to:
//  #define VAR_BOT_SLOPE RootEntity::xBotSlope
#define VAR_BOT_SLOPE true
#define VAR_TOP_SLOPE true
#define VAR_AIR_WALK true

//--[Worker Functions]
void ResetSlopeStatics()
{
    //--This function is designed to be used with the above values. If the defines are anything other
    //  than constants, this is where you reset them to their defaults after an entity has moved.
    //--This function is called, but making it actually do anything is optional.
    //VAR_BOT_SLOPE = true
    //VAR_TOP_SLOPE = true
    //VAR_AIR_WALK = true
}

//--[Remainder Functions]
void ResolveRemainderOnce(float &sSpeed, float &sRemainder, int &sMove)
{
    //--Function that doesn't use a dimensions pack, and just performs the resolve for a single move.
    //  This is used by more than generic entity movement!
    sRemainder = sRemainder + (sSpeed - sMove);
    while(sRemainder >= 1.0)
    {
        sRemainder = sRemainder - 1.0;
        sMove ++;
    }
    while(sRemainder <= -1.0)
    {
        sRemainder = sRemainder + 1.0;
        sMove --;
    }
}
void ResolveRemainders(float pXSpeed, float pYSpeed, float pZSpeed, int &sXMove, int &sYMove, int &sZMove, ThreeDimensionReal &sDimensions)
{
    //--Adds an extra movement point if the remainder matches.  This is used instead of direct
    //  floating-point mapping, since that would allow you to walk into walls.  That'd be bad.
    //--The Z data is unused in the default implementation, but may be enabled at your discretion.
    ResolveRemainderOnce(pXSpeed, sDimensions.mXRemainder, sXMove);
    ResolveRemainderOnce(pYSpeed, sDimensions.mYRemainder, sYMove);
    //ResolveRemainderOnce(pZSpeed, sDimensions.mZRemainder, sZMove);
}

//--[Movement Functions]
void MoveEntity(float pXSpeed, float pYSpeed, float pZSpeed, ThreeDimensionReal &sDimensions)
{
    //--Moves an entity according to its speeds.  The Dimensions are modified but other stuff, like
    //  hitboxes, is not, so be sure to update those after calling this.
    //--The Z component is deprecated in the current engine.
    //--To improve compatibility, booleans are used to determine which slope behaviors are legal
    //  for the entity in question. The booleans should be

    //--Fast-access pointers.
    MapManager *rMapManager = MapManager::Fetch();
    int tCheckPosition;
    int tIncrement;

    //--Get decimal-less values for movement speed.
    int tXMove = pXSpeed;
    int tYMove = pYSpeed;
    int tZMove = pZSpeed;

    //--Behavioral flags. These are set statically before the routine is run.
    bool tAllowBotSlope = VAR_BOT_SLOPE;
    bool tAllowTopSlope = VAR_TOP_SLOPE;

    //--Add an extra movement for remainders, where applicable.
    ResolveRemainders(pXSpeed, pYSpeed, pZSpeed, tXMove, tYMove, tZMove, sDimensions);

    //--Movement loop, auto-caps at 50 to prevent accidentally locking.  It will print an error if
    //  it locks, indicating something is off in the clipping.
    int tMoveCap = 0;
    bool tIsDone = false;
    while(!tIsDone && tMoveCap < 100)
    {
        //--Refresh if the top or bottom is blocked. The opposing slope is illegal if they are. That
        //  is, we cannot go up a slope on the ground if we're already touching the ceiling.
        tAllowBotSlope = VAR_BOT_SLOPE && !rMapManager->IsTopBlocked(sDimensions);
        tAllowTopSlope = VAR_TOP_SLOPE && !rMapManager->IsBotBlocked(sDimensions);

        //--[X Axis Setup]
        tCheckPosition = 0;
        tIncrement = 0;
        if(tXMove > 0)
        {
            tCheckPosition = (int)sDimensions.mRgt;
            tIncrement     = 1;
        }
        else if(tXMove < 0)
        {
            tCheckPosition = (int)sDimensions.mLft - 1;
            tIncrement     = -1;
        }

        //--[X Axis Movement]
        if(tIncrement != 0)
        {
            //--Normal
            if(rMapManager->CanMoveToH(tCheckPosition, sDimensions.mTop, sDimensions.mBot))
            {
                //--Is "Air-Walking" allowed?  If no, we must maintain contact with the ground to move forwards.
                if(!VAR_AIR_WALK)
                {
                    //--But, if bottom-sloping is allowed, we have to be able to walk into the air a bit!
                    //  Therefore, we need to see if the slope is over m=1.  If it's too steep, based on
                    //  our width, then we can't move out.
                    if(tAllowBotSlope)
                    {
                        float tWidth = sDimensions.mRgt - sDimensions.mLft;
                        if(rMapManager->GetClipAt(tCheckPosition, sDimensions.mBot))
                        {
                            sDimensions.mLft     = sDimensions.mLft     + tIncrement;
                            sDimensions.mRgt     = sDimensions.mRgt     + tIncrement;
                            sDimensions.mXCenter = sDimensions.mXCenter + tIncrement;
                        }
                        else if(rMapManager->GetClipAt(tCheckPosition, sDimensions.mBot + tWidth - 1))
                        {
                            sDimensions.mLft     = sDimensions.mLft     + tIncrement;
                            sDimensions.mRgt     = sDimensions.mRgt     + tIncrement;
                            sDimensions.mXCenter = sDimensions.mXCenter + tIncrement;
                        }
                    }
                    //--If air-walking and bottom-sloping are both disabled, then only if contacting
                    //  the ground are we allowed to move.
                    else if(rMapManager->GetClipAt(tCheckPosition, sDimensions.mBot))
                    {
                        sDimensions.mLft     = sDimensions.mLft     + tIncrement;
                        sDimensions.mRgt     = sDimensions.mRgt     + tIncrement;
                        sDimensions.mXCenter = sDimensions.mXCenter + tIncrement;
                    }
                }
                //--Air-walking is enabled, so go ahead as normal.
                else
                {
                    sDimensions.mLft     = sDimensions.mLft     + tIncrement;
                    sDimensions.mRgt     = sDimensions.mRgt     + tIncrement;
                    sDimensions.mXCenter = sDimensions.mXCenter + tIncrement;
                }
            }
            //--Upward Slope (m = 1 pixel)
            else if(tAllowBotSlope && rMapManager->CanMoveToH(tCheckPosition, sDimensions.mTop, sDimensions.mBot - 1))
            {
                sDimensions.mLft     = sDimensions.mLft     + tIncrement;
                sDimensions.mRgt     = sDimensions.mRgt     + tIncrement;
                sDimensions.mXCenter = sDimensions.mXCenter + tIncrement;
                sDimensions.mTop     = sDimensions.mTop     - 1;
                sDimensions.mBot     = sDimensions.mBot     - 1;
                sDimensions.mYCenter = sDimensions.mYCenter - 1;

                //--Turn sloping off for the duration.  This saves resources since Downward Ground
                //  Slope cannot possibly trigger on the same pass as an Upward Slope!
                tAllowBotSlope = false;
            }
            //--Downward Ceiling Slope (m = 1 pixel)
            else if(tAllowTopSlope && rMapManager->CanMoveToH(tCheckPosition, sDimensions.mTop + 1, sDimensions.mBot))
            {
                sDimensions.mLft     = sDimensions.mLft     + tIncrement;
                sDimensions.mRgt     = sDimensions.mRgt     + tIncrement;
                sDimensions.mXCenter = sDimensions.mXCenter + tIncrement;
                sDimensions.mTop     = sDimensions.mTop     + 1;
                sDimensions.mBot     = sDimensions.mBot     + 1;
                sDimensions.mYCenter = sDimensions.mYCenter + 1;
            }

            //--Downward Ground Slope (m = 1 pixel).  This is done *after* a successful move.
            if(tAllowBotSlope && !rMapManager->IsTouchingGround(sDimensions))
            {
                //--Simulate moving down one pixel
                sDimensions.mBot = sDimensions.mBot + 1;

                //--Are we now touching the ground?  If so, finish the move.
                if(rMapManager->IsTouchingGround(sDimensions))
                {
                    sDimensions.mTop     = sDimensions.mTop     + 1;
                    sDimensions.mYCenter = sDimensions.mYCenter + 1;
                }
                //--We aren't, so un-simulate the movement.
                else
                {
                    sDimensions.mBot = sDimensions.mBot - 1;
                }
            }

            //--Increment towards zero.
            tXMove -= tIncrement;
        }

        //--[Y Axis Setup]
        tCheckPosition = 0;
        tIncrement = 0;
        if(tYMove > 0)
        {
            tCheckPosition = (int)sDimensions.mBot;
            tIncrement = 1;
        }
        else if(tYMove < 0)
        {
            tCheckPosition = (int)sDimensions.mTop - 1;
            tIncrement = -1;
        }

        //--[Y Axis Movement]
        if(tIncrement != 0)
        {
            //--Normal
            if(rMapManager->CanMoveToV(tCheckPosition, sDimensions.mLft, sDimensions.mRgt))
            {
                sDimensions.mTop     = sDimensions.mTop     + tIncrement;
                sDimensions.mBot     = sDimensions.mBot     + tIncrement;
                sDimensions.mYCenter = sDimensions.mYCenter + tIncrement;
            }
            else
            {
            }
            tYMove -= tIncrement;
        }

        //--Lock checking
        tMoveCap ++;
        if(tMoveCap >= 100) fprintf(stderr, "Movement loop got caught\n");
        if(tXMove == 0 && tYMove == 0 && tZMove == 0) tIsDone = true;
    }

    //--Now that movement is over, reset any static variables back to their defaults. This prevents
    //  entities that rely on using default values from behaving oddly if the previous entity changed
    //  a flag on them.
    ResetSlopeStatics();
}
