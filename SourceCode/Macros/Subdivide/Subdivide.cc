//--Base
#include "Subdivide.h"

//--Classes
//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers

//=========================================== System ==============================================
Subdivide::Subdivide()
{
    //--Constructor is private and the class cannot be instantiated.
}

//--Splitting
char *Subdivide::SubdivideString(int &sCharsParsed, const char *pBaseString, int pCharacterLimit, float pPixelLimit, SugarFont *pFont, float pFontScale)
{
    //--Given a pBaseString, splits that string based on its length. That is, cuts off letters from the end of the string
    //  if the string goes over the provided lengths, starting at the last space before the cutting word.
    //--If there is no space in the entire string, cuts at the last character before the limit.
    //--pCharacter limit sets a hard memory size on the string. Alternately, a font may be provided and the string's
    //  length will be tested using that font against pPixelLimit. If pCharacterLimit is -1, it is not tested,
    //  and if pPixelLimit is -1 or the font is invalid, that will not be tested. If neither limit is provided, the
    //  base string is returned.
    //--The return value is always a heap-allocated string of at least one character. sCharsParsed will represent how many
    //  characters were parsed, and trailing spaces will be parsed but not added to the string.
    if(!pBaseString) return InitializeString(" ");

    //--Check that one of the limits was provided. If not, just return a new copy of the base string.
    if(pCharacterLimit < 1 && (!pFont || pPixelLimit < 1.0f || pFontScale == 0.0f)) return InitializeString(pBaseString);

    //--Set the character parse to 0.
    bool tAtLeastOneNormalLetter = false;
    sCharsParsed = 0;

    //--Setup variables.
    bool tIsCheckingFont = (pFont != NULL && pPixelLimit >= 1.0f && pFontScale > 0.0f);
    int tOriginalLen = (int)strlen(pBaseString);
    SugarAutoBuffer *tCopyBuf = new SugarAutoBuffer();

    //--Begin parsing.
    bool tHitEndNormally = false;
    while(true)
    {
        //--If the character is a '\n', move to the next line immediately.
        if(pBaseString[sCharsParsed] == '\n')
        {
            sCharsParsed ++;
            tHitEndNormally = true;
            break;
        }
        else if(sCharsParsed < tOriginalLen + 1 && pBaseString[sCharsParsed] == '\\' && pBaseString[sCharsParsed+1] == 'n')
        {
            sCharsParsed += 2;
            tHitEndNormally = true;
            break;
        }
        //--Normal case. Append the character.
        else
        {
            tAtLeastOneNormalLetter = true;
            tCopyBuf->AppendCharacter(pBaseString[sCharsParsed]);
        }

        //--Move the cursor up.
        sCharsParsed ++;

        //--Edge check: End of string.
        if(sCharsParsed >= tOriginalLen)
        {
            tHitEndNormally = true;
            break;
        }

        //--Edge check: Character limit.
        if(pCharacterLimit > 0 && sCharsParsed >= pCharacterLimit)
        {
            break;
        }

        //--Edge check: Font length.
        if(tIsCheckingFont)
        {
            //--Turn this into a string temporarily.
            tCopyBuf->AppendNull();
            float cLength = pFont->GetTextWidth((const char *)tCopyBuf->GetRawData()) * pFontScale;

            //--Undo the string conversion.
            tCopyBuf->Seek(-1);

            //--Check the limit.
            if(cLength >= pPixelLimit) break;
        }
    }

    //--Hit the end without at least one valid letter.
    if(!tAtLeastOneNormalLetter)
    {
        delete tCopyBuf;
        return InitializeString(" ");
    }

    //--Turn the copybuffer into a usable string, and deallocate the original buffer.
    tCopyBuf->AppendNull();
    char *nFinalString = (char *)tCopyBuf->LiberateData();
    delete tCopyBuf;

    //--If the end was not hit normally, we need to trim the final string.
    if(!tHitEndNormally)
    {
        //--Now parse backwards to the last space.
        int tCutoffLetter = -1;
        for(int i = (int)strlen(nFinalString) - 1; i >= 0; i --)
        {
            if(nFinalString[i] == ' ')
            {
                tCutoffLetter = i+1;
                break;
            }
        }

        //--If the cutoff letter is -1, then no spaces were found. Just hard-break on the letter.
        if(tCutoffLetter == -1)
        {
            nFinalString[strlen(nFinalString)] = '\0';
            sCharsParsed = strlen(nFinalString);
        }
        //--Otherwise, cutoff on the space.
        else
        {
            //--Cut the buffer.
            nFinalString[tCutoffLetter - 1] = '\0';
            sCharsParsed = tCutoffLetter;
        }
    }

    //--Pass back the new string.
    return nFinalString;
}

char *Subdivide::SubdivideStringBraces(int &sCharsParsed, const char *pBaseString, int pCharacterLimit, float pPixelLimit, SugarFont *pFont, float pFontScale)
{
    //--Same as above variant, but uses a "brace stack" to handle [tags like this]. These tags don't get appended to the final version but
    //  are left in the original string, to be used for dialogue cues.
    if(!pBaseString) return InitializeString(" ");

    //--Check that one of the limits was provided. If not, just return a new copy of the base string.
    if(pCharacterLimit < 1 && (!pFont || pPixelLimit < 1.0f || pFontScale == 0.0f)) return InitializeString(pBaseString);

    //--Set the character parse to 0.
    sCharsParsed = 0;

    //--Setup variables.
    bool tIsCheckingFont = (pFont != NULL && pPixelLimit >= 1.0f && pFontScale > 0.0f);
    int tOriginalLen = (int)strlen(pBaseString);
    SugarAutoBuffer *tCopyBuf = new SugarAutoBuffer();

    //--Brace stack.
    int tBraceLettersSkipped = 0;
    int tBraceStack = 0;

    //--Begin parsing.
    bool tHitEndNormally = false;
    while(true)
    {
        //--If the character is a '\n', move to the next line immediately.
        if(pBaseString[sCharsParsed] == '\n')
        {
            sCharsParsed ++;
            tHitEndNormally = true;
            break;
        }
        //--Hard brace check. Once in a hard brace, ignore letters until we exit one. This is used to place tags
        //  but the tagged text doesn't go into the copied-out string.
        else if(pBaseString[sCharsParsed] == '[')
        {
            tBraceLettersSkipped ++;
            tBraceStack ++;
        }
        //--Hard brace exit.
        else if(pBaseString[sCharsParsed] == ']')
        {
            tBraceLettersSkipped ++;
            tBraceStack --;
            if(tBraceStack < 0) tBraceStack = 0;
        }
        //--Normal case. Append the character.
        else
        {
            //--If currently in a brace stack, this letter does not get appended.
            if(tBraceStack < 1)
            {
                tCopyBuf->AppendCharacter(pBaseString[sCharsParsed]);
            }
            //--Track changes.
            else
            {
                tBraceLettersSkipped ++;
            }
        }

        //--Move the cursor up.
        sCharsParsed ++;

        //--Edge check: End of string.
        if(sCharsParsed >= tOriginalLen)
        {
            tHitEndNormally = true;
            break;
        }

        //--Edge check: Character limit.
        if(pCharacterLimit > 0 && sCharsParsed >= pCharacterLimit)
        {
            break;
        }

        //--Edge check: Font length.
        if(tIsCheckingFont)
        {
            //--Turn this into a string temporarily.
            tCopyBuf->AppendNull();
            float cLength = pFont->GetTextWidth((const char *)tCopyBuf->GetRawData()) * pFontScale;

            //--Undo the string conversion.
            tCopyBuf->Seek(-1);

            //--Check the limit.
            if(cLength >= pPixelLimit) break;
        }
    }

    //--Turn the copybuffer into a usable string, and deallocate the original buffer.
    tCopyBuf->AppendNull();
    char *nFinalString = (char *)tCopyBuf->LiberateData();
    delete tCopyBuf;

    //--If the end was not hit normally, we need to trim the final string.
    if(!tHitEndNormally)
    {
        //--Now parse backwards to the last space.
        int tCutoffLetter = -1;
        for(int i = (int)strlen(nFinalString) - 1; i >= 0; i --)
        {
            if(nFinalString[i] == ' ')
            {
                tCutoffLetter = i+1;
                break;
            }
        }

        //--If the cutoff letter is -1, then no spaces were found. Just hard-break on the letter.
        if(tCutoffLetter == -1)
        {
            nFinalString[strlen(nFinalString)] = '\0';
            sCharsParsed = strlen(nFinalString) + tBraceLettersSkipped;
        }
        //--Otherwise, cutoff on the space.
        else
        {
            //--Cut the buffer.
            nFinalString[tCutoffLetter - 1] = '\0';
            sCharsParsed = tCutoffLetter + tBraceLettersSkipped;
        }
    }

    //--Pass back the new string.
    return nFinalString;
}
SugarLinkedList *Subdivide::SubdivideStringToList(const char *pString, const char *pDelimiters)
{
    //--Given a string, breaks it apart and populates a SugarLinkedList with its sub-components. The string of delimiters provides
    //  which letters cause a string break.
    //--Always returns a list, but on error, the list may have no elements. The list must be deallocated by the caller.
    SugarLinkedList *nStringList = new SugarLinkedList(true);
    if(!pString || !pDelimiters) return nStringList;

    //--Backslash '\\' is not allowed on the delimiter list.
    int cDelimiterLen = (int)strlen(pDelimiters);
    for(int i = 0; i < cDelimiterLen; i ++)
    {
        if(pDelimiters[i] == '\\') return nStringList;
    }

    //--Create a buffer that will hold strings as they are assembled. A string with no delimiters will only take that
    //  much space, though in most cases considerably less buffer size is needed.
    int cLen = (int)strlen(pString);
    char *tBuffer = (char *)starmemoryalloc(sizeof(char) * (cLen+1));

    //--Variables.
    int l = 0;
    int c = 0;
    bool tLastWasEscape = false;

    //--Break the string into a set of substrings.
    for(int i = 0; i < cLen; i ++)
    {
        //--Escape sequences not active, normal copy.
        if(!tLastWasEscape)
        {
            //--Scan across all delimiters.
            bool tWasDelimiterFound = false;
            for(int p = 0; p < cDelimiterLen; p ++)
            {
                //--Match.
                if(pString[i] == pDelimiters[p])
                {
                    tWasDelimiterFound = true;
                    break;
                }
            }

            //--Delimiter was found. Create a new substring.
            if(tWasDelimiterFound)
            {
                //--Create, register.
                char *nString = InitializeString(tBuffer);
                nStringList->AddElementAsTail("X", nString, &FreeThis);

                //--Reset.
                c = 0;
                tBuffer[0] = '\0';
            }
            //--If a backslash is found, this becomes an escape sequence, and the backslash is not copied.
            else if(pString[i] == '\\')
            {
                tLastWasEscape = true;
            }
            //--Copy.
            else
            {
                l ++;
                tBuffer[c+0] = pString[i];
                tBuffer[c+1] = '\0';
                c ++;
            }
        }
        //--Escape sequence is active. The next letter is always copied, regardless of what it is.
        else
        {
            l ++;
            tBuffer[c+0] = pString[i];
            tBuffer[c+1] = '\0';
            c ++;
            tLastWasEscape = false;
        }
    }

    //--If there's anything left in the buffer, put it in the list.
    if(tBuffer[0] != '\0')
    {
        char *nString = InitializeString(tBuffer);
        nStringList->AddElementAsTail("X", nString, &FreeThis);
    }

    //--Clean.
    free(tBuffer);
    return nStringList;
}
