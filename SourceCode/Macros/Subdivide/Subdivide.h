//--[Subdivide]
//--Subdivision functions. Takes in a string and breaks it into smaller strings. Useful for fitting lines into a console.

#pragma once

//--[Includes]
class SugarFont;
class SugarLinkedList;

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class Subdivide
{
    private:
    //--System
    Subdivide();

    protected:

    public:
    //--Splitting
    static char *SubdivideString(int &sCharsParsed, const char *pBaseString, int pCharacterLimit, float pPixelLimit, SugarFont *pFont, float pFontScale);
    static char *SubdivideStringBraces(int &sCharsParsed, const char *pBaseString, int pCharacterLimit, float pPixelLimit, SugarFont *pFont, float pFontScale);
    static SugarLinkedList *SubdivideStringToList(const char *pString, const char *pDelimiters);
};

//--Hooking Functions

