//--Base
#include "Definitions.h"
#include "Structures.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
//--Libraries
//--Managers

void RenderPercent(float pXPercent, float pYPercent, SugarBitmap *pBitmap)
{
    //--Given an image and an X/Y percent, renders that percentage of the image from the top-left. The image shortens instead
    //  of stretching to fit. This is often used to render health bars.
    if(!pBitmap || pXPercent <= 0.0f || pYPercent <= 0.0f) return;

    //--Compute pixel position.
    float cLft = pBitmap->GetXOffset();
    float cTop = pBitmap->GetYOffset();
    float cRgt = cLft + (pBitmap->GetWidth()  * pXPercent);
    float cBot = cTop + (pBitmap->GetHeight() * pYPercent);

    //--Compute texel position.
    float cTxL = 0.0f;
    float cTxT = 1.0f;
    float cTxR = pXPercent;
    float cTxB = 1.0f - pYPercent;

    //--Render.
    pBitmap->Bind();
    glBegin(GL_QUADS);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
    glEnd();
}
