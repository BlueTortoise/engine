//--Base
#include "OpenGLMacros.h"

//--Classes
//--CoreClasses
//--Definitions
#include "GlDfn.h"
#include <stdarg.h>
#include <stdio.h>

//--GUI
//--Libraries
//--Managers

//--[Public Statics]
static char xShaderNameBuf[8192];

//--[Uniform 1 Integer]
void ShaderUniform1i(GLint pShaderHandle, GLint pUniformValue, const char *pFormat)
{
    //--Uploads a uniform integer to the shader with the given name.
    GLint cHandle = sglGetUniformLocation(pShaderHandle, pFormat);
    sglUniform1i(cHandle, pUniformValue);
}
void ShaderUniform1iArg(GLint pShaderHandle, GLint pUniformValue, const char *pFormat, ...)
{
    //--Same as above, but resolves a string first. Needed for arrays.
    va_list tArgList;
    va_start(tArgList, pFormat);

    //--Print the args into a buffer.
    vsprintf(xShaderNameBuf, pFormat, tArgList);

    //--Call.
    ShaderUniform1i(pShaderHandle, pUniformValue, xShaderNameBuf);

    //--Clean up.
    va_end(tArgList);
}

//--[Uniform 1 Float]
void ShaderUniform1f(GLint pShaderHandle, GLfloat pUniformValue, const char *pFormat)
{
    //--Uploads a uniform integer to the shader with the given name.
    GLint cHandle = sglGetUniformLocation(pShaderHandle, pFormat);
    sglUniform1f(cHandle, pUniformValue);
}
void ShaderUniform1fArg(GLint pShaderHandle, GLfloat pUniformValue, const char *pFormat, ...)
{
    //--Same as above, but resolves a string first. Needed for arrays.
    va_list tArgList;
    va_start(tArgList, pFormat);

    //--Print the args into a buffer.
    vsprintf(xShaderNameBuf, pFormat, tArgList);

    //--Call.
    ShaderUniform1f(pShaderHandle, pUniformValue, xShaderNameBuf);

    //--Clean up.
    va_end(tArgList);
}

//--[Uniform 4 Floats]
void ShaderUniform4f(GLint pShaderHandle, GLfloat pUniformValue0, GLfloat pUniformValue1, GLfloat pUniformValue2, GLfloat pUniformValue3, const char *pFormat)
{
    //--Uploads four uniform floats to the shader with the given name.
    GLint cHandle = sglGetUniformLocation(pShaderHandle, pFormat);
    sglUniform4f(cHandle, pUniformValue0, pUniformValue1, pUniformValue2, pUniformValue3);
}
void ShaderUniform4fArg(GLint pShaderHandle, GLfloat pUniformValue0, GLfloat pUniformValue1, GLfloat pUniformValue2, GLfloat pUniformValue3, const char *pFormat, ...)
{
    //--Same as above, but resolves a string first. Needed for arrays.
    va_list tArgList;
    va_start(tArgList, pFormat);

    //--Print the args into a buffer.
    vsprintf(xShaderNameBuf, pFormat, tArgList);

    //--Call.
    ShaderUniform4f(pShaderHandle, pUniformValue0, pUniformValue1, pUniformValue2, pUniformValue3, xShaderNameBuf);

    //--Clean up.
    va_end(tArgList);
}
