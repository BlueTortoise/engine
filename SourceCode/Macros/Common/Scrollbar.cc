//--Base
//--Classes
//--CoreClasses
#include "SugarBitmap.h"

//--Definitions

//--Libraries
//--Managers

void RenderScrollbarFront(float pLft, float pTop, float pBot, int pLowest, int pHighest, int pMaximum, float pIndent, SugarBitmap *pImage)
{
    //--Renders the front of a scrollbar to the given position, given where it is in the list and how much of the list it occupies.
    if(!pImage || pMaximum < 1) return;

    //--Percentages.
    float cPercentTop = pLowest  / (float)pMaximum;
    float cPercentBot = pHighest / (float)pMaximum;

    //--Compute the positions of the top and bottom of the scrollbar.
    float cPositionTop = pTop + (cPercentTop * (pBot - pTop));
    float cPositionBot = pTop + (cPercentBot * (pBot - pTop));

    //--If the percentage between the top and bottom is less than the height of the scrollbar,
    //  just render the scrollbar normally.
    if(cPositionBot - cPositionTop < pImage->GetHeight())
    {
        pImage->Draw(pLft, cPositionTop);
    }
    //--Render a top, bottom, and middle.
    else
    {
        //--Setup.
        pImage->Bind();
        glBegin(GL_QUADS);

        //--Texture percent.
        float cTxInd = pIndent / (pImage->GetHeightSafe());

        //--Common.
        float cTxL = 0.0f;
        float cTxR = 1.0f;
        float cLft = pLft;
        float cRgt = cLft + pImage->GetWidth();

        //--Top section.
        float cTxT = 1.0f;
        float cTxB = 1.0f - cTxInd;
        float cTop = cPositionTop;
        float cBot = cTop + pIndent;
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);

        //--Bottom section.
        cTxT = 0.0f + cTxInd;
        cTxB = 0.0f;
        cTop = cPositionBot - pIndent;
        cBot = cPositionBot;
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);

        //--Middle section.
        cTxT = 1.0f - cTxInd;
        cTxB = cTxInd;
        cTop = cPositionTop + pIndent;
        cBot = cPositionBot - pIndent;
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);

        glEnd();
    }
}
