//--Base
#include "Structures.h"

//--Classes
//--CoreClasses
//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers

//--[EasingPack1D]
void EasingPack1D::Initialize()
{
    mTimer = 1;
    mTimerMax = 1;
    mXBgn = 0.0f;
    mXCur = 0.0f;
    mXEnd = 0.0f;
}
void EasingPack1D::Increment(int pEasingCode)
{
    //--Already at max, do nothing.
    if(mTimer >= mTimerMax) return;

    //--Increment.
    mTimer ++;

    //--Max reached, set to end values.
    if(mTimer >= mTimerMax)
    {
        mXCur = mXEnd;
    }
    //--Midway.
    else
    {
        //--Type clamp.
        if(pEasingCode < EASING_CODE_LOWEST)  pEasingCode = EASING_CODE_LOWEST;
        if(pEasingCode > EASING_CODE_HIGHEST) pEasingCode = EASING_CODE_HIGHEST;

        //--Compute percentage.
        float cPct = 0.0f;
        if(     pEasingCode == EASING_CODE_LINEAR)    cPct = EasingFunction::Linear(        mTimer, mTimerMax);
        else if(pEasingCode == EASING_CODE_QUADIN)    cPct = EasingFunction::QuadraticIn(   mTimer, mTimerMax);
        else if(pEasingCode == EASING_CODE_QUADOUT)   cPct = EasingFunction::QuadraticOut(  mTimer, mTimerMax);
        else if(pEasingCode == EASING_CODE_QUADINOUT) cPct = EasingFunction::QuadraticInOut(mTimer, mTimerMax);

        //--Set.
        mXCur = mXBgn + ((mXEnd - mXBgn) * cPct);
    }
}
void EasingPack1D::MoveTo(float pTargetX, int pTicks)
{
    //--Instant
    if(pTicks < 1)
    {
        mTimer = 1;
        mTimerMax = 1;
        mXBgn = pTargetX;
        mXCur = pTargetX;
        mXEnd = pTargetX;
    }
    //--Transition.
    else
    {
        mTimer = 0;
        mTimerMax = pTicks;
        mXBgn = mXCur;
        mXEnd = pTargetX;
    }
}

//--[EasingPack2D]
void EasingPack2D::Initialize()
{
    mTimer = 1;
    mTimerMax = 1;
    mXBgn = 0.0f;
    mXCur = 0.0f;
    mXEnd = 0.0f;
    mYBgn = 0.0f;
    mYCur = 0.0f;
    mYEnd = 0.0f;
}
void EasingPack2D::Increment(int pEasingCode)
{
    //--Already at max, do nothing.
    if(mTimer >= mTimerMax) return;

    //--Increment.
    mTimer ++;

    //--Max reached, set to end values.
    if(mTimer >= mTimerMax)
    {
        mXCur = mXEnd;
        mYCur = mYEnd;
    }
    //--Midway.
    else
    {
        //--Type clamp.
        if(pEasingCode < EASING_CODE_LOWEST)  pEasingCode = EASING_CODE_LOWEST;
        if(pEasingCode > EASING_CODE_HIGHEST) pEasingCode = EASING_CODE_HIGHEST;

        //--Compute percentage.
        float cPct = 0.0f;
        if(     pEasingCode == EASING_CODE_LINEAR)    cPct = EasingFunction::Linear(        mTimer, mTimerMax);
        else if(pEasingCode == EASING_CODE_QUADIN)    cPct = EasingFunction::QuadraticIn(   mTimer, mTimerMax);
        else if(pEasingCode == EASING_CODE_QUADOUT)   cPct = EasingFunction::QuadraticOut(  mTimer, mTimerMax);
        else if(pEasingCode == EASING_CODE_QUADINOUT) cPct = EasingFunction::QuadraticInOut(mTimer, mTimerMax);

        //--Set.
        mXCur = mXBgn + ((mXEnd - mXBgn) * cPct);
        mYCur = mYBgn + ((mYEnd - mYBgn) * cPct);
    }
}
void EasingPack2D::MoveTo(float pTargetX, float pTargetY, int pTicks)
{
    //--Instant
    if(pTicks < 1)
    {
        mTimer = 1;
        mTimerMax = 1;
        mXBgn = pTargetX;
        mXCur = pTargetX;
        mXEnd = pTargetX;
        mYBgn = pTargetY;
        mYCur = pTargetY;
        mYEnd = pTargetY;
    }
    //--Transition.
    else
    {
        mTimer = 0;
        mTimerMax = pTicks;
        mXBgn = mXCur;
        mXEnd = pTargetX;
        mYBgn = mYCur;
        mYEnd = pTargetY;
    }
}
