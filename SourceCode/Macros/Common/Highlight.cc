//--Base
//--Classes
//--CoreClasses
#include "SugarBitmap.h"

//--Definitions

//--Libraries
//--Managers

void RenderExpandableHighlight(float pLft, float pTop, float pRgt, float pBot, float pInd, SugarBitmap *pImage)
{
    ///--[Documentation and Setup]
    //--Renders a highlight image in nine pieces, automatically stretching dynamically with borders. All values are in
    //  screen-pixels, or canvas pixels.
    if(!pImage || pInd < 1.0f) return;
    pImage->Bind();

    //--Texture sizes.
    float cTxX = pInd / pImage->GetWidthSafe();
    float cTxY = pInd / pImage->GetHeightSafe();

    //--If the right is less than the image width from the left, fix the sizes. This guarantees at least the border edges render.
    if(pRgt - pLft < pInd * 2.0f) pRgt = pLft + (pInd * 2.0f);
    if(pBot - pTop < pInd * 2.0f) pBot = pTop + (pInd * 2.0f);

    ///--[Optimization]
    //--Horizontal, in pixels.
    float cHr[4];
    cHr[0] = pLft;
    cHr[1] = pLft + pInd;
    cHr[2] = pRgt - pInd;
    cHr[3] = pRgt;

    //--Vertical, in pixels.
    float cVr[4];
    cVr[0] = pTop;
    cVr[1] = pTop + pInd;
    cVr[2] = pBot - pInd;
    cVr[3] = pBot;

    //--Horizontal, in texels.
    float cTH[4];
    cTH[0] = 0.0f;
    cTH[1] = cTxX;
    cTH[2] = 1.0f - cTxX;
    cTH[3] = 1.0f;

    //--Vertical, in texels.
    float cVH[4];
    cVH[0] = 1.0f;
    cVH[1] = 1.0f - cTxY;
    cVH[2] = cTxY;
    cVH[3] = 0.0f;


    //--Start rendering.
    glBegin(GL_QUADS);

    ///--[Top]
    //--Top left corner.
    glTexCoord2f(cTH[0], cVH[0]); glVertex2f(cHr[0], cVr[0]);
    glTexCoord2f(cTH[1], cVH[0]); glVertex2f(cHr[1], cVr[0]);
    glTexCoord2f(cTH[1], cVH[1]); glVertex2f(cHr[1], cVr[1]);
    glTexCoord2f(cTH[0], cVH[1]); glVertex2f(cHr[0], cVr[1]);

    //--Top bridge.
    glTexCoord2f(cTH[1], cVH[0]); glVertex2f(cHr[1], cVr[0]);
    glTexCoord2f(cTH[2], cVH[0]); glVertex2f(cHr[2], cVr[0]);
    glTexCoord2f(cTH[2], cVH[1]); glVertex2f(cHr[2], cVr[1]);
    glTexCoord2f(cTH[1], cVH[1]); glVertex2f(cHr[1], cVr[1]);

    //--Top right corner.
    glTexCoord2f(cTH[2], cVH[0]); glVertex2f(cHr[2], cVr[0]);
    glTexCoord2f(cTH[3], cVH[0]); glVertex2f(cHr[3], cVr[0]);
    glTexCoord2f(cTH[3], cVH[1]); glVertex2f(cHr[3], cVr[1]);
    glTexCoord2f(cTH[2], cVH[1]); glVertex2f(cHr[2], cVr[1]);

    ///--[Middle]
    //--Left bridge.
    glTexCoord2f(cTH[0], cVH[1]); glVertex2f(cHr[0], cVr[1]);
    glTexCoord2f(cTH[1], cVH[1]); glVertex2f(cHr[1], cVr[1]);
    glTexCoord2f(cTH[1], cVH[2]); glVertex2f(cHr[1], cVr[2]);
    glTexCoord2f(cTH[0], cVH[2]); glVertex2f(cHr[0], cVr[2]);

    //--Center.
    glTexCoord2f(cTH[1], cVH[1]); glVertex2f(cHr[1], cVr[1]);
    glTexCoord2f(cTH[2], cVH[1]); glVertex2f(cHr[2], cVr[1]);
    glTexCoord2f(cTH[2], cVH[2]); glVertex2f(cHr[2], cVr[2]);
    glTexCoord2f(cTH[1], cVH[2]); glVertex2f(cHr[1], cVr[2]);

    //--Top right corner.
    glTexCoord2f(cTH[2], cVH[1]); glVertex2f(cHr[2], cVr[1]);
    glTexCoord2f(cTH[3], cVH[1]); glVertex2f(cHr[3], cVr[1]);
    glTexCoord2f(cTH[3], cVH[2]); glVertex2f(cHr[3], cVr[2]);
    glTexCoord2f(cTH[2], cVH[2]); glVertex2f(cHr[2], cVr[2]);

    ///--[Bottom]
    //--Left bridge.
    glTexCoord2f(cTH[0], cVH[2]); glVertex2f(cHr[0], cVr[2]);
    glTexCoord2f(cTH[1], cVH[2]); glVertex2f(cHr[1], cVr[2]);
    glTexCoord2f(cTH[1], cVH[3]); glVertex2f(cHr[1], cVr[3]);
    glTexCoord2f(cTH[0], cVH[3]); glVertex2f(cHr[0], cVr[3]);

    //--Center.
    glTexCoord2f(cTH[1], cVH[2]); glVertex2f(cHr[1], cVr[2]);
    glTexCoord2f(cTH[2], cVH[2]); glVertex2f(cHr[2], cVr[2]);
    glTexCoord2f(cTH[2], cVH[3]); glVertex2f(cHr[2], cVr[3]);
    glTexCoord2f(cTH[1], cVH[3]); glVertex2f(cHr[1], cVr[3]);

    //--Right bridge.
    glTexCoord2f(cTH[2], cVH[2]); glVertex2f(cHr[2], cVr[2]);
    glTexCoord2f(cTH[3], cVH[2]); glVertex2f(cHr[3], cVr[2]);
    glTexCoord2f(cTH[3], cVH[3]); glVertex2f(cHr[3], cVr[3]);
    glTexCoord2f(cTH[2], cVH[3]); glVertex2f(cHr[2], cVr[3]);

    //--Finish.
    glEnd();
}
