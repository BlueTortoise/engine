//--[IRenderable]
//--Interface class that specifies that the inheriting class is an object which can be rendered
//  to the screen. This interface is not abstract, it has default behaviors for objects that do
//  not do anything special.

#pragma once

#include "Definitions.h"
#include "Structures.h"

class IRenderable : public RootObject
{
    private:

    protected:

    public:
    //--System
    IRenderable();
    virtual ~IRenderable();

    //--Public Variables
    //--Property Queries
    virtual float GetDepth();
    virtual void SetDepth(float pValue);

    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    virtual void AddToRenderList(SugarLinkedList *pRenderList);
    virtual void Render();
    static int CompareRenderable(const void *pRenderableA, const void *pRenderableB);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

//--Hooking Functions

