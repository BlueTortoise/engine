//--[IResettable]
//--Interface for objects which can be reset. The reset action will always come with a string
//  informing the reset type, though some objects may not care what the string is.

#pragma once

#include "Definitions.h"
#include "Structures.h"

class IResettable
{
    private:

    protected:

    public:
    //--System
    virtual ~IResettable() {};

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    virtual void Reset(const char *pResetType) = 0;

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

//--Hooking Functions

