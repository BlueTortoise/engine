//--Base
#include "AdventureItem.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

//=========================================== System ==============================================
AdventureItem::AdventureItem()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVENTUREITEM;

    //--[AdventureItem]
    //--System
    mIsAdamantite = false;
    mIsStackable = false;
    mStackSize = 1;
    mItemUniqueID = xItemUniqueIDCounter;
    xItemUniqueIDCounter ++;
    mLocalName = InitializeString("AdventureItem");
    mDescription = NULL;
    mTopNameLine = InitializeString("AdventureItem");
    mBotNameLine = NULL;
    mHasOverrideQuality = false;
    mOverrideQuality = 0.0f;

    //--Property Lines
    mLastResolveFlag = ADITEM_PROPERTY_NONE;
    mPropertyLinesTotal = 0;
    mPropertyLines = NULL;
    mInternalType[0] = '\0';

    //--Value
    mValue = 0;

    //--Consumable
    mIsConsumable = false;

    //--Key Item
    mIsUnique = false;
    mIsKeyItem = false;

    //--Equipment
    mIsEquipment = false;
    mEquipmentAttackSound = NULL;
    mEquipmentCriticalSound = NULL;
    mEquipmentAttackAnimation = NULL;
    mEquipSlotList = NULL;
    mEquipEntityList = NULL;
    mEquipmentBonus.Zero();
    mFinalEquipBonus.Zero();
    for(int i = 0; i < ADITEM_DAMAGETYPE_TOTAL; i ++)
    {
        mEquipmentDamageTypes[i].Initialize();
    }

    //--Gem Slots
    mGemSlotsTotal = 0;
    memset(mGemSlots, 0, sizeof(AdventureItem *) * ADITEM_MAX_GEMS);

    //--Gems
    mIsGem = false;
    mOriginalGemName = NULL;
    mOriginalGemDescription = NULL;
    mGemColorCode = 0;

    //--Combat Items
    mAbilityPath = NULL;

    //--Rendering
    rIconImg = NULL;

    //--Tags
    mTagList = new SugarLinkedList(true);

    //--Public Variables
    mGemErrorFlag = false;
    rGemParent = NULL;
    rEquippingEntity = NULL;
}
AdventureItem::~AdventureItem()
{
    free(mLocalName);
    free(mTopNameLine);
    free(mBotNameLine);
    free(mDescription);
    free(mEquipmentAttackSound);
    free(mEquipmentCriticalSound);
    free(mEquipmentAttackAnimation);
    delete mEquipEntityList;
    delete mEquipSlotList;
    free(mAbilityPath);
    for(int i = 0; i < mPropertyLinesTotal; i ++) free(mPropertyLines[i]);
    free(mPropertyLines);
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++) delete mGemSlots[i];
    delete mTagList;
    free(mOriginalGemName);
    free(mOriginalGemDescription);
}

//--[Public Statics]
//--Unique ID counter for items. All items in the game get an ID unique to them, which is used to
//  track their upgrade state. The ID values can never be 0.
int AdventureItem::xItemUniqueIDCounter = 1;

//--Comparison Lines. These lines are used when displaying comparisons between two items in the equipment
//  or shop screen. The names provided are the names of the comparing items, and the lines are not rebuilt
//  if both of the items are identical. Otherwise, building them is similar to building property lines.
char *AdventureItem::xComparisonItemA = NULL;
char *AdventureItem::xComparisonItemB = NULL;
int AdventureItem::xComparisonLinesTotal = 0;
char **AdventureItem::xComparisonLines = NULL;

//====================================== Property Queries =========================================
bool AdventureItem::IsAdamantite()
{
    return mIsAdamantite;
}
bool AdventureItem::IsStackable()
{
    return mIsStackable;
}
int AdventureItem::GetStackSize()
{
    if(!mIsStackable) return 1;
    return mStackSize;
}
const char *AdventureItem::GetAbilityPath()
{
    return mAbilityPath;
}
uint32_t AdventureItem::GetItemUniqueID()
{
    return mItemUniqueID;
}
const char *AdventureItem::GetName()
{
    return (const char *)mLocalName;
}
const char *AdventureItem::GetTopName()
{
    return (const char *)mTopNameLine;
}
const char *AdventureItem::GetBotName()
{
    return (const char *)mBotNameLine;
}
const char *AdventureItem::GetDescription()
{
    return (const char *)mDescription;
}
int AdventureItem::GetValue()
{
    //--Not a gem, return base.
    if(!mIsGem) return mValue;

    //--Sum up the value of all gems inside this one if it's a gem.
    int tValue = mValue;
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        if(mGemSlots[i]) tValue = tValue + mGemSlots[i]->GetValue();
    }
    return tValue;
}
int AdventureItem::GetGemSlots()
{
    return mGemSlotsTotal;
}
bool AdventureItem::IsUnique()
{
    return mIsUnique;
}
bool AdventureItem::IsKeyItem()
{
    return mIsKeyItem;
}
SugarBitmap *AdventureItem::GetIconImage()
{
    return rIconImg;
}
const char *AdventureItem::GetItemTypeString()
{
    return mInternalType;
}
int AdventureItem::GetTagCount(const char *pTag)
{
    int *rTag = (int *)mTagList->GetElementByName(pTag);
    if(!rTag) return 0;
    return *rTag;
}

//========================================= Manipulators ==========================================
void AdventureItem::SetAdamantite()
{
    mIsAdamantite = true;
    strcpy(mInternalType, "Crafting Item");
}
void AdventureItem::SetStackable(bool pIsStackable)
{
    mIsStackable = pIsStackable;
}
void AdventureItem::SetStackSize(int pSize)
{
    mStackSize = pSize;
    if(mStackSize < 1) mStackSize = 1;
}
void AdventureItem::SetAbilityPath(const char *pPath)
{
    ResetString(mAbilityPath, pPath);
}
void AdventureItem::OverrideID(uint32_t pID)
{
    //--Note: Should only be used when loading a save file!
    mItemUniqueID = pID;
}
void AdventureItem::SetName(const char *pName)
{
    //--Error check.
    if(!pName) return;

    //--Base name.
    ResetString(mLocalName, pName);
    //fprintf(stderr, "Setting name %s\n", mLocalName);

    //--Look for spaces.
    int tSpaceIsOn = -1;
    int tLen = (int)strlen(mLocalName);
    for(int i = 0; i < tLen-1; i ++)
    {
        //--Not a space, ignore.
        if(mLocalName[i] != ' ') continue;

        //--Found a space. Is it the second one? If so, fail. Too many spaces means the names
        //  need to be set manually.
        if(tSpaceIsOn != -1)
        {
            ResetString(mTopNameLine, pName);
            ResetString(mBotNameLine, NULL);
            //fprintf(stderr, " Discarded.\n");
            return;
        }

        //--First space we've found. Split the string here. First, clean up.
        tSpaceIsOn = i;
        ResetString(mTopNameLine, NULL);
        ResetString(mBotNameLine, NULL);

        //--Allocate space for the top line.
        SetMemoryData(__FILE__, __LINE__);
        mTopNameLine = (char *)starmemoryalloc(sizeof(char) * (i+1));
        strncpy(mTopNameLine, mLocalName, i);
        mTopNameLine[i] = '\0';

        //--Bottom line is simpler.
        ResetString(mBotNameLine, &mLocalName[i+1]);
        //fprintf(stderr, " Splitter: %s - |%s|%s|\n", mLocalName, mTopNameLine, mBotNameLine);
    }

    //--If no spaces get found, the top name is the same as the local name.
    if(tSpaceIsOn == -1)
    {
        ResetString(mTopNameLine, mLocalName);
    }
}
void AdventureItem::SetNameLines(const char *pTop, const char *pBot)
{
    if(pTop && strcasecmp(pTop, "Null"))
    {
        ResetString(mTopNameLine, pTop);
    }
    else
    {
        ResetString(mTopNameLine, NULL);
    }
    if(pBot && strcasecmp(pBot, "Null"))
    {
        ResetString(mBotNameLine, pBot);
    }
    else
    {
        ResetString(mBotNameLine, NULL);
    }
}
void AdventureItem::SetDescription(const char *pDescription)
{
    ResetString(mDescription, pDescription);
}
void AdventureItem::SetValue(int pValue)
{
    mValue = pValue;
}
void AdventureItem::SetUnique(bool pFlag)
{
    mIsUnique = pFlag;
}
void AdventureItem::SetKeyItem(bool pFlag)
{
    mIsKeyItem = pFlag;
}
void AdventureItem::SetConsumable(bool pFlag)
{
    mIsConsumable = pFlag;
}
void AdventureItem::SetEquipmentFlag(bool pFlag)
{
    //--Store the old flag.
    bool tOldFlag = mIsEquipment;

    //--Set the new flag.
    mIsEquipment = pFlag;

    //--Initialization case:
    if(!tOldFlag && mIsEquipment)
    {
        mEquipEntityList = new SugarLinkedList(false);
        mEquipSlotList = new SugarLinkedList(false);
    }
    //--Dealloc case:
    else
    {
        delete mEquipEntityList;
        mEquipEntityList = NULL;
        delete mEquipSlotList;
        mEquipSlotList = NULL;
    }

    //--Type flag.
    strcpy(mInternalType, "Equipment");
}
void AdventureItem::SetIconImage(const char *pPath)
{
    rIconImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
AdventureItem *AdventureItem::PlaceGemInSlot(int pSlot, AdventureItem *pGem)
{
    //--Removes the existing gem from the slot and passes it back, replacing it with the new
    //  gem. Takes ownership of the new gem, and the caller takes ownership of the returned gem.
    //--If the mGemErrorFlag is set to true, something went wrong and *ownership does not change*.
    //--It is legal to pass NULL in to remove a gem. It is also legal to receive NULL back
    //  if the gem slot was empty.
    mGemErrorFlag = false;

    //--Range check.
    if(pSlot < 0 || pSlot >= ADITEM_MAX_GEMS) { mGemErrorFlag = true; return NULL; }
    if(pSlot >= mGemSlotsTotal) { mGemErrorFlag = true; return NULL; }

    //--Take the current gem in the slot out. It can be NULL.
    AdventureItem *rPreviousGem = mGemSlots[pSlot];
    mGemSlots[pSlot] = pGem;

    //--If the previous gem exists, remove its parent point.
    if(rPreviousGem)
    {
        RemoveItemFromProperties(rPreviousGem);
        rPreviousGem->rGemParent = NULL;
    }

    //--If the new gem exists, set us as its parent.
    if(mGemSlots[pSlot])
    {
        AddItemToProperties(mGemSlots[pSlot]);
        mGemSlots[pSlot]->rGemParent = this;
    }

    //--Pass back the previous gem. It can be NULL, in which case the slot was empty.
    return rPreviousGem;
}
AdventureItem *AdventureItem::RemoveGemFromSlot(int pSlot)
{
    //--Removes the gem from the slot. Just calls PlaceGemInSlot() with NULL. Error flag rules
    //  are the same as before.
    return PlaceGemInSlot(pSlot, NULL);
}
void AdventureItem::AddItemToProperties(AdventureItem *pItem)
{
    //--When an item is added to a socket, this function modifies the state of the caller to add
    //  the newly socketed item's properties together. This means the parent gains the health, damage,
    //  accuracy, evade, etc of the socketed item.
    //--When the item is removed, RemoveItemFromProperties() should be called.
    //--When upgrading, call the remove and then re-add the item.
    if(!pItem) return;

    //--Flag to mark this for property re-resolve.
    mLastResolveFlag = ADITEM_PROPERTY_NONE;
}
void AdventureItem::RemoveItemFromProperties(AdventureItem *pItem)
{
    //--Reverse of AddItemToProperties(). Used when unsocketing something.
    if(!pItem) return;

    //--Flag to mark this for property re-resolve.
    mLastResolveFlag = ADITEM_PROPERTY_NONE;
}
void AdventureItem::SetOverrideQuality(float pQuality)
{
    mHasOverrideQuality = pQuality;
    mOverrideQuality = pQuality;
}
void AdventureItem::AddTag(const char *pTag)
{
    //--Tag already exists, increment it instead.
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(rInteger)
    {
        (*rInteger) = (*rInteger) + 1;
    }
    //--Create.
    else
    {
        int *nInteger = (int *)starmemoryalloc(sizeof(int));
        *nInteger = 1;
        mTagList->AddElement(pTag, nInteger, &FreeThis);
    }
}
void AdventureItem::RemoveTag(const char *pTag)
{
    //--Tag doesn't exist, do nothing:
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(!rInteger) return;

    //--Greater than 1.
    if(*rInteger > 1)
    {
        (*rInteger) = (*rInteger) - 1;
    }
    //--Zero it off. Remove it.
    else
    {
        mTagList->RemoveElementS(pTag);
    }
}

//========================================= Core Methods ==========================================
AdventureItem *AdventureItem::Clone()
{
    //--Clones the item, creating an exact duplicate.
    AdventureItem *nClone = new AdventureItem();

    //--System
    nClone->mIsAdamantite = mIsAdamantite;
    nClone->mIsStackable = mIsStackable;
    nClone->mStackSize = 1;
    ResetString(nClone->mLocalName, mLocalName);
    ResetString(nClone->mDescription, mDescription);
    ResetString(nClone->mTopNameLine, mTopNameLine);
    ResetString(nClone->mBotNameLine, mBotNameLine);

    //--Property Lines
    nClone->mLastResolveFlag = mLastResolveFlag;
    nClone->mPropertyLinesTotal = mPropertyLinesTotal;
    SetMemoryData(__FILE__, __LINE__);
    nClone->mPropertyLines = (char **)starmemoryalloc(sizeof(char *) * mPropertyLinesTotal);
    for(int i = 0; i < mPropertyLinesTotal; i ++)
    {
        nClone->mPropertyLines[i] = InitializeString(mPropertyLines[i]);
    }

    //--Value
    nClone->mValue = mValue;

    //--Consumable
    nClone->mIsConsumable = mIsConsumable;

    //--Gem Slots
    nClone->mGemSlotsTotal = mGemSlotsTotal;
    memset(mGemSlots, 0, sizeof(AdventureItem *) * ADITEM_MAX_GEMS);

    //--Key Item
    nClone->mIsUnique = mIsUnique;
    nClone->mIsKeyItem = mIsKeyItem;

    //--Equipment
    nClone->mIsEquipment = mIsEquipment;
    if(mEquipEntityList)
    {
        nClone->mEquipEntityList = new SugarLinkedList(false);
        void *rDummyPtr = mEquipEntityList->PushIterator();
        while(rDummyPtr)
        {
            nClone->mEquipEntityList->AddElement(mEquipEntityList->GetIteratorName(), rDummyPtr);
            mEquipEntityList->AutoIterate();
        }
    }
    if(mEquipSlotList)
    {
        nClone->mEquipSlotList = new SugarLinkedList(false);
        void *rDummyPtr = mEquipSlotList->PushIterator();
        while(rDummyPtr)
        {
            nClone->mEquipSlotList->AddElement(mEquipSlotList->GetIteratorName(), rDummyPtr);
            mEquipSlotList->AutoIterate();
        }
    }
    memcpy(&nClone->mEquipmentBonus,  &mEquipmentBonus,  sizeof(mEquipmentBonus));
    memcpy(&nClone->mFinalEquipBonus, &mFinalEquipBonus, sizeof(mFinalEquipBonus));
    for(int i = 0; i < ADITEM_DAMAGETYPE_TOTAL; i ++)
    {
        memcpy(&nClone->mEquipmentDamageTypes[i], &mEquipmentDamageTypes[i], sizeof(mEquipmentDamageTypes[i]));
    }

    //--Combat Items
    ResetString(nClone->mAbilityPath, mAbilityPath);

    //--Rendering
    nClone->rIconImg = rIconImg;

    //--All done.
    return nClone;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
AdventureItem *AdventureItem::GetGemInSlot(int pSlot)
{
    //--Note: It is legal to receive NULL back on error, OR empty slot. Attempting to get a gem
    //  past the allowable gem slot always returns NULL since it's logically impossible to cram
    //  a gem in there.
    if(pSlot < 0 || pSlot >= ADITEM_MAX_GEMS) return NULL;
    return mGemSlots[pSlot];
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void AdventureItem::HookToLuaState(lua_State *pLuaState)
{
    /* //--[System]
       AdItem_GetProperty("Name") (1 String)
       AdItem_GetProperty("Quantity") (1 Integer)

       //--[Equipment Properties]
       AdItem_GetProperty("Damage Type", iCategory, iSlot) (1 Float)
       AdItem_GetProperty("Attack Animation") (1 String)
       AdItem_GetProperty("Attack Sound") (1 String)
       AdItem_GetProperty("Critical Sound") (1 String)
       AdItem_GetProperty("Ability Path") (1 String)

       //--[Gem Properties]
       AdItem_GetProperty("Is Gem") (1 Boolean)
       AdItem_GetProperty("Base Gem Name") (1 String)
       AdItem_GetProperty("Gem Colors") (1 Integer)
       AdItem_GetProperty("Subgem Name", iSlot) (1 String)
       Returns the requested property from the active AdventureItem. */
    lua_register(pLuaState, "AdItem_GetProperty", &Hook_AdItem_GetProperty);

    /* //--[General]
       AdItem_SetProperty("Name", sName)
       AdItem_SetProperty("Is Adamantite")
       AdItem_SetProperty("Description", sDescription)
       AdItem_SetProperty("Is Consumable", bFlag)
       AdItem_SetProperty("Is Gem", uiGemColor)
       AdItem_SetProperty("Is Key Item", bFlag)
       AdItem_SetProperty("Is Unique", bFlag)
       AdItem_SetProperty("Is Equipment", bFlag)
       AdItem_SetProperty("Rendering Image", sDLPath)

       //--[Equippable]
       AdItem_SetProperty("Add Equippable Character", sName)
       AdItem_SetProperty("Add Equippable Slot", sSlotName)
       AdItem_SetProperty("Gem Slots", iTotal)
       AdItem_SetProperty("Equipment Attack Animation", sAnimation)
       AdItem_SetProperty("Equipment Attack Sound", sSound)
       AdItem_SetProperty("Statistic", iSlot, iValue)
       AdItem_SetProperty("Damage Type", iCategory, iSlot, fValue)

       //--[Tags]
       AdItem_SetProperty("Add Tag", sTagName, iCount)
       AdItem_SetProperty("Remove Tag", sTagName, iCount)

       //--[Combat Item]
       AdItem_SetProperty("Create Action") (Pushes Activity Stack)

       //--[Gem]
       AdItem_SetProperty("Autogenerate Gem Description")

       Sets the requested property in the active AdventureItem. */
    lua_register(pLuaState, "AdItem_SetProperty", &Hook_AdItem_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdItem_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdItem_GetProperty("Name") (1 String)
    //AdItem_GetProperty("Quantity") (1 Integer)

    //--[Equipment Properties]
    //AdItem_GetProperty("Has Gem In Slot", iSlot) (1 Boolean)
    //AdItem_GetProperty("Base Statistic", iSlot) (1 Integer)
    //AdItem_GetProperty("Final Statistic", iSlot) (1 Integer)
    //AdItem_GetProperty("Damage Type", iCategory, iSlot) (1 Float)
    //AdItem_GetProperty("Attack Animation") (1 String)
    //AdItem_GetProperty("Attack Sound") (1 String)
    //AdItem_GetProperty("Critical Sound") (1 String)
    //AdItem_GetProperty("Ability Path") (1 String)

    //--[Gem Properties]
    //AdItem_GetProperty("Is Gem") (1 Boolean)
    //AdItem_GetProperty("Base Gem Name") (1 String)
    //AdItem_GetProperty("Gem Colors") (1 Integer)
    //AdItem_GetProperty("Subgem Name", iSlot) (1 String)

    ///--[Argument Check]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdItem_GetProperty");

    //--Active object.
    AdventureItem *rItem = (AdventureItem *)DataLibrary::Fetch()->rActiveObject;
    if(!rItem || !rItem->IsOfType(POINTER_TYPE_ADVENTUREITEM)) return LuaTypeError("AdItem_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[System]
    //--Name.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        lua_pushstring(L, rItem->GetName());
        tReturns = 1;
    }
    //--Quantity.
    else if(!strcasecmp(rSwitchType, "Quantity") && tArgs == 1)
    {
        lua_pushinteger(L, rItem->GetStackSize());
        tReturns = 1;
    }
    ///--[Equipment Properties]
    //--Whether or not there is a gem in the given slot.
    else if(!strcasecmp(rSwitchType, "Has Gem In Slot") && tArgs == 2)
    {
        AdventureItem *rCheckGem = rItem->GetGemInSlot(lua_tointeger(L, 2));
        if(rCheckGem)
            lua_pushboolean(L, true);
        else
            lua_pushboolean(L, false);
        tReturns = 1;
    }
    //--Returns a statistic for the piece of equipment before gems are added.
    else if(!strcasecmp(rSwitchType, "Base Statistic") && tArgs == 2)
    {
        CombatStatistics *rStatPack = rItem->GetEquipStatistics();
        lua_pushinteger(L, rStatPack->GetStatByIndex(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Returns a statistic for the piece of equipment after gems are added.
    else if(!strcasecmp(rSwitchType, "Final Statistic") && tArgs == 2)
    {
        CombatStatistics *rStatPack = rItem->GetFinalStatistics();
        lua_pushinteger(L, rStatPack->GetStatByIndex(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Damage factor for the given type and category.
    else if(!strcasecmp(rSwitchType, "Damage Type") && tArgs == 3)
    {
        lua_pushnumber(L, rItem->GetDamageType(lua_tointeger(L, 2), lua_tointeger(L, 3)));
        tReturns = 1;
    }
    //--String representing the attack animation to use.
    else if(!strcasecmp(rSwitchType, "Attack Animation") && tArgs == 1)
    {
        lua_pushstring(L, rItem->GetEquipmentAttackAnimation());
        tReturns = 1;
    }
    //--String representing the attack sound to use.
    else if(!strcasecmp(rSwitchType, "Attack Sound") && tArgs == 1)
    {
        lua_pushstring(L, rItem->GetEquipmentAttackSound());
        tReturns = 1;
    }
    //--String representing the critical strike sound to use.
    else if(!strcasecmp(rSwitchType, "Critical Sound") && tArgs == 1)
    {
        lua_pushstring(L, rItem->GetEquipmentCriticalSound());
        tReturns = 1;
    }
    //--Path to the on-equip ability.
    else if(!strcasecmp(rSwitchType, "Ability Path") && tArgs == 1)
    {
        const char *rPath = rItem->GetAbilityPath();
        if(rPath)
            lua_pushstring(L, rPath);
        else
            lua_pushstring(L, "Null");
        tReturns = 1;
    }
    ///--[Gem Properties]
    //--Returns true if the item is a gem, false if not.
    else if(!strcasecmp(rSwitchType, "Is Gem") && tArgs == 1)
    {
        lua_pushboolean(L, rItem->IsGem());
        tReturns = 1;
    }
    //--Returns the gem's original name before merging.
    else if(!strcasecmp(rSwitchType, "Base Gem Name") && tArgs == 1)
    {
        lua_pushstring(L, rItem->GetGemBaseName());
        tReturns = 1;
    }
    //--Returns the colors of the gem and all its subtypes.
    else if(!strcasecmp(rSwitchType, "Gem Colors") && tArgs == 1)
    {
        lua_pushinteger(L, rItem->GetGemColors());
        tReturns = 1;
    }
    //--Name of the gem slotted in the given slot. Can be used for equipment as well.
    else if(!strcasecmp(rSwitchType, "Subgem Name") && tArgs == 2)
    {
        AdventureItem *rSubgem = rItem->GetGemInSlot(lua_tointeger(L, 2));
        if(rSubgem)
            lua_pushstring(L, rSubgem->GetGemBaseName());
        else
            lua_pushstring(L, "Null");
        tReturns = 1;
    }
    ///--[Error]
    else
    {
        LuaPropertyError("AdItem_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_AdItem_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[General]
    //AdItem_SetProperty("Name", sName)
    //AdItem_SetProperty("Name Lines", sTopName, sBotName)
    //AdItem_SetProperty("Is Adamantite")
    //AdItem_SetProperty("Is Stackable")
    //AdItem_SetProperty("Description", sDescription)
    //AdItem_SetProperty("Value", iValue)
    //AdItem_SetProperty("Is Consumable", bFlag)
    //AdItem_SetProperty("Is Gem", uiGemColor)
    //AdItem_SetProperty("Is Key Item", bFlag)
    //AdItem_SetProperty("Is Unique", bFlag)
    //AdItem_SetProperty("Rendering Image", sDLPath)

    //--[Equippable]
    //AdItem_SetProperty("Is Equipment", bFlag)
    //AdItem_SetProperty("Add Equippable Character", sName)
    //AdItem_SetProperty("Add Equippable Slot", sSlotName)
    //AdItem_SetProperty("Gem Slots", iTotal)
    //AdItem_SetProperty("Equipment Attack Animation", sAnimation)
    //AdItem_SetProperty("Equipment Attack Sound", sSound)
    //AdItem_SetProperty("Equipment Critical Sound", sSound)
    //AdItem_SetProperty("Statistic", iSlot, iValue)
    //AdItem_SetProperty("Damage Type", iCategory, iSlot, fValue)
    //AdItem_SetProperty("Push Gem In Slot", iSlot) (Pushes Activity Stack)

    //--[Tags]
    //AdItem_SetProperty("Add Tag", sTagName, iCount)
    //AdItem_SetProperty("Remove Tag", sTagName, iCount)

    //--[Combat Item]
    //AdItem_SetProperty("Ability Path", sPath)

    //--[Gem]
    //AdItem_SetProperty("Autogenerate Gem Description")

    ///--[Argument Check]
    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdItem_SetProperty");

    //--Active object.
    AdventureItem *rItem = (AdventureItem *)DataLibrary::Fetch()->rActiveObject;
    if(!rItem || !rItem->IsOfType(POINTER_TYPE_ADVENTUREITEM))
    {
        return LuaTypeError("AdItem_SetProperty");
    }

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[General Properties]
    //--Display name of the item. Does not affect the search name!
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 2)
    {
        rItem->SetName(lua_tostring(L, 2));
    }
    //--Display names of the item used for UI display.
    else if(!strcasecmp(rSwitchType, "Name Lines") && tArgs == 3)
    {
        rItem->SetNameLines(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Causes the item to become Adamantite. Can only exist in shops. When purchased, becomes a crafting item.
    else if(!strcasecmp(rSwitchType, "Is Adamantite") && tArgs == 1)
    {
        rItem->SetAdamantite();
    }
    //--Items can stack together.
    else if(!strcasecmp(rSwitchType, "Is Stackable") && tArgs == 1)
    {
        rItem->SetStackable(true);
    }
    //--Description of the item.
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 2)
    {
        rItem->SetDescription(lua_tostring(L, 2));
    }
    //--How much the item is worth at shops.
    else if(!strcasecmp(rSwitchType, "Value") && tArgs == 2)
    {
        rItem->SetValue(lua_tointeger(L, 2));
    }
    //--Consumable, can be equipped in combat. Will always have at least one max charge.
    else if(!strcasecmp(rSwitchType, "Is Consumable") && tArgs == 2)
    {
        rItem->SetConsumable(lua_toboolean(L, 2));
    }
    //--Gem, item can be socketed and has a color. Can be merged based on its color.
    else if(!strcasecmp(rSwitchType, "Is Gem") && tArgs == 2)
    {
        rItem->SetIsGem(lua_tointeger(L, 2));
    }
    //--Key item. Cannot be discarded, does not preclude usage or equipping.
    else if(!strcasecmp(rSwitchType, "Is Key Item") && tArgs == 2)
    {
        rItem->SetKeyItem(lua_toboolean(L, 2));
    }
    //--Unique. Will not appear in shops if the player already has one.
    else if(!strcasecmp(rSwitchType, "Is Unique") && tArgs == 2)
    {
        rItem->SetUnique(lua_toboolean(L, 2));
    }
    //--Rendering image. Which icon to use when seeing this item on the UI.
    else if(!strcasecmp(rSwitchType, "Rendering Image") && tArgs == 2)
    {
        rItem->SetIconImage(lua_tostring(L, 2));
    }
    ///--[Equipment Subsection]
    //--Marks this item as a piece of equipment which is equipped in a slot to provide stat changes.
    else if(!strcasecmp(rSwitchType, "Is Equipment") && tArgs == 2)
    {
        rItem->SetEquipmentFlag(lua_toboolean(L, 2));
    }
    //--Adds a character who can equip the item.
    else if(!strcasecmp(rSwitchType, "Add Equippable Character") && tArgs == 2)
    {
        rItem->AddEquippableCharacter(lua_tostring(L, 2));
    }
    //--Adds a slot the item can be equipped in.
    else if(!strcasecmp(rSwitchType, "Add Equippable Slot") && tArgs == 2)
    {
        rItem->AddEquippableSlot(lua_tostring(L, 2));
    }
    //--Sets how many gems this item can hold.
    else if(!strcasecmp(rSwitchType, "Gem Slots") && tArgs == 2)
    {
        rItem->SetGemCap(lua_tointeger(L, 2));
    }
    //--Which animation the weapon plays when used to attack in combat.
    else if(!strcasecmp(rSwitchType, "Equipment Attack Animation") && tArgs == 2)
    {
        rItem->SetEquipmentAttackAnimation(lua_tostring(L, 2));
    }
    //--Which sound the weapon plays when used to attack in combat.
    else if(!strcasecmp(rSwitchType, "Equipment Attack Sound") && tArgs == 2)
    {
        rItem->SetEquipmentAttackSound(lua_tostring(L, 2));
    }
    //--Which sound the weapon plays when it strikes critically.
    else if(!strcasecmp(rSwitchType, "Equipment Critical Sound") && tArgs == 2)
    {
        rItem->SetEquipmentCriticalSound(lua_tostring(L, 2));
    }
    //--Sets a statistic by its index, used for comparison and combat stats when equipped.
    else if(!strcasecmp(rSwitchType, "Statistic") && tArgs == 3)
    {
        rItem->SetStatistic(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Sets a damage type. Only used by weapons.
    else if(!strcasecmp(rSwitchType, "Damage Type") && tArgs == 4)
    {
        rItem->SetDamageType(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tonumber(L, 4));
    }
    //--Pushes the gem in the given slot, or NULL.
    else if(!strcasecmp(rSwitchType, "Push Gem In Slot") && tArgs == 2)
    {
        DataLibrary::Fetch()->PushActiveEntity(rItem->GetGemInSlot(lua_tointeger(L, 2)));
    }
    ///--[Tags]
    //--Adds the given tag the given number of times.
    else if(!strcasecmp(rSwitchType, "Add Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rItem->AddTag(lua_tostring(L, 2));
    }
    //--Removes the given tag the given number of times.
    else if(!strcasecmp(rSwitchType, "Remove Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rItem->RemoveTag(lua_tostring(L, 2));
    }
    ///--[Combat Item]
    //--Sets the path to the ability for this item.
    else if(!strcasecmp(rSwitchType, "Ability Path") && tArgs == 2)
    {
        rItem->SetAbilityPath(lua_tostring(L, 2));
    }
    ///--[Gems]
    //--Causes hybrid gems to generate their hybrid descriptions.
    else if(!strcasecmp(rSwitchType, "Autogenerate Gem Description") && tArgs == 1)
    {
        rItem->AutogenerateGemDescription();
    }
    ///--[Misc]
    //--Error.
    else
    {
        LuaPropertyError("AdItem_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
