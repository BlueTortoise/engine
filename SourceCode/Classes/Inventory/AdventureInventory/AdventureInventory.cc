//--Base
#include "AdventureInventory.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdvCombat.h"
#include "AdventureItem.h"
#include "AdventureLevel.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "LuaManager.h"
#include "MapManager.h"

//=========================================== System ==============================================
AdventureInventory::AdventureInventory()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVENTUREINVENTORY;

    //--[AdventureInventory]
    //--System
    mWasAnythingCreated = false;

    //--Paths
    mGemNameResolvePath = NULL;

    //--Crafting Materials
    mPlatina = 0;
    memset(mCraftingItemCounts, 0, sizeof(int) * CRAFT_ADAMANTITE_TOTAL);

    //--Catalysts
    memset(mCatalystCounts, 0, sizeof(int) * CATALYST_TOTAL);

    //--Upgrading
    mIsUpgradeable = false;

    //--Storage List
    mItemList = new SugarLinkedList(true);
    mExtendedItemList = new SugarLinkedList(false);
    mGemList = new SugarLinkedList(false);
    mGemMergeList = new SugarLinkedList(false);

    //--Doctor Bag
    mIsDoctorBagEnabled = false;
    mDoctorBagCharges = 150;
    mDoctorBagChargesMax = 150;

    //--Shop Buyback
    mShopBuybackList = new SugarLinkedList(true);

    //--Gems
    rMarkedEquipment = NULL;
    rMasterGem = NULL;
    rSocketingItem = NULL;

    //--Public Variables
    rLastReggedItem = NULL;
    mBlockStackingOnce = false;
}
AdventureInventory::~AdventureInventory()
{
    free(mGemNameResolvePath);
    delete mItemList;
    delete mExtendedItemList;
    delete mGemList;
    delete mGemMergeList;
    delete mShopBuybackList;
}

//--[Public Statics]
//--Item used to show properties on the forge menu. When needed, the item registration script is called
//  as normal but the item is registered to this static slot, which can be queried by the menu to
//  show its properties as compared to the original. Item should be deleted and NULL-ed externally!
AdventureItem *AdventureInventory::xUpgradeItem = NULL;

//--Image path of the last query run through AdventureLevel::xItemImageListPath.
char *AdventureInventory::xLastItemImagePath = NULL;

//====================================== Property Queries =========================================
const char *AdventureInventory::GetGemNameResolvePath()
{
    return mGemNameResolvePath;
}
int AdventureInventory::GetItemCount()
{
    return mItemList->GetListSize();
}
int AdventureInventory::GetCountOf(const char *pName)
{
    //--Error check.
    if(!pName) return 0;

    //--Adamantite Check:
    if(!strcasecmp(pName, "Adamantite Powder")) return mCraftingItemCounts[CRAFT_ADAMANTITE_POWDER];
    if(!strcasecmp(pName, "Adamantite Flakes")) return mCraftingItemCounts[CRAFT_ADAMANTITE_FLAKES];
    if(!strcasecmp(pName, "Adamantite Shard"))  return mCraftingItemCounts[CRAFT_ADAMANTITE_SHARD];
    if(!strcasecmp(pName, "Adamantite Piece"))  return mCraftingItemCounts[CRAFT_ADAMANTITE_PIECE];
    if(!strcasecmp(pName, "Adamantite Chunk"))  return mCraftingItemCounts[CRAFT_ADAMANTITE_CHUNK];
    if(!strcasecmp(pName, "Adamantite Ore"))    return mCraftingItemCounts[CRAFT_ADAMANTITE_ORE];

    //--Other items:
    int tRunningTotal = 0;
    AdventureItem *rCheckItem = (AdventureItem *)mItemList->PushIterator();
    while(rCheckItem)
    {
        //--Name check.
        if(!strcasecmp(rCheckItem->GetName(), pName))
        {
            //--Stackables just return the stack count immediately.
            if(rCheckItem->IsStackable())
            {
                mItemList->PopIterator();
                return rCheckItem->GetStackSize();
            }

            //--Otherwise, increment the total and keep going.
            tRunningTotal ++;
        }

        //--Next.
        rCheckItem = (AdventureItem *)mItemList->AutoIterate();
    }
    return tRunningTotal;
}
int AdventureInventory::GetPlatina()
{
    return mPlatina;
}
int AdventureInventory::GetCraftingCount(int pSlot)
{
    if(pSlot < 0 || pSlot >= CRAFT_ADAMANTITE_TOTAL) return 0;
    return mCraftingItemCounts[pSlot];
}
int AdventureInventory::GetCatalystCount(int pType)
{
    if(pType < 0 || pType >= CATALYST_TOTAL) return 0;
    return mCatalystCounts[pType];
}
int AdventureInventory::GetDoctorBagCharges()
{
    return mDoctorBagCharges;
}
int AdventureInventory::GetDoctorBagChargesMax()
{
    return mDoctorBagChargesMax;
}

//========================================= Manipulators ==========================================
void AdventureInventory::SetGemNameResolvePath(const char *pPath)
{
    ResetString(mGemNameResolvePath, pPath);
}
void AdventureInventory::RegisterItem(AdventureItem *pItem)
{
    //--Error check.
    if(!pItem) return;
    mWasAnythingCreated = true;

    //--Search through the item list. If another item exists with the same name, and is stackable,
    //  then increment the stack for that item instead and delete the old item.
    //--Note: This algorithm will not spot items that are still being created because their stackable
    //  property will never be true (yet). This will only catch instances where an item was already
    //  fully formed and is being registered, such as when changing equipment or buying from a shop.
    if(pItem->IsStackable())
    {
        //--Iterate.
        AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
        while(rItem)
        {
            //--If the item in question is stackable and has the same name:
            if(rItem->IsStackable() && !strcasecmp(pItem->GetName(), rItem->GetName()))
            {
                //--Add to the stack.
                rItem->SetStackSize(rItem->GetStackSize() + 1);
                rLastReggedItem = rItem;

                //--Delete the item itself.
                delete pItem;

                //--Clean.
                mWasAnythingCreated = false;
                mItemList->PopIterator();
                return;
            }

            //--Next item.
            rItem = (AdventureItem *)mItemList->AutoIterate();
        }
    }

    //--No matching stackables, so create a new instance.
    mItemList->AddElementAsTail(pItem->GetName(), pItem, &RootObject::DeleteThis);
    rLastReggedItem = pItem;
}
void AdventureInventory::RegisterAdamantite(AdventureItem *pItem, bool pDeleteItem)
{
    //--Registers the item as if it was an adamantite crafting ingredient.
    if(!pItem) return;

    //--Fast-access pointers.
    const char *rItemName = pItem->GetName();

    //--Check the constant names. Add a crafting ingredient on a match.
    if(!strcasecmp(rItemName, "Adamantite Powder"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_POWDER] ++;
    }
    else if(!strcasecmp(rItemName, "Adamantite Flakes"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_FLAKES] ++;
    }
    else if(!strcasecmp(rItemName, "Adamantite Shard"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_SHARD] ++;
    }
    else if(!strcasecmp(rItemName, "Adamantite Piece"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_PIECE] ++;
    }
    else if(!strcasecmp(rItemName, "Adamantite Chunk"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_CHUNK] ++;
    }
    else if(!strcasecmp(rItemName, "Adamantite Ore"))
    {
        mCraftingItemCounts[CRAFT_ADAMANTITE_ORE] ++;
    }

    //--If flagged, the item is deleted. Adamantite purchased from shops is not deleted!
    if(pDeleteItem) delete pItem;
}
void AdventureInventory::SetPlatina(int pAmount)
{
    mPlatina = pAmount;
    if(mPlatina < 0) mPlatina = 0;
}
void AdventureInventory::SetCraftingMaterial(int pSlot, int pAmount)
{
    if(pSlot < 0 || pSlot >= CRAFT_ADAMANTITE_TOTAL || pAmount < 0) return;
    mCraftingItemCounts[pSlot] = pAmount;
}
void AdventureInventory::AddCatalyst(int pType)
{
    if(pType < 0 || pType >= CATALYST_TOTAL) return;
    mCatalystCounts[pType] ++;;
}
void AdventureInventory::SetCatalystCount(int pType, int pAmount)
{
    if(pType < 0 || pType >= CATALYST_TOTAL) return;
    if(pAmount < 0) pAmount = 0;
    mCatalystCounts[pType] = pAmount;
}
void AdventureInventory::RemoveItem(const char *pName)
{
    //--Dumps the item out and deallocates it. Remember to remove any lingering references!
    //  Removes the first instance of the item found, in case of duplicates.
    if(!pName) return;

    //--First, find the first instance of the item. If it's stackable, we need to decrement the stack.
    AdventureItem *rItem = (AdventureItem *)mItemList->GetElementByName(pName);
    if(!rItem) return;

    //--Stackable? Decrement.
    if(rItem->IsStackable() && rItem->GetStackSize() > 1)
    {
        rItem->SetStackSize(rItem->GetStackSize() - 1);
    }
    //--Not stackable, or only one item. Remove it.
    else
    {
        mItemList->RemoveElementP(rItem);
        if(rLastReggedItem == rItem) rLastReggedItem = NULL;
        if(rMarkedEquipment== rItem) rMarkedEquipment = NULL;
        if(rMasterGem      == rItem) rMasterGem = NULL;
        if(rSocketingItem  == rItem) rSocketingItem = NULL;
    }
}
void AdventureInventory::SetDoctorBagCharges(int pAmount)
{
    //--Set.
    mDoctorBagCharges = pAmount;
    if(mDoctorBagCharges < 0) mDoctorBagCharges = 0;
    if(mDoctorBagCharges > mDoctorBagChargesMax) mDoctorBagCharges = mDoctorBagChargesMax;

    //--Update the DataLibrary. This is used for saving/loading only.
    SysVar *rDoctorBagVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Special/iDoctorBagCharges");
    if(rDoctorBagVariable) rDoctorBagVariable->mNumeric = (float)pAmount;
}
void AdventureInventory::SetDoctorBagMaxCharges(int pAmount)
{
    //--Set.
    mDoctorBagChargesMax = pAmount;
    if(mDoctorBagChargesMax < 0) mDoctorBagChargesMax = 0;
    if(mDoctorBagCharges > mDoctorBagChargesMax) mDoctorBagCharges = mDoctorBagChargesMax;

    //--Update the DataLibrary. This is used for saving/loading only.
    SysVar *rDoctorBagVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Special/iDoctorBagChargesMax");
    if(rDoctorBagVariable) rDoctorBagVariable->mNumeric = (float)pAmount;
}
void AdventureInventory::MarkLastItemAsMasterGem()
{
    //--Marks the last-created item in the inventory as the "master" gem. Subgems will be merged with
    //  the master gem when told to with MergeLastItemWithMasterGem().
    //--This is used for the loading handler when reassembling gems.
    rMasterGem = (AdventureItem *)rLastReggedItem;
}
void AdventureInventory::MergeLastItemWithMasterGem()
{
    //--Merge the last registered gem with the master gem, assuming both exist.
    if(!rMasterGem || !rLastReggedItem || (rMasterGem == rLastReggedItem)) return;
    bool tMerged = rMasterGem->MergeWithGem((AdventureItem *)rLastReggedItem);
    if(tMerged)
    {
        LiberateItemP(rLastReggedItem);
        rLastReggedItem = rMasterGem;
    }
}
void AdventureInventory::ClearMasterGem()
{
    //--Clean up after done merging gems.
    rMasterGem = NULL;
}
void AdventureInventory::MarkLastItemAsSocketItem()
{
    //--As above, used for socketing gems into equipped items during loading.
    rSocketingItem = (AdventureItem *)rLastReggedItem;
}
void AdventureInventory::SocketLastItemInSocketItem()
{
    if(!rSocketingItem || !rLastReggedItem) return;
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        if(rSocketingItem->GetGemInSlot(i)) continue;
        rSocketingItem->PlaceGemInSlot(i, (AdventureItem *)rLastReggedItem);
        LiberateItemP(rLastReggedItem);
        return;
    }
}
void AdventureInventory::ClearSocketItem()
{
    rSocketingItem = NULL;
}
void AdventureInventory::MarkLastItemAsEquipItem()
{
    rMarkedEquipment = (AdventureItem *)rLastReggedItem;
}
void AdventureInventory::ClearLastEquipItem()
{
    rMarkedEquipment = NULL;
}

//========================================= Core Methods ==========================================
void AdventureInventory::Clear()
{
    //--Clear the inventory back to a factory-zero state. Do not use this while it has any pointers
    //  outstanding or you will quite-obviously crash the program.

    //--System
    mWasAnythingCreated = false;

    //--Crafting Materials
    mPlatina = 0;
    memset(mCraftingItemCounts, 0, sizeof(int) * CRAFT_ADAMANTITE_TOTAL);

    //--Catalysts
    memset(mCatalystCounts, 0, sizeof(int) * CATALYST_TOTAL);

    //--Storage List
    mItemList->ClearList();
    mExtendedItemList->ClearList();
    mGemList->ClearList();
    mGemMergeList->ClearList();

    //--Public Variables
    rLastReggedItem = NULL;
    rMarkedEquipment = NULL;
    rMasterGem = NULL;
    rSocketingItem = NULL;
}
int AdventureInventory::IsItemEquipped(const char *pItemName)
{
    //--Returns if the item is equipped by any character. Returns how many times it was equipped, if any. Can be 0.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    int cRosterCount = rAdventureCombat->GetRosterCount();

    int tCount = 0;
    for(int i = 0; i < cRosterCount; i ++)
    {
        //--Check that the party member exists.
        AdvCombatEntity *rEntity = rAdventureCombat->GetRosterMemberI(i);
        if(!rEntity) continue;

        //--Check their equipment.
        //TODO FOR BBP
    }

    //--Character not found.
    return tCount;
}
int AdventureInventory::IsItemEquippedBy(const char *pCharacter, const char *pItemName)
{
    //--Returns how many times an item is equipped by the named character. Can be 0 if they don't have it.
    //TODO FOR BBP
    return 0;
}
void *AdventureInventory::xrDummyUnequipItem = new AdventureItem();
void *AdventureInventory::xrDummyGemsItem = new AdventureItem();
void AdventureInventory::BuildEquippableList(AdvCombatEntity *pEntity, const char *pEquipName, SugarLinkedList *pList)
{
    //--Given a character and a list, modifes the list to include all the items that the character
    //  can equip in the given slot. Does not clear the list inherently, just appends.
    //--Deallocation should not be active for the given list.
    if(!pEntity || !pList || !pEquipName) return;

    //--Get the equipment slot package.
    EquipmentSlotPack *rEquipmentSlotPack = pEntity->GetEquipmentSlotPackageS(pEquipName);
    if(!rEquipmentSlotPack) return;

    //--If the slot is able to be empty, add the "Unequip" option. There must be a piece of equipment in the slot first.
    if(rEquipmentSlotPack->mEquippedItem  && rEquipmentSlotPack->mCanBeEmpty)
    {
        pList->AddElementAsTail("Unequip", xrDummyUnequipItem);
        ((AdventureItem *)xrDummyUnequipItem)->SetOverrideQuality(100001.0f);
    }

    //--If the currently equipped item has at least one gem slot, put the gem-change item on it.
    if(rEquipmentSlotPack->mEquippedItem && rEquipmentSlotPack->mEquippedItem->GetGemSlots() > 0)
    {
        pList->AddElementAsTail("Change Gems", xrDummyGemsItem);
        ((AdventureItem *)xrDummyUnequipItem)->SetOverrideQuality(100000.0f);
    }

    //--Scan.
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        //--Item must be equippable by this character.
        if(rItem->IsEquippableBy(pEntity->GetName()) && rItem->IsEquippableIn(pEquipName))
        {
            pList->AddElementAsTail("X", rItem);
        }

        //--Next.
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }
}
void AdventureInventory::BuildExtendedItemList()
{
    //--Creates a list of all items currently held by the player, including ones that are equipped.
    //  The items that are equipped are denoted by having a named entry, while the ones in the inventory
    //  have "Null" for a name.
    mExtendedItemList->ClearList();

    //--Party list. Iterate across the roster.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    int cRosterSize = rAdventureCombat->GetActivePartyCount();
    for(int i = 0; i < cRosterSize; i ++)
    {
        //--Get the entity in question.
        AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rEntity) continue;

        //--Append all equipment to the list.
        int tEquipSlots = rEntity->GetEquipmentSlotsTotal();
        for(int p = 0; p < tEquipSlots; p ++)
        {
            AdventureItem *rItem = rEntity->GetEquipmentBySlotI(p);
            if(rItem) mExtendedItemList->AddElementAsTail(rEntity->GetName(), rItem);
        }
    }

    //--Now add the regular items.
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        mExtendedItemList->AddElementAsTail("Null", rItem);
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }
}
void AdventureInventory::BuildGemList()
{
    //--Creates a list of all items currently held by the player, including ones socketed into a piece
    //  of equipment. Socketed gems are denoted with the owning character's name. Gems that are in
    //  the inventory have the string "Null" for a name.
    mGemList->ClearList();

    //--Party list. Iterate across the roster.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    int cRosterSize = rAdventureCombat->GetActivePartyCount();
    for(int i = 0; i < cRosterSize; i ++)
    {
        //--Get the entity in question.
        AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rEntity) continue;

        //--Append all equipment to the list.
        int tSlotsTotal = rEntity->GetEquipmentSlotsTotal();
        for(int p = 0; p < tSlotsTotal; p ++)
        {
            AdventureItem *rSlotItem = rEntity->GetEquipmentBySlotI(p);
            if(!rSlotItem) continue;

            int tGemSlots = rSlotItem->GetGemSlots();
            for(int o = 0; o < tGemSlots; o ++)
            {
                AdventureItem *rGem = rSlotItem->GetGemInSlot(o);
                if(rGem)
                {
                    mGemList->AddElementAsTail("Null", rGem);
                }
            }
        }
    }

    //--Now add the regular items.
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        if(rItem->IsGem()) mGemList->AddElementAsTail("Null", rItem);
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }
}
void AdventureInventory::BuildGemListNoEquip()
{
    //--Builds a list of all gems that are only in the inventory, not equipped. Used when swapping gems out.
    mGemList->ClearList();

    //--Zeroth element is the unequip entry.
    mGemList->AddElementAsTail("Unequip", xrDummyUnequipItem);
    ((AdventureItem *)xrDummyUnequipItem)->SetOverrideQuality(100001.0f);

    //--Now build all the items.
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        if(rItem->IsGem()) mGemList->AddElementAsTail("Null", rItem);
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }

    //--Sort the list.
    SortListByCriteria(mGemList, AINV_SORT_CRITERIA_QUALITY, true);
}
SugarLinkedList *AdventureInventory::BuildGemMergeList(AdventureItem *pGem)
{
    //--Builds a list of gems that can be merged with the provided gem. The returned list is the
    //  local mGemMergeList which may have zero entries.
    mGemMergeList->ClearList();
    if(!pGem) return mGemMergeList;

    //--Iterate across the items:
    //fprintf(stderr, "Building gem merge list.\n");
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        //--If the gem is the one passed in, skip it.
        if(pGem == rItem)
        {
            //fprintf(stderr, " Highlight gem is query gem.\n");
        }
        //--If the item is a gem:
        else if(rItem->IsGem())
        {
            //--Get its color and check for exclusion.
            uint8_t tGemColor = rItem->GetGemColors();
            if((tGemColor & pGem->GetGemColors()) == 0)
            {
                mGemMergeList->AddElementAsTail("X", rItem);
                //fprintf(stderr, " Success %i vs %i\n", tGemColor, pGem->GetGemColors());
            }
            else
            {
                //fprintf(stderr, " Failed %i vs %i\n", tGemColor, pGem->GetGemColors());
            }
        }

        //--Next.
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }

    //--Pass it back for easy access.
    return mGemMergeList;
}
int AdventureInventory::ComputeCatalystBonus(int pType)
{
    //--Computes and returns the catalyst bonus for the given type. Can return zero.

    //--Health.
    if(pType == CATALYST_HEALTH)
    {
        return (mCatalystCounts[CATALYST_HEALTH] / CATALYST_NEEDED_HEALTH) * CATALYST_BUFF_HEALTH;
    }

    //--Attack power.
    if(pType == CATALYST_ATTACK)
    {
        return (mCatalystCounts[CATALYST_ATTACK] / CATALYST_NEEDED_ATTACK) * CATALYST_BUFF_ATTACK;
    }

    //--Initiative.
    if(pType == CATALYST_INITIATIVE)
    {
        return (mCatalystCounts[CATALYST_INITIATIVE] / CATALYST_NEEDED_INITIATIVE) * CATALYST_BUFF_INITIATIVE;
    }

    //--Dodge.
    if(pType == CATALYST_DODGE)
    {
        return (mCatalystCounts[CATALYST_DODGE] / CATALYST_NEEDED_DODGE) * CATALYST_BUFF_DODGE;
    }

    //--Accuracy.
    if(pType == CATALYST_ACCURACY)
    {
        return (mCatalystCounts[CATALYST_ACCURACY] / CATALYST_NEEDED_ACCURACY) * CATALYST_BUFF_ACCURACY;
    }

    //--Skill.
    if(pType == CATALYST_SKILL)
    {
        return (mCatalystCounts[CATALYST_SKILL] / CATALYST_NEEDED_SKILL);
    }

    //--Inaccessable code, here for safety.
    return 0;
}
AdventureItem *AdventureInventory::LocateItemByItemID(uint32_t pSearchID)
{
    //--Attempts to locate the item in the item list based on its ItemUniqueID. This routine is
    //  explicitly coded to be used during loading. It searches the player's equipment, the sockets
    //  in the player's equipment, and the base inventory. The first item found with the matching
    //  ID is returned, or NULL if no item is found.
    //--While it is not normally logically possible for an item to have a duplicate ID, it could
    //  happen as the result of a bug. In those cases, the first item found is returned.
    //--Does not work with stackable items, so don't use those.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Party list. Holds all characters regardless of who's in the party.
    int cRosterSize = rAdventureCombat->GetActivePartyCount();
    for(int i = 0; i < cRosterSize; i ++)
    {
        //--Check the entity.
        AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rEntity) continue;

        //--Check all equipment slots for the item.
        //TODO FOR BBP
    }

    //--Not on a character. Check the main list.
    AdventureItem *rItem = (AdventureItem *)mItemList->PushIterator();
    while(rItem)
    {
        //--Match.
        if(rItem->GetItemUniqueID() == pSearchID)
        {
            mItemList->PopIterator();
            return rItem;
        }


        //--Next.
        rItem = (AdventureItem *)mItemList->AutoIterate();
    }

    //--No matches.
    return NULL;
}
void AdventureInventory::StackItems()
{
    //--Stacks identical, stackable items together into one slot. Each time a stack occurs, we need
    //  to restart the process in order to maintain pointer consistency.

    //--This flag allows blocking of stacking. It is used when creating items for other inventories,
    //  such as shop vendor items.
    if(mBlockStackingOnce)
    {
        mBlockStackingOnce = false;
        return;
    }

    //--Proceed with stacking!
    bool tAtLeastOneRestack = true;
    while(tAtLeastOneRestack)
    {
        //--Flag reset.
        tAtLeastOneRestack = false;

        //--Iterate.
        AdventureItem *rFirstItem = (AdventureItem *)mItemList->PushIterator();
        while(rFirstItem)
        {
            //--If this item is stackable:
            if(rFirstItem->IsStackable())
            {
                //--Iterate ahead. Liberate any matches.
                mItemList->SetRandomPointerToThis(rFirstItem);
                AdventureItem *rSecondItem = (AdventureItem *)mItemList->IncrementAndGetRandomPointerEntry();

                //--Iterate across the list.
                while(rSecondItem)
                {
                    //--Item is stackable and shares a name with the first item:
                    if(rSecondItem->IsStackable() && !strcasecmp(rFirstItem->GetName(), rSecondItem->GetName()))
                    {
                        //--Liberate second entry, +1 stack for the first entry.
                        rFirstItem->SetStackSize(rFirstItem->GetStackSize() + 1);
                        mItemList->RemoveRandomPointerEntry();
                        if(rLastReggedItem == rSecondItem) rLastReggedItem = NULL;
                        if(rMasterGem      == rSecondItem) rMasterGem = NULL;
                        if(rSocketingItem  == rSecondItem) rSocketingItem = NULL;

                        //--Flag gets tripped.
                        tAtLeastOneRestack = true;
                    }

                    //--Next item.
                    rSecondItem = (AdventureItem *)mItemList->IncrementAndGetRandomPointerEntry();
                }

            }

            //--If a re-order occurred, stop here and restart.
            if(tAtLeastOneRestack)
            {
                mItemList->PopIterator();
                break;
            }

            //--Otherwise, check the next item.
            rFirstItem = (AdventureItem *)mItemList->AutoIterate();
        }
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
SugarLinkedList *AdventureInventory::GetExtendedItemList()
{
    return mExtendedItemList;
}
SugarLinkedList *AdventureInventory::GetItemList()
{
    return mItemList;
}
SugarLinkedList *AdventureInventory::GetGemList()
{
    return mGemList;
}
SugarLinkedList *AdventureInventory::GetGemMergeList()
{
    return mGemMergeList;
}
SugarLinkedList *AdventureInventory::GetBuybackList()
{
    return mShopBuybackList;
}
AdventureItem *AdventureInventory::GetItem(const char *pName)
{
    //--Note: Can legally return NULL.
    return (AdventureItem *)mItemList->GetElementByName(pName);
}
AdventureItem *AdventureInventory::LiberateItemS(const char *pName)
{
    mItemList->SetDeallocation(false);
    AdventureItem *mLiberatedItem = (AdventureItem *)mItemList->RemoveElementS(pName);
    mItemList->SetDeallocation(true);
    if(mLiberatedItem == rLastReggedItem)  rLastReggedItem = NULL;
    if(mLiberatedItem == rMarkedEquipment) rMarkedEquipment = NULL;
    if(mLiberatedItem == rMasterGem)       rMasterGem = NULL;

    //--Socketing item is NOT cleared when liberated, as it may be equipped to an entity.
    //  Use caution!
    //if(mLiberatedItem == rSocketingItem)  rSocketingItem = NULL;
    return mLiberatedItem;
}
AdventureItem *AdventureInventory::GetMarkedEquipment()
{
    return rMarkedEquipment;
}
AdventureItem *AdventureInventory::GetMasterGem()
{
    return rMasterGem;
}
void AdventureInventory::LiberateItemP(void *pPtr)
{
    if(!mItemList->IsElementOnList(pPtr))
    {
        return;
    }
    mItemList->SetDeallocation(false);
    mItemList->RemoveElementP(pPtr);
    mItemList->SetDeallocation(true);
    if(pPtr == rLastReggedItem)  rLastReggedItem = NULL;
    if(pPtr == rMarkedEquipment) rMarkedEquipment = NULL;
    if(pPtr == rMasterGem)       rMasterGem = NULL;

    //--Socketing item is NOT cleared when liberated, as it may be equipped to an entity.
    //  Use caution!
    //if(pPtr == rSocketingItem)  rSocketingItem = NULL;
}

//====================================== Static Functions =========================================
AdventureInventory *AdventureInventory::Fetch()
{
    return MapManager::Fetch()->GetAdventureInventory();
}

//========================================= Lua Hooking ===========================================
void AdventureInventory::HookToLuaState(lua_State *pLuaState)
{
    /* AdInv_CreateItem(sName)
       Creates, registers, and pushes a new AdventureItem. This item is registered to the player's
       inventory by default. */
    lua_register(pLuaState, "AdInv_CreateItem", &Hook_AdInv_CreateItem);

    /* AdInv_GetProperty("Unique ID Counter") (1 Integer)
       AdInv_GetProperty("Platina") (1 Integer)
       AdInv_GetProperty("Total Items Unequipped") (1 Integer)
       AdInv_GetProperty("Total Items Equipped") (1 Integer)
       AdInv_GetProperty("Item Count", sName) (1 Integer)
       AdInv_GetProperty("Is Item Equipped", sCharacter, sItemName) (1 Integer)
       AdInv_GetProperty("Crafting Count", iSlot) (1 Integer)
       AdInv_GetProperty("Doctor Bag Charges") (1 Integer)
       AdInv_GetProperty("Doctor Bag Charges Max") (1 Integer)
       AdInv_GetProperty("Catalyst Count", iSlot) (1 Integer)
       Sets the requested property in the adventure inventory. */
    lua_register(pLuaState, "AdInv_GetProperty", &Hook_AdInv_GetProperty);

    /* //--[Static]
       AdInv_SetProperty("Item Image Path", sPath) (Static)

       //--[System]
       AdInv_SetProperty("Gem Name Resolve Path", sPath)
       AdInv_SetProperty("Clear")
       AdInv_SetProperty("Stack Items")

       //--[Doctor Bag]
       AdInv_SetProperty("Doctor Bag Charges", iAmount)
       AdInv_SetProperty("Doctor Bag Charges Max", iAmount)

       //--[Catalysts]
       AdInv_SetProperty("Catalyst Count", iCatalystType, iAmount)

       //--[Platina]
       AdInv_SetProperty("Add Platina", iAmount)
       AdInv_SetProperty("Remove Platina", iAmount)
       AdInv_SetProperty("Remove Item", sName)

       //--[Crafting]
       AdInv_SetProperty("Crafting Material", iSlot, iAmount)
       AdInv_SetProperty("Add Crafting Material", iSlot, iAmount)
       AdInv_SetProperty("Remove Crafting Material", iSlot, iAmount)

       //--[Gems]
       AdInv_SetProperty("Mark Last Item As Master Gem")
       AdInv_SetProperty("Merge Last Gem With Master")
       AdInv_SetProperty("Push Master Gem") (Pushes Activity Stack)
       AdInv_SetProperty("Clear Master Gem")
       AdInv_SetProperty("Mark Last Item As Socket Item")
       AdInv_SetProperty("Socket Last Item In Socket Item")
       AdInv_SetProperty("Clear Socket Item")
       Sets the requested property in the AdventureInventory. */
    lua_register(pLuaState, "AdInv_SetProperty", &Hook_AdInv_SetProperty);

    /* AdInv_PushItem(sItemName)
       Pushes the first instance of the item found in the inventory. This is rarely used, and should
       only be used if the item has a unique name. */
    lua_register(pLuaState, "AdInv_PushItem", &Hook_AdInv_PushItem);

    /* AdInv_PushItemI(iIndex)
       Pushes the item in the given slot. */
    lua_register(pLuaState, "AdInv_PushItemI", &Hook_AdInv_PushItemI);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdInv_CreateItem(lua_State *L)
{
    //AdInv_CreateItem(sName)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdInv_CreateItem");

    //--Create.
    AdventureItem *nItem = new AdventureItem();
    nItem->SetName(lua_tostring(L, 1));

    //--Register.
    AdventureInventory::Fetch()->RegisterItem(nItem);

    //--Push as active object.
    DataLibrary::Fetch()->PushActiveEntity(nItem);

    return 0;
}
int Hook_AdInv_GetProperty(lua_State *L)
{
    //AdInv_GetProperty("Unique ID Counter") (1 Integer)
    //AdInv_GetProperty("Platina") (1 Integer)
    //AdInv_GetProperty("Total Items Unequipped") (1 Integer)
    //AdInv_GetProperty("Total Items Equipped") (1 Integer)
    //AdInv_GetProperty("Item Count", sName) (1 Integer)
    //AdInv_GetProperty("Is Item Equipped", sCharacter, sItemName) (1 Integer)
    //AdInv_GetProperty("Crafting Count", iSlot) (1 Integer)
    //AdInv_GetProperty("Doctor Bag Charges") (1 Integer)
    //AdInv_GetProperty("Doctor Bag Charges Max") (1 Integer)
    //AdInv_GetProperty("Catalyst Count", iSlot) (1 Integer)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdInv_GetProperty");

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--The current Unique ID counter for items. Used for savefiles.
    if(!strcasecmp(rSwitchType, "Unique ID Counter") && tArgs == 1)
    {
        lua_pushinteger(L, AdventureItem::xItemUniqueIDCounter);
        tReturns = 1;
    }
    //--How much money the party has.
    else if(!strcasecmp(rSwitchType, "Platina") && tArgs == 1)
    {
        lua_pushinteger(L, rInventory->GetPlatina());
        tReturns = 1;
    }
    //--How many items are in the inventory but not equipped to characters.
    else if(!strcasecmp(rSwitchType, "Total Items Unequipped") && tArgs == 1)
    {
        SugarLinkedList *rItemList = rInventory->GetItemList();
        lua_pushinteger(L, rItemList->GetListSize());
        tReturns = 1;
    }
    //--How many items are in the inventory, including those equipped to characters.
    else if(!strcasecmp(rSwitchType, "Total Items Equipped") && tArgs == 1)
    {
        rInventory->BuildExtendedItemList();
        SugarLinkedList *rExtendedItemsList = rInventory->GetExtendedItemList();
        lua_pushinteger(L, rExtendedItemsList->GetListSize());
        tReturns = 1;
    }
    //--Returns how many times the given name is in the inventory. Content does not matter, just name.
    else if(!strcasecmp(rSwitchType, "Item Count") && tArgs == 2)
    {
        lua_pushinteger(L, rInventory->GetCountOf(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Returns 1 or more if the item is equipped on the character, 0 if not.
    else if(!strcasecmp(rSwitchType, "Is Item Equipped") && tArgs == 3)
    {
        lua_pushinteger(L, rInventory->IsItemEquippedBy(lua_tostring(L, 2), lua_tostring(L, 3)));
        tReturns = 1;
    }
    //--How many crafting items of the given slot amount are present.
    else if(!strcasecmp(rSwitchType, "Crafting Count") && tArgs == 2)
    {
        lua_pushinteger(L, rInventory->GetCraftingCount(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Doctor Bag Charges
    else if(!strcasecmp(rSwitchType, "Doctor Bag Charges") && tArgs == 1)
    {
        lua_pushinteger(L, rInventory->GetDoctorBagCharges());
        tReturns = 1;
    }
    //--Doctor Bag Charges Max
    else if(!strcasecmp(rSwitchType, "Doctor Bag Charges Max") && tArgs == 1)
    {
        lua_pushinteger(L, rInventory->GetDoctorBagChargesMax());
        tReturns = 1;
    }
    //--Catalyst Count of a given type.
    else if(!strcasecmp(rSwitchType, "Catalyst Count") && tArgs == 2)
    {
        lua_pushinteger(L, rInventory->GetCatalystCount(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("AdItem_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_AdInv_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[Static]
    //AdInv_SetProperty("Item Image Path", sPath) (Static)

    //--[System]
    //AdInv_SetProperty("Gem Name Resolve Path", sPath)
    //AdInv_SetProperty("Clear")
    //AdInv_SetProperty("Stack Items")

    //--[Doctor Bag]
    //AdInv_SetProperty("Doctor Bag Charges", iAmount)
    //AdInv_SetProperty("Doctor Bag Charges Max", iAmount)

    //--[Catalysts]
    //AdInv_SetProperty("Catalyst Count", iCatalystType, iAmount)

    //--[Platina]
    //AdInv_SetProperty("Add Platina", iAmount)
    //AdInv_SetProperty("Remove Platina", iAmount)
    //AdInv_SetProperty("Remove Item", sName)

    //--[Crafting]
    //AdInv_SetProperty("Crafting Material", iSlot, iAmount)
    //AdInv_SetProperty("Add Crafting Material", iSlot, iAmount)
    //AdInv_SetProperty("Remove Crafting Material", iSlot, iAmount)

    //--[Gems]
    //AdInv_SetProperty("Mark Last Item As Master Gem")
    //AdInv_SetProperty("Merge Last Gem With Master")
    //AdInv_SetProperty("Push Master Gem") (Pushes Activity Stack)
    //AdInv_SetProperty("Clear Master Gem")
    //AdInv_SetProperty("Mark Last Item As Socket Item")
    //AdInv_SetProperty("Socket Last Item In Socket Item")
    //AdInv_SetProperty("Clear Socket Item")
    //AdInv_SetProperty("Mark Last Item As Equipment")
    //AdInv_SetProperty("Clear Equipment Marker")

    ///--[Argument Check]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdInv_SetProperty");

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--DLPath of the last name run through the image remapping script.
    if(!strcasecmp(rSwitchType, "Item Image Path") && tArgs == 2)
    {
        ResetString(AdventureInventory::xLastItemImagePath, lua_tostring(L, 2));
    }
    ///--[System]
    //--Path to the file that resolves gem names when they change.
    else if(!strcasecmp(rSwitchType, "Gem Name Resolve Path") && tArgs == 2)
    {
        rInventory->SetGemNameResolvePath(lua_tostring(L, 2));
    }
    //--Clear the entire inventory. Do NOT use this while an item is on the activity stack!
    else if(!strcasecmp(rSwitchType, "Clear") && tArgs == 1)
    {
        rInventory->Clear();
    }
    //--Re-sorts all stackable items. Call this after adding an item which can stack.
    else if(!strcasecmp(rSwitchType, "Stack Items") && tArgs == 1)
    {
        rInventory->StackItems();
    }
    ///--[Doctor Bag]
    //--How many charges the doctor bag has.
    else if(!strcasecmp(rSwitchType, "Doctor Bag Charges") && tArgs == 2)
    {
        rInventory->SetDoctorBagCharges(lua_tointeger(L, 2));
    }
    //--How many charges the doctor bag can contain.
    else if(!strcasecmp(rSwitchType, "Doctor Bag Charges Max") && tArgs == 2)
    {
        rInventory->SetDoctorBagMaxCharges(lua_tointeger(L, 2));
    }
    ///--[Catalysts]
    //--Number of each Catalyst the player has.
    else if(!strcasecmp(rSwitchType, "Catalyst Count") && tArgs == 3)
    {
        rInventory->SetCatalystCount(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    ///--[Platina]
    //--Add dosh.
    else if(!strcasecmp(rSwitchType, "Add Platina") && tArgs == 2)
    {
        rInventory->SetPlatina(rInventory->GetPlatina() + lua_tointeger(L, 2));
    }
    //--Remove dosh.
    else if(!strcasecmp(rSwitchType, "Remove Platina") && tArgs == 2)
    {
        rInventory->SetPlatina(rInventory->GetPlatina() - lua_tointeger(L, 2));
    }
    //--Removes an item.
    else if(!strcasecmp(rSwitchType, "Remove Item") && tArgs == 2)
    {
        rInventory->RemoveItem(lua_tostring(L, 2));
    }
    ///--[Crafting]
    //--Flatly sets the crafting count.
    else if(!strcasecmp(rSwitchType, "Crafting Material") && tArgs == 3)
    {
        rInventory->SetCraftingMaterial(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Adds crafting material.
    else if(!strcasecmp(rSwitchType, "Add Crafting Material") && tArgs == 3)
    {
        int tExisting = rInventory->GetCraftingCount(lua_tointeger(L, 2));
        rInventory->SetCraftingMaterial(lua_tointeger(L, 2), tExisting + lua_tointeger(L, 3));
    }
    //--Remove crafting material.
    else if(!strcasecmp(rSwitchType, "Remove Crafting Material") && tArgs == 3)
    {
        int tExisting = rInventory->GetCraftingCount(lua_tointeger(L, 2));
        rInventory->SetCraftingMaterial(lua_tointeger(L, 2), tExisting - lua_tointeger(L, 3));
    }
    ///--[Gems]
    //--Marks the last registered item as the master gem. Used for loading saves.
    else if(!strcasecmp(rSwitchType, "Mark Last Item As Master Gem") && tArgs == 1)
    {
        rInventory->MarkLastItemAsMasterGem();
    }
    //--Merges the last registered item with the master gem.
    else if(!strcasecmp(rSwitchType, "Merge Last Gem With Master") && tArgs == 1)
    {
        rInventory->MergeLastItemWithMasterGem();
    }
    //--Pushes the activity stack with the master gem. Can push null.
    else if(!strcasecmp(rSwitchType, "Push Master Gem") && tArgs == 1)
    {
        DataLibrary::Fetch()->PushActiveEntity(rInventory->GetMasterGem());
    }
    //--Clears the master gem pointer.
    else if(!strcasecmp(rSwitchType, "Clear Master Gem") && tArgs == 1)
    {
        rInventory->ClearMasterGem();
    }
    //--Marks the last registered item as the item which gems will socket into later.
    else if(!strcasecmp(rSwitchType, "Mark Last Item As Socket Item") && tArgs == 1)
    {
        rInventory->MarkLastItemAsSocketItem();
    }
    //--Sockets the last registered item into the marked socket item.
    else if(!strcasecmp(rSwitchType, "Socket Last Item In Socket Item") && tArgs == 1)
    {
        rInventory->SocketLastItemInSocketItem();
    }
    //--Clears the socketing item.
    else if(!strcasecmp(rSwitchType, "Clear Socket Item") && tArgs == 1)
    {
        rInventory->ClearSocketItem();
    }
    //--Marks the last created item as the one a character will equip next. Used for loading the game.
    else if(!strcasecmp(rSwitchType, "Mark Last Item As Equipment") && tArgs == 1)
    {
        rInventory->MarkLastItemAsEquipItem();
    }
    //--Clears the last item equipment flag.
    else if(!strcasecmp(rSwitchType, "Clear Equipment Marker") && tArgs == 1)
    {
        rInventory->ClearLastEquipItem();
    }
    ///--[Error]
    else
    {
        LuaPropertyError("AdInv_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
int Hook_AdInv_PushItem(lua_State *L)
{
    //AdInv_PushItem(sItemName)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdInv_PushItem");

    //--Set as active.
    rDataLibrary->rActiveObject = AdventureInventory::Fetch()->GetItem(lua_tostring(L, 1));
    return 0;
}
int Hook_AdInv_PushItemI(lua_State *L)
{
    //AdInv_PushItemI(iIndex)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdInv_PushItemI");

    //--Set as active.
    SugarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();
    void *rItem = rItemList->GetElementBySlot(lua_tointeger(L, 1));
    rDataLibrary->rActiveObject = rItem;
    return 0;
}
