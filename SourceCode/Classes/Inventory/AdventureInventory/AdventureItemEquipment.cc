//--Base
#include "AdventureItem.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

//--[Property Queries]
bool AdventureItem::IsEquipment()
{
    return mIsEquipment;
}
bool AdventureItem::IsEquippableBy(const char *pName)
{
    if(!mEquipEntityList) return false;
    return (mEquipEntityList->GetElementByName(pName) != NULL);
}
bool AdventureItem::IsEquippableIn(const char *pSlot)
{
    if(!mEquipSlotList) return false;
    return (mEquipSlotList->GetElementByName(pSlot) != NULL);
}
bool AdventureItem::MatchesEquippable(AdventureItem *pItem)
{
    //--Returns true if the item matches at least one equippable slot.
    if(!mEquipSlotList || !pItem || !pItem->mEquipSlotList) return false;

    //--Iterate across equipment slots.
    void *rDummyPtrA = mEquipSlotList->PushIterator();
    while(rDummyPtrA)
    {
        //--Get the name of the slot.
        const char *rSlotName = mEquipSlotList->GetIteratorName();

        //--Check all slots in the presented item.
        void *rDummyPtrB = pItem->mEquipSlotList->PushIterator();
        while(rDummyPtrB)
        {
            //--Other name.
            const char *rOtherSlotName = pItem->mEquipSlotList->GetIteratorName();
            if(!strcasecmp(rSlotName, rOtherSlotName))
            {
                mEquipSlotList->PopIterator();
                pItem->mEquipSlotList->PopIterator();
                return true;
            }
            rDummyPtrB = pItem->mEquipSlotList->AutoIterate();
        }
        rDummyPtrA = mEquipSlotList->AutoIterate();
    }

    //--All checks failed.
    return false;
}
const char *AdventureItem::GetEquipmentAttackAnimation()
{
    return mEquipmentAttackAnimation;
}
const char *AdventureItem::GetEquipmentAttackSound()
{
    return mEquipmentAttackSound;
}
const char *AdventureItem::GetEquipmentCriticalSound()
{
    return mEquipmentCriticalSound;
}
CombatStatistics *AdventureItem::GetEquipStatistics()
{
    return &mEquipmentBonus;
}
CombatStatistics *AdventureItem::GetFinalStatistics()
{
    return &mFinalEquipBonus;
}
int AdventureItem::GetStatistic(int pSlot)
{
    //--Iterate across any gems and add their values.
    return mFinalEquipBonus.GetStatByIndex(pSlot);
}
float AdventureItem::GetDamageType(int pCategory, int pTypeIndex)
{
    if(pCategory  < 0 || pCategory  >= ADITEM_DAMAGETYPE_TOTAL) return 0.0f;
    if(pTypeIndex < 0 || pTypeIndex >= ADVC_DAMAGE_TOTAL)       return 0.0f;
    return mEquipmentDamageTypes[pCategory].mValueList[pTypeIndex];
}

//--[Manipulators]
void AdventureItem::AddEquippableCharacter(const char *pName)
{
    static int xDummyVar;
    if(!pName || !mEquipEntityList || mEquipEntityList->GetElementByName(pName) != NULL) return;
    mEquipEntityList->AddElement(pName, &xDummyVar);
}
void AdventureItem::AddEquippableSlot(const char *pSlot)
{
    static int xDummyVar;
    if(!pSlot || !mEquipSlotList || mEquipSlotList->GetElementByName(pSlot) != NULL) return;
    mEquipSlotList->AddElement(pSlot, &xDummyVar);
}
void AdventureItem::SetEquipmentAttackAnimation(const char *pAnimation)
{
    ResetString(mEquipmentAttackAnimation, pAnimation);
}
void AdventureItem::SetEquipmentAttackSound(const char *pSound)
{
    ResetString(mEquipmentAttackSound, pSound);
}
void AdventureItem::SetEquipmentCriticalSound(const char *pSound)
{
    ResetString(mEquipmentCriticalSound, pSound);
}
void AdventureItem::SetGemCap(int pGemCap)
{
    mGemSlotsTotal = pGemCap;
    if(mGemSlotsTotal < 1) mGemSlotsTotal = 0;
    if(mGemSlotsTotal > ADITEM_MAX_GEMS) mGemSlotsTotal = ADITEM_MAX_GEMS;
}
void AdventureItem::SetStatistic(int pSlot, int pValue)
{
    mEquipmentBonus.SetStatByIndex(pSlot, pValue);
    ComputeStatistics();
}
void AdventureItem::SetDamageType(int pCategory, int pTypeIndex, float pValue)
{
    if(pCategory  < 0 || pCategory  >= ADITEM_DAMAGETYPE_TOTAL) return;
    if(pTypeIndex < 0 || pTypeIndex >= ADVC_DAMAGE_TOTAL)       return;
    mEquipmentDamageTypes[pCategory].mValueList[pTypeIndex] = pValue;
}
void AdventureItem::AppendToStatsPack(CombatStatistics &sPackage)
{
    sPackage.AddStatistics(mFinalEquipBonus);
}
float AdventureItem::ComputeQuality()
{
    //--Computes an abstract 'quality' value. The value is used in sorting items which otherwise
    //  can't really be fairly compared. The value itself doesn't actually mean anything in particular,
    //  and importantly, does not map onto the platina cost of the item.
    //--Many types of items return a quality of 0.0 inherently. Equipment and gems are the most common
    //  users of quality. Quality can also be manually overridden by scripts.
    if(mHasOverrideQuality) return mOverrideQuality;

    //--Setup.
    float tReturnVal = 0.0f;

    //--Finish up.
    return tReturnVal;
}
void AdventureItem::ComputeStatistics()
{
    //--Adds together the base equipment statistics with the gems to get the final statistics.
    memcpy(&mFinalEquipBonus, &mEquipmentBonus, sizeof(CombatStatistics));
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
    {
        if(!mGemSlots[i]) continue;
        mFinalEquipBonus.AddStatistics(*mGemSlots[i]->GetFinalStatistics());
    }
}
