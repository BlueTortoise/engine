//--[AdventureItem]
//--Represents an Item in Adventure Mode, which may be one of a consumable, key item, equipment, or overworld trap.
//  Some items can be used in multiple roles.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"
#include "AdvCombatDefStruct.h"

//--[Local Structures]
//--[Local Definitions]

//--Upgrade item types
#ifndef _ADAM_TYPES_
#define _ADAM_TYPES_
#define CRAFT_ADAMANTITE_POWDER 0
#define CRAFT_ADAMANTITE_FLAKES 1
#define CRAFT_ADAMANTITE_SHARD 2
#define CRAFT_ADAMANTITE_PIECE 3
#define CRAFT_ADAMANTITE_CHUNK 4
#define CRAFT_ADAMANTITE_ORE 5
#define CRAFT_ADAMANTITE_TOTAL 6
#endif

//--Property Line Flags
#define ADITEM_PROPERTY_NONE -1
#define ADITEM_PROPERTY_INVENTORY 0
#define ADITEM_PROPERTY_EQUIPSCREEN 1

//--Gem Colors
#define ADITEM_GEMCOLOR_GREY   0x01
#define ADITEM_GEMCOLOR_RED    0x02
#define ADITEM_GEMCOLOR_BLUE   0x04
#define ADITEM_GEMCOLOR_ORANGE 0x08
#define ADITEM_GEMCOLOR_VIOLET 0x10
#define ADITEM_GEMCOLOR_YELLOW 0x20
#define ADITEM_GEMCOLOR_ALL    0xFF
#define ADITEM_MAX_GEMS 6
#define ADITEM_MAX_GEM_RANK 5

//--Damage Type Equipment Categories
#define ADITEM_DAMAGETYPE_BASE 0
#define ADITEM_DAMAGETYPE_OVERRIDE 1
#define ADITEM_DAMAGETYPE_BONUS 2
#define ADITEM_DAMAGETYPE_TOTAL 3

//--[Classes]
class AdventureItem : public RootObject
{
    private:
    //--System
    bool mIsAdamantite;
    bool mIsStackable;
    int mStackSize;
    uint32_t mItemUniqueID;
    char *mLocalName;
    char *mDescription;
    char *mTopNameLine;
    char *mBotNameLine;
    bool mHasOverrideQuality;
    float mOverrideQuality;

    //--Property Lines
    int mLastResolveFlag;
    int mPropertyLinesTotal;
    char **mPropertyLines;
    char mInternalType[STD_MAX_LETTERS];

    //--Value
    int mValue;

    //--Consumable
    bool mIsConsumable;

    //--Key Item
    bool mIsUnique;
    bool mIsKeyItem;

    //--Equipment
    bool mIsEquipment;
    char *mEquipmentAttackSound;
    char *mEquipmentCriticalSound;
    char *mEquipmentAttackAnimation;
    SugarLinkedList *mEquipSlotList; //dummyptr
    SugarLinkedList *mEquipEntityList; //dummyptr
    CombatStatistics mEquipmentBonus;
    CombatStatistics mFinalEquipBonus; //After Gems
    DamageTypes mEquipmentDamageTypes[ADITEM_DAMAGETYPE_TOTAL];

    //--Gem Slots
    int mGemSlotsTotal;
    char *mOriginalGemName;
    char *mOriginalGemDescription;
    AdventureItem *mGemSlots[ADITEM_MAX_GEMS];

    //--Gems
    bool mIsGem;
    uint8_t mGemColorCode;

    //--Combat Items
    char *mAbilityPath;

    //--Tags
    SugarLinkedList *mTagList; //int *, master

    //--Rendering
    SugarBitmap *rIconImg;

    protected:

    public:
    //--System
    AdventureItem();
    virtual ~AdventureItem();

    //--Public Variables
    bool mGemErrorFlag;
    AdventureItem *rGemParent;
    AdvCombatEntity *rEquippingEntity;
    static int xItemUniqueIDCounter;

    //--Comparison Lines
    static char *xComparisonItemA;
    static char *xComparisonItemB;
    static int xComparisonLinesTotal;
    static char **xComparisonLines;

    //--Property Queries
    bool IsAdamantite();
    bool IsStackable();
    int GetStackSize();
    const char *GetAbilityPath();
    uint32_t GetItemUniqueID();
    const char *GetName();
    const char *GetTopName();
    const char *GetBotName();
    const char *GetDescription();
    int GetValue();
    int8_t GetUpgradeCode(int pSlot);
    int GetGemSlots();
    bool IsUnique();
    bool IsKeyItem();
    SugarBitmap *GetIconImage();
    const char *GetItemTypeString();
    int GetTagCount(const char *pTag);

    //--Manipulators
    void SetAdamantite();
    void SetStackable(bool pIsStackable);
    void SetStackSize(int pSize);
    void SetAbilityPath(const char *pPath);
    void OverrideID(uint32_t pID);
    void SetName(const char *pName);
    void SetNameLines(const char *pTop, const char *pBot);
    void SetDescription(const char *pDescription);
    void SetValue(int pValue);
    void SetUnique(bool pFlag);
    void SetKeyItem(bool pFlag);
    void SetConsumable(bool pFlag);
    void SetEquipmentFlag(bool pFlag);
    void SetIconImage(const char *pPath);
    AdventureItem *PlaceGemInSlot(int pSlot, AdventureItem *pGem);
    AdventureItem *RemoveGemFromSlot(int pSlot);
    void AddItemToProperties(AdventureItem *pItem);
    void RemoveItemFromProperties(AdventureItem *pItem);
    void SetOverrideQuality(float pQuality);
    void AddTag(const char *pTag);
    void RemoveTag(const char *pTag);

    //--Equipment Items
    bool IsEquipment();
    bool IsEquippableBy(const char *pName);
    bool IsEquippableIn(const char *pSlot);
    bool MatchesEquippable(AdventureItem *pItem);
    const char *GetEquipmentAttackAnimation();
    const char *GetEquipmentAttackSound();
    const char *GetEquipmentCriticalSound();
    CombatStatistics *GetEquipStatistics();
    CombatStatistics *GetFinalStatistics();
    int GetStatistic(int pSlot);
    float GetDamageType(int pCategory, int pTypeIndex);
    void AddEquippableCharacter(const char *pName);
    void AddEquippableSlot(const char *pSlot);
    void SetEquipmentAttackAnimation(const char *pAnimation);
    void SetEquipmentAttackSound(const char *pSound);
    void SetEquipmentCriticalSound(const char *pSound);
    void SetGemCap(int pGemCap);
    void SetStatistic(int pSlot, int pValue);
    void SetDamageType(int pCategory, int pTypeIndex, float pValue);
    void AppendToStatsPack(CombatStatistics &sPackage);
    float ComputeQuality();
    void ComputeStatistics();

    //--Combat Items
    AdvCombatAbility *GetCombatAbility();
    void RegisterAbility(AdvCombatAbility *pAbility);

    //--Gems
    bool IsGem();
    const char *GetGemBaseName();
    uint8_t GetGemColors();
    bool CanBeMerged(AdventureItem *pGem);
    int GetRank();
    void SetIsGem(uint8_t pGemColors);
    bool MergeWithGem(AdventureItem *pGem);
    void AutogenerateGemDescription();

    //--Core Methods
    AdventureItem *Clone();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    AdventureItem *GetGemInSlot(int pSlot);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdItem_GetProperty(lua_State *L);
int Hook_AdItem_SetProperty(lua_State *L);
