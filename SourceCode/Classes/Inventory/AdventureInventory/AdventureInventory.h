//--[AdventureInventory]
//--Inventory used for Adventure Mode. Should not be confused with the other inventory which is used for
//  Classic/Enhanced/Corrupter Modes, this one does not use the same basic parts!

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
typedef struct
{
    uint32_t mItemUniqueID;
    uint32_t mItemState;
}ItemUpgradePack;

//--[Local Definitions]
//--Adamantite and Catalyst types. Defined in several spots.
#ifndef _ADAM_TYPES_
#define _ADAM_TYPES_
#define CRAFT_ADAMANTITE_POWDER 0
#define CRAFT_ADAMANTITE_FLAKES 1
#define CRAFT_ADAMANTITE_SHARD 2
#define CRAFT_ADAMANTITE_PIECE 3
#define CRAFT_ADAMANTITE_CHUNK 4
#define CRAFT_ADAMANTITE_ORE 5
#define CRAFT_ADAMANTITE_TOTAL 6
#endif

//--Catalyst types.
#define CATALYST_HEALTH 0
#define CATALYST_ATTACK 1
#define CATALYST_INITIATIVE 2
#define CATALYST_DODGE 3
#define CATALYST_ACCURACY 4
#define CATALYST_SKILL 5
#define CATALYST_TOTAL 6

//--Buffs due to Catalysts.
#define CATALYST_NEEDED_HEALTH 5
#define CATALYST_BUFF_HEALTH 10
#define CATALYST_NEEDED_ATTACK 3
#define CATALYST_BUFF_ATTACK 2
#define CATALYST_NEEDED_INITIATIVE 3
#define CATALYST_BUFF_INITIATIVE 1
#define CATALYST_NEEDED_DODGE 3
#define CATALYST_BUFF_DODGE 2
#define CATALYST_NEEDED_ACCURACY 3
#define CATALYST_BUFF_ACCURACY 2
#define CATALYST_NEEDED_SKILL 6

//--Sort Criteria
#define AINV_SORT_CRITERIA_QUALITY 0
#define AINV_SORT_CRITERIA_NAME 1
#define AINV_SORT_CRITERIA_MAX 2

//--Shop UI
#define AINV_MAX_BUYBACK 20

//--[Classes]
class AdventureInventory : public RootObject
{
    private:
    //--System
    bool mWasAnythingCreated;

    //--Paths
    char *mGemNameResolvePath;

    //--Crafting Materials
    int mPlatina;
    int mCraftingItemCounts[CRAFT_ADAMANTITE_TOTAL];

    //--Catalysts
    int mCatalystCounts[CATALYST_TOTAL];

    //--Upgrading
    bool mIsUpgradeable;

    //--Storage List
    SugarLinkedList *mItemList; //AdventureItem *, master
    SugarLinkedList *mExtendedItemList; //AdventureItem *, reference
    SugarLinkedList *mGemList; //AdventureItem *, reference
    SugarLinkedList *mGemMergeList; //AdventureItem *, reference

    //--Doctor Bag
    bool mIsDoctorBagEnabled;
    int mDoctorBagCharges;
    int mDoctorBagChargesMax;

    //--Shop Buyback
    SugarLinkedList *mShopBuybackList; //ShopInventoryPack *, master

    //--Gems
    AdventureItem *rMarkedEquipment;
    AdventureItem *rMasterGem;
    AdventureItem *rSocketingItem;

    protected:

    public:
    //--System
    AdventureInventory();
    virtual ~AdventureInventory();

    //--Public Variables
    void *rLastReggedItem;
    bool mBlockStackingOnce;
    static void *xrDummyUnequipItem;
    static void *xrDummyGemsItem;
    static AdventureItem *xUpgradeItem;
    static bool xSortListInReverse;
    static char *xLastItemImagePath;

    //--Property Queries
    const char *GetGemNameResolvePath();
    int GetItemCount();
    int GetCountOf(const char *pName);
    int GetPlatina();
    int GetCraftingCount(int pSlot);
    int GetCatalystCount(int pType);
    int GetDoctorBagCharges();
    int GetDoctorBagChargesMax();

    //--Manipulators
    void SetGemNameResolvePath(const char *pPath);
    void RegisterItem(AdventureItem *pItem);
    void RegisterAdamantite(AdventureItem *pItem, bool pDeleteItem);
    void SetPlatina(int pAmount);
    void SetCraftingMaterial(int pSlot, int pAmount);
    void AddCatalyst(int pType);
    void SetCatalystCount(int pType, int pAmount);
    void RemoveItem(const char *pName);
    void SetDoctorBagCharges(int pAmount);
    void SetDoctorBagMaxCharges(int pAmount);
    void MarkLastItemAsMasterGem();
    void MergeLastItemWithMasterGem();
    void ClearMasterGem();
    void MarkLastItemAsSocketItem();
    void SocketLastItemInSocketItem();
    void ClearSocketItem();
    void MarkLastItemAsEquipItem();
    void ClearLastEquipItem();

    //--Core Methods
    void Clear();
    int IsItemEquipped(const char *pItemName);
    int IsItemEquippedBy(const char *pCharacter, const char *pItemName);
    void BuildEquippableList(AdvCombatEntity *pEntity, const char *pEquipName, SugarLinkedList *pList);
    void BuildExtendedItemList();
    void BuildGemList();
    void BuildGemListNoEquip();
    SugarLinkedList *BuildGemMergeList(AdventureItem *pGem);
    int ComputeCatalystBonus(int pType);
    AdventureItem *LocateItemByItemID(uint32_t pSearchID);
    void StackItems();

    //--Debug
    void Debug_AwardPlatina();
    void Debug_AwardCrafting();

    //--Saving
    void WriteToBuffer(SugarAutoBuffer *pBuffer);
    void ReadFromFile(VirtualFile *fInfile);

    //--Sorting
    void SortListByCriteria(SugarLinkedList *pList, int pCriteriaFlag, bool pHighestFirst);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    SugarLinkedList *GetExtendedItemList();
    SugarLinkedList *GetItemList();
    SugarLinkedList *GetGemList();
    SugarLinkedList *GetGemMergeList();
    SugarLinkedList *GetBuybackList();
    AdventureItem *GetItem(const char *pName);
    AdventureItem *LiberateItemS(const char *pName);
    AdventureItem *GetMarkedEquipment();
    AdventureItem *GetMasterGem();
    void LiberateItemP(void *pPtr);

    //--Static Functions
    static AdventureInventory *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdInv_CreateItem(lua_State *L);
int Hook_AdInv_GetProperty(lua_State *L);
int Hook_AdInv_SetProperty(lua_State *L);
int Hook_AdInv_PushItem(lua_State *L);
int Hook_AdInv_PushItemI(lua_State *L);
