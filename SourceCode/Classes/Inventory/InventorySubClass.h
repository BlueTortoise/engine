//--[InventorySubClass]
//--This is the class that an Actor (or a container, or a room) holds to keep track of its items.
//  It can be pushed onto the activity stack for direct manipulation of an inventory.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--[Local Definitions]
#define REJECTION_NONE 0x00
#define REJECTION_FULL 0x01
#define REJECTION_ILLEGAL_TYPE 0x02
#define REJECTION_OTHER_ERROR 0x03

//--[Classes]
class InventorySubClass : public RootObject
{
    private:
    //--System
    //--Storage List
    int mMaxInventorySize;
    SugarLinkedList *mStorageList;

    protected:

    public:
    //--System
    InventorySubClass();
    virtual ~InventorySubClass();

    //--Public Variables
    bool mRequiresReconstitution;

    //--Property Queries
    int GetInventorySize();
    int GetItemCount(const char *pItemName);
    int GetItemCountP(void *pPtr);
    InventoryItem *GetItemI(uint32_t pID);
    InventoryItem *GetItemS(const char *pName);
    InventoryItem *GetItemL(int pSlot);
    int GetSlotOfItemS(const char *pName);
    bool IsItemInList(void *pCheckPtr);

    //--Manipulators
    uint8_t RegisterItem(InventoryItem *pItem);
    InventoryItem *LiberateItemI(uint32_t pID);
    InventoryItem *LiberateItemS(const char *pName);
    InventoryItem *LiberateItemL(int pSlot);
    void LiberateItemP(void *pPtr);
    void RemoveItemI(uint32_t pID);
    void RemoveItemS(const char *pName);
    void RemoveItemL(int pSlot);

    //--Core Methods
    bool PurgeMarkedItems();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

