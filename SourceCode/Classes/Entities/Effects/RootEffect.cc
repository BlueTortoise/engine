//--Base
#include "RootEffect.h"

//--Classes
#include "PandemoniumLevel.h"
#include "PandemoniumRoom.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
RootEffect::RootEffect()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ROOTEFFECT;

    //--[RootEntity]
    //--System
    //--Nameable
    //--Renderable
    //--Data Storage
    //--Public Variables

    //--[RootEffect]
    //--System
    mTurnsLeft = 0;
    mDisplayName = NULL;

    //--Calling
    mScriptPath = NULL;

    //--Owning Room
    mOwningRoomName = NULL;
}
RootEffect::~RootEffect()
{
    free(mDisplayName);
    free(mScriptPath);
    free(mOwningRoomName);
}

//====================================== Property Queries =========================================
const char *RootEffect::GetDisplayName()
{
    return (const char *)mDisplayName;
}
const char *RootEffect::GetOwningRoomName()
{
    return (const char *)mOwningRoomName;
}

//========================================= Manipulators ==========================================
void RootEffect::SetDisplayName(const char *pName)
{
    ResetString(mDisplayName, pName);
}
void RootEffect::SetTurnsLeft(int pTurns)
{
    mTurnsLeft = pTurns;
}
void RootEffect::SetScriptPath(const char *pPath)
{
    //--Special case: Passing "Null" will null off the path. Note that this will implicitly flag the
    //  object for deletion if the next update completes before a new path is set.
    if(!pPath || !strcasecmp(pPath, "Null"))
    {
        ResetString(mScriptPath, NULL);
    }
    //--Normal case.
    else
    {
        ResetString(mScriptPath, pPath);
    }
}
void RootEffect::SetOwningRoom(const char *pRoomName)
{
    ResetString(mOwningRoomName, pRoomName);
}

//========================================= Core Methods ==========================================
void RootEffect::RespondToBoot()
{
    if(!mScriptPath) return;
    DataLibrary::Fetch()->PushActiveEntity(this);
        LuaManager::Fetch()->ExecuteLuaFile(mScriptPath, 1, "N", (float)EFFECT_INITIALIZE);
    DataLibrary::Fetch()->PopActiveEntity();
}
void RootEffect::RespondToSelfDestruct()
{
    //--Calls the script one last time before removing the reference to it, this time with EFFECT_DESTRUCTOR.
    if(!mScriptPath) return;
    DataLibrary::Fetch()->PushActiveEntity(this);
        LuaManager::Fetch()->ExecuteLuaFile(mScriptPath, 1, "N", (float)EFFECT_DESTRUCTOR);
    DataLibrary::Fetch()->PopActiveEntity();

    //--Clear.
    free(mScriptPath);
    mScriptPath = NULL;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void RootEffect::Update()
{
    //--Note: This update routine is called during the room's individual update, not the EM's update.
    if(mScriptPath)
    {
        //--Subtract one from the turn timer. If the timer goes below zero, don't execute logic.
        mTurnsLeft --;
        if(mTurnsLeft < 0)
        {
            mSelfDestruct = true;
        }
        //--Normal logic execution.
        else
        {
            //--Push the RootEffect on the activity stack. Accessing the owning PandemoniumRoom can
            //  be done by pushing it.
            DataLibrary::Fetch()->PushActiveEntity(this);
                LuaManager::Fetch()->ExecuteLuaFile(mScriptPath, 1, "N", (float)EFFECT_UPDATE);
            DataLibrary::Fetch()->PopActiveEntity();
        }
    }
    //--If there's no script path, flag the object for immediate removal.
    else
    {
        mSelfDestruct = true;
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void RootEffect::HookToLuaState(lua_State *pLuaState)
{
    /* Effect_Create(sRoomName, sCallingScript)
       Creates and pushes a new RootEffect object. If the room's name is provided, the object will
       be created in that room. Otherwise, it will attempt to use the room on top of the activity
       stack. If the object atop the stack is not a room, the function will fail with a warning message.
       If the function succeeds, the calling script is called immediately afterwards with code 0. */
    lua_register(pLuaState, "Effect_Create", &Hook_Effect_Create);

    /* Effect_SetProperty("Display Name", sName)
       Effect_SetProperty("Duration", iTurns)
       Effect_SetProperty("Calling Script", sPath)
       Performs the requested change with the RootEffect on top of the activity stack. */
    lua_register(pLuaState, "Effect_SetProperty", &Hook_Effect_SetProperty);

    /* Effect_PushOwner()
       Pushes the owner of the RootEffect atop the activity stack. */
    lua_register(pLuaState, "Effect_PushOwner", &Hook_Effect_PushOwner);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_Effect_Create(lua_State *L)
{
    //Effect_Create(sRoomName, sCallingScript)
    int tArgs = lua_gettop(L);

    //--Store the previous top of the activity stack. Push it.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    if(tArgs < 2) return LuaArgError("RE_SetDepth", L);

    //--Get the room for registration.
    PandemoniumRoom *rOwningRoom = PandemoniumLevel::Fetch()->GetRoom(lua_tostring(L, 1));
    if(!rOwningRoom)
    {
        DebugManager::ForcePrint("RootEffect: Error, attempt to create with room %s which doesn't exist.\n", lua_tostring(L, 1));
        return 0;
    }

    //--Create and set.
    RootEffect *nEffect = new RootEffect();
    nEffect->SetOwningRoom(lua_tostring(L, 1));
    nEffect->SetScriptPath(lua_tostring(L, 2));
    rDataLibrary->rActiveObject = nEffect;

    //--Register.
    rOwningRoom->RegisterEffect(nEffect);

    //--Run the boot process.
    nEffect->RespondToBoot();

    //--Everything went peachy.
    return 0;
}
int Hook_Effect_SetProperty(lua_State *L)
{
    //Effect_SetProperty("Display Name", sName)
    //Effect_SetProperty("Duration", iTurns)
    //Effect_SetProperty("Calling Script", sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Effect_SetProperty");

    //--Active object.
    RootEffect *rEffect = (RootEffect *)DataLibrary::Fetch()->rActiveObject;
    if(!rEffect || rEffect->GetType() < POINTER_TYPE_EFFECT_BEGIN || rEffect->GetType() > POINTER_TYPE_EFFECT_END) return LuaTypeError("Effect_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Display name, shows in the entities pane.
    if(!strcasecmp(rSwitchType, "Display Name") && tArgs == 2)
    {
        rEffect->SetDisplayName(lua_tostring(L, 2));
    }
    //--Duration. Set to 0 to cause the object to clear on the next pass.
    else if(!strcasecmp(rSwitchType, "Duration") && tArgs == 2)
    {
        rEffect->SetTurnsLeft(lua_tointeger(L, 2));
    }
    //--Calling script. Changes the path the object uses to update and destruct.
    else if(!strcasecmp(rSwitchType, "Calling Script") && tArgs == 2)
    {
        rEffect->SetScriptPath(lua_tostring(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("Actor_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_Effect_PushOwner(lua_State *L)
{
    //Effect_PushOwner()

    //--Active object.
    RootEffect *rEffect = (RootEffect *)DataLibrary::Fetch()->rActiveObject;

    //--Push.
    DataLibrary::Fetch()->PushActiveEntity();

    //--Check.
    if(!rEffect || rEffect->GetType() < POINTER_TYPE_EFFECT_BEGIN || rEffect->GetType() > POINTER_TYPE_EFFECT_END) return LuaTypeError("Effect_PushOwner");

    //--Peachy.
    DataLibrary::Fetch()->rActiveObject = PandemoniumLevel::Fetch()->GetRoom(rEffect->GetOwningRoomName());
    return 0;
}
