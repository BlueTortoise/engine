//--Base
#include "TilemapActor.h"

//--Classes
#include "AdventureLevel.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers

//--[Collision Functions]
//--These four functions check if any collision along the given edge is true.
bool TilemapActor::IsLftClipped()
{
    //--Autopass.
    if(mIgnoreCollisionsWhenMoving) return false;

    //--Setup.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX - 1.0f;
    float cTop = mTrueY;

    //--Iterate.
    for(int y = 0; y < TA_SIZE; y ++)
    {
        if(rActiveLevel->GetClipAt(cLft, (cTop + y), 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    //--Additional: Wander NPCs must not wander into the player.
    if(mIsWanderNPC)
    {
        for(int y = 0; y < TA_SIZE; y ++)
        {
            if(rActiveLevel->GetWanderClipAt(cLft, (cTop + y)))
            {
                return true;
            }
        }
    }

    return false;
}
bool TilemapActor::IsTopClipped()
{
    //--Autopass.
    if(mIgnoreCollisionsWhenMoving) return false;

    //--Setup.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX;
    float cTop = mTrueY - 1.0f;

    //--Iterate.
    for(int x = 0; x < TA_SIZE; x ++)
    {
        if(rActiveLevel->GetClipAt(cLft + x, cTop, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    //--Additional: Wander NPCs must not wander into the player.
    if(mIsWanderNPC)
    {
        for(int x = 0; x < TA_SIZE; x ++)
        {
            if(rActiveLevel->GetWanderClipAt(cLft + x, cTop))
            {
                return true;
            }
        }
    }

    return false;
}
bool TilemapActor::IsRgtClipped()
{
    //--Autopass.
    if(mIgnoreCollisionsWhenMoving) return false;

    //--Setup.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX + TA_SIZE;
    float cTop = mTrueY;

    //--Iterate.
    for(int y = 0; y < TA_SIZE; y ++)
    {
        if(rActiveLevel->GetClipAt(cLft, cTop + y, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    //--Additional: Wander NPCs must not wander into the player.
    if(mIsWanderNPC)
    {
        for(int y = 0; y < TA_SIZE; y ++)
        {
            if(rActiveLevel->GetWanderClipAt(cLft, cTop + y))
            {
                return true;
            }
        }
    }

    return false;
}
bool TilemapActor::IsBotClipped()
{
    //--Autopass.
    if(mIgnoreCollisionsWhenMoving) return false;

    //--Setup.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX;
    float cTop = mTrueY + TA_SIZE;

    //--Iterate.
    for(int x = 0; x < TA_SIZE; x ++)
    {
        if(rActiveLevel->GetClipAt(cLft + x, cTop, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    //--Additional: Wander NPCs must not wander into the player.
    if(mIsWanderNPC)
    {
        for(int x = 0; x < TA_SIZE; x ++)
        {
            if(rActiveLevel->GetWanderClipAt(cLft + x, cTop))
            {
                return true;
            }
        }
    }

    return false;
}

//--[Narrow Collision Functions]
//--These are the same as their above equivalents but they do next check the edge of the edge (hah!), instead
//  narrowing the hitbox by 1 pixel on both sides. This is used for slope checks.
bool TilemapActor::IsLftClippedNarrow()
{
    //--Setup.
    TiledLevel *rActiveLevel = TiledLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX - 1.0f;
    float cTop = mTrueY;

    //--Iterate.
    for(int y = 1; y < TA_SIZE - 1; y ++)
    {
        if(rActiveLevel->GetClipAt(cLft, (cTop + y), 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    return false;
}
bool TilemapActor::IsTopClippedNarrow()
{
    //--Setup.
    TiledLevel *rActiveLevel = TiledLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX;
    float cTop = mTrueY - 1.0f;

    //--Iterate.
    for(int x = 1; x < TA_SIZE - 1; x ++)
    {
        if(rActiveLevel->GetClipAt(cLft + x, cTop, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    return false;
}
bool TilemapActor::IsRgtClippedNarrow()
{
    //--Setup.
    TiledLevel *rActiveLevel = TiledLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX + TA_SIZE;
    float cTop = mTrueY;

    //--Iterate.
    for(int y = 1; y < TA_SIZE - 1; y ++)
    {
        if(rActiveLevel->GetClipAt(cLft, cTop + y, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    return false;
}
bool TilemapActor::IsBotClippedNarrow()
{
    //--Setup.
    TiledLevel *rActiveLevel = TiledLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Position.
    float cLft = mTrueX;
    float cTop = mTrueY + TA_SIZE;

    //--Iterate.
    for(int x = 1; x < TA_SIZE - 1; x ++)
    {
        if(rActiveLevel->GetClipAt(cLft + x, cTop, 0.0f, mCollisionDepth))
        {
            return true;
        }
    }

    return false;
}
