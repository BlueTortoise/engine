//--Base
#include "TilemapActor.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureDebug.h"
#include "AdventureLevel.h"
#include "TileLayer.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "GlDfn.h"
#include "HitDetection.h"
#include "OpenGLMacros.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"

//--[Definitions]
//--Flashwhite sequence.
#define FLASHWHITE_TICKS_UP 60
#define FLASHWHITE_TICKS_HOLD 30
#define FLASHWHITE_TICKS_DOWN 60
#define FLASHWHITE_TICKS_TOTAL (FLASHWHITE_TICKS_UP + FLASHWHITE_TICKS_HOLD + FLASHWHITE_TICKS_DOWN)

//--Positioning.
#define TRUE_OFF_X  4.0f
#define TRUE_OFF_Y  8.0f

//--Oscillation constants.
#define OSCILLATION_TICKS 60
#define OSCILLATION_AMT 1.2f

//=========================================== Drawing =============================================
void TilemapActor::Render()
{
    ///--[Documentation and Setup]
    //--Renders the TilemapActor, on the assumption that the rendering cursor has externally been
    //  set to the correct location.
    if(!xAllowRender) return;

    //--Disabled enemies never render.
    if(mIsDisabled) return;

    //--Entities with negative movement timers do not render until they reach positive numbers.
    if(mNegativeMoveTimer > 0) return;

    //--If the entity is set to render before tiles, and the tile flag is not set, don't render.
    if(mRendersAfterTiles != xIsRenderingBeforeTiles) return;

    ///--[Color Setup]
    StarlightColor cMuggedColor = StarlightColor::MapRGBAF(0.50f, 0.50f, 0.50f, 1.0f);
    StarlightColor cNormalColor = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    StarlightColor tActiveColor = cNormalColor;
    if(mMugStunTimer > 0) tActiveColor = cMuggedColor;

    ///--[Lighting]
    //--If lights are active, the entity will upload its base Y position to the shader and will be
    //  lit using that.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel->IsLightingActive())
    {
        //--Common.
        uint32_t cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;

        //--Standard: Most entities are 2 tiles tall and use a base Y.
        if(!mIgnoreSpecialLighting)
        {
            ShaderUniform1i(cShaderHandle, 3, "uUseOverrideY");
            ShaderUniform1f(cShaderHandle, ((mTrueY + 4.0f) - rActiveLevel->GetCameraTop()) * rActiveLevel->GetCameraScale(), "uOverrideY");
        }
        //--Special: Entity is not 2 tiles tall.
        else
        {
            ShaderUniform1i(cShaderHandle, 0, "uUseOverrideY");
        }
    }

    ///--[Stunned]
    //--Stunned enemies also flash.
    if(mStunTimer > 0 && mStunTimer % 6 < 3) return;

    ///--[Normal Rendering]
    //--Check image integrity.
    SugarBitmap *rRenderFrame = ResolveFrame();
    if(!rRenderFrame) return;

    //--Reposition.
    float tXPosition = mTrueX + mOffsetX - TRUE_OFF_X;
    float tYPosition = mTrueY + mOffsetY - TRUE_OFF_Y;
    float tYVerticalOff = mVerticalOffY;
    float tOscillation = 0.0f;

    //--When oscillating, the Y position is offset slightly.
    if(mYOscillates && !mIsShowingFlashwhiteSequence && !mIsShowingSpecialImage)
    {
        tOscillation = (sinf((float)mYOscillateTimer / (float)OSCILLATION_TICKS * 3.1415926f) * OSCILLATION_AMT);
        tYVerticalOff = tYVerticalOff + tOscillation;
    }
    tYPosition = tYPosition + tYVerticalOff;

    //--Compute Z depth. We go up based on which collision layer we're using.
    float tZPosition = DEPTH_MIDGROUND + (mTrueY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
    tZPosition = tZPosition + (DEPTH_PER_TILE * (float)mCollisionDepth * 2.0f);
    if(mOverrideDepth != -2.0f) tZPosition = mOverrideDepth;

    //--Move to the computed position.
    glTranslatef(tXPosition, tYPosition, tZPosition);

    ///--Flashwhite. Set up the stencil buffer.
    if(mIsShowingFlashwhiteSequence)
    {
        //--Set the GL State up for the first stencil pass.
        glEnable(GL_STENCIL_TEST);
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilFunc(GL_ALWAYS, 2, 0xFF);
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        glStencilMask(0xFF);
    }

    //--Handle for the active shader.
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    GLint cShaderHandle = 0;

    //--Enemies that are tougher than normal have an outline.
    if(mEnemyToughness > 0)
    {
        //--If Lighting is currently inactive, we need to turn the color outline shader on.
        if(!rActiveLevel->IsLightingActive())
        {
            rDisplayManager->ActivateProgram("TilemapActor ColorOutline");
            cShaderHandle = rDisplayManager->mLastProgramHandle;
            if(mMugStunTimer > 0)
            {
                ShaderUniform1f(cShaderHandle, 0.5f, "uColorMixerR");
                ShaderUniform1f(cShaderHandle, 0.5f, "uColorMixerG");
                ShaderUniform1f(cShaderHandle, 0.5f, "uColorMixerB");
            }
            else
            {
                ShaderUniform1f(cShaderHandle, 1.0f, "uColorMixerR");
                ShaderUniform1f(cShaderHandle, 1.0f, "uColorMixerG");
                ShaderUniform1f(cShaderHandle, 1.0f, "uColorMixerB");
            }
        }
        //--If lighting is active, we need to toggle the variable in the existing shader.
        else
        {
            cShaderHandle = rDisplayManager->mLastProgramHandle;
            ShaderUniform1i(cShaderHandle, 1, "uShowColorOutline");
        }

        //--In both cases, upload the needed color. It's the same in either shader.
        int tUseToughness = mEnemyToughness - 1;
        if(tUseToughness >= 3) tUseToughness = 2;
        uint32_t tColorHandle = sglGetUniformLocation(cShaderHandle, "uOutlineColor");
        sglUniform4f(tColorHandle, xToughnessLookups[tUseToughness].r, xToughnessLookups[tUseToughness].g, xToughnessLookups[tUseToughness].b, xToughnessLookups[tUseToughness].a);
    }

    //--Party members render fullbright if the player has a lantern.
    if(rActiveLevel->IsLightingActive() && mIsPartyEntity && rActiveLevel->DoesPlayerHaveLightSource())
    {
        GLint cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
        ShaderUniform1i(cShaderHandle, 1, "uFullbrightEntity");
    }

    //--Render normally.
    if(!mIsTiny)
    {
        //--Render the shadow.
        if(rShadowImg && !mNoAutomaticShadow) rShadowImg->Draw(0, tYVerticalOff * -1.0f);

        //--Render the frame.
        if(!mDisableMainRender)
        {
            tActiveColor.SetAsMixer();
            rRenderFrame->Draw();
            StarlightColor::ClearMixer();
        }

        //--If there is a manual over-frame, render that.
        if(rManualOverlay) rManualOverlay->Draw();
    }
    //--Tiny enemies.
    else
    {
        glTranslatef(8.0f, 8.0f, 0.0f);
        glScalef(0.75f, 0.75f, 1.0f);
        if(rShadowImg && !mNoAutomaticShadow) rShadowImg->Draw(0, tYVerticalOff * -1.0f);
        tActiveColor.SetAsMixer();
        rRenderFrame->Draw();
        StarlightColor::ClearMixer();
        glScalef(1.0f / 0.75f, 1.0f / 0.75f, 1.0f);
        glTranslatef(-8.0f, -8.0f, 0.0f);
    }

    //--Deactivate fullbright if we turned it on.
    if(rActiveLevel->IsLightingActive() && mIsPartyEntity && rActiveLevel->DoesPlayerHaveLightSource())
    {
        GLint cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
        ShaderUniform1i(cShaderHandle, 0, "uFullbrightEntity");
    }

    //--Deactivate the shader if it was active.
    if(mEnemyToughness > 0)
    {
        //--Handle for the active shader.
        DisplayManager *rDisplayManager = DisplayManager::Fetch();

        //--If Lighting is currently inactive, we need to turn the color outline shader on.
        if(!rActiveLevel->IsLightingActive())
        {
            rDisplayManager->ActivateProgram(NULL);
        }
        //--If lighting is active, we need to toggle the variable in the existing shader.
        else
        {
            GLint cShaderHandle = rDisplayManager->mLastProgramHandle;
            ShaderUniform1i(cShaderHandle, 0, "uShowColorOutline");
        }
    }

    ///--[Spotting Sprites]
    //--Render enemy spotting sprites, if applicable.
    if(mIsEnemy && rSpottedExclamation && rSpottedQuestion && !mIsDying)
    {
        //--Enemy is spotting the player!
        if(mAIState == TA_AI_CHASE || mSpottedPlayerTicks > (int)(mSpottedPlayerTicksMax * 0.95f))
        {
            rSpottedExclamation->Draw(13, -4);
        }
        //--Enemy lost track of the player.
        else if(mAIState == TA_AI_LEASH || mSpottedPlayerTicks > 0)
        {
            rSpottedQuestion->Draw(13, -4);
        }

        //--Render a circle around the spotting icon to indicate how much time they have.
        if(mSpottedPlayerTicks > 0 && mAIState != TA_AI_CHASE)
        {
            //--Reposition.
            glTranslatef(17.0f, 2.0f, 0.0f);

            //--Completion.
            float tPercent = (float)mSpottedPlayerTicks / (float)mSpottedPlayerTicksMax;

            //--Color is based on percentage.
            if(tPercent < 0.50f)
            {
                glColor3f(tPercent * 2.0f, tPercent * 2.0f, 0.0f);
                if(rActiveLevel->IsLightingActive())
                {
                    cShaderHandle = rDisplayManager->mLastProgramHandle;
                    ShaderUniform1i(cShaderHandle, 1, "uUseMixer");
                    uint32_t tColorHandle = sglGetUniformLocation(cShaderHandle, "uMixer");
                    sglUniform4f(tColorHandle, tPercent * 2.0f, tPercent * 2.0f, 0.0f, 1.0f);
                }
            }
            //--Danger!
            else
            {
                glColor3f(1.0f, 1.0f - (tPercent), 0.0f);
                if(rActiveLevel->IsLightingActive())
                {
                    cShaderHandle = rDisplayManager->mLastProgramHandle;
                    ShaderUniform1i(cShaderHandle, 1, "uUseMixer");
                    uint32_t tColorHandle = sglGetUniformLocation(cShaderHandle, "uMixer");
                    sglUniform4f(tColorHandle, 1.0f, 1.0f - (tPercent), 0.0f, 1.0f);
                }
            }

            //--Setup.
            SugarBitmap *rWhitePixel = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/System/System/WhitePixel");
            if(rWhitePixel)
            {
                //--Bind.
                rWhitePixel->Bind();

                //--Render.
                glLineWidth(4.0f);
                glBegin(GL_LINE_STRIP);
                    for(int i = 0; i < (int)(360.0f * tPercent); i += 5)
                    {
                        float cRads = (i-90.0f) * TORADIAN;
                        glVertex2f(cosf(cRads)  * 13.0f, sinf(cRads)  * 13.0f);
                    }
                glEnd();

                //--Lighting clean.
                if(rActiveLevel->IsLightingActive())
                {
                    ShaderUniform1i(cShaderHandle, 0, "uUseMixer");
                }
            }

            //--Clean.
            glLineWidth(1.0f);
            StarlightColor::ClearMixer();
            glTranslatef(-17.0f, -2.0f, 0.0f);
        }
    }

    ///--[Mugged]
    //--If stunned due to mugging, render stars.
    if(mMugStunTimer > 0)
    {
        //--Timer. Normally the stun timer ticks down.
        float cTicksPerFrame = 3.0f;
        int cUseTimer = TA_MUG_STUN_TICKS - mMugStunTimer;
        int cUseFrame = (int)(cUseTimer / cTicksPerFrame) % TA_MUG_STUN_FRAMES_TOTAL;

        //--Render.
        if(rMugStun[cUseFrame]) rMugStun[cUseFrame]->Draw();
    }

    ///--[Flashwhite]
    //--Renders a white overlay with stencils. Used for transformations.
    if(mIsShowingFlashwhiteSequence)
    {
        //--Now switch to white blending.
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilFunc(GL_EQUAL, 2, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        glStencilMask(0xFF);

        //--Blend based on the timing.
        float tPercent = 0.0f;
        if(mFlashwhiteTimer < FLASHWHITE_TICKS_UP)
        {
            tPercent = EasingFunction::QuadraticOut(mFlashwhiteTimer, FLASHWHITE_TICKS_UP);
        }
        else if(mFlashwhiteTimer < FLASHWHITE_TICKS_UP + FLASHWHITE_TICKS_HOLD)
        {
            tPercent = 1.0f;
        }
        else
        {
            tPercent = 1.0f - EasingFunction::QuadraticOut(mFlashwhiteTimer - FLASHWHITE_TICKS_UP - FLASHWHITE_TICKS_HOLD, FLASHWHITE_TICKS_DOWN);
        }

        //--If Lighting is currently active, toggle this in the shader.
        if(rActiveLevel->IsLightingActive())
        {
            cShaderHandle = rDisplayManager->mLastProgramHandle;
            ShaderUniform1i(cShaderHandle, (int)(tPercent * 100.0f), "uIsFlashwhite");
        }

        //--Render.
        glDisable(GL_TEXTURE_2D);
        glColor4f(1.0f, 1.0f, 1.0f, tPercent);
        rRenderFrame->Draw();

        //--Clean up.
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_STENCIL_TEST);
        StarlightColor::ClearMixer();
        if(rActiveLevel->IsLightingActive())
        {
            cShaderHandle = rDisplayManager->mLastProgramHandle;
            ShaderUniform1i(cShaderHandle, 0, "uIsFlashwhite");
        }
    }

    ///--[Clean Up]
    glTranslatef(tXPosition * -1.0f, tYPosition * -1.0f, tZPosition * -1.0f);

    ///--[Debug]
    if(false)
    {
        //--Setup.
        glLineWidth(3.0f);
        glDisable(GL_TEXTURE_2D);

        //--Blue dynamic position.
        glColor3f(0.0f, 0.0f, 1.0f);
        glTranslatef(mTrueX, mTrueY, 0.0f);
        glBegin(GL_LINE_LOOP);
            glVertex2f(   0.0f,    0.0f);
            glVertex2f(TA_SIZE,    0.0f);
            glVertex2f(TA_SIZE, TA_SIZE);
            glVertex2f(   0.0f, TA_SIZE);
        glEnd();
        glTranslatef(mTrueX * -1.0f, mTrueY * -1.0f, 0.0f);

        //--Activation hitbox.
        glColor3f(0.0f, 1.0f, 0.0f);
        glBegin(GL_LINE_LOOP);
            glVertex2f(mActivationDim.mLft, mActivationDim.mTop);
            glVertex2f(mActivationDim.mRgt, mActivationDim.mTop);
            glVertex2f(mActivationDim.mRgt, mActivationDim.mBot);
            glVertex2f(mActivationDim.mLft, mActivationDim.mBot);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        glLineWidth(1.0f);
        glColor3f(1.0f, 1.0f, 1.0f);
    }
}
void TilemapActor::RenderUI()
{
    //--[Documentation and Setup]
    //--Renders the UI component of the entity, which is text or bars that may appear above or around them
    //  rather than on the same layer in the world.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    SugarBitmap *rRenderFrame = ResolveFrame();
    if(!rRenderFrame) return;

    //--Positions.
    float tXPosition = mTrueX + mOffsetX - TRUE_OFF_X;
    float tYPosition = mTrueY + mOffsetY - TRUE_OFF_Y;

    //--[Reinforcement Display]
    //--Only showed when the level is pulsing. Renders above the layers on the same depth as the UI.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    if(mLastComputedReinforcement > 0 && xrReinforcementFont && rActiveLevel && rActiveLevel->IsPulsing())
    {
        //--Buffer the reinforcement number.
        char tBuffer[32];
        sprintf(tBuffer, "%i Turns!", mLastComputedReinforcement-1);

        //--Compute the size.
        float tSize = 0.5f + (mReinforcementTimer / 10.0f) * 0.5f;
        if(tSize >= 1.25f)
        {
            tSize = 1.25f - (tSize - 1.25f);
            if(tSize < 1.0f) tSize = 1.0f;
        }

        //--Compute Y factor.
        float tYFactor = EasingFunction::QuadraticInOut(mReinforcementTimer, 15.0f) * 10.0f;
        if(tYFactor > 10.0f) tYFactor = 10.0f;
        tYFactor = -10.0f;

        //--Compute rendering position.
        float tReinforceX = tXPosition - (xrReinforcementFont->GetTextWidth(tBuffer) * 0.5f * tSize) + (rRenderFrame->GetTrueWidth() * 0.5f);
        float tReinforceY = tYPosition - tYFactor;

        //--Render.
        xrReinforcementFont->DrawTextArgs(tReinforceX, tReinforceY, 0, tSize, tBuffer);
        return;
    }

    //--[Ambush]
    //--Ambushed the enemy, shows "Preemptive Strike!"
    if(mStunTimer == 45 && xrReinforcementFont && rActiveLevel && rActiveLevel->IsPulsing() && rAdventureCombat->DoesPlayerHaveInitiative() && !mFollowTarget && !mIsDying)
    {
        //--Compute the size.
        float tSize = 0.5f + ((mReinforcementTimer / 10.0f) * 0.5f);
        if(tSize >= 1.25f)
        {
            tSize = 1.25f + (1.25f - tSize);
            if(tSize < 1.0f) tSize = 1.0f;
        }
        else if(tSize < 0.5f)
        {
            tSize = 0.50f;
        }

        //--Compute Y factor.
        float tYFactor = EasingFunction::QuadraticInOut(mReinforcementTimer, 15.0f) * 10.0f;
        if(tYFactor > 10.0f) tYFactor = 10.0f;
        tYFactor = -10.0f;

        //--Compute rendering position.
        float tReinforceX = tXPosition - (xrReinforcementFont->GetTextWidth("Preemptive Strike!") * 0.5f * tSize) + (rRenderFrame->GetTrueWidth() * 0.5f);
        float tReinforceY = tYPosition - tYFactor;

        //--Render.
        xrReinforcementFont->DrawText(tReinforceX, tReinforceY, 0, tSize, "Preemptive Strike!");
        return;
    }

    //--[Mugging]
    //--Renders a bar indicating the enemy's mugging status.
    RenderMugBar();

    //--[Other Display]
    if(mUIDisplayString && xrReinforcementFont)
    {
        //--Compute the size.
        int tUseTicks = TA_DISPLAY_STRING_TICKS * 0.666f;
        float tSize = 0.5f + (((float)mDisplayStringTimer / (float)tUseTicks) * 0.5f);
        if(tSize >= 1.25f)
        {
            tSize = 1.25f + (1.25f - tSize);
            if(tSize < 1.0f) tSize = 1.0f;
        }
        else if(tSize < 0.5f)
        {
            tSize = 0.50f;
        }

        //--Compute rendering position.
        float cWidth = (xrReinforcementFont->GetTextWidth(mUIDisplayString) * 0.5f * tSize);
        float tPositionX = tXPosition + TA_DISPLAY_STRING_OFFSET_X - cWidth + (rRenderFrame->GetTrueWidth() * 0.5f);
        float tPositionY = tYPosition + TA_DISPLAY_STRING_OFFSET_Y;

        //--Render.
        xrReinforcementFont->DrawText(tPositionX, tPositionY, 0, tSize, mUIDisplayString);
        return;
    }
}
void TilemapActor::RenderMugBar()
{
    //--[Documentation and Setup]
    //--Renders the mug bar above the enemy, indicating their mugging progress.
    if(mMugTimer < 1 || mIsDying || !rMugBarEmpty || !rMugBarFull) return;

    //--[Positions]
    //--Entity position.
    float cBarOffsetX = 5.0f;
    float cBarOffsetY = 1.0f;
    float tXPosition = mTrueX + mOffsetX - TRUE_OFF_X + cBarOffsetX;
    float tYPosition = mTrueY + mOffsetY - TRUE_OFF_Y + cBarOffsetY;

    //--[Base]
    //--Render the base bar. No computations are needed.
    rMugBarEmpty->Draw(tXPosition, tYPosition);

    //--[Fill]
    //--Compute the percentage to render.
    float cPercent = (float)mMugTimer / (float)TA_MUG_TICKS_TOTAL;

    //--Get sizes.
    float cSizeX = rMugBarFull->GetWidth();
    float cSizeY = rMugBarFull->GetHeight();
    float cBorder = 2.0f;

    //--Let side is the "Full" bar.
    float cLft = tXPosition + cBorder - 2.0f;
    float cTop = tYPosition + cBorder - 2.0f;
    float cRgt = cLft + (cSizeX * cPercent);
    float cBot = cTop + cSizeY;
    float cTxL = 0.0f;
    float cTxT = 0.0f;
    float cTxR = cPercent;
    float cTxB = 1.0f;
    rMugBarFull->Bind();
    glBegin(GL_QUADS);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cBot);
    glEnd();
}
void TilemapActor::RenderVisibilityCone(bool pIsLightingActive, GLint pShaderHandle)
{
    //--Renders what the Actor can see. Must be called apart from the main render function since it uses transparency.
    //--When rendering with lights on, the function assumes that lighting has been setup for us. That is, the
    //  viewcone texture is already bound and the shader is activated and valid.
    if(!xRenderViewcone || !mIsEnemy || mIsDisabled || mIsIgnoringPlayer) return;

    //--Don't render a viewcone when stunned.
    if(mMugStunTimer > 0) return;

    //--Never render a viewcone if camouflage mode is activated.
    if(AdventureDebug::xCamouflageMode) return;

    //--Get the level and check for a level transition.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel->IsLevelTransitioning())
    {
        return;
    }

    //--Bag-Refills don't render.
    if(mIsBagRefill) return;

    //--Enemies who follow someone also don't render their viewcones.
    if(mFollowTarget) return;

    //--Enemies who are leashing don't render a viewcone.
    if(mAIState == TA_AI_LEASH) return;

    //--Don't render viewcone when dying. Or when not actually hostile.
    if(mIsDying || !mEnemyListing || mEnemyListing->GetListSize() < 1) return;

    //--Don't render viewcones if on a different Z level from the player.
    float tPlayerX, tPlayerY, tPlayerZ;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);
    if(mCollisionDepth != tPlayerZ) return;
    mCurrentViewAngle = (mFacing * 45.0f) - 90.0f;

    //--Setup.
    glLineWidth(2.0f);

    //--Determine color.
    StarlightColor cRenderColor;
    if(mAIState != TA_AI_CHASE)
    {
        float tDetectPercent = (float)mSpottedPlayerTicks / (float)mSpottedPlayerTicksMax;
        if(rActiveLevel->GetRetreatTimer() > 0 && tDetectPercent < 0.50f)
        {
            tDetectPercent = 0.50f;
        }
        cRenderColor.SetRGBAF(1.0f, 1.0f - (tDetectPercent), 0.0f, 1.0f);
    }
    else
    {
        cRenderColor.SetRGBAF(1.0f, 0.0f, 0.0f, 1.0f);
    }

    //--Position and begin rendering.
    float cCenterX = mTrueX + 4.0f;
    float cCenterY = mTrueY + 4.0f;

    //--Determine view distance. This is modified by some scripts.
    float tUseViewDistance = mViewDistance;
    if(rActiveLevel->GetRetreatTimer() > 0) tUseViewDistance = mViewDistance * 2.0f;

    //--[Triangle Fan Version]
    //--Designed to work with the raytracer.
    if(true)
    {
        //--Determine the accuracy, in degrees.
        //int cAccuracy = (int)(mViewAngle * 0.05f);
        float cAccuracy = 1.2f;
        if(cAccuracy < 1.0f) cAccuracy = 1.0f;

        //--GL Setup.
        glDepthMask(false);
        glDisable(GL_DEPTH_TEST);
        glBegin(GL_TRIANGLE_FAN);

            //--Starting point. Set lights.
            glColor4f(cRenderColor.r, cRenderColor.g, cRenderColor.b, 0.0f);
            glTexCoord2f(0.5f, 0.5f);
            glVertex2f(cCenterX, cCenterY);

            //--Run across the list.
            for(float i = 0; i < (float)mViewAngle; i = i + cAccuracy)
            {
                //--Compute.
                float cAngle = (mCurrentViewAngle + (float)i - (mViewAngle / 2.0f)) * TORADIAN;

                //--Determine the interception point.
                float cEndX, cEndY;
                rActiveLevel->GetLineCollision(mCollisionDepth, cCenterX, cCenterY, cCenterX + (cosf(cAngle) * tUseViewDistance), cCenterY + (sinf(cAngle) * tUseViewDistance), cEndX, cEndY);

                //--Get the final distance.
                float cDistance = GetPlanarDistance(cCenterX, cCenterY, cEndX, cEndY);
                float cLightPercent = cDistance / tUseViewDistance;
                glColor4f(cRenderColor.r, cRenderColor.g, cRenderColor.b, cLightPercent * 0.75f);

                //--Render.
                glVertex2f(cEndX, cEndY);
            }

            //--Starting point at loop end.
            glColor4f(cRenderColor.r, cRenderColor.g, cRenderColor.b, 0.0f);
        glEnd();
        glEnable(GL_DEPTH_TEST);
        glDepthMask(true);
    }
    //--[Polygon Version]
    //--Incompatible with the raytracer.
    else
    {
        //--GL Setup.
        glTranslatef(cCenterX, cCenterY, 0.0f);
        glScalef(mViewDistance, mViewDistance, 1.0f);
        glBegin(GL_POLYGON);

            //--Starting point. Set lights.
            glColor4f(cRenderColor.r, cRenderColor.g, cRenderColor.b, 0.0f);

            //--Render a vertex. The texture position never changes since it's all white anyway.
            glTexCoord2f(0.5f, 0.5f);
            glVertex2f(0.0f, 0.0f);

            //--Outer edges.
            glColor4f(cRenderColor.r, cRenderColor.g, cRenderColor.b, 0.45f);

            //--Run across the list.
            for(int i = 0; i < (int)mViewAngle; i ++)
            {
                //--Compute.
                float cAngle = (mCurrentViewAngle + (float)i - (mViewAngle / 2.0f)) * TORADIAN;

                glVertex2f(cosf(cAngle), sinf(cAngle));
            }

            //--Starting point at loop end.
            glColor4f(cRenderColor.r, cRenderColor.g, cRenderColor.b, 0.0f);
            glVertex2f(0.0f, 0.0f);

        glEnd();

        //--GL clean up.
        glScalef(1.0f / mViewDistance, 1.0f / mViewDistance, 1.0f);
        glTranslatef(cCenterX * -1.0f, cCenterY * -1.0f, 0.0f);
    }

    //--[Clean Up]
    glLineWidth(1.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
}
SugarBitmap *TilemapActor::ResolveFrame()
{
    //--Figures out which frame the TilemapActor should be rendering and returns it. Can legally return
    //  NULL in cases where the animation was not set or the case was unhandled.
    if(mIsShowingSpecialImage)
    {
        return rCurrentSpecialImage;
    }

    //--If this flag is set, always face up.
    int tUseFacing = mFacing;
    if(mAlwaysFacesUp) tUseFacing = TA_DIR_NORTH;

    //--Error check.
    if(tUseFacing < 0 || tUseFacing >= TA_DIR_TOTAL) return NULL;

    //--Void Rifts use their own special logic cases.
    if(mIsVoidRiftMode)
    {
        //--If the void rift is at a low timer, run the special frames.
        float cTicksPerFrame = 12.0f;
        if(mVoidRiftTimer < cTicksPerFrame * 4.0f)
        {
            int tFrame = ((int)(mVoidRiftTimer / cTicksPerFrame) % TA_MOVE_IMG_TOTAL);
            if(mVoidRiftFrame == 0) return rMoveImages[TA_DIR_NORTH][tFrame];
            if(mVoidRiftFrame == 1) return rMoveImages[TA_DIR_WEST][tFrame];
            if(mVoidRiftFrame == 2) return rMoveImages[TA_DIR_EAST][tFrame];
            return rMoveImages[TA_DIR_SOUTH][tFrame];
        }
        //--Otherwise, render the walk-cycle south.
        else
        {
            int tFrame = ((int)(mVoidRiftTimer / cTicksPerFrame) % TA_MOVE_IMG_TOTAL);
            return rMoveImages[TA_DIR_SOUTH][tFrame];
        }
    }

    //--If not moving, return the idle frame.
    if(!mIsMoving && !mAutoAnimates)
    {
        return rMoveImages[tUseFacing][0];
    }
    //--If moving, return animations.
    else
    {
        //--Select which set of frames to use.
        bool tUseRunArray = false;
        float cUseTicksPerFrame = mWalkTicksPerFrame;
        if(mIsRunning || mRanLastTick)
        {
            tUseRunArray = true;
            cUseTicksPerFrame = mRunTicksPerFrame;
        }

        //--Normal case:
        if(!mUsesSpecialIdleImages)
        {
            //--Compute the frame.
            int tUseTimer = mMoveTimer;
            if(tUseTimer < 0) tUseTimer = 0;
            int tFrame = ((int)(tUseTimer / cUseTicksPerFrame) % TA_MOVE_IMG_TOTAL);

            //--Use the regular movement array.
            if(!tUseRunArray || !rRunImages[tUseFacing][tFrame])
            {
                return rMoveImages[tUseFacing][tFrame];
            }
            //--Running array.
            else
            {
                return rRunImages[tUseFacing][tFrame];
            }
        }
        //--Special idle. Uses frames 1-2-3-2-1-2-3 instead of a wrapping loop.
        else
        {
            //--Compute the frame.
            int tUseTimer = mMoveTimer;
            if(tUseTimer < 0) tUseTimer = 0;
            int tFrame = ((int)(tUseTimer / cUseTicksPerFrame) % TA_MOVE_IMG_TOTAL);
            if(tFrame == 0) tFrame = 2;

            //--Use the regular movement array.
            if(!tUseRunArray)
            {
                return rMoveImages[tUseFacing][tFrame];
            }
            //--Running array.
            else
            {
                return rRunImages[tUseFacing][tFrame];
            }
        }
    }

    //--Unhandled case, returns NULL.
    return NULL;
}
SugarBitmap *TilemapActor::GetMapImage(int pTimer)
{
    //--Image used for representing this character on the map screen.
    int tFrame = ((int)(pTimer / mWalkTicksPerFrame) % TA_MOVE_IMG_TOTAL);
    return rMoveImages[TA_DIR_SW][tFrame];
}
