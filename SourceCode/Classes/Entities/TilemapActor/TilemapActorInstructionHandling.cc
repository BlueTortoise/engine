//--Base
#include "TilemapActor.h"

//--Classes
#include "TiledLevel.h"
#include "TileLayer.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"

bool TilemapActor::HandleInstruction(int pInstructionCode)
{
    //--Given an instruction code, the TilemapActor will attempt to carry out that instruction. If they
    //  are unable to so, then false is returned, otherwise true is returned.
    if(pInstructionCode < 0 || pInstructionCode > TA_INS_HIGHEST) return false;

    //--Already handling an instruction, fail.
    if(IsHandlingInstruction()) return false;

    //--Level, used for collision information. If no level is found, collisions always pass. If the player
    //  is currently standing on a collision, this check also passes as a failsafe.
    TiledLevel *rActiveLevel = TiledLevel::Fetch();
    if(rActiveLevel && rActiveLevel->GetClipAt(mTileX * TileLayer::cxSizePerTile, mTileY * TileLayer::cxSizePerTile, 0.0f)) rActiveLevel = NULL;

    //--Set the instruction.
    mCurrentInstruction = pInstructionCode;

    //--Move North.
    if(mCurrentInstruction == TA_INS_MOVE_NORTH)
    {
        //--Collision passes, or level doesn't exist: Move.
        if(!rActiveLevel || !rActiveLevel->GetClipAt(mTileX * TileLayer::cxSizePerTile, (mTileY - 1.0f) * TileLayer::cxSizePerTile, 0.0f))
        {
            mIsMoving = true;
            mFacing = TA_DIR_NORTH;
            mMoveDirection = TA_DIR_NORTH;
        }
        //--Instruction fails immediately, but facing changes.
        else
        {
            mFacing = TA_DIR_NORTH;
            mCurrentInstruction = TA_INS_NONE;
        }
    }
    //--Move East.
    else if(mCurrentInstruction == TA_INS_MOVE_EAST)
    {
        //--Collision passes, or level doesn't exist: Move.
        if(!rActiveLevel || !rActiveLevel->GetClipAt((mTileX + 1.0f) * TileLayer::cxSizePerTile, mTileY * TileLayer::cxSizePerTile, 0.0f))
        {
            mIsMoving = true;
            mFacing = TA_DIR_EAST;
            mMoveDirection = TA_DIR_EAST;
        }
        //--Instruction fails immediately, but facing changes.
        else
        {
            mFacing = TA_DIR_EAST;
            mCurrentInstruction = TA_INS_NONE;
        }
    }
    //--Move South.
    else if(mCurrentInstruction == TA_INS_MOVE_SOUTH)
    {
        //--Collision passes, or level doesn't exist: Move.
        if(!rActiveLevel || !rActiveLevel->GetClipAt(mTileX * TileLayer::cxSizePerTile, (mTileY + 1.0f) * TileLayer::cxSizePerTile, 0.0f))
        {
            mIsMoving = true;
            mFacing = TA_DIR_SOUTH;
            mMoveDirection = TA_DIR_SOUTH;
        }
        //--Instruction fails immediately, but facing changes.
        else
        {
            mFacing = TA_DIR_SOUTH;
            mCurrentInstruction = TA_INS_NONE;
        }
    }
    //--Move East.
    else if(mCurrentInstruction == TA_INS_MOVE_WEST)
    {
        //--Collision passes, or level doesn't exist: Move.
        if(!rActiveLevel || !rActiveLevel->GetClipAt((mTileX - 1.0f) * TileLayer::cxSizePerTile, mTileY * TileLayer::cxSizePerTile, 0.0f))
        {
            mIsMoving = true;
            mFacing = TA_DIR_WEST;
            mMoveDirection = TA_DIR_WEST;
        }
        //--Instruction fails immediately, but facing changes.
        else
        {
            mFacing = TA_DIR_WEST;
            mCurrentInstruction = TA_INS_NONE;
        }
    }
    //--Unhandled instruction.
    else
    {
        mCurrentInstruction = TA_INS_NONE;
        DebugManager::ForcePrint("TilemapActor:HandleInstruction - Error, unhandled instruction %i\n", pInstructionCode);
        return false;
    }

    //--All checks passed, return true.
    return true;
}
void TilemapActor::EnqueueInstruction(int pInstructionCode, bool pIsPlayerControl)
{
    //--Method used to add instructions to the instruction queue. The flag pIsPlayerControl indicates
    //  that this should be the first instruction or the second, and to clear any instructions in those
    //  slots. This allows the player to seamlessly transition keypresses.
    //--If not a player control, then just append it to the end of the queue.
    if(!pIsPlayerControl)
    {
        SetMemoryData(__FILE__, __LINE__);
        int *nIntPtr = (int *)starmemoryalloc(sizeof(int));
        *nIntPtr = pInstructionCode;
        mInstructionList->AddElementAsTail("X", nIntPtr, &FreeThis);
        return;
    }

    //--Player controls. If there are no instructions, then just append to the queue.
    if(mInstructionList->GetListSize() < 1)
    {
        SetMemoryData(__FILE__, __LINE__);
        int *nIntPtr = (int *)starmemoryalloc(sizeof(int));
        *nIntPtr = pInstructionCode;
        mInstructionList->AddElementAsTail("X", nIntPtr, &FreeThis);
        return;
    }

    //--If we got this far, there is already an instruction in the 0th slot. The 1th slot therefore
    //  becomes this instruction, and anything else is cleared.
    while(mInstructionList->RemoveElementI(0) != NULL)
    {

    }

    //--Now append it.
    SetMemoryData(__FILE__, __LINE__);
    int *nIntPtr = (int *)starmemoryalloc(sizeof(int));
    *nIntPtr = pInstructionCode;
    mInstructionList->AddElementAsTail("X", nIntPtr, &FreeThis);
}
void TilemapActor::WipeInstructions()
{
    mInstructionList->ClearList();
}
