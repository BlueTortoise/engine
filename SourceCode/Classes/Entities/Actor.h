//--[Actor]
//--An entity on the field. Can be an item or a character. Actors are what take up space in
//  rooms. They also can have AIs, though items never do.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootEntity.h"
struct ActorIconPack;

//--[Local Structures]
//--[Local Definitions]
#define TEAM_NONE 0x00
#define TEAM_PLAYER 0x01
#define TEAM_MONSTERS 0x02

#define RENDER_HUMAN 0
#define RENDER_MONSTER 1
#define RENDER_TUBE 2
#define RENDER_MIRROR 3
#define RENDER_NEST 4
#define RENDER_GLYPH 5
#define RENDER_COFFIN 6

#define MAX_PENDING_INSTRUCTIONS 32

#define NAVIGATION_OFFSET 1

#define INSTRUCTION_COMPLETETURN -2
#define INSTRUCTION_NOENTRY -1
#define INSTRUCTION_SKIPTURN 0
#define INSTRUCTION_MOVENORTH (NAV_NORTH + NAVIGATION_OFFSET)
#define INSTRUCTION_MOVEEAST (NAV_EAST + NAVIGATION_OFFSET)
#define INSTRUCTION_MOVESOUTH (NAV_SOUTH + NAVIGATION_OFFSET)
#define INSTRUCTION_MOVEWEST (NAV_WEST + NAVIGATION_OFFSET)
#define INSTRUCTION_MOVEUP (NAV_UP + NAVIGATION_OFFSET)
#define INSTRUCTION_MOVEDOWN (NAV_DOWN + NAVIGATION_OFFSET)

#define PRINTCODE_NONE 0
#define PRINTCODE_PICKUP_ITEM 1
#define PRINTCODE_EQUIP_ITEM 2
#define PRINTCODE_DROP_ITEM 3
#define PRINTCODE_DRINK_HEALTH_POTION 4
#define PRINTCODE_STANDARD_ATTACK 5 //Note: Don't use this! It's a placeholder.
#define PRINTCODE_KO_PERMANENT 6
#define PRINTCODE_KO_NONPERMANENT 7
#define PRINTCODE_REVIVE_FROM_TIMEOUT 8
#define PRINTCODE_MOVE_EXIT 9
#define PRINTCODE_MOVE_ENTER 10
#define PRINTCODE_MOVE_FLEEING_EXIT 11
#define PRINTCODE_MOVE_FLEEING_ENTER 12
#define PRINTCODE_MOVE_CANTFLEE 13
#define PRINTCODE_MOVE_WAIT 14
#define PRINTCODE_KO_WILLPOWER 15
#define PRINTCODE_REVIVE_FROM_WILLPOWER 16
#define PRINTCODE_DRINK_WILL_POTION 17
#define PRINTCODE_DRINK_ELIXIR 18
#define PRINTCODE_TIME_PASSES_STUNNED 19

#define SPECIAL_DEACTIVATE_MANUAL_POSITION -31013

//--[Classes]
class Actor : public RootEntity
{
    private:
    //--System
    char *mDescription;

    //--Manual Positioning
    bool mHasManualPosition;
    int mManualX;
    int mManualY;
    int mManualZ;

    //--Control Scheme
    bool mIsPlayerControlled;

    //--Obscuring
    int mLastSetVisible;
    int mIsVisibleTimer;
    SugarLinkedList *mObscuringList;

    //--AIs
    int mPendingOverrideInstruction;
    int mLastInstruction;
    char *mAIScript;
    char *mForceOverrideScript;
    char *mWakeupScript;
    uint8_t mTeamFlags;

    //--Combat Statistics
    bool mHasCombatStats;
    bool mFleesFromCombat;
    bool mLastMoveWasFlee;
    int mStaminaRegenTimer;
    int mWillpowerRegenTimer;
    CombatStats mCombatStats;
    InventoryItem *mWeapon;
    InventoryItem *mArmor;

    //--Bane Properties
    SugarLinkedList *mBanePropertyList;

    //--Combat Phrasing
    char *mAsPlayerAttackPhrase;
    char *mAsEnemyAttackPhrase;
    char *mAsNeutralAttackPhrase;

    //--Knockout Behavior
    bool mHasDecrementedStunThisTurn;
    bool mPersistsAfterKnockout;
    bool mRecoversToFullHP;
    int mTurnsToRecover;
    int mScoreForKill;

    //--Area Knowledge
    bool mIsMovementRestricted;
    PandemoniumRoom *rPreviousRoom;
    PandemoniumRoom *rCurrentRoom;

    //--Turn Handling
    bool mHasHandledTurn;
    int mPendingInstructionsTotal;
    int mPendingInstructionList[MAX_PENDING_INSTRUCTIONS];

    //--Display
    uint16_t mRenderFlag;
    StarlightColor mLocalColor;

    //--Inventory Control
    bool mIgnoresItems;
    bool mReconstituteInventory;
    InventorySubClass *mInventory;

    //--Images
    SugarBitmap *rActiveImage;
    SugarLinkedList *mImageList;

    protected:

    public:
    //--System
    Actor();
    virtual ~Actor();

    //--Public Variables
    int mLastRenderSlot;
    ActorIconPack *mIconPack;

    //--Public Statics
    static bool xAllowOneWeapon;
    static bool xAllowOneArmor;
    static bool xAreControlsEnabled;
    static int xInstruction;
    static bool xWakeUpFlag;
    static bool xPrintCombatRolls;
    static bool xPrintPaddingLines;
    static const char *xrCorrupterCheckName;
    static int xBaseFleeCost;

    //--Property Queries
    uint8_t GetTeam();
    char *GetDescription();
    char *GetOverrideScript();
    bool IsPlayerControlled();
    bool PersistsThroughKnockout();
    int GetWorldX();
    int GetWorldY();
    int GetWorldZ();
    PandemoniumRoom *GetLocation();
    virtual bool HasHandledTurn();
    uint16_t GetRenderFlag();
    StarlightColor GetLocalColor();
    SugarBitmap *GetActiveImage();
    SugarBitmap *GetImageRef(const char *pImageName);
    int GetLastInstruction();
    bool IsStunned();
    bool IsControlled();
    bool IgnoresItems();
    int GetTurnsToRecovery();
    bool IsObscured();
    bool IsObscuredBy(uint32_t pID);
    int GetVisibilityTimer();
    bool JustBecameInvisible();
    bool HasBane(const char *pName);
    bool CanChangeRooms(const char *pRoomName);

    //--Manipulators
    void SetPlayerControl(bool pFlag);
    void SetWorldPosition(int pX, int pY, int pZ);
    void SetDescription(const char *pDescription);
    void SetTeam(uint8_t pFlags);
    void SetIgnoresItems(bool pFlag);
    void SetRoom(PandemoniumRoom *pRoom);
    void SetRoomByName(const char *pName);
    void SetRenderFlag(uint16_t pFlag);
    void SetLocalColor(StarlightColor pColor);
    void SetAIScript(const char *pPath);
    void SetOverrideScript(const char *pPath);
    void SetWakeupScript(const char *pPath);
    void RegisterImageS(const char *pName, const char *pImagePath);
    void RegisterImageP(const char *pName, SugarBitmap *pImage);
    void SetActiveImageS(const char *pName);
    void SetActiveImageDL(const char *pDLPath);
    void SetHealth(int pAmount);
    void SetHealthBypass(int pAmount);
    void SetWillPower(int pAmount);
    void ResetWillpowerRegen();
    void SetStamina(int pAmount);
    void SetAsPlayerAttackPhrase(const char *pPhrase);
    void SetAsEnemyAttackPhrase(const char *pPhrase);
    void SetAsNeutralAttackPhrase(const char *pPhrase);
    void AddObscurer(uint32_t pID);
    void RemoveObscurer(uint32_t pID);
    void AddBane(const char *pName);
    void SetMovementRestriction(bool pFlag);
    void IncrementVisible();
    void DecrementVisible();
    void AppendNoMoveErrorToConsole(const char *pRoomTarget);

    //--Combat
    bool HasCombatStatistics();
    bool FleesFromCombat();
    bool WasLastMoveFlee();
    CombatStats GetCombatStatistics();
    int GetWeaponDefenseBonus();
    void SetCombatStatistics(CombatStats pBaseCopy);
    void SetFleeingFlag(bool pFlag);
    void SetPersistFlag(bool pFlag);
    void SetRecoverToFullFlag(bool pFlag);
    void SetTurnsToRecover(int pTurns);
    void SetScoreForKill(int pPoints);
    void AttackTarget(uint32_t pTargetID);
    void AttackTarget(Actor *pTarget);
    void RespondToKilled(bool pBlockPrintout);
    void RespondToConfounded(bool pBlockPrintout);
    void Revive(bool pBlockPrintout);

    //--Context Menu Control
    void PopulateContextMenuBy(Actor *pActor, ContextMenu *pMenu);

    //--Inventory Control
    bool GetInventoryReconstituteFlag();
    int GetInventorySize();
    int GetItemCount(const char *pItemName);
    InventoryItem *GetItemBySlot(int pSlot);
    void SetReconstituteFlag(bool pValue);
    void PickUpItemByName(const char *pItemName);
    void PickUpItemByPtr(InventoryItem *pPtr);
    void RegisterItem(InventoryItem *pItem);
    void ReceiveItem(InventoryItem *pItem);
    void ReceiveClonedItem(const char *pPrototypeName);
    void EquipWeapon(InventoryItem *pItem);
    void EquipArmor(InventoryItem *pItem);
    void UnequipWeapon();
    void UnequipArmor();
    void DropItem(InventoryItem *pItem);
    void DropItemBySlot(int pSlot);
    void DropItemByPtr(void *pPtr);
    void UseItemBySlot(int pSlot);
    void UseItemByName(const char *pName);
    void UseItemByPtr(void *pPtr);
    void UseItemBySlotOn(int pSlot, uint32_t pID);
    void UseItemByNameOn(const char *pName, uint32_t pID);
    void DropEverything();
    void LoseItem(const char *pName);
    void SwapWeaponsWith(uint32_t pID);

    //--Printing
    bool PrintByCode(uint32_t pCode, bool pPlayerMustBePresent, const char *pName1, const char *pName2, const char *pName3);
    static bool PrintMessage(const char *pMessage);
    static void PrintEntitiesInRoom(SugarLinkedList *pEntityList);

    //--Core Methods
    int DetermineFleeCost();
    void AppendInstructionS(const char *pInstruction);
    void AppendInstruction(int pInstruction);
    void HandleInstructionS(const char *pInstruction);
    void HandleInstruction(int pInstruction);
    void WipeInstructions();
    void AppendMoveToRoom(const char *pRoomName);
    int GetInstructionForMoveTo(const char *pRoomName);
    bool IsHostileTo(Actor *pOtherActor);
    bool IsHostileTo(uint8_t pTeamFlags);
    char *AssembleAttackPhrase(Actor *pVictim);
    void RecheckObscurers();
    float GetBaneOfTarget(uint32_t pTargetID);

    private:
    //--Private Core Methods

    public:
    //--Update
    virtual void HandleTurnUpdate();
    virtual void HandleEndOfTurn();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    PandemoniumRoom *GetCurrentRoom();
    InventoryItem *GetWeapon();
    InventoryItem *GetArmor();

    //--Static Functions
    static Actor *FindPlayer();
    static int GetInstructionFromString(const char *pInstruction);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Actor_Spawn(lua_State *L);
int Hook_Actor_ExistsWithName(lua_State *L);
int Hook_Actor_PathTo(lua_State *L);
int Hook_Actor_SetProperty(lua_State *L);
int Hook_Actor_GetProperty(lua_State *L);
int Hook_Actor_FlagTurnComplete(lua_State *L);
int Hook_Actor_PrintMessage(lua_State *L);
int Hook_Actor_IsXHostileToY(lua_State *L);
int Hook_Actor_PickUpItem(lua_State *L);
int Hook_Actor_SwapWeaponsWith(lua_State *L);
int Hook_Actor_UseItem(lua_State *L);
int Hook_Actor_DropEverything(lua_State *L);
int Hook_Actor_TripWakeup(lua_State *L);
int Hook_Actor_OverridePortrait(lua_State *L);
int Hook_Actor_GetBaneAgainst(lua_State *L);
int Hook_Actor_PushWeapon(lua_State *L);
