//--Base
#include "StarlightTextureLoader.h"

//--Classes
#include "SpineExpression.h"

//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
#include "spine/Atlas.h"
#include "spine/spine.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

//--Namepsace.
using namespace spine;

//=========================================== System ==============================================
StarlightTextureLoader::StarlightTextureLoader()
{

}
StarlightTextureLoader::~StarlightTextureLoader()
{

}
void StarlightTextureLoader::load(AtlasPage &page, const String& path)
{
    //--The image in question is stored at the static path, not the provided path.
    SugarBitmap *nBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(SpineExpression::xLoadPath);
    if(!nBitmap) return;

    //--Store the bitmap on the renderer object's void pointer. It will be used later during rendering.
    page.setRendererObject(nBitmap);

    //--Store the width and height so spine can compute the atlas sizes.
    page.width = nBitmap->GetWidth();
    page.height = nBitmap->GetHeight();
}
void StarlightTextureLoader::unload(void *texture)
{
    //--Handled by the DataLibrary.
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
