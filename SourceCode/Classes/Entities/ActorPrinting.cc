//--Base
#include "Actor.h"

//--Classes
#include "PandemoniumLevel.h"
#include "PandemoniumRoom.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

bool Actor::PrintByCode(uint32_t pCode, bool pPlayerMustBePresent, const char *pName1, const char *pName2, const char *pName3)
{
    //--Given a code, prints the given instructions to the console. This is context-sensitive, since
    //  if the Actor is not the Player, they will see a different string. Also, this function will do
    //  nothing if the player is not present and the override flag is false.
    //--If this is the entity performing the action, then the phrasing is "You [do] [x]". If someone
    //  else is doing it but the player sees it, it's "[person] [does] [x]". Therefore, the names passed
    //  in may not always be used. Generally, pName1 is the actor's name, and pName2 is the item's
    //  or target's name. pName3 may be an action.
    //--Returns true if it printed anything, false if not.
    if(!rCurrentRoom && pPlayerMustBePresent) return false;

    //--Check if the player must be present.
    if(rCurrentRoom && pPlayerMustBePresent)
    {
        Actor *rPlayer = rCurrentRoom->IsPlayerPresent();
        if(!rPlayer) return false;
    }

    //--Special: Player can't see! If this is the case, actions never render.
    if(!mIsPlayerControlled)
    {
        //--Find the Actor who is player controlled.
        Actor *rPlayerActor = Actor::FindPlayer();
        if(rPlayerActor && rPlayerActor->IsObscured()) return false;
    }

    //--Setup.
    float tOverrideFontSize = 0.0f; //If this is 0, the default font size is used.
    StarlightColor tOverrideColor = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f);
    bool tPrintExtraLine = false;
    char tBuffer[128];

    //--Now follow the switching list.
    if(pCode == PRINTCODE_NONE)
    {
        sprintf(tBuffer, "Erroneous code 0.");
    }
    //--Picking up an item.
    else if(pCode == PRINTCODE_PICKUP_ITEM)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You take the %s.", pName2);
        }
        else
        {
            sprintf(tBuffer, "%s takes the %s.", pName1, pName2);
        }
        tPrintExtraLine = true;
    }
    //--Equipping an item.
    else if(pCode == PRINTCODE_EQUIP_ITEM)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You equip the %s.", pName2);
        }
        else
        {
            sprintf(tBuffer, "%s equips the %s.", pName1, pName2);
        }
        tPrintExtraLine = true;
    }
    //--Dropping an item.
    else if(pCode == PRINTCODE_DROP_ITEM)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You drop the %s.", pName2);
        }
        else
        {
            sprintf(tBuffer, "%s drops the %s.", pName1, pName2);
        }
        tPrintExtraLine = true;
    }
    //--Drinking a health potion.
    else if(pCode == PRINTCODE_DRINK_HEALTH_POTION)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You drink the health potion, and immediately feel better.");
            tOverrideColor.SetRGBAF(0.0f, 0.5f, 0.0f, 1.0f);
        }
        else
        {
            sprintf(tBuffer, "%s drinks the health potion, looking much better.", pName1);
        }
        tPrintExtraLine = true;
    }
    //--Attacking an enemy. No fluff.
    else if(pCode == PRINTCODE_STANDARD_ATTACK)
    {
        sprintf(tBuffer, "Do not use this printcode for combat!");
    }
    //--Getting KO'd while a permanent Actor.
    else if(pCode == PRINTCODE_KO_PERMANENT)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You fall to the ground, too hurt to move!");
            tOverrideColor.SetRGBAF(0.5f, 0.0f, 0.0f, 1.0f);
        }
        else
        {
            sprintf(tBuffer, "%s falls to the ground, too hurt to move!", pName1);
        }
        tPrintExtraLine = true;
    }
    //--Getting KO'd while not a permanent Actor.
    else if(pCode == PRINTCODE_KO_NONPERMANENT)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You vanish in a puff of logic.");
            tOverrideColor.SetRGBAF(0.5f, 0.0f, 0.0f, 1.0f);
        }
        else
        {
            sprintf(tBuffer, "Defeated, %s vanishes with a *poof*.", pName1);
        }
        tPrintExtraLine = true;
    }
    //--Getting back up after being downed.
    else if(pCode == PRINTCODE_REVIVE_FROM_TIMEOUT)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You get back up!");
            tOverrideColor.SetRGBAF(0.0f, 0.5f, 0.0f, 1.0f);
        }
        else
        {
            sprintf(tBuffer, "%s gets back up!", pName1);
        }
        tPrintExtraLine = true;
    }
    //--Moving (Exit). This is called when an entity exits the room, *after* the entity has moved. It
    //  informs the player (if present) about who left and which way they went. pName3 will be the
    //  direction ("west", "east", "up").
    else if(pCode == PRINTCODE_MOVE_EXIT)
    {
        //--The player will never legally be present if they are seeing this.
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "Debug: %s can't be present for an exit.", pName1);
        }
        //--Normal case, announces the entity's exit.
        else
        {
            sprintf(tBuffer, "%s goes %s.", pName1, pName3);
        }
        tPrintExtraLine = true;
    }
    //--Moving (Enter). This is called when an entity enters the room the player is in, if applicable.
    //  It announces who has done the entering, and may provide extra data if fleeing or exiting
    //  a room with another undesirable.
    else if(pCode == PRINTCODE_MOVE_ENTER)
    {
        //--The player will print "Entered %s", which is the room's location.
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "Entered %s.", pName2);
            tOverrideFontSize = 20.0f;
        }
        //--Normal case, announces the entity's entry.
        else
        {
            sprintf(tBuffer, "%s arrives from %s.", pName1, pName3);
        }
        tPrintExtraLine = true;
    }
    //--Fleeing (Exit). Called when an entity exits the room, which contained a hostile entity, and
    //  the entity is capable of fleeing. Monsters typically aren't.
    else if(pCode == PRINTCODE_MOVE_FLEEING_EXIT)
    {
        //--The player will never legally be present if they are seeing this.
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "Debug: %s can't be present for a fleeing exit.", pName1);
        }
        //--Normal case, announces the entity's exit.
        else
        {
            sprintf(tBuffer, "%s runs %s, away from %s.", pName1, pName3, pName2);
        }
        tPrintExtraLine = true;
    }
    //--Fleeing (Enter). This is called when an entity has successfully fled an encounter and ends
    //  in the room the player is currently in.
    else if(pCode == PRINTCODE_MOVE_FLEEING_ENTER)
    {
        //--The player will print "Entered %s", which is the room's location.
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You run away! Entered %s.", pName2);
            tOverrideFontSize = 20.0f;
        }
        //--Normal case, announces the entity's entry.
        else
        {
            sprintf(tBuffer, "%s arrives from %s, running away from something.", pName1, pName3);
        }
        tPrintExtraLine = true;
    }
    //--Fleeing (Failure). Can't flee from lack of stamina.
    else if(pCode == PRINTCODE_MOVE_CANTFLEE)
    {
        //--You fool!
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You try to flee, but can't!");
            tOverrideColor.SetRGBAF(0.5f, 0.0f, 0.0f, 1.0f);
        }
        //--Normal case, announces the entity's entry.
        else
        {
            sprintf(tBuffer, "%s tries to flee but is blocked!", pName1);
        }
        tPrintExtraLine = true;
    }
    //--Time passes. Only players can do this.
    else if(pCode == PRINTCODE_MOVE_WAIT)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "Time passes.");
            tOverrideFontSize = 18.0f;
            tPrintExtraLine = true;
        }
        else
        {
            return false;
        }
    }
    //--Getting willpower KO'd.
    else if(pCode == PRINTCODE_KO_WILLPOWER)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You slump to the side, unable to focus on anything.");
            tOverrideColor.SetRGBAF(0.5f, 0.0f, 0.0f, 1.0f);
        }
        else
        {
            sprintf(tBuffer, "%s slumps to the side, totally unable to focus.", pName1);
        }
        tPrintExtraLine = true;
    }
    //--Getting back up after willpower loss.
    else if(pCode == PRINTCODE_REVIVE_FROM_WILLPOWER)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You straighten yourself, having recovered enough focus to act.");
            tOverrideColor.SetRGBAF(0.0f, 0.5f, 0.0f, 1.0f);
        }
        else
        {
            sprintf(tBuffer, "%s straightens herself, having recovered enough focus to act.", pName1);
        }
        tPrintExtraLine = true;
    }
    //--Recovering willpower with a potion.
    else if(pCode == PRINTCODE_DRINK_WILL_POTION)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You drink the potion, immediately feeling much more clear-headed.");
            tOverrideColor.SetRGBAF(0.0f, 0.5f, 0.0f, 1.0f);
        }
        else
        {
            sprintf(tBuffer, "%s drinks the potion, and begins looking much more clear-headed.", pName1);
        }
        tPrintExtraLine = true;
    }
    //--Someone uses an Elixir on you.
    else if(pCode == PRINTCODE_DRINK_ELIXIR)
    {
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You use the elixir to rejuvinate %s!\n", pName1);
        }
        else
        {
            sprintf(tBuffer, "%s has been rejuvinated by an elixir, thanks to %s!\n", pName1, pName2);
            tOverrideColor.SetRGBAF(0.0f, 0.5f, 0.0f, 1.0f);
        }
        tPrintExtraLine = true;
    }
    //--Time passes while the player is stunned.
    else if(pCode == PRINTCODE_TIME_PASSES_STUNNED)
    {
        //--This really should only be written by player controlled characters.
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "Time passes as you lie in a haze...\n");
            tOverrideFontSize = 18.0f;
        }
        else
        {
            sprintf(tBuffer, "Error! Only the player should use PRINTCODE_TIME_PASSES_STUNNED!\n");
        }
        tPrintExtraLine = true;
    }

    //--Error.
    else
    {
        sprintf(tBuffer, "Error, no print code %i.", pCode);
    }

    //--Now register it.
    PandemoniumLevel::AppendToConsoleStatic(tBuffer, tOverrideFontSize, tOverrideColor);
    if(tPrintExtraLine && xPrintPaddingLines) PandemoniumLevel::AppendToConsoleStatic(" ");
    return true;
}
bool Actor::PrintMessage(const char *pMessage)
{
    //--Static command to print a message directly. This is usually used for error messages, since
    //  most messages are meant to have two phrasings based on who is doing them.
    //--While it's basically a routing call to AppendToConsoleStatic(), it's here for completeness
    //  since it goes through the Actor's Lua hooks.
    if(!pMessage) return false;

    //--Register it.
    PandemoniumLevel::AppendToConsoleStatic(pMessage);
    return true;
}
void Actor::PrintEntitiesInRoom(SugarLinkedList *pEntityList)
{
    //--Prints a list of all the entities in a room, assuming the player can see them. This is usually
    //  called after PrintMessage() in order to make sure the player knows who/what is in the area.
    if(!pEntityList) return;

    //--First, count how many entities there are. Player-controlled entities are ignored.
    int tNonPlayerEntities = 0;
    Actor *rActor = (Actor *)pEntityList->PushIterator();
    while(rActor)
    {
        if(!rActor->IsPlayerControlled()) tNonPlayerEntities ++;
        rActor = (Actor *)pEntityList->AutoIterate();
    }

    //--No entities? Don't print anything.
    if(tNonPlayerEntities == 0)
    {
        //PandemoniumLevel::AppendToConsoleStatic(" ");
        return;
    }

    //--If we got this far, we need to construct a sentence describing the room's contents.
    int i = 0;
    char tBuffer[256];
    strcpy(tBuffer, ">You can see ");

    //--Loop.
    rActor = (Actor *)pEntityList->PushIterator();
    while(rActor)
    {
        //--Make sure it's not the entering actor.
        if(!rActor->IsPlayerControlled())
        {
            //--Flags.
            i ++;
            strcat(tBuffer, rActor->GetName());

            //--If that wasn't the last Actor in the list, print a comma.
            if(i < tNonPlayerEntities - 1)
            {
                strcat(tBuffer, ", ");
            }
            //--Proper grammar.
            else if(i == tNonPlayerEntities - 1)
            {
                //--Exactly two has a different structure.
                if(tNonPlayerEntities == 2)
                {
                    strcat(tBuffer, " and ");
                }
                //--More than two.
                else
                {
                    strcat(tBuffer, ", and ");
                }
            }
            //--Otherwise, end the sentence.
            else
            {
                strcat(tBuffer, " here.");
            }
        }

        //--Next.
        rActor = (Actor *)pEntityList->AutoIterate();
    }

    //--Send the buffer off.
    PandemoniumLevel::AppendToConsoleStatic(tBuffer);
    if(xPrintPaddingLines) PandemoniumLevel::AppendToConsoleStatic(" ");
}
