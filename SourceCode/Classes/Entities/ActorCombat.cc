//--Base
#include "Actor.h"

//--Classes
#include "InventoryItem.h"
#include "PandemoniumLevel.h"
#include "PandemoniumRoom.h"
#include "VisualLevel.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "EntityManager.h"

//--Property Queries
bool Actor::HasCombatStatistics()
{
    return mHasCombatStats;
}
bool Actor::FleesFromCombat()
{
    return mFleesFromCombat;
}
bool Actor::WasLastMoveFlee()
{
    return mLastMoveWasFlee;
}
CombatStats Actor::GetCombatStatistics()
{
    return mCombatStats;
}

//--Manipulators
void Actor::SetCombatStatistics(CombatStats pBaseCopy)
{
    //--Sets combat stats by copying this. If the HPMax in the base copy is 0, removes combat stats.
    mHasCombatStats = (pBaseCopy.mHPMax > 0);
    memcpy(&mCombatStats, &pBaseCopy, sizeof(CombatStats));

    //--Clamps.
    if(mCombatStats.mHPMax < 1) mCombatStats.mHPMax = 1;
    if(mCombatStats.mWillPowerMax < 1) mCombatStats.mWillPowerMax = 1;
    if(mCombatStats.mStaminaMax < 1) mCombatStats.mStaminaMax = 1;
}
void Actor::SetFleeingFlag(bool pFlag)
{
    mFleesFromCombat = pFlag;
}
void Actor::SetPersistFlag(bool pFlag)
{
    mPersistsAfterKnockout = pFlag;
}
void Actor::SetRecoverToFullFlag(bool pFlag)
{
    mRecoversToFullHP = pFlag;
}
void Actor::SetTurnsToRecover(int pTurns)
{
    if(pTurns < 0) pTurns = 0;
    mTurnsToRecover = pTurns;
}
void Actor::SetScoreForKill(int pPoints)
{
    mScoreForKill = pPoints;
}

//--Core Methods
void Actor::AttackTarget(uint32_t pTargetID)
{
    //--Overload, attacks the target by referencing its ID. We can't attack ourselves, silly!

    //--Flag us as attacking.
    mHasHandledTurn = true;
    Actor::xInstruction = INSTRUCTION_COMPLETETURN;

    //--Get the target and check it for legality.
    Actor *rTarget = (Actor *)EntityManager::Fetch()->GetEntityI(pTargetID);
    if(!rTarget || rTarget == this || rTarget->GetType() != POINTER_TYPE_ACTOR) return;

    //--Run the other routine.
    AttackTarget(rTarget);
}
void Actor::AttackTarget(Actor *pTarget)
{
    //--Attacks the target with our current loadout. This counts as using a turn. This also assumes
    //  that the range-rules are true (can't attack something in a different room) and that the
    //  target is a legal target (can't attack targets that are down).
    if(!pTarget) return;

    //--We can't attack ourselves!
    if(pTarget == this) return;

    //--This flag sets whether or not we print. The player must be in the room to print anything.
    bool tIsPlayerPresent = false;
    if(rCurrentRoom && rCurrentRoom->IsPlayerPresent())
    {
        tIsPlayerPresent = true;
    }

    //--Flag us as attacking.
    mHasHandledTurn = true;
    Actor::xInstruction = INSTRUCTION_COMPLETETURN;

    //--Variable collection.
    int tMyBaseAcc = mCombatStats.mAccuracy;
    int tMyWeapAcc = 0;
    int tMyRoll = (rand() % 10) + 1;
    if(mWeapon) tMyWeapAcc = mWeapon->GetAccuracyBonus();

    //--Target's defense.
    CombatStats tTheirStats = pTarget->GetCombatStatistics();
    int tTheirBaseDef = tTheirStats.mDefensePower;
    int tTheirWeapDef = pTarget->GetWeaponDefenseBonus();

    //--Hit or miss?
    bool tScoredHit = (tMyBaseAcc + tMyWeapAcc + tMyRoll >= tTheirBaseDef + tTheirWeapDef);
    char tImpactBuffer[8];
    if(tScoredHit)
    {
        strcpy(tImpactBuffer, "Hit!");
    }
    //--Missed!
    else
    {
        strcpy(tImpactBuffer, "Miss!");
    }

    //--Write to console. We must do this directly, printcodes aren't complex enough.
    char tBuffer[256];

    //--If the attacker is player-controlled, use a different sentence structure.
    if(mIsPlayerControlled)
    {
        sprintf(tBuffer, "You attack %s! (%i + %i + %i) vs (%i + %i): %s", pTarget->GetName(), tMyBaseAcc, tMyWeapAcc, tMyRoll, tTheirBaseDef, tTheirWeapDef, tImpactBuffer);
    }
    //--If the defender is player-controlled, use this sentence structure.
    else if(pTarget->IsPlayerControlled())
    {
        sprintf(tBuffer, "%s attacks you! (%i + %i + %i) vs (%i + %i): %s", mLocalName, tMyBaseAcc, tMyWeapAcc, tMyRoll, tTheirBaseDef, tTheirWeapDef, tImpactBuffer);
    }
    //--Non-player attacks non-player.
    else
    {
        sprintf(tBuffer, "%s attacks %s! (%i + %i + %i) vs (%i + %i): %s", mLocalName, pTarget->GetName(), tMyBaseAcc, tMyWeapAcc, tMyRoll, tTheirBaseDef, tTheirWeapDef, tImpactBuffer);
    }

    //--Append.
    if(tIsPlayerPresent)
    {
        PandemoniumLevel::AppendToConsoleStatic(tBuffer);
        if(Actor::xPrintPaddingLines) PandemoniumLevel::AppendToConsoleStatic(" ");
    }

    //--In the event of a hit, damage the target.
    if(tScoredHit)
    {
        //--Roll the damage.
        int tDamageRoll = (rand() % 3) + 2;
        int tBonus = 0;
        if(mWeapon) tBonus = mWeapon->GetAttackPower();
        int tFinalDamage = tDamageRoll + tBonus;

        //--Actually deal the damage here.
        int tHealthRemaining = tTheirStats.mHP - tFinalDamage;
        if(tHealthRemaining < 0) tHealthRemaining = 0;

        //--Player-controlled.
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "You deal %i damage to %s. (%i remaining)", tFinalDamage, pTarget->GetName(), tHealthRemaining);
        }
        //--If the defender is player-controlled, use this sentence structure.
        else if(pTarget->IsPlayerControlled())
        {
            sprintf(tBuffer, "%s deals %i damage to you. (%i remaining)", mLocalName, tFinalDamage, tHealthRemaining);
        }
        //--Non-player attacks non-player.
        else
        {
            sprintf(tBuffer, "%s deals %i damage to %s. (%i remaining)", mLocalName, tFinalDamage, pTarget->GetName(), tHealthRemaining);
        }

        //--Append.
        if(tIsPlayerPresent)
        {
            PandemoniumLevel::AppendToConsoleStatic(tBuffer);
            if(Actor::xPrintPaddingLines) PandemoniumLevel::AppendToConsoleStatic(" ");
        }

        //--If the target is ko'd, handle that here.
        pTarget->SetHealth(tHealthRemaining);
        if(tHealthRemaining < 1)
        {
            pTarget->RespondToKilled(false);
        }
    }
    //--For a miss, just print this sentence.
    else
    {
        //--Player-controlled.
        if(mIsPlayerControlled)
        {
            sprintf(tBuffer, "Your attack missed %s.", pTarget->GetName());
        }
        //--If the defender is player-controlled, use this sentence structure.
        else if(pTarget->IsPlayerControlled())
        {
            sprintf(tBuffer, "%s's attack missed you.", mLocalName);
        }
        //--Non-player attacks non-player.
        else
        {
            sprintf(tBuffer, "%s's attack missed %s.", mLocalName, pTarget->GetName());
        }

        //--Append.
        if(tIsPlayerPresent)
        {
            PandemoniumLevel::AppendToConsoleStatic(tBuffer);
            if(Actor::xPrintPaddingLines) PandemoniumLevel::AppendToConsoleStatic(" ");
        }
    }
}
void Actor::RespondToKilled(bool pBlockPrintout)
{
    //--Called when this actor is killed. For enemies that persist, they fall down stunned for
    //  15 turns. Enemies that don't persist award their score.
    if(mPersistsAfterKnockout)
    {
        //--Set the stun timer to 15.
        mTurnsToRecover = 15;

        //--Print.
        if(!pBlockPrintout)
        {
            //--Human.
            if(mIsPlayerControlled)
            {
                PrintByCode(PRINTCODE_KO_PERMANENT, true, mLocalName, "Null", "Null");
            }
            //--Enemy (former human).
            else
            {
                PrintByCode(PRINTCODE_KO_PERMANENT, true, mLocalName, "Null", "Null");
            }
        }
    }
    //--Score award. Enemy vanishes.
    else
    {
        //--Mark this target for deletion.
        mSelfDestruct = true;

        //--Print. This must be done before room removal, or the entity won't know if the
        //  player is present!
        if(!pBlockPrintout)
            PrintByCode(PRINTCODE_KO_NONPERMANENT, true, mLocalName, "Null", "Null");

        //--Remove self from the current room. This will prevent other entities from targetting us.
        SetRoom(NULL);

        //--Print. This is direct-to-console.
        if(mScoreForKill != 0 && false)
        {
            char tBuffer[256];
            sprintf(tBuffer, "You have been awarded %i points.", mScoreForKill);
            PandemoniumLevel::AppendToConsoleStatic(tBuffer);
            if(Actor::xPrintPaddingLines) PandemoniumLevel::AppendToConsoleStatic(" ");
        }

        //--Spawn a sprite to disappear.
        VisualLevel *rVisualLevel = VisualLevel::Fetch();
        if(rVisualLevel && mIsVisibleTimer > 0)
        {
            SetMemoryData(__FILE__, __LINE__);
            ExtraEntityRenderPack *nRenderPack = (ExtraEntityRenderPack *)starmemoryalloc(sizeof(ExtraEntityRenderPack));
            memset(nRenderPack, 0, sizeof(ExtraEntityRenderPack));
            nRenderPack->mParentID = GetID();
            nRenderPack->mRenderSlot = mLastRenderSlot;
            nRenderPack->mRenderTicksLeft = VL_ENTITY_FADE_TICKS;
            nRenderPack->rRenderImg = GetActiveImage();
            rVisualLevel->RegisterExtraEntityPack(nRenderPack);
        }
    }
}
void Actor::RespondToConfounded(bool pBlockPrintout)
{
    //--Called when the Actor's willpower hits zero. This confounded state prevents all action
    //  but, since Willpower slowly regenerates, the Actor does not count strictly as stunned.
    //--Monsters still vanish, though.
    if(mPersistsAfterKnockout)
    {
        //--Print.
        if(!pBlockPrintout)
        {
            //--Human.
            if(mIsPlayerControlled)
            {
                PrintByCode(PRINTCODE_KO_WILLPOWER, true, mLocalName, "Null", "Null");
            }
            //--Enemy (former human).
            else
            {
                PrintByCode(PRINTCODE_KO_WILLPOWER, true, mLocalName, "Null", "Null");
            }
        }
    }
    //--Score award. Enemy vanishes.
    else
    {
        //--Mark this target for deletion.
        mSelfDestruct = true;

        //--Print. This must be done before room removal, or the entity won't know if the
        //  player is present!
        if(!pBlockPrintout)
            PrintByCode(PRINTCODE_KO_NONPERMANENT, true, mLocalName, "Null", "Null");

        //--Remove self from the current room. This will prevent other entities from targetting us.
        SetRoom(NULL);

        //--Print. This is direct-to-console.
        if(mScoreForKill != 0 && false)
        {
            char tBuffer[256];
            sprintf(tBuffer, "You have been awarded %i points.", mScoreForKill);
            PandemoniumLevel::AppendToConsoleStatic(tBuffer);
            if(Actor::xPrintPaddingLines) PandemoniumLevel::AppendToConsoleStatic(" ");
        }
    }
}
void Actor::Revive(bool pBlockPrintout)
{
    //--Immediately revives the target, restoring HP based on their logic flags.
    mTurnsToRecover = 0;
    mHasHandledTurn = false;
    if(!pBlockPrintout) PrintByCode(PRINTCODE_REVIVE_FROM_TIMEOUT, true, mLocalName, "Null", "Null");

    //--Reset this timer.
    mWillpowerRegenTimer = 0;

    //--Recovery code.
    if(mRecoversToFullHP)
    {
        mCombatStats.mHP = mCombatStats.mHPMax;
        mCombatStats.mWillPower = mCombatStats.mWillPowerMax;
    }
    //--Partial recovery.
    else
    {
        //--Health.
        if(mCombatStats.mHP < mCombatStats.mHPMax / 4)
        {
            mCombatStats.mHP = mCombatStats.mHPMax / 4;
        }

        //--Willpower.
        if(mCombatStats.mWillPower < mCombatStats.mWillPowerMax / 4)
        {
            mCombatStats.mWillPower = mCombatStats.mWillPowerMax / 4;
        }

        //--Edge clamps.
        if(mCombatStats.mHP < 1) mCombatStats.mHP = 1;
        if(mCombatStats.mWillPower < 1) mCombatStats.mWillPower = 1;
    }
}
