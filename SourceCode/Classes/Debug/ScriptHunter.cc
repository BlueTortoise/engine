//--Base
#include "ScriptHunter.h"

//--Classes
//--CoreClasses
#include "SugarFileSystem.h"

//--Definitions
//--Libraries
//--Managers

//=========================================== System ==============================================
ScriptHunter::ScriptHunter()
{
    //--[ScriptHunter]
    //--System
    //--Target Info
    mTargetDirectory = NULL;
}
ScriptHunter::~ScriptHunter()
{
    free(mTargetDirectory);
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void ScriptHunter::SetTargetDirectory(const char *pPath)
{
    ResetString(mTargetDirectory, pPath);
}

//========================================= Core Methods ==========================================
void ScriptHunter::Execute()
{
    //--[Documentation and Setup]
    //--The meat of the class. Starting at the target directory, runs through all .lua files and
    //  all sub directories. Every lua file is checked in binary for possible errors. The lua files
    //  are not executed as lua files, so syntax errors or missing variables may be missed.
    if(!mTargetDirectory) return;

    //--[Path Scan]
    //--File System, does the iterating work.
    SugarFileSystem *tFileSystem = new SugarFileSystem();

    //--Scan.
    tFileSystem->ScanDirectory(mTargetDirectory);
    fprintf(stderr, "Script Hunter runs on %s. %i files found.\n", mTargetDirectory, tFileSystem->GetTotalEntries());

    //--[Iteration]
    //--Iterate across the files and call the subhandlers.
    int tTotalLuaFiles = 0;
    int tFilesTotal = tFileSystem->GetTotalEntries();
    for(int i = 0; i < tFilesTotal; i ++)
    {
        //--Get the info.
        FileInfo *rFileInfo = tFileSystem->GetEntry(i);
        if(!rFileInfo)
        {
            continue;
        }

        //--We only care about .lua files, ignore .slf files in all cases.
        bool tIsLuaFile = false;
        int tLen = (int)strlen(rFileInfo->mPath);
        for(int p = tLen-1; p >= 0; p --)
        {
            //--When we reach a period, check the file type. We only care about .lua files.
            if(rFileInfo->mPath[p] == '.')
            {
                if(!strncasecmp(&rFileInfo->mPath[p+1], "lua", 3))
                {
                    tTotalLuaFiles ++;
                    tIsLuaFile = true;
                    break;
                }
            }
        }

        //--Execute.
        if(tIsLuaFile) CheckBinaryForBugs(rFileInfo->mPath);
    }

    //--[Clean Up]
    fprintf(stderr, "Script hunter completes. %i lua files scanned.\n", tTotalLuaFiles);
    delete tFileSystem;
}

//===================================== Private Core Methods ======================================
bool ScriptHunter::CheckBinaryForBugs(const char *pPath)
{
    //--Given a path to a file, checks that file for bugs and spits the results both to the console
    //  and to the error log. Returns true if at least one bug was found, false if not.
    if(!pPath) return false;

    //--Variable used to track bugs.
    bool tAnyBugsFound = false;

    //--Seek backwards to the last slash.
    char *tFileName = SugarFileSystem::PareFileName(pPath, true);
    if(!strcasecmp(tFileName, "TestFile.lua") && false)
    {
        free(tFileName);
        return false;
    }

    //--Open the file as binary.
    FILE *fInfile = fopen(pPath, "rb");

    //--Determine the file length.
    fseek(fInfile, 0, SEEK_END);
    int tFileSize = ftell(fInfile);
    fseek(fInfile, 0, SEEK_SET);

    //--Diagnostics variables.
    bool tQuotesMustBeEscaped = false;
    int tFileCursor = 0;
    int tLineCursor = 0;
    int tCharCursor = 0;
    int tLBracketTracker = 0;
    int tRBracketTracker = 0;

    //--Dialogue diagnostics.
    bool tInDialogueSet = false;
    int tDialogueProgress = 0;
    char tBuffer[11] = "fnDialogue";

    //--Start reading.
    uint8_t tPreviousByte = 0;
    while(tFileCursor < tFileSize)
    {
        //--Read the byte.
        uint8_t tByte = 0;
        fread(&tByte, sizeof(uint8_t), 1, fInfile);
        tFileCursor ++;
        tCharCursor ++;

        //--If the byte is standard ASCII, all is well.
        if(tByte < 128)
        {
            //--If the letter matches the dialogue case and not in the dialogue yet:
            if(!tInDialogueSet && tBuffer[tDialogueProgress] == tByte)
            {
                tDialogueProgress ++;
                if(tDialogueProgress >= 10) tInDialogueSet = true;
            }
            //--No match, fail.
            else
            {
                tDialogueProgress = 0;
            }

            //--[R-Bracket Case]
            //--If the byte is an RBracket ']', track that.
            if(tByte == ']')
            {
                //--Count.
                tRBracketTracker ++;

                //--Hitting two R-brackets disables quote checking.
                if(tRBracketTracker == 2) tQuotesMustBeEscaped = false;

                //--Can't have 3 at once!
                if(tRBracketTracker >= 3)
                {
                    if(!tAnyBugsFound)
                    {
                        tAnyBugsFound = true;
                        fprintf(stderr, "%s\n", pPath);
                    }
                    fprintf(stderr, " Warning: Three R-brackets found on line %i.\n", tLineCursor+1);
                }
            }
            //--If this isn't an R bracket, reset those trackers.
            else
            {
                tRBracketTracker = 0;
            }

            //--[L-Bracker Case]
            //--Used to track dialogue. When inside two L-brackets, quotes must be escaped.
            if(tByte == '[')
            {
                //--Count.
                tLBracketTracker ++;

                //--If we hit two of these, quotes start checking for escape sequences.
                if(tLBracketTracker == 2) tQuotesMustBeEscaped = true;
            }
            //--Reset.
            else
            {
                tLBracketTracker = 0;
            }

            //--[Line-Ending Cases]
            //--Windows-style line-ending case.
            if(tByte == 13)
            {
                //--Reset bracket trackers.
                tRBracketTracker = 0;
                tInDialogueSet = false;
                tDialogueProgress = 0;

                //--Increment the line counter, reset the char counter.
                tLineCursor ++;
                tCharCursor = 0;

                //--Skip another byte.
                fread(&tByte, sizeof(uint8_t), 1, fInfile);
                tFileCursor ++;
            }
            //--Unix-style line-ending case.
            else if(tByte == 10)
            {
                //--Reset bracket trackers.
                tRBracketTracker = 0;
                tInDialogueSet = false;
                tDialogueProgress = 0;

                //--Increment the line counter, reset the char counter.
                tLineCursor ++;
                tCharCursor = 0;
            }

            //--[Quote Case]
            //--Quotes must have a backslash on the previous byte.
            if(tByte == '"' && tPreviousByte != '\\' && tQuotesMustBeEscaped && tInDialogueSet)
            {
                if(!tAnyBugsFound)
                {
                    tAnyBugsFound = true;
                    fprintf(stderr, "%s\n", pPath);
                }
                fprintf(stderr, " Warning: Non-escaped quote found on line %i.\n", tLineCursor+1);
            }

            //--Store this as the previous byte.
            tPreviousByte = tByte;
            continue;
        }

        //--If no bugs were found so far, print the header.
        if(!tAnyBugsFound)
        {
            tAnyBugsFound = true;
            fprintf(stderr, "%s\n", pPath);
        }

        //--Warning line.
        fprintf(stderr, " Warning: Non-ASCII character at line %i column %i.\n", tLineCursor+1, tCharCursor+0);
        break;

        //--Uh-oh, the byte wasn't standard ASCII. Figure out how many bytes we need to skip for
        //  the same character, because UTF is multiple characters long.
        //--First is the four-byte case.
        if(tByte & 0xF0)
        {
            fseek(fInfile, 2, SEEK_CUR);
            tFileCursor += 3;
        }
        //--Three-byte.
        else if(tByte & 0xE0)
        {
            fseek(fInfile, 1, SEEK_CUR);
            tFileCursor += 2;
        }
        //--Two-byte.
        else
        {
            fseek(fInfile, 0, SEEK_CUR);
            tFileCursor ++;
        }

        //--Reset the previous byte to 0.
        tPreviousByte = 0;
    }


    //--Finish up.
    fclose(fInfile);
    free(tFileName);
    return tAnyBugsFound;
}

//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
