//--[ModParser]
//--Used to sort through a list of 3D Custom Girl mods, by people's request. Bunch of weirdos.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--Contains a set of numbers indicating which entries are found and which are not.
#define MP_MAX 1000000
typedef struct ModParserPack
{
    //--Members
    int mHighest;
    bool mTaken[MP_MAX];

    //--Functions
    void Initialize()
    {
        mHighest = 0;
        memset(mTaken, 0, sizeof(mTaken));
    }
}ModParserPack;

//--[Local Definitions]
//--[Classes]
class ModParser
{
    private:
    //--System
    //--Name Categories
    SugarLinkedList *mNameList;

    protected:

    public:
    //--System
    ModParser();
    ~ModParser();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    void ParseFile(const char *pPath);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

