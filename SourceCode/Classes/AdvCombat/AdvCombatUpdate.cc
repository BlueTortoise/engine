//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatAnimation.h"
#include "AdvCombatEntity.h"
#include "AdventureLevel.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "LuaManager.h"

//#define COMBAT_UPDATE__DEBUG
#ifdef COMBAT_UPDATE__DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

void AdvCombat::Update()
{
    ///--[Documentation and Setup]
    //--Handles the combat update. Combat runs a set of timers then handles controls or logic updates, depending
    //  on its internal state.
    DebugPush(mIsActive, "Combat Update: Begin.\n");

    ///--[Pulsing]
    //--If the AdventureLevel is still pulsing, this object will be active but should not do anything.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel && rActiveLevel->IsPulsing()) { DebugPop("Level pulsing, exited normally.\n"); return; }

    ///--[Stop Non-Active Update]
    //--If the object is not active, update stops at this point.
    if(!mIsActive) { DebugPop("Object not active, exited normally.\n"); return; }

    ///--[Post-Reinitialize]
    //--After combat called Reinitialize(), this flag will be true. Once all enemies have registered themselves
    //  and all setup is done, this will be called.
    if(mCombatReinitialized)
    {
        //--Unset flag.
        DebugPrint("Running post-reinitialize.\n");
        mCombatReinitialized = false;

        //--Player's party resets their jobs. This is also where Script Response and Ability Script codes are
        //  called, and any other setup takes place.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
        while(rEntity)
        {
            //--Call subroutine.
            rEntity->HandleCombatStart();

            //--Next.
            rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
        }

        //--Debug.
        DebugPrint("Finished post-reinitialize.\n");
    }

    ///--[Timers]
    //--Debug.
    DebugPrint("Running timers.\n");

    //--Reset the application scatter counter.
    mTextScatterCounter = 0;

    //--Turn display timer. Indicates what turn it is, auto-caps out and stops rendering.
    if(mTurnDisplayTimer < ACE_TURN_DISPLAY_TICKS) mTurnDisplayTimer ++;

    //--Title timer.
    if(mShowTitle)
    {
        mTitleTicks ++;
        if(mTitleTicks >= mTitleTicksMax) mShowTitle = false;
    }

    //--Run the turn-order length handler.
    if(mTurnBarMoveTimer < mTurnBarMoveTimerMax)
    {
        //--Increment.
        mTurnBarMoveTimer ++;

        //--Recompute size.
        float cPercent = EasingFunction::QuadraticInOut(mTurnBarMoveTimer, mTurnBarMoveTimerMax);
        mTurnWidCurrent = mTurnWidStart + ((mTurnWidTarget - mTurnWidStart) * cPercent);
    }

    //--Prediction box timers.
    UpdatePredictionBoxes();

    //--Confirmation window timer.
    if(mIsShowingConfirmationWindow)
    {
        if(mConfirmationTimer < ADVCOMBAT_CONFIRMATION_TICKS) mConfirmationTimer ++;
    }
    //--Timer decrement.
    else
    {
        if(mConfirmationTimer > 0) mConfirmationTimer --;
    }

    //--Player's highlight cursor. Auto-clamps.
    mAbilityHighlightPack.Increment(EASING_CODE_QUADOUT);

    //--All entities in both rosters update their movement coordinates as needed. This is also where they update timers.
    bool tAnyEntitiesMoving = false;
    bool tAnyEntitiesFinishedKnockout = false;
    AdvCombatEntity *rUpdateEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rUpdateEntity)
    {
        //--Timers.
        rUpdateEntity->UpdatePosition();
        rUpdateEntity->UpdateTimers();

        //--Knockout handler.
        if(rUpdateEntity->IsKnockingOut() && rUpdateEntity->IsKnockoutFinished())
        {
            rUpdateEntity->FinishKnockout();
        }

        //--Flag for movement.
        tAnyEntitiesMoving = (tAnyEntitiesMoving || rUpdateEntity->IsMoving() || rUpdateEntity->IsKnockingOut());

        //--Next.
        rUpdateEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }
    rUpdateEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rUpdateEntity)
    {
        //--Timers.
        rUpdateEntity->UpdatePosition();
        rUpdateEntity->UpdateTimers();

        //--Knockout handler. Enemies get moved to the graveyard list.
        if(rUpdateEntity->IsKnockingOut() && rUpdateEntity->IsKnockoutFinished())
        {
            //--Finish the internal counter.
            rUpdateEntity->FinishKnockout();
            tAnyEntitiesFinishedKnockout = true;

            //--Run response scripts. Typically only AIs reply to this. The entity in question
            //  is placed for query.
            rKnockoutEntity = rUpdateEntity;
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_AIKNOCKOUT);
            rKnockoutEntity = NULL;

            //--Remove from active party.
            mrEnemyCombatParty->RemoveElementP(rUpdateEntity);

            //--Add to graveyard.
            mrEnemyGraveyard->AddElement(mEnemyRoster->GetIteratorName(), rUpdateEntity);
        }

        //--Flag for movement.
        tAnyEntitiesMoving = (tAnyEntitiesMoving || rUpdateEntity->IsMoving() || rUpdateEntity->IsKnockingOut());

        //--Next.
        rUpdateEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    //--Target timer.
    if(mIsSelectingTargets)
    {
        mTargetFlashTimer ++;
        if(mTargetFlashTimer >= ADVCOMBAT_TARGET_FLASH_PERIOD) mTargetFlashTimer = 0;
    }
    //--Clear.
    else
    {
        mTargetFlashTimer = 0;
    }

    //--Update all animations.
    AdvCombatAnimation *rAnimation = (AdvCombatAnimation *)mCombatAnimations->PushIterator();
    while(rAnimation)
    {
        rAnimation->Update();
        rAnimation = (AdvCombatAnimation *)mCombatAnimations->AutoIterate();
    }

    //--Update all combat text.
    CombatTextPack *rTextPack = (CombatTextPack *)mTextPackages->SetToHeadAndReturn();
    while(rTextPack)
    {
        rTextPack->mTicks ++;
        if(rTextPack->mTicks >= rTextPack->mTicksMax) mTextPackages->RemoveRandomPointerEntry();
        rTextPack = (CombatTextPack *)mTextPackages->IncrementAndGetRandomPointerEntry();
    }

    //--Page timer.
    if(mShowingSecondPage)
    {
        if(mSecondPageTimer < ADVCOMBAT_PAGE_CHANGE_TICKS) mSecondPageTimer ++;
    }
    else
    {
        if(mSecondPageTimer > 0) mSecondPageTimer --;
    }

    //--Debug.
    DebugPrint("Finished timers.\n");

    //--If any entities finished their knockout animation, move other enemies to compensate.
    if(tAnyEntitiesFinishedKnockout)
    {
        tAnyEntitiesMoving = true;
        PositionEnemies();
    }

    //--If the cutscene manager has any events, or the world dialogue is visible:
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue->IsVisible() || rCutsceneManager->HasAnyEvents())
    {
        //--Debug.
        DebugPrint("Updating world dialogue.\n");

        //--Run the dialogue.
        rWorldDialogue->Update();

        //--Cutscenes can also update here.
        if(rCutsceneManager->HasAnyEvents())
        {
            //--Run the update.
            rCutsceneManager->Update();
        }

        //--If stopping events, stop here.
        if(rWorldDialogue->IsStoppingEvents())
        {
            if(mIsShowingCombatInspector) DeactivateInspector();
            DebugPop("Updated normally, exited from dialogue.\n");
            return;
        }
    }

    //--Combat inspector.
    UpdateInspector();

    ///--[Subhandlers]
    //--Subhandlers take control away from the usual combat update.
    DebugPrint("Running subhandlers.\n");
    if(mCombatResolution != ADVCOMBAT_END_NONE) { UpdateResolution();   DebugPop("Updated resolution, exited normally.\n"); return; }
    if(mIsIntroduction)                         { UpdateIntroduction(); DebugPop("Updated introduction, exited normally.\n"); return; }
    if(mIsShowingConfirmationWindow)            { UpdateConfirmation(); DebugPop("Updated confirmation exited normally.\n"); return; }
    if(mIsShowingCombatInspector)               { DebugPop("Updated normally, exited from combat inspector.\n"); return; }
    DebugPrint("Finished subhandlers.\n");

    //--Application packs don't run if entities are moving!
    if(tAnyEntitiesMoving)
    {
        //--If the player was acting, then we can allow the player's interface to keep operating even if entities are moving.
        if(mIsPerformingPlayerUpdate)
        {
            DebugPrint("Handling player controls.\n");
            HandlePlayerControls();
            DebugPrint("Finished player controls.\n");
        }

        DebugPop("Updated. Waiting for entities to finish moving.\n");
        return;
    }

    ///--[Application]
    //--Application packs are what changes HP/MP/Effects in an organized fashion. The effect (whatever it is)
    //  applies when the timer hits zero.
    bool tAnyOneApplicationRunning = false;
    DebugPrint("Running application packages.\n");

    //--If any applications are in the queue, hide the player's interface.
    if(mApplicationQueue->GetListSize() > 0 || mEventQueue->GetListSize() > 0)
    {
        if(mPlayerInterfaceTimer > 0) mPlayerInterfaceTimer --;
    }

    //--Run applications.
    ApplicationPack *rPackage = (ApplicationPack *)mApplicationQueue->SetToHeadAndReturn();
    while(rPackage)
    {
        //--Decrement.
        rPackage->mTicks --;

        //--Ending case.
        if(rPackage->mTicks < 0)
        {
            //--Run responses to the application. They can query the application both before and after.
            rCurrentApplicationPack = rPackage;
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY);
            HandleApplication(rPackage->mOriginatorID, rPackage->mTargetID, rPackage->mEffectString);
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY);

            //--Clean.
            rCurrentApplicationPack = NULL;
            mApplicationQueue->RemoveRandomPointerEntry();
        }
        else
        {
            tAnyOneApplicationRunning = true;
        }

        //--Next.
        rPackage = (ApplicationPack *)mApplicationQueue->IncrementAndGetRandomPointerEntry();
    }
    DebugPrint("Finished application packages.\n");

    ///--[Events]
    //--Debug.
    DebugPrint("Running event handlers.\n");
    if(tAnyOneApplicationRunning)
    {
        DebugPop("Updated. Waiting for applications to finish.\n");
        return;
    }

    //--If any events are in the queue, handle those.
    CombatEventPack *rEventPackage = (CombatEventPack *)mEventQueue->GetElementBySlot(0);
    while(rEventPackage)
    {
        //--On the 0th tick, execute the event's ability script.
        if(mEventFirstTick)
        {
            //--Ask the event if it can still run using code ACA_SCRIPT_CODE_QUERY_CAN_RUN. Only the
            //  ability itself runs this, no other scripts get to respond.
            mEventCanRun = false;
            if(rEventPackage->rAbility)
            {
                //--Set flags. By default, the event fails and the script must mark it as functional.
                rFiringEventPackage = rEventPackage;
                rEventPackage->rAbility->CallCode(ACA_SCRIPT_CODE_QUERY_CAN_RUN);
                rFiringEventPackage = NULL;
            }

            //--If we got this far, the event can run.
            mEventFirstTick = false;
            mEventTimer = 0;
            mEntitiesHaveRepositioned = false;

            //--Order all entities involved to get onscreen.
            if(mEventCanRun)
            {
                PositionCharactersOnScreenForEvent(rEventPackage);
            }
            //--Event cannot run, so all party members need to go offscreen.
            else
            {
                PositionCharactersOffScreen();
            }
        }

        //--If any entities are repositioning, stop the update.
        if(IsAnyoneMoving())
        {
            DebugPop("Event cycle stopped due to entities move.\n");
            return;
        }

        //--If this flag is still false, but nobody is moving, the entities have repositioned. We can call the event now.
        if(!mEntitiesHaveRepositioned)
        {
            //--Flag.
            mEntitiesHaveRepositioned = true;

            //--Run the event, if it can run.
            if(rEventPackage->rAbility && mEventCanRun)
            {
                rFiringEventPackage = rEventPackage;
                rEventPackage->rAbility->CallCode(ACA_SCRIPT_CODE_EXECUTE);
                rFiringEventPackage = NULL;
            }

            //--If the event timer does not reach the minimum ticks, set it to that. This makes sure the UI has time to scroll
            //  offscreen.
            if(mEventTimer < ADVCOMBAT_EVENT_TICKS_MINIMUM)
            {
                mEventTimer = ADVCOMBAT_EVENT_TICKS_MINIMUM;
            }
        }

        //--Decrement this timer.
        mEventTimer --;

        //--End. Start the next event.
        if(mEventTimer < 1)
        {
            //--Pop off the event.
            mEventFirstTick = true;
            mEventQueue->RemoveElementI(0);

            //--Set the next event.
            rEventPackage = (CombatEventPack *)mEventQueue->GetElementBySlot(0);

            //--If there's no next event, mark the action as completed. This only occurs if the acting character
            //  actually acted, toggling the mNoEventsEndsAction flag to true.
            //--If a passive effect or ability caused an event to occur, then we don't end the character's turn.
            if(!rEventPackage && mNoEventsEndsAction)
            {
                CompleteAction();
            }

            //--Check if any entities got knocked out. If they did, stop here. Give them time to animate the KO
            //  before moving to the next event.
            if(CheckKnockouts())
            {
                AudioManager::Fetch()->PlaySound("Combat|MonsterDie");
                DebugPop("Event cycle running, exited due to knockout.\n");
                return;
            }

            //--Run the next event, if it exists.
            continue;
        }
        //--Otherwise, end the update here.
        else
        {
            DebugPop("Event cycle running, exited normally.\n");
            return;
        }
    }

    //--Debug.
    DebugPrint("Finished event handlers.\n");

    ///--[Turn Resolution]
    //--This control loop will start turns and actions until someone is able to handle the action, or the battle is over.
    //  If 10 turns start with no resolution and nobody able to act, the engine assumes it jammed. Note that a stunned
    //  entity is still considered to be acting even if they will pass their turn, so this should not theoretically occur.
    DebugPrint("Checking turn resolution.\n");
    int tTurnsStarted = 0;
    while(true)
    {
        //--Application is occurring.
        if(mApplicationQueue->GetListSize() > 0)
        {
            DebugPop("Application queue gained event after other action, exited.\n");
            return;
        }

        //--If we reach this point and an event is occurring, it may have been added. Wait until it's done before resuming.
        if(mEventQueue->GetListSize() > 0)
        {
            DebugPop("Event queue gained event after other action, exited.\n");
            return;
        }

        //--Victory condition check. First, is the player defeated? This gets priority. Even if the player and enemy
        //  are defeated on the same turn, the player still loses.
        if(IsPlayerDefeated())
        {
            BeginResolutionSequence(ADVCOMBAT_END_DEFEAT);
            DebugPop("Beginning defeat sequence, exited.\n");
            return;
        }

        //--Is the enemy defeated?
        if(IsEnemyDefeated())
        {
            BeginResolutionSequence(ADVCOMBAT_END_VICTORY);
            DebugPop("Beginning victory sequence, exited.\n");
            return;
        }

        //--If a turn is active, run the end-of-turn code. If any events fire, those get handled and we return out. The next
        //  time this code will be bypassed and a new turn will begin.
        if(mIsTurnActive)
        {
            EndTurn();
            continue;
        }

        //--If the turn order list is empty, start a new turn. This will be the case immediately after the
        //  intro has concluded, as well.
        if(mrTurnOrder->GetListSize() < 1)
        {
            //--If there are entities that are unable to act during a turn for 10 turns in a row, but the battle
            //  is not concluded, break out here.
            if(tTurnsStarted >= 10)
            {
                DebugManager::ForcePrint("AdvCombat:Update - Warning, combat loop jammed for 10 turns without concluding.\n");
                BeginResolutionSequence(ADVCOMBAT_END_VICTORY);
                DebugPop("Combat loop jammed, exiting.\n");
                return;
            }

            //--Begin a new turn and flag that we did so. If the control loop starts 2 new turns but somehow does
            //  not conclude, it got jammed.
            tTurnsStarted ++;
            BeginTurn();
            continue;
        }

        //--If no action is taking place, begin an action. This breaks out of the control loop, and allows the acting
        //  entity to get on with the business of acting.
        if(!mIsActionOccurring)
        {
            BeginAction();
            break;
        }

        //--If the action is not concluding, and is occurring, begin normal action handling. This occurs on successive ticks
        //  while the player is making a decision.
        if(mIsActionOccurring) break;
    }

    //--Debug.
    DebugPrint("Finished turn resolution loop.\n");

    ///--[Acting Entity]
    //--Get the acting entity.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity) { DebugPop("No acting entity, exiting.\n"); return; }

    ///--[AI Handling]
    //--If the entity has an AI controlling it:
    if(rActingEntity->GetAIPath() != NULL)
    {
        //--Stop updating for AIs if anyone is moving or anything is animating. This causes a nice breather
        //  between actions but is not strictly necessary.
        if(mPlayerInterfaceTimer > 0) mPlayerInterfaceTimer --;
        if(IsAnyoneMoving() || IsAnythingAnimating() || mPlayerInterfaceTimer > 0)
        {
            DebugPop("Pausing AI for animations.\n");
            return;
        }

        //--Set this flag to false.
        mAIHandledAction = false;

        //--Clear any outstanding variables.
        ClearTargetClusters();

        //--Execute the AI. It should decide what action to take.
        AdvCombat::xIsAISpawningEvents = true;
        const char *rPath = rActingEntity->GetAIPath();
        LuaManager::Fetch()->PushExecPop(rActingEntity, rPath, 1, "N", (float)ACE_AI_SCRIPT_CODE_ACTIONBEGIN);
        AdvCombat::xIsAISpawningEvents = false;

        //--If the AI spawned at least one event, it handled the update. If it did not, we spawn a "Pass Turn"
        //  action for the AI. This is to prevent infinite loops.
        if(mEventQueue->GetListSize() < 1)
        {
            ExecuteAbility(mSysAbilityPassTurn, NULL);
        }
    }
    ///--[Player Handling]
    //--If there is no commanding AI, the player inputs commands here. This gets its own file.
    else
    {
        mIsPerformingPlayerUpdate = true;
        DebugPrint("Handling player controls.\n");
        HandlePlayerControls();
        DebugPrint("Finished player controls.\n");
    }

    //--Debug.
    DebugPop("Reached end of update function, exited normally.\n");
}
