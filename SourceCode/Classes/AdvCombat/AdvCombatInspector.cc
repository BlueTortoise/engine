//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void AdvCombat::ActivateInspector()
{
    mIsShowingCombatInspector = true;
    mCombatInspectorMode = ADVCOMBAT_INSPECTOR_CHARSELECT;
    mCombatInspectorCharacterCursor = 0;
    CalculateHighlightPos(mCombatInspectorMode, mCombatInspectorCharacterCursor, 0);
}
void AdvCombat::DeactivateInspector()
{
    mIsShowingCombatInspector = false;
}

//======================================== Core Methods ===========================================
void AdvCombat::CalculateHighlightPos(int pMode, int pIndex, int pTicks)
{
    //--Computes the highlight position based on the given mode and index, and orders the highlights
    //  to move there in the given number of ticks.
    if(pMode == ADVCOMBAT_INSPECTOR_CHARSELECT)
    {
        float cLft =   0.0f;
        float cTop =  61.0f;
        float cWid = 263.0f;
        float cHei =  50.0f;
        float cSpY =  55.0f;
        mInspectorHighlightPos.MoveTo(cLft, cTop + (cSpY * pIndex), pTicks);
        mInspectorHighlightWid.MoveTo(cWid, cHei, pTicks);
    }
    //--Effect queries.
    else if(pMode == ADVCOMBAT_INSPECTOR_EFFECTS)
    {
        float cEffectLft = 280.0f;
        float cEffectTop =  50.0f;
        float cEffectHei =  27.0f;
        mInspectorHighlightPos.MoveTo(cEffectLft-1.0f, cEffectTop + (cEffectHei * pIndex)-3.0f, pTicks);
        mInspectorHighlightWid.MoveTo(590.0f, cEffectHei+4.0f, pTicks);
    }
    //--Abilities query. Ignores index.
    else if(pMode == ADVCOMBAT_INSPECTOR_ABILITIES)
    {
        float cXPos = ADVCOMBAT_POSITION_ABILITY_X + (mCombatInspectorAbilityCursorX * ADVCOMBAT_POSITION_ABILITY_W);
        float cYPos = ADVCOMBAT_POSITION_ABILITY_Y + (mCombatInspectorAbilityCursorY * ADVCOMBAT_POSITION_ABILITY_H);
        mInspectorHighlightPos.MoveTo(cXPos, cYPos, pTicks);
        mInspectorHighlightWid.MoveTo(50.0f, 50.0f, pTicks);
    }
}

//============================================ Update =============================================
void AdvCombat::UpdateInspector()
{
    ///--[Documentation and Setup]
    //--Updates the combat inspector, allowing the player to inspect effects, abilities, and resistances
    //  during combat. The inspector suspends Application/Event cycles while visible.
    if(mIsShowingCombatInspector)
    {
        if(mCombatInspectorTimer < ADVCOMBAT_INSPECTOR_TICKS) mCombatInspectorTimer ++;
    }
    else
    {
        if(mCombatInspectorTimer > 0) mCombatInspectorTimer --;
    }

    //--Highlights.
    mInspectorHighlightPos.Increment(EASING_CODE_QUADINOUT);
    mInspectorHighlightWid.Increment(EASING_CODE_QUADINOUT);

    //--If the inspector is not active, stop the update here.
    if(!mIsShowingCombatInspector) return;

    ///--[Controls]
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Construct a list that represents all entities in both the player and enemy party. This simplifies
    //  some rendering since they are treated as one group for the inspector.
    SugarLinkedList *trEntityList = new SugarLinkedList(false);
    mrCombatParty->CloneToList(trEntityList);
    mrEnemyCombatParty->CloneToList(trEntityList);

    ///--[Character Selection]
    if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_CHARSELECT)
    {
        //--Up and down to change entities.
        if(rControlManager->IsFirstPress("Up"))
        {
            mCombatInspectorCharacterCursor --;
            if(mCombatInspectorCharacterCursor < 0) mCombatInspectorCharacterCursor = trEntityList->GetListSize() - 1;
            if(mCombatInspectorCharacterCursor < 0) mCombatInspectorCharacterCursor = 0;
            CalculateHighlightPos(mCombatInspectorMode, mCombatInspectorCharacterCursor, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        if(rControlManager->IsFirstPress("Down"))
        {
            mCombatInspectorCharacterCursor ++;
            if(mCombatInspectorCharacterCursor >= trEntityList->GetListSize()) mCombatInspectorCharacterCursor = 0;
            CalculateHighlightPos(mCombatInspectorMode, mCombatInspectorCharacterCursor, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--'Activate' switches to effects mode.
        if(rControlManager->IsFirstPress("Activate"))
        {
            mCombatInspectorMode = ADVCOMBAT_INSPECTOR_EFFECTS;
            mCombatInspectorEffectCursor = 0;
            CalculateHighlightPos(mCombatInspectorMode, mCombatInspectorEffectCursor, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Jump, switches to abilities mode.
        if(rControlManager->IsFirstPress("Jump"))
        {
            mCombatInspectorMode = ADVCOMBAT_INSPECTOR_ABILITIES;
            mCombatInspectorAbilityCursorX = 0;
            mCombatInspectorAbilityCursorY = 0;
            CalculateHighlightPos(mCombatInspectorMode, 0, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--[Cancel]
        //--Close the inspector.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            AudioManager::Fetch()->PlaySound("Menu|Select");
            DeactivateInspector();
            delete trEntityList;
            return;
        }
    }
    ///--[Effect Queries]
    else if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_EFFECTS)
    {
        //--Get a list of all effects that apply to the given entity.
        int tEffectListSize = 0;
        AdvCombatEntity *rSelectedEntity = (AdvCombatEntity *)trEntityList->GetElementBySlot(mCombatInspectorCharacterCursor);
        if(rSelectedEntity)
        {
            //--Get the list.
            SugarLinkedList *trEffectList = GetEffectsReferencing(rSelectedEntity);

            //--Scrub effects that are hidden.
            AdvCombatEffect *rCheckEffect = (AdvCombatEffect *)trEffectList->SetToHeadAndReturn();
            while(rCheckEffect)
            {
                if(!rCheckEffect->IsVisibleOnUI()) trEffectList->RemoveRandomPointerEntry();
                rCheckEffect = (AdvCombatEffect *)trEffectList->IncrementAndGetRandomPointerEntry();
            }

            //--Save the resulting size. We don't need the list anymore.
            tEffectListSize = trEffectList->GetListSize();
            delete trEffectList;
        }

        //--Up and down to change effects.
        if(rControlManager->IsFirstPress("Up"))
        {
            mCombatInspectorEffectCursor --;
            if(mCombatInspectorEffectCursor < 0) mCombatInspectorEffectCursor = tEffectListSize - 1;
            if(mCombatInspectorEffectCursor < 0) mCombatInspectorEffectCursor = 0;
            CalculateHighlightPos(mCombatInspectorMode, mCombatInspectorEffectCursor, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        if(rControlManager->IsFirstPress("Down"))
        {
            mCombatInspectorEffectCursor ++;
            if(mCombatInspectorEffectCursor >= tEffectListSize) mCombatInspectorEffectCursor = 0;
            CalculateHighlightPos(mCombatInspectorMode, mCombatInspectorEffectCursor, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Cancel/Activate, returns to entity select.
        if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Activate"))
        {
            mCombatInspectorMode = ADVCOMBAT_INSPECTOR_CHARSELECT;
            CalculateHighlightPos(mCombatInspectorMode, mCombatInspectorCharacterCursor, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Jump, switches to abilities mode.
        if(rControlManager->IsFirstPress("Jump"))
        {
            mCombatInspectorMode = ADVCOMBAT_INSPECTOR_ABILITIES;
            mCombatInspectorAbilityCursorX = 0;
            mCombatInspectorAbilityCursorY = 0;
            CalculateHighlightPos(mCombatInspectorMode, 0, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    ///--[Abilities Query]
    else if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_ABILITIES)
    {
        //--Up and down to change effects.
        if(rControlManager->IsFirstPress("Up"))
        {
            mCombatInspectorAbilityCursorY --;
            if(mCombatInspectorAbilityCursorY < 0) mCombatInspectorAbilityCursorY = ACE_ABILITY_GRID_SIZE_Y - 1;
            CalculateHighlightPos(mCombatInspectorMode, 0, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        if(rControlManager->IsFirstPress("Down"))
        {
            mCombatInspectorAbilityCursorY ++;
            if(mCombatInspectorAbilityCursorY >= ACE_ABILITY_GRID_SIZE_Y) mCombatInspectorAbilityCursorY = 0;
            CalculateHighlightPos(mCombatInspectorMode, 0, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        if(rControlManager->IsFirstPress("Left"))
        {
            mCombatInspectorAbilityCursorX --;
            if(mCombatInspectorAbilityCursorX < 0) mCombatInspectorAbilityCursorX = ACE_ABILITY_GRID_SIZE_X - 1;
            CalculateHighlightPos(mCombatInspectorMode, 0, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        if(rControlManager->IsFirstPress("Right"))
        {
            mCombatInspectorAbilityCursorX ++;
            if(mCombatInspectorAbilityCursorX >= ACE_ABILITY_GRID_SIZE_X) mCombatInspectorAbilityCursorX = 0;
            CalculateHighlightPos(mCombatInspectorMode, 0, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--'Activate' switches to effects mode.
        if(rControlManager->IsFirstPress("Activate"))
        {
            mCombatInspectorMode = ADVCOMBAT_INSPECTOR_EFFECTS;
            mCombatInspectorEffectCursor = 0;
            CalculateHighlightPos(mCombatInspectorMode, mCombatInspectorEffectCursor, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Cancel/Jump, returns to entity select.
        if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Jump"))
        {
            mCombatInspectorMode = ADVCOMBAT_INSPECTOR_CHARSELECT;
            CalculateHighlightPos(mCombatInspectorMode, mCombatInspectorCharacterCursor, ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Clean up.
    delete trEntityList;
}

//=========================================== Drawing =============================================
void AdvCombat::RenderInspector()
{
    ///--[Documentation and Setup]
    //--Renders the combat inspector. The inspector slides in from the top/bottom of the screen and
    //  is visible as it slides out, even when it is not commanding the update.
    float cScrollPercent = EasingFunction::QuadraticInOut(mCombatInspectorTimer, ADVCOMBAT_INSPECTOR_TICKS);
    if(cScrollPercent == 0.0f) return;

    //--Backing.
    SugarBitmap::DrawFullBlack(cScrollPercent * 0.75f);

    //--Construct a list that represents all entities in both the player and enemy party. This simplifies
    //  some rendering since they are treated as one group for the inspector.
    SugarLinkedList *trEntityList = new SugarLinkedList(false);
    mrCombatParty->CloneToList(trEntityList);
    mrEnemyCombatParty->CloneToList(trEntityList);

    //--Subroutines.
    RenderInspectorLower(trEntityList);
    RenderInspectorUpper(trEntityList);
    delete trEntityList;
}
void AdvCombat::RenderInspectorUpper(SugarLinkedList *pList)
{
    ///--[Documentation and Setup]
    //--Renders the upper half of the combat inspector. The list provided is the list of all entities in combat.
    if(!Images.mIsReady || !pList) return;

    //--These slide in from the top.
    float cScrollPercent = EasingFunction::QuadraticInOut(mCombatInspectorTimer, ADVCOMBAT_INSPECTOR_TICKS);
    float cSlideOffsetY = VIRTUAL_CANVAS_Y * (1.0f-cScrollPercent) * -1.0f;
    glTranslatef(0.0f, cSlideOffsetY, 0.0f);

    //--Static frames.
    Images.Data.rInspectorFrames->Draw();

    //--Static text
    Images.Data.rInspectorHeading->DrawText( 573.0f, 9.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Effects");
    Images.Data.rInspectorHeading->DrawText( 980.0f, 9.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Statistics");
    Images.Data.rInspectorHeading->DrawText(1218.0f, 9.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Resistances");

    ///--[Instructions]
    mCombatInspectorSwitchToAbilitiesString->DrawText(5.0f,  9.0f, 0, 1.0f, Images.Data.rInspectorEffectFont);
    mCombatInspectorSwitchToEffects->DrawText        (5.0f, 30.0f, 0, 1.0f, Images.Data.rInspectorEffectFont);

    ///--[Entity Names]
    //--Setup.
    float cIconLft = -17.0f;
    float cIconTop = 23.0f;
    float cNameLft = 45.0f;
    float cIconHei = Images.Data.rInspectorNameBar->GetHeight() + 5.0f;
    float cYOffset = 30.0f;

    //--For each entity in the player's and enemy's party, render an entry.
    int i = 0;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)pList->PushIterator();
    while(rEntity)
    {
        //--Backing.
        Images.Data.rInspectorNameBar->Draw(0.0f, (i * cIconHei) + cYOffset);

        //--Get the turn-order icon.
        SugarBitmap *rTurnIcon = rEntity->GetTurnIcon();
        if(rTurnIcon) rTurnIcon->Draw(cIconLft, cIconTop + (i * cIconHei) + cYOffset);

        //--Render the entity's name.
        const char *rDisplayName = rEntity->GetDisplayName();
        if(rDisplayName) Images.Data.rInspectorNameFont->DrawText(cNameLft, cIconTop + (i * cIconHei) + 21.0f + cYOffset, 0, 1.0f, rDisplayName);

        //--Next.
        i ++;
        rEntity = (AdvCombatEntity *)pList->AutoIterate();
    }

    ///--[Effect Listing]
    //--Setup.
    float cEffectLft = 280.0f;
    float cEffectRgt = 865.0f;
    float cEffectTop =  50.0f;
    float cEffectHei =  27.0f;
    float tEffectY = cEffectTop;

    //--Get a list of all effects that apply to the given entity.
    AdvCombatEntity *rSelectedEntity = (AdvCombatEntity *)pList->GetElementBySlot(mCombatInspectorCharacterCursor);
    if(rSelectedEntity)
    {
        //--Get list.
        uint32_t tID = rSelectedEntity->GetID();
        SugarLinkedList *trEffectList = GetEffectsReferencing(rSelectedEntity);

        //--Scrub effects that are hidden.
        AdvCombatEffect *rCheckEffect = (AdvCombatEffect *)trEffectList->SetToHeadAndReturn();
        while(rCheckEffect)
        {
            if(!rCheckEffect->IsVisibleOnUI()) trEffectList->RemoveRandomPointerEntry();
            rCheckEffect = (AdvCombatEffect *)trEffectList->IncrementAndGetRandomPointerEntry();
        }

        //--Iterate.
        AdvCombatEffect *rEffect = (AdvCombatEffect *)trEffectList->PushIterator();
        while(rEffect)
        {
            //--Render the effect origin icon.
            SugarBitmap *rEffectBack  = rEffect->GetBackImage();
            SugarBitmap *rEffectFrame = rEffect->GetFrameImage();
            SugarBitmap *rEffectFront = rEffect->GetFrontImage();
            glTranslatef(cEffectLft, tEffectY, 0.0f);
            glScalef(0.5f, 0.5f, 1.0f);
            if(rEffectBack)  rEffectBack->Draw();
            if(rEffectFrame) rEffectFrame->Draw();
            if(rEffectFront) rEffectFront->Draw();
            glScalef(2.0f, 2.0f, 1.0f);
            glTranslatef(-cEffectLft, -tEffectY, 0.0f);

            //--The 0th/1st description line is the title line of the effect.
            AdvCombatEffectTargetPack *rPackage = rEffect->GetTargetPackByID(tID);
            if(rPackage)
            {
                //--Zero line is the left-aligned title.
                if(rPackage->mDescriptionLinesTotal > 0)
                {
                    StarlightString *rZeroLine = rPackage->mDescriptionLines[0];
                    if(rZeroLine) rZeroLine->DrawText(cEffectLft + 27.0f, tEffectY, 0, 1.0f, Images.Data.rAbilityDescriptionFont);
                }
                //--One line is the right-aligned title.
                if(rPackage->mDescriptionLinesTotal > 1)
                {
                    StarlightString *rOneLine = rPackage->mDescriptionLines[1];
                    if(rOneLine) rOneLine->DrawText(cEffectRgt, tEffectY, SUGARFONT_RIGHTALIGN_X, 1.0f, Images.Data.rAbilityDescriptionFont);
                }
            }

            //--Next.
            tEffectY = tEffectY + cEffectHei;
            rEffect = (AdvCombatEffect *)trEffectList->AutoIterate();
        }

        //--Clean.
        delete trEffectList;
    }

    ///--[Statistics Listing]
    //--Renders current statistics values. If the temp-effect column is nonzero, changes color.
    float cStatisticsLft =  893.0f;
    float cStatisticsRgt = 1065.0f;
    float cStatisticsTop =   50.0f;
    float cStatisticsHei = Images.Data.rInspectorEffectFont->GetTextHeight();
    float tStatisticsY = cStatisticsTop;

    //--Lookup table.
    for(int i = 0; i < CI_STATS_SLOT_TOTAL; i ++)
    {
        //--Image.
        Images.Data.rInspectorStatusIcons[i]->Draw(cStatisticsLft, tStatisticsY + 3);

        //--Statistic.
        int tValue = rSelectedEntity->GetStatistic(mCombatInspectorPopulateSlots[i]);

        //--Normal:
        if(i != CI_STATS_SLOT_MPREGEN)
            Images.Data.rInspectorEffectFont->DrawTextArgs(cStatisticsRgt, tStatisticsY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tValue);
        else
        {
            Images.Data.rInspectorEffectFont->DrawTextArgs(cStatisticsRgt-18.0f, tStatisticsY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i/", tValue);
            Images.Data.rInspectorClock->Draw(cStatisticsRgt-18.0f, tStatisticsY+4.0f);
        }

        //--Next.
        tStatisticsY = tStatisticsY + cStatisticsHei;
    }
    tStatisticsY = tStatisticsY + (cStatisticsHei * 1.0f);

    //--Render Health, Adrenaline, and Shields. These do not fir with the other stats.
    Images.Data.rInspectorHeading->DrawText( 980.0f, tStatisticsY, SUGARFONT_AUTOCENTER_X, 1.0f, "Health");
    tStatisticsY = tStatisticsY + (cStatisticsHei * 2.0f);

    Images.Data.rInspectorHealth->Draw(cStatisticsLft, tStatisticsY + 3);
    Images.Data.rInspectorEffectFont->DrawTextArgs(cStatisticsRgt, tStatisticsY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i/%i", rSelectedEntity->GetHealth(), rSelectedEntity->GetStatistic(STATS_HPMAX));
    tStatisticsY = tStatisticsY + cStatisticsHei;

    Images.Data.rInspectorAdrenaline->Draw(cStatisticsLft, tStatisticsY + 3);
    Images.Data.rInspectorEffectFont->DrawTextArgs(cStatisticsRgt, tStatisticsY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rSelectedEntity->GetAdrenaline());
    tStatisticsY = tStatisticsY + cStatisticsHei;

    Images.Data.rInspectorShields->Draw(cStatisticsLft, tStatisticsY + 3);
    Images.Data.rInspectorEffectFont->DrawTextArgs(cStatisticsRgt, tStatisticsY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rSelectedEntity->GetShields());

    ///--[Resistances Listing]
    //--Renders resistance values, including reduction.
    float cResistanceLft = 1091.0f;
    float cResistanceRgt = 1344.0f;
    float cResistanceTop =   50.0f;
    float cResistanceHei = Images.Data.rInspectorEffectFont->GetTextHeight();
    float tResistanceY = cResistanceTop;

    //--Lookup table.
    for(int i = 0; i < CI_RESIST_SLOT_TOTAL; i ++)
    {
        //--Image.
        Images.Data.rInspectorResistanceIcons[i]->Draw(cResistanceLft, tResistanceY+4.0f);

        //--If the resistance value is 1000, the value is "Immune".
        int tResistValue = rSelectedEntity->GetStatistic(STATS_RESIST_START + i);
        if(tResistValue >= 1000)
        {
            Images.Data.rInspectorEffectFont->DrawText(cResistanceRgt, tResistanceY, SUGARFONT_RIGHTALIGN_X, 1.0f, "Immune");
        }
        //--Otherwise, render computed percentage.
        else
        {
            //--Resistance amount.
            Images.Data.rInspectorEffectFont->DrawTextArgs(cResistanceLft+44.0f, tResistanceY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tResistValue);

            //--Computed reduction.
            float cReduction = 1.0f - AdvCombatEntity::ComputeResistance(tResistValue);

            //--If the value is positive, render it with a minus sign.
            if(cReduction >= 0.0f)
            {
                Images.Data.rInspectorEffectFont->DrawTextArgs(cResistanceRgt, tResistanceY, SUGARFONT_RIGHTALIGN_X, 1.0f, "-%0.0f%% Dmg", cReduction * 100.0f);
            }
            //--Negative, give it a plus sign.
            else
            {
                Images.Data.rInspectorEffectFont->DrawTextArgs(cResistanceRgt, tResistanceY, SUGARFONT_RIGHTALIGN_X, 1.0f, "+%0.0f%% Dmg", cReduction * -100.0f);
            }
        }

        //--Next.
        tResistanceY = tResistanceY + cResistanceHei;
    }

    ///--[Highlight]
    //--Rendered as part of the top window for consistency.
    RenderExpandableHighlight(mInspectorHighlightPos.mXCur, mInspectorHighlightPos.mYCur, mInspectorHighlightPos.mXCur + mInspectorHighlightWid.mXCur, mInspectorHighlightPos.mYCur + mInspectorHighlightWid.mYCur, 4.0f, Images.Data.rAbilityHighlight);

    //--Clean.
    glTranslatef(0.0f, -cSlideOffsetY, 0.0f);
}
void AdvCombat::RenderInspectorLower(SugarLinkedList *pList)
{
    ///--[Documentation and Setup]
    //--Renders the bottom half of the combat inspector. This includes the entity portrait, ability bar, and description window.
    //  The list provided is the list of all entities in combat.
    if(!pList) return;

    //--Compute scroll percent.
    float cScrollPercent = EasingFunction::QuadraticInOut(mCombatInspectorTimer, ADVCOMBAT_INSPECTOR_TICKS);
    float cSlideOffsetY = VIRTUAL_CANVAS_Y * (1.0f-cScrollPercent);
    glTranslatef(0.0f, cSlideOffsetY, 0.0f);

    ///--[Character Portrait]
    //--Determine the highlighted entity. If no entity is present, quit out early.
    AdvCombatEntity *rActiveEntity = (AdvCombatEntity *)pList->GetElementBySlot(mCombatInspectorCharacterCursor);
    if(!rActiveEntity)
    {
        glTranslatef(0.0f, -cSlideOffsetY, 0.0f);
        return;
    }

    //--Setup.
    SugarBitmap *rCharacterPortrait = rActiveEntity->GetCombatPortrait();
    SugarBitmap *rCountermask = rActiveEntity->GetCombatCounterMask();
    TwoDimensionRealPoint cRenderPos = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_MAIN);

    //--Render the inset.
    Images.Data.rMainPortraitRingBack->Draw();

    //--Switch to character masking.
    DisplayManager::ActivateMaskRender(ACE_STENCIL_ACTIVE_CHARACTER_PORTRAIT);
    Images.Data.rMainPortraitMask->Draw();

    //--If a countermask is available, render that.
    if(rCountermask)
    {
        DisplayManager::ActivateMaskRender(0);
        rCountermask->Draw(cRenderPos.mXCenter, cRenderPos.mYCenter);
    }

    //--Render the portrait.
    DisplayManager::ActivateStencilRender(ACE_STENCIL_ACTIVE_CHARACTER_PORTRAIT);
    if(rCharacterPortrait) rCharacterPortrait->Draw(cRenderPos.mXCenter, cRenderPos.mYCenter);
    DisplayManager::DeactivateStencilling();

    //--Render the frame and name banner.
    Images.Data.rMainPortraitRing->Draw();
    Images.Data.rMainNameBanner->Draw();

    //--Render the character's display name.
    const char *rDisplayName = rActiveEntity->GetDisplayName();
    if(rDisplayName)
    {
        float cCenterX = 129.0f;
        float cCenterY = 665.0f;
        Images.Data.rActiveCharacterNameFont->DrawText(cCenterX, cCenterY, SUGARFONT_AUTOCENTER_XY, 1.0f, rDisplayName);
    }

    ///--[Health Bar]
    //--Static backing.
    Images.Data.rHealthBarUnderlay->Draw();

    //--Render constants.
    float cDescriptionLft = 52.0f;
    float cValueRgt = 79.0f;
    float cShieldLft = 232.0f;
    float cHPTop = 690.0f;
    float cMPTop = 711.0f;
    float cCPTop = 732.0f;
    float cCPPipLft =  76.0f;
    float cCPPipTop = 735.0f;
    float cCPPipWid =  10.0f;

    //--Shield and Adrenaline values. If zero, don't render the overlay.
    int cShieldValue = rActiveEntity->GetDisplayShield();
    int cAdrenalineValue = rActiveEntity->GetDisplayAdrenaline();

    //--HP Bar.
    float cHPPercent = rActiveEntity->GetHealthPercent();
    RenderPercent(cHPPercent, 1.0f, Images.Data.rHealthBarHPFill);

    //--MP Bar.
    float cMPPercent = rActiveEntity->GetMagicPercent();
    RenderPercent(cMPPercent, 1.0f, Images.Data.rHealthBarMPFill);

    //--Static frame.
    Images.Data.rMainHealthBarFrame->Draw();

    //--Shield overlay.
    if(cShieldValue > 0 && cAdrenalineValue > 0)
        Images.Data.rHealthBarMix->Draw();
    else if(cShieldValue > 0)
        Images.Data.rHealthBarShield->Draw();
    else if(cAdrenalineValue > 0)
        Images.Data.rHealthBarAdrenaline->Draw();

    //--Render HP.
    Images.Data.rAllyHPFont->DrawText(cDescriptionLft, cHPTop, 0, 1.0f, "HP");
    Images.Data.rAllyHPFont->DrawTextArgs(cValueRgt,   cHPTop, 0, 1.0f, "%i/%i", rActiveEntity->GetHealth(), rActiveEntity->GetStatistic(STATS_HPMAX));

    //--Render MP.
    Images.Data.rAllyHPFont->DrawText(cDescriptionLft, cMPTop, 0, 1.0f, "MP");
    Images.Data.rAllyHPFont->DrawTextArgs(cValueRgt,   cMPTop, 0, 1.0f, "%i/%i", rActiveEntity->GetMagic(), rActiveEntity->GetStatistic(STATS_MPMAX));

    //--Render CP. Each one is a "pip".
    Images.Data.rAllyHPFont->DrawText(cDescriptionLft, cCPTop, 0, 1.0f, "CP");
    int cCPTotal = rActiveEntity->GetComboPoints();
    for(int i = 0; i < cCPTotal; i ++)
    {
        Images.Data.rCPPip->Draw(cCPPipLft + (cCPPipWid * i), cCPPipTop);
    }

    //--Both shield and adrenaline are present:
    if(cShieldValue > 0 && cAdrenalineValue > 0)
    {
        //--Render shields in teal.
        StarlightColor::SetMixer(0.1f, 1.0f, 1.0f, 1.0f);
        Images.Data.rAllyHPFont->DrawTextArgs(cShieldLft, cHPTop, 0, 1.0f, "%i", cShieldValue);

        //--Render adrenaline in orange.
        StarlightColor::SetMixer(0.9f, 0.7f, 0.2f, 1.0f);
        Images.Data.rAllyHPFont->DrawTextArgs(cShieldLft, cMPTop, 0, 1.0f, "%i", cAdrenalineValue);
        StarlightColor::ClearMixer();
    }
    //--Just shield.
    else if(cShieldValue > 0)
    {
        StarlightColor::SetMixer(0.1f, 1.0f, 1.0f, 1.0f);
        Images.Data.rAllyHPFont->DrawTextArgs(cShieldLft, cHPTop, 0, 1.0f, "%i", cShieldValue);
        StarlightColor::ClearMixer();
    }
    //--Just adrenaline.
    else if(cAdrenalineValue > 0)
    {
        StarlightColor::SetMixer(0.9f, 0.7f, 0.2f, 1.0f);
        Images.Data.rAllyHPFont->DrawTextArgs(cShieldLft, cHPTop, 0, 1.0f, "%i", cAdrenalineValue);
        StarlightColor::ClearMixer();
    }

    ///--[Ability Display]
    //--Render all the character's available ability icons and a cursor for the player to interact with.
    //  In the combat inspector, abilities are always shown as available.
    if(mSkillCatalystExtension > 0)
    {
        int tFrameIndex = mSkillCatalystExtension - 1;
        if(tFrameIndex >= ADVCOMBAT_CATALYST_MAX) tFrameIndex = ADVCOMBAT_CATALYST_MAX - 1;
        Images.Data.rAbilityFrameCatalyst[tFrameIndex]->Draw();
    }
    Images.Data.rAbilityFrameCustom->Draw();
    Images.Data.rAbilityFrameMain->Draw();

    //--Positions.
    float cAbltyLft = ADVCOMBAT_POSITION_ABILITY_X;
    float cAbltyTop = ADVCOMBAT_POSITION_ABILITY_Y;
    float cAbltyWid = ADVCOMBAT_POSITION_ABILITY_W;
    float cAbltyHei = ADVCOMBAT_POSITION_ABILITY_H;

    //--Render abilities.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            //--Skip empty slots.
            AdvCombatAbility *rAbility = rActiveEntity->GetAbilityBySlot(x, y);
            if(!rAbility) continue;

            //--Resolve position.
            float cRenderX = cAbltyLft + (cAbltyWid * x);
            float cRenderY = cAbltyTop + (cAbltyHei * y);

            //--Get rendering images.
            SugarBitmap *rImageBack  = rAbility->GetIconBack();
            SugarBitmap *rImageFrame = rAbility->GetIconFrame();
            SugarBitmap *rImage      = rAbility->GetIcon();
            SugarBitmap *rCPImage    = rAbility->GetCPIcon();

            //--Ability is not cooling down:
            if(rAbility->GetCooldown() < 1)
            {
                if(rImageBack)  rImageBack ->Draw(cRenderX, cRenderY);
                if(rImageFrame) rImageFrame->Draw(cRenderX, cRenderY);
                if(rImage)      rImage     ->Draw(cRenderX, cRenderY);
                if(rCPImage)    rCPImage   ->Draw(cRenderX, cRenderY);
            }
            //--Ability is cooling, so render the turn timers.
            else
            {
                //--Render the non-CP parts, but grey the back and image out.
                StarlightColor::SetMixer(0.2f, 0.2f, 0.2f, 1.0f);
                if(rImageBack)  rImageBack ->Draw(cRenderX, cRenderY);
                StarlightColor::ClearMixer();
                if(rImageFrame) rImageFrame->Draw(cRenderX, cRenderY);
                StarlightColor::SetMixer(0.2f, 0.2f, 0.2f, 1.0f);
                if(rImage)      rImage     ->Draw(cRenderX, cRenderY);
                StarlightColor::ClearMixer();

                //--Render the turn icon.
                Images.Data.rCooldownClock->Draw(cRenderX+6, cRenderY-15);

                //--Render the turn indicator. It caps at 9.
                int tSlot = rAbility->GetCooldown();
                if(tSlot < 0) tSlot = 0;
                if(tSlot > 9) tSlot = 9;
                Images.Data.rCooldownNums[tSlot]->Draw(cRenderX+27, cRenderY-15);
            }
        }
    }

    ///--[Description Display]
    //--Backing.
    Images.Data.rDescriptionWindow->Draw();

    //--Render the description of the hovered ability here.
    if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_ABILITIES)
    {
        //--Get the highlighted ability. It may not exist if the slot is empty.
        AdvCombatAbility *rHighlightedAbility = rActiveEntity->GetAbilityBySlot(mCombatInspectorAbilityCursorX, mCombatInspectorAbilityCursorY);
        if(rHighlightedAbility)
        {
            //--Header.
            float cNameX = 985.0f;
            float cNameY = 527.0f;
            Images.Data.rAbilityHeaderFont->DrawText(cNameX, cNameY, SUGARFONT_AUTOCENTER_XY, 1.0f, rHighlightedAbility->GetDisplayName());

            //--Position.
            float cTxtLft = 839.0f;
            float cTxtTop = 557.0f;
            float cTxtHei =  20.0f;

            //--Render.
            int tTotalLines = rHighlightedAbility->GetDescriptionLinesTotal();
            for(int i = 0; i < tTotalLines; i ++)
            {
                StarlightString *rDescription = rHighlightedAbility->GetDescriptionLine(i);
                if(rDescription) rDescription->DrawText(cTxtLft, cTxtTop + (cTxtHei * i), 0, 1.0f, Images.Data.rAbilityDescriptionFont);
            }
        }
    }
    //--Effects.
    else if(mCombatInspectorMode == ADVCOMBAT_INSPECTOR_EFFECTS)
    {
        //--Get list.
        AdvCombatEntity *rSelectedEntity = (AdvCombatEntity *)pList->GetElementBySlot(mCombatInspectorCharacterCursor);
        if(!rSelectedEntity) { glTranslatef(0.0f, -cSlideOffsetY, 0.0f); return; }
        uint32_t tID = rSelectedEntity->GetID();
        SugarLinkedList *trEffectList = GetEffectsReferencing(rSelectedEntity);

        //--Scrub effects that are hidden.
        AdvCombatEffect *rCheckEffect = (AdvCombatEffect *)trEffectList->SetToHeadAndReturn();
        while(rCheckEffect)
        {
            if(!rCheckEffect->IsVisibleOnUI()) trEffectList->RemoveRandomPointerEntry();
            rCheckEffect = (AdvCombatEffect *)trEffectList->IncrementAndGetRandomPointerEntry();
        }

        //--Get the highlighted effect.
        AdvCombatEffect *rEffect = (AdvCombatEffect *)trEffectList->GetElementBySlot(mCombatInspectorEffectCursor);
        if(rEffect)
        {
            AdvCombatEffectTargetPack *rPackage = rEffect->GetTargetPackByID(tID);
            if(rPackage)
            {
                //--Header.
                float cNameX = 844.0f;
                float cNameY = 502.0f;
                Images.Data.rAbilityHeaderFont->DrawText(cNameX, cNameY, 0, 1.0f, rEffect->GetDisplayName());

                //--Position.
                float cTxtLft = 839.0f;
                float cTxtTop = 537.0f;
                float cTxtHei =  20.0f;

                //--Render. We start at line 2 since lines 0/1 are the title lines.
                int tTotalLines = rPackage->mDescriptionLinesTotal;
                for(int i = 2; i < tTotalLines; i ++)
                {
                    StarlightString *rDescription = rPackage->mDescriptionLines[i];
                    if(rDescription) rDescription->DrawText(cTxtLft, cTxtTop + (cTxtHei * (i-2)), 0, 1.0f, Images.Data.rAbilityDescriptionFont);
                }
            }
        }

        //--Clean.
        delete trEffectList;
    }

    ///--[Clean]
    glTranslatef(0.0f, -cSlideOffsetY, 0.0f);
}
