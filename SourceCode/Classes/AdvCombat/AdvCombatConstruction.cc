//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "AdventureLevel.h"
#include "AliasStorage.h"
#include "FieldAbility.h"
#include "TilemapActor.h"

//--CoreClasses
#include "SugarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "AdvCombatDefStruct.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioPackage.h"
#include "AudioManager.h"
#include "ControlManager.h"

void AdvCombat::Initialize()
{
    //--[Documentation and Setup]
    //--In order to keep the constructor in very large classes from becoming unmanageable, this Initialize() function is called
    //  when it is created. It otherwise does all the work of the constructor.
    //--Should only be called once on allocation. If resetting the class, use Reinitialize().

    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBAT;

    //--[AdvCombat]
    //--System
    mCombatReinitialized = false;
    mIsActive = false;
    mWasLastDefeatSurrender = false;
    mAIHandledAction = false;
    mPlayerGainedInitiative = false;
    mDoctorResolvePath = NULL;
    mDoctorResolveValue = 0;

    //--Field Abilities
    mExtraScripts = new SugarLinkedList(true);
    for(int i = 0; i < ADVCOMBAT_FIELD_ABILITY_SLOTS; i ++)
    {
        rActiveFieldAbilities[i] = NULL;
    }

    //--Introduction
    mIsIntroduction = true;
    mIntroTimer = 0;

    //--Title
    mShowTitle = false;
    mTitleTicks = 1;
    mTitleTicksMax = 1;
    mTitleText = NULL;

    //--Turn State
    mIsTurnActive = false;
    mCurrentTurn = -1;
    mTurnDisplayTimer = ACE_TURN_DISPLAY_TICKS;
    mTurnBarMoveTimer = 1;
    mTurnBarMoveTimerMax = 1;
    mTurnWidStart = 0;
    mTurnWidCurrent = 0;
    mTurnWidTarget = 0;
    mrTurnOrder = new SugarLinkedList(false);
    rTurnDiscardImg = NULL;

    //--Action Handling
    mNoEventsEndsAction = false;
    mWasLastActionFree = false;
    mActionFirstTick = false;
    mIsActionOccurring = false;
    mIsActionConcluding = false;
    mIsPerformingPlayerUpdate = false;
    mPlayerInterfaceTimer = 0;
    mAbilitySelectionX = 0;
    mAbilitySelectionY = 0;
    mAbilityHighlightPack.Initialize();
    mEventTimer = 0;
    mEventFirstTick = false;
    mEventCanRun = false;
    mEntitiesHaveRepositioned = false;
    rEventZeroPackage = NULL;
    rFiringEventPackage = NULL;
    mEventQueue = new SugarLinkedList(true);
    mTextScatterCounter = 0;
    mApplicationQueue = new SugarLinkedList(true);

    //--Second Page
    mShowingSecondPage = false;
    mSecondPageTimer = 0;
    mStoredAbilitySelectionX = 0;
    mStoredAbilitySelectionY = 0;
    mPageLftString = new StarlightString();
    mPageRgtString = new StarlightString();

    //--Target Selection
    mLockTargetClusters = false;
    mTargetClusterCounter = 0;
    mIsSelectingTargets = false;
    mTargetFlashTimer = 0;
    mLastUsedTargetType = InitializeString("Custom");
    mTargetClusters = new SugarLinkedList(true);

    //--Prediction Boxes
    mIsClearingPredictionBoxes = false;
    rPredictionCluster = NULL;
    mPredictionBoxes = new SugarLinkedList(true);

    //--Confirmation Window
    mIsShowingConfirmationWindow = false;
    mConfirmationTimer = 0;
    mConfirmationString = new StarlightString();
    mAcceptString = new StarlightString();
    mCancelString = new StarlightString();
    rConfirmationFunction = NULL;

    //--Combat Inspector
    mIsShowingCombatInspector = false;
    mCombatInspectorMode = ADVCOMBAT_INSPECTOR_CHARSELECT;
    mCombatInspectorTimer = 0;
    mCombatInspectorCharacterCursor = 0;
    mCombatInspectorEffectCursor = 0;
    mCombatInspectorAbilityCursorX = 0;
    mCombatInspectorAbilityCursorY = 0;
    mInspectorHighlightPos.Initialize();
    mInspectorHighlightWid.Initialize();
    mCombatInspectorPopulateSlots[CI_STATS_SLOT_HPMAX] = STATS_HPMAX;
    mCombatInspectorPopulateSlots[CI_STATS_SLOT_MPMAX] = STATS_MPMAX;
    mCombatInspectorPopulateSlots[CI_STATS_SLOT_MPREGEN] = STATS_MPREGEN;
    mCombatInspectorPopulateSlots[CI_STATS_SLOT_ATTACK] = STATS_ATTACK;
    mCombatInspectorPopulateSlots[CI_STATS_SLOT_INITIATIVE] = STATS_INITIATIVE;
    mCombatInspectorPopulateSlots[CI_STATS_SLOT_ACCURACY] = STATS_ACCURACY;
    mCombatInspectorPopulateSlots[CI_STATS_SLOT_EVADE] = STATS_EVADE;
    mCombatInspectorPopulateSlots[CI_STATS_SLOT_THREAT_MULTIPLIER] = STATS_THREAT_MULTIPLIER;
    mCombatInspectorSwitchToAbilitiesString = new StarlightString();
    mCombatInspectorSwitchToEffects = new StarlightString();;

    //--Ability Execution
    mSkillCatalystExtension = 0;
    rCurrentApplicationPack = NULL;
    rActiveAbility = NULL;
    rActiveTargetCluster = NULL;

    //--System Abilities
    mSysAbilityPassTurn = new AdvCombatAbility();
    mSysAbilityPassTurn->SetScriptResponse(ACA_SCRIPT_CODE_CREATE, true);
    mSysAbilityPassTurn->SetScriptResponse(ACA_SCRIPT_CODE_SPECIALCREATE, true);
    mSysAbilityPassTurn->SetInternalName("System|PassTurn");
    mSysAbilityRetreat = new AdvCombatAbility();
    mSysAbilityRetreat->SetScriptResponse(ACA_SCRIPT_CODE_CREATE, true);
    mSysAbilityRetreat->SetScriptResponse(ACA_SCRIPT_CODE_SPECIALCREATE, true);
    mSysAbilityRetreat->SetInternalName("System|Retreat");
    mSysAbilitySurrender = new AdvCombatAbility();
    mSysAbilitySurrender->SetScriptResponse(ACA_SCRIPT_CODE_CREATE, true);
    mSysAbilitySurrender->SetScriptResponse(ACA_SCRIPT_CODE_SPECIALCREATE, true);
    mSysAbilitySurrender->SetInternalName("System|Surrender");

    //--Animation Storage
    mCombatAnimations = new SugarLinkedList(true);
    mTextPackages = new SugarLinkedList(true);

    //--Option Flags
    mAutoUseDoctorBag = true;
    mTouristMode = false;
    mMemoryCursor = true;
    mRestoreMainWeaponOnCombatEnd = true;

    //--Combat State Flags
    mIsUnwinnable = false;
    mIsUnloseable = false;
    mIsUnretreatable = false;
    mIsUnsurrenderable = false;
    mResolutionTimer = 0;
    mResolutionState = 0;
    mCombatResolution = ADVCOMBAT_END_NONE;

    //--Enemy Storage
    mUniqueEnemyIDCounter = 0;
    rKnockoutEntity = NULL;
    mEnemyAliases = new AliasStorage();
    mEnemyRoster = new SugarLinkedList(true);
    mrEnemyCombatParty = new SugarLinkedList(false);
    mrEnemyReinforcements = new SugarLinkedList(false);
    mrEnemyGraveyard = new SugarLinkedList(false);
    mWorldReferences = new SugarLinkedList(true);

    //--Party Storage
    mPartyRoster = new SugarLinkedList(true);
    mrActiveParty = new SugarLinkedList(false);
    mrCombatParty = new SugarLinkedList(false);

    //--Stat Storage for Level Ups
    mJobTempStatistics.Zero();

    //--Victory Storage
    mXPToAward = 0;
    mJPToAward = 0;
    mPlatinaToAward = 0;
    mDoctorToAward = 0;
    mXPBonus = 0;
    mJPBonus = 0;
    mPlatinaBonus = 0;
    mDoctorBonus = 0;
    mXPToAwardStorage = 0;
    mJPToAwardStorage = 0;
    mPlatinaToAwardStorage = 0;
    mDoctorToAwardStorage = 0;
    mItemsToAwardList = new SugarLinkedList(true);
    mItemsAwardedList = new SugarLinkedList(true);

    //--Effect Storage
    mMustRebuildEffectReferences = false;
    mSpecialShieldString = new StarlightString();
    mSpecialAdrenalineString = new StarlightString();
    mGlobalEffectList = new SugarLinkedList(true);
    mEffectGraveyard = new SugarLinkedList(true);
    mrTempEffectList = new SugarLinkedList(false);

    ///--Response Constants
    //--Entity Script
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_BEGINCOMBAT]                  = -1; //--Handled by AdvCombatEntity::HandleCombatStart().
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_BEGINTURN]                    = ACE_SCRIPT_CODE_BEGINTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_BEGINACTION]                  = ACE_SCRIPT_CODE_BEGINACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_BEGINFREEACTION]              = ACE_SCRIPT_CODE_BEGINFREEACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ENDACTION]                    = ACE_SCRIPT_CODE_ENDACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ENDTURN]                      = ACE_SCRIPT_CODE_ENDTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ENDCOMBAT]                    = ACE_SCRIPT_CODE_ENDCOMBAT;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_QUEUEEVENT]                   = ACE_SCRIPT_CODE_EVENTQUEUED;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_AIKNOCKOUT]                   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE] = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_EXECUTE]              = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT]       = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX]   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS]           = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY]        = -1;

    //--AI Script
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_BEGINCOMBAT]                  = ACE_AI_SCRIPT_CODE_COMBATSTART;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_BEGINTURN]                    = ACE_AI_SCRIPT_CODE_TURNSTART;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_BEGINACTION]                  = ACE_AI_SCRIPT_CODE_ACTIONBEGIN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_BEGINFREEACTION]              = ACE_AI_SCRIPT_CODE_FREEACTIONBEGIN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ENDACTION]                    = ACE_AI_SCRIPT_CODE_ACTIONEND;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ENDTURN]                      = ACE_AI_SCRIPT_CODE_TURNEND;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ENDCOMBAT]                    = ACE_AI_SCRIPT_CODE_COMBATENDS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_QUEUEEVENT]                   = ACE_AI_SCRIPT_CODE_COMBATSTART;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_AIKNOCKOUT]                   = ACE_AI_SCRIPT_CODE_KNOCKEDOUT;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE] = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_EXECUTE]              = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT]       = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX]   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS]           = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY]         = ACE_AI_SCRIPT_CODE_APPLICATION_START;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY]        = ACE_AI_SCRIPT_CODE_APPLICATION_END;

    //--Job Script
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_BEGINCOMBAT]                  = ADVCJOB_CODE_BEGINCOMBAT;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_BEGINTURN]                    = ADVCJOB_CODE_BEGINTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_BEGINACTION]                  = ADVCJOB_CODE_BEGINACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_BEGINFREEACTION]              = ADVCJOB_CODE_BEGINFREEACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ENDACTION]                    = ADVCJOB_CODE_ENDACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ENDTURN]                      = ADVCJOB_CODE_ENDTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ENDCOMBAT]                    = ADVCJOB_CODE_ENDCOMBAT;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_QUEUEEVENT]                   = ADVCJOB_CODE_EVENTQUEUED;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_AIKNOCKOUT]                   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE] = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_EXECUTE]              = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT]       = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX]   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS]           = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY]        = -1;

    //--Ability Script
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_BEGINCOMBAT]                  = -1; //--Handled by AdvCombatEntity::HandleCombatStart().
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_BEGINTURN]                    = ACA_SCRIPT_CODE_BEGINTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_BEGINACTION]                  = ACA_SCRIPT_CODE_BEGINACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_BEGINFREEACTION]              = ACA_SCRIPT_CODE_BEGINFREEACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ENDACTION]                    = ACA_SCRIPT_CODE_POSTACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ENDTURN]                      = ACA_SCRIPT_CODE_TURNENDS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ENDCOMBAT]                    = ACA_SCRIPT_CODE_COMBATENDS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_QUEUEEVENT]                   = ACA_SCRIPT_CODE_EVENT_QUEUED;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_AIKNOCKOUT]                   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS]         = ACA_SCRIPT_CODE_PAINTTARGETS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE] = ACA_SCRIPT_CODE_PAINTTARGETS_RESPONSE;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_EXECUTE]              = ACA_SCRIPT_CODE_EXECUTE;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT]       = ACA_SCRIPT_CODE_GUI_APPLY_EFFECT;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX]   = ACA_SCRIPT_CODE_BUILD_PREDICTION_BOX;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS]           = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY]        = -1;

    //--Effect Script
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_BEGINCOMBAT]                  = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_BEGINTURN]                    = ACEFF_SCRIPT_CODE_BEGINTURN;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_BEGINACTION]                  = ACEFF_SCRIPT_CODE_BEGINACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_BEGINFREEACTION]              = ACEFF_SCRIPT_CODE_BEGINFREEACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ENDACTION]                    = ACEFF_SCRIPT_CODE_POSTACTION;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ENDTURN]                      = ACEFF_SCRIPT_CODE_TURNENDS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ENDCOMBAT]                    = ACEFF_SCRIPT_CODE_COMBATENDS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_QUEUEEVENT]                   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_AIKNOCKOUT]                   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE] = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_EXECUTE]              = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT]       = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX]   = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS]           = ACEFF_SCRIPT_CODE_APPLYSTATS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS]         = ACEFF_SCRIPT_CODE_UNAPPLYSTATS;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY]         = -1;
    cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY]        = -1;

    //--Cutscene Handlers
    mStandardRetreatScript = NULL;
    mStandardDefeatScript = NULL;
    mStandardVictoryScript = NULL;
    mCurrentRetreatScript = NULL;
    mCurrentDefeatScript = NULL;
    mCurrentVictoryScript = NULL;

    //--Audio
    mDefaultCombatMusic = InitializeString("Null");
    mCombatMusicStart = 0.0f;
    mNextCombatMusic = NULL;
    mNextCombatMusicStart = 0.0f;
    mEndCombatTracksTotal = 0;
    mEndCombatMusicResume = NULL;
    mQuietBackgroundMusicForVictory = false;
    rPreviousMusicWhenReplaying = NULL;
    mPreviousTimeWhenReplaying = 0.0f;

    //--Images
    memset(&Images, 0, sizeof(Images));

    //--Public Variables
    mIsRunningEventQuery = false;
}
void AdvCombat::Reinitialize()
{
    //--[Documentation and Setup]
    //--Re-initializes the class back to neutral variables, but leaves constants and referenced images in place. Used when the
    //  combat reboots between battles.

    //--[RootObject]
    //--System

    //--[AdvCombat]
    //--System
    mCombatReinitialized = true;
    mAIHandledAction = false;
    mPlayerGainedInitiative = false;
    mDoctorResolveValue = 0;

    //--Field Abilities
    mExtraScripts->ClearList();

    //--Title
    mShowTitle = false;
    mTitleTicks = 1;
    mTitleTicksMax = 1;
    ResetString(mTitleText, NULL);

    //--Introduction
    mIsIntroduction = true;
    mIntroTimer = 0;
    mWasLastDefeatSurrender = false;

    //--Turn State
    mIsTurnActive = false;
    mCurrentTurn = -1;
    mTurnDisplayTimer = ACE_TURN_DISPLAY_TICKS;
    mTurnBarMoveTimer = 1;
    mTurnBarMoveTimerMax = 1;
    mTurnWidStart = 0;
    mTurnWidCurrent = 0;
    mTurnWidTarget = 0;
    mrTurnOrder->ClearList();
    rTurnDiscardImg = NULL;

    //--Action Handling
    mNoEventsEndsAction = false;
    mWasLastActionFree = false;
    mActionFirstTick = false;
    mIsActionOccurring = false;
    mIsActionConcluding = false;
    mIsPerformingPlayerUpdate = false;
    mPlayerInterfaceTimer = 0;
    mAbilitySelectionX = 0;
    mAbilitySelectionY = 0;
    mAbilityHighlightPack.Initialize();
    mEventTimer = 0;
    mEventFirstTick = true;
    mEventCanRun = false;
    mEntitiesHaveRepositioned = false;
    rEventZeroPackage = NULL;
    rFiringEventPackage = NULL;
    mEventQueue->ClearList();
    mTextScatterCounter = 0;
    mApplicationQueue->ClearList();

    //--Second Page
    mShowingSecondPage = false;
    mSecondPageTimer = 0;
    mStoredAbilitySelectionX = 0;
    mStoredAbilitySelectionY = 0;

    //--Target Selection
    mLockTargetClusters = false;
    mTargetClusterCounter = 0;
    mIsSelectingTargets = false;
    mTargetClusterCursor = 0;
    mTargetFlashTimer = 0;
    ResetString(mLastUsedTargetType, "Custom");
    mTargetClusters->ClearList();

    //--Prediction Boxes
    mIsClearingPredictionBoxes = false;
    rPredictionCluster = NULL;
    mPredictionBoxes->ClearList();

    //--Confirmation Window
    mIsShowingConfirmationWindow = false;
    mConfirmationTimer = 0;
    rConfirmationFunction = NULL;

    //--Combat Inspector
    mIsShowingCombatInspector = false;
    mCombatInspectorMode = ADVCOMBAT_INSPECTOR_CHARSELECT;
    mCombatInspectorTimer = 0;
    mCombatInspectorCharacterCursor = 0;
    mCombatInspectorEffectCursor = 0;
    mCombatInspectorAbilityCursorX = 0;
    mCombatInspectorAbilityCursorY = 0;
    mInspectorHighlightPos.Initialize();
    mInspectorHighlightWid.Initialize();

    //--Ability Execution
    rCurrentApplicationPack = NULL;
    rActiveAbility = NULL;
    rActiveTargetCluster = NULL;

    //--System Abilities
    //--Animation Storage
    mTextPackages->ClearList();

    //--Option Flags
    //--Combat State Flags
    mIsUnwinnable = false;
    mIsUnloseable = false;
    mIsUnretreatable = false;
    mIsUnsurrenderable = false;
    mResolutionTimer = 0;
    mResolutionState = 0;
    mCombatResolution = ADVCOMBAT_END_NONE;

    //--Enemy Storage
    rKnockoutEntity = NULL;
    mUniqueEnemyIDCounter = 0;
    mEnemyRoster->ClearList();
    mrEnemyCombatParty->ClearList();
    mrEnemyReinforcements->ClearList();
    mrEnemyGraveyard->ClearList();
    mWorldReferences->ClearList();

    //--Party Storage
    mrCombatParty->ClearList();
    AdvCombatEntity *rCharacterPtr = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rCharacterPtr)
    {
        rCharacterPtr->Reinitialize();
        mrCombatParty->AddElementAsTail(mrActiveParty->GetIteratorName(), rCharacterPtr);
        rCharacterPtr = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }

    //--Stat Storage for Level Ups
    mJobTempStatistics.Zero();

    //--Victory Storage
    mXPToAward = 0;
    mJPToAward = 0;
    mPlatinaToAward = 0;
    mDoctorToAward = 0;
    mXPBonus = 0;
    mJPBonus = 0;
    mPlatinaBonus = 0;
    mDoctorBonus = 0;
    mXPToAwardStorage = 0;
    mJPToAwardStorage = 0;
    mPlatinaToAwardStorage = 0;
    mDoctorToAwardStorage = 0;
    mItemsToAwardList->ClearList();
    mItemsAwardedList->ClearList();

    //--Effect Storage
    mMustRebuildEffectReferences = false;
    mGlobalEffectList->ClearList();
    mEffectGraveyard->ClearList();
    mrTempEffectList->ClearList();

    //--Cutscene Handlers
    ResetString(mCurrentRetreatScript, NULL);
    ResetString(mCurrentDefeatScript, NULL);
    ResetString(mCurrentVictoryScript, NULL);

    //--Audio
    free(mEndCombatMusicResume);
    mEndCombatTracksTotal = 0;
    mEndCombatMusicResume = NULL;

    //--Images
    //--Public Variables
    mIsRunningEventQuery = false;

    //--[Confirmation Strings]
    //--These strings reinitialize every time combat starts since the player may have changed controls.
    mAcceptString->SetString("[IMG0] Accept");
    mAcceptString->AllocateImages(1);
    mAcceptString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mAcceptString->CrossreferenceImages();

    mCancelString->SetString("[IMG0] Cancel");
    mCancelString->AllocateImages(1);
    mCancelString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mCancelString->CrossreferenceImages();

    //--[Combat Inspector Strings]
    //--As above, need to be updated as controls may change.
    mCombatInspectorSwitchToAbilitiesString->SetString("[IMG0] Check Abilities");
    mCombatInspectorSwitchToAbilitiesString->AllocateImages(1);
    mCombatInspectorSwitchToAbilitiesString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Jump"));
    mCombatInspectorSwitchToAbilitiesString->CrossreferenceImages();

    mCombatInspectorSwitchToEffects->SetString("[IMG0] Check Effects");
    mCombatInspectorSwitchToEffects->AllocateImages(1);
    mCombatInspectorSwitchToEffects->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mCombatInspectorSwitchToEffects->CrossreferenceImages();

    //--[Page Change Strings]
    //--Changes with controls.
    mPageLftString->SetString("[IMG0]/[IMG1] Abilities");
    mPageLftString->AllocateImages(2);
    mPageLftString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("DnLevel"));
    mPageLftString->SetImageP(1, 3.0f, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mPageLftString->CrossreferenceImages();

    mPageRgtString->SetString("[IMG0] Change Job");
    mPageRgtString->AllocateImages(1);
    mPageRgtString->SetImageP(0, 3.0f, ControlManager::Fetch()->ResolveControlImage("DnLevel"));
    mPageRgtString->CrossreferenceImages();

    //--[Data Library]
    //--Create this path to store temporary variables.
    DataLibrary::Fetch()->AddPath("Root/Variables/Combat/");

    //--[Effects]
    //--Call this to clear any dangling effect references from all entities.
    RebuildEffectReferences();

    //--[Music]
    //--If there is a music track available, use that.
    mQuietBackgroundMusicForVictory = false;
    if(mNextCombatMusic || mDefaultCombatMusic)
    {
        //--Setup.
        AudioManager *rAudioManager = AudioManager::Fetch();
        AdventureLevel *rCheckLevel = AdventureLevel::Fetch();

        //--Determine which track to use.
        const char *rUseMusic = mDefaultCombatMusic;
        float tUseMusicStart = mCombatMusicStart;
        if(mNextCombatMusic)
        {
            rUseMusic = mNextCombatMusic;
            tUseMusicStart = mNextCombatMusicStart;
        }

        //--Not Layering Tracks
        if(rCheckLevel && !rCheckLevel->IsLayeringMusic())
        {
            //--If the music start position is -1, don't reset the music at all. Let it play.
            if(tUseMusicStart == -1.0f)
            {
                mQuietBackgroundMusicForVictory = true;
            }
            //--Store the music position and play the new music.
            else
            {
                //--Store.
                AudioPackage *rPlayingMusic = rAudioManager->GetPlayingMusic();
                if(rPlayingMusic)
                {
                    mEndCombatTracksTotal = 1;
                    mEndCombatMusicResume = (float *)starmemoryalloc(sizeof(float));
                    mEndCombatMusicResume[0] = rPlayingMusic->GetPosition();
                }

                //--Start playing music.
                rAudioManager->PlayMusic(rUseMusic);
                rAudioManager->SeekMusicTo(tUseMusicStart);
            }
        }
        //--Layered tracks. This is only the case if the combat music is not the maximum intensity track.
        //  The music positions are still stored but the values won't be used.
        else if(rCheckLevel && !AdventureLevel::xIsCombatMaxIntensity)
        {
            //--Allocate.
            mEndCombatTracksTotal = AdventureLevel::xCurrentlyRunningTracks;
            mEndCombatMusicResume = (float *)starmemoryalloc(sizeof(float) * mEndCombatTracksTotal);

            //--Iterate.
            for(int i = 0; i < AdventureLevel::xCurrentlyRunningTracks; i ++)
            {
                //--Check the package. If it exists, store its position.
                AudioPackage *rLayer = rAudioManager->GetMusicPack(AdventureLevel::xLayerNames[i]);
                if(rLayer)
                {
                    mEndCombatMusicResume[i] = rLayer->GetPosition();
                    rLayer->FadeOut(15);
                }
                //--Otherwise, store 0.0f.
                else
                {
                    mEndCombatMusicResume[i] = 0.0f;
                }
            }

            //--Start playing music.
            rAudioManager->PlayMusic(rUseMusic);
            rAudioManager->SeekMusicTo(tUseMusicStart);
        }
        //--Mandate combat intensity as maximum if combat is set that way.
        else if(rCheckLevel && AdventureLevel::xIsCombatMaxIntensity)
        {
            AdventureLevel::xCombatMandatedIntensity = 100.0f;
        }

        //--If the next-music handler was used, clear it.
        if(mNextCombatMusic)
        {
            ResetString(mNextCombatMusic, NULL);
            mNextCombatMusicStart = 0.0f;
        }
    }
}
void AdvCombat::Construct()
{
    //--[Documentation and Setup]
    //--Resolve all image references in the object, then verify them. Also sets up hardcoded positions.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--[Fonts]
    Images.Data.rVictoryFontHeader       = rDataLibrary->GetFont("Adventure Combat Header");
    Images.Data.rVictoryFontExp          = rDataLibrary->GetFont("Adventure Combat Exp");
    Images.Data.rVictoryFontPlatina      = rDataLibrary->GetFont("Adventure Combat Platina");
    Images.Data.rVictoryFontDoctor       = rDataLibrary->GetFont("Adventure Combat Doctor");
    Images.Data.rVictoryFontItem         = rDataLibrary->GetFont("Adventure Combat VicItem");
    Images.Data.rAllyHPFont              = rDataLibrary->GetFont("Adventure Combat Ally Bar");
    Images.Data.rPortraitHPFont          = rDataLibrary->GetFont("Adventure Combat Portrait UI");
    Images.Data.rActiveCharacterNameFont = rDataLibrary->GetFont("Adventure Combat Acting Name Font");
    Images.Data.rNewTurnFont             = rDataLibrary->GetFont("Adventure Combat New Turn Font");
    Images.Data.rAbilityHeaderFont       = rDataLibrary->GetFont("Adventure Combat Description Header");
    Images.Data.rAbilityDescriptionFont  = rDataLibrary->GetFont("Adventure Combat Description");
    Images.Data.rConfirmationFontBig     = rDataLibrary->GetFont("Adventure Combat Confirmation Big");
    Images.Data.rConfirmationFontSmall   = rDataLibrary->GetFont("Adventure Combat Confirmation Small");
    Images.Data.rTargetClusterFont       = rDataLibrary->GetFont("Adventure Combat Target Cluster Selection");
    Images.Data.rCombatTextFont          = rDataLibrary->GetFont("Adventure Combat Damage Effect");
    Images.Data.rAbilityTitleFont        = rDataLibrary->GetFont("Adventure Combat Ability Title");
    Images.Data.rEffectFont              = rDataLibrary->GetFont("Adventure Menu Effect");
    Images.Data.rPredictionFont          = rDataLibrary->GetFont("Adventure Combat Prediction");
    Images.Data.rInspectorHeading        = rDataLibrary->GetFont("Adventure Combat Inspector Heading");
    Images.Data.rInspectorNameFont       = rDataLibrary->GetFont("Adventure Combat Inspector Names");
    Images.Data.rInspectorEffectFont     = rDataLibrary->GetFont("Adventure Combat Inspector Effects");

    //--Reinforcement font, used by TilemapActors. Resolved by this object for convenience.
    TilemapActor::xrReinforcementFont = rDataLibrary->GetFont("Adventure Level Reinforcement");;

    //--[Ally Bar]
    const char *cAllyBarNames[] = {"AllyFrame", "AllyPortraitMask"};
    Crossload(&Images.Data.rAllyFrame, sizeof(SugarBitmap *) * 2, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/AllyBar|%s", cAllyBarNames);

    //--[Defeat Overlay]
    const char *cDefeatNames[] = {"Defeat_0", "Defeat_1", "Defeat_2", "Defeat_3", "Defeat_4", "Defeat_5"};
    Crossload(&Images.Data.rDefeatFrames[0], sizeof(SugarBitmap *) * ADVCOMBAT_DEFEAT_FRAMES_TOTAL, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/Defeat|%s", cDefeatNames);

    //--[Enemy Health Bar]
    const char *cEnemyHealthBarNames[] = {"EnemyHealthBarEdge", "EnemyHealthBarFill", "EnemyHealthBarFrame", "EnemyHealthBarStun", "EnemyHealthBarStunMarker", "EnemyHealthBarUnder"};
    Crossload(&Images.Data.rEnemyHealthBarEdge, sizeof(SugarBitmap *) * 6, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/EnemyHealthBar|%s", cEnemyHealthBarNames);

    //--[Combat Inspector]
    //--Base.
    const char *cInspectorNames[] = {"Frames", "Name Inset"};
    Crossload(&Images.Data.rInspectorFrames, sizeof(SugarBitmap *) * 2, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/Inspector|%s", cInspectorNames);
    Images.Data.rInspectorClock = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Turns");

    //--Status Icons
    const char *cInsIcons[] = {"Health", "Mana", "Mana", "Attack", "Initiative", "Accuracy", "Evade", "Terrify"};
    Crossload(&Images.Data.rInspectorStatusIcons[0], sizeof(SugarBitmap *) * CI_STATS_SLOT_TOTAL, sizeof(SugarBitmap *), "Root/Images/AdventureUI/StatisticIcons/%s", cInsIcons);

    //--Resistance Icons
    const char *cInResIcons[] = {"Protection", "Slashing", "Striking", "Piercing", "Flaming", "Freezing", "Shocking", "Crusading", "Obscuring", "Bleeding", "Poisoning", "Corroding", "Terrifying"};
    Crossload(&Images.Data.rInspectorResistanceIcons[0], sizeof(SugarBitmap *) * CI_RESIST_SLOT_TOTAL, sizeof(SugarBitmap *), "Root/Images/AdventureUI/DamageTypeIcons/%s", cInResIcons);

    //--Other Icons.
    Images.Data.rInspectorHealth     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Health");
    Images.Data.rInspectorAdrenaline = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Adrenaline");
    Images.Data.rInspectorShields    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Shields");

    //--[Player Interface]
    const char *cPlayerInterfaceNames[] = {"AbilityFrameCatalyst1", "AbilityFrameCatalyst2", "AbilityFrameCatalyst3", "AbilityFrameCatalyst4", "AbilityFrameCatalyst5", "AbilityFrameCatalyst6",
     "AbilityFrameCustom", "AbilityFrameMain", "AbilityFrameSecond", "AbilityHighlight", "CPPip", "Descriptionwindow", "HealthBarAdrenaline",
     "HealthBarHPFill", "HealthBarMPFill", "HealthBarMix", "HealthBarShield", "HealthBarUnderlay", "MainHealthBarFrame", "MainNameBanner", "MainPortraitMask", "MainPortraitRing",
     "MainPortraitRingBack", "PredictionBox", "TargetArrowLft", "TargetArrowRgt", "TargetBox", "TitleBox"};
    Crossload(&Images.Data.rAbilityFrameCatalyst[0], sizeof(SugarBitmap *) * 28, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/PlayerInterface|%s", cPlayerInterfaceNames);

    //--Cooldown parts
    Images.Data.rCooldownClock   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/CmbClock");
    Images.Data.rCooldownNums[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/Cmb0");
    Images.Data.rCooldownNums[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/Cmb1");
    Images.Data.rCooldownNums[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/Cmb2");
    Images.Data.rCooldownNums[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/Cmb3");
    Images.Data.rCooldownNums[4] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/Cmb4");
    Images.Data.rCooldownNums[5] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/Cmb5");
    Images.Data.rCooldownNums[6] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/Cmb6");
    Images.Data.rCooldownNums[7] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/Cmb7");
    Images.Data.rCooldownNums[8] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/Cmb8");
    Images.Data.rCooldownNums[9] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/Cmb9");

    //--[Turn Order]
    const char *cTurnOrderNames[] = {"TurnOrderBack", "TurnOrderCircle", "TurnOrderEdge"};
    Crossload(&Images.Data.rTurnOrderBack, sizeof(SugarBitmap *) * 3, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/TurnOrder|%s", cTurnOrderNames);

    //--[Victory]
    const char *cVictoryNames[] = {"BannerBack", "BannerBig", "DoctorBack", "DoctorFill", "DoctorFrame", "DropFrame", "ExpFrameBack", "ExpFrameFill", "ExpFrameFront", "ExpFrameMask"};
    Crossload(&Images.Data.rVictoryBannerBack, sizeof(SugarBitmap *) * 10, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/Victory|%s", cVictoryNames);

    //--[Verify]
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *), false);

    //--[Special Strings]
    mSpecialShieldString->AllocateImages(1);
    mSpecialShieldString->SetImageS(0, 4.0f, "Root/Images/AdventureUI/StatisticIcons/Shields");
    mSpecialAdrenalineString->AllocateImages(1);
    mSpecialAdrenalineString->SetImageS(0, 4.0f, "Root/Images/AdventureUI/StatisticIcons/Adrenaline");
    fprintf(stderr, "Constructed combat.\n");
}
void AdvCombat::Disassemble()
{
    //--[Documentation and Setup]
    //--Performs the work of the destructor but does not delete the class.

    //--[AdvCombat]
    //--System
    free(mDoctorResolvePath);

    //--Field Abilities
    delete mExtraScripts;

    //--Introduction
    //--Title
    free(mTitleText);

    //--Turn State
    delete mrTurnOrder;

    //--Action Handling
    delete mEventQueue;
    delete mApplicationQueue;

    //--Second Page
    delete mPageLftString;
    delete mPageRgtString;

    //--Target Selection
    delete mTargetClusters;

    //--Prediction Boxes
    delete mPredictionBoxes;

    //--Confirmation Window
    delete mConfirmationString;
    delete mAcceptString;
    delete mCancelString;

    //--Combat Inspector
    delete mCombatInspectorSwitchToAbilitiesString;
    delete mCombatInspectorSwitchToEffects;

    //--Ability Execution
    //--System Abilities
    delete mSysAbilityPassTurn;
    delete mSysAbilityRetreat;
    delete mSysAbilitySurrender;

    //--Animation Storage
    delete mCombatAnimations;
    delete mTextPackages;

    //--Option Flags
    //--Combat State Flags
    //--Enemy Storage
    delete mEnemyAliases;
    delete mEnemyRoster;
    delete mrEnemyCombatParty;
    delete mrEnemyReinforcements;
    delete mrEnemyGraveyard;
    delete mWorldReferences;

    //--Party Storage
    delete mPartyRoster;
    delete mrActiveParty;
    delete mrCombatParty;

    //--Stat Storage for Level Ups
    //--Victory Storage
    delete mItemsToAwardList;
    delete mItemsAwardedList;

    //--Effect Storage
    delete mGlobalEffectList;
    delete mSpecialShieldString;
    delete mSpecialAdrenalineString;
    delete mEffectGraveyard;
    delete mrTempEffectList;

    //--Cutscene Handlers
    free(mStandardRetreatScript);
    free(mStandardDefeatScript);
    free(mStandardVictoryScript);
    free(mCurrentRetreatScript);
    free(mCurrentDefeatScript);
    free(mCurrentVictoryScript);

    //--Audio
    free(mDefaultCombatMusic);
    free(mNextCombatMusic);
    free(mEndCombatMusicResume);

    //--Images
}

//--[Public Statics]
bool AdvCombat::xIsAISpawningEvents = false;
