//--[AdvCombatPrediction]
//--Prediction box that appears over a target during target selection.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--[Local Definitions]
//--Timing
#define ADVCOMBPREDICTION_VIS_TICKS 5

//--Padding
#define ADVCOMPREDICTION_PADDING_PER_LINE 1.0f
#define ADVCOMPREDICTION_PADDING_BORDER 3.0f
#define ADVCOMPREDICTION_PADDING_TEXT_V -8.0f

//--[Classes]
class AdvCombatPrediction : public RootObject
{
    private:
    //--System
    bool mIsShowing;
    AdvCombatEntity *rHostEntity;

    //--Display
    int mVisTimer;
    float mXOffset;
    float mYOffset;
    int mStringsTotal;
    StarlightString **mStrings;

    //--Calculations
    float mXSize;
    float mYSize;

    //--Images
    float mBorderCardInd;
    SugarFont *rTextFont;
    SugarBitmap *rBorderCard;

    protected:

    public:
    //--System
    AdvCombatPrediction();
    virtual ~AdvCombatPrediction();

    //--Public Variables
    //--Property Queries
    bool IsShowing();
    bool IsVisible();

    //--Manipulators
    void SetShowing(bool pFlag);
    void AllocateStrings(int pTotal);
    void SetFont(SugarFont *pFont);
    void SetBorderCard(SugarBitmap *pBitmap, float pIndent);
    void SetHostEntity(AdvCombatEntity *pEntity);
    void SetOffsets(float pXOffset, float pYOffset);

    //--Core Methods
    void RunCalculations();

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    StarlightString *GetString(int pIndex);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

