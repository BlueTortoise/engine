//--Base
#include "AdvCombatPrediction.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers

//=========================================== System ==============================================
AdvCombatPrediction::AdvCombatPrediction()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCBOMATPREDICTION;

    //--[AdvCombatPrediction]
    //--System
    mIsShowing = true;
    rHostEntity = NULL;

    //--Display
    mVisTimer = 0;
    mXOffset = 0.0f;
    mYOffset = 0.0f;
    mStringsTotal = 0;
    mStrings = NULL;

    //--Calculations
    mXSize = 1.0f;
    mYSize = 1.0f;

    //--Images
    mBorderCardInd = 1.0f;
    rTextFont = NULL;
    rBorderCard = NULL;
}
AdvCombatPrediction::~AdvCombatPrediction()
{
    for(int i = 0; i < mStringsTotal; i ++) delete mStrings[i];
    free(mStrings);
}

//====================================== Property Queries =========================================
bool AdvCombatPrediction::IsShowing()
{
    return mIsShowing;
}
bool AdvCombatPrediction::IsVisible()
{
    return (mVisTimer > 0);
}

//========================================= Manipulators ==========================================
void AdvCombatPrediction::SetShowing(bool pFlag)
{
    mIsShowing = pFlag;
}
void AdvCombatPrediction::AllocateStrings(int pTotal)
{
    //--Deallocate.
    for(int i = 0; i < mStringsTotal; i ++) delete mStrings[i];
    free(mStrings);

    //--Reset.
    mStringsTotal = 0;
    mStrings = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mStringsTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mStrings = (StarlightString **)starmemoryalloc(sizeof(StarlightString *) * mStringsTotal);
    for(int i = 0; i < mStringsTotal; i ++) mStrings[i] = new StarlightString();
}
void AdvCombatPrediction::SetFont(SugarFont *pFont)
{
    rTextFont = pFont;
}
void AdvCombatPrediction::SetBorderCard(SugarBitmap *pBitmap, float pIndent)
{
    mBorderCardInd = pIndent;
    rBorderCard = pBitmap;
    if(mBorderCardInd < 1.0f) mBorderCardInd = 1.0f;
}
void AdvCombatPrediction::SetHostEntity(AdvCombatEntity *pEntity)
{
    rHostEntity = pEntity;
}
void AdvCombatPrediction::SetOffsets(float pXOffset, float pYOffset)
{
    mXOffset = pXOffset;
    mYOffset = pYOffset;
}

//========================================= Core Methods ==========================================
void AdvCombatPrediction::RunCalculations()
{
    //--Once all setup work is completed, precalculates sizes.
    if(!rTextFont || !rBorderCard) return;

    //--Determine the longest string. There is a minimum size. We also order all strings to resolve
    //  their images while we're here.
    float tLongest = rBorderCard->GetTrueWidth();
    for(int i = 0; i < mStringsTotal; i ++)
    {
        mStrings[i]->CrossreferenceImages();
        float tLength = mStrings[i]->GetLength(rTextFont);
        if(tLength > tLongest) tLongest = tLength;
    }

    //--Width is the longest string plus padding.
    mXSize = (tLongest) + (ADVCOMPREDICTION_PADDING_BORDER * 2.0f) + (mBorderCardInd * 2.0f);

    //--Compute height.
    mYSize = ((rTextFont->GetTextHeight() + ADVCOMPREDICTION_PADDING_PER_LINE) * mStringsTotal) + (ADVCOMPREDICTION_PADDING_BORDER * 2.0f) + (mBorderCardInd * 2.0f) + ADVCOMPREDICTION_PADDING_TEXT_V;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void AdvCombatPrediction::Update()
{
    //--Visibility timing.
    if(mIsShowing)
    {
        if(mVisTimer < ADVCOMBPREDICTION_VIS_TICKS) mVisTimer ++;
    }
    else
    {
        if(mVisTimer > 0) mVisTimer --;
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void AdvCombatPrediction::Render()
{
    //--[Documentation and Setup]
    //--Renders the object, either as offset from the host entity, or at a fixed position if no host
    //  entity exists.
    if(!rTextFont || !rBorderCard) return;

    //--[Visibility]
    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticOut(mVisTimer, ADVCOMBPREDICTION_VIS_TICKS);
    if(cAlpha <= 0.0f) return;
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--[Compute Position]
    //--If there is no entity to attach to, the offsets are the top-left positions.
    float cLft = mXOffset;
    float cTop = mYOffset;

    //--If an entity exists to attach to, the offsets are relative to their center.
    if(rHostEntity)
    {
        cLft = rHostEntity->GetCombatX() + mXOffset - (mXSize * 0.50f);
        cTop = rHostEntity->GetCombatY() + mYOffset - (mYSize * 0.50f);
    }

    //--[Border Card]
    //--Handled by subroutine.
    RenderExpandableHighlight(cLft, cTop, cLft + mXSize, cTop + mYSize, mBorderCardInd, rBorderCard);

    //--[Strings]
    //--Render each string.
    float cXPos = cLft + mBorderCardInd + ADVCOMPREDICTION_PADDING_BORDER;
    float tYPos = cTop + mBorderCardInd + ADVCOMPREDICTION_PADDING_BORDER + ADVCOMPREDICTION_PADDING_TEXT_V;
    for(int i = 0; i < mStringsTotal; i ++)
    {
        mStrings[i]->DrawText(cXPos, tYPos, 0, 1.0f, rTextFont);
        tYPos = tYPos + rTextFont->GetTextHeight() + ADVCOMPREDICTION_PADDING_PER_LINE;
    }

    //--Clean.
    StarlightColor::ClearMixer();
}

//======================================= Pointer Routing =========================================
StarlightString *AdvCombatPrediction::GetString(int pIndex)
{
    if(pIndex < 0 || pIndex >= mStringsTotal) return NULL;
    return mStrings[pIndex];
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
