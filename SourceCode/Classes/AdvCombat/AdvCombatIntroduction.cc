//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"

//--[Local Definitions]
#define ADVCOMBAT_INTRO_TICKS 15

//#define COMBAT_INTRO_DEBUG
#ifdef COMBAT_INTRO_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--[Update]
void AdvCombat::UpdateIntroduction()
{
    //--[Documentation and Setup]
    //--Handles the introduction to combat. Presently this is just the enemies sliding in from the
    //  right side of the screen. Once completed, the game begins setting turn order.
    //--By default, the introduction is active as soon as the AdvCombat class Reinitialize() is called.

    //--[Zero Tick]
    if(mIntroTimer == 0)
    {
        //--Party positions. The party is offscreen to the left at combat start.
        AdvCombatEntity *rPartyMember = (AdvCombatEntity *)mrCombatParty->PushIterator();
        while(rPartyMember)
        {
            //--Compute Y position. Rendering is centered, but we start with bottom-up.
            float cRenderY = ADVCOMBAT_POSITION_STD_Y;
            SugarBitmap *rRenderImg = rPartyMember->GetCombatPortrait();
            if(rRenderImg) cRenderY = cRenderY - (rRenderImg->GetHeight() * 0.50f);

            //--Compute the ideal position.
            float cIdealX = ComputeOffscreenX(true, rRenderImg);

            //--Set. Move offscreen immediately.
            rPartyMember->SetIdealPosition(cIdealX, cRenderY);
            rPartyMember->MoveToIdealPosition(0);

            //--Next.
            rPartyMember = (AdvCombatEntity *)mrCombatParty->AutoIterate();
        }

        //--Enemy positions.
        int i = 0;
        AdvCombatEntity *rEnemy = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
        while(rEnemy)
        {
            //--Compute Y position. Rendering is centered, but we start with bottom-up.
            float cRenderY = ADVCOMBAT_POSITION_STD_Y;
            SugarBitmap *rRenderImg = rEnemy->GetCombatPortrait();
            if(rRenderImg) cRenderY = cRenderY - (rRenderImg->GetHeight() * 0.50f);

            //--Compute the ideal position.
            float cEnemyStartX = ComputeOffscreenX(false, rRenderImg);
            float cEnemyIdealX = ComputeIdealX(false, i, mrEnemyCombatParty->GetListSize());

            //--Set. Enemy starts offscreen then moves to their ideal position.
            rEnemy->SetIdealPosition(cEnemyIdealX, cRenderY);
            rEnemy->MoveToPosition(cEnemyStartX, cRenderY, 0);
            rEnemy->MoveToIdealPosition(ADVCOMBAT_INTRO_TICKS + 30);
            rEnemy->SetIgnoreMovementForBlocking(true);

            //--Next.
            i ++;
            rEnemy = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
        }
    }

    //--[Timer]
    if(mIntroTimer < ADVCOMBAT_INTRO_TICKS)
    {
        //--Increment.
        mIntroTimer ++;

        //--Ending case.
        if(mIntroTimer >= ADVCOMBAT_INTRO_TICKS)
        {
            mIsIntroduction = false;
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_BEGINCOMBAT);
        }
    }
}

//--[Render]
void AdvCombat::RenderIntroduction()
{
    //--[Documentation and Setup]
    //--Renders the introduction. Presently this is just enemies sliding in from the right, and the UI
    //  sliding onto the field.
    if(!Images.mIsReady) return;
    DebugPush(true, "Combat Introduction Render: Begin.\n");
    float cPercent = EasingFunction::QuadraticInOut(mIntroTimer, ADVCOMBAT_INTRO_TICKS);

    //--[Backing]
    //--Flat grey backing.
    SugarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);

    //--[UI Positions]
    //--All UI objects share the offset of the largest object, which is ally bars in the top left. The maximum
    //  width is computed here.
    float cMaxWidth = Images.Data.rAllyFrame->GetXOffset() + Images.Data.rAllyFrame->GetWidth();
    float cXTranslate = cMaxWidth * (1.0f - cPercent);

    //--Render the ally frames offset to the left.
    DebugPrint("Rendering ally bars.\n");
    glTranslatef(cXTranslate * -1.0f, 0.0f, 0.0f);
    RenderAllyBars();
    glTranslatef(cXTranslate *  1.0f, 0.0f, 0.0f);

    //--Now shift to the right and render the turn order nub.
    DebugPrint("Rendering turn order.\n");
    glTranslatef(cXTranslate *  1.0f, 0.0f, 0.0f);
    RenderTurnOrder();
    glTranslatef(cXTranslate * -1.0f, 0.0f, 0.0f);
    DebugPop("Exited normally.\n");
}
