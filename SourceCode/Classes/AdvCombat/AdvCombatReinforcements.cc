//--Base
#include "AdvCombat.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "DeletionFunctions.h"

//--Libraries
//--Managers

void AdvCombat::RegisterWorldReference(TilemapActor *pActor, int pReinforcementTurns)
{
    //--Adds a reference to a TilemapActor in the world. This actor will become "defeated" if the player wins the battle
    //  and the specified number of turns elapses. It is implied that this is them adding their reinforcements to the battle,
    //  and them being defeated therein.
    if(!pActor) return;

    //--Create a wrapper.
    SetMemoryData(__FILE__, __LINE__);
    WorldRefPack *nPack = (WorldRefPack *)starmemoryalloc(sizeof(WorldRefPack));
    nPack->Initialize();
    nPack->mTurns = pReinforcementTurns;
    nPack->rActor = pActor;
    mWorldReferences->AddElement("X", nPack, &FreeThis);
}
