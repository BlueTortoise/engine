//--[AdvCombat]
//--Adventure Mode's combat engine.

//--Party Notes:
//--The player's party is split into the Roster, the Active Party, and the Combat Party.
//--The Roster is all of the player's party members, including ones not in the party. This is
//  used when switching party members later in the game.
//--The Active Party is the characters in the player's party out of combat.
//--The Combat Party is the characters under the player's control in combat. This starts as a
//  copy of the Active Party, but enemies can change sides and the player's party can change sides.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "DisplayManager.h"
#include "AdvCombatDefStruct.h"

//--[Local Structures]
typedef struct AdvVictoryItemPack
{
    const char *rItemNameRef;
    SugarBitmap *rItemImg;
    int mTimer;
    void Initialize()
    {
        rItemNameRef = NULL;
        rItemImg = NULL;
        mTimer = 0;
    }
}AdvVictoryItemPack;

//--[Local Definitions]
//--Timers
#define ADVCOMBAT_HIGHLIGHT_MOVE_TICKS 10
#define ADVCOMBAT_CONFIRMATION_TICKS 15
#define ADVCOMBAT_STD_MOVE_TICKS 25
#define ADVCOMBAT_MOVE_TRANSITION_TICKS 25
#define ADVCOMBAT_TARGET_FLASH_PERIOD 30
#define ADVCOMBAT_TITLE_TICKS_STANDARD 120
#define ADVCOMBAT_INFIELD_TEXT_TICKS 30
#define ADVCOMBAT_EVENT_TICKS_MINIMUM 25
#define ADVCOMBAT_VICTORY_PACK_TICKS 15
#define ADVCOMBAT_PAGE_CHANGE_TICKS 5

//--Rendering
#define ADVCOMBAT_STD_BACKING_OPACITY 0.75f

//--Party
#define ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE 4

//--Combat Resolution Types
#define ADVCOMBAT_END_NONE 0
#define ADVCOMBAT_END_VICTORY 1
#define ADVCOMBAT_END_DEFEAT 2
#define ADVCOMBAT_END_RETREAT 3
#define ADVCOMBAT_END_SURRENDER 4
#define ADVCOMBAT_END_TOTAL 5

//--Victory
#define ADVCOMBAT_VICTORY_OVERLAY 0
#define ADVCOMBAT_VICTORY_ZOOM 1
#define ADVCOMBAT_VICTORY_SLIDE 2
#define ADVCOMBAT_VICTORY_COUNT_OFF 3
#define ADVCOMBAT_VICTORY_DOCTOR 4
#define ADVCOMBAT_VICTORY_HIDE 5

#define ADVCOMBAT_VICTORY_OVERLAY_TICKS 42
#define ADVCOMBAT_VICTORY_OVERLAY_TICKS_PER_LETTER 4
#define ADVCOMBAT_VICTORY_ZOOM_TICKS 30
#define ADVCBOMAT_VICTORY_SLIDE_TICKS 30
#define ADVCBOMAT_VICTORY_AWARD_TICKS 45
#define ADVCBOMAT_VICTORY_DOCTOR_TICKS 45
#define ADVCBOMAT_VICTORY_HIDE_TICKS 30

#define ADVCOMBAT_VICTORY_MASK_START 1

//--Defeat
#define ADVCOMBAT_DEFEAT_FRAMES_TOTAL 6
#define ADVCOMBAT_DEFEAT_TICKS_PER_LETTER 15
#define ADVCOMBAT_DEFEAT_TICKS_LETTERS (ADVCOMBAT_DEFEAT_TICKS_PER_LETTER * 6)
#define ADVCOMBAT_DEFEAT_TICKS_HOLD 45
#define ADVCOMBAT_DEFEAT_TICKS_FADEOUT 45

//--Retreat
#define ADVCOMBAT_RETREAT_TICKS 30

//--Positions
#define ADVCOMBAT_POSITION_OFFSCREEN_LFT -100.0f
#define ADVCOMBAT_POSITION_INTRO_OFFSCREEN (VIRTUAL_CANVAS_X + 10.0f)
#define ADVCOMBAT_POSITION_STD_Y 670.0f
#define ADVCOMBAT_POSITION_ABILITY_X 269.0f
#define ADVCOMBAT_POSITION_ABILITY_Y 526.0f
#define ADVCOMBAT_POSITION_ABILITY_W 54.0f
#define ADVCOMBAT_POSITION_ABILITY_H 70.0f

//--Responses
#define ADVCOMBAT_RESPONSE_TYPE_ENTITY 0
#define ADVCOMBAT_RESPONSE_TYPE_AI 1
#define ADVCOMBAT_RESPONSE_TYPE_JOB 2
#define ADVCOMBAT_RESPONSE_TYPE_ABILITY 3
#define ADVCOMBAT_RESPONSE_TYPE_EFFECT 4
#define ADVCOMBAT_RESPONSE_TYPE_TOTAL 5

#define ADVCOMBAT_RESPONSE_BEGINCOMBAT 0
#define ADVCOMBAT_RESPONSE_BEGINTURN 1
#define ADVCOMBAT_RESPONSE_BEGINACTION 2
#define ADVCOMBAT_RESPONSE_BEGINFREEACTION 3
#define ADVCOMBAT_RESPONSE_ENDACTION 4
#define ADVCOMBAT_RESPONSE_ENDTURN 5
#define ADVCOMBAT_RESPONSE_ENDCOMBAT 6
#define ADVCOMBAT_RESPONSE_QUEUEEVENT 7
#define ADVCOMBAT_RESPONSE_AIKNOCKOUT 8
#define ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETS 9
#define ADVCOMBAT_RESPONSE_ABILITY_PAINTTARGETSRESPONSE 10
#define ADVCOMBAT_RESPONSE_ABILITY_EXECUTE 11
#define ADVCOMBAT_RESPONSE_ABILITY_GUIAPPLYEFFECT 12
#define ADVCOMBAT_RESPONSE_ABILITY_BUILDPREDICTIONBOX 13
#define ADVCOMBAT_RESPONSE_ABILITY_APPLYSTATS 14
#define ADVCOMBAT_RESPONSE_ABILITY_UNAPPLYSTATS 15
#define ADVCOMBAT_RESPONSE_APPLICATION_PREAPPLY 16
#define ADVCOMBAT_RESPONSE_APPLICATION_POSTAPPLY 17
#define ADVCOMBAT_RESPONSE_TOTAL 18

//--Combat Inspector Timer
#define ADVCOMBAT_INSPECTOR_TICKS 25
#define ADVCOMBAT_INSPECTOR_HIGHLIGHT_TICKS 10

//--Combat Inspector Modes
#define ADVCOMBAT_INSPECTOR_CHARSELECT 0
#define ADVCOMBAT_INSPECTOR_EFFECTS 1
#define ADVCOMBAT_INSPECTOR_ABILITIES 2

//--Field Abilities
#define ADVCOMBAT_FIELD_ABILITY_SLOTS 5

//--Catalyst
#define ADVCOMBAT_CATALYST_MAX 6

//--[Forward Declarations]
struct TargetCluster;
struct CombatEventPack;
struct ApplicationPack;

//--[Function Pointers]
typedef void(*ConfirmationFnPtr)();

//--[Classes]
class AdvCombat : public RootObject
{
    private:
    //--System
    bool mCombatReinitialized;
    bool mIsActive;
    bool mWasLastDefeatSurrender;
    bool mAIHandledAction;
    bool mPlayerGainedInitiative;
    char *mDoctorResolvePath;
    int mDoctorResolveValue;

    //--Field Abilities
    SugarLinkedList *mExtraScripts; //ExtraScriptPack *, master
    FieldAbility *rActiveFieldAbilities[ADVCOMBAT_FIELD_ABILITY_SLOTS];

    //--Introduction
    bool mIsIntroduction;
    int mIntroTimer;

    //--Title
    bool mShowTitle;
    int mTitleTicks;
    int mTitleTicksMax;
    char *mTitleText;

    //--Turn State
    bool mIsTurnActive;
    int mCurrentTurn;
    int mTurnDisplayTimer;
    int mTurnBarMoveTimer;
    int mTurnBarMoveTimerMax;
    int mTurnWidStart;
    int mTurnWidCurrent;
    int mTurnWidTarget;
    SugarLinkedList *mrTurnOrder; //AdvCombatEntity *, ref
    SugarBitmap *rTurnDiscardImg;

    //--Action Handling
    bool mNoEventsEndsAction;
    bool mWasLastActionFree;
    bool mActionFirstTick;
    bool mIsActionOccurring;
    bool mIsActionConcluding;
    bool mIsPerformingPlayerUpdate;
    int mPlayerInterfaceTimer;
    int mAbilitySelectionX;
    int mAbilitySelectionY;
    EasingPack2D mAbilityHighlightPack;
    bool mEventFirstTick;
    bool mEventCanRun;
    bool mEntitiesHaveRepositioned;
    int mEventTimer;
    CombatEventPack *rEventZeroPackage;
    CombatEventPack *rFiringEventPackage;
    SugarLinkedList *mEventQueue; //CombatEventPack *, master
    int mTextScatterCounter;
    SugarLinkedList *mApplicationQueue; //ApplicationPack *, master

    //--Second Page
    bool mShowingSecondPage;
    int mSecondPageTimer;
    int mStoredAbilitySelectionX;
    int mStoredAbilitySelectionY;
    StarlightString *mPageLftString;
    StarlightString *mPageRgtString;

    //--Target Selection
    bool mLockTargetClusters;
    int mTargetClusterCounter;
    bool mIsSelectingTargets;
    int mTargetClusterCursor;
    int mTargetFlashTimer;
    char *mLastUsedTargetType;
    SugarLinkedList *mTargetClusters; //TargetCluster *, master

    //--Prediction Boxes
    bool mIsClearingPredictionBoxes;
    TargetCluster *rPredictionCluster;
    SugarLinkedList *mPredictionBoxes; //AdvCombatPrediction *, master

    //--Confirmation Window
    bool mIsShowingConfirmationWindow;
    int mConfirmationTimer;
    StarlightString *mConfirmationString;
    StarlightString *mAcceptString;
    StarlightString *mCancelString;
    ConfirmationFnPtr rConfirmationFunction;

    //--Combat Inspector
    bool mIsShowingCombatInspector;
    int mCombatInspectorMode;
    int mCombatInspectorTimer;
    int mCombatInspectorCharacterCursor;
    int mCombatInspectorEffectCursor;
    int mCombatInspectorAbilityCursorX;
    int mCombatInspectorAbilityCursorY;
    EasingPack2D mInspectorHighlightPos;
    EasingPack2D mInspectorHighlightWid;
    int mCombatInspectorPopulateSlots[CI_STATS_SLOT_TOTAL];
    StarlightString *mCombatInspectorSwitchToAbilitiesString;
    StarlightString *mCombatInspectorSwitchToEffects;

    //--Ability Execution
    int mSkillCatalystExtension;
    ApplicationPack *rCurrentApplicationPack; //Populated for ACE_AI_SCRIPT_CODE_APPLICATION_START and ACE_AI_SCRIPT_CODE_APPLICATION_END
    AdvCombatAbility *rActiveAbility;
    TargetCluster *rActiveTargetCluster;

    //--System Abilities
    AdvCombatAbility *mSysAbilityPassTurn;
    AdvCombatAbility *mSysAbilityRetreat;
    AdvCombatAbility *mSysAbilitySurrender;

    //--Animation Storage
    SugarLinkedList *mCombatAnimations; //AdvCombatAnimation *, master
    SugarLinkedList *mTextPackages; //CombatTextPack *, master

    //--Option Flags
    bool mAutoUseDoctorBag;
    bool mTouristMode;
    bool mMemoryCursor;
    bool mRestoreMainWeaponOnCombatEnd;

    //--Combat State Flags
    bool mIsUnwinnable;
    bool mIsUnloseable;
    bool mIsUnretreatable;
    bool mIsUnsurrenderable;
    int mResolutionTimer;
    int mResolutionState;
    int mCombatResolution;
    int mResolutionDoctorStart;
    int mResolutionDoctorEnd;

    //--Enemy Storage
    int mUniqueEnemyIDCounter;
    AliasStorage *mEnemyAliases;
    AdvCombatEntity *rKnockoutEntity;
    SugarLinkedList *mEnemyRoster; //AdvCombatEntity *, master
    SugarLinkedList *mrEnemyCombatParty; //AdvCombatEntity *, ref
    SugarLinkedList *mrEnemyReinforcements; //AdvCombatEntity *, ref
    SugarLinkedList *mrEnemyGraveyard; //AdvCombatEntity *, ref
    SugarLinkedList *mWorldReferences; //WorldRefPack *, master

    //--Party Storage
    SugarLinkedList *mPartyRoster; //AdvCombatEntity *, master
    SugarLinkedList *mrActiveParty; //AdvCombatEntity *, ref
    SugarLinkedList *mrCombatParty; //AdvCombatEntity *, ref

    //--Stat Storage for Level Ups
    CombatStatistics mJobTempStatistics;

    //--Victory Storage
    int mXPToAward;
    int mJPToAward;
    int mPlatinaToAward;
    int mDoctorToAward;
    int mXPBonus;
    int mJPBonus;
    int mPlatinaBonus;
    int mDoctorBonus;
    int mXPToAwardStorage;
    int mJPToAwardStorage;
    int mPlatinaToAwardStorage;
    int mDoctorToAwardStorage;
    SugarLinkedList *mItemsToAwardList; //char *, master
    SugarLinkedList *mItemsAwardedList; //AdvVictoryItemPack *, ref

    //--Effect Storage
    bool mMustRebuildEffectReferences;
    StarlightString *mSpecialShieldString;
    StarlightString *mSpecialAdrenalineString;
    SugarLinkedList *mGlobalEffectList; //AdvCombatEffect *, master
    SugarLinkedList *mEffectGraveyard; //AdvCombatEffect *, master
    SugarLinkedList *mrTempEffectList; //AdvCombatEffect *, reference

    //--Response Codes
    int cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_TOTAL][ADVCOMBAT_RESPONSE_TOTAL];

    //--Cutscene Handlers
    char *mStandardRetreatScript;
    char *mStandardDefeatScript;
    char *mStandardVictoryScript;
    char *mCurrentRetreatScript;
    char *mCurrentDefeatScript;
    char *mCurrentVictoryScript;

    //--Audio
    char *mDefaultCombatMusic;
    float mCombatMusicStart;
    char *mNextCombatMusic;
    float mNextCombatMusicStart;
    int mEndCombatTracksTotal;
    float *mEndCombatMusicResume;
    bool mQuietBackgroundMusicForVictory;
    AudioPackage *rPreviousMusicWhenReplaying;
    float mPreviousTimeWhenReplaying;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            SugarFont *rVictoryFontHeader;
            SugarFont *rVictoryFontExp;
            SugarFont *rVictoryFontPlatina;
            SugarFont *rVictoryFontDoctor;
            SugarFont *rVictoryFontItem;
            SugarFont *rAllyHPFont;
            SugarFont *rPortraitHPFont;
            SugarFont *rActiveCharacterNameFont;
            SugarFont *rNewTurnFont;
            SugarFont *rAbilityHeaderFont;
            SugarFont *rAbilityDescriptionFont;
            SugarFont *rConfirmationFontBig;
            SugarFont *rConfirmationFontSmall;
            SugarFont *rTargetClusterFont;
            SugarFont *rCombatTextFont;
            SugarFont *rAbilityTitleFont;
            SugarFont *rEffectFont;
            SugarFont *rPredictionFont;
            SugarFont *rInspectorHeading;
            SugarFont *rInspectorNameFont;
            SugarFont *rInspectorEffectFont;

            //--Ally Bar
            SugarBitmap *rAllyFrame;
            SugarBitmap *rAllyPortraitMask;

            //--Defeat Overlay
            SugarBitmap *rDefeatFrames[ADVCOMBAT_DEFEAT_FRAMES_TOTAL];

            //--Enemy Health Bar
            SugarBitmap *rEnemyHealthBarEdge;
            SugarBitmap *rEnemyHealthBarFill;
            SugarBitmap *rEnemyHealthBarFrame;
            SugarBitmap *rEnemyHealthBarStun;
            SugarBitmap *rEnemyHealthBarStunMarker;
            SugarBitmap *rEnemyHealthBarUnder;

            //--Combat Inspector
            SugarBitmap *rInspectorFrames;
            SugarBitmap *rInspectorNameBar;
            SugarBitmap *rInspectorClock;
            SugarBitmap *rInspectorStatusIcons[CI_STATS_SLOT_TOTAL];
            SugarBitmap *rInspectorResistanceIcons[CI_RESIST_SLOT_TOTAL];
            SugarBitmap *rInspectorHealth;
            SugarBitmap *rInspectorAdrenaline;
            SugarBitmap *rInspectorShields;

            //--Player Interface
            SugarBitmap *rAbilityFrameCatalyst[ADVCOMBAT_CATALYST_MAX];
            SugarBitmap *rAbilityFrameCustom;
            SugarBitmap *rAbilityFrameMain;
            SugarBitmap *rAbilityFrameSecondary;
            SugarBitmap *rAbilityHighlight;
            SugarBitmap *rCPPip;
            SugarBitmap *rDescriptionWindow;
            SugarBitmap *rHealthBarAdrenaline;
            SugarBitmap *rHealthBarHPFill;
            SugarBitmap *rHealthBarMPFill;
            SugarBitmap *rHealthBarMix;
            SugarBitmap *rHealthBarShield;
            SugarBitmap *rHealthBarUnderlay;
            SugarBitmap *rMainHealthBarFrame;
            SugarBitmap *rMainNameBanner;
            SugarBitmap *rMainPortraitMask;
            SugarBitmap *rMainPortraitRing;
            SugarBitmap *rMainPortraitRingBack;
            SugarBitmap *rPredictionBorderCard;
            SugarBitmap *rTargetArrowLft;
            SugarBitmap *rTargetArrowRgt;
            SugarBitmap *rTargetBox;
            SugarBitmap *rTitleBox;
            SugarBitmap *rCooldownClock;
            SugarBitmap *rCooldownNums[10];

            //--Turn Order
            SugarBitmap *rTurnOrderBack;
            SugarBitmap *rTurnOrderCircle;
            SugarBitmap *rTurnOrderEdge;

            //--Victory
            SugarBitmap *rVictoryBannerBack;
            SugarBitmap *rVictoryBannerBig;
            SugarBitmap *rVictoryDoctorBack;
            SugarBitmap *rVictoryDoctorFill;
            SugarBitmap *rVictoryDoctorFrame;
            SugarBitmap *rVictoryDropFrame;
            SugarBitmap *rVictoryExpFrameBack;
            SugarBitmap *rVictoryExpFrameFill;
            SugarBitmap *rVictoryExpFrameFront;
            SugarBitmap *rVictoryExpFrameMask;
        }Data;
    }Images;

    protected:

    public:
    ///--System
    AdvCombat();
    virtual ~AdvCombat();

    //--Public Variables
    bool mIsRunningEventQuery;
    static bool xIsAISpawningEvents;

    //--Property Queries
    bool IsActive();
    bool IsStoppingWorldUpdate();
    bool DoesPlayerHaveInitiative();
    bool IsAnythingAnimating();
    bool IsAnyoneMoving();
    int GetSkillCatalystSlots();
    int GetPartyGroupingID(uint32_t pUniqueID);
    FieldAbility *GetFieldAbility(int pSlot);
    bool IsAbilityUnlocked(const char *pCharacter, const char *pJob, const char *pAbility);
    bool IsUnretreatable();
    bool IsUnsurrenderable();
    bool IsUnloseable();

    //--Manipulators
    void Activate();
    void Deactivate();
    void SetDefaultCombatMusic(const char *pMusicName, float pStartPoint);
    void SetNextCombatMusic(const char *pMusicName, float pStartPoint);
    void SetUnloseable(bool pFlag);
    void SetUnwinnable(bool pFlag);
    void SetUnretreatable(bool pFlag);
    void SetUnsurrenderable(bool pFlag);
    void SpawnTitle(int pTicks, const char *pText);
    void MarkEventCanRun(bool pFlag);
    void ResumeMusic();
    void SetFieldAbility(int pSlot, FieldAbility *pAbility);
    void RegisterExtraScriptPack(ExtraScriptPack *pPackage);
    void SetSkillCatalystSlots(int pSlots);

    //--Core Methods
    void HealFromDoctorBag(int pPartyIndex);
    bool IsEntityInvolvedInEvent(AdvCombatEntity *pEntity, CombatEventPack *pEvent);
    bool CheckKnockouts();
    void CleanDataLibrary();

    ///--AIs
    void MarkHandledAction();
    void SetAbilityAsActive(int pSlot);
    void RunActiveAbilityTargetScript();
    void SetTargetClusterAsActive(int pSlot);

    ///--Animations
    void RegisterAnimation(const char *pName, AdvCombatAnimation *pAnimation);
    void CreateAnimationInstance(const char *pAnimationName, const char *pRefName, int pStartingTicks, float pX, float pY);
    AdvCombatAnimation *GetAnimation(const char *pName);

    ///--Applications
    void ScatterPosition(int &sIndex, float &sXPos, float &sYPos);
    void HandleApplication(uint32_t pOriginatorID, uint32_t pTargetID, const char *pString);

    ///--Confirmation
    void ActivateConfirmationWindow();
    void DeactivateConfirmationWindow();
    StarlightString *GetConfirmationString();
    void SetConfirmationText(const char *pString);
    void SetConfirmationFunc(ConfirmationFnPtr pFuncPtr);
    static void ConfirmExecuteAbility();
    void UpdateConfirmation();
    void RenderConfirmation();

    ///--Construction
    void Initialize();
    void Reinitialize();
    void Construct();
    void Disassemble();

    ///--Debug
    void Debug_RestoreParty();
    void Debug_AwardXP(int pAmount);
    void Debug_AwardJP(int pAmount);
    void Debug_AwardPlatina(int pAmount);
    void Debug_AwardCrafting();

    ///--Effects
    void RegisterEffect(AdvCombatEffect *pEffect);
    void RebuildEffectReferences();
    SugarLinkedList *GetEffectsReferencing(AdvCombatEntity *pEntity);
    void StoreEffectsReferencing(uint32_t pID);
    SugarLinkedList *GetTemporaryEffectList();
    void ClearTemporaryEffectList();
    void MarkEffectForRemoval(uint32_t pID);
    void PulseEffectsForRemoval();

    ///--Enemy Handlers
    int GetNextEnemyID();
    void RegisterEnemy(const char *pUniqueName, AdvCombatEntity *pEntity, int pReinforcementTurns);

    ///--Inspector
    void ActivateInspector();
    void DeactivateInspector();
    void CalculateHighlightPos(int pMode, int pIndex, int pTicks);
    void UpdateInspector();
    void RenderInspector();
    void RenderInspectorUpper(SugarLinkedList *pList);
    void RenderInspectorLower(SugarLinkedList *pList);

    ///--Introduction
    void UpdateIntroduction();
    void RenderIntroduction();

    ///--Options
    bool IsAutoUseDoctorBag();
    bool IsTouristMode();
    bool IsMemoryCursor();
    void SetAutoUseDoctorBag(bool pFlag);
    void SetTouristMode(bool pFlag);
    void SetMemoryCursor(bool pFlag);

    ///--Paths
    void SetStandardRetreatScript(const char *pPath);
    void SetStandardDefeatScript(const char *pPath);
    void SetStandardVictoryScript(const char *pPath);
    void SetRetreatScript(const char *pPath);
    void SetDefeatScript(const char *pPath);
    void SetVictoryScript(const char *pPath);
    void SetDoctorResolvePath(const char *pPath);

    ///--Player Input
    void MovePlayerHighlightTo(int pX, int pY);
    void HandlePlayerControls();
    void ExecuteAbility(AdvCombatAbility *pAbility, TargetCluster *pTargetCluster);
    void ExecuteActiveAbility();

    ///--Positioning
    float ComputeIdealX(bool pIsLeftSide, int pPosition, int pTotalEntries);
    float ComputeOffscreenX(bool pIsLeftSide, SugarBitmap *pImage);
    void PositionCharactersOnScreenForEvent(CombatEventPack *pEvent);
    void PositionCharactersNormally();
    void PositionCharactersOffScreen();
    void PositionEnemies();
    void PositionListOnscreen(bool pIsLeftSide, int pTicks, SugarLinkedList *pList);
    void PositionListOffscreen(bool pIsLeftSide, int pTicks, SugarLinkedList *pList);

    ///--Prediction Boxes
    void CreatePredictionBox(const char *pBoxName);
    void AllocatePredictionBoxStrings(const char *pBoxName, int pStringsTotal);
    void SetPredictionBoxOffsets(const char *pBoxName, float pXOffset, float pYOffset);
    void SetPredictionBoxHostI(const char *pBoxName, uint32_t pHostID);
    void SetPredictionBoxStringText(const char *pBoxName, int pStringIndex, const char *pText);
    void SetPredictionBoxStringImagesTotal(const char *pBoxName, int pStringIndex, int pImagesTotal);
    void SetPredictionBoxStringImage(const char *pBoxName, int pStringIndex, int pSlot, float pYOffset, const char *pPath);
    void BuildPredictionBoxes();
    void RunPredictionBoxCalculations();
    void SetClusterAsActive(int pIndex);
    void ClearPredictionBoxes();
    void ClearPredictionBoxesNow();
    void UpdatePredictionBoxes();
    void RenderPredictionBoxes();
    TargetCluster *GetPredictionTargetCluster();

    ///--Reinforcements
    void RegisterWorldReference(TilemapActor *pActor, int pReinforcementTurns);

    ///--Resolution
    void AwardXP(int pAmount);
    void AwardJP(int pAmount);
    void AwardPlatina(int pAmount);
    void AwardDoctorBag(int pAmount);
    bool AwardItems(int pStart, int pEnd);
    void SetDoctorResolvedValue(int pValue);
    void RunCombatEndScripts();
    int GetVictoryXP();
    int GetVictoryPlatina();
    int GetVictoryJP();
    int GetVictoryDoctor();
    int GetBonusXP();
    int GetBonusPlatina();
    int GetBonusJP();
    int GetBonusDoctor();
    bool IsPlayerDefeated();
    bool IsEnemyDefeated();
    void SetBonusXP(int pAmount);
    void SetBonusPlatina(int pAmount);
    void SetBonusJP(int pAmount);
    void SetBonusDoctor(int pAmount);
    void BeginResolutionSequence(int pSequence);
    void UpdateResolution();
    void RenderResolution();
    void RenderVictory();
    void RenderVictoryBanner(float pPercent);
    void RenderVictoryDrops(float pPercent);
    void RenderVictoryDoctor(float pPercent);
    void RenderExpBars(float pPercent);
    void RenderDefeat();
    void RenderRetreat();

    ///--Responses
    void RunAllResponseScripts(int pCode);
    void RunPartyResponseScripts(int pCode);
    void RunPartyAIScripts(int pCode);
    void RunPartyJobScripts(int pCode);
    void RunPartyAbilityScripts(int pCode);
    void RunEnemyResponseScripts(int pCode);
    void RunEnemyAIScripts(int pCode);
    void RunEnemyJobScripts(int pCode);
    void RunEnemyAbilityScripts(int pCode);
    void RunEffects(int pCode);

    ///--Roster Handling
    int GetRosterCount();
    int GetActivePartyCount();
    int GetCombatPartyCount();
    bool IsEntityInPlayerParty(void *pPtr);
    bool DoesPartyMemberExist(const char *pInternalName);
    bool IsPartyMemberActive(const char *pInternalName);
    void RegisterPartyMember(const char *pInternalName, AdvCombatEntity *pEntity);
    void PushPartyMemberS(const char *pInternalName);
    void PushPartyMemberI(int pSlot);
    void PushActivePartyMemberS(const char *pInternalName);
    void PushActivePartyMemberI(int pSlot);
    void PushCombatPartyMemberS(const char *pInternalName);
    void PushCombatPartyMemberI(int pSlot);
    void SetPartyBySlot(int pSlot, const char *pPartyMemberName);
    void ClearParty();
    void FullRestoreParty();
    void FullRestoreRoster();
    void MoveEntityToPartyGroup(uint32_t pID, int pPartyCode);

    ///--Save/Load
    void WriteLoadInfo(SugarAutoBuffer *pBuffer);

    ///--Targeting
    //--System
    void ClearTargetClusters();
    void MovePartyByActiveCluster();

    //--Property Queries
    const char *GetLastTargetCode();
    int GetTargetClusterTotal();
    TargetCluster *GetTargetClusterI(int pSlot);
    TargetCluster *GetTargetClusterS(const char *pClusterName);
    AdvCombatEntity *GetEntityInCluster(const char *pClusterName, int pSlot);
    TargetCluster *GetActiveCluster();

    //--Manipulators
    void ResetTargetCode();
    TargetCluster *CreateTargetCluster(const char *pClusterName, const char *pDisplayName);
    void RemoveTargetClusterI(int pIndex);
    void RegisterElementToCluster(const char *pClusterName, void *pElement);
    void RegisterEntityToClusterByID(const char *pClusterName, uint32_t pUniqueID);
    void RemoveEntityFromClusterI(const char *pClusterName, int pSlot);
    void RemoveEntityFromClusterP(const char *pClusterName, void *pPtr);

    //--Addition Macros
    void AddPartyTargetToClusterI(const char *pClusterName, int pSlot);
    void AddPartyTargetToClusterS(const char *pClusterName, const char *pPartyMemberName);
    void AddEnemyTargetToClusterI(const char *pClusterName, int pSlot);
    void AddEnemyTargetToClusterS(const char *pClusterName, const char *pEnemyName);

    //--Autobuild Macros
    void PopulateTargetsByCode(const char *pCodeName, void *pCaller);

    ///--Turn Order
    int GetTurn();
    void GivePlayerInitiative();
    void RemovePlayerInitiative();
    void RollTurnOrder();
    static int SortByInitiative(const void *pEntryA, const void *pEntryB);
    void RecomputeTurnBarLength();
    void BeginTurnDisplay();
    void BeginTurn();
    void BeginAction();
    void SetEventTimer(int pTicks);
    void SetAsFreeAction();
    void EnqueueEvent(CombatEventPack *pPack);
    void EnqueueApplicationPack(ApplicationPack *pPack);
    void CompleteAction();
    void EndTurn();

    ///--World
    void PulseWorld(bool pDontCheckEnemies);

    private:
    //--Private Core Methods
    public:
    ///--Update
    void Update();

    //--File I/O

    ///--Drawing
    void Render();
    void RenderEntity(AdvCombatEntity *pEntity);
    void RenderEntityUI(AdvCombatEntity *pEntity);
    void RenderAllyBars();
    void RenderTurnOrder();
    void RenderPlayerBar();
    void RenderNewTurn();
    void RenderTargetCluster();
    void RenderTitle();

    //--Pointer Routing
    SugarLinkedList *GetEnemyPartyList();
    CombatEventPack *GetFiringEventPack();
    CombatEventPack *GetEventQueryPack();
    AdvCombatEntity *GetEntityByID(uint32_t pUniqueID);
    AdvCombatEntity *GetRosterMemberI(int pIndex);
    AdvCombatEntity *GetRosterMemberS(const char *pName);
    AdvCombatEntity *GetActiveMemberI(int pIndex);
    AdvCombatEntity *GetActiveMemberS(const char *pName);
    AdvCombatEntity *GetCombatMemberI(int pIndex);
    AdvCombatEntity *GetCombatMemberS(const char *pName);
    AdvCombatEntity *GetKnockoutEntity();
    AliasStorage *GetEnemyAliasStorage();
    AdvCombatEntity *GetActingEntity();
    AdvCombatEntity *GetEventOriginator();
    AdvCombatAbility *GetSystemPassTurn();
    AdvCombatAbility *GetSystemRetreat();
    AdvCombatAbility *GetSystemSurrender();
    ApplicationPack *GetApplicationPack();
    CombatStatistics *GetJobLevelUpStorage();

    //--Static Functions
    static AdvCombat *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombat_GetProperty(lua_State *L);
int Hook_AdvCombat_SetProperty(lua_State *L);
