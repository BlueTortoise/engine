//--Base
#include "AdvCombatEffect.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

//=========================================== System ==============================================
AdvCombatEffect::AdvCombatEffect()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATEFFECT;

    //--[AdvCombatEffect]
    //--System
    mLocalName = InitializeString("Effect");
    mDisplayName = InitializeString("Effect");
    mRemoveNow = false;
    mDontShowOnUI = false;

    //--Display
    rBackImg = NULL;
    rFrameImg = NULL;
    rFrontImg = NULL;

    //--Script Handler
    mApplicationPriority = 0;
    mScriptPath = NULL;

    //--Targets
    rOriginator = NULL;
    mTargetList = new SugarLinkedList(true);

    //--Tags
    mTagList = new SugarLinkedList(true);
}
AdvCombatEffect::~AdvCombatEffect()
{
    free(mLocalName);
    free(mDisplayName);
    free(mScriptPath);
    delete mTargetList;
    delete mTagList;
}

//================================== AdvCombatEffectTargetPack ====================================
void AdvCombatEffectTargetPack::Initialize()
{
    //--Variables.
    mTargetID = 0;
    rTargetPtr = NULL;
    mDisplayString = new StarlightString();
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;
}
void AdvCombatEffectTargetPack::AllocateDescriptionLines(int pTotal)
{
    for(int i = 0; i < mDescriptionLinesTotal; i ++)
    {
        delete mDescriptionLines[i];
    }
    free(mDescriptionLines);
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;
    if(pTotal < 1) return;
    mDescriptionLinesTotal = pTotal;
    mDescriptionLines = (StarlightString **)starmemoryalloc(sizeof(StarlightString *) * mDescriptionLinesTotal);
    for(int i = 0; i < mDescriptionLinesTotal; i ++)
    {
        mDescriptionLines[i] = new StarlightString();
    }
}
void AdvCombatEffectTargetPack::SetDescriptionText(int pSlot, const char *pText)
{
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return;
    mDescriptionLines[pSlot]->SetString(pText);
}
void AdvCombatEffectTargetPack::AllocateDescriptionImages(int pSlot, int pImageTotal)
{
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return;
    mDescriptionLines[pSlot]->AllocateImages(pImageTotal);
}
void AdvCombatEffectTargetPack::SetDescriptionImage(int pSlot, int pImageIndex, float pYOffset, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return;
    mDescriptionLines[pSlot]->SetImageS(pImageIndex, pYOffset, pDLPath);
}
void AdvCombatEffectTargetPack::CrossloadImages(int pSlot)
{
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return;
    mDescriptionLines[pSlot]->CrossreferenceImages();
}
void AdvCombatEffectTargetPack::DeleteThis(void *pPtr)
{
    AdvCombatEffectTargetPack *rPtr = (AdvCombatEffectTargetPack *)pPtr;
    delete rPtr->mDisplayString;
    for(int i = 0; i < rPtr->mDescriptionLinesTotal; i ++)
    {
        delete rPtr->mDescriptionLines[i];
    }
    free(rPtr->mDescriptionLines);
    free(rPtr);
}

//--[Public Variables]
//====================================== Property Queries =========================================
const char *AdvCombatEffect::GetDisplayName()
{
    return mDisplayName;
}
const char *AdvCombatEffect::GetScriptPath()
{
    return mScriptPath;
}
bool AdvCombatEffect::IsRemovedNow()
{
    return mRemoveNow;
}
bool AdvCombatEffect::IsIDOnTargetList(uint32_t pID)
{
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        if(rPackage->mTargetID == pID)
        {
            mTargetList->PopIterator();
            return true;
        }
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }
    return false;
}
int AdvCombatEffect::GetTagCount(const char *pTag)
{
    int *rCheckPtr = (int *)mTagList->GetElementByName(pTag);
    if(!rCheckPtr) return 0;
    return *rCheckPtr;
}
bool AdvCombatEffect::IsVisibleOnUI()
{
    return !mDontShowOnUI;
}

//========================================= Manipulators ==========================================
void AdvCombatEffect::SetDisplayName(const char *pName)
{
    ResetString(mDisplayName, pName);
}
void AdvCombatEffect::SetOriginatorByID(uint32_t pID)
{
    //--Note: Entity must exist within the AdvCombat class or it will come back NULL. Pass 0 to NULL
    //  off the originator.
    rOriginator = AdvCombat::Fetch()->GetEntityByID(pID);
}
void AdvCombatEffect::SetScriptPath(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}
void AdvCombatEffect::SetRemoveNow(bool pFlag)
{
    mRemoveNow = pFlag;
}
void AdvCombatEffect::SetHideOnUIFlag(bool pFlag)
{
    mDontShowOnUI = pFlag;
}
void AdvCombatEffect::AddTargetByID(uint32_t pID)
{
    AdvCombatEntity *rCheckEntity = AdvCombat::Fetch()->GetEntityByID(pID);
    if(!rCheckEntity) return;

    //--Scan the list to see if an entry already exists with it.
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        if(rPackage->mTargetID == pID)
        {
            mTargetList->PopIterator();
            return;
        }
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }

    //--New package needs to be created.
    SetMemoryData(__FILE__, __LINE__);
    AdvCombatEffectTargetPack *nPackage = (AdvCombatEffectTargetPack *)starmemoryalloc(sizeof(AdvCombatEffectTargetPack));
    nPackage->Initialize();
    nPackage->mTargetID = pID;
    nPackage->rTargetPtr = rCheckEntity;
    mTargetList->AddElement("X", nPackage, &AdvCombatEffectTargetPack::DeleteThis);
}
void AdvCombatEffect::SetFrontImage(const char *pPath)
{
    rFrontImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void AdvCombatEffect::SetFrameImage(const char *pPath)
{
    rFrameImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void AdvCombatEffect::SetBackImage(const char *pPath)
{
    rBackImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void AdvCombatEffect::AddTag(const char *pTag)
{
    //--Tag already exists, increment it instead.
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(rInteger)
    {
        (*rInteger) = (*rInteger) + 1;
    }
    //--Create.
    else
    {
        int *nInteger = (int *)starmemoryalloc(sizeof(int));
        *nInteger = 1;
        mTagList->AddElement(pTag, nInteger, &FreeThis);
    }
}
void AdvCombatEffect::RemoveTag(const char *pTag)
{
    //--Tag doesn't exist, do nothing:
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(!rInteger) return;

    //--Greater than 1.
    if(*rInteger > 1)
    {
        (*rInteger) = (*rInteger) - 1;
    }
    //--Zero it off. Remove it.
    else
    {
        mTagList->RemoveElementS(pTag);
    }
}

//========================================= Core Methods ==========================================
void AdvCombatEffect::ApplyStatsToTargets()
{
    //--Error check.
    if(!mScriptPath) return;

    //--Push this effect on the activity stack. It stays there for all function calls.
    LuaManager *rLuaManager = LuaManager::Fetch();
    DataLibrary::Fetch()->PushActiveEntity(this);

    //--Iterate across the entities.
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        //--Apply to the target.
        rLuaManager->ExecuteLuaFile(mScriptPath, 2, "N", (float)ACEFF_SCRIPT_CODE_APPLYSTATS, "N", (float)rPackage->mTargetID);

        //--Order them to add their stats back up.
        if(rPackage->rTargetPtr) rPackage->rTargetPtr->ComputeStatistics();

        //--Next.
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }

    //--Pop activity stack.
    DataLibrary::Fetch()->PopActiveEntity();
}
void AdvCombatEffect::UnApplyStatsToTargets()
{
    //--Error check.
    if(!mScriptPath) return;

    //--Push this effect on the activity stack. It stays there for all function calls.
    LuaManager *rLuaManager = LuaManager::Fetch();
    DataLibrary::Fetch()->PushActiveEntity(this);

    //--Iterate across the entities.
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        //--Un-apply.
        rLuaManager->ExecuteLuaFile(mScriptPath, 2, "N", (float)ACEFF_SCRIPT_CODE_UNAPPLYSTATS, "N", (float)rPackage->mTargetID);

        //--Order them to add their stats back up.
        if(rPackage->rTargetPtr) rPackage->rTargetPtr->ComputeStatistics();

        //--Next.
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }

    //--Pop activity stack.
    DataLibrary::Fetch()->PopActiveEntity();
}
void AdvCombatEffect::ApplyStatsToTarget(uint32_t pTargetID)
{
    //--Runs the stat-modify algorithm only on the target provided. If the target is not on the target list,
    //  does nothing.
    LuaManager *rLuaManager = LuaManager::Fetch();
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        if(rPackage->mTargetID == pTargetID)
        {
            DataLibrary::Fetch()->PushActiveEntity(this);
            rLuaManager->ExecuteLuaFile(mScriptPath, 2, "N", (float)ACEFF_SCRIPT_CODE_APPLYSTATS, "N", (float)rPackage->mTargetID);
            DataLibrary::Fetch()->PopActiveEntity();
            mTargetList->PopIterator();
            break;
        }
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }
}
void AdvCombatEffect::UnApplyStatsToTarget(uint32_t pTargetID)
{
    //--Runs the stat-modify algorithm only on the target provided. If the target is not on the target list,
    //  does nothing.
    LuaManager *rLuaManager = LuaManager::Fetch();
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        if(rPackage->mTargetID == pTargetID)
        {
            DataLibrary::Fetch()->PushActiveEntity(this);
            rLuaManager->ExecuteLuaFile(mScriptPath, 2, "N", (float)ACEFF_SCRIPT_CODE_UNAPPLYSTATS, "N", (float)rPackage->mTargetID);
            DataLibrary::Fetch()->PopActiveEntity();
            mTargetList->PopIterator();
            break;
        }
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
SugarBitmap *AdvCombatEffect::GetBackImage()
{
    return rBackImg;
}
SugarBitmap *AdvCombatEffect::GetFrameImage()
{
    return rFrameImg;
}
SugarBitmap *AdvCombatEffect::GetFrontImage()
{
    return rFrontImg;
}
AdvCombatEntity *AdvCombatEffect::GetOriginator()
{
    return rOriginator;
}
SugarLinkedList *AdvCombatEffect::GetTargetList()
{
    return mTargetList;
}
StarlightString *AdvCombatEffect::GetStringByTargetID(uint32_t pTargetID)
{
    //--Returns the StarlightString associated with the given target ID. If the ID is not on the
    //  target list, returns NULL.
    AdvCombatEffectTargetPack *rPackage = GetTargetPackByID(pTargetID);
    if(rPackage) return rPackage->mDisplayString;

    //--Not found.
    return NULL;
}
AdvCombatEffectTargetPack *AdvCombatEffect::GetTargetPackByID(uint32_t pTargetID)
{
    //--Returns the target package associated with the ID, or NULL if not found.
    AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)mTargetList->PushIterator();
    while(rPackage)
    {
        if(rPackage->mTargetID == pTargetID)
        {
            mTargetList->PopIterator();
            return rPackage;
        }
        rPackage = (AdvCombatEffectTargetPack *)mTargetList->AutoIterate();
    }
    return NULL;
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
