//--[AdvCombatEffect]
//--An effect in combat, which can be buffs, debuffs, damage-over-times, or anything a scripter
//  can dream up.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"
#include "StarlightString.h"

//--[Local Structures]
//--Wrapper around an AdvCombatEntity and a StarlightString.
typedef struct AdvCombatEffectTargetPack
{
    //--Members
    uint32_t mTargetID;
    AdvCombatEntity *rTargetPtr;
    StarlightString *mDisplayString;
    int mDescriptionLinesTotal;
    StarlightString **mDescriptionLines;

    //--Functions
    void Initialize();
    void AllocateDescriptionLines(int pTotal);
    void SetDescriptionText(int pSlot, const char *pText);
    void AllocateDescriptionImages(int pSlot, int pImageTotal);
    void SetDescriptionImage(int pSlot, int pImageIndex, float pYOffset, const char *pDLPath);
    void CrossloadImages(int pSlot);
    static void DeleteThis(void *pPtr);
}AdvCombatEffectTargetPack;

//--[Local Definitions]
//--[Classes]
class AdvCombatEffect : public RootObject
{
    private:
    //--System
    char *mLocalName;
    char *mDisplayName;
    bool mRemoveNow;
    bool mDontShowOnUI;

    //--Display
    SugarBitmap *rBackImg;
    SugarBitmap *rFrameImg;
    SugarBitmap *rFrontImg;

    //--Script Handler
    int mApplicationPriority;
    char *mScriptPath;

    //--Targets
    AdvCombatEntity *rOriginator;
    SugarLinkedList *mTargetList; //AdvCombatEffectTargetPack *, master

    //--Tags
    SugarLinkedList *mTagList; //int *, master

    protected:

    public:
    //--System
    AdvCombatEffect();
    virtual ~AdvCombatEffect();

    //--Public Variables
    //--Property Queries
    const char *GetDisplayName();
    const char *GetScriptPath();
    bool IsRemovedNow();
    bool IsIDOnTargetList(uint32_t pID);
    int GetTagCount(const char *pTag);
    bool IsVisibleOnUI();

    //--Manipulators
    void SetDisplayName(const char *pName);
    void SetOriginatorByID(uint32_t pID);
    void SetScriptPath(const char *pPath);
    void SetRemoveNow(bool pFlag);
    void SetHideOnUIFlag(bool pFlag);
    void AddTargetByID(uint32_t pID);
    void SetFrontImage(const char *pPath);
    void SetFrameImage(const char *pPath);
    void SetBackImage(const char *pPath);
    void AddTag(const char *pTag);
    void RemoveTag(const char *pTag);

    //--Core Methods
    void ApplyStatsToTargets();
    void UnApplyStatsToTargets();
    void ApplyStatsToTarget(uint32_t pTargetID);
    void UnApplyStatsToTarget(uint32_t pTargetID);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    SugarBitmap *GetBackImage();
    SugarBitmap *GetFrameImage();
    SugarBitmap *GetFrontImage();
    AdvCombatEntity *GetOriginator();
    SugarLinkedList *GetTargetList();
    StarlightString *GetStringByTargetID(uint32_t pTargetID);
    AdvCombatEffectTargetPack *GetTargetPackByID(uint32_t pTargetID);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombatEffect_GetProperty(lua_State *L);
int Hook_AdvCombatEffect_SetProperty(lua_State *L);

