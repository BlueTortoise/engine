//--Base
#include "AdvCombatEffect.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//========================================= Lua Hooking ===========================================
void AdvCombatEffect::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombatEffect_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatEffect_GetProperty", &Hook_AdvCombatEffect_GetProperty);

    /* AdvCombatEffect_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatEffect_SetProperty", &Hook_AdvCombatEffect_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdvCombatEffect_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatEffect_GetProperty("Is ID on Target List", iUniqueID) (1 Boolean)
    //AdvCombatEffect_GetProperty("Get Total Targets") (1 Integer)
    //AdvCombatEffect_GetProperty("Get ID Of Target", iSlot) (1 Integer)
    //AdvCombatEffect_GetProperty("Get Tag Count", sTag) (1 Integer)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatEffect_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATEFFECT)) return LuaTypeError("AdvCombatEffect_GetProperty");
    AdvCombatEffect *rEffect = (AdvCombatEffect *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--True if the ID is on the target list, false otherwise.
    if(!strcasecmp(rSwitchType, "Is ID on Target List") && tArgs == 2)
    {
        lua_pushboolean(L, rEffect->IsIDOnTargetList(lua_tointeger(L, 2)));
        tReturns = 1;
    }
    //--Gets how many targets there are total.
    else if(!strcasecmp(rSwitchType, "Get Total Targets") && tArgs == 1)
    {
        SugarLinkedList *rTargetList = rEffect->GetTargetList();
        lua_pushinteger(L, rTargetList->GetListSize());
        tReturns = 1;
    }
    //--Gets the ID of the given target.
    else if(!strcasecmp(rSwitchType, "Get ID Of Target") && tArgs == 2)
    {
        SugarLinkedList *rTargetList = rEffect->GetTargetList();
        AdvCombatEffectTargetPack *rTargetPack = (AdvCombatEffectTargetPack *)rTargetList->GetElementBySlot(lua_tointeger(L, 2));
        if(rTargetPack)
        {
            lua_pushinteger(L, rTargetPack->mTargetID);
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--Number of times the given tag is on this effect. Can be zero.
    else if(!strcasecmp(rSwitchType, "Get Tag Count") && tArgs == 2)
    {
        lua_pushinteger(L, rEffect->GetTagCount(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatEffect_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombatEffect_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatEffect_SetProperty("Display Name", sName)
    //AdvCombatEffect_SetProperty("Script", sPath)
    //AdvCombatEffect_SetProperty("Add Target By ID", iTargetID)
    //AdvCombatEffect_SetProperty("Flag Remove Now")
    //AdvCombatEffect_SetProperty("Set Hidden On UI", bFlag)
    //AdvCombatEffect_SetProperty("Push Originator") (Pushes Activity Stack)
    //AdvCombatEffect_SetProperty("Back Image", sDLPath)
    //AdvCombatEffect_SetProperty("Frame Image", sDLPath)
    //AdvCombatEffect_SetProperty("Front Image", sDLPath)

    //--[Tags]
    //AdvCombatEffect_SetProperty("Add Tag", sTagName, iCount)
    //AdvCombatEffect_SetProperty("Remove Tag", sTagName, iCount)

    //--[String]
    //AdvCombatEffect_SetProperty("Short Text", iTargetID, sString)
    //AdvCombatEffect_SetProperty("Allocate Short Text Images", iTargetID, iImagesTotal)
    //AdvCombatEffect_SetProperty("Short Text Image", iTargetID, iIndex, fOffsetY, sDLPath)
    //AdvCombatEffect_SetProperty("Crossload Short Text Images", iTargetID)

    //--[Description]
    //AdvCombatEffect_SetProperty("Allocate Description Strings", iTargetID, iTotal)
    //AdvCombatEffect_SetProperty("Description Text", iTargetID, iSlot, sText)
    //AdvCombatEffect_SetProperty("Allocate Description Images", iTargetID, iSlot, iTotal)
    //AdvCombatEffect_SetProperty("Description Image", iTargetID, iSlot, iIndex, fOffsetY, sDLPath)
    //AdvCombatEffect_SetProperty("Crossload Description Images", iTargetID, iSlot)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatEffect_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATEFFECT)) return LuaTypeError("AdvCombatEffect_SetProperty");
    AdvCombatEffect *rEffect = (AdvCombatEffect *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Name that appears in the UI.
    if(!strcasecmp(rSwitchType, "Display Name") && tArgs == 2)
    {
        rEffect->SetDisplayName(lua_tostring(L, 2));
    }
    //--Script to use for further queries.
    else if(!strcasecmp(rSwitchType, "Script") && tArgs == 2)
    {
        rEffect->SetScriptPath(lua_tostring(L, 2));
    }
    //--Manually adds a new target by ID.
    else if(!strcasecmp(rSwitchType, "Add Target By ID") && tArgs == 2)
    {
        rEffect->AddTargetByID(lua_tointeger(L, 2));
    }
    //--Marks the effect as needing removal once the tick is over. Removed effects go to the graveyard.
    else if(!strcasecmp(rSwitchType, "Flag Remove Now") && tArgs == 1)
    {
        rEffect->SetRemoveNow(true);
    }
    //--If true, the effect will not appear on the UI at all.
    else if(!strcasecmp(rSwitchType, "Set Hidden On UI") && tArgs == 2)
    {
        rEffect->SetHideOnUIFlag(lua_toboolean(L, 2));
    }
    //--Pushes the originator of the effect on the activity stack. Can be NULL.
    else if(!strcasecmp(rSwitchType, "Push Originator") && tArgs == 1)
    {
        AdvCombatEntity *rOriginator = rEffect->GetOriginator();
        DataLibrary::Fetch()->PushActiveEntity(rOriginator);
    }
    //--Back image when used to render on the UI.
    else if(!strcasecmp(rSwitchType, "Back Image") && tArgs == 2)
    {
        rEffect->SetBackImage(lua_tostring(L, 2));
    }
    //--Frame image when used to render on the UI.
    else if(!strcasecmp(rSwitchType, "Frame Image") && tArgs == 2)
    {
        rEffect->SetFrameImage(lua_tostring(L, 2));
    }
    //--Front image when used to render on the UI.
    else if(!strcasecmp(rSwitchType, "Front Image") && tArgs == 2)
    {
        rEffect->SetFrontImage(lua_tostring(L, 2));
    }
    ///--[Tags]
    //--Adds the given tag the given number of times.
    else if(!strcasecmp(rSwitchType, "Add Tag") && tArgs == 3)
    {
        int tTimes = lua_tointeger(L, 3);
        for(int i = 0; i < tTimes; i ++) rEffect->AddTag(lua_tostring(L, 2));
    }
    //--Removes the given tag the given number of times.
    else if(!strcasecmp(rSwitchType, "Remove Tag") && tArgs == 3)
    {
        int tTimes = lua_tointeger(L, 3);
        for(int i = 0; i < tTimes; i ++) rEffect->RemoveTag(lua_tostring(L, 2));
    }
    ///--[String]
    //--String that appears above the HP display of an enemy.
    else if(!strcasecmp(rSwitchType, "Short Text") && tArgs == 3)
    {
        uint32_t tID = (uint32_t)lua_tointeger(L, 2);
        StarlightString *rString = rEffect->GetStringByTargetID(tID);
        if(rString) rString->SetString(lua_tostring(L, 3));
    }
    //--Allocates images for the enemy HP display.
    else if(!strcasecmp(rSwitchType, "Allocate Short Text Images") && tArgs == 3)
    {
        uint32_t tID = (uint32_t)lua_tointeger(L, 2);
        StarlightString *rString = rEffect->GetStringByTargetID(tID);
        if(rString) rString->AllocateImages(lua_tointeger(L, 3));
    }
    //--Sets images for the enemy HP display.
    else if(!strcasecmp(rSwitchType, "Short Text Image") && tArgs == 5)
    {
        uint32_t tID = (uint32_t)lua_tointeger(L, 2);
        StarlightString *rString = rEffect->GetStringByTargetID(tID);
        if(rString) rString->SetImageS(lua_tointeger(L, 3), lua_tonumber(L, 4), lua_tostring(L, 5));
    }
    //--Cross-references images and text in the enemy HP display.
    else if(!strcasecmp(rSwitchType, "Crossload Short Text Images") && tArgs == 2)
    {
        uint32_t tID = (uint32_t)lua_tointeger(L, 2);
        StarlightString *rString = rEffect->GetStringByTargetID(tID);
        if(rString) rString->CrossreferenceImages();
    }
    ///--[Description]
    //--Allocates description strings.
    else if(!strcasecmp(rSwitchType, "Allocate Description Strings") && tArgs == 3)
    {
        AdvCombatEffectTargetPack *rTargetPackage = rEffect->GetTargetPackByID((uint32_t)lua_tointeger(L, 2));
        if(rTargetPackage) rTargetPackage->AllocateDescriptionLines(lua_tointeger(L, 3));
    }
    //--Sets description string.
    else if(!strcasecmp(rSwitchType, "Description Text") && tArgs == 4)
    {
        AdvCombatEffectTargetPack *rTargetPackage = rEffect->GetTargetPackByID((uint32_t)lua_tointeger(L, 2));
        if(rTargetPackage) rTargetPackage->SetDescriptionText(lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Sets how many images are expected on the description line.
    else if(!strcasecmp(rSwitchType, "Allocate Description Images") && tArgs == 4)
    {
        AdvCombatEffectTargetPack *rTargetPackage = rEffect->GetTargetPackByID((uint32_t)lua_tointeger(L, 2));
        if(rTargetPackage) rTargetPackage->AllocateDescriptionImages(lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Sets description image.
    else if(!strcasecmp(rSwitchType, "Description Image") && tArgs == 6)
    {
        AdvCombatEffectTargetPack *rTargetPackage = rEffect->GetTargetPackByID((uint32_t)lua_tointeger(L, 2));
        if(rTargetPackage) rTargetPackage->SetDescriptionImage(lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tonumber(L, 5), lua_tostring(L, 6));
    }
    //--Provides references for description strings.
    else if(!strcasecmp(rSwitchType, "Crossload Description Images") && tArgs == 3)
    {
        AdvCombatEffectTargetPack *rTargetPackage = rEffect->GetTargetPackByID((uint32_t)lua_tointeger(L, 2));
        if(rTargetPackage) rTargetPackage->CrossloadImages(lua_tointeger(L, 3));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatEffect_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
