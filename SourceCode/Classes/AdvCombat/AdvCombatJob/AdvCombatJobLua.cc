//--Base
#include "AdvCombatJob.h"

//--Classes
#include "AdvCombatAbility.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//========================================= Lua Hooking ===========================================
void AdvCombatJob::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombatJob_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatJob_GetProperty", &Hook_AdvCombatJob_GetProperty);

    /* AdvCombatJob_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatJob_SetProperty", &Hook_AdvCombatJob_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdvCombatJob_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatJob_GetProperty("Name") (1 String)
    //AdvCombatJob_GetProperty("Current JP") (1 Integer)
    //AdvCombatJob_GetProperty("Total Abilities") (1 Integer)
    //AdvCombatJob_GetProperty("Ability Name I", iSlot) (1 String)
    //AdvCombatJob_GetProperty("Is Ability Unlocked I", iSlot) (1 Boolean)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatJob_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATJOB)) return LuaTypeError("AdvCombatJob_GetProperty");
    AdvCombatJob *rJob = (AdvCombatJob *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Job's internal name.
    if(!strcasecmp("Name", rSwitchType) && tArgs == 1)
    {
        lua_pushstring(L, rJob->GetInternalName());
        tReturns = 1;
    }
    //--How much JP is available for spending.
    else if(!strcasecmp("Current JP", rSwitchType) && tArgs == 1)
    {
        lua_pushinteger(L, rJob->GetCurrentJP());
        tReturns = 1;
    }
    //--How many abilities this job has. Includes internal and external abilities.
    else if(!strcasecmp("Total Abilities", rSwitchType) && tArgs == 1)
    {
        lua_pushinteger(L, rJob->GetAbilityList()->GetListSize());
        tReturns = 1;
    }
    //--Ability name in the given slot.
    else if(!strcasecmp("Ability Name I", rSwitchType) && tArgs == 2)
    {
        AdvCombatAbility *rAbility = rJob->GetAbility(lua_tointeger(L, 2));
        if(rAbility)
            lua_pushstring(L, rAbility->GetInternalName());
        else
            lua_pushstring(L, "Null");
        tReturns = 1;
    }
    //--Boolean indicating if the ability is unlocked.
    else if(!strcasecmp("Is Ability Unlocked I", rSwitchType) && tArgs == 2)
    {
        AdvCombatAbility *rAbility = rJob->GetAbility(lua_tointeger(L, 2));
        if(rAbility)
            lua_pushboolean(L, rAbility->IsUnlocked());
        else
            lua_pushboolean(L, false);
        tReturns = 1;
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatJob_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombatJob_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatJob_SetProperty("Display Name", sPath)
    //AdvCombatJob_SetProperty("Script Path", sPath)
    //AdvCombatJob_SetProperty("Script Response", iIndex, bFlag)
    //AdvCombatJob_SetProperty("Push Owner") (Pushes Active Entity)
    //AdvCombatJob_SetProperty("Fire Script", iCode)

    //--[Storage]
    //AdvCombatJob_SetProperty("JP Available", iAmount)

    //--[Abilities]
    //AdvCombatJob_SetProperty("Ability Unlocked", sAbilityName, bIsUnlocked)

    //--[Tags]
    //AdvCombatJob_SetProperty("Add Tag", sTagName, iCount)
    //AdvCombatJob_SetProperty("Remove Tag", sTagName, iCount)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatJob_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATJOB)) return LuaTypeError("AdvCombatJob_SetProperty");
    AdvCombatJob *rJob = (AdvCombatJob *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Sets the name that appears in the UI.
    if(!strcasecmp("Display Name", rSwitchType) && tArgs == 2)
    {
        rJob->SetDisplayName(lua_tostring(L, 2));
    }
    //--Sets the path this job calls whenever it needs a script call.
    else if(!strcasecmp("Script Path", rSwitchType) && tArgs == 2)
    {
        rJob->SetScriptPath(lua_tostring(L, 2));
    }
    //--Sets whether the job responds to a given code.
    else if(!strcasecmp("Script Response", rSwitchType) && tArgs == 3)
    {
        rJob->SetScriptResponse(lua_tointeger(L, 2), lua_toboolean(L, 3));
    }
    //--Pushes owner of Job.
    else if(!strcasecmp(rSwitchType, "Push Owner") && tArgs == 1)
    {
        DataLibrary::Fetch()->PushActiveEntity(rJob->GetOwner());
    }
    //--Fires the job's code.
    else if(!strcasecmp(rSwitchType, "Fire Script") && tArgs == 2)
    {
        rJob->CallScript(lua_tointeger(L, 2));
    }
    ///--[Storage]
    //--Amount of JP available right now.
    else if(!strcasecmp(rSwitchType, "JP Available") && tArgs == 2)
    {
        rJob->SetCurrentJP(lua_tointeger(L, 2));
    }
    ///--[Abilities]
    //--Unlocks or seals the given ability.
    else if(!strcasecmp(rSwitchType, "Ability Unlocked") && tArgs == 3)
    {
        SugarLinkedList *rAbilityList = rJob->GetAbilityList();
        AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementByName(lua_tostring(L, 2));
        if(rAbility) rAbility->SetUnlocked(lua_toboolean(L, 3));
    }
    ///--[Tags]
    //--Adds a tag the given number of times.
    else if(!strcasecmp(rSwitchType, "Add Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rJob->AddTag(lua_tostring(L, 2));
    }
    //--Removes a tag the given number of times.
    else if(!strcasecmp(rSwitchType, "Remove Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rJob->RemoveTag(lua_tostring(L, 2));
    }
    ///--[Error case]
    else
    {
        LuaPropertyError("AdvCombatJob_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
