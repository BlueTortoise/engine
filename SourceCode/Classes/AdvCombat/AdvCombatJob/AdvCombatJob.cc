//--Base
#include "AdvCombatJob.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "LuaManager.h"

//=========================================== System ==============================================
AdvCombatJob::AdvCombatJob()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATJOB;

    //--[AdvCombatJob]
    //--System
    mLocalName = InitializeString("Job");
    mDisplayName = InitializeString("Job");
    mScriptPath = NULL;
    rOwner = NULL;
    memset(mScriptResponses, 0, sizeof(mScriptResponses));

    //--Ability Listing
    mCurrentJP = 0;
    mTotalJP = 0;
    mrAbilityList = new SugarLinkedList(false);

    //--Tags
    mTagList = new SugarLinkedList(true);
}
AdvCombatJob::~AdvCombatJob()
{
    free(mLocalName);
    free(mDisplayName);
    free(mScriptPath);
    delete mrAbilityList;
    delete mTagList;
}

//====================================== Property Queries =========================================
const char *AdvCombatJob::GetInternalName()
{
    return mLocalName;
}
const char *AdvCombatJob::GetDisplayName()
{
    return mDisplayName;
}
const char *AdvCombatJob::GetScriptPath()
{
    return mScriptPath;
}
int AdvCombatJob::GetCurrentJP()
{
    return mCurrentJP;
}
int AdvCombatJob::GetTotalJP()
{
    return mTotalJP;
}
int AdvCombatJob::GetTagCount(const char *pTag)
{
    int *rTagPtr = (int *)mTagList->GetElementByName(pTag);
    if(!rTagPtr) return 0;
    return *rTagPtr;
}
bool AdvCombatJob::HasInternalVersionOfAbility(AdvCombatAbility *pAbility)
{
    //--Purchasable abilities are JOBNAME|ABILITYNAME, while internal versions of an ability are
    //  JOBNAMEINTERNAL|ABILITYNAME and are automatically equipped to the ability bar. This function
    //  returns true if an internal version of the ability exists in this job.
    if(!pAbility) return false;
    int tAbilityflag = pAbility->HasInternalVersion();

    //--Already checked and computed this, return cases:
    if(tAbilityflag == ADVCA_HAS_NO_INTERNAL) return false;
    if(tAbilityflag == ADVCA_HAS_INTERNAL) return true;

    //--If the value was not one of the above, we need to resolve the answer now.
    if(!mrAbilityList->IsElementOnList(pAbility) || !rOwner)
    {
        pAbility->SetInternalVersionFlag(ADVCA_HAS_NO_INTERNAL);
        return false;
    }

    //--Get the internal name of the ability.
    const char *rAbilityInternalName = pAbility->GetInternalName();

    //--Locate the bar in the name.
    int tBarSlot = -1;
    int tLen = (int)strlen(rAbilityInternalName);
    for(int i = 0; i < tLen; i ++)
    {
        if(rAbilityInternalName[i] == '|')
        {
            tBarSlot = i;
            break;
        }
    }

    //--No bar, fail.
    if(tBarSlot == -1 || tBarSlot == 0)
    {
        pAbility->SetInternalVersionFlag(ADVCA_HAS_NO_INTERNAL);
        return false;
    }

    //--Create a buffer for the internal version.
    char tBuffer[256];
    strncpy(tBuffer, rAbilityInternalName, sizeof(char) * tBarSlot);
    tBuffer[tBarSlot] = '\0';
    strcat(tBuffer, "Internal");
    strcat(tBuffer, &rAbilityInternalName[tBarSlot]);

    //--See if the owner has the ability in question. If so, it's on the bar.
    if(rOwner->GetAbilityList()->GetElementByName(tBuffer) != NULL)
    {
        pAbility->SetInternalVersionFlag(ADVCA_HAS_INTERNAL);
        return true;
    }

    //--Not on the bar.
    pAbility->SetInternalVersionFlag(ADVCA_HAS_NO_INTERNAL);
    return false;
}

//========================================= Manipulators ==========================================
void AdvCombatJob::SetOwner(AdvCombatEntity *pOwner)
{
    rOwner = pOwner;
}
void AdvCombatJob::SetInternalName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void AdvCombatJob::SetDisplayName(const char *pName)
{
    if(!pName) return;
    ResetString(mDisplayName, pName);
}
void AdvCombatJob::SetScriptPath(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}
void AdvCombatJob::SetScriptResponse(int pIndex, bool pFlag)
{
    if(pIndex < 0 || pIndex >= ADVCJOB_CODE_TOTAL) return;
    mScriptResponses[pIndex] = pFlag;
}
void AdvCombatJob::RegisterAbility(AdvCombatAbility *pAbility)
{
    if(!pAbility) return;
    mrAbilityList->AddElementAsTail(pAbility->GetInternalName(), pAbility);
}
void AdvCombatJob::UnregisterAbility(AdvCombatAbility *pAbility)
{
    while(mrAbilityList->RemoveElementP(pAbility))
    {

    }
}
void AdvCombatJob::SetCurrentJP(int pAmount)
{
    int tOldAmount = mCurrentJP;
    mCurrentJP = pAmount;
    mTotalJP = mTotalJP + mCurrentJP - tOldAmount;
}
void AdvCombatJob::AddTag(const char *pTag)
{
    //--Tag already exists, increment it instead.
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(rInteger)
    {
        (*rInteger) = (*rInteger) + 1;
    }
    //--Create.
    else
    {
        int *nInteger = (int *)starmemoryalloc(sizeof(int));
        *nInteger = 1;
        mTagList->AddElement(pTag, nInteger, &FreeThis);
    }
}
void AdvCombatJob::RemoveTag(const char *pTag)
{
    //--Tag doesn't exist, do nothing:
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(!rInteger) return;

    //--Greater than 1.
    if(*rInteger > 1)
    {
        (*rInteger) = (*rInteger) - 1;
    }
    //--Zero it off. Remove it.
    else
    {
        mTagList->RemoveElementS(pTag);
    }
}

//========================================= Core Methods ==========================================
void AdvCombatJob::AssumeJob(AdvCombatEntity *pCaller)
{
    //--The given caller will assume this job, having their stats and graphics modified as needed.
    //  The caller is placed on the activity stack, but the job can be retrieved with lua commands.
    if(!mScriptPath || !pCaller) return;

    //--Reset the job stats.
    CombatStatistics *rJobStatGroup = pCaller->GetStatisticsGroup(ADVCE_STATS_JOB);
    rJobStatGroup->Zero();

    //--Call the lua script to do the heavy lifting.
    CallScript(ADVCJOB_CODE_SWITCHTO);
}
void AdvCombatJob::CallScript(int pCode)
{
    if(!mScriptPath) return;
    if(pCode < 0 || pCode >= ADVCJOB_CODE_TOTAL) return;
    if(!mScriptResponses[pCode]) return;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)pCode);
}
void AdvCombatJob::CallScriptLevelUp(int pLevel)
{
    //--Used for level-up cases, passes the requested level to the script. Doesn't necessarily need
    //  to be the current level for a character.
    if(!mScriptPath) return;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 2, "N", (float)ADVCJOB_CODE_LEVEL, "N", (float)pLevel);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
AdvCombatEntity *AdvCombatJob::GetOwner()
{
    return rOwner;
}
AdvCombatAbility *AdvCombatJob::GetAbility(int pSlot)
{
    return (AdvCombatAbility *)mrAbilityList->GetElementBySlot(pSlot);
}
SugarLinkedList *AdvCombatJob::GetAbilityList()
{
    return mrAbilityList;
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
