//--Base
#include "AdvCombat.h"

//--Classes
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "AdventureLevel.h"
#include "AdvCombatEntity.h"
#include "TilemapActor.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioPackage.h"
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//#define COMBAT_RESOLUTION_DEBUG
#ifdef COMBAT_RESOLUTION_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//=========================================== System ==============================================
void AdvCombat::AwardXP(int pAmount)
{
    AdvCombatEntity *rPartyMember = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rPartyMember)
    {
        //--Track level.
        int tLevelAtStart = rPartyMember->GetLevel();
        rPartyMember->SetXP(rPartyMember->GetXP() + pAmount);

        //--If the level increased, set this flag.
        int tLevelAtEnd = rPartyMember->GetLevel();
        if(tLevelAtEnd > tLevelAtStart) rPartyMember->mLeveledUpThisBattle = true;

        //--Next.
        rPartyMember = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }
}
void AdvCombat::AwardJP(int pAmount)
{
    AdvCombatEntity *rPartyMember = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rPartyMember)
    {
        rPartyMember->GainJP(pAmount);
        rPartyMember = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }
}
void AdvCombat::AwardPlatina(int pAmount)
{
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    rInventory->SetPlatina(rInventory->GetPlatina() + pAmount);
}
void AdvCombat::AwardDoctorBag(int pAmount)
{
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    rInventory->SetDoctorBagCharges(rInventory->GetDoctorBagCharges() + pAmount);
}
bool AdvCombat::AwardItems(int pStart, int pEnd)
{
    //--Takes elements from mItemsToAwardList and creates packages for them on mItemsAwardedList. This also adds them
    //  to the inventory. If pStart and pEnd are equal, does nothing.
    //--Returns true if it played a sound.
    if(pEnd <= pStart || !AdventureLevel::xItemListPath) return false;

    //--Setup.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Iterate.
    for(int i = pStart + 1; i < pEnd + 1; i ++)
    {
        //--Get the name of the item in question. If it doesn't exist, stop here.
        const char *rItemName = (const char *)mItemsToAwardList->GetElementBySlot(i);
        if(!rItemName) return false;

        //--Item exists, create a package around it. The package is needed for timing purposes.
        AdvVictoryItemPack *nPackage = (AdvVictoryItemPack *)starmemoryalloc(sizeof(AdvVictoryItemPack));
        nPackage->Initialize();
        nPackage->rItemNameRef = rItemName;

        //--Add the item to the inventory.
        LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", rItemName);

        //--Get a copy. We need its image, assuming it exists.
        AdventureItem *rItem = rInventory->GetItem(rItemName);
        if(rItem) nPackage->rItemImg = rItem->GetIconImage();

        //--Register.
        mItemsAwardedList->AddElementAsTail("X", nPackage, &FreeThis);
    }

    //--SFX.
    AudioManager::Fetch()->PlaySound("World|TakeItem");
    return true;
}
void AdvCombat::SetDoctorResolvedValue(int pValue)
{
    mDoctorResolveValue = pValue;
}

//====================================== Property Queries =========================================
int AdvCombat::GetVictoryXP()
{
    return mXPToAward;
}
int AdvCombat::GetVictoryPlatina()
{
    return mPlatinaToAward;
}
int AdvCombat::GetVictoryJP()
{
    return mJPToAward;
}
int AdvCombat::GetVictoryDoctor()
{
    return mDoctorToAward;
}
int AdvCombat::GetBonusXP()
{
    return mXPBonus;
}
int AdvCombat::GetBonusPlatina()
{
    return mPlatinaBonus;
}
int AdvCombat::GetBonusJP()
{
    return mJPBonus;
}
int AdvCombat::GetBonusDoctor()
{
    return mDoctorBonus;
}
bool AdvCombat::IsPlayerDefeated()
{
    //--If all the player's party members are KO'd or otherwise unable to act, the player loses.
    //  Note that the IsDefeated() call is not just a KO - permanent stun or stasis also counts.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        if(!rEntity->IsDefeated())
        {
            mrCombatParty->PopIterator();
            return false;
        }
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--If we got this far, none of the entities could act. Player loses.
    return true;
}
bool AdvCombat::IsEnemyDefeated()
{
    //--If all the enemy party membrs are KO'd or otherwise unable to act, the player wins.
    //  Unlike the player party, defeated enemies are placed on the graveyard. Therefore, if
    //  the enemy list has no members, the party wins.
    //--Note that reinforcements are *not* checked. Defeating all active enemies before reinforcements
    //  arrive still awards the win.
    if(mrEnemyCombatParty->GetListSize() < 1) return true;
    return false;
}

//========================================= Manipulators ==========================================
void AdvCombat::SetBonusXP(int pAmount)
{
    mXPBonus = pAmount;
}
void AdvCombat::SetBonusPlatina(int pAmount)
{
    mPlatinaBonus = pAmount;
}
void AdvCombat::SetBonusJP(int pAmount)
{
    mJPBonus = pAmount;
}
void AdvCombat::SetBonusDoctor(int pAmount)
{
    mDoctorBonus = pAmount;
}
void AdvCombat::BeginResolutionSequence(int pSequence)
{
    //--In all cases, set the state/timer to 0.
    mResolutionTimer = 0;
    mResolutionState = 0;
    if(pSequence < 0 || pSequence >= ADVCOMBAT_END_TOTAL) pSequence = ADVCOMBAT_END_NONE;
    mCombatResolution = pSequence;
}

//============================================ Update =============================================
void AdvCombat::UpdateResolution()
{
    //--No resolution, do nothing.
    if(mCombatResolution == ADVCOMBAT_END_NONE)
    {
        return;
    }
    //--Victory.
    else if(mCombatResolution == ADVCOMBAT_END_VICTORY)
    {
        //--Always update members of this list.
        AdvVictoryItemPack *rVictoryPack = (AdvVictoryItemPack *)mItemsAwardedList->PushIterator();
        while(rVictoryPack)
        {
            if(rVictoryPack->mTimer < ADVCOMBAT_VICTORY_PACK_TICKS) rVictoryPack->mTimer ++;
            rVictoryPack = (AdvVictoryItemPack *)mItemsAwardedList->AutoIterate();
        }

        //--0th tick: Run all combat resolution handlers.
        if(mResolutionState == ADVCOMBAT_VICTORY_OVERLAY && mResolutionTimer == 0)
        {
            //--[Music]
            //--If mandating maximum intensity for layered tracks, we need to store the position of the audio
            //  before stopping music for the victory theme.
            AudioManager *rAudioManager = AudioManager::Fetch();
            AdventureLevel *rCheckLevel = AdventureLevel::Fetch();

            //--Not Layering Tracks
            if(rCheckLevel && !rCheckLevel->IsLayeringMusic())
            {
            }
            //--Layered tracks, not mandating intensity.
            else if(rCheckLevel && !AdventureLevel::xIsCombatMaxIntensity)
            {
            }
            //--Mandate combat intensity as maximum if combat is set that way. We need to store track positions.
            else if(rCheckLevel && AdventureLevel::xIsCombatMaxIntensity)
            {
                //--Allocate.
                mEndCombatTracksTotal = AdventureLevel::xCurrentlyRunningTracks;
                mEndCombatMusicResume = (float *)starmemoryalloc(sizeof(float) * mEndCombatTracksTotal);

                //--Iterate.
                for(int i = 0; i < AdventureLevel::xCurrentlyRunningTracks; i ++)
                {
                    //--Check the package. If it exists, store its position.
                    AudioPackage *rLayer = rAudioManager->GetMusicPack(AdventureLevel::xLayerNames[i]);
                    if(rLayer)
                    {
                        mEndCombatMusicResume[i] = rLayer->GetPosition();
                        rLayer->FadeOut(15);
                    }
                    //--Otherwise, store 0.0f.
                    else
                    {
                        mEndCombatMusicResume[i] = 0.0f;
                    }
                }
            }

            //--If this flag is set, it means we want the music to resume once the victory theme is over.
            if(mQuietBackgroundMusicForVictory)
            {
                rPreviousMusicWhenReplaying = rAudioManager->GetPlayingMusic();
                if(rPreviousMusicWhenReplaying) mPreviousTimeWhenReplaying = rPreviousMusicWhenReplaying->GetPosition();
            }

            //--Stop music and play the victory theme.
            rAudioManager->StopAllMusic();
            rAudioManager->PlayMusicNoFade("CombatVictory");

            //--[Party Entities]
            //--Reset this flag for all entities.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
            while(rEntity)
            {
                rEntity->mLeveledUpThisBattle = false;
                rEntity->mLevelLastTick = rEntity->GetLevel();
                rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
            }

            //--Reset totals.
            mXPToAwardStorage = 0;
            mJPToAwardStorage = 0;
            mPlatinaToAwardStorage = 0;
            mDoctorToAwardStorage = 0;
            mItemsToAwardList->ClearList();

            //--[Tally Values]
            //--Iterate across all entities in the graveyard. Add their totals to the counters.
            AdvCombatEntity *rGraveEntity = (AdvCombatEntity *)mrEnemyGraveyard->PushIterator();
            while(rGraveEntity)
            {
                //--Totals.
                mXPToAwardStorage      += rGraveEntity->GetRewardXP();
                mJPToAwardStorage      += rGraveEntity->GetRewardJP();
                mPlatinaToAwardStorage += rGraveEntity->GetRewardPlatina();

                //--Doctor bag. If the value is -1, it means to use the turn resolver to figure out how to award doctor
                //  bag values. Otherwise, directly add it.
                int tDoctorReward = rGraveEntity->GetRewardDoctor();
                if(tDoctorReward >= 0)
                {
                    mDoctorToAwardStorage += tDoctorReward;
                }
                else
                {
                    //--Reset.
                    mDoctorResolveValue = 0;
                    int tTurnKOd = rGraveEntity->GetTurnKOd();

                    //--Run the script, if it exists.
                    if(mDoctorResolvePath) LuaManager::Fetch()->ExecuteLuaFile(mDoctorResolvePath, 1, "N", (float)tTurnKOd);

                    //--If the value was above zero, add it.
                    if(mDoctorResolveValue > 0) mDoctorToAwardStorage += mDoctorResolveValue;
                }

                //--Add any items to the list.
                SugarLinkedList *rItemsList = rGraveEntity->GetRewardList();
                char *rItemName = (char *)rItemsList->SetToHeadAndReturn();
                while(rItemName)
                {
                    rItemsList->LiberateRandomPointerEntry();
                    mItemsToAwardList->AddElement("X", rItemName, &FreeThis);
                    rItemName = (char *)rItemsList->SetToHeadAndReturn();
                }

                //--Next.
                rGraveEntity = (AdvCombatEntity *)mrEnemyGraveyard->AutoIterate();
            }

            //--[Response Scripts]
            //--Run.
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_ENDCOMBAT);

            //--[Add Totals]
            //--Add bonuses to the storage values.
            mXPToAwardStorage += mXPBonus;
            mJPToAwardStorage += mJPBonus;
            mPlatinaToAwardStorage += mPlatinaBonus;
            mDoctorToAwardStorage += mDoctorBonus;

            //--Line up the current with totals.
            mXPToAward = mXPToAwardStorage;
            mJPToAward = mJPToAwardStorage;
            mPlatinaToAward = mPlatinaToAwardStorage;
            mDoctorToAward = mDoctorToAwardStorage;

            //--Run the doctor bag heal action.
            if(mAutoUseDoctorBag) HealFromDoctorBag(-1);
        }

        //--Overlay. The word "VICTORY!" displays.
        if(mResolutionState == ADVCOMBAT_VICTORY_OVERLAY)
        {
            mResolutionTimer ++;
            if(mResolutionTimer >= ADVCOMBAT_VICTORY_OVERLAY_TICKS) { mResolutionState ++; mResolutionTimer = 0; }
        }
        //--Victory zooms to the top of the screen.
        else if(mResolutionState == ADVCOMBAT_VICTORY_ZOOM)
        {
            mResolutionTimer ++;
            if(mResolutionTimer >= ADVCOMBAT_VICTORY_ZOOM_TICKS) { mResolutionState ++; mResolutionTimer = 0; }
        }
        //--Displays slide in from the side of the screen.
        else if(mResolutionState == ADVCOMBAT_VICTORY_SLIDE)
        {
            mResolutionTimer ++;
            if(mResolutionTimer >= ADVCBOMAT_VICTORY_SLIDE_TICKS) { mResolutionState ++; mResolutionTimer = 0; }
        }
        //--XP/JP/Drops/etc count off.
        else if(mResolutionState == ADVCOMBAT_VICTORY_COUNT_OFF)
        {
            //--Flag for sound checking.
            bool tHasPlayedSound = false;

            //--Timer.
            mResolutionTimer ++;
            if(mResolutionTimer < ADVCBOMAT_VICTORY_AWARD_TICKS)
            {
                if(ControlManager::Fetch()->IsAnyKeyPressed())
                {
                    mResolutionTimer = ADVCBOMAT_VICTORY_AWARD_TICKS;
                }
            }
            else
            {
                if(ControlManager::Fetch()->IsAnyKeyPressed())
                {
                    //--When updating the resolution state, check if we need to do any doctor bag usage.
                    if(mAutoUseDoctorBag)
                    {
                        //--Check if any healing is needed.
                        bool tAnyHealingNeeded = false;
                        for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
                        {
                            AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)mrActiveParty->GetElementBySlot(i);
                            if(rCheckEntity)
                            {
                                float cHPPercent = rCheckEntity->GetHealthPercent();
                                if(cHPPercent < 1.0f)
                                {
                                    tAnyHealingNeeded = true;
                                    break;
                                }
                            }
                        }

                        //--Healing was needed:
                        if(tAnyHealingNeeded)
                        {
                            mResolutionState = ADVCOMBAT_VICTORY_DOCTOR;
                            mResolutionTimer = 0;
                            AdventureInventory *rInventory = AdventureInventory::Fetch();
                            mResolutionDoctorStart = rInventory->GetDoctorBagCharges();
                            mResolutionDoctorEnd = rInventory->GetDoctorBagCharges();
                        }
                        //--Skip.
                        else
                        {
                            mResolutionState = ADVCOMBAT_VICTORY_HIDE;
                            mResolutionTimer = 0;
                        }
                    }
                    //--Skip to hiding.
                    else
                    {
                        mResolutionState = ADVCOMBAT_VICTORY_HIDE;
                        mResolutionTimer = 0;
                    }
                }
            }

            //--Compute how much of each thing should be awarded by this tick, versus the amount that has been. Award the difference.
            float cPercent = EasingFunction::Linear(mResolutionTimer, ADVCBOMAT_VICTORY_AWARD_TICKS);

            //--Platina.
            int tPlatinaTarget = mPlatinaToAwardStorage * cPercent;
            int tPlatinaSoFar = (mPlatinaToAwardStorage - mPlatinaToAward);
            int tPlatinaDif = tPlatinaTarget - tPlatinaSoFar;
            if(tPlatinaDif > 0)
            {
                AwardPlatina(tPlatinaDif);
                mPlatinaToAward = mPlatinaToAward - tPlatinaDif;
            }

            //--Doctor Bag.
            int tDoctorTarget = mDoctorToAwardStorage * cPercent;
            int tDoctorSoFar = (mDoctorToAwardStorage - mDoctorToAward);
            int tDoctorDif = tDoctorTarget - tDoctorSoFar;
            if(tDoctorDif > 0)
            {
                AwardDoctorBag(tDoctorDif);
                mDoctorToAward = mDoctorToAward - tDoctorDif;
            }

            //--XP.
            int tXPTarget = mXPToAwardStorage * cPercent;
            int tXPSoFar = (mXPToAwardStorage - mXPToAward);
            int tXPDif = tXPTarget - tXPSoFar;
            if(tXPDif > 0)
            {
                AwardXP(tXPDif);
                mXPToAward = mXPToAward - tXPDif;
            }

            //--JP.
            int tJPTarget = mJPToAwardStorage * cPercent;
            int tJPSoFar = (mJPToAwardStorage - mJPToAward);
            int tJPDif = tJPTarget - tJPSoFar;
            if(tJPDif > 0)
            {
                AwardJP(tJPDif);
                mJPToAward = mJPToAward - tJPDif;
            }

            //--Items.
            int tItemsTarget = mItemsToAwardList->GetListSize() * cPercent;
            int tItemsSoFar = mItemsAwardedList->GetListSize();
            int tItemsDif = tItemsTarget - tItemsSoFar;
            if(tItemsDif > 0)
            {
                tHasPlayedSound = (tHasPlayedSound || AwardItems(tItemsSoFar, tItemsTarget));
            }

            //--If we haven't played a sound yet, play a tick sound every few ticks.
            if(!tHasPlayedSound && mResolutionTimer % 4 == 1 && mResolutionTimer < ADVCBOMAT_VICTORY_AWARD_TICKS)
            {
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Use the doctor bag. Gets skipped if the flag is off or nobody is injured.
        else if(mResolutionState == ADVCOMBAT_VICTORY_DOCTOR)
        {
            //--First tick.
            if(mResolutionTimer == 0)
            {
                //--Store the values.
                AdventureInventory *rInventory = AdventureInventory::Fetch();
                mResolutionDoctorStart = rInventory->GetDoctorBagCharges();
                HealFromDoctorBag(-1);
                mResolutionDoctorEnd = rInventory->GetDoctorBagCharges();

                //--If the two values were exactly the same, no actual heal took place (somehow).
                if(mResolutionDoctorStart == mResolutionDoctorEnd)
                {
                    mResolutionState = ADVCOMBAT_VICTORY_HIDE;
                    mResolutionTimer = 0;
                }
                //--Otherwise, play the sound.
                else
                {
                    AudioManager::Fetch()->PlaySound("Combat|DoctorBag");
                    mResolutionTimer ++;
                }
            }
            //--Successive ticks.
            else
            {
                mResolutionTimer ++;
                if(mResolutionTimer >= ADVCBOMAT_VICTORY_DOCTOR_TICKS + 30) { mResolutionState ++; mResolutionTimer = 0; }
            }
        }
        //--Everything hides.
        else if(mResolutionState == ADVCOMBAT_VICTORY_HIDE)
        {
            mResolutionTimer ++;
            if(mResolutionTimer >= ADVCBOMAT_VICTORY_HIDE_TICKS) { mResolutionState ++; mResolutionTimer = 0; }
        }
        //--Finish.
        else
        {
            //--If there is an override script, handle it here:
            if(mCurrentVictoryScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mCurrentVictoryScript);
            }
            //--Run the standard victory script if it exists:
            else if(mStandardVictoryScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mStandardVictoryScript);
            }

            //--By default, return everyone to their starting jobs if they changed jobs. This only applies to
            //  party members. This deliberately fires *after* the victory script, which may want to query
            //  the ending job of party members.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
            while(rEntity)
            {
                rEntity->HandleCombatEnd();
                rEntity->RevertToCombatStartJob();
                rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
            }

            //--All entities handle the post-battle code.
            AdventureLevel *rLevel = AdventureLevel::Fetch();
            if(rLevel) rLevel->RunPostCombatOnEntities();

            //--Purge world references.
            WorldRefPack *rPackage = (WorldRefPack *)mWorldReferences->PushIterator();
            while(rPackage)
            {
                if(rPackage->mTurns < 1 && rPackage->rActor)
                {
                    rPackage->rActor->BeginDying();
                    rLevel->KillEnemy(rPackage->rActor->GetUniqueEnemyName());
                }
                rPackage = (WorldRefPack *)mWorldReferences->AutoIterate();
            }
            mWorldReferences->ClearList();

            //--In all cases, deactivate this object.
            Deactivate();
            ResumeMusic();
        }
    }
    //--Defeat/Surrender.
    else if(mCombatResolution == ADVCOMBAT_END_DEFEAT || mCombatResolution == ADVCOMBAT_END_SURRENDER)
    {
        //--0th tick: Run all combat resolution handlers.
        if(mResolutionTimer == 0)
        {
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_ENDCOMBAT);
        }

        //--Defeat doesn't care about player input and doesn't have modes.
        int cTotalTicks = (ADVCOMBAT_DEFEAT_TICKS_LETTERS + ADVCOMBAT_DEFEAT_TICKS_HOLD + ADVCOMBAT_DEFEAT_TICKS_FADEOUT);
        if(mResolutionTimer < cTotalTicks)
        {
            mResolutionTimer ++;
        }
        //--Ending.
        else
        {
            //--If this is a surrender, set this flag.
            if(mCombatResolution == ADVCOMBAT_END_SURRENDER)
            {
                mWasLastDefeatSurrender = true;
            }

            //--If there is an override script, handle it here:
            if(mCurrentDefeatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mCurrentDefeatScript);
            }
            //--Run the standard defeat script if it exists:
            else if(mStandardDefeatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mStandardDefeatScript);
            }

            //--All entities handle the post-battle code.
            AdventureLevel *rLevel = AdventureLevel::Fetch();
            if(rLevel) rLevel->RunPostCombatOnEntities();

            //--Return characters to their starting jobs and handle stat changes.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
            while(rEntity)
            {
                rEntity->HandleCombatEnd();
                rEntity->RevertToCombatStartJob();
                rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
            }

            //--In all cases, deactivate this object.
            Deactivate();
            ResumeMusic();
        }
    }
    //--Retreat.
    else if(mCombatResolution == ADVCOMBAT_END_RETREAT)
    {
        //--0th tick: Run all combat resolution handlers.
        if(mResolutionTimer == 0)
        {
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_ENDCOMBAT);
        }

        //--Retreat just counts to the end.
        if(mResolutionTimer < ADVCOMBAT_RETREAT_TICKS)
        {
            mResolutionTimer ++;
        }
        //--Ending.
        else
        {
            //--If there is an override script, handle it here:
            if(mCurrentRetreatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mCurrentRetreatScript);
            }
            //--Run the standard retreat script if it exists:
            else if(mStandardRetreatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mStandardRetreatScript);
            }

            //--All entities handle the post-battle code.
            AdventureLevel *rLevel = AdventureLevel::Fetch();
            if(rLevel) rLevel->RunPostCombatOnEntities();

            //--Return characters to their starting jobs and handle stat changes.
            AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
            while(rEntity)
            {
                rEntity->HandleCombatEnd();
                rEntity->RevertToCombatStartJob();
                rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
            }

            //--In all cases, deactivate this object.
            Deactivate();
            ResumeMusic();

            //--Send an instruction to the current level that the retreat case is active. This increases
            //  enemy detection range and, if the player gets in another fight, they can't retreat.
            AdventureLevel::Fetch()->SetRetreatTimer(AL_RETREAT_STANDARD);
        }
    }
}

//=========================================== Drawing =============================================
void AdvCombat::RenderResolution()
{
    //--Renders the victory handler. This is for all cases of victory: Defeat, surrender, retreat, or victory.
    //  This is basically just a routing call.
    if(mCombatResolution == ADVCOMBAT_END_NONE || !Images.mIsReady) return;
    DebugPush(true, "Combat Resolution Render: Begin.\n");

    //--Victory:
    DebugPrint("Handling Victory.\n");
    if(mCombatResolution == ADVCOMBAT_END_VICTORY)   RenderVictory();

    //--Defeat and Surrender share functions:
    DebugPrint("Handling Defeat/Surrender.\n");
    if(mCombatResolution == ADVCOMBAT_END_DEFEAT)    RenderDefeat();
    if(mCombatResolution == ADVCOMBAT_END_SURRENDER) RenderDefeat();

    //--Retreat:
    DebugPrint("Handling Retreat.\n");
    if(mCombatResolution == ADVCOMBAT_END_RETREAT) RenderRetreat();
    DebugPop("Exited normally.\n");
}

//=========================================== Victory =============================================
void AdvCombat::RenderVictoryBanner(float pPercent)
{
    //--Worker function, used to render the "VICTORY!" and backing banner the same way in many places
    //  within the below function.

    //--BannerBack slides in from the top.
    float cBannerBackYSize = 160.0f;
    float cBannerBackYOff = cBannerBackYSize * (1.0f - pPercent) * -1.0f;
    Images.Data.rVictoryBannerBack->Draw(0.0f, cBannerBackYOff);

    //--Compute position.
    float cScale = 1.00f - (0.50f * pPercent);
    float cBanRenderX = ( VIRTUAL_CANVAS_X * 0.50f / cScale) - (Images.Data.rVictoryBannerBig->GetTrueWidth() * 0.50f);
    float cBanRenderY = -140.0f * pPercent;
    float cTxtRenderX = ((VIRTUAL_CANVAS_X * 0.50f / cScale) - (Images.Data.rVictoryFontHeader->GetTextWidth("VICTORY!") * 0.50f));
    float cTxtRenderY = (262.0f + cBanRenderY);

    //--Scale, render.
    glScalef(cScale, cScale, 1.0f);
    Images.Data.rVictoryBannerBig->Draw(cBanRenderX, cBanRenderY);
    Images.Data.rVictoryFontHeader->DrawText(cTxtRenderX, cTxtRenderY, SUGARFONT_AUTOCENTER_Y, 1.0f, "VICTORY!");
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
}
void AdvCombat::RenderVictoryDrops(float pPercent)
{
    //--Renders the drops box. The passed percentage is how far off the screen to scroll.
    float cFrameX = -650.0f * (1.0f - pPercent);
    glTranslatef(cFrameX, 0.0f, 0.0f);

    //--Backing.
    Images.Data.rVictoryDropFrame->Draw();

    //--Text.
    Images.Data.rVictoryFontPlatina->DrawText(453.0f, 193.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Platina");
    Images.Data.rVictoryFontPlatina->DrawText(453.0f, 339.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Items");

    //--Cash amount, if there is a pending addition.
    int tPlatina = AdventureInventory::Fetch()->GetPlatina();
    if(mPlatinaToAward > 0)
    {
        Images.Data.rVictoryFontPlatina->DrawTextArgs(453.0f, 262.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i + %i", tPlatina, mPlatinaToAward);
    }
    //--Cash amount, no amount pending.
    else
    {
        Images.Data.rVictoryFontPlatina->DrawTextArgs(453.0f, 262.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i", tPlatina);
    }

    //--Render items gained. The list only has items that have been awarded thus far. They also fade in over time.
    float cXPos = 320.0f;
    float cYPos = 366.0f;
    float cYHei = 24.0f;
    AdvVictoryItemPack *rPackage = (AdvVictoryItemPack *)mItemsAwardedList->PushIterator();
    while(rPackage)
    {
        //--Alpha:
        float cAlpha = EasingFunction::Linear(rPackage->mTimer, ADVCOMBAT_VICTORY_PACK_TICKS);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

        //--Icon, if it exists.
        if(rPackage->rItemImg) rPackage->rItemImg->Draw(cXPos, cYPos);

        //--Name.
        Images.Data.rVictoryFontItem->DrawText(cXPos + 24.0f, cYPos, 0, 1.0f, rPackage->rItemNameRef);

        //--Next.
        cYPos = cYPos + cYHei;
        rPackage = (AdvVictoryItemPack *)mItemsAwardedList->AutoIterate();
    }

    //--Clean.
    StarlightColor::ClearMixer();
    glTranslatef(-cFrameX, 0.0f, 0.0f);
}
void AdvCombat::RenderVictoryDoctor(float pPercent)
{
    //--Doctor bag from the bottom.
    float cFrameY = 100.0f * (1.0f - pPercent);
    glTranslatef(0.0f, cFrameY, 0.0f);

    //--Backing.
    Images.Data.rVictoryDoctorBack->Draw();

    //--Get doctor bag percentages.
    AdventureInventory *rAdvInventory = AdventureInventory::Fetch();
    int tDoctorChargesCur = rAdvInventory->GetDoctorBagCharges();
    int tDoctorChargesMax = rAdvInventory->GetDoctorBagChargesMax();
    if(tDoctorChargesMax < 1) tDoctorChargesMax = 1;

    //--Special: When in this mode, we change the doctor bag current value.
    if(mResolutionState == ADVCOMBAT_VICTORY_DOCTOR)
    {
        float tActualPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCBOMAT_VICTORY_DOCTOR_TICKS);
        if(tActualPercent < 0.0f) tActualPercent = 0.0f;
        if(tActualPercent > 1.0f) tActualPercent = 1.0f;
        tDoctorChargesCur = (int)(mResolutionDoctorStart + ((mResolutionDoctorEnd - mResolutionDoctorStart) * tActualPercent));
    }

    //--Compute and render percentage.
    float tPercent = (float)tDoctorChargesCur / (float)tDoctorChargesMax;
    if(tPercent < 0.0f) tPercent = 0.0f;
    if(tPercent > 1.0f) tPercent = 1.0f;
    RenderPercent(tPercent, 1.0f, Images.Data.rVictoryDoctorFill);

    //--Capping frame.
    Images.Data.rVictoryDoctorFrame->Draw();

    //--Text with to-award.
    if(mDoctorToAward > 0)
    {
        Images.Data.rVictoryFontDoctor->DrawTextArgs(384.0f, 689.0f, 0, 1.0f, "Doctor Bag: %i + %i", tDoctorChargesCur, mDoctorToAward);
    }
    //--No award.
    else
    {
        Images.Data.rVictoryFontDoctor->DrawTextArgs(384.0f, 689.0f, 0, 1.0f, "Doctor Bag: %i", tDoctorChargesCur);
    }

    //--Extra text during doctor mode.
    if(mResolutionState == ADVCOMBAT_VICTORY_DOCTOR)
    {
        Images.Data.rVictoryFontDoctor->DrawTextArgs(781.0f, 689.0f, 0, 1.0f, "Healed!");
    }

    //--Clean.
    glTranslatef(0.0f, -cFrameY, 0.0f);
}
void AdvCombat::RenderExpBars(float pPercent)
{
    //--Exp frames slide from the right. Only the active party is rendered, combat party and active
    //  party may not be the same.
    float cFrameX = 700.0f * (1.0f - pPercent);
    glTranslatef(cFrameX, 0.0f, 0.0f);

    //--Offset tracking.
    float cYOffset = 0.0f;
    float cYSpacing = 142.0f;

    //--Iterate.
    int i = 0;
    AdvCombatEntity *rPartyMember = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rPartyMember)
    {
        //--Get Level/XP variables.
        int tLevel = rPartyMember->GetLevel();
        int tCurXP = rPartyMember->GetXP();
        int tCurrLevel = rPartyMember->GetXPOfLevel();
        int tNextLevel = rPartyMember->GetXPToNextLevel();
        int tNextLevelBase = rPartyMember->GetXPToNextLevelMax();

        //--Compute.
        float cPercent = 0.0f;
        if(tNextLevelBase > 0) cPercent = (float)(tCurXP-tCurrLevel) / (float)tNextLevelBase;

        //--[Common]
        //--Backing.
        Images.Data.rVictoryExpFrameBack->Draw();

        //--[Max Level]
        //--If at max level, the percentage is always 100%.
        if(tNextLevel < 0)
        {
            cPercent = 1.0f;
        }
        //--[Has Not Leveled]
        //--If the entity has not leveled up this battle, render the bar normally.
        else if(!rPartyMember->mLeveledUpThisBattle)
        {
        }
        //--[Has Leveled]
        //--Render "Level Up!" instead of the XP counter. XP keeps ticking.
        else
        {
        }

        //--[Common]
        //--Get fill percentage, render XP bar.
        RenderPercent(cPercent, 1.0f, Images.Data.rVictoryExpFrameFill);

        //--Front.
        Images.Data.rVictoryExpFrameFront->Draw();

        //--If at max level, render "MAX" instead of the level number.
        if(tNextLevel < 0)
        {
            Images.Data.rVictoryFontPlatina->DrawText(1031.0f, 197.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "MAX");
        }
        //--Render the level number.
        else
        {
            Images.Data.rVictoryFontPlatina->DrawTextArgs(1031.0f, 197.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i", tLevel+1);
        }

        //--[Portrait]
        //--Render potrait.
        SugarBitmap *rCombatPortrait = rPartyMember->GetCombatPortrait();
        SugarBitmap *rCountermask = rPartyMember->GetVictoryCounterMask();
        if(rCombatPortrait)
        {
            //--Masking.
            DisplayManager::ActivateMaskRender(ADVCOMBAT_VICTORY_MASK_START + i);
            Images.Data.rVictoryExpFrameMask->Draw();

            //--Stencil and position.
            TwoDimensionRealPoint tRenderDim = rPartyMember->GetUIRenderPosition(ACE_UI_INDEX_VICTORY_MAIN);

            //--Half scale.
            glScalef(0.5f, 0.5f, 1.0f);

            //--If a countermask is available, render that.
            if(rCountermask)
            {
                DisplayManager::ActivateMaskRender(0);
                rCountermask->Draw(tRenderDim.mXCenter * 2.0f, tRenderDim.mYCenter * 2.0f);
            }

            //--Render.
            DisplayManager::ActivateStencilRender(ADVCOMBAT_VICTORY_MASK_START + i);
            rCombatPortrait->Draw(tRenderDim.mXCenter * 2.0f, tRenderDim.mYCenter * 2.0f);

            //--Clean.
            glScalef(2.0f, 2.0f, 1.0f);
            DisplayManager::DeactivateStencilling();
        }

        //--JP gained.
        Images.Data.rVictoryFontExp->DrawTextArgs(806.0f, 105.0f, 0, 1.0f, "JP: +%i", mJPToAwardStorage);

        //--[XP Left]
        //--If at max level, render nothing for the XP count.
        if(tNextLevel < 0)
        {
        }
        //--[Has Not Leveled]
        //--If the entity has not leveled up this battle, render the XP needed.
        else if(!rPartyMember->mLeveledUpThisBattle)
        {
            //--XP left to award:
            if(mXPToAward > 0)
            {
                Images.Data.rVictoryFontExp->DrawTextArgs(806.0f, 152.0f, 0, 1.0f, "EXP: %i + %i", tCurXP, mXPToAward);
                Images.Data.rVictoryFontExp->DrawTextArgs(806.0f, 168.0f, 0, 1.0f, "Next: %i", tNextLevel);
            }
            //--Just the XP value:
            else
            {
                Images.Data.rVictoryFontExp->DrawTextArgs(806.0f, 152.0f, 0, 1.0f, "EXP: %i", tCurXP);
                Images.Data.rVictoryFontExp->DrawTextArgs(806.0f, 168.0f, 0, 1.0f, "Next: %i", tNextLevel);
            }
        }
        //--[Has Leveled]
        //--Render "Level Up!" instead of the XP counter. XP keeps ticking.
        else
        {
            //--XP left to award:
            if(mXPToAward > 0)
            {
                Images.Data.rVictoryFontExp->DrawTextArgs(806.0f, 152.0f, 0, 1.0f, "EXP: %i + %i", tCurXP, mXPToAward);
                Images.Data.rVictoryFontExp->DrawTextArgs(806.0f, 168.0f, 0, 1.0f, "Level Up!", tNextLevel);
            }
            //--Just the XP value:
            else
            {
                Images.Data.rVictoryFontExp->DrawTextArgs(806.0f, 152.0f, 0, 1.0f, "EXP: %i", tCurXP);
                Images.Data.rVictoryFontExp->DrawTextArgs(806.0f, 168.0f, 0, 1.0f, "Level Up!", tNextLevel);
            }
        }

        //--Next.
        i ++;
        cYOffset = cYOffset + cYSpacing;
        glTranslatef(0.0f, cYSpacing, 0.0f);
        rPartyMember = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }

    //--Clean.
    glTranslatef(-cFrameX, -cYOffset, 0.0f);
}
void AdvCombat::RenderVictory()
{
    //--[Documentation and Setup]
    //--Victory! The player sees UI showing their party and the spoils of the fight.

    //--[Backing]
    //--Backing underlay.
    SugarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);

    //--[Victory Overlay]
    //--In the first phase, render the "VICTORY!" text.
    if(mResolutionState == ADVCOMBAT_VICTORY_OVERLAY)
    {
        //--Banner.
        float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCOMBAT_VICTORY_OVERLAY_TICKS / 10.0f);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPercent);
        Images.Data.rVictoryBannerBig->Draw();
        StarlightColor::ClearMixer();

        //--Get the full length. We compute the left position using the full length.
        float cRenderX = (VIRTUAL_CANVAS_X * 0.50f) - (Images.Data.rVictoryFontHeader->GetTextWidth("VICTORY!") * 0.50f);
        float cRenderY = 262.0f;

        //--Create a buffer with the number of letters we are currently displaying.
        int tLetters = mResolutionTimer / ADVCOMBAT_VICTORY_OVERLAY_TICKS_PER_LETTER;
        if(tLetters >= 1)
        {
            char tBuffer[10];
            memset(tBuffer, 0, sizeof(char) * 10);
            strncpy(tBuffer, "VICTORY!", tLetters);
            Images.Data.rVictoryFontHeader->DrawText(cRenderX, cRenderY, SUGARFONT_AUTOCENTER_Y, 1.0f, tBuffer);
        }
    }
    //--Zoom "Victory" to the top of the screen, slide in the banner beneath it.
    else if(mResolutionState == ADVCOMBAT_VICTORY_ZOOM)
    {
        //--Compute percent.
        float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCOMBAT_VICTORY_ZOOM_TICKS);
        RenderVictoryBanner(cPercent);
    }
    //--Slide displays in from the side of the screen.
    else if(mResolutionState == ADVCOMBAT_VICTORY_SLIDE)
    {
        //--Victory! banner does not move.
        RenderVictoryBanner(1.0f);

        //--Completion percentage.
        float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCBOMAT_VICTORY_SLIDE_TICKS);
        RenderVictoryDrops(cPercent);
        RenderVictoryDoctor(cPercent);
        RenderExpBars(cPercent);
    }
    //--XP/JP/Drops/etc count off.
    else if(mResolutionState == ADVCOMBAT_VICTORY_COUNT_OFF)
    {
        //--Victory! banner does not move.
        RenderVictoryBanner(1.0f);

        //--All these render at full completion.
        RenderVictoryDrops(1.0f);
        RenderVictoryDoctor(1.0f);
        RenderExpBars(1.0f);
    }
    //--Changes doctor bag quantities. This is handled within the subroutine.
    else if(mResolutionState == ADVCOMBAT_VICTORY_DOCTOR)
    {
        RenderVictoryBanner(1.0f);
        RenderVictoryDrops(1.0f);
        RenderVictoryDoctor(1.0f);
        RenderExpBars(1.0f);
    }
    //--Slide all panels offscreen.
    else if(mResolutionState == ADVCOMBAT_VICTORY_HIDE)
    {
        //--Victory! banner slides up.
        float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCBOMAT_VICTORY_HIDE_TICKS);
        float cSlideOff = -110.0f * cPercent;
        glTranslatef(0.0f, cSlideOff, 0.0f);
        RenderVictoryBanner(1.0f);
        glTranslatef(0.0f, -cSlideOff, 0.0f);

        //--Completion percentage.
        RenderVictoryDrops(1.0f - cPercent);
        RenderVictoryDoctor(1.0f - cPercent);
        RenderExpBars(1.0f - cPercent);
    }
}

//=========================================== Defeat ==============================================
void AdvCombat::RenderDefeat()
{
    //--[Documentation and Setup]
    //--Shows the defeat overlay and then fades to black.

    //--[Phase 1]
    //--Show the defeat letters.
    if(mResolutionTimer < ADVCOMBAT_DEFEAT_TICKS_LETTERS)
    {
        //--Backing is the normal blackout, advancing to fullblack.
        float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCOMBAT_DEFEAT_TICKS_LETTERS);
        float cDif = (1.0f - ADVCOMBAT_STD_BACKING_OPACITY) * cPercent;
        SugarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY + cDif);

        //--Render the defeat letters.
        int tLettersToRender = mResolutionTimer / ADVCOMBAT_DEFEAT_TICKS_PER_LETTER;
        if(tLettersToRender >= ADVCOMBAT_DEFEAT_FRAMES_TOTAL) tLettersToRender = ADVCOMBAT_DEFEAT_FRAMES_TOTAL;
        for(int i = 0; i < tLettersToRender; i ++)
        {
            Images.Data.rDefeatFrames[i]->Draw();
        }
    }
    //--[Phase 2]
    //--Hold on the full set.
    else if(mResolutionTimer < ADVCOMBAT_DEFEAT_TICKS_LETTERS + ADVCOMBAT_DEFEAT_TICKS_HOLD)
    {
        //--Backing is fullblack.
        SugarBitmap::DrawFullBlack(1.0f);

        //--Render the defeat letters.
        for(int i = 0; i < ADVCOMBAT_DEFEAT_FRAMES_TOTAL; i ++)
        {
            Images.Data.rDefeatFrames[i]->Draw();
        }
    }
    //--[Phase 3]
    //--Full set, black fade over.
    else
    {
        //--Backing is fullblack.
        SugarBitmap::DrawFullBlack(1.0f);

        //--Render the defeat letters.
        for(int i = 0; i < ADVCOMBAT_DEFEAT_FRAMES_TOTAL; i ++)
        {
            Images.Data.rDefeatFrames[i]->Draw();
        }

        //--Black overlay.
        int tUseTimer = mResolutionTimer - (ADVCOMBAT_DEFEAT_TICKS_LETTERS + ADVCOMBAT_DEFEAT_TICKS_HOLD);
        float cPct = EasingFunction::QuadraticOut(tUseTimer, ADVCOMBAT_DEFEAT_TICKS_FADEOUT);
        SugarBitmap::DrawFullBlack(cPct);
    }
}
void AdvCombat::RenderRetreat()
{
    //--Just a black overlay.
    float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCOMBAT_RETREAT_TICKS);
    float cDif = (1.0f - ADVCOMBAT_STD_BACKING_OPACITY) * cPercent;
    SugarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY + cDif);
}
