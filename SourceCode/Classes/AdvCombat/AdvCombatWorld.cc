//--Base
#include "AdvCombat.h"

//--Classes
#include "AdventureLevel.h"
#include "TilemapActor.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "EntityManager.h"

void AdvCombat::PulseWorld(bool pDontCheckEnemies)
{
    //--[Documentation and Setup]
    //--When combat is active, the map file will animate an overlay causing combat to crossfade. It optionally
    //  checks for nearby enemies and adds them as reinforcements. This occurs after the reinitialization of
    //  the AdvCombat class and typically right after the Activate() call.

    //--[UI Change]
    //--Activate the reinforcement pulse sequence. This renders the overlay.
    AdventureLevel *rLevel = AdventureLevel::Fetch();
    if(rLevel) rLevel->SetReinforcementPulseMode(true);

    //--[Enemy Reinforcements]
    //--Run the world pulse to add enemies. This does not occur if flagged. The flag is often false for
    //  script-activated combats. It defaults to true for most world encounters.
    if(!pDontCheckEnemies)
    {
        //--Get a list of entities.
        SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();

        //--Run through the entity list.
        RootEntity *rEntity = (RootEntity *)rEntityList->PushIterator();
        while(rEntity)
        {
            //--The entity must be a TilemapActor. We are not interested in anything else.
            if(rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
            {
                //--Cast.
                TilemapActor *rTilemapActor = (TilemapActor *)rEntity;

                //--Run the internal routine.
                rTilemapActor->ComputeReinforcement();
            }

            //--Next.
            rEntity = (RootEntity *)rEntityList->AutoIterate();
        }
    }
}
