//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"
#include "TilemapActor.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"

//--Libraries
//--Managers

int AdvCombat::GetTurn()
{
    return mCurrentTurn;
}
void AdvCombat::GivePlayerInitiative()
{
    mPlayerGainedInitiative = true;
}
void AdvCombat::RemovePlayerInitiative()
{
    mPlayerGainedInitiative = false;
}
void AdvCombat::RollTurnOrder()
{
    //--[Documentation and Setup]
    //--Rolls turn order. All characters roll an initiative value based on their internal value plus rnd() % 40.
    //  Once rolling is done, characters are sorted into buckets based on Always-Strikes-First/Last and Fast/Slow flags.
    //  Then, those are dumped into the turn order and we're done.
    //--Ties are broken by character ID, with party members starting at zero and counting up, then enemies.
    mrTurnOrder->ClearList();

    //--[Initiative Rolling]
    //--Regardless of bucket position, everyone needs to roll. Note that entities who are stunned or otherwise not acting
    //  still roll, they just pass their turns. KO'd party members do not roll, when revived they wait until the next turn.
    int cTotalEntities = 0;
    AdvCombatEntity *rPartyMember = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rPartyMember)
    {
        //--Ask if the member needs to roll.
        if(rPartyMember->CanActThisTurn())
        {
            int cInitiativeRoll = rPartyMember->GetStatistic(ADVCE_STATS_FINAL, STATS_INITIATIVE) + (rand() % ACE_TURN_ORDER_RANGE);
            rPartyMember->SetCurrentInitiative(cInitiativeRoll);
            rPartyMember->SetTurnID(cTotalEntities);
            cTotalEntities ++;
        }

        //--Next.
        rPartyMember = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--Same deal but for the baddies.
    AdvCombatEntity *rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEnemyMember)
    {
        //--Enemies always roll since KO'd enemies leave the combat party.
        int cInitiativeRoll = rEnemyMember->GetStatistic(ADVCE_STATS_FINAL, STATS_INITIATIVE) + (rand() % ACE_TURN_ORDER_RANGE);
        rEnemyMember->SetCurrentInitiative(cInitiativeRoll);
        rEnemyMember->SetTurnID(cTotalEntities);
        cTotalEntities ++;

        //--Next.
        rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }

    //--[Bucketing]
    //--We create a set of lists for each possibility. There are ACE_BUCKET_TOTAL lists and we hard clamp into them.
    //  Entities compute which one they should be in based on their turn-order flags. These are tags that are:
    //  [Always Strikes Last], [Slow], [Fast], [Always Strikes First]
    SugarLinkedList *tTurnLists[ACE_BUCKET_TOTAL];
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++) tTurnLists[i] = new SugarLinkedList(false);

    //--Run across the player's party and put them in the appropriate buckets.
    rPartyMember = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rPartyMember)
    {
        //--Skip non-acting party members.
        if(rPartyMember->CanActThisTurn())
        {
            //--Compute, clamp.
            int tTurnBucket = rPartyMember->ComputeTurnBucket();
            if(tTurnBucket < ACE_BUCKET_LOWEST)  tTurnBucket = ACE_BUCKET_LOWEST;
            if(tTurnBucket > ACE_BUCKET_HIGHEST) tTurnBucket = ACE_BUCKET_HIGHEST;

            //--Place the entity in the bucket.
            tTurnLists[tTurnBucket]->AddElement("X", rPartyMember);
        }

        //--Next.
        rPartyMember = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--Same for the baddies. If the player got initiative, the baddies are not added to the turn buckets
    //  if this is turn 0.
    if(!mPlayerGainedInitiative || mCurrentTurn != 0)
    {
        AdvCombatEntity *rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
        while(rEnemyMember)
        {
            //--Compute, clamp.
            int tTurnBucket = rEnemyMember->ComputeTurnBucket();
            if(tTurnBucket < ACE_BUCKET_LOWEST)  tTurnBucket = ACE_BUCKET_LOWEST;
            if(tTurnBucket > ACE_BUCKET_HIGHEST) tTurnBucket = ACE_BUCKET_HIGHEST;

            //--Place the entity in the bucket.
            tTurnLists[tTurnBucket]->AddElement("X", rEnemyMember);

            //--Next.
            rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
        }
    }
    //--If the player gained initiative, then the enemies are not added here, they are added later.

    //--[Sorting]
    //--All of the buckets now sort the highest initiative to the front.
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++)
    {
        tTurnLists[i]->SortListUsing(&AdvCombat::SortByInitiative);
    }

    //--[Turn Order]
    //--At last, we can now populate the turn order list.
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++)
    {
        AdvCombatEntity *rPtr = (AdvCombatEntity *)tTurnLists[i]->PushIterator();
        while(rPtr)
        {
            //--Entity is knocked out! Don't add them.
            if(!rPtr->IsDefeated())
            {
                mrTurnOrder->AddElementAsTail(rPtr->GetName(), rPtr);
            }
            rPtr = (AdvCombatEntity *)tTurnLists[i]->AutoIterate();
        }
    }

    //--If enemies got ambushed and this is the first turn, place them on the turn order list at the end.
    //  They also receive a special flag, causing them to use the ability "Ambushed" on their first turn.
    if(mPlayerGainedInitiative && mCurrentTurn == 0)
    {
        AdvCombatEntity *rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
        while(rEnemyMember)
        {
            rEnemyMember->SetAmbushed(true);
            mrTurnOrder->AddElementAsTail(rEnemyMember->GetName(), rEnemyMember);
            rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
        }
    }

    //--[Clean]
    //--Deallocate.
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++)
    {
        delete tTurnLists[i];
    }

    //--[Debug]
    if(false)
    {
        //--Header.
        fprintf(stderr, "Turn order report:\n");

        //--List elements.
        int i = 0;
        AdvCombatEntity *rCheckPtr = (AdvCombatEntity *)mrTurnOrder->PushIterator();
        while(rCheckPtr)
        {
            fprintf(stderr, " %i: %s %p - %i %i\n", i, mrTurnOrder->GetIteratorName(), rCheckPtr, rCheckPtr->ComputeTurnBucket(), rCheckPtr->GetCurrentInitiative());
            i ++;
            rCheckPtr = (AdvCombatEntity *)mrTurnOrder->AutoIterate();
        }
    }
}
int AdvCombat::SortByInitiative(const void *pEntryA, const void *pEntryB)
{
    //--Compare the two entities by their initiative. If that fails, use their turn IDs.
    SugarLinkedListEntry **rEntryA = (SugarLinkedListEntry **)pEntryA;
    SugarLinkedListEntry **rEntryB = (SugarLinkedListEntry **)pEntryB;

    //--Cast the data pointer into an AdvCombatEntity.
    AdvCombatEntity *rEntityA = (AdvCombatEntity *)(*rEntryA)->rData;
    AdvCombatEntity *rEntityB = (AdvCombatEntity *)(*rEntryB)->rData;

    //--If the initiatives are identical, compare the turn IDs which cannot be identical.
    int cInitiativeA = rEntityA->GetCurrentInitiative();
    int cInitiativeB = rEntityB->GetCurrentInitiative();
    if(cInitiativeA == cInitiativeB)
    {
        return rEntityB->GetTurnID() - rEntityA->GetTurnID();
    }

    //--Otherwise, just return the comparison.
    return cInitiativeB - cInitiativeA;
}
void AdvCombat::RecomputeTurnBarLength()
{
    //--Whenever the turn list changes length, call this function to recompute its length and move the icons on it.
    float cTurnOrderWidPerPortrait = 48.0f;

    //--Reset timers.
    mTurnBarMoveTimer = 0;
    mTurnBarMoveTimerMax = 15;

    //--Set positions.
    mTurnWidStart = mTurnWidCurrent;
    mTurnWidTarget = (cTurnOrderWidPerPortrait * (mrTurnOrder->GetListSize())) + 10.0f;
}
void AdvCombat::BeginTurnDisplay()
{
    //--Causes the turn display text to appear on screen.
    mTurnDisplayTimer = 0;
}

///--[Turn Control]
void AdvCombat::BeginTurn()
{
    //--Called when there are no actors able to act on the turn list. Starts a new turn.
    //  First, Increment the turn counter. Reinitialize() puts it at -1, meaning the first turn is turn 0.
    mCurrentTurn ++;

    //--Check reinforcements.
    bool tAddedAnyEnemies = false;
    WorldRefPack *rPackage = (WorldRefPack *)mWorldReferences->SetToHeadAndReturn();
    while(rPackage)
    {
        //--If the package has a negative turn count, add to the battle.
        rPackage->mTurns --;
        if(rPackage->mTurns == 0)
        {
            tAddedAnyEnemies = true;
            rPackage->rActor->AppendEnemiesToCombat();
        }

        //--Next.
        rPackage = (WorldRefPack *)mWorldReferences->IncrementAndGetRandomPointerEntry();
    }

    //--If any entities reinforced, position them onscreen.
    if(tAddedAnyEnemies) PositionEnemies();

    //--Run the turn order roller.
    RollTurnOrder();

    //--Mark the new turn length.
    RecomputeTurnBarLength();

    //--Reset the turn timer.
    BeginTurnDisplay();

    //--Mark a new action as taking place.
    mIsActionConcluding = false;
    mPlayerInterfaceTimer = 0;

    //--Specify that no action is occurring. This will cause BeginAction() to start after this ends.
    mIsActionOccurring = false;

    //--Run all response codes.
    RunAllResponseScripts(ADVCOMBAT_RESPONSE_BEGINTURN);
}
void AdvCombat::BeginAction()
{
    //--An 'Action' is the command of a single character. It is implied that whoever is 0th in the turn order
    //  list is the one performing the action.
    //--Player entities will slide in from the left side of the screen. Enemy entities stack together on the right
    //  side and never exit the playing field.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity) return;

    //--Entity resets their free-action counter. This occurs before abilities run their scripts.
    int tFreeActionGen = rActingEntity->GetStatistic(STATS_FREEACTIONGEN);
    rActingEntity->SetFreeActions(tFreeActionGen);
    rActingEntity->SetFreeActionsPerformed(0);

    //--Flag.
    mIsActionConcluding = false;
    mIsActionOccurring = true;
    mActionFirstTick = true;

    //--Check which party the entity is in. Player party, slide onscreen.
    if(IsEntityInPlayerParty(rActingEntity))
    {
        //--Order all other party members offscreen.
        PositionCharactersOffScreen();

        //--Now order the acting character back onscreen.
        float cIdealX = ComputeIdealX(true, 0, 1);
        rActingEntity->SetIdealPositionX(cIdealX);
        rActingEntity->MoveToIdealPosition(ADVCOMBAT_STD_MOVE_TICKS);
    }
    //--Enemy party. Player party stays where it is, and goes on or offscreen based on the entity's action.
    else
    {

    }

    //--Run all response codes.
    RunAllResponseScripts(ADVCOMBAT_RESPONSE_BEGINACTION);
}
void AdvCombat::SetEventTimer(int pTicks)
{
    mEventTimer = pTicks;
}
void AdvCombat::SetAsFreeAction()
{
    mWasLastActionFree = true;
}
void AdvCombat::EnqueueEvent(CombatEventPack *pPack)
{
    //--Adds an event to the tail of the event queue.
    if(!pPack) return;
    mEventQueue->AddElementAsTail("X", pPack, &CombatEventPack::DeleteThis);
}
void AdvCombat::EnqueueApplicationPack(ApplicationPack *pPack)
{
    if(!pPack) return;
    mApplicationQueue->AddElement("X", pPack, &ApplicationPack::DeleteThis);
}
void AdvCombat::CompleteAction()
{
    //--After the previous 'Action' is completed, call this. It advances the turn order by one. This mostly
    //  moves entities offscreen.
    mNoEventsEndsAction = false;
    mIsActionOccurring = false;
    mIsActionConcluding = false;
    mIsSelectingTargets = false;

    //--Get the zeroth entity on the turn order list. This is needed for later.
    void *rZerothTurnCharPtr = mrTurnOrder->GetElementBySlot(0);

    //--Scan the turn order list. If any entities were KO'd, they lose their turn.
    AdvCombatEntity *rKOCheck = (AdvCombatEntity *)mrTurnOrder->SetToHeadAndReturn();
    while(rKOCheck)
    {
        if(rKOCheck->IsDefeated())
        {
            rKOCheck->SetTurnKOd(mCurrentTurn);
            mrTurnOrder->RemoveRandomPointerEntry();
        }
        rKOCheck = (AdvCombatEntity *)mrTurnOrder->IncrementAndGetRandomPointerEntry();
    }

    //--Check if the entity in the 0th slot exists for crash prevention. Entities may be removed by
    //  being defeated.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity)
    {
        PositionCharactersOffScreen();
        return;
    }

    //--Free Action. Same acting entity. Their Free Action counter decreases by 1. Abilities may check
    //  the Free Action counter to see if they have any left, but the turn begins anew here.
    //--Because the acting entity is the same, they don't need to move offscreen.
    if(mWasLastActionFree)
    {
        //--Unset this flag.
        mWasLastActionFree = false;

        //--Check if the acting entity got KO'd, which may happen due to counterattacks. If this happens,
        //  the turn ends anyway.
        if(rActingEntity->IsDefeated())
        {

        }
        //--Entity is able to act.
        else
        {
            //--Flags.
            mIsActionConcluding = false;
            mIsActionOccurring = true;
            mActionFirstTick = true;

            //--Remove one Free Action. Increment the total actions counter.
            int tFreeActions = rActingEntity->GetFreeActions();
            int tFreeActionsPerformed = rActingEntity->GetFreeActionsPerformed();
            rActingEntity->SetFreeActions(tFreeActions - 1);
            rActingEntity->SetFreeActionsPerformed(tFreeActionsPerformed + 1);

            //--Run all response codes.
            RunAllResponseScripts(ADVCOMBAT_RESPONSE_BEGINFREEACTION);
            return;
        }
    }

    //--Order entities to move offscreen. This is bypassed by Free Actions.
    //PositionCharactersOffScreen();

    //--Check the next entity. It can be NULL if that was the last entity in the turn.
    AdvCombatEntity *rNextActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(1);

    //--Null. A new turn will need to begin.
    if(!rNextActingEntity)
    {

    }
    //--If the acting entity and the next acting entity are the same, we don't need to move them:
    else if(rActingEntity == rNextActingEntity)
    {

    }
    //--Otherwise, if the entity was in the player's party, move them offscreen. The next entity will move
    //  onscreen when their action begins, possible at the same time as the other is moving off.
    else if(IsEntityInPlayerParty(rActingEntity) && IsEntityInPlayerParty(rNextActingEntity))
    {
        //--Compute how far offscreen to go. Wider portraits go further.
        float cOffscreenX = ADVCOMBAT_POSITION_OFFSCREEN_LFT;
        SugarBitmap *rCombatPortrait = rActingEntity->GetCombatPortrait();
        if(rCombatPortrait) cOffscreenX = cOffscreenX - rCombatPortrait->GetWidth();

        //--Position.
        rActingEntity->SetIdealPositionX(cOffscreenX);
        rActingEntity->MoveToIdealPosition(ADVCOMBAT_STD_MOVE_TICKS);
    }
    //--Enemy entity.
    else
    {

    }

    //--Run all response codes.
    RunAllResponseScripts(ADVCOMBAT_RESPONSE_ENDACTION);

    //--Once that's done, pop the turn order. Note that this happens after effects/abilities, because those need to know
    //  who the previous acting entity was.
    //--This does not happen if that entity was already removed, such as being KO'd.
    void *rNewZerothActor = mrTurnOrder->GetElementBySlot(0);
    if(rZerothTurnCharPtr == rNewZerothActor)
    {
        mrTurnOrder->RemoveElementI(0);
    }

    //--Recompute turn bar length.
    RecomputeTurnBarLength();
}
void AdvCombat::EndTurn()
{
    //--Called when all entities have finished acting and the turn order list has no elements.
    mIsTurnActive = false;

    //--Run all response codes.
    RunAllResponseScripts(ADVCOMBAT_RESPONSE_ENDTURN);
}
