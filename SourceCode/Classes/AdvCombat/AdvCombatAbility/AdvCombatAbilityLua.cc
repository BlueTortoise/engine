//--Base
#include "AdvCombatAbility.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

//========================================= Lua Hooking ===========================================
void AdvCombatAbility::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombatAbility_GetProperty("Owner ID") (1 Integer)
       AdvCombatAbility_GetProperty("Cooldown") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatAbility_GetProperty", &Hook_AdvCombatAbility_GetProperty);

    /* AdvCombatAbility_SetProperty("Display Name", sName)
       AdvCombatAbility_SetProperty("Description", sDescription)
       AdvCombatAbility_SetProperty("Icon Back", sDLPath)
       AdvCombatAbility_SetProperty("Icon", sDLPath)
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatAbility_SetProperty", &Hook_AdvCombatAbility_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdvCombatAbility_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatAbility_GetProperty("Internal Name") (1 String)
    //AdvCombatAbility_GetProperty("Owner ID") (1 Integer)
    //AdvCombatAbility_GetProperty("Cooldown") (1 Integer)
    //AdvCombatAbility_GetProperty("Tag Count", sTag) (1 Integer)
    //AdvCombatAbility_GetProperty("Is Owner Acting") (1 Boolean)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatAbility_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATABILITY)) return LuaTypeError("AdvCombatAbility_GetProperty");
    AdvCombatAbility *rAbility = (AdvCombatAbility *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Returns the internal name of the ability, which typically includes the job it is assigned to.
    if(!strcasecmp(rSwitchType, "Internal Name") && tArgs == 1)
    {
        lua_pushstring(L, rAbility->GetInternalName());
        tReturns = 1;
    }
    //--ID of owning entity.
    else if(!strcasecmp(rSwitchType, "Owner ID") && tArgs == 1)
    {
        AdvCombatEntity *rOwner = rAbility->GetOwner();
        if(rOwner)
        {
            lua_pushinteger(L, rOwner->GetID());
        }
        else
        {
            lua_pushinteger(L, 0);
        }
        tReturns = 1;
    }
    //--Cooldown on the ability.
    else if(!strcasecmp(rSwitchType, "Cooldown") && tArgs == 1)
    {
        lua_pushinteger(L, rAbility->GetCooldown());
        tReturns = 1;
    }
    //--How many times the given tag exists.
    else if(!strcasecmp(rSwitchType, "Tag Count") && tArgs == 2)
    {
        lua_pushinteger(L, rAbility->GetTagCount(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--True if the owner of this ability is the acting entity.
    else if(!strcasecmp(rSwitchType, "Is Owner Acting") && tArgs == 1)
    {
        AdvCombatEntity *rOwner = rAbility->GetOwner();
        AdvCombatEntity *rActingEntity = AdvCombat::Fetch()->GetActingEntity();
        if(!rOwner || !rActingEntity)
        {
            lua_pushboolean(L, false);
        }
        else
        {
            lua_pushboolean(L, rOwner == (rActingEntity));
        }
        tReturns = 1;
    }
    ///--[Error Case]
    else
    {
        LuaPropertyError("AdvCombatAbility_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombatAbility_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatAbility_SetProperty("Script Path", sPath)
    //AdvCombatAbility_SetProperty("Script Response", iCode, bFlag)
    //AdvCombatAbility_SetProperty("Push Owner") (Pushes Activity Stack)
    //AdvCombatAbility_SetProperty("Usable", bFlag)
    //AdvCombatAbility_SetProperty("Equippable", bFlag)
    //AdvCombatAbility_SetProperty("Cooldown", iTurns)

    //--[Special Flags]
    //AdvCombatAbility_SetProperty("Requires Target", bFlag)
    //AdvCombatAbility_SetProperty("Opens Confirmation Window", bFlag, sConfirmationText)

    //--[Tags]
    //AdvCombatAbility_SetProperty("Add Tag", sTagName, iCount)
    //AdvCombatAbility_SetProperty("Remove Tag", sTagName, iCount)

    //--[Display]
    //AdvCombatAbility_SetProperty("Display Name", sName)
    //AdvCombatAbility_SetProperty("Icon Back", sDLPath)
    //AdvCombatAbility_SetProperty("Icon Frame", sDLPath)
    //AdvCombatAbility_SetProperty("Icon", sDLPath)
    //AdvCombatAbility_SetProperty("CP Icon", sDLPath)

    //--[Description]
    //AdvCombatAbility_SetProperty("Description", sDescription)
    //AdvCombatAbility_SetProperty("Allocate Description Images", iCount)
    //AdvCombatAbility_SetProperty("Description Image", iSlot, fYOffset, sDLPath)
    //AdvCombatAbility_SetProperty("Crossload Description Images")

    //--[Event Response]
    //AdvCombatAbility_SetProperty("Enqueue This Ability As Event", iPriority)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatAbility_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1000)
    {
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATABILITY)) return LuaTypeError("AdvCombatAbility_SetProperty");
    AdvCombatAbility *rAbility = (AdvCombatAbility *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Sets the path this ability uses to perform logic.
    if(!strcasecmp(rSwitchType, "Script Path") && tArgs == 2)
    {
        rAbility->SetScriptPath(lua_tostring(L, 2));
    }
    //--Sets whether or not the given script response code fires.
    else if(!strcasecmp(rSwitchType, "Script Response") && tArgs == 3)
    {
        rAbility->SetScriptResponse(lua_tointeger(L, 2), lua_toboolean(L, 3));
    }
    //--Pushes the entity that owns this ability.
    else if(!strcasecmp(rSwitchType, "Push Owner") && tArgs == 1)
    {
        AdvCombatEntity *rCaller = rAbility->GetOwner();
        DataLibrary::Fetch()->PushActiveEntity(rCaller);
    }
    //--Whether or not the ability can be used right now. Often ignored by AIs.
    else if(!strcasecmp(rSwitchType, "Usable") && tArgs == 2)
    {
        rAbility->SetUsable(lua_toboolean(L, 2));
    }
    //--Some abilities, like those that unlock field abilities, can never be equipped.
    else if(!strcasecmp(rSwitchType, "Equippable") && tArgs == 2)
    {
        rAbility->SetEquippable(lua_toboolean(L, 2));
    }
    //--Cooldown value.
    else if(!strcasecmp(rSwitchType, "Cooldown") && tArgs == 2)
    {
        rAbility->SetCooldown(lua_tointeger(L, 2));
    }
    ///--[Special Flags]
    //--Whether or not the ability explicitly requires a target to execute. Only used for system abilities
    //  like Retreat/Surrender.
    else if(!strcasecmp(rSwitchType, "Requires Target") && tArgs == 2)
    {
        rAbility->SetNeedsTarget(lua_toboolean(L, 2));
    }
    //--If the ability opens a confirmation window, and what the text should be on that window.
    else if(!strcasecmp(rSwitchType, "Opens Confirmation Window") && tArgs == 3)
    {
        rAbility->SetActivatesConfirmation(lua_toboolean(L, 2), lua_tostring(L, 3));
    }
    ///--[Tags]
    //--Adds the requested tag the requested number of times.
    else if(!strcasecmp(rSwitchType, "Add Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rAbility->AddTag(lua_tostring(L, 2));
    }
    //--Removes the requested tag the requested number of times.
    else if(!strcasecmp(rSwitchType, "Remove Tag") && tArgs == 3)
    {
        int tCount = lua_tointeger(L, 3);
        for(int i = 0; i < tCount; i ++) rAbility->RemoveTag(lua_tostring(L, 2));
    }
    ///--[Display]
    //--Name that appears on the UI.
    else if(!strcasecmp(rSwitchType, "Display Name") && tArgs == 2)
    {
        rAbility->SetDisplayName(lua_tostring(L, 2));
    }
    //--DLPath of the backing behind the icon.
    else if(!strcasecmp(rSwitchType, "Icon Back") && tArgs == 2)
    {
        rAbility->SetIconBack(lua_tostring(L, 2));
    }
    //--Frame around the icon.
    else if(!strcasecmp(rSwitchType, "Icon Frame") && tArgs == 2)
    {
        rAbility->SetIconFrame(lua_tostring(L, 2));
    }
    //--DLPath of the icon that appears in combat.
    else if(!strcasecmp(rSwitchType, "Icon") && tArgs == 2)
    {
        rAbility->SetIcon(lua_tostring(L, 2));
    }
    //--CP icon that appears over everything else.
    else if(!strcasecmp(rSwitchType, "CP Icon") && tArgs == 2)
    {
        rAbility->SetCPIcon(lua_tostring(L, 2));
    }
    ///--[Description]
    //--Description lines in-combat. Supports in-stride images.
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 2)
    {
        rAbility->SetDescription(lua_tostring(L, 2));
    }
    //--Set how many images are in the description.
    else if(!strcasecmp(rSwitchType, "Allocate Description Images") && tArgs == 2)
    {
        rAbility->AllocateDescriptionImages(lua_tointeger(L, 2));
    }
    //--Sets the image in the given slot.
    else if(!strcasecmp(rSwitchType, "Description Image") && tArgs == 4)
    {
        rAbility->SetDescriptionImage(lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tostring(L, 4));
    }
    //--Call once images are set and the string is parsed to set image pointers.
    else if(!strcasecmp(rSwitchType, "Crossload Description Images") && tArgs == 1)
    {
        rAbility->CrossreferenceDescriptionImages();
    }
    ///--[Event Response]
    else if(!strcasecmp(rSwitchType, "Enqueue This Ability As Event") && tArgs == 2)
    {
        rAbility->EnqueueAsEvent(lua_tointeger(L, 2));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAbility_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
