//--Base
#include "AdvCombatAbility.h"

//--Classes
#include "AdvCombat.h"

//--CoreClasses
#include "StarlightString.h"
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
AdvCombatAbility::AdvCombatAbility()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATABILITY;

    //--[AdvCombatAbility]
    //--System
    mLocalName = InitializeString("Null");
    mDisplayName = InitializeString("Null");
    mScriptPath = NULL;
    memset(mScriptResponses, 0, sizeof(mScriptResponses));
    mCannotBeEquipped = false;
    mIsEquipped = false;
    mHasInternalVersion = ADVCA_HAS_NOT_RESOLVED;

    //--Usage
    mIsUsableNow = false;
    mRequiresTarget = true;
    mOpensConfirmation = false;
    mCooldown = 0;

    //--Confirmation
    mConfirmationText = NULL;

    //--Display
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;
    rIconBack = NULL;
    rIconFrame = NULL;
    rIcon = NULL;
    rCPIcon = NULL;

    //--Storage
    mIsUnlocked = false;
    mJPCost = 0;
    rOwningEntity = NULL;
    mTagList = new SugarLinkedList(true);
}
AdvCombatAbility::~AdvCombatAbility()
{
    free(mLocalName);
    free(mDisplayName);
    free(mScriptPath);
    free(mConfirmationText);
    for(int i = 0; i < mDescriptionLinesTotal; i ++) delete mDescriptionLines[i];
    free(mDescriptionLines);
    delete mTagList;
}

//--[Private Statics]
//--Uses for enqueuing sound effects.
char AdvCombatAbility::xSoundEffectQueue[STD_MAX_LETTERS];
int AdvCombatAbility::xSoundEffectPriority = -1;

//====================================== Property Queries =========================================
const char *AdvCombatAbility::GetInternalName()
{
    return mLocalName;
}
const char *AdvCombatAbility::GetDisplayName()
{
    return mDisplayName;
}
const char *AdvCombatAbility::GetScriptPath()
{
    return mScriptPath;
}
bool AdvCombatAbility::IsUsableNow()
{
    return mIsUsableNow;
}
bool AdvCombatAbility::NeedsTargetCluster()
{
    return mRequiresTarget;
}
bool AdvCombatAbility::ActivatesConfirmation()
{
    return mOpensConfirmation;
}
int AdvCombatAbility::GetCooldown()
{
    return mCooldown;
}
char *AdvCombatAbility::GetConfirmationText()
{
    return mConfirmationText;
}
int AdvCombatAbility::GetDescriptionLinesTotal()
{
    return mDescriptionLinesTotal;
}
StarlightString *AdvCombatAbility::GetDescriptionLine(int pSlot)
{
    if(pSlot < 0 || pSlot >= mDescriptionLinesTotal) return NULL;
    return mDescriptionLines[pSlot];
}
SugarBitmap *AdvCombatAbility::GetIconBack()
{
    return rIconBack;
}
SugarBitmap *AdvCombatAbility::GetIconFrame()
{
    return rIconFrame;
}
SugarBitmap *AdvCombatAbility::GetIcon()
{
    return rIcon;
}
SugarBitmap *AdvCombatAbility::GetCPIcon()
{
    return rCPIcon;
}
bool AdvCombatAbility::IsUnlocked()
{
    return mIsUnlocked;
}
int AdvCombatAbility::GetJPCost()
{
    return mJPCost;
}
bool AdvCombatAbility::IsEquipped()
{
    return mIsEquipped;
}
bool AdvCombatAbility::CanBeEquipped()
{
    return !mCannotBeEquipped;
}
int AdvCombatAbility::GetTagCount(const char *pTag)
{
    if(!pTag) return 0;
    int *rTag = (int *)mTagList->GetElementByName(pTag);
    if(!rTag) return 0;
    return (*rTag);
}
int AdvCombatAbility::HasInternalVersion()
{
    return mHasInternalVersion;
}

//========================================= Manipulators ==========================================
void AdvCombatAbility::SetOwner(AdvCombatEntity *pOwner)
{
    rOwningEntity = pOwner;
}
void AdvCombatAbility::SetInternalName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void AdvCombatAbility::SetDisplayName(const char *pName)
{
    if(!pName) return;
    ResetString(mDisplayName, pName);
}
void AdvCombatAbility::SetScriptPath(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}
void AdvCombatAbility::SetScriptResponse(int pIndex, bool pFlag)
{
    if(pIndex < 0 || pIndex >= ACA_SCRIPT_CODE_TOTAL) return;
    mScriptResponses[pIndex] = pFlag;
}
void AdvCombatAbility::SetUsable(bool pIsUsable)
{
    mIsUsableNow = pIsUsable;
}
void AdvCombatAbility::SetEquippable(bool pIsEquippable)
{
    mCannotBeEquipped = !pIsEquippable;
}
void AdvCombatAbility::SetNeedsTarget(bool pNeedsTarget)
{
    mRequiresTarget = pNeedsTarget;
}
void AdvCombatAbility::SetActivatesConfirmation(bool pFlag, const char *pConfirmationString)
{
    mOpensConfirmation = pFlag;
    ResetString(mConfirmationText, pConfirmationString);
}
void AdvCombatAbility::SetDescription(const char *pDescription)
{
    //--Deallocate.
    for(int i = 0; i < mDescriptionLinesTotal; i ++) delete mDescriptionLines[i];
    free(mDescriptionLines);

    //--Zero.
    mDescriptionLinesTotal = 0;
    mDescriptionLines = NULL;
    if(!pDescription) return;

    //--Find out how many line-breaks there are in the description.
    int tLinesNeeded = 1;
    int cLen = (int)strlen(pDescription);
    for(int i = 0; i < cLen; i ++)
    {
        if(pDescription[i] == '\n' || pDescription[i] == 10 || pDescription[i] == 13)
        {
            tLinesNeeded ++;
        }
    }

    //--Allocate space.
    SetMemoryData(__FILE__, __LINE__);
    mDescriptionLinesTotal = tLinesNeeded;
    mDescriptionLines = (StarlightString **)starmemoryalloc(sizeof(StarlightString *) * mDescriptionLinesTotal);
    for(int i = 0; i < mDescriptionLinesTotal; i ++) mDescriptionLines[i] = NULL;

    //--Iterate, populate.
    char tCurBuf[1024];
    int c = 0;
    int p = 0;
    for(int i = 0; i < cLen; i ++)
    {
        //--On a line break, end the string and create a new one.
        if(pDescription[i] == '\n' || pDescription[i] == 10 || pDescription[i] == 13)
        {
            //--Error check:
            if(p >= mDescriptionLinesTotal) break;

            //--Set.
            mDescriptionLines[p] = new StarlightString();
            mDescriptionLines[p]->SetString(tCurBuf);

            //--Reset string values.
            c = 0;
            p ++;
            tCurBuf[0] = '\0';
        }
        //--Copy across.
        else
        {
            tCurBuf[c+0] = pDescription[i];
            tCurBuf[c+1] = '\0';
            c ++;
        }
    }

    //--If there's anything in the buffer, put it in the last string.
    if(tCurBuf[0] != '\0' && p < mDescriptionLinesTotal)
    {
        mDescriptionLines[p] = new StarlightString();
        mDescriptionLines[p]->SetString(tCurBuf);
    }
}
void AdvCombatAbility::AllocateDescriptionImages(int pImages)
{
    for(int i = 0; i < mDescriptionLinesTotal; i ++)
    {
        if(mDescriptionLines[i]) mDescriptionLines[i]->AllocateImages(pImages);
    }
}
void AdvCombatAbility::SetDescriptionImage(int pSlot, float pYOffset, const char *pDLPath)
{
    for(int i = 0; i < mDescriptionLinesTotal; i ++)
    {
        if(mDescriptionLines[i]) mDescriptionLines[i]->SetImageS(pSlot, pYOffset, pDLPath);
    }
}
void AdvCombatAbility::CrossreferenceDescriptionImages()
{
    for(int i = 0; i < mDescriptionLinesTotal; i ++)
    {
        if(mDescriptionLines[i]) mDescriptionLines[i]->CrossreferenceImages();
    }
}
void AdvCombatAbility::SetIconBack(const char *pDLPath)
{
    rIconBack = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatAbility::SetIconFrame(const char *pDLPath)
{
    rIconFrame = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatAbility::SetIcon(const char *pDLPath)
{
    rIcon = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatAbility::SetCPIcon(const char *pDLPath)
{
    rCPIcon = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatAbility::SetUnlocked(bool pFlag)
{
    mIsUnlocked = pFlag;
}
void AdvCombatAbility::SetJPCost(int pCost)
{
    mJPCost = pCost;
}
void AdvCombatAbility::SetEquipped(bool pFlag)
{
    mIsEquipped = pFlag;
}
void AdvCombatAbility::SetCooldown(int pTurns)
{
    mCooldown = pTurns;
}
void AdvCombatAbility::AddTag(const char *pTag)
{
    if(!pTag) return;
    int *rTag = (int *)mTagList->GetElementByName(pTag);
    if(rTag)
    {
        (*rTag) = (*rTag) + 1;
    }
    else
    {
        int *nTag = (int *)starmemoryalloc(sizeof(int));
        *nTag = 1;
        mTagList->AddElement(pTag, nTag, &FreeThis);
    }
}
void AdvCombatAbility::RemoveTag(const char *pTag)
{
    if(!pTag) return;
    int *rTag = (int *)mTagList->GetElementByName(pTag);
    if(!rTag) return;

    if(*rTag > 1)
    {
        (*rTag) = (*rTag) - 1;
    }
    else
    {
        mTagList->RemoveElementS(pTag);
    }
}
void AdvCombatAbility::SetInternalVersionFlag(int pFlag)
{
    mHasInternalVersion = pFlag;
}

//========================================= Core Methods ==========================================
void AdvCombatAbility::CallCode(int pCode)
{
    //--Script path must exist.
    if(!mScriptPath) return;

    //--Code must be within range.
    if(pCode < 0 || pCode >= ACA_SCRIPT_CODE_TOTAL) return;

    //--Code must be toggled true.
    if(!mScriptResponses[pCode]) return;

    //--Fire!
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)pCode);
}
void AdvCombatAbility::EnqueueAsEvent(int pPriority)
{
    //--Called during ACA_SCRIPT_CODE_EVENT_QUEUED as a response, this causes the ability to enqueue itself as
    //  an event with the given priority. When doing this, the zeroth target cluster is the one selected.
    //--Priority indicates where in the event queue the ability needs to be. 0 is the ability that is active,
    //  negative is before it, positive is after it.
    CombatEventPack *nPackage = (CombatEventPack *)starmemoryalloc(sizeof(CombatEventPack));
    nPackage->Initialize();
    nPackage->mPriority = pPriority;
    nPackage->rAbility = this;
    nPackage->rOriginator = rOwningEntity;

    //--Order combat to clear target clusters.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    rAdventureCombat->ClearTargetClusters();

    //--Fire target painting. Always use the zeroth target cluster. If somehow there was no cluster, don't enqueue the event.
    CallCode(ACA_SCRIPT_CODE_PAINTTARGETS_RESPONSE);
    TargetCluster *rZeroCluster = rAdventureCombat->GetTargetClusterI(0);
    if(!rZeroCluster && NeedsTargetCluster())
    {
        CombatEventPack::DeleteThis(nPackage);
        return;
    }

    //--Clone the target cluster.
    nPackage->mTargetCluster = rZeroCluster->Clone();

    //--Enqueue the event. Response events cannot fire additional events.
    rAdventureCombat->EnqueueEvent(nPackage);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
AdvCombatEntity *AdvCombatAbility::GetOwner()
{
    //--Gets the entity that owns this ability. Only populated during script calls, otherwise returns
    //  NULL. Assume this pointer is unstable.
    return rOwningEntity;
}

//====================================== Static Functions =========================================
void AdvCombatAbility::HandleCombatActionSounds()
{
    //--Static function, called at the end of the tick. Whichever sound effect had the highest priority
    //  will play. If many sound effects trigger from actions at the same time, this prevents a lot of overlay.
    //  Keeps splash-damage attacks from hurting your ears.
    if(xSoundEffectPriority == -1) return;

    //--Play the sound effect if the priority was not -1.
    AudioManager::Fetch()->PlaySound(xSoundEffectQueue);

    //--Reset the data. If no new effect is set to play, the -1 code will be ignored.
    xSoundEffectPriority = -1;
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
