//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

//--[Sorter Function]
int CompareCombatEventPacks(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used by SortList() below. This one is Z to A.
    SugarLinkedListEntry **rEntryA = (SugarLinkedListEntry **)pEntryA;
    SugarLinkedListEntry **rEntryB = (SugarLinkedListEntry **)pEntryB;

    //--Cast to CombatEventPackage.
    CombatEventPack *rPackageA = (CombatEventPack *)(*rEntryA)->rData;
    CombatEventPack *rPackageB = (CombatEventPack *)(*rEntryB)->rData;

    return (rPackageA->mPriority - rPackageB->mPriority);
}

void AdvCombat::MovePlayerHighlightTo(int pX, int pY)
{
    //--Moves the highlight position to the marked slot.
    float cXPos = ADVCOMBAT_POSITION_ABILITY_X + (pX * ADVCOMBAT_POSITION_ABILITY_W);
    float cYPos = ADVCOMBAT_POSITION_ABILITY_Y + (pY * ADVCOMBAT_POSITION_ABILITY_H);
    mAbilityHighlightPack.MoveTo(cXPos, cYPos, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
}
#include "Global.h"
void AdvCombat::HandlePlayerControls()
{
    ///--[Documentation and Setup]
    //--Handles player controls when an entity with no AI is acting.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get the acting entity.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity) return;

    ///--[Timers]
    //--Player interface timer runs to ADVCOMBAT_MOVE_TRANSITION_TICKS. This causes it to slide up.
    if(mPlayerInterfaceTimer < ADVCOMBAT_MOVE_TRANSITION_TICKS)
    {
        //--Check the events list. If there are any events queued, don't do this.
        if(mEventQueue->GetListSize() < 1 && mApplicationQueue->GetListSize() < 1) mPlayerInterfaceTimer ++;
    }

    ///--[First tick]
    //--If this is the first tick that the player has gained control, we need to reposition the ability
    //  selection. It is implied, as this is the first tick, that target selection is not occurring.
    if(mActionFirstTick)
    {
        //--Flag.
        mActionFirstTick = false;

        //--Reposition the ability highlight.
        mAbilitySelectionX = 0;
        mAbilitySelectionY = 0;
        mAbilityHighlightPack.MoveTo(ADVCOMBAT_POSITION_ABILITY_X, ADVCOMBAT_POSITION_ABILITY_Y, 0);
    }

    //--[Debug]
    //--If the player presses the 9 key, they win! This is for debug.
    if(rControlManager->IsFirstPress("Key_9"))
    {
        BeginResolutionSequence(ADVCOMBAT_END_VICTORY);
    }

    //--[Combat Inspector]
    //--Can be triggered at any time during the player's turn.
    if(rControlManager->IsFirstPress("F1"))
    {
        AudioManager::Fetch()->PlaySound("Menu|Select");
        ActivateInspector();
        return;
    }

    //--[Fill MP]
    //--Debug command.
    if(rControlManager->IsFirstPress("F2"))
    {
        AudioManager::Fetch()->PlaySound("Menu|Select");
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
        while(rEntity)
        {
            rEntity->SetMagic(rEntity->GetStatistic(STATS_MPMAX));
            rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
        }
        return;
    }

    ///--[Ability Selection]
    //--Selecting which ability to use this action.
    if(!mIsSelectingTargets)
    {
        //--[Primary Ability Sheet]
        //--This sheet usually contains a character's abilities.
        if(!mShowingSecondPage)
        {
            //--[Activate]
            //--Pressing the activate key typically goes into target selection. Some abilities, like retreat
            //  or surrender, may bring up confirmation windows instead.
            if(rControlManager->IsFirstPress("Activate"))
            {
                //--Get the ability in question. If it doesn't exist, play the failure sound.
                AdvCombatAbility *rAbility = rActingEntity->GetAbilityBySlot(mAbilitySelectionX, mAbilitySelectionY);
                if(!rAbility)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
                //--If the ability exists, but is unusuable, play the failure sound.
                else if(!rAbility->IsUsableNow())
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
                //--If the ability opens a confirmation window without painting targets, do there here:
                else if(rAbility->ActivatesConfirmation())
                {
                    ActivateConfirmationWindow();
                    SetConfirmationText(rAbility->GetConfirmationText());
                    SetConfirmationFunc(&AdvCombat::ConfirmExecuteAbility);
                    rActiveAbility = rAbility;
                    rActiveTargetCluster = NULL;
                }
                //--The ability exists and is usable, but does not need confirmation. Fire its target painting script.
                else
                {
                    //--Execute the script.
                    ClearTargetClusters();
                    mTargetClusterCursor = 0;
                    mTargetFlashTimer = 0;
                    rAbility->CallCode(ACA_SCRIPT_CODE_PAINTTARGETS);

                    //--If the script didn't paint any targets, fail.
                    if(mTargetClusters->GetListSize() < 1)
                    {
                        AudioManager::Fetch()->PlaySound("Menu|Failed");
                    }
                    //--Script painted at least one target cluster, switch to targeting mode.
                    else
                    {
                        //--Flag, storage.
                        mIsSelectingTargets = true;
                        rActiveAbility = rAbility;
                        rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);

                        //--Build prediction boxes.
                        mIsClearingPredictionBoxes = false;
                        BuildPredictionBoxes();
                        RunPredictionBoxCalculations();
                        SetClusterAsActive(mTargetClusterCursor);

                        //--Move party.
                        MovePartyByActiveCluster();

                        //--SFX.
                        AudioManager::Fetch()->PlaySound("Menu|Select");
                    }
                }
            }

            //--[Page Swap]
            if(rControlManager->IsFirstPress("DnLevel"))
            {
                mShowingSecondPage = true;
                mStoredAbilitySelectionX = mAbilitySelectionX;
                mStoredAbilitySelectionY = mAbilitySelectionY;
                mAbilitySelectionX = 0;
                mAbilitySelectionY = 0;
                AudioManager::Fetch()->PlaySound("Menu|Select");
                return;
            }

            //--[Cancel]
            //--Pressing cancel attempts to pass the player's turn. This brings up a confirmation window.
            if(rControlManager->IsFirstPress("Cancel"))
            {
            }

            //--[Directional Keys]
            //--Directional keys change where the ability highlight is.
            bool tRepositionHighlight = false;
            if(rControlManager->IsFirstPress("Left"))
            {
                tRepositionHighlight = true;
                mAbilitySelectionX --;
                if(mAbilitySelectionX < 0) mAbilitySelectionX = ACE_ABILITY_GRID_SIZE_X - 1;
            }
            if(rControlManager->IsFirstPress("Right"))
            {
                tRepositionHighlight = true;
                mAbilitySelectionX ++;
                if(mAbilitySelectionX >= ACE_ABILITY_GRID_SIZE_X) mAbilitySelectionX = 0;
            }
            if(rControlManager->IsFirstPress("Up"))
            {
                tRepositionHighlight = true;
                mAbilitySelectionY --;
                if(mAbilitySelectionY < 0) mAbilitySelectionY = ACE_ABILITY_GRID_SIZE_Y - 1;
            }
            if(rControlManager->IsFirstPress("Down"))
            {
                tRepositionHighlight = true;
                mAbilitySelectionY ++;
                if(mAbilitySelectionY >= ACE_ABILITY_GRID_SIZE_Y) mAbilitySelectionY = 0;
            }

            //--If this flag was set, we need to reposition the highlight.
            if(tRepositionHighlight)
            {
                MovePlayerHighlightTo(mAbilitySelectionX, mAbilitySelectionY);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--[Secondary Ability Sheet]
        //--Usually contains job changes and/or toggles.
        else
        {
            //--[Activate]
            //--Pressing the activate key typically goes into target selection. Some abilities, like retreat
            //  or surrender, may bring up confirmation windows instead.
            if(rControlManager->IsFirstPress("Activate"))
            {
                //--Get the ability in question. If it doesn't exist, play the failure sound.
                AdvCombatAbility *rAbility = rActingEntity->GetSecondaryBySlot(mAbilitySelectionX, mAbilitySelectionY);
                if(!rAbility)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
                //--If the ability exists, but is unusuable, play the failure sound.
                else if(!rAbility->IsUsableNow())
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
                //--If the ability opens a confirmation window without painting targets, do there here:
                else if(rAbility->ActivatesConfirmation())
                {
                    ActivateConfirmationWindow();
                    SetConfirmationText(rAbility->GetConfirmationText());
                    SetConfirmationFunc(&AdvCombat::ConfirmExecuteAbility);
                    rActiveAbility = rAbility;
                    rActiveTargetCluster = NULL;
                }
                //--The ability exists and is usable, but does not need confirmation. Fire its target painting script.
                else
                {
                    //--Execute the script.
                    ClearTargetClusters();
                    mTargetClusterCursor = 0;
                    mTargetFlashTimer = 0;
                    rAbility->CallCode(ACA_SCRIPT_CODE_PAINTTARGETS);

                    //--If the script didn't paint any targets, fail.
                    if(mTargetClusters->GetListSize() < 1)
                    {
                        AudioManager::Fetch()->PlaySound("Menu|Failed");
                    }
                    //--Script painted at least one target cluster, switch to targeting mode.
                    else
                    {
                        //--Flag, storage.
                        mIsSelectingTargets = true;
                        rActiveAbility = rAbility;
                        rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);

                        //--Build prediction boxes.
                        mIsClearingPredictionBoxes = false;
                        BuildPredictionBoxes();
                        RunPredictionBoxCalculations();
                        SetClusterAsActive(mTargetClusterCursor);

                        //--Move party.
                        MovePartyByActiveCluster();

                        //--SFX.
                        AudioManager::Fetch()->PlaySound("Menu|Select");
                    }
                }
            }

            //--[Page Swap]
            if(rControlManager->IsFirstPress("DnLevel") || rControlManager->IsFirstPress("Cancel"))
            {
                mShowingSecondPage = false;
                mAbilitySelectionX = mStoredAbilitySelectionX;
                mAbilitySelectionY = mStoredAbilitySelectionY;
                AudioManager::Fetch()->PlaySound("Menu|Select");
                return;
            }

            //--[Directional Keys]
            //--Directional keys change where the ability highlight is.
            bool tRepositionHighlight = false;
            if(rControlManager->IsFirstPress("Left"))
            {
                tRepositionHighlight = true;
                mAbilitySelectionX --;
                if(mAbilitySelectionX < 0) mAbilitySelectionX = ACE_ABILITY_GRID_SIZE_X - 1;
            }
            if(rControlManager->IsFirstPress("Right"))
            {
                tRepositionHighlight = true;
                mAbilitySelectionX ++;
                if(mAbilitySelectionX >= ACE_ABILITY_GRID_SIZE_X) mAbilitySelectionX = 0;
            }
            if(rControlManager->IsFirstPress("Up"))
            {
                tRepositionHighlight = true;
                mAbilitySelectionY --;
                if(mAbilitySelectionY < 0) mAbilitySelectionY = ACE_ABILITY_GRID_SIZE_Y - 1;
            }
            if(rControlManager->IsFirstPress("Down"))
            {
                tRepositionHighlight = true;
                mAbilitySelectionY ++;
                if(mAbilitySelectionY >= ACE_ABILITY_GRID_SIZE_Y) mAbilitySelectionY = 0;
            }

            //--If this flag was set, we need to reposition the highlight.
            if(tRepositionHighlight)
            {
                MovePlayerHighlightTo(mAbilitySelectionX, mAbilitySelectionY);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
    }
    ///--[Target Selection]
    //--Selecting which target to execute the ability on.
    else
    {
        //--Activate, executes action on target cluster.
        if(rControlManager->IsFirstPress("Activate"))
        {
            mIsSelectingTargets = false;
            rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
            MovePartyByActiveCluster();
            ExecuteActiveAbility();
            ClearPredictionBoxes();
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Cancel, exits target mode.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsSelectingTargets = false;
            rActiveTargetCluster = NULL;
            MovePartyByActiveCluster();
            ClearPredictionBoxes();
            AudioManager::Fetch()->PlaySound("Menu|Select");
            return;
        }

        //--Storage.
        int tOldTargetCluster = mTargetClusterCursor;

        //--Decrements target cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            mTargetClusterCursor --;
            if(mTargetClusterCursor < 0) mTargetClusterCursor = mTargetClusters->GetListSize() - 1;
        }
        //--Increments target cursor.
        if(rControlManager->IsFirstPress("Right"))
        {
            mTargetClusterCursor ++;
            if(mTargetClusterCursor >= mTargetClusters->GetListSize()) mTargetClusterCursor = 0;
        }

        //--If this flag was set, we need to reposition the highlight.
        if(mTargetClusterCursor != tOldTargetCluster)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Move entities around.
            rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
            MovePartyByActiveCluster();
            SetClusterAsActive(mTargetClusterCursor);
        }
    }
}
void AdvCombat::ExecuteAbility(AdvCombatAbility *pAbility, TargetCluster *pTargetCluster)
{
    //--Executes the given ability on the given target cluster. The target cluster can be NULL for some
    //  abilities, but the ability should always be non-NULL.
    //--This enqueues the event that will hold the ability. We also need to check if any entities
    //  are going to "respond" to this event, which they may do before or after. They do this by
    //  enqueuing their own events.
    if(!pAbility)
    {
        return;
    }

    //--Ask the ability if it needs a target cluster. If it does, and the cluster is NULL, fail.
    if(pAbility->NeedsTargetCluster() && !pTargetCluster)
    {
        return;
    }

    //--Flag.
    mShowingSecondPage = false;

    //--Create a package around the ability.
    CombatEventPack *nPackage = (CombatEventPack *)starmemoryalloc(sizeof(CombatEventPack));
    nPackage->Initialize();
    nPackage->rAbility = pAbility;
    nPackage->rOriginator = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);

    //--We need to clone the target cluster as it may get cleared if there are other events enqueued.
    if(pTargetCluster) nPackage->mTargetCluster = pTargetCluster->Clone();
    ClearTargetClusters();

    //--Enqueue. The rEventZeroPackage is used by abilities querying for responses.
    rEventZeroPackage = nPackage;
    EnqueueEvent(nPackage);

    //--Set this flag. This means the character who is acting has performed their action. This prevents
    //  the event queue from ending their turn if another event fired, such as a passive effect firing an event.
    mNoEventsEndsAction = true;

    //--Fire response handlers.
    RunAllResponseScripts(ADVCOMBAT_RESPONSE_QUEUEEVENT);

    //--Once all response events are assembled, sort the event queue by priority.
    mEventQueue->SortListUsing(&CompareCombatEventPacks);

    //--Clear the zero package.
    rEventZeroPackage = NULL;

    //--Player, if acting, is no longer acting.
    mIsPerformingPlayerUpdate = false;
}
void AdvCombat::ExecuteActiveAbility()
{
    //--Executes the ability stored in the rConfirmationAbility. Typically called from outside the class
    //  by a static function after the player presses the confirmation button.
    ExecuteAbility(rActiveAbility, rActiveTargetCluster);
    rActiveAbility = NULL;
    rActiveTargetCluster = NULL;
}
