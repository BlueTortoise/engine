//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"

void AdvCombat::RegisterEffect(AdvCombatEffect *pEffect)
{
    //--Duplicates aren't allowed.
    if(mGlobalEffectList->IsElementOnList(pEffect)) return;

    //--Add it, flag the list to rebuild.
    mMustRebuildEffectReferences = true;
    mGlobalEffectList->AddElementAsTail("X", pEffect, &RootObject::DeleteThis);
}
void AdvCombat::RebuildEffectReferences()
{
    //--There is a global list of effects, and a global list of entities. Entities show which effects
    //  are impacting them on their UI, just above their HP. Because this is purely a visual thing,
    //  entities do not contain a robust list of effects. Instead, every time the effect list is modified,
    //  we rebuild which entities are affected by what.
    //--This is controlled by the variable mMustRebuildEffectReferences.
    if(!mMustRebuildEffectReferences) return;
    mMustRebuildEffectReferences = false;

    //--Iterate across all entities. Clear the effect render list for all of them.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mPartyRoster->PushIterator();
    while(rEntity)
    {
        rEntity->GetEffectRenderList()->ClearList();
        rEntity = (AdvCombatEntity *)mPartyRoster->AutoIterate();
    }
    rEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rEntity)
    {
        rEntity->GetEffectRenderList()->ClearList();
        rEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    //--Iterate across all effects.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->PushIterator();
    while(rEffect)
    {
        //--Run across all targets in the effect.
        SugarLinkedList *rTargetList = rEffect->GetTargetList();
        AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)rTargetList->PushIterator();
        while(rPackage)
        {
            //--Get the entity. If it's found, add the effect to the rendering list. Effects can never
            //  contain duplicate entries, so the entity always has unique effect entries as well.
            AdvCombatEntity *rEntity = GetEntityByID(rPackage->mTargetID);
            if(rEntity) rEntity->GetEffectRenderList()->AddElement("X", rEffect);

            //--Next.
            rPackage = (AdvCombatEffectTargetPack *)rTargetList->AutoIterate();
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->AutoIterate();
    }
}
SugarLinkedList *AdvCombat::GetEffectsReferencing(AdvCombatEntity *pEntity)
{
    //--Returns a SugarLinkedList containing all effects currently listing the given entity as a target. The list
    //  must be deallocated by the caller. The list can legally be empty.
    SugarLinkedList *nEffectList = new SugarLinkedList(false);
    if(!pEntity) return nEffectList;

    //--Iterate across all effects.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->PushIterator();
    while(rEffect)
    {
        //--Run across all targets in the effect.
        SugarLinkedList *rTargetList = rEffect->GetTargetList();
        AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)rTargetList->PushIterator();
        while(rPackage)
        {
            //--Get the entity pointer.
            AdvCombatEntity *rEntity = GetEntityByID(rPackage->mTargetID);

            //--Match, add to list.
            if(rEntity == pEntity)
            {
                nEffectList->AddElement("X", rEffect);
            }

            //--Next.
            rPackage = (AdvCombatEffectTargetPack *)rTargetList->AutoIterate();
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->AutoIterate();
    }

    //--Return the list.
    return nEffectList;
}
void AdvCombat::StoreEffectsReferencing(uint32_t pID)
{
    //--Clear.
    mrTempEffectList->ClearList();
    if(pID == 0) return;

    //--Iterate across all effects.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->PushIterator();
    while(rEffect)
    {
        //--Run across all targets in the effect.
        SugarLinkedList *rTargetList = rEffect->GetTargetList();
        AdvCombatEffectTargetPack *rPackage = (AdvCombatEffectTargetPack *)rTargetList->PushIterator();
        while(rPackage)
        {
            //--Match, add to list.
            if(rPackage->mTargetID == pID) mrTempEffectList->AddElement("X", rEffect);

            //--Next.
            rPackage = (AdvCombatEffectTargetPack *)rTargetList->AutoIterate();
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->AutoIterate();
    }
}
SugarLinkedList *AdvCombat::GetTemporaryEffectList()
{
    return mrTempEffectList;
}
void AdvCombat::ClearTemporaryEffectList()
{
    mrTempEffectList->ClearList();
}
void AdvCombat::MarkEffectForRemoval(uint32_t pID)
{
    //--Locate the specific effect and remove it.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->SetToHeadAndReturn();
    while(rEffect)
    {
        //--If the script flagged destruction for the effect, put it on the effect graveyard.
        if(rEffect->GetID() == pID)
        {
            //--Flag.
            mMustRebuildEffectReferences = true;

            //--Before removal, ask the effect to remove any stat modifications it made.
            rEffect->UnApplyStatsToTargets();

            //--Remove, place on graveyard.
            mGlobalEffectList->LiberateRandomPointerEntry();
            mEffectGraveyard->AddElement("X", rEffect, &RootObject::DeleteThis);
            break;
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->IncrementAndGetRandomPointerEntry();
    }

    //--Rebuild references.
    RebuildEffectReferences();
}
void AdvCombat::PulseEffectsForRemoval()
{
    //--Run across all effects and check any that need to be removed.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->SetToHeadAndReturn();
    while(rEffect)
    {
        //--If the script flagged destruction for the effect, put it on the effect graveyard.
        if(rEffect->IsRemovedNow())
        {
            //--Flag.
            mMustRebuildEffectReferences = true;

            //--Before removal, ask the effect to remove any stat modifications it made.
            rEffect->UnApplyStatsToTargets();

            //--Remove, place on graveyard.
            mGlobalEffectList->LiberateRandomPointerEntry();
            mEffectGraveyard->AddElement("X", rEffect, &RootObject::DeleteThis);
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->IncrementAndGetRandomPointerEntry();
    }

    //--Rebuild references.
    RebuildEffectReferences();
}
