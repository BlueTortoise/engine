//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatAnimation.h"
#include "AdvCombatDefStruct.h"
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "AdventureLevel.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"

//#define COMBAT_RENDER_DEBUG
#ifdef COMBAT_RENDER_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//========================================== Core Call ============================================
void AdvCombat::Render()
{
    ///--[Documentation and Setup]
    //--Renders the Adventure Combat interface. Unlike many other objects using the vis-timer format, Adventure Combat does not
    //  use a global mixer alpha.
    if(!Images.mIsReady || !mIsActive) return;
    DebugPush(true, "Combat Render: Begin.\n");

    //--If the world is still pulsing, don't render.
    DebugPrint("Checking Adventure Level.\n");
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel && rActiveLevel->IsPulsing()) { DebugPop("Level pulsing, exited normally.\n"); return; }

    ///--[Subhandlers]
    //--Subhandlers can take over rendering. First, combat is over:
    DebugPrint("Checking resolution/introduction states.\n");
    if(mCombatResolution != ADVCOMBAT_END_NONE) { RenderResolution();   DebugPop("Resolution running, exited normally.\n"); return; }
    if(mIsIntroduction)                         { RenderIntroduction(); DebugPop("Introduction running, exited normally.\n"); return; }

    //--Anything below this point is "Normal" combat rendering.

    ///--[List Building]
    //--This normally does nothing until the effect list changes, at which point it repopulates. The lists within are only
    //  used for rendering.
    DebugPrint("Rebuilding effect references.\n");
    RebuildEffectReferences();

    ///--[Grey Backing]
    //--Flat grey backing.
    DebugPrint("Backing.\n");
    SugarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);

    ///--[Entities]
    //--Render player entities.
    DebugPrint("Rendering player entities.\n");
    AdvCombatEntity *rPartyEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rPartyEntity)
    {
        RenderEntity(rPartyEntity);
        rPartyEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--Render enemy entities.
    DebugPrint("Rendering enemy entities.\n");
    AdvCombatEntity *rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEnemyEntity)
    {
        RenderEntity(rEnemyEntity);
        rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }

    ///--[UI]
    //--Component rendering.
    DebugPrint("Rendering UI.\n");
    RenderAllyBars();
    RenderTurnOrder();
    RenderNewTurn();
    RenderTargetCluster();

    //--Player's interface.
    DebugPrint("Rendering player interface.\n");
    if(mPlayerInterfaceTimer > 0)
    {
        //--Compute percent.
        float cPct = EasingFunction::QuadraticInOut(mPlayerInterfaceTimer, ADVCOMBAT_MOVE_TRANSITION_TICKS);

        //--Compute offset.
        float cMaxOffset = 300.0f;
        float cOffset = cMaxOffset * (1.0f - cPct);

        //--Translate, render.
        glTranslatef(0.0f, cOffset, 0.0f);
        RenderPlayerBar();
        glTranslatef(0.0f, -cOffset, 0.0f);
    }

    //--Render animations.
    DebugPrint("Rendering combat animations.\n");
    AdvCombatAnimation *rAnimation = (AdvCombatAnimation *)mCombatAnimations->PushIterator();
    while(rAnimation)
    {
        rAnimation->Render();
        rAnimation = (AdvCombatAnimation *)mCombatAnimations->AutoIterate();
    }

    //--Render the enemy UIs.
    DebugPrint("Rendering enemy UIs.\n");
    rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEnemyEntity)
    {
        RenderEntityUI(rEnemyEntity);
        rEnemyEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }

    //--Text package.
    DebugPrint("Rendering text packages.\n");
    float cCombatYDrift = -30.0f;
    CombatTextPack *rTextPack = (CombatTextPack *)mTextPackages->PushIterator();
    while(rTextPack)
    {
        //--Compute Y position.
        float cPercent = EasingFunction::QuadraticOut(rTextPack->mTicks, rTextPack->mTicksMax);
        float cYUsePos = rTextPack->mY + (cCombatYDrift * cPercent);

        //--Render.
        rTextPack->mColor.SetAsMixer();
        Images.Data.rCombatTextFont->DrawText(rTextPack->mX, cYUsePos, SUGARFONT_AUTOCENTER_XY, rTextPack->mScale, rTextPack->mText);

        //--Next.
        rTextPack = (CombatTextPack *)mTextPackages->AutoIterate();
    }
    StarlightColor::ClearMixer();

    //--Prediction boxes.
    DebugPrint("Rendering prediction boxes.\n");
    RenderPredictionBoxes();

    //--Titles.
    DebugPrint("Rendering titles.\n");
    RenderTitle();

    //--[Overlays]
    //--Dialogue, if present.
    DebugPrint("Rendering world dialogue.\n");
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue->IsVisible())
    {
        rWorldDialogue->Render();
    }

    //--Combat inspector.
    DebugPrint("Rendering inspector.\n");
    RenderInspector();

    //--Confirmation window overlay.
    DebugPrint("Rendering confirmation window.\n");
    RenderConfirmation();
    DebugPop("Exited normally.\n");
}

//======================================== Entity Render ==========================================
void AdvCombat::RenderEntity(AdvCombatEntity *pEntity)
{
    //--[Documentation and Setup]
    //--Given an entity, renders it at its combat position. Special effects are handled here. This only
    //  renders the combat portrait, it does not render and UI over the entity.
    if(!pEntity) return;

    //--Make sure there's a portrait to render.
    SugarBitmap *rCombatPortrait = pEntity->GetCombatPortrait();
    if(!rCombatPortrait) return;

    //--[Position Handling]
    //--Get positions.
    float cRenderX = pEntity->GetCombatX();
    float cRenderY = pEntity->GetCombatY();

    //--Center the frame by X position.
    cRenderX = cRenderX - (rCombatPortrait->GetTrueWidth()  * 0.50f);

    //--Recenter the Y position.
    float cBottomUpY = 670.0f;
    cRenderY = cBottomUpY - rCombatPortrait->GetTrueHeight();

    //--If the Y position is too far up, move it down a bit.
    if(cRenderY + rCombatPortrait->GetYOffset() < 100.0f) cRenderY = 100.0f - rCombatPortrait->GetYOffset();

    //--[Target Handling]
    //--If the entity is currently on the active target cluster, it will flash.
    if(mIsSelectingTargets && mTargetFlashTimer < ADVCOMBAT_TARGET_FLASH_PERIOD/2)
    {
        //--Make sure we're on the target cluster.
        TargetCluster *rActiveCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
        if(rActiveCluster)
        {
            //--In the cluster, set mixer to black.
            if(rActiveCluster->IsElementInCluster(pEntity))
            {
                StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f);
            }
        }
    }
    //--[Black Flash]
    //--Entities flash black when they are acting.
    else if(pEntity->IsFlashingBlack())
    {
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
    }
    //--[White Flash]
    //--Fades up to fullwhite using the stencil buffer.
    else if(pEntity->GetWhiteFlashTimer() > 0)
    {
        //--Timer.
        int tTimer = pEntity->GetWhiteFlashTimer();
        float cPercent = EasingFunction::QuadraticInOut(tTimer, ADVCE_FLASH_WHITE_TICKS);

        //--Normal image. Color mask stays on.
        DisplayManager::ActivateMaskRender(ACE_STENCIL_KNOCKOUT);
        glColorMask(true, true, true, true);
        rCombatPortrait->Draw(cRenderX, cRenderY);

        //--Render a white overlay.
        DisplayManager::ActivateStencilRender(ACE_STENCIL_KNOCKOUT);
        glDisable(GL_TEXTURE_2D);
        glColor4f(1.0f, 1.0f, 1.0f, cPercent);
        rCombatPortrait->Draw(cRenderX, cRenderY);

        //--Clean up.
        glEnable(GL_TEXTURE_2D);
        DisplayManager::DeactivateStencilling();
        StarlightColor::ClearMixer();
        return;
    }
    //--[Knockout]
    //--Suspends normal rendering.
    else if(pEntity->IsKnockingOut())
    {
        //--Timer.
        int tKnockoutTimer = pEntity->GetKnockoutTimer();

        //--Fading up to white. First, render the normal image and stencil.
        if(tKnockoutTimer < ADVCE_KNOCKOUT_TOWHITE_TICKS)
        {
            //--Normal image.
            DisplayManager::ActivateMaskRender(ACE_STENCIL_KNOCKOUT);
            rCombatPortrait->Draw(cRenderX, cRenderY);

            //--Render a white overlay.
            float cPercent = EasingFunction::QuadraticOut(tKnockoutTimer, ADVCE_KNOCKOUT_TOWHITE_TICKS);
            DisplayManager::ActivateStencilRender(ACE_STENCIL_KNOCKOUT);
            glDisable(GL_TEXTURE_2D);
            glColor4f(1.0f, 1.0f, 1.0f, cPercent);
            rCombatPortrait->Draw(cRenderX, cRenderY);

            //--Clean up.
            glEnable(GL_TEXTURE_2D);
            DisplayManager::DeactivateStencilling();
            StarlightColor::ClearMixer();
            return;
        }
        //--Fading down to black.
        else
        {
            //--Get percent.
            int tUseTimer = tKnockoutTimer - ADVCE_KNOCKOUT_TOWHITE_TICKS;

            //--Portrait, color mask is off.
            DisplayManager::ActivateMaskRender(ACE_STENCIL_KNOCKOUT);
            rCombatPortrait->Draw(cRenderX, cRenderY);

            //--Fullwhite.
            glDisable(GL_TEXTURE_2D);
            DisplayManager::ActivateStencilRender(ACE_STENCIL_KNOCKOUT);
            glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            rCombatPortrait->Draw(cRenderX, cRenderY);

            //--Render a black overlay.
            float cPercent = EasingFunction::QuadraticOut(tUseTimer, ADVCE_KNOCKOUT_TOWHITE_TICKS);
            glDisable(GL_TEXTURE_2D);
            glColor4f(0.0f, 0.0f, 0.0f, cPercent);
            rCombatPortrait->Draw(cRenderX, cRenderY);

            //--Clean up.
            glEnable(GL_TEXTURE_2D);
            DisplayManager::DeactivateStencilling();
            StarlightColor::ClearMixer();
            return;
        }
    }

    //--[Render]
    rCombatPortrait->Draw(cRenderX, cRenderY);

    //--[Clean]
    StarlightColor::ClearMixer();
}

//========================================== Entity UI ============================================
void AdvCombat::RenderEntityUI(AdvCombatEntity *pEntity)
{
    ///--[Documentation and Setup]
    //--Renders the UI over the entity, showing their HP bar and applied effects.
    if(!pEntity) return;

    //--Knockout: Causes a fade out of this object.
    float cAlpha = 1.0f;
    if(pEntity->IsKnockingOut())
    {
        //--Render a fadeout.
        int tKnockoutTimer = pEntity->GetKnockoutTimer();
        if(tKnockoutTimer < ADVCE_KNOCKOUT_TOWHITE_TICKS)
        {
            cAlpha = 1.0f - EasingFunction::QuadraticOut(tKnockoutTimer, ADVCE_KNOCKOUT_TOWHITE_TICKS);
        }
        //--Stop rendering past the towhite knockout.
        else
        {
            return;
        }
    }
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Get positions.
    float cRenderX = pEntity->GetCombatX();
    float cRenderY = 440.0f;

    ///--[HP Bar Frame]
    //--Frame position is centered.
    float cFrameX = cRenderX - (Images.Data.rEnemyHealthBarFrame->GetTrueWidth() * 0.50f);

    //--Render the fixed parts.
    Images.Data.rEnemyHealthBarUnder->Draw(cFrameX, cRenderY);

    //--Render the fill according to its percentage.
    float cFillOffsetX = 8.0f;
    float cFillOffsetY = 5.0f;
    Images.Data.rEnemyHealthBarFill->Bind();

    //--Compute positions and sizes.
    float cPercent = pEntity->GetDisplayHPPct();
    float cLft = cFrameX + cFillOffsetX;
    float cTop = cRenderY + cFillOffsetY;
    float cRgt = cLft + (Images.Data.rEnemyHealthBarFill->GetWidth() * cPercent);
    float cBot = cTop + Images.Data.rEnemyHealthBarFill->GetHeight();
    float cTxL = 0.0f;
    float cTxT = 0.0f;
    float cTxR = cPercent;
    float cTxB = 1.0f;
    glBegin(GL_QUADS);
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cBot);
    glEnd();

    //--Frame.
    Images.Data.rEnemyHealthBarFrame->Draw(cFrameX, cRenderY);

    //--Render the HP values.
    int tHPMax = pEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_HPMAX);
    int tShowHP = round(cPercent * tHPMax);
    Images.Data.rPortraitHPFont->DrawTextArgs(cRenderX, cRenderY + 17.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i/%i", tShowHP, tHPMax);

    ///--[Stun]
    //--Does not render if the entity is not flagged as stunnable. Note that an entity not flagged as stunnable
    //  still tracks stun values, and can still be stunned if the script says so, it only controls rendering.
    if(pEntity->IsStunnable())
    {
        //--Render the frame. This is meant to attach to the base frame.
        Images.Data.rEnemyHealthBarStun->Draw(cFrameX, cRenderY);

        //--Get values.
        int tStunCur    = pEntity->GetDisplayStun();
        int tStunCap    = pEntity->GetStatistic(STATS_STUN_CAP);
        int tStunRes    = pEntity->GetStunResist();
        int tStunResCnt = pEntity->GetStunResistTimer();

        //--Render the current stun value and threshold.
        float cStunOffY = 24.0f;
        Images.Data.rPortraitHPFont->DrawTextArgs(cFrameX+64.0f, cRenderY+cStunOffY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i/", tStunCur);
        Images.Data.rPortraitHPFont->DrawTextArgs(cFrameX+64.0f, cRenderY+cStunOffY, 0, 1.0f, "%i", tStunCap);

        //--Render the stun resist and turn timer.
        float cSizePerStar = 12.0f;
        for(int i = 0; i < tStunRes; i ++)
        {
            Images.Data.rEnemyHealthBarStunMarker->Draw(cFrameX + (cSizePerStar * i), cRenderY);
        }

        //--Turn timer.
        cStunOffY = cStunOffY + 14;
        Images.Data.rPortraitHPFont->DrawTextArgs(cFrameX+80.0f, cRenderY+cStunOffY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", 3-tStunResCnt);
    }

    ///--[Effects]
    //--Setup.
    float cEffectHei = 24.0f;
    float tEffectRenderY = cRenderY - cEffectHei;

    //--The zeroth effect is shields, if any are present.
    int tShowShield = pEntity->GetDisplayShield();
    if(tShowShield > 0)
    {
        char tBuffer[128];
        sprintf(tBuffer, "[IMG0]%i", tShowShield);
        mSpecialShieldString->SetString(tBuffer);
        mSpecialShieldString->CrossreferenceImages();
        mSpecialShieldString->DrawText(cRenderX-47, tEffectRenderY, 0, 1.0f, Images.Data.rEffectFont);
        tEffectRenderY = tEffectRenderY - cEffectHei;
    }

    //--The first effect is adrenaline, if present.
    int tShowAdrenaline = pEntity->GetDisplayAdrenaline();
    if(tShowAdrenaline > 0)
    {
        char tBuffer[128];
        sprintf(tBuffer, "[IMG0]%i", tShowAdrenaline);
        mSpecialAdrenalineString->SetString(tBuffer);
        mSpecialAdrenalineString->CrossreferenceImages();
        mSpecialAdrenalineString->DrawText(cRenderX-47, tEffectRenderY, 0, 1.0f, Images.Data.rEffectFont);
        tEffectRenderY = tEffectRenderY - cEffectHei;
    }

    //--Run across the list of effects applying to this entity and render their strings.
    SugarLinkedList *rEffectList = pEntity->GetEffectRenderList();
    AdvCombatEffect *rEffect = (AdvCombatEffect *)rEffectList->PushIterator();
    while(rEffect)
    {
        //--If the effect is marked as hidden, do not render it.
        if(rEffect->IsVisibleOnUI())
        {
            //--Locate the package associated with this entity.
            StarlightString *rString = rEffect->GetStringByTargetID(pEntity->GetID());
            if(rString && rString->GetTotalChars() > 0)
            {
                rString->DrawText(cRenderX-47, tEffectRenderY, 0, 1.0f, Images.Data.rEffectFont);
                tEffectRenderY = tEffectRenderY - cEffectHei;
            }
        }

        rEffect = (AdvCombatEffect *)rEffectList->AutoIterate();
    }

    ///--[Clean]
    StarlightColor::ClearMixer();
}

//========================================== Ally Bars ============================================
void AdvCombat::RenderAllyBars()
{
    ///--[Documentation and Setup]
    //--Renders the bars in the top left that show the HP/MP/Etc of the player's party.
    if(!Images.mIsReady) return;
    DebugPush(true, "Combat Render Ally Bars: Begin.\n");

    //--Positions
    float cYPos = 0.0f;
    float cYStep = 38.0f;

    //--For each party member:
    DebugPrint("Begin cycle.\n");
    int tMaskCode = ACE_STENCIL_ALLY_PORTRAIT_START;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        //--Render the backing.
        DebugPrint("Rendering bar for %p.\n", rEntity);
        DebugPrint("Name: %s.\n", rEntity->GetDisplayName());
        Images.Data.rAllyFrame->Draw(0.0f, cYPos);

        //--Mask.
        DisplayManager::ActivateMaskRender(tMaskCode);
        Images.Data.rAllyPortraitMask->Draw(0.0f, cYPos);

        //--Now render only on pixels the mask rendered on.
        DebugPrint("Rendering stencil mask and portrait.\n");
        DisplayManager::ActivateStencilRender(tMaskCode);
        TwoDimensionRealPoint cRenderCoords = rEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_ALLY);
        SugarBitmap *rCombatPortrait = rEntity->GetCombatPortrait();
        if(rCombatPortrait)
        {
            rCombatPortrait->Draw(0.0f + cRenderCoords.mXCenter, cYPos + cRenderCoords.mYCenter);
        }

        //--Disable stencilling.
        DisplayManager::DeactivateStencilling();

        //--Render the HP.
        DebugPrint("Rendering statistics.\n");
        int tHPMax = rEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_HPMAX);
        int tHP = round(tHPMax * rEntity->GetDisplayHPPct());
        Images.Data.rAllyHPFont->DrawTextArgs(178.0f, 24.0f + cYPos - 14.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i/%i", tHP, tHPMax);

        //--Render the MP.
        int tMPMax = rEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_MPMAX);
        int tMP = round(tMPMax * rEntity->GetDisplayMPPct());
        Images.Data.rAllyHPFont->DrawTextArgs(263.0f, 24.0f + cYPos - 14.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i/%i", tMP, tMPMax);

        //--Render the CP.
        int tCP = rEntity->GetComboPoints();
        Images.Data.rAllyHPFont->DrawTextArgs(290.0f, 24.0f + cYPos - 14.0f, 0, 1.0f, "x%i", tCP);

        //--Render shields.
        int tShields = rEntity->GetDisplayShield();
        Images.Data.rAllyHPFont->DrawTextArgs(178.0f, 24.0f + cYPos + 1, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tShields);

        //--Render adrenaline.
        int tAdrenaline = rEntity->GetDisplayAdrenaline();
        Images.Data.rAllyHPFont->DrawTextArgs(263.0f, 24.0f + cYPos + 1, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tAdrenaline);

        //--Effect setup.
        DebugPrint("Rendering effects.\n");
        float cEffectLft = 322.0f;
        float cEffectTop =   8.0f + cYPos;
        float cEffectScl =   0.5f;
        float cEffectInv = 1.0f / cEffectScl;
        float cEffectWid =  26.0f * cEffectInv; //Incorporates scale.
        float tEffectTal = 0.0f;
        SugarLinkedList *rEffectList = rEntity->GetEffectRenderList();

        //--Position.
        glTranslatef(cEffectLft, cEffectTop, 0.0f);
        glScalef(cEffectScl, cEffectScl, 1.0f);

        //--Render effects.
        DebugPrint("Iterating across %i effects.\n", rEffectList->GetListSize());
        AdvCombatEffect *rEffect = (AdvCombatEffect *)rEffectList->PushIterator();
        while(rEffect)
        {

            //--Locate the package associated with this entity.
            DebugPrint("Effect %p begin.\n", rEffect);
            if(rEffect->IsVisibleOnUI() && rEffect->IsIDOnTargetList(rEntity->GetID()))
            {
                //--Get images.
                SugarBitmap *rEffectBack = rEffect->GetBackImage();
                SugarBitmap *rEffectFrame = rEffect->GetFrameImage();
                SugarBitmap *rEffectFront = rEffect->GetFrontImage();

                //--Render.
                if(rEffectBack)  rEffectBack->Draw();
                if(rEffectFrame) rEffectFrame->Draw();
                if(rEffectFront) rEffectFront->Draw();

                glTranslatef(cEffectWid, 0.0f, 0.0f);
                tEffectTal = tEffectTal + cEffectWid;
            }
            DebugPrint("Effect %p complete.\n", rEffect);

            rEffect = (AdvCombatEffect *)rEffectList->AutoIterate();
        }

        //--Clean.
        DebugPrint("Cleaning up.\n");
        glTranslatef(-tEffectTal, 0.0f, 0.0f);
        glScalef(cEffectInv, cEffectInv, 1.0f);
        glTranslatef(-cEffectLft, -cEffectTop, 0.0f);

        //--Next.
        tMaskCode ++;
        cYPos = cYPos + cYStep;
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }
    DebugPop("Combat Render Ally Bars: Complete.\n");
}

//========================================= Turn Order ============================================
void AdvCombat::RenderTurnOrder()
{
    ///--[Documentation and Setup]
    //--Renders the turn order in the top right.
    if(!Images.mIsReady) return;

    //--Render the left component of the bar at the end of its width.
    float cLftPos = VIRTUAL_CANVAS_X - (mTurnWidCurrent + Images.Data.rTurnOrderEdge->GetWidth());
    float cTopPos = 6.0f;
    Images.Data.rTurnOrderEdge->Draw(cLftPos, cTopPos);

    //--Backing bar.
    cLftPos = cLftPos + Images.Data.rTurnOrderEdge->GetWidth();
    float cRgtPos = VIRTUAL_CANVAS_X;
    float cBotPos = cTopPos + Images.Data.rTurnOrderBack->GetHeight();
    Images.Data.rTurnOrderBack->Bind();
    glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f); glVertex2f(cLftPos, cTopPos);
        glTexCoord2f(1.0f, 1.0f); glVertex2f(cRgtPos, cTopPos);
        glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgtPos, cBotPos);
        glTexCoord2f(0.0f, 0.0f); glVertex2f(cLftPos, cBotPos);
    glEnd();

    //--[Portraits]
    //--Render the portraits in the opposite order. The acting character is on the right side.
    float cRenderW = 48.0f;
    float cRenderX = cLftPos - 24.0f + (mrTurnOrder->GetListSize() * cRenderW) - cRenderW;
    float cRenderY = 5.0f;
    AdvCombatEntity *rTurnEntity = (AdvCombatEntity *)mrTurnOrder->PushIterator();
    while(rTurnEntity)
    {
        //--Get the turn order portrait.
        SugarBitmap *rTurnIcon = rTurnEntity->GetTurnIcon();
        if(rTurnIcon) rTurnIcon->Draw(cRenderX, cRenderY);

        //--Next.
        cRenderX = cRenderX - cRenderW;
        rTurnEntity = (AdvCombatEntity *)mrTurnOrder->AutoIterate();
    }

    //--Turn marker. Renders over the portrait.
    Images.Data.rTurnOrderCircle->Draw();
}

//====================================== Player Interface =========================================
void AdvCombat::RenderPlayerBar()
{
    ///--[Documentation and Setup]
    //--Renders the player's turn commands bar on the bottom of the screen. This should not render unless
    //  a player-controlled character is acting.
    if(!Images.mIsReady) return;

    //--Get the active character.
    AdvCombatEntity *rActiveEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActiveEntity) return;

    //--Color Setup.
    StarlightColor cWhite = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    StarlightColor cGrey  = StarlightColor::MapRGBAF(0.5f, 0.5f, 0.5f, 1.0f);

    ///--[Character Portrait]
    //--Setup.
    SugarBitmap *rCharacterPortrait = rActiveEntity->GetCombatPortrait();
    SugarBitmap *rCountermask = rActiveEntity->GetCombatCounterMask();
    TwoDimensionRealPoint cRenderPos = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_MAIN);

    //--Render the inset.
    Images.Data.rMainPortraitRingBack->Draw();

    //--Switch to character masking.
    DisplayManager::ActivateMaskRender(ACE_STENCIL_ACTIVE_CHARACTER_PORTRAIT);
    Images.Data.rMainPortraitMask->Draw();

    //--If a countermask is available, render that.
    if(rCountermask)
    {
        DisplayManager::ActivateMaskRender(0);
        rCountermask->Draw(cRenderPos.mXCenter, cRenderPos.mYCenter);
    }

    //--Render the portrait.
    DisplayManager::ActivateStencilRender(ACE_STENCIL_ACTIVE_CHARACTER_PORTRAIT);
    if(rCharacterPortrait) rCharacterPortrait->Draw(cRenderPos.mXCenter, cRenderPos.mYCenter);
    DisplayManager::DeactivateStencilling();

    //--Render the frame and name banner.
    Images.Data.rMainPortraitRing->Draw();
    Images.Data.rMainNameBanner->Draw();

    //--Render the character's display name.
    const char *rDisplayName = rActiveEntity->GetDisplayName();
    if(rDisplayName)
    {
        float cCenterX = 129.0f;
        float cCenterY = 665.0f;
        Images.Data.rActiveCharacterNameFont->DrawText(cCenterX, cCenterY, SUGARFONT_AUTOCENTER_XY, 1.0f, rDisplayName);
    }

    ///--[Health Bar]
    //--Static backing.
    Images.Data.rHealthBarUnderlay->Draw();

    //--Render constants.
    float cDescriptionLft = 52.0f;
    float cValueRgt = 79.0f;
    float cShieldLft = 232.0f;
    float cHPTop = 690.0f;
    float cMPTop = 711.0f;
    float cCPTop = 732.0f;
    float cCPPipLft =  76.0f;
    float cCPPipTop = 735.0f;
    float cCPPipWid =  10.0f;

    //--Shield and Adrenaline values. If zero, don't render the overlay.
    int cShieldValue = rActiveEntity->GetDisplayShield();
    int cAdrenalineValue = rActiveEntity->GetDisplayAdrenaline();

    //--HP Bar.
    float cHPPercent = rActiveEntity->GetHealthPercent();
    RenderPercent(cHPPercent, 1.0f, Images.Data.rHealthBarHPFill);

    //--MP Bar.
    float cMPPercent = rActiveEntity->GetMagicPercent();
    RenderPercent(cMPPercent, 1.0f, Images.Data.rHealthBarMPFill);

    //--Static frame.
    Images.Data.rMainHealthBarFrame->Draw();

    //--Shield overlay.
    if(cShieldValue > 0 && cAdrenalineValue > 0)
        Images.Data.rHealthBarMix->Draw();
    else if(cShieldValue > 0)
        Images.Data.rHealthBarShield->Draw();
    else if(cAdrenalineValue > 0)
        Images.Data.rHealthBarAdrenaline->Draw();

    //--Render HP.
    Images.Data.rAllyHPFont->DrawText(cDescriptionLft, cHPTop, 0, 1.0f, "HP");
    Images.Data.rAllyHPFont->DrawTextArgs(cValueRgt,   cHPTop, 0, 1.0f, "%i/%i", rActiveEntity->GetHealth(), rActiveEntity->GetStatistic(STATS_HPMAX));

    //--Render MP.
    Images.Data.rAllyHPFont->DrawText(cDescriptionLft, cMPTop, 0, 1.0f, "MP");
    Images.Data.rAllyHPFont->DrawTextArgs(cValueRgt,   cMPTop, 0, 1.0f, "%i/%i", rActiveEntity->GetMagic(), rActiveEntity->GetStatistic(STATS_MPMAX));

    //--Render CP. Each one is a "pip".
    Images.Data.rAllyHPFont->DrawText(cDescriptionLft, cCPTop, 0, 1.0f, "CP");
    int cCPTotal = rActiveEntity->GetComboPoints();
    for(int i = 0; i < cCPTotal; i ++)
    {
        Images.Data.rCPPip->Draw(cCPPipLft + (cCPPipWid * i), cCPPipTop);
    }

    //--Both shield and adrenaline are present:
    if(cShieldValue > 0 && cAdrenalineValue > 0)
    {
        //--Render shields in teal.
        StarlightColor::SetMixer(0.1f, 1.0f, 1.0f, 1.0f);
        Images.Data.rAllyHPFont->DrawTextArgs(cShieldLft, cHPTop, 0, 1.0f, "%i", cShieldValue);

        //--Render adrenaline in orange.
        StarlightColor::SetMixer(0.9f, 0.7f, 0.2f, 1.0f);
        Images.Data.rAllyHPFont->DrawTextArgs(cShieldLft, cMPTop, 0, 1.0f, "%i", cAdrenalineValue);
        StarlightColor::ClearMixer();
    }
    //--Just shield.
    else if(cShieldValue > 0)
    {
        StarlightColor::SetMixer(0.1f, 1.0f, 1.0f, 1.0f);
        Images.Data.rAllyHPFont->DrawTextArgs(cShieldLft, cHPTop, 0, 1.0f, "%i", cShieldValue);
        StarlightColor::ClearMixer();
    }
    //--Just adrenaline.
    else if(cAdrenalineValue > 0)
    {
        StarlightColor::SetMixer(0.9f, 0.7f, 0.2f, 1.0f);
        Images.Data.rAllyHPFont->DrawTextArgs(cShieldLft, cHPTop, 0, 1.0f, "%i", cAdrenalineValue);
        StarlightColor::ClearMixer();
    }

    ///--[Ability Display]
    //--Primary page.
    if(mSecondPageTimer < ADVCOMBAT_PAGE_CHANGE_TICKS)
    {
        //--Transparency.
        float cPercent = EasingFunction::QuadraticOut(mSecondPageTimer, ADVCOMBAT_PAGE_CHANGE_TICKS);
        float cAlpha = 1.0f - cPercent;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

        //--Position.
        float cYOff = 270.0f * cPercent;
        glTranslatef(0.0f, cYOff, 0.0f);

        //--Render all the character's available ability icons and a cursor for the player to interact with.
        if(mSkillCatalystExtension > 0)
        {
            int tFrameIndex = mSkillCatalystExtension - 1;
            if(tFrameIndex >= ADVCOMBAT_CATALYST_MAX) tFrameIndex = ADVCOMBAT_CATALYST_MAX - 1;
            Images.Data.rAbilityFrameCatalyst[tFrameIndex]->Draw();
        }
        Images.Data.rAbilityFrameCustom->Draw();
        Images.Data.rAbilityFrameMain->Draw();

        //--Positions.
        float cAbltyLft = ADVCOMBAT_POSITION_ABILITY_X;
        float cAbltyTop = ADVCOMBAT_POSITION_ABILITY_Y;
        float cAbltyWid = ADVCOMBAT_POSITION_ABILITY_W;
        float cAbltyHei = ADVCOMBAT_POSITION_ABILITY_H;

        //--Render abilities.
        for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
        {
            for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
            {
                //--Skip empty slots.
                AdvCombatAbility *rAbility = rActiveEntity->GetAbilityBySlot(x, y);
                if(!rAbility) continue;

                //--Resolve position.
                float cRenderX = cAbltyLft + (cAbltyWid * x);
                float cRenderY = cAbltyTop + (cAbltyHei * y);

                //--Get rendering images.
                SugarBitmap *rImageBack  = rAbility->GetIconBack();
                SugarBitmap *rImageFrame = rAbility->GetIconFrame();
                SugarBitmap *rImage      = rAbility->GetIcon();
                SugarBitmap *rCPImage    = rAbility->GetCPIcon();

                //--Ability is not cooling down:
                if(rAbility->GetCooldown() < 1)
                {
                    //--If the ability cannot be used for any reason, grey it out.
                    if(rAbility->IsUsableNow() == false) cGrey.SetAsMixerAlpha(cAlpha);

                    //--Render.
                    if(rImageBack)  rImageBack ->Draw(cRenderX, cRenderY);
                    if(rImageFrame) rImageFrame->Draw(cRenderX, cRenderY);
                    if(rImage)      rImage     ->Draw(cRenderX, cRenderY);
                    if(rCPImage)    rCPImage   ->Draw(cRenderX, cRenderY);

                    //--Clean.
                    if(rAbility->IsUsableNow() == false) cWhite.SetAsMixerAlpha(cAlpha);
                }
                //--Ability is cooling, so render the turn timers.
                else
                {
                    //--Render the non-CP parts, but grey the back and image out.
                    StarlightColor::SetMixer(0.2f, 0.2f, 0.2f, 1.0f);
                    if(rImageBack)  rImageBack ->Draw(cRenderX, cRenderY);
                    StarlightColor::ClearMixer();
                    if(rImageFrame) rImageFrame->Draw(cRenderX, cRenderY);
                    StarlightColor::SetMixer(0.2f, 0.2f, 0.2f, 1.0f);
                    if(rImage)      rImage     ->Draw(cRenderX, cRenderY);
                    StarlightColor::ClearMixer();

                    //--Render the turn icon.
                    Images.Data.rCooldownClock->Draw(cRenderX+6, cRenderY-15);

                    //--Render the turn indicator. It caps at 9.
                    int tSlot = rAbility->GetCooldown();
                    if(tSlot < 0) tSlot = 0;
                    if(tSlot > 9) tSlot = 9;
                    Images.Data.rCooldownNums[tSlot]->Draw(cRenderX+27, cRenderY-15);
                }
            }
        }

        //--Clean.
        glTranslatef(0.0f, -cYOff, 0.0f);

        //--Independent of position but not color, render this string.
        mPageRgtString->DrawText(806.0f, 495.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, Images.Data.rAbilityDescriptionFont);
        StarlightColor::ClearMixer();
    }

    //--Secondary page. Renders above primary, can render concurrently.
    if(mSecondPageTimer > 0)
    {
        //--Transparency.
        float cPercent = 1.0f - EasingFunction::QuadraticOut(mSecondPageTimer, ADVCOMBAT_PAGE_CHANGE_TICKS);
        float cAlpha = 1.0f - cPercent;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

        //--Position.
        float cYOff = 270.0f * cPercent;
        glTranslatef(0.0f, cYOff, 0.0f);

        //--Only the main backing is used.
        Images.Data.rAbilityFrameSecondary->Draw();

        //--Positions.
        float cAbltyLft = ADVCOMBAT_POSITION_ABILITY_X;
        float cAbltyTop = ADVCOMBAT_POSITION_ABILITY_Y;
        float cAbltyWid = ADVCOMBAT_POSITION_ABILITY_W;
        float cAbltyHei = ADVCOMBAT_POSITION_ABILITY_H;

        //--Render abilities.
        for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
        {
            for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
            {
                //--Skip empty slots.
                AdvCombatAbility *rAbility = rActiveEntity->GetSecondaryBySlot(x, y);
                if(!rAbility) continue;

                //--Resolve position.
                float cRenderX = cAbltyLft + (cAbltyWid * x);
                float cRenderY = cAbltyTop + (cAbltyHei * y);

                //--Get rendering images.
                SugarBitmap *rImageBack  = rAbility->GetIconBack();
                SugarBitmap *rImageFrame = rAbility->GetIconFrame();
                SugarBitmap *rImage      = rAbility->GetIcon();
                SugarBitmap *rCPImage    = rAbility->GetCPIcon();

                //--If the ability cannot be used for any reason, grey it out.
                if(rAbility->IsUsableNow() == false) cGrey.SetAsMixerAlpha(cAlpha);

                //--Render.
                if(rImageBack)  rImageBack ->Draw(cRenderX, cRenderY);
                if(rImageFrame) rImageFrame->Draw(cRenderX, cRenderY);
                if(rImage)      rImage     ->Draw(cRenderX, cRenderY);
                if(rCPImage)    rCPImage   ->Draw(cRenderX, cRenderY);

                //--Clean.
                if(rAbility->IsUsableNow() == false) cWhite.SetAsMixerAlpha(cAlpha);
            }
        }

        //--Clean.
        glTranslatef(0.0f, -cYOff, 0.0f);

        //--Independent of position but not color, render this string.
        mPageLftString->DrawText(806.0f, 495.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, Images.Data.rAbilityDescriptionFont);
        StarlightColor::ClearMixer();
    }

    //--Render the ability highlight.
    Images.Data.rAbilityHighlight->Draw(mAbilityHighlightPack.mXCur, mAbilityHighlightPack.mYCur);

    ///--[Description Display]
    //--Backing.
    Images.Data.rDescriptionWindow->Draw();

    //--Render the description of the hovered ability here.
    AdvCombatAbility *rHighlightedAbility = rActiveEntity->GetAbilityBySlot(mAbilitySelectionX, mAbilitySelectionY);
    if(mShowingSecondPage) rHighlightedAbility = rActiveEntity->GetSecondaryBySlot(mAbilitySelectionX, mAbilitySelectionY);
    if(rHighlightedAbility)
    {
        //--Header.
        float cNameX = 859.0f;
        float cNameY = 503.0f;
        Images.Data.rAbilityHeaderFont->DrawText(cNameX, cNameY, 0, 1.0f, rHighlightedAbility->GetDisplayName());

        //--Position.
        float cTxtLft = 839.0f;
        float cTxtTop = 538.0f;
        float cTxtHei =  20.0f;

        //--Render.
        int tTotalLines = rHighlightedAbility->GetDescriptionLinesTotal();
        for(int i = 0; i < tTotalLines; i ++)
        {
            StarlightString *rDescription = rHighlightedAbility->GetDescriptionLine(i);
            if(rDescription) rDescription->DrawText(cTxtLft, cTxtTop + (cTxtHei * i), 0, 1.0f, Images.Data.rAbilityDescriptionFont);
        }
    }
}

//=========================================== New Turn ============================================
void AdvCombat::RenderNewTurn()
{
    ///--[Documentation and Setup]
    //--Renders the "TURN X" at the top of the screen. Does nothing if the time has expired.
    if(mTurnDisplayTimer >= ACE_TURN_DISPLAY_TICKS || !Images.mIsReady) return;

    //--Timer setup.
    int cLetterTicks = ACE_TURN_TICKS_PER_LETTER * 5;
    int cHoldTicks = cLetterTicks + ACE_TURN_TICKS_HOLD;

    //--Populate the buffer. Note that turn 0 renders as "TURN 1". We never pass "TURN 1000", it just stays there
    //  to prevent overflow. The actual turn counter continues to rise.
    int tUseTurns = mCurrentTurn+1;
    if(tUseTurns > 1000) tUseTurns = 1000;
    char tFullBuffer[10];
    sprintf(tFullBuffer, "TURN %i", tUseTurns);

    //--Positions
    float cXStart = (VIRTUAL_CANVAS_X * 0.50f) - (Images.Data.rNewTurnFont->GetTextWidth(tFullBuffer) * 0.50f);
    float cYStart = VIRTUAL_CANVAS_Y * 0.00f;
    float cYEnd   = VIRTUAL_CANVAS_Y * 0.10f;

    //--If we're in the displaying new letters part:
    if(mTurnDisplayTimer < cLetterTicks)
    {
        //--For each letter in "TURN"
        float tXPos = cXStart;
        int tUseTicks = mTurnDisplayTimer;
        for(int i = 0; i < 4; i ++)
        {
            //--Percentage drop. The letter starts at the top of the screen and moves down.
            float cPercent = EasingFunction::QuadraticOut(tUseTicks, ACE_TURN_TICKS_PER_LETTER);
            float cYPos = cYStart + ((cYEnd - cYStart) * cPercent);

            //--Render.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPercent);
            float tIncrement = Images.Data.rNewTurnFont->DrawLetter(tXPos, cYPos, 0, 1.0f, tFullBuffer[i], tFullBuffer[i+1]);
            tXPos = tXPos + tIncrement;

            //--Clean.
            StarlightColor::ClearMixer();

            //--Next. End if the letter should not display.
            tUseTicks -= ACE_TURN_TICKS_PER_LETTER;
            if(tUseTicks <= 0) break;
        }

        //--Render the turn number if there are ticks for it.
        if(tUseTicks > 0)
        {
            //--Increment by the distance of a space.
            float cPercent = EasingFunction::QuadraticOut(tUseTicks, ACE_TURN_TICKS_PER_LETTER);
            float cYPos = cYStart + ((cYEnd - cYStart) * cPercent);
            float tIncrement = Images.Data.rNewTurnFont->DrawLetter(tXPos, cYPos, 0, 1.0f, ' ', tFullBuffer[5]);
            tXPos = tXPos + tIncrement;

            //--Render.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPercent);
            Images.Data.rNewTurnFont->DrawText(tXPos, cYPos, 0, 1.0f, &tFullBuffer[6]);

            //--Clean.
            StarlightColor::ClearMixer();
        }
    }
    //--If we're in the "Hold" part:
    else if(mTurnDisplayTimer < cHoldTicks)
    {
        Images.Data.rNewTurnFont->DrawText(cXStart, cYEnd, 0, 1.0f, tFullBuffer);
    }
    //--If we're in the "Fade" part:
    else
    {
        float cAlpha = 1.0f - EasingFunction::QuadraticOut(mTurnDisplayTimer - cHoldTicks, ACE_TURN_TICKS_FADE);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        Images.Data.rNewTurnFont->DrawText(cXStart, cYEnd, 0, 1.0f, tFullBuffer);
        StarlightColor::ClearMixer();
    }
}

//======================================= Target Cluster ==========================================
void AdvCombat::RenderTargetCluster()
{
    //--At the top-center of the screen, renders a box and the name of the entity being targeted.
    if(!mIsSelectingTargets || !Images.mIsReady) return;

    //--Get the active target cluster.
    TargetCluster *rActiveCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(mTargetClusterCursor);
    if(!rActiveCluster) return;

    //--Setup.
    float cBoxWid = Images.Data.rTargetBox->GetWidthSafe();
    float cBoxHei = Images.Data.rTargetBox->GetHeightSafe();
    float cEdgeSizePxl = Images.Data.rTargetArrowLft->GetWidth() + 2.0f;
    float cEdgeSizePct = cEdgeSizePxl / cBoxWid;

    //--Text ideal center.
    float cTextCntX = (VIRTUAL_CANVAS_X * 0.50f);
    float cTextCntY = (VIRTUAL_CANVAS_Y * 0.12f);

    //--Position to render the arrows. Variable based on box width.
    float cArrowLftX = 0.0f;
    float cArrowRgtX = 0.0f;
    float cArrowTopY = 0.0f;

    //--Get the length of the name.
    int cLength = Images.Data.rTargetClusterFont->GetTextWidth(rActiveCluster->mDisplayName);

    //--If the length of the name is shorter than the box size with arrow padding, just render it with no modifications.
    if(cLength < cBoxWid - (cEdgeSizePxl * 2.0f))
    {
        //--Position, render.
        float cX = cTextCntX - (cBoxWid * 0.50f);
        float cY = cTextCntY - (cBoxHei * 0.50f);
        Images.Data.rTargetBox->Draw(cX, cY);

        //--Store arrow positions.
        cArrowLftX = cX + 6.0f;
        cArrowRgtX = cX + cBoxWid - 16.0f;
        cArrowTopY = cY;
    }
    //--Otherwise, stretch the box to make space.
    else
    {
        //--Compute positions.
        float cNeededLen = cLength + (cEdgeSizePxl * 2.0f);
        float cX = cTextCntX - (cNeededLen * 0.50f);
        float cY = cTextCntY - (cBoxHei * 0.50f);

        //--Store arrow positions.
        cArrowLftX = cX + 6.0f;
        cArrowRgtX = cX + cNeededLen - 16.0f;
        cArrowTopY = cY;

        //--Begin rendering.
        Images.Data.rTargetBox->Bind();
        glBegin(GL_QUADS);

        //--Render the left edge.
        float cLft = cX;
        float cTop = cY;
        float cRgt = cLft + cEdgeSizePxl;
        float cBot = cTop + cBoxHei;
        float cTxL = 0.0f;
        float cTxT = 0.0f;
        float cTxR = cEdgeSizePct;
        float cTxB = 1.0f;
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cBot);

        //--Render the right edge.
        cLft = cX + cNeededLen - cEdgeSizePxl;
        cRgt = cX + cNeededLen;
        cTxL = 1.0f - cEdgeSizePct;
        cTxR = 1.0f;
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cBot);

        //--Stretch the middle section.
        cLft = cX + cEdgeSizePxl;
        cRgt = cX + cNeededLen - cEdgeSizePxl;
        cTxL = cEdgeSizePct;
        cTxR = 1.0f - cEdgeSizePct;
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cBot);

        //--Finish rendering.
        glEnd();
    }

    //--Render the text centered.
    Images.Data.rTargetClusterFont->DrawText(cTextCntX, cTextCntY - 3.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, rActiveCluster->mDisplayName);

    //--If there are more than one target cluster, render arrows. The cluster wraps so always render both.
    if(mTargetClusters->GetListSize() > 1)
    {
        Images.Data.rTargetArrowLft->Draw(cArrowLftX, cArrowTopY);
        Images.Data.rTargetArrowRgt->Draw(cArrowRgtX, cArrowTopY);
    }
}

//========================================== Title Box ============================================
void AdvCombat::RenderTitle()
{
    //--[Documentation and Setup]
    //--Shows a title indicating what ability is in use. Typically used by enemies to telegraph their attacks.
    if(!Images.mIsReady || !mShowTitle || !mTitleText) return;

    //--[Transparency]
    //--Title fades in over 10 ticks and out over 10 ticks.
    float cAlpha = 1.0f;
    if(mTitleTicks < 10)
    {
        cAlpha = EasingFunction::QuadraticOut(mTitleTicks, 10);
    }
    else if(mTitleTicks >= mTitleTicksMax - 10)
    {
        cAlpha = 1.0f - EasingFunction::QuadraticIn(mTitleTicks - (mTitleTicksMax - 10), 10);
    }

    //--Set alpha.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--[Backing]
    //--Setup.
    float cBoxWid = Images.Data.rTitleBox->GetWidthSafe();
    float cBoxHei = Images.Data.rTitleBox->GetHeightSafe();
    float cEdgeSizePxl = 6.0f;
    float cEdgeSizePct = cEdgeSizePxl / cBoxWid;

    //--Text ideal center.
    float cTextCntX = (VIRTUAL_CANVAS_X * 0.50f);
    float cTextCntY = (VIRTUAL_CANVAS_Y * 0.10f);

    //--Get the length of the name.
    int cLength = Images.Data.rAbilityTitleFont->GetTextWidth(mTitleText);

    //--If the length of the name is shorter than the box size, just render it with no modifications.
    if(cLength < cBoxWid - (cEdgeSizePxl * 2.0f))
    {
        float cX = cTextCntX - (cBoxWid * 0.50f);
        float cY = cTextCntY - (cBoxHei * 0.50f);
        Images.Data.rTitleBox->Draw(cX, cY);
    }
    //--Otherwise, stretch the box to make space.
    else
    {
        //--Compute positions.
        float cNeededLen = cLength + (cEdgeSizePxl * 2.0f);
        float cX = cTextCntX - (cNeededLen * 0.50f);
        float cY = cTextCntY - (cBoxHei * 0.50f);

        //--Begin rendering.
        Images.Data.rTitleBox->Bind();
        glBegin(GL_QUADS);

        //--Render the left edge.
        float cLft = cX;
        float cTop = cY;
        float cRgt = cLft + cEdgeSizePxl;
        float cBot = cTop + cBoxHei;
        float cTxL = 0.0f;
        float cTxT = 0.0f;
        float cTxR = cEdgeSizePct;
        float cTxB = 1.0f;
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cBot);

        //--Render the right edge.
        cLft = cX + cNeededLen - cEdgeSizePxl;
        cRgt = cX + cNeededLen;
        cTxL = 1.0f - cEdgeSizePct;
        cTxR = 1.0f;
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cBot);

        //--Stretch the middle section.
        cLft = cX + cEdgeSizePxl;
        cRgt = cX + cNeededLen - cEdgeSizePxl;
        cTxL = cEdgeSizePct;
        cTxR = 1.0f - cEdgeSizePct;
        glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cBot);
        glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cBot);

        //--Finish rendering.
        glEnd();
    }

    //--[Text]
    //--Render the text centered.
    Images.Data.rAbilityTitleFont->DrawText(cTextCntX, cTextCntY - 3.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, mTitleText);

    //--[Clean]
    StarlightColor::ClearMixer();
}
