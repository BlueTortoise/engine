//--Base
#include "AdvCombat.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

//--[Property Queries]
bool AdvCombat::IsAutoUseDoctorBag()
{
    return mAutoUseDoctorBag;
}
bool AdvCombat::IsTouristMode()
{
    return mTouristMode;
}
bool AdvCombat::IsMemoryCursor()
{
    return mMemoryCursor;
}

//--[Manipulators]
void AdvCombat::SetAutoUseDoctorBag(bool pFlag)
{
    mAutoUseDoctorBag = pFlag;
}
void AdvCombat::SetTouristMode(bool pFlag)
{
    mTouristMode = pFlag;
}
void AdvCombat::SetMemoryCursor(bool pFlag)
{
    mMemoryCursor = pFlag;
}
