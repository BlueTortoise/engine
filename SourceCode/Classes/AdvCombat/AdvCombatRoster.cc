//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

//--[Property Queries]
int AdvCombat::GetRosterCount()
{
    return mPartyRoster->GetListSize();
}
int AdvCombat::GetActivePartyCount()
{
    return mrActiveParty->GetListSize();
}
int AdvCombat::GetCombatPartyCount()
{
    return mrCombatParty->GetListSize();
}
bool AdvCombat::IsEntityInPlayerParty(void *pPtr)
{
    return mrCombatParty->IsElementOnList(pPtr);
}
bool AdvCombat::DoesPartyMemberExist(const char *pInternalName)
{
    //--Used to prevent double-registrations.
    if(!pInternalName) return false;
    return (mPartyRoster->GetElementByName(pInternalName) != NULL);
}
bool AdvCombat::IsPartyMemberActive(const char *pInternalName)
{
    if(!pInternalName) return false;
    return (mrActiveParty->GetElementByName(pInternalName) != NULL);
}

//--[Manipulators]
void AdvCombat::SetPartyBySlot(int pSlot, const char *pPartyMemberName)
{
    //--Clearing case. Pass "Null" or a NULL pointer to clear the slot.
    if(!pPartyMemberName || !strcasecmp(pPartyMemberName, "Null"))
    {
        mrActiveParty->RemoveElementI(pSlot);
        return;
    }

    //--Locate the character.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mPartyRoster->GetElementByName(pPartyMemberName);
    if(!rEntity) return;

    //--Insert.
    mrActiveParty->AddElementInSlot(pPartyMemberName, rEntity, pSlot);
}
void AdvCombat::RegisterPartyMember(const char *pInternalName, AdvCombatEntity *pEntity)
{
    //--Registers the given entity to the roster. Doesn't put it on the active party list.
    if(!pInternalName || !pEntity) return;
    mPartyRoster->AddElementAsTail(pInternalName, pEntity, &RootObject::DeleteThis);
}
void AdvCombat::PushPartyMemberS(const char *pInternalName)
{
    //--Pushes the named party member as the active object, or NULL if not found.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();
    if(!pInternalName) return;

    //--Locate, push if it exists.
    rDataLibrary->rActiveObject = mPartyRoster->GetElementByName(pInternalName);
}
void AdvCombat::PushPartyMemberI(int pSlot)
{
    //--Pushes the slotted party member as the active object, or NULL if not found.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();
    rDataLibrary->rActiveObject = mPartyRoster->GetElementBySlot(pSlot);
}
void AdvCombat::PushActivePartyMemberS(const char *pInternalName)
{
    //--Pushes the named party member as the active object, or NULL if not found.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();
    if(!pInternalName) return;

    //--Locate, push if it exists.
    rDataLibrary->rActiveObject = mrActiveParty->GetElementByName(pInternalName);
}
void AdvCombat::PushActivePartyMemberI(int pSlot)
{
    //--Pushes the slotted party member as the active object, or NULL if not found.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();
    rDataLibrary->rActiveObject = mrActiveParty->GetElementBySlot(pSlot);
}
void AdvCombat::PushCombatPartyMemberS(const char *pInternalName)
{
    //--Pushes the named party member as the active object, or NULL if not found.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();
    if(!pInternalName) return;

    //--Locate, push if it exists.
    rDataLibrary->rActiveObject = mrCombatParty->GetElementByName(pInternalName);
}
void AdvCombat::PushCombatPartyMemberI(int pSlot)
{
    //--Pushes the slotted party member as the active object, or NULL if not found.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();
    rDataLibrary->rActiveObject = mrCombatParty->GetElementBySlot(pSlot);
}

//--[Core Methods]
void AdvCombat::ClearParty()
{
    mrActiveParty->ClearList();
    mrCombatParty->ClearList();
}
void AdvCombat::FullRestoreParty()
{
    //--Fully restores all active party members. Usually used at rest points.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rEntity)
    {
        rEntity->FullRestore();
        rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }
}
void AdvCombat::FullRestoreRoster()
{
    //--Fully restores the entire player roster.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mPartyRoster->PushIterator();
    while(rEntity)
    {
        rEntity->FullRestore();
        rEntity = (AdvCombatEntity *)mPartyRoster->AutoIterate();
    }
}
void AdvCombat::MoveEntityToPartyGroup(uint32_t pID, int pPartyCode)
{
    //--Given an ID, moves the entity with the matching ID to the given party group. Removes the entity
    //  from any other parties it may have been in.
    //--The master pointer is not affected and remains in whatever master list it started in, only references
    //  get removed. Pass a party code of -1 to strip all references.

    //--Locate the entity.
    AdvCombatEntity *rEntity = GetEntityByID(pID);
    if(!rEntity) return;

    //--Name storage. Find the name the element currently has on the list.
    char tNameBuf[128];
    if(mPartyRoster->IsElementOnList(rEntity))
    {
        int tSlot = mPartyRoster->GetSlotOfElementByPtr(rEntity);
        const char *rName = mPartyRoster->GetNameOfElementBySlot(tSlot);
        if(rName) strcpy(tNameBuf, rName);
    }
    if(mEnemyRoster->IsElementOnList(rEntity))
    {
        int tSlot = mEnemyRoster->GetSlotOfElementByPtr(rEntity);
        const char *rName = mEnemyRoster->GetNameOfElementBySlot(tSlot);
        if(rName) strcpy(tNameBuf, rName);
    }

    //--Clean all references.
    mrCombatParty->RemoveElementP(rEntity);
    mrEnemyCombatParty->RemoveElementP(rEntity);
    mrEnemyReinforcements->RemoveElementP(rEntity);
    mrEnemyGraveyard->RemoveElementP(rEntity);

    //--Based on the flag, register the entity to the group.
    if(pPartyCode == AC_PARTY_GROUP_PARTY)
    {
        mrCombatParty->AddElementAsTail(tNameBuf, rEntity);
    }
    else if(pPartyCode == AC_PARTY_GROUP_ENEMY)
    {
        mrEnemyCombatParty->AddElementAsTail(tNameBuf, rEntity);
    }
    else if(pPartyCode == AC_PARTY_GROUP_GRAVEYARD)
    {
        mrEnemyGraveyard->AddElementAsTail(tNameBuf, rEntity);
    }
    else if(pPartyCode == AC_PARTY_GROUP_REINFORCEMENTS)
    {
        mrEnemyReinforcements->AddElementAsTail(tNameBuf, rEntity);
    }
}
