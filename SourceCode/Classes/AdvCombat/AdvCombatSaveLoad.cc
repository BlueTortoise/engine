//--Base
#include "AdvCombat.h"

//--Classes
#include "AdventureLevel.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--Libraries
//--Managers

void AdvCombat::WriteLoadInfo(SugarAutoBuffer *pBuffer)
{
    ///--[Documentation and Setup]
    //--Writes the LOADINFO_ block to a given buffer to be used in savefiles. The LOADINFO_ block
    //  contains levels, sprites, and other data for members of the active party. It is not used by
    //  the game itself, but by the main menu when showing the player the loading menu.
    if(!pBuffer) return;

    //--Write the names of the four characters. Their names are not linear mappings, rather, they will be
    //  names like "Mei_Alraune" if "Mei" is in "Alraune" form. We also write a single integer representing their level.

    //--Write the names of the members of the active party, as well as their form/job listing. These are used for
    //  sprite selection, and are not their final jobs. A single integer represents their level.
    char tBuffer[64];
    for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
    {
        //--Get entry.
        AdvCombatEntity *rCharacter = (AdvCombatEntity *)mrActiveParty->GetElementBySlot(i);

        //--If the entity exists, append "Name_Form" and their level.
        if(rCharacter)
        {
            //--Figure out the current job.
            AdvCombatJob *rActiveJob = rCharacter->GetActiveJob();

            //--Active job exists, print.
            if(rActiveJob)
            {
                sprintf(tBuffer, "%s_%s", rCharacter->GetName(), rActiveJob->GetInternalName());
            }
            //--No job.
            else
            {
                sprintf(tBuffer, "%s_%s", rCharacter->GetName(), "Error");
            }

            //--Print.
            pBuffer->AppendStringWithLen(tBuffer);
            pBuffer->AppendInt32(rCharacter->GetLevel());
        }
        //--If not, write "NULL" and 0.
        else
        {
            pBuffer->AppendStringWithLen("NULL");
            pBuffer->AppendInt32(0);
        }
    }
}
