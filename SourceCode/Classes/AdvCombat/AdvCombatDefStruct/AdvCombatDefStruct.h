//--[Adventure Combat: Definitions and Structures]
//--Header file used for all structures and definitions in the Adventure Combat code.
#pragma once
#include "Definitions.h"

//--[Party Codes]
//--Codes that indicate which party grouping an entity is in.
#define AC_PARTY_GROUP_NONE 0
#define AC_PARTY_GROUP_PARTY 1
#define AC_PARTY_GROUP_ENEMY 2
#define AC_PARTY_GROUP_GRAVEYARD 3
#define AC_PARTY_GROUP_REINFORCEMENTS 4
#define AC_PARTY_GROUP_ENVIRONMENT 5
#define AC_PARTY_GROUP_OTHER 6

//--[UI Positions]
//--These are a set of lookups for offsets on the UI. A character's portrait renders on the UI
//  with masks and offsets at various locations, these are them.
#define ACE_UI_INDEX_STATUS_MAIN 0
#define ACE_UI_INDEX_STATUS_LEFT 1
#define ACE_UI_INDEX_STATUS_RIGHT 2
#define ACE_UI_INDEX_COMBAT_ALLY 3
#define ACE_UI_INDEX_COMBAT_MAIN 4
#define ACE_UI_INDEX_BASEMENU_MAIN 5
#define ACE_UI_INDEX_DOCTOR_MAIN 6
#define ACE_UI_INDEX_VICTORY_MAIN 7
#define ACE_UI_INDEX_EQUIP_MAIN 8
#define ACE_UI_INDEX_EQUIP_LEFT 9
#define ACE_UI_INDEX_EQUIP_RIGHT 10
#define ACE_UI_INDEX_TOTAL 11

//--Stencil Codes
#define ACE_STENCIL_ACTIVE_CHARACTER_PORTRAIT 1
#define ACE_STENCIL_ALLY_PORTRAIT_START 2
#define ACE_STENCIL_KNOCKOUT 10

//--[Battlefield Position Code]
#define AC_POSCODE_DIRECT 0
#define AC_POSCODE_PLAYER_PARTY 1
#define AC_POSCODE_ENEMY_PARTY 2

//--[Ability Grid]
#define ACE_ABILITY_GRID_SIZE_X 10
#define ACE_ABILITY_GRID_SIZE_Y 3

//--[AI Script Codes]
#define ACE_AI_SCRIPT_CODE_COMBATSTART 0
#define ACE_AI_SCRIPT_CODE_TURNSTART 1
#define ACE_AI_SCRIPT_CODE_ACTIONBEGIN 2
#define ACE_AI_SCRIPT_CODE_FREEACTIONBEGIN 3
#define ACE_AI_SCRIPT_CODE_ACTIONEND 4
#define ACE_AI_SCRIPT_CODE_TURNEND 5
#define ACE_AI_SCRIPT_CODE_KNOCKEDOUT 6
#define ACE_AI_SCRIPT_CODE_COMBATENDS 7
#define ACE_AI_SCRIPT_CODE_APPLICATION_START 8
#define ACE_AI_SCRIPT_CODE_APPLICATION_END 9

//--[Ability Script Codes]
#define ACA_SCRIPT_CODE_CREATE 0
#define ACA_SCRIPT_CODE_ASSUMEJOB 1
#define ACA_SCRIPT_CODE_BEGINCOMBAT 2
#define ACA_SCRIPT_CODE_BEGINTURN 3
#define ACA_SCRIPT_CODE_BEGINACTION 4
#define ACA_SCRIPT_CODE_BEGINFREEACTION 5
#define ACA_SCRIPT_CODE_POSTACTION 6
#define ACA_SCRIPT_CODE_PAINTTARGETS 7
#define ACA_SCRIPT_CODE_PAINTTARGETS_RESPONSE 8
#define ACA_SCRIPT_CODE_EXECUTE 9
#define ACA_SCRIPT_CODE_TURNENDS 10
#define ACA_SCRIPT_CODE_COMBATENDS 11
#define ACA_SCRIPT_CODE_SPECIALCREATE 12 //Used by Lua, reserved
#define ACA_SCRIPT_CODE_GUI_APPLY_EFFECT 13
#define ACA_SCRIPT_CODE_EVENT_QUEUED 14
#define ACA_SCRIPT_CODE_BUILD_PREDICTION_BOX 15
#define ACA_SCRIPT_CODE_QUERY_CAN_RUN 16
#define ACA_SCRIPT_CODE_TOTAL 17

//--[Effect Script Codes]
#define ACEFF_SCRIPT_CODE_CREATE 0
#define ACEFF_SCRIPT_CODE_APPLYSTATS 1
#define ACEFF_SCRIPT_CODE_UNAPPLYSTATS 2
#define ACEFF_SCRIPT_CODE_BEGINTURN 3
#define ACEFF_SCRIPT_CODE_BEGINACTION 4
#define ACEFF_SCRIPT_CODE_BEGINFREEACTION 5
#define ACEFF_SCRIPT_CODE_POSTACTION 6
#define ACEFF_SCRIPT_CODE_TURNENDS 7
#define ACEFF_SCRIPT_CODE_COMBATENDS 8

//--[Entity Script Codes]
#define ACE_SCRIPT_CODE_CREATE 0
#define ACE_SCRIPT_CODE_BEGINCOMBAT 1
#define ACE_SCRIPT_CODE_BEGINTURN 2
#define ACE_SCRIPT_CODE_BEGINACTION 3
#define ACE_SCRIPT_CODE_BEGINFREEACTION 4
#define ACE_SCRIPT_CODE_ENDACTION 5
#define ACE_SCRIPT_CODE_ENDTURN 6
#define ACE_SCRIPT_CODE_ENDCOMBAT 7
#define ACE_SCRIPT_CODE_EVENTQUEUED 8
#define ACE_SCRIPT_CODE_TOTAL 9

//--[Query Code]
//--This code can theoretically be used in a lot of different scripts, so it's really high
//  so as not to interfere with other codes.
#define QUERY_SCRIPT_CODE 1000

//--[Turn Order]
//--Amount of random scatter on initiative rolls.
#define ACE_TURN_ORDER_RANGE 40

//--Turn-order buckets
#define ACE_BUCKET_ALWAYS_STRIKES_LAST 2
#define ACE_BUCKET_SLOW 1
#define ACE_BUCKET_FAST -1
#define ACE_BUCKET_ALWAYS_STRIKES_FIRST -2

//--Turn-order bucket clamps
#define ACE_BUCKET_LOWEST 0
#define ACE_BUCKET_NEUTRAL 3
#define ACE_BUCKET_HIGHEST 6
#define ACE_BUCKET_TOTAL 7

//--Turn-order display ticks
#define ACE_TURN_TICKS_PER_LETTER 10
#define ACE_TURN_TICKS_HOLD 15
#define ACE_TURN_TICKS_FADE 15
#define ACE_TURN_DISPLAY_TICKS ((ACE_TURN_TICKS_PER_LETTER * 5) + ACE_TURN_TICKS_HOLD + ACE_TURN_TICKS_FADE)

//--[Resistances/Damage Types]
//--There are 12 and each gets it own lovely little icon. Protection is "All direct damage" and is typeless,
//  but doesn't help against DoTs and effect application.
//--Note: This is parallel with the Lua variables [gciDamageType_Slashing to gciDamageType_Terrifying]
//  but is offset by ADVC_DAMAGE_OFFSETODAMAGETYPE to handle the protection being present. This is
//  because protection is a resistance type but not a damage type.
#define ADVC_DAMAGE_PROTECTION 0
#define ADVC_DAMAGE_SLASHING 1
#define ADVC_DAMAGE_STRIKING 2
#define ADVC_DAMAGE_PIERCING 3
#define ADVC_DAMAGE_FLAMING 4
#define ADVC_DAMAGE_FREEZING 5
#define ADVC_DAMAGE_SHOCKING 6
#define ADVC_DAMAGE_CRUSADING 7
#define ADVC_DAMAGE_OBSCURING 8
#define ADVC_DAMAGE_BLEEDING 9
#define ADVC_DAMAGE_POISONING 10
#define ADVC_DAMAGE_CORRODING 11
#define ADVC_DAMAGE_TERRIFYING 12
#define ADVC_DAMAGE_TOTAL 13

#define ADVC_DAMAGE_OFFSETODAMAGETYPE -1

//--[Combat Statistics]
//--List of statistics.
#define STATS_HPMAX 0
#define STATS_MPMAX 1
#define STATS_MPREGEN 2
#define STATS_CPMAX 3
#define STATS_FREEACTIONMAX 4
#define STATS_FREEACTIONGEN 5
#define STATS_ATTACK 6
#define STATS_INITIATIVE 7
#define STATS_ACCURACY 8
#define STATS_EVADE 9
#define STATS_RESIST_START 10
#define STATS_RESIST_PROTECTION 10
#define STATS_RESIST_SLASH 11
#define STATS_RESIST_STRIKE 12
#define STATS_RESIST_PIERCE 13
#define STATS_RESIST_FLAME 14
#define STATS_RESIST_FREEZE 15
#define STATS_RESIST_SHOCK 16
#define STATS_RESIST_CRUSADE 17
#define STATS_RESIST_OBSCURE 18
#define STATS_RESIST_BLEED 19
#define STATS_RESIST_POISON 20
#define STATS_RESIST_CORRODE 21
#define STATS_RESIST_TERRIFY 22
#define STATS_RESIST_END 22
#define STATS_STUN_CAP 23
#define STATS_THREAT_MULTIPLIER 24
#define STATS_TOTAL 25

//--Combat Inspector Statistic Slots
#define CI_STATS_SLOT_HPMAX 0
#define CI_STATS_SLOT_MPMAX 1
#define CI_STATS_SLOT_MPREGEN 2
#define CI_STATS_SLOT_ATTACK 3
#define CI_STATS_SLOT_INITIATIVE 4
#define CI_STATS_SLOT_ACCURACY 5
#define CI_STATS_SLOT_EVADE 6
#define CI_STATS_SLOT_THREAT_MULTIPLIER 7
#define CI_STATS_SLOT_TOTAL 8

//--Combat Inspector Resistance Slots
#define CI_RESIST_SLOT_PROTECTION 0
#define CI_RESIST_SLOT_SLASH 1
#define CI_RESIST_SLOT_STRIKE 2
#define CI_RESIST_SLOT_PIERCE 3
#define CI_RESIST_SLOT_FLAME 4
#define CI_RESIST_SLOT_FREEZE 5
#define CI_RESIST_SLOT_SHOCK 6
#define CI_RESIST_SLOT_CRUSADE 7
#define CI_RESIST_SLOT_OBSCURE 8
#define CI_RESIST_SLOT_BLEED 9
#define CI_RESIST_SLOT_POISON 10
#define CI_RESIST_SLOT_CORRODE 11
#define CI_RESIST_SLOT_TERRIFY 12
#define CI_RESIST_SLOT_TOTAL 13

//--[CombatStatistics]
//--Represents a set of combat stats, like HP, MP, Attack Power, etc. These are used
//  to store and compute buffs and whatnot.
//--The stats can be accessed via a list array.
//--The functions are implemented in AdvCombatStats.cc
typedef struct CombatStatistics
{
    //--Members
    int mValueList[STATS_TOTAL];

    //--Functions
    void Initialize();
    void Zero();
    int GetStatByIndex(int pIndex);
    void SetStatByIndex(int pIndex, int pValue);
    void SetHPMax(int pValue);
    void SetInitiative(int pValue);
    void AddStatistics(CombatStatistics pPackage);
    void ClampStatistics();
}CombatStatistics;

//--[DamageTypes]
//--Contains a floating point representing the percentage of a damage type that should be used
//  for something, typically a piece of equipment.
typedef struct DamageTypes
{
    float mValueList[ADVC_DAMAGE_TOTAL];
    void Initialize()
    {
        for(int i = 0; i < ADVC_DAMAGE_TOTAL; i ++) mValueList[i] = 0.0f;
    }
}DamageTypes;

//--[World Reference]
//--Package containing a TilemapActor and a number of turns.
typedef struct WorldRefPack
{
    int mTurns;
    TilemapActor *rActor;
    void Initialize()
    {
        mTurns = 0;
        rActor = NULL;
    }
}WorldRefPack;

//--[Equipment]
//--Maximum length of equipment type name. Common names are "Weapon A" and "Armor".
#define EQP_TYPE_MAX_LETTERS 32

//--[Target Cluster]
//--Represents a group of targets. Uses a SugarLinkedList * to store them. Implemented in
//  AdvCombatTargeting.cc.
typedef struct TargetCluster
{
    //--Static.
    static uint32_t xTargetClusterIDs;

    //--Storage.
    uint32_t mUniqueID;
    char *mDisplayName;
    SugarLinkedList *mrPredictionBoxList; //AdvCombatPrediction *, ref
    SugarLinkedList *mrTargetList; //AdvCombatEntity *, ref

    //--Functions.
    void Initialize();
    static void DeleteThis(void *pPtr);
    void SetDisplayName(const char *pName);
    bool IsElementInCluster(void *pPtr);
    void RegisterElement(void *pElement);
    void RemoveElementI(int pSlot);
    void RemoveElementP(void *pPtr);
    TargetCluster *Clone();
}TargetCluster;

//--[CombatEventPack]
//--Represents an action which is an ability acting on a set of targets. Each turn consists of a set of
//  Entities performing Actions, which contains one or more Events. Sheesh.
//--Must own its target cluster, as multiple events may execute and clear the master list of clusters. All
//  other pointers can safely be references.
typedef struct CombatEventPack
{
    //--Members.
    int mPriority;
    AdvCombatEntity *rOriginator;
    AdvCombatAbility *rAbility;
    TargetCluster *mTargetCluster;

    //--Functions.
    void Initialize()
    {
        mPriority = 0;
        rOriginator = NULL;
        rAbility = NULL;
        mTargetCluster = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        CombatEventPack *rPtr = (CombatEventPack *)pPtr;
        delete rPtr->mTargetCluster;
        free(rPtr);
    }
}CombatEventPack;

//--[ApplicationPack]
//--Represents a change in HP, MP, Effect application, or anything else. These occur at specific times
//  when an event occurs to make sure HP bars scroll appropriately.
typedef struct ApplicationPack
{
    //--Members
    int mTicks;
    char *mEffectString;
    uint32_t mOriginatorID;
    uint32_t mTargetID;

    //--Functions
    void Initialize()
    {
        mTicks = 0;
        mEffectString = NULL;
        mOriginatorID = 0;
        mTargetID = 0;
    }
    static void DeleteThis(void *pPtr)
    {
        ApplicationPack *rPtr = (ApplicationPack *)pPtr;
        free(rPtr->mEffectString);
        free(rPtr);
    }
}ApplicationPack;

//--[CombatTextPack]
//--Used to indicate damage/effect application in combat.
typedef struct CombatTextPack
{
    //--Members
    int mTicks;
    int mTicksMax;
    char *mText;
    float mX;
    float mY;
    float mScale;
    StarlightColor mColor;

    //--Functions
    void Initialize()
    {
        mTicks = 0;
        mTicksMax = 1;
        mText = NULL;
        mX = 0.0f;
        mY = 0.0f;
        mScale = 1.0f;
        mColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    }
    static void DeleteThis(void *pPtr)
    {
        CombatTextPack *rPtr = (CombatTextPack *)pPtr;
        free(rPtr->mText);
        free(rPtr);
    }
}CombatTextPack;

//--[EquipmentSlotPack]
//--Used to mark a given equipment slot. Contains a name for the slot and optionally a piece
//  of equipment, which it assumes ownership of.
typedef struct EquipmentSlotPack
{
    //--Members
    bool mIsComputedForStats;
    bool mCanBeEmpty;
    AdventureItem *mEquippedItem;

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
}EquipmentSlotPack;

//--[ExtraScriptPack]
//--A script that executes during combat. Always goes first in execution priority.
typedef struct ExtraScriptPack
{
    //--Members
    char *mPath;

    //--Functions
    void Initialize()
    {
        mPath = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        ExtraScriptPack *rPackage = (ExtraScriptPack *)pPtr;
        free(rPackage->mPath);
        free(rPackage);
    }
}ExtraScriptPack;
