//--Base
#include "AdvCombatDefStruct.h"

//--Classes
#include "AdventureItem.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers

void EquipmentSlotPack::Initialize()
{
    mIsComputedForStats = true;
    mCanBeEmpty = false;
    mEquippedItem = NULL;
}
void EquipmentSlotPack::DeleteThis(void *pPtr)
{
    EquipmentSlotPack *rPtr = (EquipmentSlotPack *)pPtr;
    delete rPtr->mEquippedItem;
    free(rPtr);
}
