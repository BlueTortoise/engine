//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

//======================================== Calculations ===========================================
float AdvCombat::ComputeIdealX(bool pIsLeftSide, int pPosition, int pTotalEntries)
{
    //--Calculates the middle point where an entity should be standing, given a side of the screen
    //  and how many entities are sharing it with them.
    //--First, the player's party. This typically contains as many as four entries (but can have more)
    //  so is given 1/3rd of screen real estate. The 0th entity is considered to be the acting entity
    //  but might also just be the first in the party order if receiving hits from an enemy.
    if(pIsLeftSide)
    {
        //--If there is one entity, or fewer (for range-checks):
        if(pTotalEntries <= 1)
        {
            return VIRTUAL_CANVAS_X * 0.25f;
        }

        //--If we got this far, there are at least two total entities. Divvy the space from the rightmost
        //  position to the left edge with padding.
        float cLftPoint = VIRTUAL_CANVAS_X * 0.05f;
        float cRgtPoint = VIRTUAL_CANVAS_X * 0.30f;
        float cSpacePerEntity = (cRgtPoint - cLftPoint) / (float)pTotalEntries;

        //--Multiply, and add a half-slot to center the entity.
        return cRgtPoint - (cSpacePerEntity * (float)(pPosition + 0.50f));
    }

    //--Enemy party. First, exactly one entry and a range-check:
    if(pTotalEntries <= 1)
    {
        return VIRTUAL_CANVAS_X * 0.65f;
    }

    //--If we got this far, there are at least two total entities. Divvy the space from the leftmost
    //  position to the right edge with padding.
    float cLftPoint = VIRTUAL_CANVAS_X * 0.55f;
    float cRgtPoint = VIRTUAL_CANVAS_X * 0.95f;
    float cSpacePerEntity = (cRgtPoint - cLftPoint) / (float)pTotalEntries;

    //--Multiply, and add a half-slot to center the entity.
    return cLftPoint + (cSpacePerEntity * (float)(pPosition + 0.50f));
}
float AdvCombat::ComputeOffscreenX(bool pIsLeftSide, SugarBitmap *pImage)
{
    //--Computes the expected offscreen position, taking into account the width of the image.
    //  Player party is on the left.
    if(pIsLeftSide)
    {
        float cLftPoint = ADVCOMBAT_POSITION_OFFSCREEN_LFT;
        if(pImage) cLftPoint = cLftPoint - (pImage->GetTrueWidth() * 0.50f);
        return cLftPoint;
    }

    //--Right side.
    float cRgtPoint = ADVCOMBAT_POSITION_INTRO_OFFSCREEN;
    if(pImage) cRgtPoint = cRgtPoint + (pImage->GetTrueWidth() * 1.00f);
    return cRgtPoint;
}

//======================================== Core Methods ===========================================
void AdvCombat::PositionCharactersOnScreenForEvent(CombatEventPack *pEvent)
{
    //--After all events are registered, position on-screen all characters who are involved with the event.
    //  Pass NULL to move all party members offscreen.
    if(!pEvent)
    {
        PositionCharactersOffScreen();
        return;
    }

    //--Temporary list.
    SugarLinkedList *tOnscreenPartyList = new SugarLinkedList(false);

    //--Iterate across the party.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        //--Entity is involved, add it to the onscreen list.
        if(IsEntityInvolvedInEvent(rEntity, pEvent))
        {
            tOnscreenPartyList->AddElementAsTail("X", rEntity);
        }
        //--Not involved, order them offscreen.
        else
        {
            float tX = ComputeOffscreenX(true, rEntity->GetCombatPortrait());
            rEntity->SetIdealPositionX(tX);
            rEntity->MoveToIdealPosition(ADVCOMBAT_STD_MOVE_TICKS);
        }

        //--Next.
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }

    //--Order all the onscreen entities to move to their positions.
    int i = 0;
    rEntity = (AdvCombatEntity *)tOnscreenPartyList->PushIterator();
    while(rEntity)
    {
        //--Compute Y position. Rendering is centered, but we start with bottom-up.
        float cRenderY = ADVCOMBAT_POSITION_STD_Y;
        SugarBitmap *rRenderImg = rEntity->GetCombatPortrait();
        if(rRenderImg) cRenderY = cRenderY - (rRenderImg->GetHeight() * 0.50f);

        //--Compute and set.
        float tX = ComputeIdealX(true, i, tOnscreenPartyList->GetListSize());
        rEntity->SetIdealPosition(tX, cRenderY);
        rEntity->MoveToIdealPosition(ADVCOMBAT_STD_MOVE_TICKS);

        //--Next.
        i ++;
        rEntity = (AdvCombatEntity *)tOnscreenPartyList->AutoIterate();
    }

    //--Clean.
    delete tOnscreenPartyList;
}
void AdvCombat::PositionListOnscreen(bool pIsLeftSide, int pTicks, SugarLinkedList *pList)
{
    //--Given a list containing AdvCombatEntitys, computes and positions all of them as a cohesive party group.
    if(!pList) return;

    //--Setup.
    int i = 0;
    int tListSize = pList->GetListSize();

    //--Iterate.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)pList->PushIterator();
    while(rEntity)
    {
        //--Compute Y position. Rendering is centered, but we start with bottom-up.
        float cRenderY = ADVCOMBAT_POSITION_STD_Y;
        SugarBitmap *rRenderImg = rEntity->GetCombatPortrait();
        if(rRenderImg) cRenderY = cRenderY - (rRenderImg->GetHeight() * 0.50f);

        //--Position.
        float tX = ComputeIdealX(pIsLeftSide, i, tListSize);
        rEntity->SetIdealPosition(tX, cRenderY);
        rEntity->MoveToIdealPosition(pTicks);

        //--Next.
        i ++;
        rEntity = (AdvCombatEntity *)pList->AutoIterate();
    }
}
void AdvCombat::PositionListOffscreen(bool pIsLeftSide, int pTicks, SugarLinkedList *pList)
{
    //--Given a list containing AdvCombatEntitys, computes and positions all of them offscreen.
    if(!pList) return;

    //--Iterate.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)pList->PushIterator();
    while(rEntity)
    {
        //--Position.
        float tX = ComputeOffscreenX(pIsLeftSide, rEntity->GetCombatPortrait());
        rEntity->SetIdealPositionX(tX);
        rEntity->MoveToIdealPosition(pTicks);

        //--Next.
        rEntity = (AdvCombatEntity *)pList->AutoIterate();
    }
}

//============================================ Macros =============================================
void AdvCombat::PositionCharactersNormally()
{
    //--First, order characters offscreen.
    PositionCharactersOffScreen();

    //--Next, see if there is an event. If so, position with that in mind.
    CombatEventPack *rEventPackage = (CombatEventPack *)mEventQueue->GetElementBySlot(0);
    if(rEventPackage)
    {
        PositionCharactersOnScreenForEvent(rEventPackage);
        return;
    }

    //--See if there is an acting party member.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity || !IsEntityInPlayerParty(rActingEntity)) return;

    //--We have an acting entity, so mark them as onscreen.
    float cIdealX = ComputeIdealX(true, 0, 1);
    rActingEntity->SetIdealPositionX(cIdealX);
    rActingEntity->MoveToIdealPosition(ADVCOMBAT_STD_MOVE_TICKS);
}
void AdvCombat::PositionCharactersOffScreen()
{
    PositionListOffscreen(true, ADVCOMBAT_STD_MOVE_TICKS, mrCombatParty);
}
void AdvCombat::PositionEnemies()
{
    PositionListOnscreen(false, ADVCOMBAT_STD_MOVE_TICKS, mrEnemyCombatParty);
}
