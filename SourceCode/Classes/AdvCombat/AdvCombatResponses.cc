//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"

//--[Core]
void AdvCombat::RunAllResponseScripts(int pCode)
{
    //--Range check.
    if(pCode < 0 || pCode >= ADVCOMBAT_RESPONSE_TOTAL) return;

    //--Script Response
    ExtraScriptPack *rPack = (ExtraScriptPack *)mExtraScripts->PushIterator();
    while(rPack)
    {
        if(rPack->mPath)
        {
            LuaManager::Fetch()->ExecuteLuaFile(rPack->mPath, 1, "N", (float)pCode);
        }
        rPack = (ExtraScriptPack *)mExtraScripts->AutoIterate();
    }

    //--Entity Response
    RunPartyResponseScripts(cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][pCode]);
    RunEnemyResponseScripts(cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ENTITY][pCode]);

    //--AI Response
    RunPartyAIScripts(cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][pCode]);
    RunEnemyAIScripts(cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_AI][pCode]);

    //--Job Response
    RunPartyJobScripts(cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][pCode]);
    RunEnemyJobScripts(cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_JOB][pCode]);

    //--Ability Response
    RunPartyAbilityScripts(cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][pCode]);
    RunEnemyAbilityScripts(cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_ABILITY][pCode]);

    //--Effect Response
    RunEffects(cResponseCodes[ADVCOMBAT_RESPONSE_TYPE_EFFECT][pCode]);
}

//--[Party]
void AdvCombat::RunPartyResponseScripts(int pCode)
{
    if(pCode == -1) return;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        rEntity->CallResponseScript(pCode);
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }
}
void AdvCombat::RunPartyAIScripts(int pCode)
{
    if(pCode == -1) return;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        rEntity->CallAIResponseScript(pCode);
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }
}
void AdvCombat::RunPartyJobScripts(int pCode)
{
    if(pCode == -1) return;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        AdvCombatJob *rActiveJob = rEntity->GetActiveJob();
        if(rActiveJob) rActiveJob->CallScript(pCode);
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }
}
void AdvCombat::RunPartyAbilityScripts(int pCode)
{
    if(pCode == -1) return;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrCombatParty->PushIterator();
    while(rEntity)
    {
        rEntity->RunAbilityScripts(pCode);
        rEntity = (AdvCombatEntity *)mrCombatParty->AutoIterate();
    }
}

//--[Enemy]
void AdvCombat::RunEnemyResponseScripts(int pCode)
{
    if(pCode == -1) return;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEntity)
    {
        rEntity->CallResponseScript(pCode);
        rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }
}
void AdvCombat::RunEnemyAIScripts(int pCode)
{
    if(pCode == -1) return;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEntity)
    {
        rEntity->CallAIResponseScript(pCode);
        rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }
}
void AdvCombat::RunEnemyJobScripts(int pCode)
{
    if(pCode == -1) return;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEntity)
    {
        AdvCombatJob *rActiveJob = rEntity->GetActiveJob();
        if(rActiveJob) rActiveJob->CallScript(pCode);
        rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }
}
void AdvCombat::RunEnemyAbilityScripts(int pCode)
{
    if(pCode == -1) return;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEntity)
    {
        rEntity->RunAbilityScripts(pCode);
        rEntity = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }
}

//--[Effects]
void AdvCombat::RunEffects(int pCode)
{
    //--Note: Application of statistics by effects should not be handled here. It should be called by
    //  AdvCombatEffect::ApplyStatsToTargets() instead, usually after construction.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mGlobalEffectList->SetToHeadAndReturn();
    while(rEffect)
    {
        //--Get script.
        const char *rScript = rEffect->GetScriptPath();

        //--Execute.
        LuaManager::Fetch()->PushExecPop(rEffect, rScript, 1, "N", (float)pCode);

        //--If the script flagged destruction for the effect, put it on the effect graveyard.
        if(rEffect->IsRemovedNow())
        {
            //--Flag.
            mMustRebuildEffectReferences = true;

            //--Before removal, ask the effect to remove any stat modifications it made.
            rEffect->UnApplyStatsToTargets();

            //--Remove, place on graveyard.
            mGlobalEffectList->LiberateRandomPointerEntry();
            mEffectGraveyard->AddElement("X", rEffect, &RootObject::DeleteThis);
        }

        //--Next.
        rEffect = (AdvCombatEffect *)mGlobalEffectList->IncrementAndGetRandomPointerEntry();
    }
}
