//--Base
#include "AdvCombatEntity.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

bool AdvCombatEntity::IsNormalTarget()
{
    //--A "Normal" target is one who has an HP over 0 and is not afflicted by some status effect which prevents damage.
    //  The definition of normal is fairly loose, but most hostile attacks target normal targets.
    if(mHP < 1) return false;

    //--All checks passed.
    return true;
}
