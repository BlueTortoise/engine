//--[AdvCombatEntity]
//--Represents an entity in combat, either party member or enemy.

//--[Statistics Information]
//--The AdvCombatEntity has many sets of statistics. A description of the types follows:
//--mBaseStatistics
//  Base statistics for the character that never change. These are not influenced by levels.
//  For enemies, these are the statistics they use as enemies never change jobs or use equipment.
//--mJobStatistics
//  Stat bonuses applied from the character's current job. Can change in combat.
//--mEquipmentStatistics
//  Stat bonuses applied from the character's equipment. Changes when they change equipment. Includes
//  all gem bonuses.
//--mScriptStatistics
//  Bonuses as applied by scripts, used for plot events. Under normal circumstances, these are zeroes.
//--mPermaEffectStatistics
//  Bonuses applied by effects that are permanent in some way. For example, some jobs have passives
//  that apply on top of their base job statistics, or effects that are applied by abilities equipped.
//--mTemporaryEffectStatistics
//  Bonuses from effects that are currently applied. These expire at the end of combat.
//--mSummedStatistics
//  All stats added together. This is the set that is queried in combat.

//--[Stunning Information]
//--If an entity is stunnable, they have a stun value. The standard stun value is 100, but this is set
//  as a statistic and can be changed by Effects, Equipment, etc.
//--When an entity receives stun damage, at the start of their action they check the stun value. If it
//  is less than their stun cap, they decrement it by (StunCap / ADVCE_STUN_DECREMENT_DIVISOR).
//--If it is greater than or equal to the stun cap, they decrement it by the stun cap and pass their turn.
//--When stunned, the stun resist increases by 1. The max stun resist is 3. The stun damage taken is checked
//  against the matching ADVCE_STUN_RESIST_FACTORX, which reduces it.
//--Every ADVCE_STUN_TURNS_TO_DECREMENT, the stun resist decreases by 1, minimum 0. The counter resets when
//  the entity is stunned.
//--The maximum amount of stun that can be accumulated at once is (StunCap * ADVCE_STUN_CAP_FROM_BASE_FACTOR).
//  Stun damage over this amount is lost.
//--Important: All stun handling is script driven. AIs can choose to ignore stun. Player scripts or effects
//  may cause stun to be ignored. Stun will continue to accumulate even if the entity is immune.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "AdvCombatDefStruct.h"

//--[Local Structures]
//--[Local Definitions]
//--Statistic Groupings
#define ADVCE_STATS_BASE 0
#define ADVCE_STATS_JOB 1
#define ADVCE_STATS_EQUIPMENT 2
#define ADVCE_STATS_SCRIPT 3
#define ADVCE_STATS_PERMAEFFECT 4
#define ADVCE_STATS_TEMPEFFECT 5
#define ADVCE_STATS_FINAL 6
#define ADVCE_STATS_TOTAL 7

//--Easing
#define ADVCE_EASING_TICKS 15

//--Flashing
#define ADVCE_FLASH_WHITE_TICKS 15
#define ADVCE_FLASH_BLACK_TICKS 15
#define ADVCE_FLASH_BLACK_SEGMENT 5

//--Knockout
#define ADVCE_KNOCKOUT_TOWHITE_TICKS 20
#define ADVCE_KNOCKOUT_TOBLACK_TICKS 20

//--Vendor Sprites
#define ADVCE_VENDOR_SPRITES_TOTAL 4

//--Stun
#define ADVCE_STUN_CAP_FROM_BASE_FACTOR 2
#define ADVCE_STUN_TURNS_TO_DECREMENT 3
#define ADVCE_STUN_DECREMENT_DIVISOR 5

//--[Classes]
class AdvCombatEntity : public RootObject
{
    private:
    //--System
    char *mLocalName;
    char *mDisplayName;
    char *mResponseScriptPath;
    bool mScriptResponses[ACE_SCRIPT_CODE_TOTAL];

    //--Persistent Statistics
    bool mIsKnockedOut;
    int mTurnKOd;
    int mHP;
    int mMP;
    int mCP;
    int mShield;
    int mAdrenaline;
    int mFreeActions;
    int mFreeActionsPerformed;
    EasingPack1D mHPEasingPack;
    EasingPack1D mMPEasingPack;
    EasingPack1D mAdrenalineEasingPack;
    EasingPack1D mShieldEasingPack;

    //--Stun Handling
    bool mIsStunnable;
    int mStunValue;
    int mStunResist;
    int mStunResistDecrementTurns;
    EasingPack1D mStunEasingPack;

    //--AI
    bool mIsAmbushed;
    char *mAIScriptPath;

    //--Combat Position
    bool mIgnoreMovementForBlocking;
    float mCombatIdealX;
    float mCombatIdealY;
    EasingPack2D mCombatPosition;

    //--Turn Order
    int mTurnID;
    int mCurrentInitiative;

    //--Statistics
    CombatStatistics mBaseStatistics;
    CombatStatistics mJobStatistics;
    CombatStatistics mEquipmentStatistics;
    CombatStatistics mScriptStatistics;
    CombatStatistics mPermaEffectStatistics;
    CombatStatistics mTemporaryEffectStatistics;
    CombatStatistics mSummedStatistics;

    //--Levels
    int mLevel;
    int mTotalXP;

    //--Jobs
    int mGlobalJP;
    AdvCombatJob *rActiveJobAtCombatStart;
    AdvCombatJob *rActiveJob;
    SugarLinkedList *mJobList; //AdvCombatJob *, master

    //--Rewards
    int mRewardXP;
    int mRewardJP;
    int mRewardPlatina;
    int mRewardDoctor;
    SugarLinkedList *mRewardItems; //const char *, master

    //--Reinforcement Handling
    int mReinforcementTurns;

    //--Ability Listing
    SugarLinkedList *mAbilityList; //AdvCombatAbility *, master
    AdvCombatAbility *rAbilityGrid[ACE_ABILITY_GRID_SIZE_X][ACE_ABILITY_GRID_SIZE_Y];
    AdvCombatAbility *rSecondaryGrid[ACE_ABILITY_GRID_SIZE_X][ACE_ABILITY_GRID_SIZE_Y];
    SugarLinkedList *mrPassiveAbilityList; //AdvCombatAbility *, ref

    //--Equipment Listing
    int mSlotForWeaponDamage;
    int mSlotOfOriginalWeapon;
    SugarLinkedList *mEquipmentSlotList; //EquipmentSlotPack *, master

    //--UI Positions
    TwoDimensionRealPoint mUIPositions[ACE_UI_INDEX_TOTAL];
    TwoDimensionReal mFaceTableDim;

    //--Effect Handling
    SugarLinkedList *mrEffectsToShowThisTick; //AdvCombatEffect *, ref

    //--Tags
    SugarLinkedList *mTagList; //int *, master

    //--Images
    bool mIsFlashingWhite;
    int mWhiteFlashTimer;
    bool mIsFlashingBlack;
    int mBlackFlashTimer;
    bool mIsKnockingOut;
    int mKnockoutTimer;
    SugarBitmap *rCombatImage;
    SugarBitmap *rCombatCountermask;
    SugarBitmap *rVictoryCountermask;
    SugarBitmap *rTurnIcon;
    SugarBitmap *rVendorImages[ADVCE_VENDOR_SPRITES_TOTAL];
    SugarBitmap *rFaceTableImg;

    protected:

    public:
    //--System
    AdvCombatEntity();
    virtual ~AdvCombatEntity();
    void Reinitialize();

    //--Public Variables
    bool mLeveledUpThisBattle;
    int mLevelLastTick;

    //--Public Static Variables
    static int xMaxLevel;
    static int *xExpTable;

    //--Property Queries
    const char *GetName();
    const char *GetDisplayName();
    const char *GetResponsePath();
    const char *GetAIPath();
    bool IsAmbushed();
    bool RespondsToCode(int pCode);
    bool IsStunnable();
    bool IsStunnedThisTurn();
    int GetDisplayStun();
    float GetDisplayHPPct();
    float GetDisplayMPPct();
    float GetDisplayAdrenaline();
    float GetDisplayShield();
    int GetReinforcementTurns();
    SugarBitmap *GetFaceProperties(TwoDimensionReal &sDimensions);
    bool IsMoving();
    float GetCombatX();
    float GetCombatY();
    TwoDimensionRealPoint GetUIRenderPosition(int pIndex);
    bool CanActThisTurn();
    bool IsDefeated();
    bool IsAnimating();
    int GetCurrentInitiative();
    int ComputeTurnBucket();
    int GetTurnID();
    int GetWhiteFlashTimer();
    bool IsFlashingBlack();
    bool IsKnockingOut();
    bool IsKnockoutFinished();
    int GetKnockoutTimer();
    int GetGlobalJP();
    int GetFreeActions();
    int GetFreeActionsPerformed();
    int GetRewardXP();
    int GetRewardJP();
    int GetRewardPlatina();
    int GetRewardDoctor();
    int GetTurnKOd();
    SugarLinkedList *GetRewardList();
    int GetEffectsWithTag(const char *pTag);
    int GetEffectIDWithTag(const char *pTag, int pSlot);
    int GetTagCount(const char *pTag);

    //--Property Queries - World Statistics
    int GetLevel();
    int GetXP();
    int GetXPOfLevel();
    int GetXPToNextLevel();
    int GetXPToNextLevelMax();

    //--Manipulators
    void SetInternalName(const char *pName);
    void SetDisplayName(const char *pName);
    void SetResponseScript(const char *pPath);
    void SetResponseScriptCode(int pCode, bool pFlag);
    void SetAIScript(const char *pPath);
    void SetAmbushed(bool pIsAmbushed);
    void SetReinforcementTurns(int pTurns);
    void RegisterJob(const char *pName, AdvCombatJob *pJob);
    void SetActiveJob(const char *pJobName);
    void RevertToCombatStartJob();
    void SetCombatPortrait(const char *pDLPath);
    void SetCombatCountermask(const char *pDLPath);
    void SetVictoryCountermask(const char *pDLPath);
    void SetTurnIcon(const char *pDLPath);
    void SetVendorImage(int pSlot, const char *pDLPath);
    void SetUIRenderPos(int pSlot, int pX, int pY);
    void SetFaceTableData(float pLft, float pTop, float pRgt, float pBot, const char *pPath);
    void SetIdealPosition(float pXPos, float pYPos);
    void SetIdealPositionX(float pXPos);
    void MoveToPosition(float pXPos, float pYPos, int pTicks);
    void MoveToIdealPosition(int pTicks);
    void SetIgnoreMovementForBlocking(bool pFlag);
    void SetCurrentInitiative(int pValue);
    void SetTurnID(int pValue);
    void SetFlashingWhite(bool pFlag);
    void FlashBlack();
    void BeginKnockout();
    void FinishKnockout();
    void SetGlobalJP(int pAmount);
    void SetFreeActions(int pFreeActions);
    void SetFreeActionsPerformed(int pFreeActionsPerformed);
    void ResetFreeActionsToTurnStart();
    void SetXP(int pXP);
    void ComputeLevelStatistics(int pLevel);
    void GainJP(int pJP);
    void GainGlobalJP(int pJP);
    void SetRewardXP(int pXP);
    void SetRewardJP(int pJP);
    void SetRewardPlatina(int pPlatina);
    void SetRewardDoctor(int pDoctor);
    void SetTurnKOd(int pTurn);
    void AddRewardItem(const char *pName);
    void AddTag(const char *pTag);
    void RemoveTag(const char *pTag);

    //--Core Methods
    void FullRestore();
    void HandleCombatStart();
    void HandleCombatEnd();
    bool CheckKnockout();
    void CallResponseScript(int pCode);
    void CallAIResponseScript(int pCode);

    //--Abilities
    bool IsAbilityEquipped(AdvCombatAbility *pCheckAbility);
    bool IsAbilityEquippedS(const char *pName);
    const char *GetAbilityNameInSlot(int pX, int pY);
    void RegisterAbility(const char *pAbilityName, AdvCombatAbility *pAbility);
    void RegisterAbilityToJob(const char *pAbilityName, const char *pJobName);
    void RemoveAbility(const char *pAbilityName);
    void SetAbilitySlot(int pSlotX, int pSlotY, const char *pAbilityName);
    void SetSecondarySlot(int pSlotX, int pSlotY, const char *pAbilityName);
    void AddPassiveAbility(const char *pAbilityName);
    void RunAbilityScripts(int pCode);

    //--Equipment
    void CreateEquipmentSlot(const char *pName);
    int GetEquipmentSlotsTotal();
    const char *GetNameOfEquipmentSlot(int pSlot);
    int GetSlotOfEquipmentByName(const char *pName);
    int GetSlotForWeaponDamage();
    AdventureItem *GetEquipmentBySlotS(const char *pEquipSlot);
    AdventureItem *GetEquipmentBySlotI(int pEquipSlot);
    AdventureItem *GetWeapon();
    EquipmentSlotPack *GetEquipmentSlotPackageI(int pIndex);
    EquipmentSlotPack *GetEquipmentSlotPackageS(const char *pEquipSlot);
    void SetEquipmentSlotIsUsedForStats(const char *pName, bool pFlag);
    void SetEquipmentSlotCanBeEmpty(const char *pName, bool pFlag);
    bool EquipItemToSlot(const char *pSlotName, AdventureItem *pItem);
    void SetSlotUsedForWeaponDamage(int pSlot);
    void ComputeEquipmentStatistics(CombatStatistics &sStatisticsPack);
    void SwapEquipmentSlots(const char *pSlotA, const char *pSlotB);

    //--Statistics
    int GetHealth();
    int GetMagic();
    int GetComboPoints();
    int GetAdrenaline();
    int GetShields();
    int GetStun();
    int GetStunResist();
    int GetStunResistTimer();
    float GetHealthPercent();
    float GetMagicPercent();
    void SetHealth(int pHP);
    void SetMagic(int pMP);
    void SetComboPoints(int pCP);
    void SetAdrenaline(int pAdrenaline);
    void SetShields(int pShields);
    void SetStunnable(bool pIsStunnable);
    void SetStun(int pStunValue);
    void SetStunResist(int pStunResist);
    void SetStunResistTimer(int pTimer);
    void SetHealthPercent(float pPercent);
    void SetMagicPercent(float pPercent);
    int GetStatistic(int pStatisticIndex);
    int GetStatistic(int pGroupIndex, int pStatisticIndex);
    void SetStatistic(int pGroupIndex, int pStatisticIndex, int pValue);
    void InflictDamage(int pDamage, int pHealthPriority, int pAdrenalinePriority, int pShieldPriority);
    void RerunEffects();
    void ComputeStatistics();
    CombatStatistics *GetStatisticsGroup(int pIndex);
    CombatStatistics *GetStatisticsGroup(const char *pName);
    void RefreshStatsForUI();

    //--Targeting
    bool IsNormalTarget();

    //--Dummy
    int GetPropertyByInventoryHeaders(int pDum, bool pDumDum);

    private:
    //--Private Core Methods
    public:
    //--Update
    void UpdateTimers();
    void UpdatePosition();

    //--File I/O
    //--Drawing
    SugarBitmap *GetCombatPortrait();
    SugarBitmap *GetCombatCounterMask();
    SugarBitmap *GetVictoryCounterMask();
    SugarBitmap *GetTurnIcon();

    //--Pointer Routing
    SugarLinkedList *GetAbilityList();
    AdvCombatAbility *GetAbilityBySlot(int pX, int pY);
    AdvCombatAbility *GetSecondaryBySlot(int pX, int pY);
    AdvCombatJob *GetActiveJob();
    SugarLinkedList *GetJobList();
    SugarLinkedList *GetEffectRenderList();
    SugarBitmap *GetVendorImage(int pIndex);

    //--Static Functions
    static float ComputeResistance(int pResistance);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombatEntity_GetProperty(lua_State *L);
int Hook_AdvCombatEntity_SetProperty(lua_State *L);

