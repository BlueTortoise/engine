//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatJob.h"
#include "AdventureItem.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

bool AdvCombatEntity::IsAbilityEquipped(AdvCombatAbility *pCheckAbility)
{
    if(!pCheckAbility) return false;
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(rAbilityGrid[x][y] == pCheckAbility) return true;
        }
    }
    return false;
}
bool AdvCombatEntity::IsAbilityEquippedS(const char *pName)
{
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(!rAbilityGrid[x][y]) continue;
            if(!strcasecmp(rAbilityGrid[x][y]->GetInternalName(), pName))
            {
                return true;
            }
        }
    }
    return false;
}
const char *AdvCombatEntity::GetAbilityNameInSlot(int pX, int pY)
{
    //--Returns "Null" if out of range or slot is empty.
    if(pX < 0 || pX >= ACE_ABILITY_GRID_SIZE_X) return "Null";
    if(pY < 0 || pY >= ACE_ABILITY_GRID_SIZE_Y) return "Null";
    if(!rAbilityGrid[pX][pY]) return "Null";
    return rAbilityGrid[pX][pY]->GetInternalName();
}
void AdvCombatEntity::RegisterAbility(const char *pAbilityName, AdvCombatAbility *pAbility)
{
    //--Registers a new ability and takes explicit ownership of it.
    if(!pAbilityName || !pAbility) return;
    pAbility->SetOwner(this);
    pAbility->SetInternalName(pAbilityName);
    mAbilityList->AddElement(pAbilityName, pAbility, &RootObject::DeleteThis);
}
void AdvCombatEntity::RegisterAbilityToJob(const char *pAbilityName, const char *pJobName)
{
    //--Registers the ability to the given job, allowing it to be purchased in the skills UI.
    if(!pAbilityName || !pJobName) return;

    //--Get and check.
    AdvCombatAbility *rAbility = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    AdvCombatJob *rJob = (AdvCombatJob *)mJobList->GetElementByName(pJobName);
    if(!rAbility || !rJob) return;

    //--Register.
    rJob->RegisterAbility(rAbility);
}
void AdvCombatEntity::RemoveAbility(const char *pAbilityName)
{
    //--Locate the ability.
    if(!pAbilityName) return;
    AdvCombatAbility *rCheckAbility = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    if(!rCheckAbility) return;

    //--Remove it from any equipped slots.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(rAbilityGrid[x][y] == rCheckAbility) rAbilityGrid[x][y] = NULL;
        }
    }

    //--Unregister it from any jobs that reference it.
    AdvCombatJob *rJob = (AdvCombatJob *)mJobList->PushIterator();
    while(rJob)
    {
        rJob->UnregisterAbility(rCheckAbility);
        rJob = (AdvCombatJob *)mJobList->AutoIterate();
    }

    //--Remove it from the list.
    mAbilityList->RemoveElementP(rCheckAbility);
    mrPassiveAbilityList->RemoveElementP(rCheckAbility);
}
void AdvCombatEntity::SetAbilitySlot(int pSlotX, int pSlotY, const char *pAbilityName)
{
    //--Range check.
    if(pSlotX < 0 || pSlotX >= ACE_ABILITY_GRID_SIZE_X) return;
    if(pSlotY < 0 || pSlotY >= ACE_ABILITY_GRID_SIZE_Y) return;

    //--If an ability was already in the slot, mark it as not equipped.
    if(rAbilityGrid[pSlotX][pSlotY])
    {
        rAbilityGrid[pSlotX][pSlotY]->SetEquipped(false);
    }

    //--Special: If the ability is "System|Retreat" or "System|Surrender", those are acquired from the AdvCombat class.
    if(!strcasecmp(pAbilityName, "System|Retreat"))
    {
        rAbilityGrid[pSlotX][pSlotY] = AdvCombat::Fetch()->GetSystemRetreat();
        return;
    }
    else if(!strcasecmp(pAbilityName, "System|Surrender"))
    {
        rAbilityGrid[pSlotX][pSlotY] = AdvCombat::Fetch()->GetSystemSurrender();
        return;
    }

    //--Get the ability. If it comes back NULL, or "Null" is passed in, we can still set the slot to null
    //  to indicate it is empty.
    AdvCombatAbility *rAbility = NULL;
    if(strcasecmp(pAbilityName, "Null")) rAbility = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    rAbilityGrid[pSlotX][pSlotY] = rAbility;
    if(rAbilityGrid[pSlotX][pSlotY]) rAbilityGrid[pSlotX][pSlotY]->SetEquipped(true);
}
void AdvCombatEntity::SetSecondarySlot(int pSlotX, int pSlotY, const char *pAbilityName)
{
    //--Range check.
    if(pSlotX < 0 || pSlotX >= ACE_ABILITY_GRID_SIZE_X) return;
    if(pSlotY < 0 || pSlotY >= ACE_ABILITY_GRID_SIZE_Y) return;

    //--If an ability was already in the slot, mark it as not equipped.
    if(rSecondaryGrid[pSlotX][pSlotY])
    {
        rSecondaryGrid[pSlotX][pSlotY]->SetEquipped(false);
    }

    //--Special: If the ability is "System|Retreat" or "System|Surrender", those are acquired from the AdvCombat class.
    if(!strcasecmp(pAbilityName, "System|Retreat"))
    {
        rSecondaryGrid[pSlotX][pSlotY] = AdvCombat::Fetch()->GetSystemRetreat();
        return;
    }
    else if(!strcasecmp(pAbilityName, "System|Surrender"))
    {
        rSecondaryGrid[pSlotX][pSlotY] = AdvCombat::Fetch()->GetSystemSurrender();
        return;
    }

    //--Get the ability. If it comes back NULL, or "Null" is passed in, we can still set the slot to null
    //  to indicate it is empty.
    AdvCombatAbility *rAbility = NULL;
    if(strcasecmp(pAbilityName, "Null")) rAbility = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    rSecondaryGrid[pSlotX][pSlotY] = rAbility;
    if(rSecondaryGrid[pSlotX][pSlotY]) rSecondaryGrid[pSlotX][pSlotY]->SetEquipped(true);
}
void AdvCombatEntity::AddPassiveAbility(const char *pAbilityName)
{
    AdvCombatAbility *rCheckPtr = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    if(rCheckPtr) mrPassiveAbilityList->AddElementAsTail(pAbilityName, rCheckPtr);
}
void AdvCombatEntity::RunAbilityScripts(int pCode)
{
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(rAbilityGrid[x][y])   rAbilityGrid[x][y]->CallCode(pCode);
            if(rSecondaryGrid[x][y]) rSecondaryGrid[x][y]->CallCode(pCode);
        }
    }
}
