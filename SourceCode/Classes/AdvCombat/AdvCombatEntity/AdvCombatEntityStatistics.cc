//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEffect.h"
#include "AdventureItem.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

//--[Property Queries]
int AdvCombatEntity::GetHealth()
{
    return mHP;
}
int AdvCombatEntity::GetMagic()
{
    return mMP;
}
int AdvCombatEntity::GetComboPoints()
{
    return mCP;
}
int AdvCombatEntity::GetAdrenaline()
{
    return mAdrenaline;
}
int AdvCombatEntity::GetShields()
{
    return mShield;
}
int AdvCombatEntity::GetStun()
{
    return mStunValue;
}
int AdvCombatEntity::GetStunResist()
{
    return mStunResist;
}
int AdvCombatEntity::GetStunResistTimer()
{
    return mStunResistDecrementTurns;
}
float AdvCombatEntity::GetHealthPercent()
{
    //--Range-check on max HP.
    int tHPMax = mSummedStatistics.GetStatByIndex(STATS_HPMAX);
    if(tHPMax < 1) tHPMax = 1;
    return (float)mHP / (float)tHPMax;
}
float AdvCombatEntity::GetMagicPercent()
{
    int tMPMax = mSummedStatistics.GetStatByIndex(STATS_MPMAX);
    if(tMPMax < 1) return 0.0f;
    return (float)mMP / (float)tMPMax;
}
int AdvCombatEntity::GetStatistic(int pStatisticIndex)
{
    //--Assumes the final statistic is the one being queried.
    return GetStatistic(ADVCE_STATS_FINAL, pStatisticIndex);
}
int AdvCombatEntity::GetStatistic(int pGroupIndex, int pStatisticIndex)
{
    CombatStatistics *rStatGroup = GetStatisticsGroup(pGroupIndex);
    return rStatGroup->GetStatByIndex(pStatisticIndex);
}

//--[Manipulators]
void AdvCombatEntity::SetHealth(int pHP)
{
    //--Get HP Max.
    int tHPMax = mSummedStatistics.GetStatByIndex(STATS_HPMAX);
    if(tHPMax < 1) tHPMax = 1;

    //--Set HP.
    mHP = pHP;
    if(mHP >= tHPMax) mHP = tHPMax;
    if(mHP < 0) mHP = 0;

    //--Ease to destination.
    float tPercent = (float)mHP / (float)tHPMax;
    mHPEasingPack.MoveTo(tPercent, ADVCE_EASING_TICKS);
}
void AdvCombatEntity::SetMagic(int pMP)
{
    //--Get MP Max. Can legally be 0.
    int tMPMax = mSummedStatistics.GetStatByIndex(STATS_MPMAX);
    if(tMPMax < 1) tMPMax = 0;

    //--Set MP.
    mMP = pMP;
    if(mMP >= tMPMax) mMP = tMPMax;
    if(mMP < 0) mMP = 0;

    //--Ease to destination.
    if(tMPMax == 0)
    {
        mMPEasingPack.MoveTo(0.0f, ADVCE_EASING_TICKS);
    }
    else
    {
        float tPercent = (float)mMP / (float)tMPMax;
        mMPEasingPack.MoveTo(tPercent, ADVCE_EASING_TICKS);
    }
}
void AdvCombatEntity::SetComboPoints(int pCP)
{
    //--Get CP Max. Can legally be 0.
    int tCPMax = mSummedStatistics.GetStatByIndex(STATS_CPMAX);
    if(tCPMax < 1) tCPMax = 0;

    //--Set MP.
    mCP = pCP;
    if(mCP >= tCPMax) mCP = tCPMax;
    if(mCP < 0) mCP = 0;
}
void AdvCombatEntity::SetAdrenaline(int pAdrenaline)
{
    mAdrenaline = pAdrenaline;
    int tHPMax = mSummedStatistics.GetStatByIndex(STATS_HPMAX);
    if(mAdrenaline > tHPMax / 2) mAdrenaline = tHPMax / 2;

    mAdrenalineEasingPack.MoveTo(mAdrenaline, ADVCE_EASING_TICKS);
}
void AdvCombatEntity::SetShields(int pShields)
{
    mShield = pShields;
    mShieldEasingPack.MoveTo(mShield, ADVCE_EASING_TICKS);
}
void AdvCombatEntity::SetStunnable(bool pIsStunnable)
{
    mIsStunnable = pIsStunnable;
}
void AdvCombatEntity::SetStun(int pStunValue)
{
    mStunValue = pStunValue;
    mStunEasingPack.MoveTo(mStunValue, ADVCE_EASING_TICKS);
}
void AdvCombatEntity::SetStunResist(int pStunResist)
{
    mStunResist = pStunResist;
}
void AdvCombatEntity::SetStunResistTimer(int pTimer)
{
    mStunResistDecrementTurns = pTimer;
}
void AdvCombatEntity::SetHealthPercent(float pPercent)
{
    int tHPMax = mSummedStatistics.GetStatByIndex(STATS_HPMAX);
    SetHealth(tHPMax * pPercent);
}
void AdvCombatEntity::SetMagicPercent(float pPercent)
{
    int tMPMax = mSummedStatistics.GetStatByIndex(STATS_MPMAX);
    SetHealth(tMPMax * pPercent);
}
void AdvCombatEntity::SetStatistic(int pGroupIndex, int pStatisticIndex, int pValue)
{
    CombatStatistics *rStatGroup = GetStatisticsGroup(pGroupIndex);
    rStatGroup->SetStatByIndex(pStatisticIndex, pValue);
}

//--[Core Methods]
void AdvCombatEntity::InflictDamage(int pDamage, int pHealthPriority, int pAdrenalinePriority, int pShieldPriority)
{
    //--Inflicts damage to the entity in order of priority. If a priority is zero, that type is bypassed.
    //  A recursive pattern is used instead of a loop.
    if(pDamage < 1) return;

    //--Damage health.
    if(pHealthPriority > 0 && pHealthPriority >= pAdrenalinePriority && pHealthPriority >= pShieldPriority)
    {
        //--If we we have more health than the incoming damage, just decrement it off.
        int tStartHealth = GetHealth();
        if(tStartHealth >= pDamage)
        {
            SetHealth(tStartHealth - pDamage);
            return;
        }

        //--We have less health than the incoming damage, so set health to zero and re-call
        //  the routine with the other damage types.
        SetHealth(0);
        InflictDamage(pDamage - tStartHealth, 0, pAdrenalinePriority, pShieldPriority);
        return;
    }

    //--Damage adrenaline.
    if(pAdrenalinePriority > 0 && pAdrenalinePriority >= pHealthPriority && pAdrenalinePriority >= pShieldPriority)
    {
        //--If we we have more adrenaline than the incoming damage, just decrement it off.
        int tStartAdrenaline = GetAdrenaline();
        if(tStartAdrenaline >= pDamage)
        {
            SetAdrenaline(tStartAdrenaline - pDamage);
            return;
        }

        //--We have less adrenaline than the incoming damage, so set adrenaline to zero and re-call
        //  the routine with the other damage types.
        SetAdrenaline(0);
        InflictDamage(pDamage - tStartAdrenaline, pHealthPriority, 0, pShieldPriority);
        return;
    }

    //--Damage shields.
    if(pShieldPriority > 0 && pShieldPriority >= pHealthPriority && pShieldPriority >= pAdrenalinePriority)
    {
        //--If we we have more adrenaline than the incoming damage, just decrement it off.
        int tStartShields = GetShields();
        if(tStartShields >= pDamage)
        {
            SetShields(tStartShields - pDamage);
            return;
        }

        //--We have less shields than the incoming damage, so set shields to zero and re-call
        //  the routine with the other damage types.
        SetShields(0);
        InflictDamage(pDamage - tStartShields, pHealthPriority, pAdrenalinePriority, 0);
        return;
    }
}
void AdvCombatEntity::RerunEffects()
{
    //--Whenever a new effect is applied, scrap them all and rerun the effects.
    mTemporaryEffectStatistics.Zero();

    //--Get a list of all effects that point to us.
    SugarLinkedList *tEffectList = AdvCombat::Fetch()->GetEffectsReferencing(this);

    //--Run all effects to sum up their bonuses.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)tEffectList->PushIterator();
    while(rEffect)
    {
        rEffect->ApplyStatsToTarget(mUniqueID);
        rEffect = (AdvCombatEffect *)tEffectList->AutoIterate();
    }

    //--Clean.
    delete tEffectList;
}
void AdvCombatEntity::ComputeStatistics()
{
    //--Clear the final status holder.
    mSummedStatistics.Zero();

    //--Zero the equipment statistics. Run across all equipment and add them together.
    mEquipmentStatistics.Zero();
    EquipmentSlotPack *rEquipmentPack = (EquipmentSlotPack *)mEquipmentSlotList->PushIterator();
    while(rEquipmentPack)
    {
        //--Skip slots not used for computation, or that are empty.
        if(!rEquipmentPack->mEquippedItem || !rEquipmentPack->mIsComputedForStats)
        {
        }
        //--Add.
        else
        {
            rEquipmentPack->mEquippedItem->AppendToStatsPack(mEquipmentStatistics);
        }

        //--Next.
        rEquipmentPack = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
    }

    //--Now add all the values together.
    mSummedStatistics.AddStatistics(mBaseStatistics);
    mSummedStatistics.AddStatistics(mJobStatistics);
    mSummedStatistics.AddStatistics(mEquipmentStatistics);
    mSummedStatistics.AddStatistics(mScriptStatistics);
    mSummedStatistics.AddStatistics(mPermaEffectStatistics);
    mSummedStatistics.AddStatistics(mTemporaryEffectStatistics);

    //--Clamp to make sure nothing was out of range.
    mSummedStatistics.ClampStatistics();
}
void AdvCombatEntity::RefreshStatsForUI()
{
    //--Resets statistics back to zero, then applies abilities, not effects, for UI purposes. These are placed
    //  in the temporary effect category.
    mTemporaryEffectStatistics.Zero();
    RunAbilityScripts(ACA_SCRIPT_CODE_GUI_APPLY_EFFECT);
    ComputeStatistics();
}

//--[Pointer Routing]
CombatStatistics *AdvCombatEntity::GetStatisticsGroup(int pIndex)
{
    //--Get stat group by index.
    if(pIndex == ADVCE_STATS_BASE)        return &mBaseStatistics;
    if(pIndex == ADVCE_STATS_JOB)         return &mJobStatistics;
    if(pIndex == ADVCE_STATS_EQUIPMENT)   return &mEquipmentStatistics;
    if(pIndex == ADVCE_STATS_SCRIPT)      return &mScriptStatistics;
    if(pIndex == ADVCE_STATS_PERMAEFFECT) return &mPermaEffectStatistics;
    if(pIndex == ADVCE_STATS_TEMPEFFECT)  return &mTemporaryEffectStatistics;
    if(pIndex == ADVCE_STATS_FINAL)       return &mSummedStatistics;

    //--Range errors always return the base statistics.
    fprintf(stderr, "Warning: Statistics index %i is out of range.\n", pIndex);
    return &mBaseStatistics;
}
CombatStatistics *AdvCombatEntity::GetStatisticsGroup(const char *pName)
{
    //--Errors return the base statistics.
    if(!pName)
    {
        fprintf(stderr, "Warning: Null statistics group requested, returning base.\n");
        return &mBaseStatistics;
    }

    //--Groupings by string.
    if(!strcasecmp(pName, "Base"))        return &mBaseStatistics;
    if(!strcasecmp(pName, "Job"))         return &mJobStatistics;
    if(!strcasecmp(pName, "Equipment"))   return &mEquipmentStatistics;
    if(!strcasecmp(pName, "Script"))      return &mScriptStatistics;
    if(!strcasecmp(pName, "PermaEffect")) return &mPermaEffectStatistics;
    if(!strcasecmp(pName, "TempEffect"))  return &mTemporaryEffectStatistics;

    //--If not found, return base statistics.
    fprintf(stderr, "Warning: Statistics group %s not found.\n", pName);
    return &mBaseStatistics;
}
