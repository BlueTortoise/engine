//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdventureInventory.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

//=========================================== System ==============================================
void AdvCombatEntity::CreateEquipmentSlot(const char *pName)
{
    //--Creates and registers a new equipment slot with the given name. Does not allow duplicates.
    if(!pName) return;
    if(mEquipmentSlotList->GetElementByName(pName)) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    EquipmentSlotPack *nPack = (EquipmentSlotPack *)starmemoryalloc(sizeof(EquipmentSlotPack));
    nPack->Initialize();
    mEquipmentSlotList->AddElementAsTail(pName, nPack, &EquipmentSlotPack::DeleteThis);
}

//====================================== Property Queries =========================================
int AdvCombatEntity::GetEquipmentSlotsTotal()
{
    return mEquipmentSlotList->GetListSize();
}
const char *AdvCombatEntity::GetNameOfEquipmentSlot(int pSlot)
{
    return mEquipmentSlotList->GetNameOfElementBySlot(pSlot);
}
int AdvCombatEntity::GetSlotOfEquipmentByName(const char *pName)
{
    return mEquipmentSlotList->GetSlotOfElementByName(pName);
}
int AdvCombatEntity::GetSlotForWeaponDamage()
{
    return mSlotForWeaponDamage;
}
AdventureItem *AdvCombatEntity::GetEquipmentBySlotS(const char *pEquipSlot)
{
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pEquipSlot);
    if(!rPackage) return NULL;
    return rPackage->mEquippedItem;
}
AdventureItem *AdvCombatEntity::GetEquipmentBySlotI(int pEquipSlot)
{
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementBySlot(pEquipSlot);
    if(!rPackage) return NULL;
    return rPackage->mEquippedItem;
}
AdventureItem *AdvCombatEntity::GetWeapon()
{
    //--Note: Can return NULL.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementBySlot(mSlotForWeaponDamage);
    if(!rPackage) return NULL;
    return rPackage->mEquippedItem;
}
EquipmentSlotPack *AdvCombatEntity::GetEquipmentSlotPackageI(int pIndex)
{
    return (EquipmentSlotPack *)mEquipmentSlotList->GetElementBySlot(pIndex);
}
EquipmentSlotPack *AdvCombatEntity::GetEquipmentSlotPackageS(const char *pEquipSlot)
{
    return (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pEquipSlot);
}

//========================================= Manipulators ==========================================
void AdvCombatEntity::SetEquipmentSlotIsUsedForStats(const char *pName, bool pFlag)
{
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pName);
    if(!pName) return;
    rPackage->mIsComputedForStats = pFlag;
}
void AdvCombatEntity::SetEquipmentSlotCanBeEmpty(const char *pName, bool pFlag)
{
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pName);
    if(!pName) return;
    rPackage->mCanBeEmpty = pFlag;
}
bool AdvCombatEntity::EquipItemToSlot(const char *pSlotName, AdventureItem *pItem)
{
    //--Equips the item to the given slot, and returns true if the item was taken ownership of. Returns
    //  false if this entity did *not* take ownership for any reason.
    //--If the slot is already occupied, the item previously held is moved to the inventory.
    if(!pSlotName || !pItem) return false;

    //--Special: If the slot name is "ALL" and the item is the dummy item, unequip all items.
    if(!strcasecmp(pSlotName, "All") && pItem == AdventureInventory::xrDummyUnequipItem)
    {
        //--Iterate across all packages.
        EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->PushIterator();
        while(rPackage)
        {
            //--Send the item to the inventory.
            if(rPackage->mEquippedItem)
                AdventureInventory::Fetch()->RegisterItem(rPackage->mEquippedItem);

            //--Clear slot.
            rPackage->mEquippedItem = NULL;

            //--Next.
            rPackage = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
        }

        //--Compute changes in stats.
        ComputeStatistics();
        return false;
    }

    //--Make sure the equipment slot exists.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pSlotName);
    if(!rPackage) return false;

    //--If the slot is already occupied, unequip that item and send it to the inventory.
    if(rPackage->mEquippedItem)
    {
        AdventureInventory::Fetch()->RegisterItem(rPackage->mEquippedItem);
        rPackage->mEquippedItem = NULL;
    }

    //--If the item was the dummy-unequip item, set the slot to NULL. We don't take ownership of that.
    if(pItem == AdventureInventory::xrDummyUnequipItem)
    {
        rPackage->mEquippedItem = NULL;
        ComputeStatistics();
        return false;
    }

    //--Now equip the item and inform the caller we took ownership.
    rPackage->mEquippedItem = pItem;
    ComputeStatistics();
    return true;
}
void AdvCombatEntity::SetSlotUsedForWeaponDamage(int pSlot)
{
    mSlotForWeaponDamage = pSlot;
    mSlotOfOriginalWeapon = pSlot;
}

//========================================= Core Methods ==========================================
void AdvCombatEntity::ComputeEquipmentStatistics(CombatStatistics &sStatisticsPack)
{
    //--Iterate across all equipment and add the bonuses to the stats pack.
    EquipmentSlotPack *rPackage = (EquipmentSlotPack *)mEquipmentSlotList->PushIterator();
    while(rPackage)
    {
        //--Slot is empty, or is not used for stat computations. Ignore it.
        if(!rPackage->mEquippedItem || !rPackage->mIsComputedForStats)
        {
            rPackage = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
            continue;
        }

        ///--ADDITION!!!

        //--Next.
        rPackage = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
    }
}
void AdvCombatEntity::SwapEquipmentSlots(const char *pSlotA, const char *pSlotB)
{
    //--Switches the equipment in the two given slots. Doesn't check if the slots are compatible, just
    //  switches the equipment. Yeah!
    //--Also tracks the index for weapon reversion.
    if(!pSlotA || !pSlotB) return;
    int tSlotA = mEquipmentSlotList->GetSlotOfElementByName(pSlotA);
    int tSlotB = mEquipmentSlotList->GetSlotOfElementByName(pSlotB);
    if(tSlotA == -1 || tSlotB == -1) return;

    //--Get the entries in the slots.
    EquipmentSlotPack *rItemPackA = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pSlotA);
    EquipmentSlotPack *rItemPackB = (EquipmentSlotPack *)mEquipmentSlotList->GetElementByName(pSlotB);

    //--Switch the equipment.
    AdventureItem *rTempPtr = rItemPackA->mEquippedItem;
    rItemPackA->mEquippedItem = rItemPackB->mEquippedItem;
    rItemPackB->mEquippedItem = rTempPtr;

    //--Tracking: If the A slot matched the weapon slot, move it.
    if(mSlotOfOriginalWeapon == tSlotA)
    {
        mSlotOfOriginalWeapon = tSlotB;
    }
    //--If the B slot was the weapon:
    else if(mSlotOfOriginalWeapon == tSlotB)
    {
        mSlotOfOriginalWeapon = tSlotA;
    }

    //--Recompute stats since they probably changed.
    ComputeStatistics();
    fprintf(stderr, "Weapon slot changed to %i\n", mSlotOfOriginalWeapon);
}
