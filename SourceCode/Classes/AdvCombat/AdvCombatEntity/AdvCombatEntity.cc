//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEffect.h"
#include "AdvCombatJob.h"
#include "AdventureItem.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

//=========================================== System ==============================================
AdvCombatEntity::AdvCombatEntity()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATENTITY;

    //--[AdvCombatEntity]
    //--System
    mLocalName = InitializeString("Character");
    mDisplayName = InitializeString("Character");
    mResponseScriptPath = NULL;
    memset(mScriptResponses, 0, sizeof(mScriptResponses));

    //--Persistent Statistics
    mIsKnockedOut = false;
    mTurnKOd = 100;
    mHP = 1;
    mMP = 0;
    mCP = 0;
    mShield = 0;
    mAdrenaline = 0;
    mFreeActions = 0;
    mHPEasingPack.Initialize();
    mMPEasingPack.Initialize();
    mAdrenalineEasingPack.Initialize();
    mShieldEasingPack.Initialize();

    //--Stun Handling
    mIsStunnable = true;
    mStunValue = 0;
    mStunResist = 0;
    mStunResistDecrementTurns = 0;
    mStunEasingPack.Initialize();

    //--AI
    mIsAmbushed = false;
    mAIScriptPath = NULL;

    //--Combat Position
    mIgnoreMovementForBlocking = false;
    mCombatIdealX = 0.0f;
    mCombatIdealY = 0.0f;
    mCombatPosition.Initialize();

    //--Turn Order
    mTurnID = 0;
    mCurrentInitiative = 0;

    //--Statistics
    mBaseStatistics.Initialize();
    mJobStatistics.Initialize();
    mEquipmentStatistics.Initialize();
    mScriptStatistics.Initialize();
    mPermaEffectStatistics.Initialize();
    mTemporaryEffectStatistics.Initialize();
    mSummedStatistics.Initialize();

    //--Levels
    mLevel = 0;
    mTotalXP = 0;

    //--Jobs
    mGlobalJP = 0;
    rActiveJobAtCombatStart = NULL;
    rActiveJob = NULL;
    mJobList = new SugarLinkedList(true);

    //--Rewards
    mRewardXP = 0;
    mRewardJP = 0;
    mRewardPlatina = 0;
    mRewardDoctor = 0;
    mRewardItems = new SugarLinkedList(true);

    //--Reinforcement Handling
    mReinforcementTurns = 0;

    //--Ability Listing
    mAbilityList = new SugarLinkedList(true);
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            rAbilityGrid[x][y] = NULL;
            rSecondaryGrid[x][y] = NULL;
        }
    }
    mrPassiveAbilityList = new SugarLinkedList(false);

    //--Equipment Listing
    mSlotForWeaponDamage = 0;
    mSlotOfOriginalWeapon = 0;
    mEquipmentSlotList = new SugarLinkedList(true);

    //--UI Positions
    memset(&mUIPositions, 0, sizeof(TwoDimensionRealPoint) * ACE_UI_INDEX_TOTAL);
    mFaceTableDim.SetWH(0.0f, 0.0f, 1.0f, 1.0f);

    //--Effect Handling
    mrEffectsToShowThisTick = new SugarLinkedList(false);

    //--Tags
    mTagList = new SugarLinkedList(true);

    //--Images
    mIsFlashingWhite = false;
    mWhiteFlashTimer = 0;
    mIsFlashingBlack = false;
    mBlackFlashTimer = 0;
    mIsKnockingOut = false;
    mKnockoutTimer = 0;
    rCombatImage = NULL;
    rCombatCountermask = NULL;
    rVictoryCountermask = NULL;
    rTurnIcon = NULL;
    memset(rVendorImages, 0, sizeof(SugarBitmap *) * ADVCE_VENDOR_SPRITES_TOTAL);
    rFaceTableImg = NULL;

    //--Public Variables
    mLeveledUpThisBattle = false;
    mLevelLastTick = 0;
}
AdvCombatEntity::~AdvCombatEntity()
{
    free(mLocalName);
    free(mDisplayName);
    free(mResponseScriptPath);
    delete mJobList;
    delete mAbilityList;
    delete mrPassiveAbilityList;
    delete mEquipmentSlotList;
    delete mrEffectsToShowThisTick;
    delete mRewardItems;
    delete mTagList;
}
void AdvCombatEntity::Reinitialize()
{
    //--Called when a new combat begins, clears off reference lists.
    mrEffectsToShowThisTick->ClearList();

    //--Clear the temporary effects and recompute stats.
    mTemporaryEffectStatistics.Zero();
    ComputeStatistics();
}

//--[Public Variables]
//--Experience table used to compute next level requirements.
int AdvCombatEntity::xMaxLevel = 0;
int *AdvCombatEntity::xExpTable = NULL;

//====================================== Property Queries =========================================
const char *AdvCombatEntity::GetName()
{
    return mLocalName;
}
const char *AdvCombatEntity::GetDisplayName()
{
    return mDisplayName;
}
const char *AdvCombatEntity::GetResponsePath()
{
    return mResponseScriptPath;
}
const char *AdvCombatEntity::GetAIPath()
{
    return mAIScriptPath;
}
bool AdvCombatEntity::IsAmbushed()
{
    return mIsAmbushed;
}
bool AdvCombatEntity::RespondsToCode(int pCode)
{
    if(pCode < 0 || pCode >= ACE_SCRIPT_CODE_TOTAL) return false;
    return mScriptResponses[pCode];
}
bool AdvCombatEntity::IsStunnable()
{
    return mIsStunnable;
}
bool AdvCombatEntity::IsStunnedThisTurn()
{
    //--If the entity is not stunnable period, always return false.
    if(!mIsStunnable) return false;

    //--If stunnable, and the stun value is over the threshold, we will be stunned next turn
    //  so we are considered stunned.
    int tStunNeeded = GetStatistic(STATS_STUN_CAP);
    if(mStunValue >= tStunNeeded)
    {
        return true;
    }

    //--If the stun resist value is over 1, and the counter is 0, that means we got stunned last
    //  turn and are therefore still stunned.
    if(mStunResist > 0 && mStunResistDecrementTurns == 0)
    {
        return true;
    }

    //--Not stunned.
    return false;
}
int AdvCombatEntity::GetDisplayStun()
{
    return (int)mStunEasingPack.mXCur;
}
float AdvCombatEntity::GetDisplayHPPct()
{
    return mHPEasingPack.mXCur;
}
float AdvCombatEntity::GetDisplayMPPct()
{
    return mMPEasingPack.mXCur;
}
float AdvCombatEntity::GetDisplayAdrenaline()
{
    return mAdrenalineEasingPack.mXCur;
}
float AdvCombatEntity::GetDisplayShield()
{
    return mShieldEasingPack.mXCur;
}
int AdvCombatEntity::GetReinforcementTurns()
{
    return mReinforcementTurns;
}
SugarBitmap *AdvCombatEntity::GetFaceProperties(TwoDimensionReal &sDimensions)
{
    memcpy(&sDimensions, &mFaceTableDim, sizeof(TwoDimensionReal));
    return rFaceTableImg;
}
bool AdvCombatEntity::IsMoving()
{
    if(mIgnoreMovementForBlocking) return false;
    return (mCombatPosition.mTimer < mCombatPosition.mTimerMax);
}
float AdvCombatEntity::GetCombatX()
{
    return mCombatPosition.mXCur;
}
float AdvCombatEntity::GetCombatY()
{
    return mCombatPosition.mYCur;
}
TwoDimensionRealPoint AdvCombatEntity::GetUIRenderPosition(int pIndex)
{
    if(pIndex < 0 || pIndex >= ACE_UI_INDEX_TOTAL)
    {
        TwoDimensionRealPoint tDummy;
        tDummy.mXCenter = 0.0f;
        tDummy.mYCenter = 0.0f;
        return tDummy;
    }
    return mUIPositions[pIndex];
}
bool AdvCombatEntity::CanActThisTurn()
{
    return true;
}
bool AdvCombatEntity::IsDefeated()
{
    if(mHP < 1) return true;
    return false;
}
bool AdvCombatEntity::IsAnimating()
{
    if(mCombatPosition.mTimer < mCombatPosition.mTimerMax) return true;
    return false;
}
int AdvCombatEntity::GetCurrentInitiative()
{
    return mCurrentInitiative;
}
int AdvCombatEntity::ComputeTurnBucket()
{
    //--figure out which "Bucket" this entity falls into during turn order computations. There are 7
    //  such buckets
    int tReturnVal = ACE_BUCKET_NEUTRAL;

    //--Get relevant tag counts.
    int tASLTag  = GetTagCount("Always Strikes Last");
    int tSlowTag = GetTagCount("Slow");
    int tFastTag = GetTagCount("Fast");
    int tASFTag  = GetTagCount("Always Strikes First");

    //--If the tag count for ASF vs. ASL is zero, no effect.
    if(tASFTag == tASLTag)
    {
    }
    //--If there are more ASF than ASL tags, [Always Strikes First] applies.
    else if(tASFTag > tASLTag)
    {
        tReturnVal += ACE_BUCKET_ALWAYS_STRIKES_FIRST;
    }
    //--If there are more ASL than ASF tags, [Always Strikes Last] applies.
    else
    {
        tReturnVal += ACE_BUCKET_ALWAYS_STRIKES_LAST;
    }

    //--If the tag count for Fast vs. Slow is zero, no effect.
    if(tFastTag == tSlowTag)
    {
    }
    //--If there are more Fast than Slow tags, [Fast] applies.
    else if(tFastTag > tSlowTag)
    {
        tReturnVal += ACE_BUCKET_FAST;
    }
    //--If there are more Slow than Fast tags, [Slow] applies.
    else
    {
        tReturnVal += ACE_BUCKET_SLOW;
    }

    //--Character report.
    if(false)
    {
        fprintf(stderr, "Turn report for %s\n", mLocalName);
        fprintf(stderr, "  ASF vs  ASL: %i - %i = %i\n", tASFTag, tASLTag, tASFTag - tASLTag);
        fprintf(stderr, " Fast vs Slow: %i - %i = %i\n", tFastTag, tSlowTag, tFastTag - tSlowTag);
        fprintf(stderr, " Bucket: %i\n", tReturnVal);
    }

    //--Finish up.
    return tReturnVal;
}
int AdvCombatEntity::GetTurnID()
{
    return mTurnID;
}
int AdvCombatEntity::GetWhiteFlashTimer()
{
    return mWhiteFlashTimer;
}
bool AdvCombatEntity::IsFlashingBlack()
{
    if(!mIsFlashingBlack) return false;
    if(mBlackFlashTimer < ADVCE_FLASH_BLACK_SEGMENT * 1) return true;
    if(mBlackFlashTimer < ADVCE_FLASH_BLACK_SEGMENT * 2) return false;
    if(mBlackFlashTimer < ADVCE_FLASH_BLACK_SEGMENT * 3) return true;
    return false;
}
bool AdvCombatEntity::IsKnockingOut()
{
    return mIsKnockingOut;
}
bool AdvCombatEntity::IsKnockoutFinished()
{
    return (mKnockoutTimer >= (ADVCE_KNOCKOUT_TOWHITE_TICKS + ADVCE_KNOCKOUT_TOBLACK_TICKS));
}
int AdvCombatEntity::GetKnockoutTimer()
{
    return mKnockoutTimer;
}
int AdvCombatEntity::GetGlobalJP()
{
    return mGlobalJP;
}
int AdvCombatEntity::GetFreeActions()
{
    return mFreeActions;
}
int AdvCombatEntity::GetFreeActionsPerformed()
{
    return mFreeActionsPerformed;
}
int AdvCombatEntity::GetRewardXP()
{
    return mRewardXP;
}
int AdvCombatEntity::GetRewardJP()
{
    return mRewardJP;
}
int AdvCombatEntity::GetRewardPlatina()
{
    return mRewardPlatina;
}
int AdvCombatEntity::GetRewardDoctor()
{
    return mRewardDoctor;
}
int AdvCombatEntity::GetTurnKOd()
{
    return mTurnKOd;
}
SugarLinkedList *AdvCombatEntity::GetRewardList()
{
    return mRewardItems;
}
int AdvCombatEntity::GetEffectsWithTag(const char *pTag)
{
    //--Returns how many effects are currently pointing at this entity and have the given tag at least once.
    int tTotal = 0;

    //--Iterate.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->PushIterator();
    while(rEffect)
    {
        if(rEffect->GetTagCount(pTag) > 0) tTotal ++;
        rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->AutoIterate();
    }

    //--Return.
    return tTotal;
}
int AdvCombatEntity::GetEffectIDWithTag(const char *pTag, int pSlot)
{
    //--Returns the ID of the Nth effect with the given tag. Can return 0 if the Nth effect does not exist.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->PushIterator();
    while(rEffect)
    {
        if(rEffect->GetTagCount(pTag) > 0)
        {
            pSlot --;
            if(pSlot < 0)
            {
                mrEffectsToShowThisTick->PopIterator();
                return rEffect->GetID();
            }
        }
        rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->AutoIterate();
    }

    //--Nth effect with tag does not exist.
    return 0;
}
int AdvCombatEntity::GetTagCount(const char *pTag)
{
    //--Base value.
    int tCount = 0;
    int *rTagBase = (int *)mTagList->GetElementByName(pTag);
    if(rTagBase) tCount += *rTagBase;

    //--Add effects.
    AdvCombatEffect *rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->PushIterator();
    while(rEffect)
    {
        tCount += rEffect->GetTagCount(pTag);
        rEffect = (AdvCombatEffect *)mrEffectsToShowThisTick->AutoIterate();
    }

    //--Add equipment.
    EquipmentSlotPack *rEquipPack = (EquipmentSlotPack *)mEquipmentSlotList->PushIterator();
    while(rEquipPack)
    {
        if(rEquipPack->mIsComputedForStats && rEquipPack->mEquippedItem)
        {
            tCount += rEquipPack->mEquippedItem->GetTagCount(pTag);
        }
        rEquipPack = (EquipmentSlotPack *)mEquipmentSlotList->AutoIterate();
    }

    //--Add job.
    if(rActiveJob)
    {
        tCount += rActiveJob->GetTagCount(pTag);
    }

    //--Pass it back.
    return tCount;
}

//============================= Property Queries - World Statistics ===============================
int AdvCombatEntity::GetLevel()
{
    return mLevel;
}
int AdvCombatEntity::GetXP()
{
    return mTotalXP;
}
int AdvCombatEntity::GetXPOfLevel()
{
    //--Returns the XP needed to reach the current level.
    if(xMaxLevel <= 0) return 0;
    if(mLevel < 0 || mLevel+1 >= xMaxLevel) return 0;
    return xExpTable[mLevel];
}
int AdvCombatEntity::GetXPToNextLevel()
{
    //--Returns XP to next level, or -1 if at max level or on error.
    if(xMaxLevel <= 0) return -1;
    if(mLevel < 0 || mLevel+1 >= xMaxLevel) return -1;
    return xExpTable[mLevel+1] - mTotalXP;
}
int AdvCombatEntity::GetXPToNextLevelMax()
{
    //--Returns the largest possible amount of XP to the next level, as if the character had exactly
    //  as much XP needed to be at the current level. Returns -1 if at max level or on error.
    if(xMaxLevel <= 0) return -1;
    if(mLevel < 0 || mLevel+1 >= xMaxLevel) return -1;
    return xExpTable[mLevel+1] - xExpTable[mLevel+0];
}

//--[Dummy Functions, Remove Later]
int AdvCombatEntity::GetPropertyByInventoryHeaders(int pDum, bool pDumDum)
{
    return 0;
}

//========================================= Manipulators ==========================================
void AdvCombatEntity::SetInternalName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void AdvCombatEntity::SetDisplayName(const char *pName)
{
    //--If the string passed is "DEFUALT", use the local name.
    if(!strcasecmp(pName, "DEFAULT"))
    {
        ResetString(mDisplayName, mLocalName);
    }
    //--Use the provided name.
    else
    {
        ResetString(mDisplayName, pName);
    }
}
void AdvCombatEntity::SetResponseScript(const char *pPath)
{
    ResetString(mResponseScriptPath, pPath);
}
void AdvCombatEntity::SetResponseScriptCode(int pCode, bool pFlag)
{
    if(pCode < 0 || pCode >= ACE_SCRIPT_CODE_TOTAL) return;
    mScriptResponses[pCode] = pFlag;
}
void AdvCombatEntity::SetAIScript(const char *pPath)
{
    if(!pPath || !strcasecmp(pPath, "Null"))
    {
        ResetString(mAIScriptPath, NULL);
    }
    else
    {
        ResetString(mAIScriptPath, pPath);
    }
}
void AdvCombatEntity::SetAmbushed(bool pIsAmbushed)
{
    mIsAmbushed = pIsAmbushed;
}
void AdvCombatEntity::SetReinforcementTurns(int pTurns)
{
    mReinforcementTurns = pTurns;
}
void AdvCombatEntity::RegisterJob(const char *pName, AdvCombatJob *pJob)
{
    if(!pName || !pJob) return;
    mJobList->AddElement(pName, pJob, &RootObject::DeleteThis);
}
void AdvCombatEntity::SetActiveJob(const char *pJobName)
{
    //--Set job.
    rActiveJob = (AdvCombatJob *)mJobList->GetElementByName(pJobName);
    if(rActiveJob) rActiveJob->AssumeJob(this);

    //--Recompute stats.
    ComputeStatistics();
}
void AdvCombatEntity::RevertToCombatStartJob()
{
    if(!rActiveJobAtCombatStart || rActiveJob == rActiveJobAtCombatStart) return;
    rActiveJob = rActiveJobAtCombatStart;
    rActiveJobAtCombatStart->AssumeJob(this);
    ComputeStatistics();
}
void AdvCombatEntity::SetCombatPortrait(const char *pDLPath)
{
    rCombatImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetCombatCountermask(const char *pDLPath)
{
    rCombatCountermask = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetVictoryCountermask(const char *pDLPath)
{
    rVictoryCountermask = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetTurnIcon(const char *pDLPath)
{
    rTurnIcon = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetVendorImage(int pSlot, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= ADVCE_VENDOR_SPRITES_TOTAL) return;
    rVendorImages[pSlot] = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetUIRenderPos(int pSlot, int pX, int pY)
{
    if(pSlot < 0 || pSlot >= ACE_UI_INDEX_TOTAL) return;
    mUIPositions[pSlot].mXCenter = pX;
    mUIPositions[pSlot].mYCenter = pY;
}
void AdvCombatEntity::SetFaceTableData(float pLft, float pTop, float pRgt, float pBot, const char *pPath)
{
    if(!pPath) return;
    rFaceTableImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
    mFaceTableDim.Set(pLft, pTop, pRgt, pBot);

    //--If the image exists, recompute face table positions.
    if(!rFaceTableImg) return;
    float cWid = rFaceTableImg->GetWidth();
    float cHei = rFaceTableImg->GetHeight();
    mFaceTableDim.mLft = mFaceTableDim.mLft / cWid;
    mFaceTableDim.mTop = mFaceTableDim.mTop / cHei;
    mFaceTableDim.mRgt = mFaceTableDim.mRgt / cWid;
    mFaceTableDim.mBot = mFaceTableDim.mBot / cHei;
    mFaceTableDim.mXCenter = mFaceTableDim.mXCenter / cWid;
    mFaceTableDim.mYCenter = mFaceTableDim.mYCenter / cHei;
}
void AdvCombatEntity::SetIdealPosition(float pXPos, float pYPos)
{
    mCombatIdealX = pXPos;
    mCombatIdealY = pYPos;
}
void AdvCombatEntity::SetIdealPositionX(float pXPos)
{
    mCombatIdealX = pXPos;
}
void AdvCombatEntity::MoveToPosition(float pXPos, float pYPos, int pTicks)
{
    mIgnoreMovementForBlocking = false;
    if(pTicks < 1 || GetPlanarDistance(pXPos, pYPos, mCombatPosition.mXCur, mCombatPosition.mYCur) < 3.0f)
    {
        mCombatPosition.MoveTo(pXPos, pYPos, 0);
    }
    else
    {
        mCombatPosition.MoveTo(pXPos, pYPos, pTicks);
    }
}
void AdvCombatEntity::MoveToIdealPosition(int pTicks)
{
    mIgnoreMovementForBlocking = false;
    if(pTicks < 1 || GetPlanarDistance(mCombatIdealX, mCombatIdealY, mCombatPosition.mXCur, mCombatPosition.mYCur) < 3.0f)
    {
        mCombatPosition.MoveTo(mCombatIdealX, mCombatIdealY, 0);
    }
    else
    {
        mCombatPosition.MoveTo(mCombatIdealX, mCombatIdealY, pTicks);
    }
}
void AdvCombatEntity::SetIgnoreMovementForBlocking(bool pFlag)
{
    mIgnoreMovementForBlocking = pFlag;
}
void AdvCombatEntity::SetCurrentInitiative(int pValue)
{
    mCurrentInitiative = pValue;
}
void AdvCombatEntity::SetTurnID(int pValue)
{
    mTurnID = pValue;
}
void AdvCombatEntity::SetFlashingWhite(bool pFlag)
{
    mIsFlashingWhite = pFlag;
}
void AdvCombatEntity::FlashBlack()
{
    mIsFlashingBlack = true;
    mBlackFlashTimer = 0;
}
void AdvCombatEntity::BeginKnockout()
{
    mIsKnockingOut = true;
    mKnockoutTimer = 0;
}
void AdvCombatEntity::FinishKnockout()
{
    mIsKnockingOut = false;
    mIsKnockedOut = true;
}
void AdvCombatEntity::SetGlobalJP(int pAmount)
{
    mGlobalJP = pAmount;
}
void AdvCombatEntity::SetFreeActions(int pFreeActions)
{
    mFreeActions = pFreeActions;
}
void AdvCombatEntity::SetFreeActionsPerformed(int pFreeActionsPerformed)
{
    mFreeActionsPerformed = pFreeActionsPerformed;
}
void AdvCombatEntity::ResetFreeActionsToTurnStart()
{
    //--Sets number of free actions to the combat stat pack value.
    mFreeActions = GetStatistic(STATS_FREEACTIONGEN);
}
void AdvCombatEntity::SetXP(int pXP)
{
    //--Set XP.
    mTotalXP = pXP;

    //--Recompute level.
    int tOldLevel = mLevel;
    for(int i = 0; i < xMaxLevel; i ++)
    {
        if(mTotalXP < xExpTable[i]) break;
        mLevel = i;
    }

    //--If the level changed, recompute stats.
    if(tOldLevel == mLevel) return;
    ComputeLevelStatistics(-1);
}
void AdvCombatEntity::ComputeLevelStatistics(int pLevel)
{
    //--Sets the job statistics to the listed level. Pass -1 to use the current level.
    if(pLevel == -1) pLevel = mLevel;
    if(pLevel < 0 || pLevel >= xMaxLevel || !rActiveJob) return;

    //--Call the level-up script.
    rActiveJob->CallScriptLevelUp(mLevel);

    //--Stats are now in a structure in the combat handler. Get them.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    CombatStatistics *rStats = rAdventureCombat->GetJobLevelUpStorage();

    //--Store current HP percentage.
    float tCurHPPct = GetHealthPercent();

    //--Copy them over.
    int tOldHP  = GetStatistic(STATS_HPMAX);
    int tOldAtk = GetStatistic(STATS_ATTACK);
    int tOldAcc = GetStatistic(STATS_ACCURACY);
    int tOldEvd = GetStatistic(STATS_EVADE);
    int tOldIni = GetStatistic(STATS_INITIATIVE);
    memcpy(&mJobStatistics, rStats, sizeof(CombatStatistics));
    ComputeStatistics();

    //--Set HP by percent.
    int tNewMaxHP = GetStatistic(STATS_HPMAX);
    SetHealth(tNewMaxHP * tCurHPPct);

    //--Debug.
    if(false)
    {
        fprintf(stderr, "%s Recomputing stats for level: %i\n", mLocalName, pLevel);
        fprintf(stderr, " Max HP     %i -> %i\n", tOldHP,  GetStatistic(STATS_HPMAX));
        fprintf(stderr, " Attack     %i -> %i\n", tOldAtk, GetStatistic(STATS_ATTACK));
        fprintf(stderr, " Accuracy   %i -> %i\n", tOldAcc, GetStatistic(STATS_ACCURACY));
        fprintf(stderr, " Evade      %i -> %i\n", tOldEvd, GetStatistic(STATS_EVADE));
        fprintf(stderr, " Initiative %i -> %i\n", tOldIni, GetStatistic(STATS_INITIATIVE));
    }
}
void AdvCombatEntity::GainJP(int pJP)
{
    //--If there is an active job, goes to that job.
    if(rActiveJob)
    {
        //--Get how much JP this job needs. If the amount is less than the amount gained, the rest
        //  goes to the global pool.
        int tJPCurrent = rActiveJob->GetCurrentJP();
        int tJPNeeded = rActiveJob->GetTotalJP();
        if(tJPCurrent + pJP > tJPNeeded)
        {
            rActiveJob->SetCurrentJP(tJPNeeded);
            GainGlobalJP(pJP - (tJPNeeded - tJPCurrent));
        }
        //--It all goes to the job.
        else
        {
            rActiveJob->SetCurrentJP(tJPCurrent + pJP);
        }
        return;
    }

    //--No active job somehow, it all goes to the global.
    GainGlobalJP(pJP);
}
void AdvCombatEntity::GainGlobalJP(int pJP)
{
    mGlobalJP += pJP;
}
void AdvCombatEntity::SetRewardXP(int pXP)
{
    mRewardXP = pXP;
    if(mRewardXP < 0) mRewardXP = 0;
}
void AdvCombatEntity::SetRewardJP(int pJP)
{
    mRewardJP = pJP;
    if(mRewardJP < 0) mRewardJP = 0;
}
void AdvCombatEntity::SetRewardPlatina(int pPlatina)
{
    mRewardPlatina = pPlatina;
    if(mRewardPlatina < 0) mRewardPlatina = 0;
}
void AdvCombatEntity::SetRewardDoctor(int pDoctor)
{
    mRewardDoctor = pDoctor;
}
void AdvCombatEntity::SetTurnKOd(int pTurn)
{
    mTurnKOd = pTurn;
}
void AdvCombatEntity::AddRewardItem(const char *pName)
{
    if(!pName) return;
    mRewardItems->AddElement("X", InitializeString(pName), &FreeThis);
}
void AdvCombatEntity::AddTag(const char *pTag)
{
    //--Tag already exists, increment it instead.
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(rInteger)
    {
        (*rInteger) = (*rInteger) + 1;
    }
    //--Create.
    else
    {
        int *nInteger = (int *)starmemoryalloc(sizeof(int));
        *nInteger = 1;
        mTagList->AddElement(pTag, nInteger, &FreeThis);
    }
}
void AdvCombatEntity::RemoveTag(const char *pTag)
{
    //--Tag doesn't exist, do nothing:
    int *rInteger = (int *)mTagList->GetElementByName(pTag);
    if(!rInteger) return;

    //--Greater than 1.
    if(*rInteger > 1)
    {
        (*rInteger) = (*rInteger) - 1;
    }
    //--Zero it off. Remove it.
    else
    {
        mTagList->RemoveElementS(pTag);
    }
}

//========================================= Core Methods ==========================================
void AdvCombatEntity::FullRestore()
{
    //--Restores all of an entity's HP and other stats that are restored at a rest point.
    SetHealth(mSummedStatistics.GetStatByIndex(STATS_HPMAX));
}
void AdvCombatEntity::HandleCombatStart()
{
    //--When combat begins, this is called for the player's party. Enemy units never call this.
    rActiveJobAtCombatStart = rActiveJob;

    //--Reset passive abilities. These will be re-added by the AdvCombatJob later.
    mrPassiveAbilityList->ClearList();

    //--Call combat start script.
    CallResponseScript(ACE_SCRIPT_CODE_BEGINCOMBAT);

    //--Order the active job to run its combat-begins script.
    if(rActiveJob) rActiveJob->CallScript(ADVCJOB_CODE_BEGINCOMBAT);

    //--Now iterate across all equipped abilities and call their combat start script.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(rAbilityGrid[x][y])   rAbilityGrid[x][y]->CallCode(ACA_SCRIPT_CODE_BEGINCOMBAT);
            if(rSecondaryGrid[x][y]) rSecondaryGrid[x][y]->CallCode(ACA_SCRIPT_CODE_BEGINCOMBAT);
        }
    }
    mrPassiveAbilityList = new SugarLinkedList(false);
}
void AdvCombatEntity::HandleCombatEnd()
{
    //--Handles combat ending. This occurs before the victory screen appears (if it is going to) so
    //  things like XP/Drops can be "adjusted". You cheaters.
    CallResponseScript(ACE_SCRIPT_CODE_ENDCOMBAT);

    //--Order the active job to run its combat-ends script.
    if(rActiveJob) rActiveJob->CallScript(ADVCJOB_CODE_ENDCOMBAT);

    //--Now iterate across all equipped abilities and call their combat start script. Also reset cooldowns.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(rAbilityGrid[x][y])
            {
                rAbilityGrid[x][y]->SetCooldown(0);
                rAbilityGrid[x][y]->CallCode(ACA_SCRIPT_CODE_COMBATENDS);
            }
            if(rSecondaryGrid[x][y])
            {
                rSecondaryGrid[x][y]->SetCooldown(0);
                rSecondaryGrid[x][y]->CallCode(ACA_SCRIPT_CODE_COMBATENDS);
            }
        }
    }

    //--Adrenaline gets added at 50% conversion to HP.
    int tAdrenaline = mAdrenaline / 2;
    mAdrenaline = 0;
    SetHealth(mHP + tAdrenaline);
    mAdrenalineEasingPack.MoveTo(mAdrenaline, -1);

    //--Shields get zeroed.
    mShield = 0;
    mShieldEasingPack.MoveTo(mShield, -1);
}
bool AdvCombatEntity::CheckKnockout()
{
    //--Checks if the entity is knocked out now but was not previously. Returns true if they were
    //  knocked out recently, false otherwise.
    if(mIsKnockedOut) return false;

    //--Has HP, not KO'd.
    if(mHP > 0) return false;

    //--HP is zero, we got KO'd.
    mIsKnockedOut = true;
    return true;
}
void AdvCombatEntity::CallResponseScript(int pCode)
{
    //--Calls response script with the matching code.
    if(pCode < 0 || pCode >= ACE_SCRIPT_CODE_TOTAL) return;
    if(!mResponseScriptPath || !mScriptResponses[pCode]) return;
    LuaManager::Fetch()->PushExecPop(this, mResponseScriptPath, 1, "N", (float)pCode);
}
void AdvCombatEntity::CallAIResponseScript(int pCode)
{
    if(!mAIScriptPath) return;
    LuaManager::Fetch()->PushExecPop(this, mAIScriptPath, 1, "N", (float)pCode);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void AdvCombatEntity::UpdateTimers()
{
    //--Easing packages.
    mHPEasingPack.Increment(EASING_CODE_QUADINOUT);
    mMPEasingPack.Increment(EASING_CODE_QUADINOUT);
    mAdrenalineEasingPack.Increment(EASING_CODE_QUADINOUT);
    mShieldEasingPack.Increment(EASING_CODE_QUADINOUT);
    mStunEasingPack.Increment(EASING_CODE_QUADINOUT);

    //--White flash. Is a toggle.
    if(mIsFlashingWhite)
    {
        if(mWhiteFlashTimer < ADVCE_FLASH_WHITE_TICKS) mWhiteFlashTimer ++;
    }
    else
    {
        if(mWhiteFlashTimer > 0) mWhiteFlashTimer --;
    }

    //--Black flash. Plays as a sequence.
    if(mIsFlashingBlack)
    {
        mBlackFlashTimer ++;
        if(mBlackFlashTimer >= ADVCE_FLASH_BLACK_TICKS) mIsFlashingBlack = false;
    }

    //--Knockout. Plays as a sequence.
    if(mIsKnockingOut)
    {
        mKnockoutTimer ++;
    }
}
void AdvCombatEntity::UpdatePosition()
{
    mCombatPosition.Increment(EASING_CODE_QUADOUT);
    if(mIgnoreMovementForBlocking && mCombatPosition.mTimer >= mCombatPosition.mTimerMax) mIgnoreMovementForBlocking = false;
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
SugarBitmap *AdvCombatEntity::GetCombatPortrait()
{
    return rCombatImage;
}
SugarBitmap *AdvCombatEntity::GetCombatCounterMask()
{
    return rCombatCountermask;
}
SugarBitmap *AdvCombatEntity::GetVictoryCounterMask()
{
    return rVictoryCountermask;
}
SugarBitmap *AdvCombatEntity::GetTurnIcon()
{
    return rTurnIcon;
}

//======================================= Pointer Routing =========================================
SugarLinkedList *AdvCombatEntity::GetAbilityList()
{
    return mAbilityList;
}
AdvCombatAbility *AdvCombatEntity::GetAbilityBySlot(int pX, int pY)
{
    if(pX < 0 || pX >= ACE_ABILITY_GRID_SIZE_X) return NULL;
    if(pY < 0 || pY >= ACE_ABILITY_GRID_SIZE_Y) return NULL;
    return rAbilityGrid[pX][pY];
}
AdvCombatAbility *AdvCombatEntity::GetSecondaryBySlot(int pX, int pY)
{
    if(pX < 0 || pX >= ACE_ABILITY_GRID_SIZE_X) return NULL;
    if(pY < 0 || pY >= ACE_ABILITY_GRID_SIZE_Y) return NULL;
    return rSecondaryGrid[pX][pY];
}
AdvCombatJob *AdvCombatEntity::GetActiveJob()
{
    return rActiveJob;
}
SugarLinkedList *AdvCombatEntity::GetJobList()
{
    return mJobList;
}
SugarLinkedList *AdvCombatEntity::GetEffectRenderList()
{
    return mrEffectsToShowThisTick;
}
SugarBitmap *AdvCombatEntity::GetVendorImage(int pIndex)
{
    if(pIndex < 0 || pIndex >= ADVCE_VENDOR_SPRITES_TOTAL) return NULL;
    return rVendorImages[pIndex];
}

//====================================== Static Functions =========================================
float AdvCombatEntity::ComputeResistance(int pResistance)
{
    //--Standard resistance formula is (1.0f - (0.95f ^ pResistance)). This allows for negatives.
    //  This formula returns the %age reductions. A value of 1 return 0.05, 2 returns 0.0975, etc.
    //--Note that individual abilities may decide to compute resistance differently!
    return pow(0.95f, (float)pResistance);
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
