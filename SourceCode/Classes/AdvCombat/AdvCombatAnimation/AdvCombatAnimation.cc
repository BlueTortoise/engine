//--Base
#include "AdvCombatAnimation.h"

//--Classes
#include "SugarBitmap.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

//=========================================== System ==============================================
AdvCombatAnimation::AdvCombatAnimation()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATANIMATION;

    //--[AdvCombatAnimation]
    //--System
    //--Position
    mXOffset = 0.0f;
    mYOffset = 0.0f;

    //--Storage
    mTicksPerFrame = 1.0f;
    mTotalTicks = 1;
    mImagesTotal = 0;
    mrImages = NULL;

    //--Instances
    mInstanceList = new SugarLinkedList(true);
}
AdvCombatAnimation::~AdvCombatAnimation()
{
    free(mrImages);
    delete mInstanceList;
}

//====================================== Property Queries =========================================
bool AdvCombatAnimation::HasAnyInstances()
{
    if(mInstanceList->GetListSize() > 0) return true;
    return false;
}

//========================================= Manipulators ==========================================
void AdvCombatAnimation::SetOffsets(float pX, float pY)
{
    mXOffset = pX;
    mYOffset = pY;
}
void AdvCombatAnimation::SetTicksPerFrame(float pTicksPerFrame)
{
    mTicksPerFrame = pTicksPerFrame;
    if(mTicksPerFrame < 0.01f) mTicksPerFrame = 0.01f;
    mTotalTicks = mImagesTotal * mTicksPerFrame;
}
void AdvCombatAnimation::AllocateFrames(int pTotal)
{
    //--Dealloacte.
    free(mrImages);

    //--Reset.
    mImagesTotal = 0;
    mrImages = NULL;
    mTotalTicks = 0.0f;
    if(pTotal < 1) return;

    //--Allocate.
    mImagesTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mrImages = (SugarBitmap **)starmemoryalloc(sizeof(SugarBitmap *) * mImagesTotal);
    memset(mrImages, 0, sizeof(SugarBitmap **) * mImagesTotal);
    mTotalTicks = mImagesTotal * mTicksPerFrame;
}
void AdvCombatAnimation::SetFrame(int pSlot, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= mImagesTotal) return;
    mrImages[pSlot] = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatAnimation::CreateInstance(const char *pRefname, int pStartTicks, float pX, float pY)
{
    if(!pRefname) return;
    AnimationInstance *nInstance = (AnimationInstance *)starmemoryalloc(sizeof(AnimationInstance));
    nInstance->Initialize();
    nInstance->mXCenter = pX;
    nInstance->mYCenter = pY;
    nInstance->mTimer = pStartTicks;
    mInstanceList->AddElement(pRefname, nInstance, &FreeThis);
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void AdvCombatAnimation::Update()
{
    AnimationInstance *rInstance = (AnimationInstance *)mInstanceList->SetToHeadAndReturn();
    while(rInstance)
    {
        rInstance->mTimer ++;
        if(rInstance->mTimer >= mTotalTicks) mInstanceList->RemoveRandomPointerEntry();
        rInstance = (AnimationInstance *)mInstanceList->IncrementAndGetRandomPointerEntry();
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void AdvCombatAnimation::Render()
{
    AnimationInstance *rInstance = (AnimationInstance *)mInstanceList->SetToHeadAndReturn();
    while(rInstance)
    {
        //--Resolve frame.
        int tFrame = rInstance->mTimer / mTicksPerFrame;
        if(tFrame >= 0 && tFrame < mImagesTotal && mrImages[tFrame])
        {
            mrImages[tFrame]->Draw(rInstance->mXCenter + mXOffset, rInstance->mYCenter + mYOffset);
        }

        //--Next.
        rInstance = (AnimationInstance *)mInstanceList->IncrementAndGetRandomPointerEntry();
    }
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
