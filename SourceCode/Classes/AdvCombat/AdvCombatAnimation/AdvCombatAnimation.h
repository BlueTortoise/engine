//--[AdvCombatAnimation]
//--Represents a visual animation in combat, such as a slash on an enemy or a flame from a spell.
//  The class stores a set of images and a set of instances. Each instance is a "play" of the
//  animation at a location with its own timer.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--Instance. Contains a playing instance of this animation.
typedef struct AnimationInstance
{
    //--Members
    int mTimer;
    float mXCenter;
    float mYCenter;

    //--Functions
    void Initialize()
    {
        mTimer = 0;
        mXCenter = 0.0f;
        mYCenter = 0.0f;
    }
}AnimationInstance;

//--[Local Definitions]
//--[Classes]
class AdvCombatAnimation : public RootObject
{
    private:
    //--System
    //--Position
    float mXOffset;
    float mYOffset;

    //--Storage
    float mTicksPerFrame;
    float mTotalTicks;
    int mImagesTotal;
    SugarBitmap **mrImages;

    //--Instances
    SugarLinkedList *mInstanceList;

    protected:

    public:
    //--System
    AdvCombatAnimation();
    virtual ~AdvCombatAnimation();

    //--Public Variables
    //--Property Queries
    bool HasAnyInstances();

    //--Manipulators
    void SetOffsets(float pX, float pY);
    void SetTicksPerFrame(float pTicksPerFrame);
    void AllocateFrames(int pTotal);
    void SetFrame(int pSlot, const char *pDLPath);
    void CreateInstance(const char *pRefname, int pStartTicks, float pX, float pY);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombatAnimation_GetProperty(lua_State *L);
int Hook_AdvCombatAnimation_SetProperty(lua_State *L);
