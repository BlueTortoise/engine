//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"

void AdvCombat::MarkHandledAction()
{
    mAIHandledAction = true;
}
void AdvCombat::SetAbilityAsActive(int pSlot)
{
    //--Given a slot, takes the given ability in that slot from the acting entity and makes it active.
    rActiveAbility = NULL;

    //--Make sure there's an active entity.
    AdvCombatEntity *rActiveEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActiveEntity) return;

    //--Get the ability list.
    SugarLinkedList *rAbilityList = rActiveEntity->GetAbilityList();
    if(!rAbilityList) return;

    //--Get the ability in the slot. If it's null, sets the active ability to NULL!
    AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(pSlot);
    rActiveAbility = rAbility;
}
void AdvCombat::RunActiveAbilityTargetScript()
{
    //--If the active ability exists, runs its target script.
    if(!rActiveAbility) return;
    rActiveAbility->CallCode(ACA_SCRIPT_CODE_PAINTTARGETS);
}
void AdvCombat::SetTargetClusterAsActive(int pSlot)
{
    //--Given a slot, sets that target cluster as the active one. Pass an invalid slot to NULL it off.
    rActiveTargetCluster = (TargetCluster *)mTargetClusters->GetElementBySlot(pSlot);
}
