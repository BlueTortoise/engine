//--[KPopDanceMove]
//--Represents a dance move the dancer can perform.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class KPopDanceMove
{
    public:
    //--System
    int mTotalTicks;

    //--Images
    int mTotalFrames;
    int *mTicksPerFrame;
    SugarBitmap **rImages;

    protected:

    public:
    //--System
    KPopDanceMove();
    ~KPopDanceMove();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    int GetFrameTicks(int pIndex);
    SugarBitmap *GetImage(int pIndex);

    //--Manipulators
    void AllocateFrames(int pTotalFrames);
    void SetFrame(int pIndex, const char *pDLPath, int pTicksToPlay);

    //--Core Methods
    void ComputeTotalFrames();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing

    //--Static Functions
    //--Lua Hooking
};

//--Hooking Functions

