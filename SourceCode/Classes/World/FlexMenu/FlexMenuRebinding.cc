//--Base
#include "FlexMenu.h"

//--Classes
#include "AdventureMenu.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarFont.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "SaveManager.h"

//--[Local Definitions]
#define PRIMARY_OFFSET 150.0f
#define SECONDARY_OFFSET 300.0f

//--[Property Queries]
bool FlexMenu::IsRebindingMode()
{
    return mIsRebindingMode;
}

//--[Manipulators]
void FlexMenu::ActivateRebindingMode()
{
    //--Sets flags to activate key rebinding.
    if(!Images.mIsReady) return;
    mRebindingCursor = -1;
    mIsRebindingMode = true;
    mIsRebindSecondary = false;
    mCurrentRebindKey = -1;

    //--Build a name lookup.
    SetMemoryData(__FILE__, __LINE__);
    mControlNames = (char **)starmemoryalloc(sizeof(char *) * FM_CONTROLS_TOTAL);
    mControlNames[0] = InitializeString("Up");
    mControlNames[1] = InitializeString("Left");
    mControlNames[2] = InitializeString("Down");
    mControlNames[3] = InitializeString("Right");
    mControlNames[4] = InitializeString("Activate");
    mControlNames[5] = InitializeString("Cancel");
    mControlNames[6] = InitializeString("Run");
    mControlNames[7] = InitializeString("DnLevel");
    mControlNames[8] = InitializeString("UpLevel");

    //--Get scancode information.
    ControlManager *rControlManager = ControlManager::Fetch();
    for(int i = 0; i < FM_CONTROLS_TOTAL; i ++)
    {
        //--Set all six entries to -1, to indicate they aren't bound.
        for(int p = 0; p < 6; p ++) mCurrentScancodes[i][p] = -1;

        //--Get the control.
        ControlState *rControlState = rControlManager->GetControlState(mControlNames[i]);
        if(!rControlState) continue;

        //--Fill.
        mCurrentScancodes[i][0] = rControlState->mWatchKeyPri;
        mCurrentScancodes[i][1] = rControlState->mWatchMouseBtnPri;
        mCurrentScancodes[i][2] = rControlState->mWatchJoyPri;
        mCurrentScancodes[i][3] = rControlState->mWatchKeySec;
        mCurrentScancodes[i][4] = rControlState->mWatchMouseBtnSec;
        mCurrentScancodes[i][5] = rControlState->mWatchJoySec;
    }

    //--Position constants.
    float cLftPosition = VIRTUAL_CANVAS_X * 0.15f;
    float cTopPosition = VIRTUAL_CANVAS_Y * 0.15f;
    float cXSizePerControl = VIRTUAL_CANVAS_X * 0.65f;
    float cYSizePerControl = Images.Data.rUIFont->GetTextHeight() + (AM_STD_INDENT * 2.0f);

    //--Position lookups.
    for(int i = 0; i < FM_CONTROLS_TOTAL; i ++)
    {
        mControlCoordinates[i].SetWH(cLftPosition, cTopPosition + (cYSizePerControl * i), cXSizePerControl, cYSizePerControl);
    }

    //--Save and Defaults button. These are below the control lookups.
    mSaveBtn.SetWH(cLftPosition, cTopPosition + (cYSizePerControl * (FM_CONTROLS_TOTAL + 1)), cXSizePerControl * 0.5f, cYSizePerControl);
    mDefaultsBtn.SetWH(cLftPosition + (cXSizePerControl * 0.5f), cTopPosition + (cYSizePerControl * (FM_CONTROLS_TOTAL + 1)), cXSizePerControl * 0.5f, cYSizePerControl);
}
void FlexMenu::DeactivateRebindingMode(bool pSave)
{
    //--Deactivates rebinding mode. If pSave is true, saves the control changes. Otherwise, sets them
    //  back to what they were when the mode was activated.
    mIsRebindingMode = false;

    //--If flagged to save, run the overbind here.
    if(pSave && mControlNames)
    {
        ControlManager *rControlManager = ControlManager::Fetch();
        for(int i = 0; i < FM_CONTROLS_TOTAL; i ++)
        {
            rControlManager->OverbindControl(mControlNames[i], false, mCurrentScancodes[i][0], mCurrentScancodes[i][1], mCurrentScancodes[i][2]);
            rControlManager->OverbindControl(mControlNames[i], true , mCurrentScancodes[i][3], mCurrentScancodes[i][4], mCurrentScancodes[i][5]);
        }

        //--Write to the save file.
        SaveManager::Fetch()->SaveControlsFile();
    }

    //--Clear the name lookups.
    if(mControlNames)
    {
        for(int i = 0; i < FM_CONTROLS_TOTAL; i ++) free(mControlNames[i]);
        free(mControlNames);
        mControlNames = NULL;
    }
}
void FlexMenu::DeactivateRebindingModeDefaults()
{
    //--Returns the controls to their default state. Also ceases rebinding mode.
    mIsRebindingMode = false;

    //--Clear.
    if(mControlNames)
    {
        for(int i = 0; i < FM_CONTROLS_TOTAL; i ++) free(mControlNames[i]);
        free(mControlNames);
        mControlNames = NULL;
    }

    //--Set.
    ControlManager *rControlManager = ControlManager::Fetch();
    rControlManager->OverbindControl("Up",       false, 84, -1, -1);
    rControlManager->OverbindControl("Up",       true,  23, -1, -1);
    rControlManager->OverbindControl("Left",     false, 82, -1, -1);
    rControlManager->OverbindControl("Left",     true,   1, -1, -1);
    rControlManager->OverbindControl("Down",     false, 85, -1, -1);
    rControlManager->OverbindControl("Down",     true,  19, -1, -1);
    rControlManager->OverbindControl("Right",    false, 83, -1, -1);
    rControlManager->OverbindControl("Right",    true,   4, -1, -1);
    rControlManager->OverbindControl("Activate", false, 26, -1, -1);
    rControlManager->OverbindControl("Activate", true,  -1, -1, -1);
    rControlManager->OverbindControl("Cancel",   false, 24, -1, -1);
    rControlManager->OverbindControl("Cancel",   true,  -1, -1, -1);
    rControlManager->OverbindControl("Run",      false,215, -1, -1);
    rControlManager->OverbindControl("Run",      true,  -1, -1, -1);
    rControlManager->OverbindControl("UpLevel",  false,  5, -1, -1);
    rControlManager->OverbindControl("UpLevel",  true,  81, -1, -1);
    rControlManager->OverbindControl("DnLevel",  false, 17, -1, -1);
    rControlManager->OverbindControl("DnLevel",  true,  80, -1, -1);

    //--Write to the save file.
    SaveManager::Fetch()->SaveControlsFile();
}
void FlexMenu::UpdateRebindingMode()
{
    //--[Setup]
    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--[Normal Mode]
    //--Handle mouse input.
    if(mCurrentRebindKey == -1)
    {
        //--Press Escape to go back to the loading menu.
        if(rControlManager->IsFirstPress("Escape"))
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            DeactivateRebindingMode(false);
            return;
        }

        //--Setup.
        int tOldCursor = mRebindingCursor;
        mRebindingCursor = -1;

        //--Get the mouse position.
        int tMouseX, tMouseY, tMouseZ;
        rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

        //--Check the Back button.
        if(IsPointWithin2DReal(tMouseX, tMouseY, mBackBtn))
        {
            mRebindingCursor = -2;
        }
        //--Check the Save button.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mSaveBtn))
        {
            mRebindingCursor = -3;
        }
        //--Check the Defaults button.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mDefaultsBtn))
        {
            mRebindingCursor = -4;
        }
        //--Otherwise, check the various control buttons.
        else
        {
            //--Iterate.
            for(int i = 0; i < FM_CONTROLS_TOTAL; i ++)
            {
                //--Check the primary button.
                if(IsPointWithin(tMouseX, tMouseY, mControlCoordinates[i].mLft + AM_STD_INDENT + PRIMARY_OFFSET, mControlCoordinates[i].mTop + AM_STD_INDENT, mControlCoordinates[i].mLft + AM_STD_INDENT + PRIMARY_OFFSET + 100.0f, mControlCoordinates[i].mTop + AM_STD_INDENT + Images.Data.rUIFont->GetTextHeight()))
                {
                    mRebindingCursor = (i*2);
                }
                //--Check the secondary button.
                else if(IsPointWithin(tMouseX, tMouseY, mControlCoordinates[i].mLft + AM_STD_INDENT + SECONDARY_OFFSET, mControlCoordinates[i].mTop + AM_STD_INDENT, mControlCoordinates[i].mLft + AM_STD_INDENT + SECONDARY_OFFSET + 100.0f, mControlCoordinates[i].mTop + AM_STD_INDENT + Images.Data.rUIFont->GetTextHeight()))
                {
                    mRebindingCursor = (i*2) + 1;
                }
            }
        }

        //--If the cursor changed, play a sound effect. If it's -1, don't. -2 is the back button.
        if(tOldCursor != mRebindingCursor && mRebindingCursor != -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Clicking.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--If the cursor was on -2, go back.
            if(mRebindingCursor == -2)
            {
                DeactivateRebindingMode(false);
            }
            //--If the cursor was on -1, do nothing.
            else if(mRebindingCursor == -1)
            {
            }
            //--Save changes.
            else if(mRebindingCursor == -3)
            {
                DeactivateRebindingMode(true);
            }
            //--Restore defaults.
            else if(mRebindingCursor == -4)
            {
                DeactivateRebindingModeDefaults();
            }
            //--If the cursor was on any other control, begin rebinding.
            else
            {
                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

                //--Basic flags.
                mIsRebindSecondary = (mRebindingCursor % 2 == 1);

                //--Set to rebinding mode.
                mCurrentRebindKey = mRebindingCursor / 2;
            }
        }
    }
    //--[Rebinding]
    //--Handle user inputs and rebind as necessary.
    else
    {
        //--Press Escape to clear the binding. The user cannot bind over Escape.
        if(rControlManager->IsFirstPress("Escape"))
        {
            //--Set the scancodes to -1's.
            int tOffset = 0;
            if(mIsRebindSecondary) tOffset = 3;
            mCurrentScancodes[mCurrentRebindKey][tOffset + 0] = -1;
            mCurrentScancodes[mCurrentRebindKey][tOffset + 1] = -1;
            mCurrentScancodes[mCurrentRebindKey][tOffset + 2] = -1;

            //--Cancel rebinding mode.
            mCurrentRebindKey = -1;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return;
        }

        //--Otherwise, wait for a keypress.
        if(rControlManager->IsAnyKeyPressed())
        {
            //--Store these scancodes. They don't get updated until the user exits this menu.
            int tOffset = 0;
            if(mIsRebindSecondary) tOffset = 3;
            rControlManager->GetKeyPressCodes(mCurrentScancodes[mCurrentRebindKey][tOffset + 0], mCurrentScancodes[mCurrentRebindKey][tOffset + 1], mCurrentScancodes[mCurrentRebindKey][tOffset + 2]);
            //fprintf(stderr, "KMJ: %i %i %i\n", mCurrentScancodes[mCurrentRebindKey][tOffset + 0], mCurrentScancodes[mCurrentRebindKey][tOffset + 1], mCurrentScancodes[mCurrentRebindKey][tOffset + 2]);

            //--Cancel rebinding mode.
            mCurrentRebindKey = -1;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return;
        }
    }
}
void FlexMenu::RenderRebindingMode()
{
    //--[Documentation and Setup]
    //--Renders Rebinding mode. Duh.
    if(!Images.mIsReady || !mControlNames) return;

    //--Setup.
    char tBuffer[128];

    //--Colors.
    StarlightColor cBackingColorGrey = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.85f);
    StarlightColor cBackingColorBlue = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.4f, 0.85f);
    StarlightColor *rUseColor = &cBackingColorGrey;

    //--[Back Button]
    if(mRebindingCursor == -2) rUseColor = &cBackingColorBlue;
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mBackBtn, 0x01FF, *rUseColor);
    Images.Data.rUIFont->DrawText(mBackBtn.mLft + AM_STD_INDENT, mBackBtn.mTop + AM_STD_INDENT, 0, 1.0f, "Back");

    //--[Header]
    //--Render some instructions.
    strcpy(tBuffer, "Click a key to edit.");
    float cTextWid = Images.Data.rUIFont->GetTextWidth(tBuffer) * 3.0f * 0.5f;
    float cTextHei = Images.Data.rUIFont->GetTextHeight() * 3.0f * 0.5f;
    Images.Data.rUIFont->DrawText((VIRTUAL_CANVAS_X * 0.5f) - cTextWid, (VIRTUAL_CANVAS_Y * 0.08f) - cTextHei, 0, 3.0f, tBuffer);

    //--[List of Controls]
    //--Render.
    for(int i = 0; i < FM_CONTROLS_TOTAL; i ++)
    {
        //--Render a button around the object.
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mControlCoordinates[i], 0x01FF);

        //--Name of the control.
        glColor3f(1.0f, 1.0f, 1.0f);
        Images.Data.rUIFont->DrawText(mControlCoordinates[i].mLft + AM_STD_INDENT, mControlCoordinates[i].mTop + AM_STD_INDENT, 0, 1.0f, mControlNames[i]);

        //--If the primary scancode is selected...
        if(mRebindingCursor == (i*2))
            glColor3f(0.0f, 0.0f, 1.0f);
        else
            glColor3f(1.0f, 1.0f, 1.0f);

        //--Primary scancode.
        char tLetter = 'K';
        int tUseScancode = mCurrentScancodes[i][0];
        if(tUseScancode == -2) {tLetter = 'M'; tUseScancode = mCurrentScancodes[i][1]; }
        if(tUseScancode == -2) {tLetter = 'J'; tUseScancode = mCurrentScancodes[i][2]; }

        //--If the scancode is still -1, print "Unbound" instead of the number.
        if(tUseScancode == -1)
        {
            Images.Data.rUIFont->DrawText(mControlCoordinates[i].mLft + AM_STD_INDENT + PRIMARY_OFFSET, mControlCoordinates[i].mTop + AM_STD_INDENT, 0, 1.0f, "Unbound");
        }
        //--Print the scancode.
        else
        {
            Images.Data.rUIFont->DrawTextArgs(mControlCoordinates[i].mLft + AM_STD_INDENT + PRIMARY_OFFSET, mControlCoordinates[i].mTop + AM_STD_INDENT, 0, 1.0f, "%c: %i", tLetter, tUseScancode);
        }

        //--If the secondary scancode is selected...
        if(mRebindingCursor == (i*2) + 1)
            glColor3f(0.0f, 0.0f, 1.0f);
        else
            glColor3f(1.0f, 1.0f, 1.0f);

        //--Secondary scancode.
        tLetter = 'K';
        tUseScancode = mCurrentScancodes[i][3];
        if(tUseScancode == -2) {tLetter = 'M'; tUseScancode = mCurrentScancodes[i][4]; }
        if(tUseScancode == -2) {tLetter = 'J'; tUseScancode = mCurrentScancodes[i][5]; }

        //--If the scancode is still -1, print "Unbound" instead of the number.
        if(tUseScancode == -1)
        {
            Images.Data.rUIFont->DrawText(mControlCoordinates[i].mLft + AM_STD_INDENT + SECONDARY_OFFSET, mControlCoordinates[i].mTop + AM_STD_INDENT, 0, 1.0f, "Unbound");
        }
        //--Print the scancode.
        else
        {
            Images.Data.rUIFont->DrawTextArgs(mControlCoordinates[i].mLft + AM_STD_INDENT + SECONDARY_OFFSET, mControlCoordinates[i].mTop + AM_STD_INDENT, 0, 1.0f, "%c: %i", tLetter, tUseScancode);
        }

        //--Clean.
        glColor3f(1.0f, 1.0f, 1.0f);
    }

    //--[Save Button]
    rUseColor = &cBackingColorGrey;
    if(mRebindingCursor == -3) rUseColor = &cBackingColorBlue;
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mSaveBtn, 0x01FF, *rUseColor);
    Images.Data.rUIFont->DrawText(mSaveBtn.mLft + AM_STD_INDENT, mSaveBtn.mTop + AM_STD_INDENT, 0, 1.0f, "Save Changes");

    //--[Defaults Button]
    rUseColor = &cBackingColorGrey;
    if(mRebindingCursor == -4) rUseColor = &cBackingColorBlue;
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mDefaultsBtn, 0x01FF, *rUseColor);
    Images.Data.rUIFont->DrawText(mDefaultsBtn.mLft + AM_STD_INDENT, mDefaultsBtn.mTop + AM_STD_INDENT, 0, 1.0f, "Defaults");

    //--[Rebinding Mode]
    //--Render a box over the screen prompting the user to push a key.
    glColor3f(1.0f, 1.0f, 1.0f);
    if(mCurrentRebindKey != -1)
    {
        //--Setup.
        float cTextSize = 2.0f;

        //--Black underlay.
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.0f, 0.0f, 0.0f, 0.85f);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        StarlightColor::ClearMixer();

        //--Box.
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, VIRTUAL_CANVAS_X * 0.00f, VIRTUAL_CANVAS_Y * 0.36f, VIRTUAL_CANVAS_X * 1.00f, VIRTUAL_CANVAS_Y * 0.50f, 0x01FF);

        //--Text.
        strcpy(tBuffer, "Press the key/joypad/mouse button to bind with.");
        cTextWid = Images.Data.rUIFont->GetTextWidth(tBuffer) * cTextSize * 0.5f;
        cTextHei = Images.Data.rUIFont->GetTextHeight()       * cTextSize * 0.5f;
        Images.Data.rUIFont->DrawText((VIRTUAL_CANVAS_X * 0.5f) - cTextWid, (VIRTUAL_CANVAS_Y * 0.40f) - cTextHei, 0, cTextSize, tBuffer);

        //--How to clear.
        strcpy(tBuffer, "Press Escape to clear.");
        cTextWid = Images.Data.rUIFont->GetTextWidth(tBuffer) * cTextSize * 0.5f;
        cTextHei = Images.Data.rUIFont->GetTextHeight()       * cTextSize * 0.5f;
        Images.Data.rUIFont->DrawText((VIRTUAL_CANVAS_X * 0.5f) - cTextWid, (VIRTUAL_CANVAS_Y * 0.40f) - cTextHei + (Images.Data.rUIFont->GetTextHeight() * cTextSize), 0, cTextSize, tBuffer);
    }
}
