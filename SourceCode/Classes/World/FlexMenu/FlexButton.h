//--[FlexButton]
//--Flexible Button (duh) for the FlexMenu. Inherits from the base SugarButton, but
//  implements the update and render logic the program needs to use it.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "SugarButton.h"

//--[Local Structures]
//--[Local Definitions]
#define DEF_SLIDER_WID 150.0f
#define DEF_SLIDER_PADDING 30.0f
#define FB_TRIANGLE_PROP 9.0f

//--[Classes]
class FlexButton : public SugarButton
{
    private:
    //--System
    bool mIsHighlighted;

    //--Constants
    float cIndicatorOffset;
    float cBarWidth;
    float cBarHeight;
    float cThickness;

    //--Display
    float mFontSize;
    SugarFont *rOverrideFont;

    //--Options
    bool mIsOptionMode;
    int mCurrentOption;
    int mOptionsTotal;
    char **mOptions;

    //--Slider Mode
    bool mIsSliderMode;
    int mCurrentSlider;
    int mSliderMin, mSliderMax;
    int mIncrementBtnA, mIncrementBtnB;
    float mSliderWidth;
    char *mLyingLengthString;

    //--Slider Controls
    bool mIsDraggingSlider;

    //--Description Mode
    char *mDescription;
    SugarBitmap *rImgPointer;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            SugarFont *rUIFont;
        }Data;
    }Images;

    //--Statics
    static bool xHighlightIsBackground;

    protected:

    public:
    //--System
    FlexButton();
    virtual ~FlexButton();

    //--Public Variables
    //--Property Queries
    bool IsHighlighted();
    float GetTextWidth();
    bool IsMouseOverMe(float pMouseX, float pMouseY);
    bool IsOptionMode();
    bool IsSliderMode();
    int GetOptionCursor();
    int GetSliderCursor();
    const char *GetDescription();
    SugarBitmap *GetDescriptionImage();

    //--Manipulators
    void SetHighlight(bool pFlag);
    void SetFontSize(float pFactor);
    void ActivateOptionMode(int pOptions);
    void SetOption(int pSlot, const char *pText);
    void SetOptionCursor(int pCursor);
    void SetSliderMode(int pMin, int pMax);
    void SetSliderCurrent(int pValue);
    void SetLyingLengthString(const char *pText);
    void SetDescription(const char *pDescription);
    void SetImageS(const char *pImagePath);

    //--Core Methods
    SugarFont *ResolveFont();
    virtual void Execute();

    private:
    //--Private Core Methods
    float CalcSliderLeftEdge();

    public:
    //--Update
    virtual void Update(int pMouseX, int pMouseY);

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_FlexButton_SetProperty(lua_State *L);
int Hook_FlexButton_GetProperty(lua_State *L);
