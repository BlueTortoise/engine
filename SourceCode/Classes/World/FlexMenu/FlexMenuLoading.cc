//--Base
#include "FlexMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureMenu.h"
#include "StringEntry.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFileSystem.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "SaveManager.h"

//--[Manipulators]
void FlexMenu::ActivateLoadingMode()
{
    //--Clears loading mode and activates it using the provided path. Path can be a relative or absolute directory.
    mIsLoadingMode = false;
    mLoadingTimer = 0;
    mLoadingCursor = 0;
    mLoadingOffset = 0;
    mLoadingPackList->ClearList();
    mSelectedOption = -1;
    MapManager::xDontRenderExtras = false;

    //--Setup.
    char tNameBuf[4];
    char tFileBuf[80];
    SaveManager *rSaveManager = SaveManager::Fetch();

    //--Game Directory:
    const char *rAdventureDir = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sAdventurePath");

    //--Scan the given directory.
    for(int i = 0; i < AM_SCAN_TOTAL; i ++)
    {
        //--Create the name.
        sprintf(tFileBuf, "%s/../../Saves/File%03i.slf", rAdventureDir, i);

        //--Check for existence. If the pack comes back NULL, it doesn't exist.
        LoadingPack *nReceivedPack = rSaveManager->GetSaveInfo(tFileBuf);
        if(!nReceivedPack) continue;

        //--If the pack exists, store it.
        sprintf(tNameBuf, "%i", i);
        mLoadingPackList->AddElementAsHead(tNameBuf, nReceivedPack, &FreeThis);

        //--Build pointer references.
        CrossloadSaveImages(nReceivedPack);
    }

    //--Flags.
    mIsLoadingMode = true;
    MapManager::xDontRenderExtras = true;
}
void FlexMenu::DeactivateLoadingMode()
{
    //--Deactivates loading mode, deallocating all its assets.
    mIsLoadingMode = false;
    mLoadingTimer = 0;
    mLoadingCursor = 0;
    mLoadingOffset = 0;
    mLoadingPackList->ClearList();
    mSelectedOption = -1;
    MapManager::xDontRenderExtras = false;
}

//--[Core Methods]
void FlexMenu::CrossloadSaveImages(LoadingPack *pPack)
{
    //--Given a loading pack, checks the names of the members and provides reference pointers for them. These pointers go
    //  to the DataLibrary, not the menu, so they are useful after this object is deallocated.
    if(!pPack) return;

    //--Setup.
    char tBuffer[512];
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--For each party member...
    for(int p = 0; p < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; p ++)
    {
        sprintf(tBuffer, "Root/Images/GUI/LoadingImg/%s", pPack->mPartyNames[p]);
        pPack->rRenderImg[p] = (SugarBitmap *)rDataLibrary->GetEntry(tBuffer);
    }
}

//--[Update]
void FlexMenu::UpdateLoadingMode()
{
    //--[Documentation and Setup]
    //--Fast-access Pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--[String Entry Form]
    //--When entering strings, pass the update down.
    if(mIsEnteringString)
    {
        mStringEntryForm->Update();
        if(mStringEntryForm->IsComplete())
        {
            //--Flag.
            mIsEnteringString = false;

            //--Get the string entered. It must be at least one character long.
            const char *rEnteredString = mStringEntryForm->GetString();
            if(strlen(rEnteredString) < 1) return;

            //--Get the loading pack in question.
            LoadingPack *rPack = (LoadingPack *)mLoadingPackList->GetElementBySlot(mLoadingCursor - 1 + mLoadingOffset);
            if(!rPack) return;

            //--Modify the string.
            strcpy(rPack->mFileName, rEnteredString);

            //--Load the file into a VirtualFile.
            VirtualFile *tVFile = new VirtualFile(rPack->mFilePath, true);

            //--Open the save file back up.
            FILE *fOutfile = fopen(rPack->mFilePath, "wb");
            if(!fOutfile) { delete tVFile; return; }

            //--Seek past the header and the loadinfo.
            tVFile->Seek(strlen("STARv200LOADINFO_"));

            //--Seek past this string.
            char *tDeleteString = tVFile->ReadLenString();
            free(tDeleteString);

            //--Write the header again.
            fwrite("STARv200LOADINFO_", sizeof(char), strlen("STARv200LOADINFO_"), fOutfile);

            //--Write the new string.
            uint16_t tLen = strlen(rEnteredString);
            fwrite(&tLen, sizeof(uint16_t), 1, fOutfile);
            fwrite(rEnteredString, sizeof(char), tLen, fOutfile);

            //--Now write everything else from the file back into this one.
            int tCursor = tVFile->GetCurPosition();
            int tLength = tVFile->GetLength();
            uint8_t *rDataBuf = (uint8_t *)tVFile->GetData();
            fwrite(&rDataBuf[tCursor+1], sizeof(uint8_t), tLength - tCursor, fOutfile);

            //--Clean up.
            delete tVFile;
            fclose(fOutfile);
        }
        else if(mStringEntryForm->IsCancelled())
        {
            mIsEnteringString = false;
        }
        return;
    }

    //--[Hotkeys]
    //--Pressing escape deactivates loading mode.
    if(rControlManager->IsFirstPress("Escape"))
    {
        DeactivateLoadingMode();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    //--Setup.
    int tOldCursor = mLoadingCursor;

    //--[Mouse Movement]
    //--Get the mouse position.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--If the mouse moved, change the highlighted object.
    if(tMouseX != mPreviousMouseX || tMouseY != mPreviousMouseY)
    {
        //--Reset the loading cursor.
        mLoadingCursor = -1;

        //--Check the New Game button.
        if(IsPointWithin(tMouseX, tMouseY, VIRTUAL_CANVAS_X * 0.10f, VIRTUAL_CANVAS_Y * 0.17f, VIRTUAL_CANVAS_X * 0.90f, VIRTUAL_CANVAS_Y * 0.27f))
        {
            mLoadingCursor = 0;
        }
        //--Check the Back button.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mBackBtn))
        {
            mLoadingCursor = -2;
        }
        //--Check the Controls button.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mControlsBtn))
        {
            mLoadingCursor = -3;
        }
        //--Scroll up.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mScrollUp))
        {
            mLoadingCursor = -4;
        }
        //--Scroll down.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mScrollDn))
        {
            mLoadingCursor = -5;
        }
        //--Otherwise, check the loading and new game buttons.
        else
        {
            int cEntriesPerPage = 4;
            float cSizePerBox = VIRTUAL_CANVAS_Y * 0.15f;
            float cBoxSpacing = VIRTUAL_CANVAS_Y * 0.01f;
            for(int i = 0; i < cEntriesPerPage; i ++)
            {
                //--Compute position.
                float cTop = (VIRTUAL_CANVAS_Y * 0.30f) + (cSizePerBox * i) + (cBoxSpacing * i);
                if(IsPointWithin(tMouseX, tMouseY, VIRTUAL_CANVAS_X * 0.10f, cTop, VIRTUAL_CANVAS_X * 0.90f, cTop + cSizePerBox))
                {
                    mLoadingCursor = i + 1;
                }
            }
        }

        //--During a scrollbar click and drag, handle the movement.
        if(mIsClickingScrollbar)
        {
            //--Constants.
            TwoDimensionReal cBoxDimensions;
            int cEntriesPerPage = FM_FILES_PER_PAGE;
            float cSizePerBox = VIRTUAL_CANVAS_Y * 0.15f;
            float cBoxSpacing = VIRTUAL_CANVAS_Y * 0.01f;
            cBoxDimensions.Set(VIRTUAL_CANVAS_X * 0.91f, (VIRTUAL_CANVAS_Y * 0.30f), VIRTUAL_CANVAS_X * 0.94f, (VIRTUAL_CANVAS_Y * 0.30f) + (cSizePerBox * 4) + (cBoxSpacing * 3));

            //--Check the difference between the bar's top and the mouse current position.
            float cAmountPerMove = (1.0f / (float)(mLoadingPackList->GetListSize() - cEntriesPerPage) * cBoxDimensions.GetHeight()) * 0.5f;
            if(tMouseY - mScrollbarClickStartY < -cAmountPerMove && mLoadingOffset > 0)
            {
                mLoadingOffset --;
                mScrollbarClickStartY = mScrollbarClickStartY - cAmountPerMove;
            }
            else if(tMouseY - mScrollbarClickStartY > cAmountPerMove)
            {
                mLoadingOffset ++;
                if(mLoadingOffset > mLoadingPackList->GetListSize() - FM_FILES_PER_PAGE)
                {
                    mLoadingOffset --;
                }
                else
                {
                    mScrollbarClickStartY = mScrollbarClickStartY + cAmountPerMove;
                }
            }
        }

        //--Save the mouse position.
        mPreviousMouseX = tMouseX;
        mPreviousMouseY = tMouseY;
    }

    //--If the previous mouse Z was -1, store it immediately.
    if(mPreviousMouseZ == -1) mPreviousMouseZ = tMouseZ;

    //--[Mouse Scrollwheel]
    //--Interacts with the scrollbar.
    if(tMouseZ > mPreviousMouseZ)
    {
        mLoadingOffset --;
        if(mLoadingOffset < 0) mLoadingOffset = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    else if(tMouseZ < mPreviousMouseZ)
    {
        mLoadingOffset ++;
        if(mLoadingOffset > mLoadingPackList->GetListSize() - FM_FILES_PER_PAGE)  mLoadingOffset --;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--[Hotkeys]
    //--Save selection up.
    if(rControlManager->IsFirstPress("Up") || rControlManager->IsFirstPress("PageUp"))
    {
        //--Offset moves first.
        if(mLoadingCursor > 1)
        {
            mLoadingCursor --;
        }
        //--Move the loading offset and clamp.
        else if(mLoadingOffset > 0)
        {
            mLoadingOffset --;
            if(mLoadingOffset < 0) mLoadingOffset = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Otherwise, move the cursor onto 0: New Game.
        else
        {
            mLoadingCursor = 0;
        }
    }
    //--Save selection down.
    if(rControlManager->IsFirstPress("Down") || rControlManager->IsFirstPress("PageDn"))
    {
        //--Move the cursor and clamp.
        mLoadingCursor ++;

        //--If the loading offset pushes the cursor offscreen, cap it.
        if(mLoadingCursor > FM_FILES_PER_PAGE)
        {
            mLoadingOffset ++;
            if(mLoadingOffset > mLoadingPackList->GetListSize() - FM_FILES_PER_PAGE) mLoadingOffset --;
            if(mLoadingOffset < 0) mLoadingOffset = 0;
            mLoadingCursor = FM_FILES_PER_PAGE;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Store the mouse Z value.
    mPreviousMouseZ = tMouseZ;

    //--[SFX]
    //--If the cursor changed, play a sound effect.
    if(tOldCursor != mLoadingCursor && mLoadingCursor != -1)
    {
        //--If negative, play a sound.
        if(mLoadingCursor == 0 || mLoadingCursor < -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--If a package exists, play the sound. If not, don't.
        else
        {
            LoadingPack *rPack = (LoadingPack *)mLoadingPackList->GetElementBySlot(mLoadingCursor - 1 + mLoadingOffset);
            if(rPack) AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--[Selection via Mouse]
    //--Clicking.
    if(rControlManager->IsFirstPress("MouseLft") || rControlManager->IsFirstPress("Enter"))
    {
        //--Scroll up.
        if(mLoadingCursor == -4)
        {
            mLoadingOffset --;
            if(mLoadingOffset < 0) mLoadingOffset = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Scroll down.
        else if(mLoadingCursor == -5)
        {
            mLoadingOffset ++;
            if(mLoadingOffset > mLoadingPackList->GetListSize() - FM_FILES_PER_PAGE) mLoadingOffset --;
            if(mLoadingOffset < 0) mLoadingOffset = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--If the cursor was on -2, go back.
        else if(mLoadingCursor == -2)
        {
            DeactivateLoadingMode();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--If the cursor was on -3, go to the controls menu.
        else if(mLoadingCursor == -3)
        {
            ActivateRebindingMode();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--If the cursor was on -1, check for a scrollbar click.
        else if(mLoadingCursor == -1)
        {
            //--Check for a scrollbar click.
            float cSizePerBox = VIRTUAL_CANVAS_Y * 0.15f;
            float cBoxSpacing = VIRTUAL_CANVAS_Y * 0.01f;
            int cEntriesPerPage = 4;
            if(mLoadingPackList->GetListSize() - cEntriesPerPage > 0 && IsPointWithin(tMouseX, tMouseY, VIRTUAL_CANVAS_X * 0.91f, (VIRTUAL_CANVAS_Y * 0.30f), VIRTUAL_CANVAS_X * 0.94f, (VIRTUAL_CANVAS_Y * 0.30f) + (cSizePerBox * 4) + (cBoxSpacing * 3)))
            {
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                mIsClickingScrollbar = true;
                mScrollbarClickStartY = tMouseY;
            }
        }
        //--If the cursor was on 0, start a new game.
        else if(mLoadingCursor == 0)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Make sure Adventure Mode actually has an operable path.
            const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");

            //--Run.
            char tLaunchPath[256];
            sprintf(tLaunchPath, "%s/ZLaunch.lua", rAdventurePath);
            LuaManager::Fetch()->ExecuteLuaFile(tLaunchPath);

            //--Path to the savegames.
            char tSavePath[256];
            sprintf(tSavePath, "%s/../../Saves/NewGame.slf", rAdventurePath);

            //--Defaults for the SaveManager.
            SaveManager::Fetch()->SetSavegameName("New Game");
            SaveManager::Fetch()->SetSavegamePath(tSavePath);
        }
        //--Otherwise, load the game under the cursor.
        else
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Make sure Adventure Mode actually has an operable path.
            const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");

            //--Get the pack.
            LoadingPack *rPack = (LoadingPack *)mLoadingPackList->GetElementBySlot(mLoadingCursor - 1 + mLoadingOffset);
            if(!rPack) { return; }

            //--Pass these for the SaveManager.
            SaveManager::Fetch()->SetSavegameName(rPack->mFileName);
            SaveManager::Fetch()->SetSavegamePath(rPack->mFilePath);

            //--Store the path.
            char tPath[256];
            strcpy(tPath, rPack->mFilePath);

            //--Clear. All data is now wiped, don't use the loading pack anymore!
            DeactivateLoadingMode();

            //--Execute.
            char tLaunchPath[256];
            sprintf(tLaunchPath, "%s/ZLaunch.lua", rAdventurePath);
            LuaManager::Fetch()->ExecuteLuaFile(tLaunchPath, 1, "S", tPath);
        }
    }
    //--[Right Mouse]
    //--If over a loading pack, changes the note on that pack.
    else if(rControlManager->IsFirstPress("MouseRgt"))
    {
        //--If the cursor was 0 or lower, ignore it.
        if(mLoadingCursor <= 0)
        {
        }
        //--Otherwise, modify the note. This is done by bringing up the StringEntryForm.
        else
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

            //--Set flags.
            mIsEnteringString = true;
            mFileStringEntering = mLoadingCursor + mLoadingOffset;
            mStringEntryForm->SetCompleteFlag(false);

            //--Get the string from the file in question.
            LoadingPack *rPack = (LoadingPack *)mLoadingPackList->GetElementBySlot(mLoadingCursor - 1 + mLoadingOffset);
            if(!rPack) { return; }
            mStringEntryForm->SetString(rPack->mFileName);
        }
    }

    //--[Mouse Release]
    //--Cancel scrollbar clicks.
    if(rControlManager->IsFirstRelease("MouseLft"))
    {
        mIsClickingScrollbar = false;
    }
}

//--[Render]
void FlexMenu::RenderLoadingMode()
{
    //--[Documentation]
    //--Renders Loading Mode, assuming there is at least one file to load.
    if(!Images.mIsReady) return;

    //--[String Entry Form]
    //--When entering strings, pass the update down.
    if(mIsEnteringString)
    {
        mStringEntryForm->Render();
        return;
    }

    //--Setup.
    char tBuffer[80];

    //--Color setup.
    StarlightColor cBackingColorGrey = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.85f);
    StarlightColor cBackingColorBlue = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.4f, 0.85f);
    StarlightColor *rUseColor = &cBackingColorGrey;

    //--New game button sizing.
    TwoDimensionReal cBoxDimensions;
    float cSizePerBox = VIRTUAL_CANVAS_Y * 0.15f;
    float cBoxSpacing = VIRTUAL_CANVAS_Y * 0.01f;

    //--Constants.
    int cEntriesPerPage = FM_FILES_PER_PAGE;
    float cTextSize = 1.0f;

    //--[Header]
    //--Render some instructions.
    strcpy(tBuffer, "Select A Save File");
    float cTextWid = Images.Data.rUIFont->GetTextWidth(tBuffer) * 3.0f * 0.5f;
    float cTextHei = Images.Data.rUIFont->GetTextHeight() * 3.0f * 0.5f;
    Images.Data.rUIFont->DrawText((VIRTUAL_CANVAS_X * 0.5f) - cTextWid, (VIRTUAL_CANVAS_Y * 0.08f) - cTextHei, 0, 3.0f, tBuffer);

    //--[New Game Button]
    //--Always present, appears at the top of the screen.
    rUseColor = &cBackingColorGrey;
    if(mLoadingCursor == 0) rUseColor = &cBackingColorBlue;
    cBoxDimensions.Set(VIRTUAL_CANVAS_X * 0.10f, VIRTUAL_CANVAS_Y * 0.17f, VIRTUAL_CANVAS_X * 0.90f, VIRTUAL_CANVAS_Y * 0.27f);
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, cBoxDimensions.mLft, cBoxDimensions.mTop, cBoxDimensions.mRgt, cBoxDimensions.mBot, 0x01FF, *rUseColor);

    //--Text.
    strcpy(tBuffer, "New Game");
    cTextWid = Images.Data.rUIFont->GetTextWidth(tBuffer) * cTextSize * 0.5f;
    cTextHei = Images.Data.rUIFont->GetTextHeight() * cTextSize * 0.5f;
    Images.Data.rUIFont->DrawText(cBoxDimensions.mXCenter - cTextWid, cBoxDimensions.mYCenter - cTextHei, 0, cTextSize, tBuffer);

    //--[Scrollbar]
    //--Shows a scrollbar if there are more than cEntriesPerPage total.
    if(mLoadingPackList->GetListSize() > cEntriesPerPage)
    {
        //--Base.
        rUseColor = &cBackingColorGrey;
        cBoxDimensions.Set(VIRTUAL_CANVAS_X * 0.91f, (VIRTUAL_CANVAS_Y * 0.30f), VIRTUAL_CANVAS_X * 0.94f, (VIRTUAL_CANVAS_Y * 0.30f) + (cSizePerBox * 4) + (cBoxSpacing * 3));
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, cBoxDimensions.mLft, cBoxDimensions.mTop, cBoxDimensions.mRgt, cBoxDimensions.mBot, 0x01FF, *rUseColor);

        //--Compute percentages.
        float cScrollbarTopPct = (float)(mLoadingOffset)                   / (float)(mLoadingPackList->GetListSize());
        float cScrollbarBotPct = (float)(mLoadingOffset + cEntriesPerPage) / (float)(mLoadingPackList->GetListSize());

        //--Positions. Collapse the box in over its borders, then reposition the percentages.
        cBoxDimensions.Set(cBoxDimensions.mLft + 6.0f, cBoxDimensions.mTop + ((cBoxDimensions.GetHeight() - 12.0f) * cScrollbarTopPct) + 6.0f, cBoxDimensions.mRgt - 7.0f, cBoxDimensions.mTop + ((cBoxDimensions.GetHeight() - 12.0f) * cScrollbarBotPct) + 6.0f);

        //--Render.
        StarlightColor::SetMixer(0.0f, 1.0f, 1.0f, 1.0f);
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
            glVertex2f(cBoxDimensions.mLft, cBoxDimensions.mTop);
            glVertex2f(cBoxDimensions.mRgt, cBoxDimensions.mTop);
            glVertex2f(cBoxDimensions.mRgt, cBoxDimensions.mBot);
            glVertex2f(cBoxDimensions.mLft, cBoxDimensions.mBot);
        glEnd();
        glEnable(GL_TEXTURE_2D);
        StarlightColor::ClearMixer();

        //--Arrows to move.
        if(cScrollbarTopPct > 0.0f) Images.Data.rUpArrow->Draw(mScrollUp.mLft, mScrollUp.mTop);
        if(cScrollbarBotPct < 1.0f) Images.Data.rDnArrow->Draw(mScrollDn.mLft, mScrollDn.mTop);
    }

    //--[Loading Packs]
    //--Render the save game loading packs.
    for(int i = 0; i < cEntriesPerPage; i ++)
    {
        //--Get the loading pack in question.
        LoadingPack *rPack = (LoadingPack *)mLoadingPackList->GetElementBySlot(i + mLoadingOffset);
        if(!rPack) continue;

        //--Compute position.
        float cTop = (VIRTUAL_CANVAS_Y * 0.30f) + (cSizePerBox * i) + (cBoxSpacing * i);
        cBoxDimensions.Set(VIRTUAL_CANVAS_X * 0.10f, cTop, VIRTUAL_CANVAS_X * 0.90f, cTop + cSizePerBox);

        //--Render the border card.
        rUseColor = &cBackingColorGrey;
        if(mLoadingCursor == 1 + i) rUseColor = &cBackingColorBlue;
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, cBoxDimensions.mLft, cBoxDimensions.mTop, cBoxDimensions.mRgt, cBoxDimensions.mBot, 0x01FF, *rUseColor);

        //--Render the name of the save file. This is not its path, and it can be renamed!
        Images.Data.rUIFont->DrawTextArgs(cBoxDimensions.mLft + 8.0f, cBoxDimensions.mTop + 8.0f, 0, cTextSize, "File %s: %s", mLoadingPackList->GetNameOfElementBySlot(i + mLoadingOffset), rPack->mFileName);

        //--Timestamp.
        float tTimestampLen = Images.Data.rUIFont->GetTextWidth(rPack->mTimestamp) * cTextSize;
        Images.Data.rUIFont->DrawText(cBoxDimensions.mRgt - 8.0f - tTimestampLen, cBoxDimensions.mTop + 8.0f, 0, cTextSize, rPack->mTimestamp);

        //--Render what map the player was on.
        Images.Data.rUIFont->DrawText(cBoxDimensions.mLft + 8.0f, cBoxDimensions.mBot - 8.0f - (Images.Data.rUIFont->GetTextHeight() * cTextSize), 0, cTextSize, rPack->mMapLocation);

        //--Render the characters in it.
        float cSpriteScale = 2.0f;
        for(int p = 0; p < 4; p ++)
        {
            //--Compute positions.
            float cLft = cBoxDimensions.mLft + 32.0f + (200.0f * p);
            float cTop = cBoxDimensions.mTop + 24.0f;

            //--Render. We need to scale this up.
            if(rPack->rRenderImg[p])
            {
                glTranslatef(cLft, cTop, 0.0f);
                glScalef(cSpriteScale, cSpriteScale, 1.0f);
                rPack->rRenderImg[p]->Draw();
                glScalef(1.0f / cSpriteScale, 1.0f / cSpriteScale, 1.0f);
                glTranslatef(-cLft, -cTop, 0.0f);
            }

            //--Get the name from the buffer.
            int tCutoff = -1;
            for(int x = (int)strlen(rPack->mPartyNames[p]); x >= 0; x --)
            {
                if(rPack->mPartyNames[p][x] == '_')
                {
                    tCutoff = x;
                    break;
                }
            }

            //--Error.
            if(tCutoff == -1) continue;

            //--Name of this character.
            rPack->mPartyNames[p][tCutoff] = '\0';
            Images.Data.rUIFont->DrawText    (cLft + 56.0f, cTop + 28.0f, 0, 1.0f, rPack->mPartyNames[p]);
            Images.Data.rUIFont->DrawTextArgs(cLft + 56.0f, cTop + 48.0f, 0, 1.0f, "Lv. %i", rPack->mPartyLevels[p] + 1);
            rPack->mPartyNames[p][tCutoff] = '_';
        }
    }

    //--Instruction String.
    float cXPos = VIRTUAL_CANVAS_X * 0.20f;
    float cYPos = (VIRTUAL_CANVAS_Y * 0.30f) + (cSizePerBox * cEntriesPerPage) + (cBoxSpacing * cEntriesPerPage);
    Images.Data.rUIFont->DrawText(cXPos, cYPos, 0, 1.0f, "Right-click a file to edit its note.");

    //--[Back Button]
    rUseColor = &cBackingColorGrey;
    if(mLoadingCursor == -2) rUseColor = &cBackingColorBlue;
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mBackBtn, 0x01FF, *rUseColor);
    Images.Data.rUIFont->DrawText(mBackBtn.mLft + AM_STD_INDENT, mBackBtn.mTop + AM_STD_INDENT, 0, 1.0f, "Back");

    //--[Controls Button]
    rUseColor = &cBackingColorGrey;
    if(mLoadingCursor == -3) rUseColor = &cBackingColorBlue;
    VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mControlsBtn, 0x01FF, *rUseColor);
    Images.Data.rUIFont->DrawText(mControlsBtn.mLft + AM_STD_INDENT, mControlsBtn.mTop + AM_STD_INDENT, 0, 1.0f, "Controls");
}
