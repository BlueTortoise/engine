//--Base
#include "FieldAbility.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

//=========================================== System ==============================================
FieldAbility::FieldAbility()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVFIELDABILITY;

    //--[FieldAbility]
    //--System
    mLocalName = InitializeString("Unnamed Ability");
    mDisplayName = InitializeString("Unnamed Ability");
    mScriptPath = InitializeString("Null");

    //--Cooldown
    mCooldownCur = 0;
    mCooldownMax = 0;

    //--Display
    rBackingImg = NULL;
    rFrameImg = NULL;
    rMainImg = NULL;
    mDisplayStringsTotal = 0;
    mDisplayStrings = NULL;
}
FieldAbility::~FieldAbility()
{
    free(mLocalName);
    free(mDisplayName);
    free(mScriptPath);
    for(int i = 0; i < mDisplayStringsTotal; i ++)
    {
        free(mDisplayStrings[i]);
    }
    free(mDisplayStrings);
}

//====================================== Property Queries =========================================
int FieldAbility::GetCooldown()
{
    return mCooldownCur;
}
int FieldAbility::GetCooldownMax()
{
    if(mCooldownMax < 1) return 1;
    return mCooldownMax;
}
const char *FieldAbility::GetDisplayName()
{
    return mDisplayName;
}
int FieldAbility::GetDisplayStringsTotal()
{
    return mDisplayStringsTotal;
}
const char *FieldAbility::GetDisplayString(int pSlot)
{
    if(pSlot < 0 || pSlot >= mDisplayStringsTotal) return NULL;
    return mDisplayStrings[pSlot];
}

//========================================= Manipulators ==========================================
void FieldAbility::SetInternalName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void FieldAbility::SetDisplayName(const char *pName)
{
    ResetString(mDisplayName, pName);
}
void FieldAbility::SetScriptPath(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}
void FieldAbility::SetCooldownCur(int pTicks)
{
    mCooldownCur = pTicks;
    if(mCooldownCur < 0) mCooldownCur = 0;
}
void FieldAbility::SetCooldownMax(int pTicks)
{
    mCooldownMax = pTicks;
}
void FieldAbility::SetBackingImg(const char *pDLPath)
{
    rBackingImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void FieldAbility::SetFrameImg(const char *pDLPath)
{
    rFrameImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void FieldAbility::SetMainImg(const char *pDLPath)
{
    rMainImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void FieldAbility::AllocateDisplayStrings(int pTotal)
{
    for(int i = 0; i < mDisplayStringsTotal; i ++) free(mDisplayStrings[i]);
    free(mDisplayStrings);
    mDisplayStringsTotal = 0;
    mDisplayStrings = NULL;
    if(pTotal < 1) return;

    mDisplayStringsTotal = pTotal;
    mDisplayStrings = (char **)starmemoryalloc(sizeof(char *) * mDisplayStringsTotal);
    for(int i = 0; i < mDisplayStringsTotal; i ++) mDisplayStrings[i] = NULL;
}
void FieldAbility::SetDisplayString(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= mDisplayStringsTotal) return;
    ResetString(mDisplayStrings[pSlot], pString);
}

//========================================= Core Methods ==========================================
void FieldAbility::ActivateCooldown()
{
    mCooldownCur = mCooldownMax;
}
void FieldAbility::Execute(int pCode)
{
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)pCode);
}
void FieldAbility::ExecuteOn(int pCode, void *pPtr)
{
    LuaManager::Fetch()->PushExecPop(pPtr, mScriptPath, 1, "N", (float)pCode);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
SugarBitmap *FieldAbility::GetBackingImage()
{
    return rBackingImg;
}
SugarBitmap *FieldAbility::GetFrameImage()
{
    return rFrameImg;
}
SugarBitmap *FieldAbility::GetMainImage()
{
    return rMainImg;
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void FieldAbility::HookToLuaState(lua_State *pLuaState)
{
    /* FieldAbility_Create(sInternalName, sDLPath) (Pushes Activity Stack)
       Creates and pushes a FieldAbility to be set up by further calls. */
    lua_register(pLuaState, "FieldAbility_Create", &Hook_FieldAbility_Create);

    /* FieldAbility_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Field Ability class. */
    lua_register(pLuaState, "FieldAbility_GetProperty", &Hook_FieldAbility_GetProperty);

    /* FieldAbility_SetProperty("Display Name")
       FieldAbility_SetProperty("Script Path", sPath)
       FieldAbility_SetProperty("Backing Image", sDLPath)
       FieldAbility_SetProperty("Frame Image", sDLPath)
       FieldAbility_SetProperty("Main Image", sDLPath)
       FieldAbility_SetProperty("Allocate Display Strings", iTotal)
       FieldAbility_SetProperty("Set Display Strings", iSlot, sString)
       Sets the requested property in the Field Ability class. */
    lua_register(pLuaState, "FieldAbility_SetProperty", &Hook_FieldAbility_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_FieldAbility_Create(lua_State *L)
{
    //FieldAbility_Create(sInternalName, sDLPath) (Pushes Activity Stack)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("FieldAbility_Create");

    //--Create.
    FieldAbility *nFieldAbility = new FieldAbility();
    nFieldAbility->SetInternalName(lua_tostring(L, 1));
    rDataLibrary->rActiveObject = nFieldAbility;

    //--Store in the DataLibrary.
    rDataLibrary->RegisterPointer(lua_tostring(L, 2), nFieldAbility, &RootObject::DeleteThis);
    return 0;
}
int Hook_FieldAbility_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //FieldAbility_GetProperty("Dummy") (1 Integer)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("FieldAbility_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVFIELDABILITY)) return LuaTypeError("FieldAbility_GetProperty");
    //FieldAbility *rAbility = (FieldAbility *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("FieldAbility_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_FieldAbility_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //FieldAbility_SetProperty("Display Name", sName)
    //FieldAbility_SetProperty("Script Path", sPath)
    //FieldAbility_SetProperty("Cooldown Max", iTicks)
    //FieldAbility_SetProperty("Backing Image", sDLPath)
    //FieldAbility_SetProperty("Frame Image", sDLPath)
    //FieldAbility_SetProperty("Main Image", sDLPath)
    //FieldAbility_SetProperty("Allocate Display Strings", iTotal)
    //FieldAbility_SetProperty("Set Display Strings", iSlot, sString)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("FieldAbility_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVFIELDABILITY)) return LuaTypeError("FieldAbility_SetProperty");
    FieldAbility *rAbility = (FieldAbility *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Name that appears in the UI.
    if(!strcasecmp(rSwitchType, "Display Name") && tArgs == 2)
    {
        rAbility->SetDisplayName(lua_tostring(L, 2));
    }
    //--Script called for various activities.
    else if(!strcasecmp(rSwitchType, "Script Path") && tArgs == 2)
    {
        rAbility->SetScriptPath(lua_tostring(L, 2));
    }
    //--Sets ability cooldown when used.
    else if(!strcasecmp(rSwitchType, "Cooldown Max") && tArgs == 2)
    {
        rAbility->SetCooldownMax(lua_tointeger(L, 2));
    }
    //--Backing.
    else if(!strcasecmp(rSwitchType, "Backing Image") && tArgs == 2)
    {
        rAbility->SetBackingImg(lua_tostring(L, 2));
    }
    //--Frame.
    else if(!strcasecmp(rSwitchType, "Frame Image") && tArgs == 2)
    {
        rAbility->SetFrameImg(lua_tostring(L, 2));
    }
    //--Primary image.
    else if(!strcasecmp(rSwitchType, "Main Image") && tArgs == 2)
    {
        rAbility->SetMainImg(lua_tostring(L, 2));
    }
    //--Allocates display strings for the ability.
    else if(!strcasecmp(rSwitchType, "Allocate Display Strings") && tArgs == 2)
    {
        rAbility->AllocateDisplayStrings(lua_tointeger(L, 2));
    }
    //--Sets a given display string.
    else if(!strcasecmp(rSwitchType, "Set Display Strings") && tArgs == 3)
    {
        rAbility->SetDisplayString(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Error case.
    else
    {
        LuaPropertyError("FieldAbility_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
