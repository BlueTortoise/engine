//--Base
#include "VisualLevel.h"

//--Classes
#include "Actor.h"
#include "VisualRoom.h"

//--CoreClasses
//--Definitions
#include "Global.h"
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers
#include "CameraManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"

//--Routines related to the radar. Unlike the base game, the 3D mode allows the player to navigate according to the radar.
//  Left-clicks move to the target room (assuming it's adjacent!) while right-clicks turn to face the given room.

//--[Update]
bool VisualLevel::HandleRadarClick(int pMouseX, int pMouseY)
{
    //--[Documenation]
    //--Handles the case of the player clicking on the radar (excluding mousewheel cases). Returns true if it handled
    //  the input, false otherwise.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--[Setup]
    //--If neither left or right click occurred, then do nothing.
    bool tIsLftClick = rControlManager->IsFirstPress("MouseLft");
    bool tIsRgtClick = rControlManager->IsFirstPress("MouseRgt");

    //--Sizings. This is how big a room square is.
    float cSqW = PandemoniumRoom::cSquareOuter * 0.5f * VR_BODY_FACTOR;
    float cSqH = PandemoniumRoom::cSquareOuter * 0.5f * VR_BODY_FACTOR;

    //--Reposition the mouse if widescreen mode is active.
    if(Global::Shared()->gWindowWidth > VIRTUAL_CANVAS_X)
    {
        pMouseX = pMouseX - (DisplayManager::xHorizontalOffset * DisplayManager::xViewportScaleX) + 12.0f;
    }

    //--[Position Calculation]
    //--Falsify the mouse position using the rotation. This only occurs if the map rotates with the player's perspective.
    if(xRadarRotatesWithPlayer)
    {
        //--Get the camera position.
        float tXRot, tYRot, tZRot;
        CameraManager::FetchActiveCamera3D()->Get3DRotation(tXRot, tYRot, tZRot);

        //--Z rotation is in increments of xRadarRotateIncrement degrees.
        if(xRadarRotateIncrement != 0.0f) tZRot = round(tZRot / xRadarRotateIncrement) * xRadarRotateIncrement;

        //--Get the existing angle and distance.
        float tAngle = atan2f(pMouseY - mRadarSize.mYCenter, pMouseX - mRadarSize.mXCenter);
        float tDistance = GetPlanarDistance(pMouseX, pMouseY, mRadarSize.mXCenter, mRadarSize.mYCenter);

        //--Add the Z rotation to the angle.
        tAngle = tAngle - (tZRot * TORADIAN);

        //--Recompute the mouse positions.
        pMouseX = mRadarSize.mXCenter + (cosf(tAngle) * tDistance);
        pMouseY = mRadarSize.mYCenter + (sinf(tAngle) * tDistance);
    }

    //--Compute the point on the radar, in world coordinates, that was clicked. This depends on where the player is.
    Actor *rPlayerEntity = EntityManager::Fetch()->GetLastPlayerEntity();
    if(rPlayerEntity)
    {
        mRadarClickX = rPlayerEntity->GetWorldX() * cUseScale * VisualRoom::xDistanceScale;
        mRadarClickY = rPlayerEntity->GetWorldY() * cUseScale * VisualRoom::xDistanceScale;

        mRadarClickX = mRadarClickX + (((pMouseX - mRadarSize.mXCenter) / xMapZoom));
        mRadarClickY = mRadarClickY + (((pMouseY - mRadarSize.mYCenter) / xMapZoom));
    }

    //--[Highlighting Check]
    //--Get the height the player is on.
    int tVisibleZ = 0;
    if(rPlayerEntity) tVisibleZ = rPlayerEntity->GetWorldZ();

    //--Clamping.
    if(tVisibleZ > 100)
    {
        tVisibleZ = 1;
    }
    else if(tVisibleZ < -100)
    {
        tVisibleZ = -1;
    }
    else
    {
        tVisibleZ = 0;
    }

    //--Go through the rooms, check if the mouse is over them. If so, set that as the highlighted room.
    //  While ostensibly more than one room could be highlighted, we block that behavior. If two rooms are
    //  very close, only one will be highlighted.
    bool tHasHighlighted = false;
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        //--Must be on the same Z level as the player.
        if(mRooms[i]->GetWorldZ() != tVisibleZ) continue;

        //--Room must be a VisualRoom type.
        if(!mRooms[i]->IsOfType(POINTER_TYPE_VISUAL_ROOM)) continue;
        VisualRoom *rPtr = (VisualRoom *)mRooms[i];

        //--If we've already highlighted, skip.
        if(tHasHighlighted) { rPtr->mIsHighlighted = false; continue; }

        //--Get position.
        float tX = (rPtr->GetWorldX() * VisualRoom::xDistanceScale);
        float tY = (rPtr->GetWorldY() * VisualRoom::xDistanceScale);
        if(IsPointWithin(mRadarClickX, mRadarClickY, tX - cSqW, tY - cSqH, tX + cSqW, tY + cSqH))
        {
            tHasHighlighted = true;
            rPtr->mIsHighlighted = true;
        }
        else
        {
            rPtr->mIsHighlighted = false;
        }
    }

    //--[No Execution Check]
    //--Centering on the player. Occurs post-zoom.
    if((!tIsLftClick && !tIsRgtClick)) return false;

    //--[Left-Click]
    //--Is this a left-click the radar? Attempt to move to the room clicked on.
    if(tIsLftClick && tHasHighlighted)
    {
        //--Go through the rooms, check if the mouse is over them. If so, set that as the highlighted room.
        for(int i = 0; i < mRoomsTotal; i ++)
        {
            //--Must be on the same Z-level.
            if(mRooms[i]->GetWorldZ() != tVisibleZ) continue;

            //--Room must be a VisualRoom type.
            if(!mRooms[i]->IsOfType(POINTER_TYPE_VISUAL_ROOM)) continue;
            VisualRoom *rPtr = (VisualRoom *)mRooms[i];

            //--If this was the highlighted room, travel to it. This only occurs if the player exists and can move there.
            if(rPtr->mIsHighlighted && rPlayerEntity)
            {
                if(rPlayerEntity->CanChangeRooms(rPtr->GetName()))
                {
                    mRecheckFacing = true;
                    uint32_t tTargetNode = VisualRoom::FindFirstID(mPlayerNode, rPtr->GetAssociatedNodeID(), this);
                    StartMovementTo(tTargetNode);
                    break;
                }
                else
                {
                    rPlayerEntity->AppendNoMoveErrorToConsole(rPtr->GetName());
                }
            }
        }
        return true;
    }

    //--[Right-Click]
    //--Is this a right-click over the radar? Turn to face the room clicked on.
    if(tIsRgtClick && tHasHighlighted)
    {
        //--Go through the rooms, check if the mouse is over them. If so, set that as the highlighted room.
        for(int i = 0; i < mRoomsTotal; i ++)
        {
            //--Must be on the same Z-level.
            if(mRooms[i]->GetWorldZ() != tVisibleZ) continue;

            //--Room must be a VisualRoom type.
            if(!mRooms[i]->IsOfType(POINTER_TYPE_VISUAL_ROOM)) continue;
            VisualRoom *rPtr = (VisualRoom *)mRooms[i];

            //--If this was the highlighted room, attempt to turn to face it. Note that it doesn't matter if we
            //  can move to the node in this case.
            if(rPtr->mIsHighlighted)
            {
                mStopMovingAfterTurn = true;
                StartMovementTo(rPtr->GetAssociatedNodeID());
                break;
            }
        }

        return true;
    }

    //--[No Click]
    //--All checks failed, the player didn't click this.
    return false;
}

//--[Rendering]
void VisualLevel::RenderWorld()
{
    //--Renders the radar, which is what RenderWorld() was in the PandemoniumLevel. Basically uses some stencils to
    //  render the normal map in a corner of the screen.
    if(mRoomsTotal < 0 || !mRooms) return;

    //--Constants.
    float cIns = 4.0f;

    //--Border. Doesn't render the middle segment.
    RenderBorderCardOver(mRadarSize.mLft, mRadarSize.mTop, mRadarSize.mRgt, mRadarSize.mBot, 0x01FF);

    //--Render a quad with stencil set.
    glEnable(GL_STENCIL_TEST);
    glDisable(GL_ALPHA_TEST);
    glDisable(GL_DEPTH_TEST);
    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, 1, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    glColorMask(true, true, true, true);
    glDepthMask(false);
    glDisable(GL_TEXTURE_2D);
    glColor4f(0.0f, 0.0f, 0.0f, 0.01f);
    glBegin(GL_QUADS);
        glVertex2f(mRadarSize.mLft + cIns, mRadarSize.mTop + cIns);
        glVertex2f(mRadarSize.mRgt - cIns, mRadarSize.mTop + cIns);
        glVertex2f(mRadarSize.mRgt - cIns, mRadarSize.mBot - cIns);
        glVertex2f(mRadarSize.mLft + cIns, mRadarSize.mBot - cIns);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);

    //--Now set to render using the stencils. Render the VisualRooms in radar mode.
    glEnable(GL_STENCIL_TEST);
    glEnable(GL_ALPHA_TEST);
    glStencilFunc(GL_EQUAL, 1, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    glColorMask(true, true, true, true);
    glDepthMask(true);
    glStencilMask(0x00);

    //--Order each room to render.
    {
        //--Attempt to center things to the viewport. The viewport is in the top left, with the UI being
        //  the right and bottom side of the screen.
        float cTranslateX = (mRadarSize.mLft + mRadarSize.mRgt) / 2.0f;
        float cTranslateY = (mRadarSize.mTop + mRadarSize.mBot) / 2.0f;
        glTranslatef(cTranslateX, cTranslateY, 0.0f);

        //--Zooming. Occurs after centering!
        if(xMapZoom != 0.0f)
        {
            glLineWidth(2.0f * xMapZoom);
            glScalef(xMapZoom, xMapZoom, 1.0f);
        }

        //--Rotate around the camera.
        float tXRot, tYRot, tZRot;
        CameraManager::FetchActiveCamera3D()->Get3DRotation(tXRot, tYRot, tZRot);
        if(xRadarRotateIncrement != 0.0f && !mIsTurning && !mMoveToNodeID) tZRot = round(tZRot / xRadarRotateIncrement) * xRadarRotateIncrement;
        if(xRadarRotatesWithPlayer) glRotatef(tZRot, 0.0f, 0.0f, 1.0f);

        //--Get player information.
        Actor *rPlayerEntity = EntityManager::Fetch()->GetLastPlayerEntity();

        //--Centering on the player. Occurs post-zoom.
        float cCenterX = 0.0f;
        float cCenterY = 0.0f;
        if(rPlayerEntity)
        {
            cCenterX = rPlayerEntity->GetWorldX() * -cUseScale * VisualRoom::xDistanceScale;
            cCenterY = rPlayerEntity->GetWorldY() * -cUseScale * VisualRoom::xDistanceScale;
        }
        glTranslatef(cCenterX, cCenterY, 0.0f);

        //--Render the debug cursor.
        if(false)
        {
            glPointSize(3.0f);
            glColor3f(1.0f, 0.0f, 0.0f);
            glDisable(GL_TEXTURE_2D);
            glBegin(GL_POINTS);
                glVertex2f(mRadarClickX, mRadarClickY);
            glEnd();
            glEnable(GL_TEXTURE_2D);
            glColor3f(1.0f, 1.0f, 1.0f);
        }

        //--Get the map Z of the acting entity. If there isn't one, it defaults to zero.
        int tVisibleZ = 0;
        if(rPlayerEntity) tVisibleZ = rPlayerEntity->GetWorldZ();

        //--Clamping.
        if(tVisibleZ > 100)
        {
            tVisibleZ = 1;
        }
        else if(tVisibleZ < -100)
        {
            tVisibleZ = -1;
        }
        else
        {
            tVisibleZ = 0;
        }

        //--Render.
        for(int i = 0; i < mRoomsTotal; i ++)
        {
            if(mRooms[i]->GetWorldZ() != tVisibleZ) continue;

            //--Render.
            mRooms[i]->Render();
        }

        //--Clean.
        glTranslatef(-cCenterX, -cCenterY, 0.0f);
        if(xMapZoom != 0.0f)
        {
            glScalef(1.0f / xMapZoom, 1.0f / xMapZoom, 1.0f);
        }
        if(xRadarRotatesWithPlayer) glRotatef(-tZRot, 0.0f, 0.0f, 1.0f);
        glTranslatef(-cTranslateX, -cTranslateY, 0.0f);
    }

    //--Clean up.
    glDisable(GL_STENCIL_TEST);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_ALPHA_TEST);
}
