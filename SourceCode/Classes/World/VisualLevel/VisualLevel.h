//--[VisualLevel]
//--An advanced version of the PandemoniumLevel, using most of its base properties but changing
//  the renderer significantly.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "PandemoniumLevel.h"

//--[Local Structures]
typedef struct
{
    //--Associated Texture
    void *rCheckPtr;

    //--Texture Coordinates
    float mLft, mTop, mRgt, mBot;
}CharacterCardRemapPack;
typedef struct
{
    uint32_t mParentID;
    int mRenderSlot;
    int mRenderTicksLeft;
    SugarBitmap *rRenderImg;
}ExtraEntityRenderPack;

typedef struct
{
    Actor *rActorPtr;
    InventoryPack *rInventoryPackPtr;
    WorldContainer *rContainerPtr;
}UnifiedObjectPack;

//--[Local Definitions]
#ifndef FACING_DATA
    #define FACING_DATA
    #define FACING_NORTH 0
    #define FACING_SOUTH 1
    #define FACING_EAST 2
    #define FACING_WEST 3
    #define FACING_NORTHWEST 4
    #define FACING_SOUTHEAST 5
    #define FACING_NORTHEAST 6
    #define FACING_SOUTHWEST 7
    #define MAX_FACINGS 8
#endif

#define NAV_ALPHA_DELTA 0.07f
#define NAV_ALPHA_ALT_DELTA 0.01f
#define NAV_ALPHA_CLAMP_LOWER 0.15f
#define NAV_ALPHA_CLAMP_UPPER 0.85f
#define NAV_ALPHAS_TOTAL 4

#define PLAYER_HEIGHT 48.0f
#define PLAYER_BASE_MOVE_SPEED 7.5f

#define LOOK_INITIAL 0
#define LOOK_RESOLVE_ENDING 1
#define LOOK_PERFORM_ENDING 2

#define VL_TURN_TO_FACE_FACTOR 15.0f //Must be at least 2.0f or higher.
#define VL_SLOW_DOWN_FACTOR 30.0f
#define VL_DISTANCE_FACTOR 0.715f //Calibrated against a 90% speed reduction
#define VL_MAX_SPEED_REDUCTION 0.90f

#define VL_CONSOLE_SLIDE_TICKS 20
#define VL_ENTITY_SLIDE_TICKS 15
#define VL_ENTITY_FADE_TICKS 15
#define VL_CONSOLE_FADE_TICKS 15

#define VL_ENTITIES_PER_SCREEN 13
#define VL_ENTITY_ANGLE 20.0f

//--[Classes]
class VisualLevel : public PandemoniumLevel
{
    private:
    //--System
    bool mDisallowSaving;
    bool mMustRepositionPlayer;
    char *mReloadPath;

    //--WAD Compilation
    char *mGeometrySavePath;
    WADFile *mLocalWADFile;

    //--Overall Map
    SugarBitmap *mOvermap;

    //--Zone Data
    char *mSaveDataPath;
    ZoneEditor *mZoneEditor;

    //--Display
    bool mIsShowingMap;
    float cComparisonPoints[8];

    //--Player Stuff
    bool mStopMovingAfterTurn;
    bool mIsFreeMove;
    bool mIsPlayerOnGround;
    float mPlayerX;
    float mPlayerY;
    float mPlayerZ;
    float mPlayerMoveSpeed;
    float mPlayerZSpeed;
    uint32_t mPlayerNode;
    int mPlayerFacing;
    float mNavigationAlphas[NAV_ALPHAS_TOTAL];

    //--Entities
    int mEntitySwitchTimer;
    int mNewTurnTimer;
    int mSelected3DEntity;
    int mSelected3DEntityPrevious;
    int mStoredEntityCount;
    int mRefreshClickPoints;
    int mSlotsMoving;
    TwoDimensionReal mClickPoints[VL_ENTITIES_PER_SCREEN];
    SugarLinkedList *mExtraRenderEntitiesList;

    //--Entity Rendering
    static bool xMustBuildConstants;
    static float cxPositions[VL_ENTITIES_PER_SCREEN];
    float mInitialRotation;
    float mCurrentRotation;
    UnifiedObjectPack mUnifiedRenderListing[VL_ENTITIES_PER_SCREEN];

    //--Node Movement Turning
    bool mProceedImmediately;
    bool mIsTurning;
    bool mIsTurningRight;
    int mLookingTimer;
    float mStartLookZ;
    float mEndLookZ;

    //--Node Movement Moving
    bool mRecheckFacing;
    uint32_t mMoveToNodeID;
    int mTicksMovingSoFar;
    int mTicksSlowingDown;
    int mLookingFlag;
    void *rPreviousRoom;

    //--Teleportation
    bool mIsTeleporting;
    int mTeleportTimer;
    Point3D mTeleportStart;
    Point3D mTeleportEnd;

    //--Mouselook
    float mOldMouseX;
    float mOldMouseY;
    int mKeyboardRotationTimer;
    int mKeyboardRotationClamp;
    float mKeyboardRotationSpeed;

    //--UI Rendering
    int mItemOffset;
    int mItemsRendered;
    float mRadarClickX;
    float mRadarClickY;
    int mHightlightedRoom;
    float mPreviousScreenW;
    float mPreviousScreenH;
    int mConsoleOffsetTimer;
    int mConsoleOffsetDelta;
    float mConsoleOffsetY;
    int mPreviousZ;

    //--UI Constants
    bool mDontRepositionOnWorldRender;
    float mLastRenderScale;
    float mLastRenderX;
    float cScreenTextScale;
    static TwoDimensionReal mBorderSizes[9];
    TwoDimensionReal mRadarSize;
    TwoDimensionReal mSelfMenuBtnSize;
    TwoDimensionReal mItemScrollUpBtn;
    TwoDimensionReal mItemScrollDnBtn;
    TwoDimensionReal mSkipTurnBtn;

    //--Self-Menu Thievery
    TwoDimensionReal mSelfMenuDimensions;
    FlexMenu *mStoredSelfMenu;

    //--Character Card Positions
    TwoDimensionReal mCharacterPortraitDim;
    TwoDimensionReal mInventoryDim;
    SugarLinkedList *mPortraitRemapList;

    //--Story Display
    bool mShowStoryDisplay;
    bool mStoryConsoleWaitForKeypress;
    int mStoryFadeTimer;
    SugarBitmap *rCurrentStoryBitmap;
    SugarLinkedList *mStoryConsoleStrings;
    SugarLinkedList *mStoryConsoleFadingString;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--3D Game UI
            SugarBitmap *rBorderCard;
            SugarBitmap *rSelfMenuBtn;
            SugarBitmap *rSystemMenuBtn;
            SugarBitmap *rWeaponIcon;
            SugarBitmap *rArmorIcon;
            SugarBitmap *rSkipTurnBtn;

            //--Overlay
            SugarBitmap *rHitOverlay;
        }Data;
    }Images;

    protected:

    public:
    //--System
    VisualLevel();
    virtual ~VisualLevel();

    //--Public Variables
    static VisualLevel *xrConstructionLevel;
    static WADFile *xrActiveWADFile;
    static bool xRadarRotatesWithPlayer;
    static float xRadarRotateIncrement;
    static bool xRenderEntitiesInWorldRotation;
    static bool xUseAlternateUI;
    static StarlightColor xTextColor;
    static bool xAllowDamageOverlay;

    //--Property Queries
    virtual bool IsOfType(int pType);
    bool IsMoving();
    bool IsEntityOnExtraRenderList(uint32_t pID);
    int GetFixedTextWidth(const char *pString);

    //--Manipulators
    void SetReloadPath(const char *pPath);
    void BuildSkybox(const char *pPattern);
    void SetSavePath(const char *pPath);
    void SetGeometryPath(const char *pPath);
    void RegisterExtraEntityPack(ExtraEntityRenderPack *pPack);
    void FlagTeleportation();
    void RebuildSelfMenu();

    //--Core Methods
    void HandlePlayerReposition();
    void HandlePlayerCommandOverride();
    virtual void HandleNewTurn();

    private:
    //--Private Core Methods
    public:
    //--Console
    virtual void AppendToConsole(const char *pString, float pTextSize, StarlightColor pColor);
    virtual void RenderConsolePane();

    //--Entities Handling/Rendering
    bool ShouldEntityRender(Actor *pActor);
    int CountVisibleEntities();
    virtual void RenderEntitiesPane();
    int RenderBitmapInWorld(int pSlot, float pActorAlpha, SugarBitmap *pBitmap);
    int RenderBitmapInWorldRot(int pSlot, float pActorAlpha, SugarBitmap *pBitmap);
    void RefreshEntityArray();
    SugarLinkedList *AssembleNewEntityList(SugarLinkedList *pEntityList, SugarLinkedList *pInvPackList, SugarLinkedList *pContainerList);

    //--Hi-Def Texturing
    void ReplaceTexturesFrom(const char *pFilePath);

    //--Movement
    void HandleMovement();

    //--Node Movement
    void HandleNodeMovementUpdate();
    void StartMovementTo(uint32_t pNodeID);
    bool TurnToFace(uint32_t pNodeID, bool pToTheRight, int pPreviousFacing);
    bool AutoMoveTo(uint32_t pNodeID);

    //--Radar Handling
    bool HandleRadarClick(int pMouseX, int pMouseY);
    virtual void RenderWorld();

    //--Render Routines
    void RenderActor(Actor *pActor, int pWorldSlot);
    void RenderInventoryPack(InventoryPack *pPack, int pWorldSlot);
    void RenderContainer(WorldContainer *pContainer, int pWorldSlot);

    //--Story Display
    bool IsStoryModeActive();
    void SetStoryModeActivity(bool pFlag);
    void ActivateStoryMode();
    void DeactivateStoryMode();
    void AppendStoryString(ConsoleString *pString);
    void ClearStoryMode();
    void UpdateStoryMode();
    void RenderStoryMode();

    //--Timers
    void UpdateVisiblityTimers();

    //--UI Handling/Rendering
    void SetUIConstants();
    void RenderBar(StarlightColor pColor, float pX, float pY, float pW, float pH, float pPercentFull);
    void CreateRemapPack(const char *pDLPath, float pLft, float pTop, float fWid, float fHei);
    bool HandleUIUpdate();
    bool HandleCharacterCardClick(int pMouseX, int pMouseY);
    virtual void RenderNavigation();
    virtual void RenderCharacterPane();
    void RenderBorderCardOver(float pLft, float pTop, float pRgt, float pBot, uint16_t pFlags);
    static void RenderBorderCardOver(SugarBitmap *pBorderCard, TwoDimensionReal pDimensions, uint16_t pFlags);
    static void RenderBorderCardOver(SugarBitmap *pBorderCard, TwoDimensionReal pDimensions, uint16_t pFlags, StarlightColor pBackingColor);
    static void RenderBorderCardOver(SugarBitmap *pBorderCard, float pLft, float pTop, float pRgt, float pBot, uint16_t pFlags);
    static void RenderBorderCardOver(SugarBitmap *pBorderCard, float pLft, float pTop, float pRgt, float pBot, uint16_t pFlags, StarlightColor pBackingColor);
    static void RenderTextButton(SugarBitmap *pBorderCard, SugarFont *pRenderFont, TwoDimensionReal pDimensions, float pIndent, const char *pText);

    //--UI Alternative
    void RenderAltCharacterPane();
    virtual void ShowContextMenuAround(int pIndex);

    //--Update
    virtual void Update();
    void LoadNodesNow();
    void LoadGeometryNow();

    //--File I/O
    void ConstructWithFile(const char *pPath);
    void ConstructWithFile(const char *pPath, int pAdditionalPaths, const char **pPaths);
    void Finalize();

    //--Drawing
    virtual void Render();
    void RenderView();

    //--Pointer Routing
    SugarFont *GetFont();
    VisualRoom *GetRoomByNodeID(uint32_t pID);
    ZoneNode *GetNodeByID(uint32_t pID);
    ZoneNode *GetNodeByName(const char *pName);

    //--Static Functions
    static float ResolveLargestScale(SugarFont *pFont, float pWidth, const char *pText);
    static VisualLevel *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_VL_BeginLevelConstruction(lua_State *L);
int Hook_VL_SetProperty(lua_State *L);
int Hook_VL_AddSkybox(lua_State *L);
int Hook_VL_AddRoom(lua_State *L);
int Hook_VL_FinishConstruction(lua_State *L);
int Hook_VL_ReplaceTexturesFromFile(lua_State *L);
int Hook_VL_CreateTextureRemap(lua_State *L);
int Hook_VL_ActivateStoryDisplay(lua_State *L);
int Hook_VL_FlagTeleport(lua_State *L);
int Hook_VL_ShouldTruncateSelfMenu(lua_State *L);
int Hook_VL_RebuildSelfMenu(lua_State *L);
