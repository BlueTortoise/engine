//--Base
#include "VisualLevel.h"

//--Classes
#include "WADFile.h"

//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "SugarLumpManager.h"

//--Contains functions pertaining to using hi-def texture packs. I mean, you don't *have* to use hi-def,
//  this can also be used for creating total conversions. It just swaps out the textures in the base wads
//  with ones from .slf files.
//--Obviously, this must be done after level construction has completed, not least of which because some
//  wall polygons need the height of their original texture to figure out how tall they are.

void VisualLevel::ReplaceTexturesFrom(const char *pFilePath)
{
    //--Given a .slf file, and assuming we already have a WADFile loaded, replaces all textures in the WADFile that
    //  have the same name as a lump in the provided .slf file.
    bool tDidAnything = false;
    if(!pFilePath || !mLocalWADFile) return;

    //--Need the SLM for this.
    SugarLumpManager *rSLM = SugarLumpManager::Fetch();
    rSLM->Open(pFilePath);
    if(!rSLM->IsFileOpen()) return;

    //--SLM activates case-insensitive mode. WAD files normally are case-insensitive.
    SugarLumpManager::xIsCaseInsensitive = true;

    //--Begin replacing.
    int tTexturesTotal = mLocalWADFile->GetTexturesTotal();
    for(int i = 0; i < tTexturesTotal; i ++)
    {
        //--Get the texture. It can be a flat or a wall texture.
        WAD_Texture *rTexturePack = mLocalWADFile->GetTextureEntry(i);
        if(!rTexturePack) continue;

        //--If this the sky texture, don't bother.
        if(rTexturePack->mName[0] == '\0') continue;

        //--If the lump doesn't exist, fail.
        if(!rSLM->DoesLumpExist(rTexturePack->mName)) continue;

        //--Check if that name is in the .slf file. It's case-insensitive.
        SugarBitmap::xStaticFlags.mUplMagFilter = GL_LINEAR;
        SugarBitmap::xStaticFlags.mUplMinFilter = GL_LINEAR;
        SugarBitmap::xStaticFlags.mUplWrapS = GL_REPEAT;
        SugarBitmap::xStaticFlags.mUplWrapT = GL_REPEAT;
        SugarBitmap *nGetImage = rSLM->GetImage(rTexturePack->mName);
        if(!nGetImage) continue;

        //--Delete and replace.
        delete rTexturePack->mImage;
        rTexturePack->mImage = nGetImage;

        //--Mark that we replaced a texture.
        tDidAnything = true;
    }

    //--Close the SLF.
    rSLM->Close();
    SugarLumpManager::xIsCaseInsensitive = false;

    //--If anything changed, we need to re-resolve textures.
    if(tDidAnything)
    {
        mLocalWADFile->FloorsReResolveTextures();
        mLocalWADFile->WallsReResolveTextures();
    }
}
