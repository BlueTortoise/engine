//--Base
#include "AdventureLevel.h"

//--Classes
#include "TilemapActor.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
//--Managers
#include "LuaManager.h"

void AdventureLevel::SetTriggerScript(const char *pPath)
{
    ResetString(mTriggerScript, pPath);
}
bool AdventureLevel::CheckActorAgainstTriggers(TilemapActor *pActor, bool pOnlyEmulate)
{
    //--Checks the given Actor against triggers in the order they were created. The first trigger found which has a collision
    //  will be fired based on its collision case, and all other collisions are ignored if the firing script indicates
    //  it handled the update. Note that it is legal for more than one trigger to fire if they simply don't mark the update
    //  as complete, so triggers can safely overlap.
    //--IMPORTANT NOTE: Triggers are *not* checked when a cutscene is running, so the player actor can and will walk through
    //  trigger points and not set them off during a cutscene.
    //--Returns true if any trigger handled the update, false if none did.
    if(!pActor || !mTriggerScript) return false;

    //--Flags.
    xTriggerHandledUpdate = false;

    //--Iterate.
    TriggerPackage *rPackage = (TriggerPackage *)mTriggerList->PushIterator();
    while(rPackage)
    {
        //--Check for a full collision.
        if(pActor->GetWorldX() >= rPackage->mDimensions.mLft && pActor->GetWorldX() + TA_SIZE <= rPackage->mDimensions.mRgt &&
           pActor->GetWorldY() >= rPackage->mDimensions.mTop && pActor->GetWorldY() + TA_SIZE <= rPackage->mDimensions.mBot)
        {
            //--Emulating, just return true.
            if(pOnlyEmulate)
            {
                mTriggerList->PopIterator();
                return true;
            }

            //--Execute.
            xTriggerHandledUpdate = true;
            LuaManager::Fetch()->ExecuteLuaFile(mTriggerScript, 2, "S", rPackage->mFiringName, "N", 1.0f);
        }
        //--Check for a partial collision.
        else if(IsCollision2DReal(rPackage->mDimensions, pActor->GetWorldX(), pActor->GetWorldY(), pActor->GetWorldX() + TA_SIZE, pActor->GetWorldY() + TA_SIZE))
        {
            //--Emulating, just return true.
            if(pOnlyEmulate)
            {
                mTriggerList->PopIterator();
                return true;
            }

            //--Execute.
            xTriggerHandledUpdate = true;
            LuaManager::Fetch()->ExecuteLuaFile(mTriggerScript, 2, "S", rPackage->mFiringName, "N", 0.0f);
        }
        //--No hit.
        else
        {
        }

        //--If this flag is set, stop iterating here.
        if(xTriggerHandledUpdate)
        {
            mTriggerList->PopIterator();
            break;
        }

        //--Next package.
        rPackage = (TriggerPackage *)mTriggerList->AutoIterate();
    }

    //--Any triggers flagged for self-destruction get purged here.
    rPackage = (TriggerPackage *)mTriggerList->SetToHeadAndReturn();
    while(rPackage)
    {
        if(rPackage->mSelfDestruct) mTriggerList->RemoveRandomPointerEntry();
        rPackage = (TriggerPackage *)mTriggerList->IncrementAndGetRandomPointerEntry();
    }

    return xTriggerHandledUpdate;
}
