//--Base
#include "AdventureLevel.h"

//--Classes
#include "TileLayer.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers

//--[Rendering]
void AdventureLevel::RenderPatrolNodes()
{
    //--Setup.
    float cSize = 3.0f;
    glLineWidth(3.0f);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_LINES);
    StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);

    //--Begin rendering.
    TwoDimensionReal *rPatrolNode = (TwoDimensionReal *)mPatrolNodes->PushIterator();
    while(rPatrolNode)
    {
        //--Top.
        glVertex2f(rPatrolNode->mXCenter - cSize, rPatrolNode->mYCenter - cSize);
        glVertex2f(rPatrolNode->mXCenter + cSize, rPatrolNode->mYCenter - cSize);

        //--Right.
        glVertex2f(rPatrolNode->mXCenter + cSize, rPatrolNode->mYCenter - cSize);
        glVertex2f(rPatrolNode->mXCenter + cSize, rPatrolNode->mYCenter + cSize);

        //--Bottom.
        glVertex2f(rPatrolNode->mXCenter + cSize, rPatrolNode->mYCenter + cSize);
        glVertex2f(rPatrolNode->mXCenter - cSize, rPatrolNode->mYCenter + cSize);

        //--Left.
        glVertex2f(rPatrolNode->mXCenter - cSize, rPatrolNode->mYCenter + cSize);
        glVertex2f(rPatrolNode->mXCenter - cSize, rPatrolNode->mYCenter - cSize);

        //--Next.
        rPatrolNode = (TwoDimensionReal *)mPatrolNodes->AutoIterate();
    }

    //--Clean.
    StarlightColor::ClearMixer();
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glLineWidth(1.0f);
}
