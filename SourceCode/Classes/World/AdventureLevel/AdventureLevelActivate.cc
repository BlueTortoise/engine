//--Base
#include "AdventureLevel.h"

//--Classes
#include "AdventureLight.h"
#include "AdventureMenu.h"
#include "TileLayer.h"
#include "TilemapActor.h"
#include "RayCollisionEngine.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

bool AdventureLevel::ActivateAt(int pX, int pY)
{
    ///--[Documentation and Setup]
    //--Performs an activation, which consists of things like searching, opening doors, talking to NPCs,
    //  and sometimes teleporting or moving around. This is usually called by an Actor externally.
    //--It is legal to call an activation at negative coordinates (in theory). Activations do nothing
    //  at all if they fail.
    //--Returns true if something was activated, false otherwise.
    bool tDidAnything = false;

    //--Some objects are per-tile. Store that.
    int tTileX = pX / TileLayer::cxSizePerTile;
    int tTileY = pY / TileLayer::cxSizePerTile;

    ///--[Doors]
    //--Check doors.
    DoorPackage *rDoorPackage = (DoorPackage *)mDoorList->PushIterator();
    while(rDoorPackage)
    {
        //--Examination position.
        int tExamineX = (int)(rDoorPackage->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rDoorPackage->mY / TileLayer::cxSizePerTile);

        //--If this door is acting as a transition exit...
        if(rDoorPackage->mLevelTransitOnOpen && rDoorPackage->mLevelTransitExit && tTileY == tExamineY && ((tTileX == tExamineX) || (rDoorPackage->mIsWide && tTileX - 1 == tExamineX)))
        {
            //--If the player character is looking straight west or east, do nothing.
            TilemapActor *rPlayerActor = LocatePlayerActor();
            if(rPlayerActor && (rPlayerActor->GetFacing() == TA_DIR_EAST || rPlayerActor->GetFacing() == TA_DIR_WEST))
            {
            }
            //--Execute transition.
            else
            {
                tDidAnything = true;
                SetTransitionDestination(rDoorPackage->mLevelTransitOnOpen, rDoorPackage->mLevelTransitExit);

                //--Space Doors use a different sound.
                if(rDoorPackage->mUseSpaceSound)
                {
                    AudioManager::Fetch()->PlaySound("World|AutoDoorOpen");
                }
                //--Normal case.
                else
                {
                    AudioManager::Fetch()->PlaySound("World|FlipSwitch");
                }
            }
        }
        //--Door must not already be open, and be at the activation position.
        else if(!rDoorPackage->mIsOpened && tTileY == tExamineY && ((tTileX == tExamineX) || (tTileX - 1 == tExamineX && rDoorPackage->mIsWide)))
        {
            //--If the door is script locked, it will not open here.
            if(!rDoorPackage->mIsScriptLocked)
            {
                //--Flags.
                tDidAnything = true;
                rDoorPackage->mIsOpened = true;

                //--Remove it from the RCEs.
                int tDoorSlot = mDoorList->GetSlotOfElementByPtr(rDoorPackage);
                char tNameBuffer[256];
                for(int i = 0; i < 4; i ++)
                {
                    sprintf(tNameBuffer, "%i|%i", tDoorSlot, i);
                    for(int p = 0; p < mRayCollisionEnginesTotal; p ++)
                    {
                        if(!mRayCollisionEngines[p]) continue;
                        mRayCollisionEngines[p]->UnsetVariableLine(tNameBuffer);
                    }
                }

                //--Space Doors use a different sound.
                if(rDoorPackage->mUseSpaceSound)
                {
                    AudioManager::Fetch()->PlaySound("World|AutoDoorOpen");
                }
                //--Normal case.
                else
                {
                    AudioManager::Fetch()->PlaySound("World|FlipSwitch");
                }

            }

            //--If the door has an examination case, it gets called here. It may or may not have opened.
            //  The examinable script has to be set!
            if(rDoorPackage->mExamineScript)
            {
                tDidAnything = true;
                LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", rDoorPackage->mExamineScript);
            }
        }

        //--Next.
        rDoorPackage = (DoorPackage *)mDoorList->AutoIterate();
    }

    ///--[Chests]
    //--Check chests.
    DoorPackage *rChestPackage = (DoorPackage *)mChestList->PushIterator();
    while(rChestPackage)
    {
        //--Examination position.
        int tExamineX = (int)(rChestPackage->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rChestPackage->mY / TileLayer::cxSizePerTile);

        //--Chest must not already be open, and be at the activation position.
        if(!rChestPackage->mIsOpened && tTileY == tExamineY && tTileX == tExamineX)
        {
            //--If the chest is script locked, it will not open here.
            if(!rChestPackage->mIsScriptLocked)
            {
                //--Flags.
                tDidAnything = true;
                rChestPackage->mIsOpened = true;

                //--Create a variable in the DataLibrary. The path should already exist. This does not happen
                //  if this is a randomly-generated level.
                if(strcasecmp(mName, "RandomLevel"))
                {
                    char tBuffer[128];
                    sprintf(tBuffer, "Root/Variables/Chests/%s/%s", mName, mChestList->GetIteratorName());
                    SetMemoryData(__FILE__, __LINE__);
                    SysVar *nVariable = (SysVar *)starmemoryalloc(sizeof(SysVar));
                    nVariable->mIsSaved = true;
                    nVariable->mNumeric = 0.0f;
                    SetMemoryData(__FILE__, __LINE__);
                    nVariable->mAlpha = (char *)starmemoryalloc(sizeof(char) * (strlen("NULL") + 1));
                    strcpy(nVariable->mAlpha, "NULL");
                    DataLibrary::Fetch()->RegisterPointer(tBuffer, nVariable, &DataLibrary::DeleteSysVar);
                }

                //--SFX.
                AudioManager::Fetch()->PlaySound("World|FlipSwitch");
            }

            //--Contents. If this is set, the chest contains something and it will print a message.
            //  If the examine script is set, no message is printed.
            if(rChestPackage->mContents)
            {
                //--Special: Catalysts.
                if(!strncasecmp(rChestPackage->mContents, "CATALYST|", 9))
                {
                    //--Store chest location.
                    mLastChestX = rChestPackage->mX;
                    mLastChestY = rChestPackage->mY;

                    //--Run the Catalyst handler. This bypasses the item handler!
                    LuaManager::Fetch()->ExecuteLuaFile(xCatalystHandlerPath, 1, "S", &rChestPackage->mContents[9]);
                }
                //--Normal case:
                else
                {
                    //--Add the item to the inventory. This is done by calling the inventory script.
                    if(xItemListPath)
                    {
                        LuaManager::Fetch()->ExecuteLuaFile(xItemListPath, 1, "S", rChestPackage->mContents);
                    }

                    //--If there is no examinable script, start basic dialogue and print what we found.
                    if(!rChestPackage->mExamineScript)
                    {
                        //--Build the string all at once.
                        char tBuffer[256];
                        strcpy(tBuffer, "Received ");
                        strcat(tBuffer, rChestPackage->mContents);
                        strcat(tBuffer, ".");

                        //--Fetch.
                        WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
                        rWorldDialogue->Show();
                        rWorldDialogue->SetMajorSequence(false);
                        rWorldDialogue->AppendString(tBuffer);
                        rWorldDialogue->SetSilenceFlag(true);
                    }
                }
            }

            //--If the chest has an examination case, it gets called here. It may or may not have opened.
            //  The examinable script has to be set!
            if(rChestPackage->mExamineScript)
            {
                tDidAnything = true;
                LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", rChestPackage->mExamineScript);
            }
        }

        //--Next.
        rChestPackage = (DoorPackage *)mChestList->AutoIterate();
    }

    ///--[NPCs/Mugging]
    //--Check NPCs. If they have activation scripts, run that here.
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Check.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            if(((TilemapActor *)rCheckEntity)->HandleActivation((int)pX, (int)pY))
            {
                tDidAnything = true;
                rEntityList->PopIterator();
                break;
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    //--If anything has occured so far, stop here.
    if(tDidAnything) return tDidAnything;

    ///--[Examinable]
    //--Check examinable points. This cannot fire if the examination script was not set.
    if(mExaminableScript)
    {
        //--Iterate.
        ExaminePackage *rExaminePackage = (ExaminePackage *)mExaminablesList->PushIterator();
        while(rExaminePackage)
        {
            //--Flag.
            bool tShouldExecute = false;

            //--The activation position mut be within the examinable's hitbox. An examinable cannot be
            //  sized less than 1.
            if(pX >= rExaminePackage->mX && pY >= rExaminePackage->mY && pX <= rExaminePackage->mX + rExaminePackage->mW - 1 && pY <= rExaminePackage->mY + rExaminePackage->mH - 1)
            {
                //--If this flag is set, we can't examine the object from North of it.
                if(rExaminePackage->mFlags & EXAMINE_NOT_FROM_NORTH)
                {
                    //--Get the Actor's position.
                    TilemapActor *rPlayerActor = LocatePlayerActor();
                    if(rPlayerActor && rPlayerActor->GetWorldY() >= rExaminePackage->mY + rExaminePackage->mH)
                    {
                        tShouldExecute = true;
                    }
                }
                //--Normal case.
                else
                {
                    tShouldExecute = true;
                }
            }

            //--If this flag is set to true, execute the examination sequence.
            if(tShouldExecute)
            {
                //--Flag.
                tDidAnything = true;

                //--Normal case: Execute the exectuable.
                if(!rExaminePackage->mIsAutoTransition)
                {
                    LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", mExaminablesList->GetIteratorName());
                }
                //--Automated transition.
                else
                {
                    //--Sound.
                    if(strcasecmp(rExaminePackage->mAutoSound, "Null"))
                    {
                        AudioManager::Fetch()->PlaySound(rExaminePackage->mAutoSound);
                    }

                    //--Set up a transition.
                    SetTransitionDestination(rExaminePackage->mAutoRoomDest, "PlayerStart");
                    SetTransitionPostExec(rExaminePackage->mAutoLocationDest);
                }
            }

            //--Next.
            rExaminePackage = (ExaminePackage *)mExaminablesList->AutoIterate();
        }
    }

    //--If anything has occured so far, stop here.
    if(tDidAnything) return tDidAnything;

    ///--[Switches]
    //--Switches. These run the examinable script but also pass in the activation state of the switch.
    //  Switches can always and only be examined from the south!
    DoorPackage *rSwitchPack = (DoorPackage *)mSwitchList->PushIterator();
    while(rSwitchPack)
    {
        //--Examination position.
        int tExamineX = (int)(rSwitchPack->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rSwitchPack->mY / TileLayer::cxSizePerTile);

        //--Locate the actor for position check.
        int tActorPosY = -10000;
        TilemapActor *rPlayerActor = LocatePlayerActor();
        if(rPlayerActor) tActorPosY = rPlayerActor->GetWorldY();

        //--Must be at the activation position, and south of it.
        if(tTileX == tExamineX && tTileY == tExamineY && tActorPosY >= rSwitchPack->mY + TileLayer::cxSizePerTile)
        {
            //--Set the activity state of the switch. It can query this at any time.
            xIsSwitchUp = rSwitchPack->mIsOpened;

            //--Run the examination script.
            xExaminationDidSomething = false;
            LuaManager::Fetch()->ExecuteLuaFile(mExaminableScript, 1, "S", mSwitchList->GetIteratorName());

            //--If this flag got tripped, the switch handled the update.
            if(xExaminationDidSomething) tDidAnything = true;
        }

        //--Next.
        rSwitchPack = (DoorPackage *)mSwitchList->AutoIterate();
    }

    ///--[Save Points]
    //--Save points.
    SavePointPackage *rSavePack = (SavePointPackage *)mSavePointList->PushIterator();
    while(rSavePack)
    {
        //--Examination position.
        int tExamineX = (int)(rSavePack->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rSavePack->mY / TileLayer::cxSizePerTile);

        //--Must be at the activation position.
        if(tTileX == tExamineX && tTileY == tExamineY)
        {
            //--If the lighting shader is on, register a light at this point.
            if(IsLightingActive() && !rSavePack->mIsLit)
            {
                AdventureLight *nLight = new AdventureLight();
                nLight->SetName("SaveLight");
                nLight->SetPosition(rSavePack->mX + 8.0f, rSavePack->mY + 8.0f);
                nLight->SetRadial(32000.0f);
                RegisterLight(nLight);
            }

            //--Light it if it wasn't already.
            rSavePack->mIsLit = true;

            //--Store the current room as the last save point.
            ResetString(xLastSavePoint, mName);

            //--Activate the submenu in save mode.
            mAdventureMenu->Show();
            mAdventureMenu->SetSaveMode(true);
            mAdventureMenu->SetBenchMode(rSavePack->mIsBench);
            tDidAnything = true;
            mSavePointList->PopIterator();
            break;
        }

        //--Next.
        rSavePack = (SavePointPackage *)mSavePointList->AutoIterate();
    }

    ///--[No Hits]
    //--All fails, debug.
    if(!tDidAnything && false)
    {
        //--Show the WorldDialogue GUI and add some text.
        WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
        rWorldDialogue->Clear();
        rWorldDialogue->Show();
        rWorldDialogue->AddDialogueLockout();
        rWorldDialogue->AppendString("No problem here.");
        return true;
    }

    //--All checks failed.
    return tDidAnything;
}
