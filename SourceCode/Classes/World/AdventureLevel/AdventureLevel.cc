//--Base
#include "AdventureLevel.h"

//--Classes
#include "ActorNotice.h"
#include "AdvCombat.h"
#include "AdventureLevelGenerator.h"
#include "AdventureInventory.h"
#include "AdventureDebug.h"
#include "AdventureMenu.h"
#include "AdventureLight.h"
#include "KPopDanceGame.h"
#include "FieldAbility.h"
#include "RayCollisionEngine.h"
#include "RunningMinigame.h"
#include "TileLayer.h"
#include "TilemapActor.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "LoadInterrupt.h"
#include "SugarBitmap.h"
#include "SugarFileSystem.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#if defined _TARGET_OS_MAC_
#include "gl_ext_defs.h"
#endif
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "HitDetection.h"
#include "Global.h"
#include "GlDfn.h"
#include "OpenGLMacros.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "SugarLumpManager.h"
#include "OptionsManager.h"

//--[Local Definitions]
#define TRANSITION_HOLD_TICKS 10
#define TRANSITION_FADE_TICKS 15
#define PULSE_TICKS 90

//#define AL_DEBUG
#ifdef AL_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//=========================================== System ==============================================
AdventureLevel::AdventureLevel()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVENTURELEVEL;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System

    //--[TiledLevel]
    //--System
    //--Sizing
    //--Tileset Packs
    //--Raw Lists
    //--Collision Storage

    //--[AdventureLevel]
    //--System
    mBasePath = NULL;
    mIsRandomLevel = false;
    mCannotOpenMenuTimer = 1;
    mIsPlayerOnStaircase = false;
    mPlayerStartX = 0;
    mPlayerStartY = 0;
    mRunParallelDuringDialogue = false;
    mRunParallelDuringCutscene = false;

    //--Minigames
    mRunningMinigame = NULL;
    mKPopDanceGame = NULL;

    //--Ray Collision Engine
    mRayCollisionEnginesTotal = 0;
    mRayCollisionEngines = NULL;

    //--Per-map animations.
    mIsMajorAnimationMode = false;
    mMajorAnimationName[0] = '\0';
    mPerMapAnimationList = new SugarLinkedList(true);

    //--Debug Handling
    mDebugMenu = new AdventureDebug();

    //--Menu
    mIsRestSequence = false;
    mRestingTimer = 0;
    mAdventureMenu = new AdventureMenu();

    //--Object Information
    mChestList = new SugarLinkedList(true);
    mFakeChestList = new SugarLinkedList(true);
    mDoorList = new SugarLinkedList(true);
    mExitList = new SugarLinkedList(true);
    mInflectionList = new SugarLinkedList(true);
    mClimbableList = new SugarLinkedList(true);
    mLocationList = new SugarLinkedList(true);
    mPositionList = new SugarLinkedList(true);
    mInvisibleZoneList = new SugarLinkedList(true);

    //--Catalyst Tone
    mPlayCatalystTone = false;
    mCatalystToneCountdown = 0;

    //--Examinables
    mExaminableScript = NULL;
    mExaminablesList = new SugarLinkedList(true);

    //--Camera
    mIsCameraLocked = false;
    mCameraFollowID = 0;
    mCameraDimensions.Set(0.0f, 0.0f, VIRTUAL_CANVAS_X / mCurrentScale, VIRTUAL_CANVAS_Y / mCurrentScale);

    //--Backgrounds
    rBackgroundImg = NULL;
    mBackgroundStartX = 0.0f;
    mBackgroundStartY = 0.0f;
    mBackgroundScrollX = 0.0f;
    mBackgroundScrollY = 0.0f;

    //--Foreground Overlays
    mUnderlaysTotal = 0;
    mUnderlayPacks = NULL;
    mOverlaysTotal = 0;
    mOverlayPacks = NULL;

    //--Control Locking/Handling
    mControlLockTimer = 0;
    mControlEntityID = 0;
    mFollowEntityIDList = new SugarLinkedList(true);
    mControlLockList = new SugarLinkedList(false);
    memset(mPlayerXPositions, 0, sizeof(float) * PLAYER_MOVE_STORAGE_TOTAL);
    memset(mPlayerYPositions, 0, sizeof(float) * PLAYER_MOVE_STORAGE_TOTAL);

    //--Stamina
    mStaminaVisTimer = 0;
    mBlockRunningToRegen = false;
    mPlayerStamina = 100.0f;
    mPlayerStaminaMax = 100.0f;

    //--Transition Handling
    mIsExitStaircase = false;
    mTransitionDestinationMap = NULL;
    mTransitionDestinationExit = NULL;
    mTransitionPostScript = NULL;
    mTransitionDirection = 0;
    mTransitionPercentage = 0.0f;

    //--Transition Fading
    mTransitionHoldTimer = -1;
    mIsTransitionOut = false;
    mIsTransitionIn = true;
    mTransitionTimer = 0;

    //--Script-Controlled Fading
    mUseAlternateBlend = false;
    mIsScriptFading = false;
    mScriptFadeHolds = false;
    mScriptFadeDepthFlag = SCRIPT_FADE_UNDER_CHARACTERS;
    mScriptFadeTimer = 0;
    mScriptFadeTimerMax = 1;
    mStartFadeColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    mEndFadeColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);

    //--Zones and Triggers
    mTriggerScript = NULL;
    mTriggerList = new SugarLinkedList(true);

    //--Enemy Handling
    mHideAllViewcones = false;
    mRetreatTimer = 0;
    mEnemySpawnList = new SugarLinkedList(true);

    //--Switch Handling
    mSwitchList = new SugarLinkedList(true);

    //--Save Points
    mSavePointList = new SugarLinkedList(true);

    //--Reinforcement Pulsing
    mIsPulseMode = false;
    mPulseTimer = 0;

    //--Patrol Nodes
    mPatrolNodes = new SugarLinkedList(true);

    //--Camera Handling
    mIsPositiveCameraMode = false;
    mFirstLock = 0;
    mCameraZoneList = new SugarLinkedList(true);

    //--Screen Shaking and Other Effects
    mScreenShakeTimer = 0;
    mShakePeriodicityRoot = 0;
    mShakePeriodicityScatter = 0;
    mShakeSoundList = new SugarLinkedList(true);
    mRockFallingFrequency = 0;
    mFallingRockList = new SugarLinkedList(true);
    mrFallingRockImageList = new SugarLinkedList(false);

    //--Lights
    mAreLightsActive = false;
    mLightsList = new SugarLinkedList(true);
    mAmbientLight = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    mReuploadStandardLightData = false;
    mReuploadAllLightData = false;
    mLastUploadScaleX = 0.0f;
    mLastUploadScaleY = 0.0f;
    mLastUploadViewportW = 0.0f;
    mLastUploadViewportH = 0.0f;

    //--Underwater
    mIsUnderwaterMode = false;
    mUnderwaterFramebuffer = 0;
    mUnderwaterTexture = 0;
    mUnderwaterDepth = 0;

    //--Player Light
    mPlayerHasLight = false;
    mPlayerLightIsActive = false;
    mPlayerLightDoesNotDrain = false;
    mPlayerLightPower = 0;
    mPlayerLightPowerMax = 3600;
    mPlayerLightObject = NULL;

    //--Objectives Tracker
    mObjectivesList = new SugarLinkedList(true);

    //--Dislocated Render Listing
    mDislocatedRenderList = new SugarLinkedList(true);

    //--Notices System
    mNotices = new SugarLinkedList(true);

    //--Field Abilities
    mFieldAbilitiesInCancelMode = false;
    rExecutingPackage = NULL;
    mFieldAbilityString = new StarlightString();
    mExecutingFieldAbilities = new SugarLinkedList(true);
    mFieldAbilityObjectList = new SugarLinkedList(true);

    //--Images
    memset(&Images, 0, sizeof(Images));

    //--[Public Variables]
    //--Used for chest opening sequences.
    mLastChestX = 0.0f;
    mLastChestY = 0.0f;

    //--Always re-flip this when loading a new level.
    xEntitiesDrainStamina = false;

    //--[Image Resolve]
    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Utility images.
    Images.Data.rViewconePixel = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/System/System/WhitePixel");
    Images.Data.rUIFont = rDataLibrary->GetFont("Adventure Level UI");

    //--Ladder/Rope.
    Images.Data.rLadderTop  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/LadderTop");
    Images.Data.rLadderMid  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/LadderMid");
    Images.Data.rLadderBot  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/LadderBot");
    Images.Data.rRopeAnchor = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/RopeAnchor");
    Images.Data.rRopeTop    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/RopeTop");
    Images.Data.rRopeMid    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/RopeMid");
    Images.Data.rRopeBot    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/RopeBot");

    //--Stamina Bar
    Images.Data.rStaminaBarOver   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Stamina/BarOver");
    Images.Data.rStaminaBarMiddle = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Stamina/BarMiddle");
    Images.Data.rStaminaBarUnder  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Stamina/BarUnder");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--[String Setup]
    mFieldAbilityString->SetString("[IMG0]Field Abilities");
    mFieldAbilityString->AllocateImages(1);
    mFieldAbilityString->SetImageP(0, -3.0f, ControlManager::Fetch()->ResolveControlImage("OpenFieldAbilityMenu"));
    mFieldAbilityString->CrossreferenceImages();

    //--[White Pixel Handler]
    //--Changes images to white pixels if they did not load. This means that it's not necessary to load these sprites in
    //  order for the level to function. Used for Text Adventure cases, where the room is cross-booting to a TextLevel.
    SugarBitmap *rWhitePixel = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/System/System/WhitePixel");
    if(!Images.Data.rLadderTop)        Images.Data.rLadderTop        = rWhitePixel;
    if(!Images.Data.rLadderMid)        Images.Data.rLadderMid        = rWhitePixel;
    if(!Images.Data.rLadderBot)        Images.Data.rLadderBot        = rWhitePixel;
    if(!Images.Data.rRopeAnchor)       Images.Data.rRopeAnchor       = rWhitePixel;
    if(!Images.Data.rRopeTop)          Images.Data.rRopeTop          = rWhitePixel;
    if(!Images.Data.rRopeMid)          Images.Data.rRopeMid          = rWhitePixel;
    if(!Images.Data.rRopeBot)          Images.Data.rRopeBot          = rWhitePixel;
    if(!Images.Data.rStaminaBarOver)   Images.Data.rStaminaBarOver   = rWhitePixel;
    if(!Images.Data.rStaminaBarMiddle) Images.Data.rStaminaBarMiddle = rWhitePixel;
    if(!Images.Data.rStaminaBarUnder)  Images.Data.rStaminaBarUnder  = rWhitePixel;
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--[Blender Listing]
    if(!xHasBuiltListing)
    {
        xHasBuiltListing = true;
        SetMemoryData(__FILE__, __LINE__);
        xListing = (char **)starmemoryalloc(sizeof(char *) * 11);
        xListing[0] = InitializeString("GL_ZERO");
        xListing[1] = InitializeString("GL_ONE");
        xListing[2] = InitializeString("GL_SRC_COLOR");
        xListing[3] = InitializeString("GL_ONE_MINUS_SRC_COLOR");
        xListing[4] = InitializeString("GL_SRC_ALPHA");
        xListing[5] = InitializeString("GL_ONE_MINUS_SRC_ALPHA");
        xListing[6] = InitializeString("GL_DST_ALPHA");
        xListing[7] = InitializeString("GL_ONE_MINUS_DST_ALPHA");
        xListing[8] = InitializeString("GL_DST_COLOR");
        xListing[9] = InitializeString("GL_ONE_MINUS_DST_COLOR");
        xListing[10] = InitializeString("GL_SRC_ALPHA_SATURATE");

        xIntListing[0] = GL_ZERO;
        xIntListing[1] = GL_ONE;
        xIntListing[2] = GL_SRC_COLOR;
        xIntListing[3] = GL_ONE_MINUS_SRC_COLOR;
        xIntListing[4] = GL_SRC_ALPHA;
        xIntListing[5] = GL_ONE_MINUS_SRC_ALPHA;
        xIntListing[6] = GL_DST_ALPHA;
        xIntListing[7] = GL_ONE_MINUS_DST_ALPHA;
        xIntListing[8] = GL_DST_COLOR;
        xIntListing[9] = GL_ONE_MINUS_DST_COLOR;
        xIntListing[10] = GL_SRC_ALPHA_SATURATE;
    }

    //--[Framebuffer Stuff]
    //--Builds and sets up framebuffers for underwater rendering. Does nothing if the functions didn't
    //  resolve due to an outdated graphics card.
    sglGenFramebuffers(1, &mUnderwaterFramebuffer);
    glGenTextures(1, &mUnderwaterTexture);
    glGenTextures(1, &mUnderwaterDepth);

    //--Make the texture the same size as the screen.
    glBindTexture(GL_TEXTURE_2D, mUnderwaterTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    //--Make the texture the same size as the screen.
    glBindTexture(GL_TEXTURE_2D, mUnderwaterDepth);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_ALWAYS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);

    //--Set the texture as the target of the framebuffer.
    sglBindFramebuffer(GL_FRAMEBUFFER, mUnderwaterFramebuffer);
    sglFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, mUnderwaterTexture, 0);
    sglFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, mUnderwaterDepth, 0);

    //--Clean up.
    sglBindFramebuffer(GL_FRAMEBUFFER, 0);
}
AdventureLevel::~AdventureLevel()
{
    free(mBasePath);
    delete mRunningMinigame;
    delete mKPopDanceGame;
    delete mPerMapAnimationList;
    delete mAdventureMenu;
    delete mChestList;
    delete mFakeChestList;
    delete mDoorList;
    delete mExitList;
    delete mInflectionList;
    delete mClimbableList;
    delete mLocationList;
    delete mPositionList;
    delete mInvisibleZoneList;
    free(mExaminableScript);
    free(mUnderlayPacks);
    free(mOverlayPacks);
    delete mExaminablesList;
    delete mFollowEntityIDList;
    delete mControlLockList;
    free(mTransitionDestinationMap);
    free(mTransitionDestinationExit);
    free(mTransitionPostScript);
    free(mTriggerScript);
    delete mTriggerList;
    delete mSwitchList;
    delete mSavePointList;
    delete mPatrolNodes;
    delete mCameraZoneList;
    delete mLightsList;
    delete mPlayerLightObject;
    delete mObjectivesList;
    delete mDislocatedRenderList;
    delete mShakeSoundList;
    delete mFallingRockList;
    delete mrFallingRockImageList;
    delete mNotices;
    delete mExecutingFieldAbilities;
    delete mFieldAbilityObjectList;

    //--Collision.
    for(int i = 0; i < mRayCollisionEnginesTotal; i ++)
    {
        delete mRayCollisionEngines[i];
    }
    free(mRayCollisionEngines);
}

//--[Private Statics]
//--Light boost value, used to help ambient lighting on darker monitors.
float AdventureLevel::xLightBoost = 0.0f;

//--[Public Statics]
//--Global location of the active Adventure Mode scenario.
char *AdventureLevel::xRootPath = NULL;

//--Global location of the script which holds all the item information in the game.
char *AdventureLevel::xItemListPath = NULL;

//--Global location of the script which remaps items to images for some UI parts.
char *AdventureLevel::xItemImageListPath = NULL;

//--Global location of the script which handles opening a Catalyst chest.
char *AdventureLevel::xCatalystHandlerPath = NULL;

//--Flag used by TriggerPacks when updating. The update stops if one reports that it handled the update.
bool AdventureLevel::xTriggerHandledUpdate = false;

//--When a switch is examined, this flag can be queried by the scripts to quickly check if that switch
//  was up or down. Switches can also be manually checked through long-form function calls.
bool AdventureLevel::xIsSwitchUp = false;

//--Switches may or may not handle the update. If a switch did handle the update, this flag should get
//  tripped by the script.
bool AdventureLevel::xExaminationDidSomething = false;

//--Last location the player rested at a fire to save their game. Save points trigger when the menu opens,
//  not when the player actually hits save.
char *AdventureLevel::xLastSavePoint = NULL;

//--The name of the currently playing music. The static function SetMusic() will handle blending.
char *AdventureLevel::xLevelMusic = NULL;

//--Enemies ignore the player when they are of the same form. This is done with the function PulseIgnore().
//  We store the last string this function was called with since enemies can respawn or the room may change
//  and the pulse needs to re-occur.
char *AdventureLevel::xLastIgnorePulse = NULL;

//--Flag used by enemies to indicate how far they are from the player. Only the closest one is stored.
float AdventureLevel::xClosestEnemy = 10000.0f;

//--Used for counting how many catalysts are in the game, total. Debug only.
int AdventureLevel::xCountCatalyst[CATALYST_TOTAL];

//--Remappings. When the program goes to open a level, the remapping list is checked. Level names are simplified
//  so that the map editor is easier to use. The remapping indicates which subdirectory the map is actually in.
//--For example, "TrapBasementA" remaps to "TrapBasement/TrapBasementA/".
//--If a remapping is not found for a given name, the base Maps/ directory is used.
int AdventureLevel::xRemappingsTotal = 0;
char **AdventureLevel::xRemappingsCheck = NULL;
char **AdventureLevel::xRemappingsResult = NULL;

//--This flag gets flipped off every tick. Any enemy that is actively seeking the player flips it back on. When
//  it's on, the player's stamina drains if they run, otherwise they can run forever. QoL feature.
bool AdventureLevel::xEntitiesDrainStamina = false;

//--This flag gets flipped off every tick. If the player is in any invisible zone that is active, enemies will
//  act like they can't see the player for that tick.
bool AdventureLevel::xIsPlayerInInvisZone = false;

//--[Private Statics]
//--List of all enemies the player has killed. Gets cleared when they visit a save point and rest. Enemies
//  with their unique name on this list do not spawn.
SugarLinkedList *AdventureLevel::xDestroyedEnemiesList = new SugarLinkedList(false);
SugarLinkedList *AdventureLevel::xMuggedEnemiesList = new SugarLinkedList(false);
bool AdventureLevel::xHasBuiltListing = false;
char **AdventureLevel::xListing = NULL;
int AdventureLevel::xIntListing[11];

//====================================== Property Queries =========================================
bool AdventureLevel::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_ADVENTURELEVEL) return true;
    if(pType == POINTER_TYPE_TILEDLEVEL) return true;
    if(pType == POINTER_TYPE_ROOTLEVEL) return true;
    return false;
}
bool AdventureLevel::IsWorldStopped()
{
    //--Returns true if something is holding up the world update. Enemies use this to see if they should
    //  wander/chase or should be stopped. It's static for simplicity, as enemies use it as a subroutine:
    //  if the AdventureLevel does not exist, the world can't be stopped. Obviously.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Check sequence.
    if(rActiveLevel->mDebugMenu->IsVisible()) return true;
    if(rActiveLevel->mIsRestSequence) return true;
    if(rActiveLevel->mAdventureMenu->NeedsToBlockControls()) return true;
    if(WorldDialogue::Fetch()->IsVisible()) return true;
    if(CutsceneManager::Fetch()->HasAnyEvents()) return true;
    return false;
}
bool AdventureLevel::IsWorldStoppedFromCutsceneOnly()
{
    //--If the world is currently stopped, this returns whether or not it's *only* cutscenes stopping it.
    //  Some enemies animate during cutscenes for dramatic effect.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Check sequence.
    if(rActiveLevel->mDebugMenu->IsVisible()) return false;
    if(rActiveLevel->mIsRestSequence) return false;
    if(rActiveLevel->mAdventureMenu->NeedsToBlockControls()) return false;
    if(WorldDialogue::Fetch()->IsVisible()) return false;
    return true;
}
bool AdventureLevel::IsLevelTransitioning()
{
    return (mIsTransitionIn || mIsTransitionOut);
}
bool AdventureLevel::AreControlsLocked()
{
    return (mControlLockList->GetListSize() > 0);
}
bool AdventureLevel::IsPulsing()
{
    return mIsPulseMode;
}
bool AdventureLevel::GetClipAt(float pX, float pY, float pZ, int pLayer)
{
    //--Entity check.
    if(GetEntityClipAt(pX, pY, pZ)) return true;

    //--Layer range check.
    if(pLayer < 0 || pLayer >= mCollisionLayersTotal) return true;

    //--Compute the tile position to check.
    int tTileX = pX / TileLayer::cxSizePerTile;
    int tTileY = pY / TileLayer::cxSizePerTile;

    //--Edge check.
    if(tTileX < 0.0f || tTileY < 0.0f || tTileX >= mCollisionLayers[pLayer].mCollisionSizeX || tTileY >= mCollisionLayers[pLayer].mCollisionSizeY) return true;

    //--Collision hull.
    uint8_t tClipVal = mCollisionLayers[pLayer].mCollisionData[tTileX][tTileY];
    if(tClipVal == CLIP_NONE) return false;
    if(tClipVal == CLIP_ALL)  return true;

    //--Edge cases need a bit more precision. Run this subroutine.
    int tInTileX = (int)fmodf(pX, TileLayer::cxSizePerTile);
    int tInTileY = (int)fmodf(pY, TileLayer::cxSizePerTile);
    return IsClippedWithin(tClipVal, tInTileX, tInTileY);
}
bool AdventureLevel::GetClipAt(float pX, float pY, float pZ)
{
    return GetClipAt(pX, pY, pZ, 0);
}
bool AdventureLevel::GetEntityClipAt(float pX, float pY, float pZ)
{
    //--Setup.
    int tTileX = pX / TileLayer::cxSizePerTile;
    int tTileY = pY / TileLayer::cxSizePerTile;

    //--Returns the entity clip information, which is the same for both collision hulls.
    DoorPackage *rDoorPackage = (DoorPackage *)mDoorList->PushIterator();
    while(rDoorPackage)
    {
        //--If the door is open, ignore it.
        if(rDoorPackage->mIsOpened)
        {

        }
        //--Door is closed and a wide door. Can be off by 1 X unit to the right.
        else if(rDoorPackage->mIsWide && (int)(rDoorPackage->mY / TileLayer::cxSizePerTile) == tTileY)
        {
            if((int)(rDoorPackage->mX / TileLayer::cxSizePerTile) == tTileX || (int)(rDoorPackage->mX / TileLayer::cxSizePerTile) + 1 == tTileX)
            {
                mDoorList->PopIterator();
                return true;
            }
        }
        //--Door is closed, narrow, must match the X/Y.
        else if((int)(rDoorPackage->mX / TileLayer::cxSizePerTile) == tTileX && (int)(rDoorPackage->mY / TileLayer::cxSizePerTile) == tTileY)
        {
            mDoorList->PopIterator();
            return true;
        }

        //--Next.
        rDoorPackage = (DoorPackage *)mDoorList->AutoIterate();
    }

    //--Chest check. These are always clipped.
    DoorPackage *rChestPackage = (DoorPackage *)mChestList->PushIterator();
    while(rChestPackage)
    {
        //--Match.
        if((int)(rChestPackage->mX / TileLayer::cxSizePerTile) == tTileX && (int)(rChestPackage->mY / TileLayer::cxSizePerTile) == tTileY)
        {
            mChestList->PopIterator();
            return true;
        }

        //--Next.
        rChestPackage = (DoorPackage *)mChestList->AutoIterate();
    }

    //--Fake chest check. These are always clipped.
    rChestPackage = (DoorPackage *)mFakeChestList->PushIterator();
    while(rChestPackage)
    {
        //--Match.
        if((int)(rChestPackage->mX / TileLayer::cxSizePerTile) == tTileX && (int)(rChestPackage->mY / TileLayer::cxSizePerTile) == tTileY)
        {
            mFakeChestList->PopIterator();
            return true;
        }

        //--Next.
        rChestPackage = (DoorPackage *)mFakeChestList->AutoIterate();
    }

    //--NPC check. All NPCs except ones controlled by the player are clipped by default, and can turn
    //  their collision checker off. They can also reply to multiple positions.
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Check.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            if(((TilemapActor *)rCheckEntity)->IsPositionClipped(pX, pY))
            {
                rEntityList->PopIterator();
                return true;
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    //--Save points.
    SavePointPackage *rSavePackage = (SavePointPackage *)mSavePointList->PushIterator();
    while(rSavePackage)
    {
        //--Position match.
        if(rSavePackage->mX / TileLayer::cxSizePerTile == tTileX && rSavePackage->mY / TileLayer::cxSizePerTile == tTileY)
        {
            mSavePointList->PopIterator();
            return true;
        }

        //--Next.
        rSavePackage = (SavePointPackage *)mSavePointList->AutoIterate();
    }

    //--Climbables. These are only going to stop movement if they are not activated.
    ClimbablePackage *rClimbPackage = (ClimbablePackage *)mClimbableList->PushIterator();
    while(rClimbPackage)
    {
        //--Position match.
        if(!rClimbPackage->mIsActivated && rClimbPackage->mX / TileLayer::cxSizePerTile == tTileX && rClimbPackage->mY / TileLayer::cxSizePerTile == tTileY)
        {
            mClimbableList->PopIterator();
            return true;
        }

        //--Next.
        rClimbPackage = (ClimbablePackage *)mClimbableList->AutoIterate();
    }

    //--No entity hits.
    return false;
}
bool AdventureLevel::GetWanderClipAt(float pX, float pY)
{
    //--Clip routine used by wandering NPCs. They cannot wander into the player, and they can't wander over exits because they
    //  might block the player from using those exits. Enemies are not constrained in this way!

    //--Check the player actor, if it exists.
    RootEntity *rCheckActor = EntityManager::Fetch()->GetEntityI(mControlEntityID);
    if(rCheckActor && rCheckActor->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        TilemapActor *rPlayerActor = (TilemapActor *)rCheckActor;
        if(rPlayerActor->IsPositionClippedOverride(pX, pY)) return true;
    }

    //--Can't touch exits.
    ExitPackage *rExitPackage = (ExitPackage *)mExitList->PushIterator();
    while(rExitPackage)
    {
        //--Range check.
        if(IsPointWithin(pX, pY, rExitPackage->mX, rExitPackage->mY, rExitPackage->mX + rExitPackage->mW, rExitPackage->mY + rExitPackage->mH))
        {
            mExitList->PopIterator();
            return true;
        }

        rExitPackage = (ExitPackage *)mExitList->AutoIterate();
    }

    //--All checks passed, no collision.
    return false;
}
bool AdventureLevel::IsClippedWithin(const uint8_t &sClipVal, const int &sInTileX, const int &sInTileY)
{
    //--Inlined function meant to check all the myriad sub-cases of collisions. This checks instances like
    //  triangles, half-clips, curves, and the like.
    const int cHalfSize = TileLayer::cxSizePerTile * 0.5f;

    //--Top-Left edge case.
    if(sClipVal == CLIP_TL)
    {
        return (sInTileX < cHalfSize || sInTileY < cHalfSize);
    }
    //--Top edge case.
    if(sClipVal == CLIP_TOP)
    {
        return (sInTileY < cHalfSize);
    }
    //--Top-Right edge case.
    if(sClipVal == CLIP_TR)
    {
        return (sInTileX >= cHalfSize || sInTileY < cHalfSize);
    }
    //--Diagonal - Top Left - Empty. Identical to the bottom-right version!
    if(sClipVal == CLIP_DTLE || sClipVal == CLIP_DBRE)
    {
        return (sInTileX + sInTileY >= TileLayer::cxSizePerTile - 1 && sInTileX + sInTileY <= TileLayer::cxSizePerTile + 1);
    }
    //--Diagonal - Top Right - Empty. Identical to the bottom-left version!
    if(sClipVal == CLIP_DTRE || sClipVal == CLIP_DBLE)
    {
        return (abs(sInTileX - sInTileY) < 2);
    }
    //--Diagonal - Top Left - Full.
    if(sClipVal == CLIP_DTLF)
    {
        return (sInTileX + sInTileY >= TileLayer::cxSizePerTile);
    }
    //--Diagonal - Top Right - Full.
    if(sClipVal == CLIP_DTRF)
    {
        return (sInTileY >= sInTileX);
    }
    //--Left edge.
    if(sClipVal == CLIP_LFT)
    {
        return (sInTileX < cHalfSize);
    }
    //--Vertical, narrow.
    if(sClipVal == CLIP_NARROWV)
    {
        return (sInTileX < 3 || sInTileX > 13);
    }
    //--Right edge.
    if(sClipVal == CLIP_RGT)
    {
        return (sInTileX >= cHalfSize);
    }
    //--Diagonal - Bottom Left - Full.
    if(sClipVal == CLIP_DBLF)
    {
        return (sInTileY < sInTileX);
    }
    //--Diagonal - Bottom Right - Full.
    if(sClipVal == CLIP_DBRF)
    {
        return (sInTileX + sInTileY < TileLayer::cxSizePerTile);
    }
    //--Bottom left edge.
    if(sClipVal == CLIP_BL)
    {
        return (sInTileX < cHalfSize || sInTileY >= cHalfSize);
    }
    //--Bottom edge.
    if(sClipVal == CLIP_BOT)
    {
        return (sInTileY >= cHalfSize);
    }
    //--Bottom right edge.
    if(sClipVal == CLIP_BR)
    {
        return (sInTileX >= cHalfSize || sInTileY >= cHalfSize);
    }
    //--Left edge, very thing.
    if(sClipVal == CLIP_THIN_LF)
    {
        return (sInTileX < 3);
    }
    //--Right edge.
    if(sClipVal == CLIP_THIN_RT)
    {
        return (sInTileX >= TileLayer::cxSizePerTile - 3);
    }
    //--Quarter, Top Left.
    if(sClipVal == CLIP_QTL)
    {
        return (sInTileX < cHalfSize && sInTileY < cHalfSize);
    }
    //--Quarter, Top Right.
    if(sClipVal == CLIP_QTR)
    {
        return (sInTileX >= cHalfSize && sInTileY < cHalfSize);
    }
    //--Quarter, Bottom Left.
    if(sClipVal == CLIP_QBL)
    {
        return (sInTileX < cHalfSize && sInTileY >= cHalfSize);
    }
    //--Quarter, Bottom Right.
    if(sClipVal == CLIP_QBR)
    {
        return (sInTileX >= cHalfSize && sInTileY >= cHalfSize);
    }

    //--All checks failed. The default return is true.
    return true;
}
float AdventureLevel::GetPlayerStartX()
{
    return mPlayerStartX;
}
float AdventureLevel::GetPlayerStartY()
{
    return mPlayerStartY;
}
float AdventureLevel::GetCameraLft()
{
    return mCameraDimensions.mLft;
}
float AdventureLevel::GetCameraTop()
{
    return mCameraDimensions.mTop;
}
float AdventureLevel::GetDefaultCameraScale()
{
    return TL_DEFAULT_CAMERA_SCALE;
}
float AdventureLevel::GetCameraScale()
{
    return mCurrentScale;
}
bool AdventureLevel::IsCharacterInWorldParty(const char *pName)
{
    //--Error check:
    if(!pName) return false;

    //--Check the player-actor.
    RootEntity *rActor = EntityManager::Fetch()->GetEntityI(mControlEntityID);
    if(rActor && rActor->IsOfType(POINTER_TYPE_TILEMAPACTOR) && !strcasecmp(rActor->GetName(), pName))
    {
        return true;
    }

    //--Check the followers.
    uint32_t *rEntityIDPtr = (uint32_t *)mFollowEntityIDList->PushIterator();
    while(rEntityIDPtr)
    {
        //--Get the entity and verify it exists.
        RootEntity *rFollower = EntityManager::Fetch()->GetEntityI(*rEntityIDPtr);
        if(rFollower && rFollower->IsOfType(POINTER_TYPE_TILEMAPACTOR) && !strcasecmp(rFollower->GetName(), pName))
        {
            mFollowEntityIDList->PopIterator();
            return true;
        }

        //--Next.
        rEntityIDPtr = (uint32_t *)mFollowEntityIDList->AutoIterate();
    }

    //--All checks failed, entity is not in the party.
    return false;
}
TwoDimensionReal AdventureLevel::GetPatrolNode(const char *pName)
{
    //--Dummy.
    TwoDimensionReal xDummy;
    if(!pName) return xDummy;

    //--If the patrol node exists, return it.
    TwoDimensionReal *rObject = (TwoDimensionReal *)mPatrolNodes->GetElementByName(pName);
    if(rObject) return (*rObject);

    //--Not found, return the dummy.
    return xDummy;
}
void AdventureLevel::GetLocationByName(const char *pName, float &sX, float &sY)
{
    TwoDimensionReal *rLocation = (TwoDimensionReal *)mLocationList->GetElementByName(pName);
    if(rLocation)
    {
        sX = rLocation->mLft;
        sY = rLocation->mTop;
    }
}
bool AdventureLevel::DoesPlayerHaveLightSource()
{
    return (mPlayerHasLight && mPlayerLightIsActive);
}
float AdventureLevel::GetLightBoost()
{
    return xLightBoost;
}
void AdventureLevel::GetLineCollision(int pCollisionLayer, float pXA, float pYA, float pXB, float pYB, float &sReturnX, float &sReturnY)
{
    sReturnX = pXB;
    sReturnY = pYB;
    if(pCollisionLayer < 0 || pCollisionLayer >= mRayCollisionEnginesTotal) return;

    mRayCollisionEngines[pCollisionLayer]->GetShortestIntercept(pXA, pYA, pXB, pYB, sReturnX, sReturnY);
}
bool AdventureLevel::IsDoorOpen(const char *pName)
{
    DoorPackage *rDoorPackage = (DoorPackage *)mDoorList->GetElementByName(pName);
    if(!rDoorPackage) return false;

    return rDoorPackage->mIsOpened;
}
bool AdventureLevel::DoesExitExist(const char *pName)
{
    return (mExitList->GetElementByName(pName) != NULL);
}
void AdventureLevel::GetExaminablePos(const char *pName, int &sLft, int &sTop, int &sRgt, int &sBot)
{
    //--Returns the position of the named examinable object, or sets everything to -1 if not found.
    //  These are returned in pixel world-units.
    sLft = -1;
    sTop = -1;
    sRgt = -1;
    sBot = -1;
    if(!pName) return;

    //--Find, set.
    ExaminePackage *rExaminePackage = (ExaminePackage *)mExaminablesList->GetElementByName(pName);
    if(!rExaminePackage) return;
    sLft = rExaminePackage->mX;
    sTop = rExaminePackage->mY;
    sRgt = rExaminePackage->mX + rExaminePackage->mW;
    sBot = rExaminePackage->mY + rExaminePackage->mH;
}
bool AdventureLevel::DoesNoticeExist(void *pPtr)
{
    return mNotices->IsElementOnList(pPtr);
}
void AdventureLevel::BuildObjectListAt(float pWorldX, float pWorldY)
{
    //--Builds a list of all objects intersecting the given point. This is used so Field Abilities
    //  can "hit" a wide range of objects.
    mFieldAbilityObjectList->ClearList();
    int tTileIntX = (int)pWorldX / TileLayer::cxSizePerTile;
    int tTileIntY = (int)pWorldY / TileLayer::cxSizePerTile;
    TwoDimensionReal tTempDim;

    //--Chest.
    DoorPackage *rChestPackage = (DoorPackage *)mChestList->PushIterator();
    while(rChestPackage)
    {
        //--Examination position.
        int tExamineX = (int)(rChestPackage->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rChestPackage->mY / TileLayer::cxSizePerTile);

        //--Hit.
        if(tExamineX == tTileIntX && tExamineY == tTileIntY)
        {
            RegisterObjectToList(mChestList->GetIteratorName(), AL_SPECIAL_TYPE_CHEST);
        }

        //--Next.
        rChestPackage = (DoorPackage *)mChestList->AutoIterate();
    }

    //--Fake Chest.
    DoorPackage *rFakeChestPackage = (DoorPackage *)mFakeChestList->PushIterator();
    while(rFakeChestPackage)
    {
        //--Examination position.
        int tExamineX = (int)(rFakeChestPackage->mX / TileLayer::cxSizePerTile);
        int tExamineY = (int)(rFakeChestPackage->mY / TileLayer::cxSizePerTile);

        //--Hit.
        if(tExamineX == tTileIntX && tExamineY == tTileIntY)
        {
            RegisterObjectToList(mFakeChestList->GetIteratorName(), AL_SPECIAL_TYPE_FAKECHEST);
        }

        //--Next.
        rFakeChestPackage = (DoorPackage *)mFakeChestList->AutoIterate();
    }

    //--Door.
    DoorPackage *rDoorPack = (DoorPackage *)mDoorList->PushIterator();
    while(rDoorPack)
    {
        //--Examination position.
        if(!rDoorPack->mIsWide)
        {
            tTempDim.SetWH(rDoorPack->mX, rDoorPack->mY, TileLayer::cxSizePerTile, TileLayer::cxSizePerTile);
        }
        else
        {
            tTempDim.SetWH(rDoorPack->mX, rDoorPack->mY, TileLayer::cxSizePerTile*2, TileLayer::cxSizePerTile);
        }

        //--Hit.
        if(IsPointWithin2DReal(pWorldX, pWorldY, tTempDim))
        {
            RegisterObjectToList(mDoorList->GetIteratorName(), AL_SPECIAL_TYPE_DOOR);
        }

        //--Next.
        rDoorPack = (DoorPackage *)mDoorList->AutoIterate();
    }

    //--Exit.
    ExitPackage *rExitPack = (ExitPackage *)mExitList->PushIterator();
    while(rExitPack)
    {
        //--Hit.
        if(IsPointWithin(pWorldX, pWorldY, rExitPack->mX, rExitPack->mY, rExitPack->mX+rExitPack->mW, rExitPack->mY+rExitPack->mH))
        {
            RegisterObjectToList(mExitList->GetIteratorName(), AL_SPECIAL_TYPE_EXIT);
        }

        //--Next.
        rExitPack = (ExitPackage *)mExitList->AutoIterate();
    }

    //--Location
    TwoDimensionReal *rLocationPack = (TwoDimensionReal *)mLocationList->PushIterator();
    while(rLocationPack)
    {
        //--Hit.
        if(IsPointWithin2DReal(pWorldX, pWorldY, *rLocationPack))
        {
            RegisterObjectToList(mLocationList->GetIteratorName(), AL_SPECIAL_TYPE_LOCATION);
        }

        //--Next.
        rLocationPack = (TwoDimensionReal *)mLocationList->AutoIterate();
    }

    //--Entities.
    EntityManager *rEntityManager = EntityManager::Fetch();
    SugarLinkedList *rEntityList = rEntityManager->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            float tWorldX = rActor->GetWorldX();
            float tWorldY = rActor->GetWorldY();

            float tDistance = GetPlanarDistance(tWorldX, tWorldY, pWorldX, pWorldY);
            if(tDistance < 10.0f)
            {
                RegisterObjectToList(rCheckEntity->GetName(), AL_SPECIAL_TYPE_ACTOR);
            }
        }

        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    //--TODO
    /*
    SugarLinkedList *mInflectionList; //InflectionPackage *, master
    SugarLinkedList *mClimbableList; //ClimbablePackage *, master
    SugarLinkedList *mInvisibleZoneList;//InvisZone *, master

//--Types for Local Packages
#define AL_SPECIAL_TYPE_INFLECTION 5
#define AL_SPECIAL_TYPE_CLIMBABLE 6
#define AL_SPECIAL_TYPE_INVISZONE 9
*/

}
void AdventureLevel::RegisterObjectToList(const char *pName, int pType)
{
    if(!pName || !pType) return;
    FieldAbilityObjectPack *nObjectPack = (FieldAbilityObjectPack *)starmemoryalloc(sizeof(FieldAbilityObjectPack));
    nObjectPack->Initialize();
    nObjectPack->mType = pType;
    strncpy(nObjectPack->mName, pName, 127);
    mFieldAbilityObjectList->AddElement("X", nObjectPack, &FreeThis);
}
int AdventureLevel::GetNumberOfObjectsAt()
{
    return mFieldAbilityObjectList->GetListSize();
}
int AdventureLevel::GetObjectCodeAt(int pSlot)
{
    FieldAbilityObjectPack *rPackage = (FieldAbilityObjectPack *)mFieldAbilityObjectList->GetElementBySlot(pSlot);
    if(!rPackage) return AL_SPECIAL_TYPE_FAIL;
    return rPackage->mType;
}
const char *AdventureLevel::GetObjectNameAt(int pSlot)
{
    FieldAbilityObjectPack *rPackage = (FieldAbilityObjectPack *)mFieldAbilityObjectList->GetElementBySlot(pSlot);
    if(!rPackage) return NULL;
    return rPackage->mName;
}
int AdventureLevel::GetRetreatTimer()
{
    return mRetreatTimer;
}

//========================================= Manipulators ==========================================
void AdventureLevel::SetName(const char *pName)
{
    //--Basics.
    if(!pName) return;
    ResetString(mName, pName);

    //--Random levels never do this.
    if(!strcasecmp(pName, "RandomLevel"))
    {
        mIsRandomLevel = true;
        return;
    }

    //--Setup.
    char tSaveBuf[128];

    //--Go through the spawn packages and update all the names to use the new room name. This should
    //  only be done once!
    SpawnPackage *rPackage = (SpawnPackage *)mEnemySpawnList->PushIterator();
    while(rPackage)
    {
        strcpy(tSaveBuf, rPackage->mUniqueSpawnName);
        sprintf(rPackage->mUniqueSpawnName, "STD|%s|%s", mName, tSaveBuf);
        rPackage = (SpawnPackage *)mEnemySpawnList->AutoIterate();
    }

    //--Check if the player has the Platinum Compass.
    bool tHasCompass = false;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rPlatinumCompassVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/Global/CatalystTone/iHasCatalystTone");
    if(rPlatinumCompassVar && rPlatinumCompassVar->mNumeric == 1.0f)
    {
        tHasCompass = true;
    }

    //--Go through the chest listing. Now that we know the name of the level, we can specify whether
    //  the chests are open or not based on if a matching variable exists.
    DoorPackage *rChest = (DoorPackage *)mChestList->PushIterator();
    while(rChest)
    {
        //--Check the VM. If a variable exists, this chest starts in the opened position and ignores open attempts.
        char tBuffer[128];
        sprintf(tBuffer, "Root/Variables/Chests/%s/%s", mName, mChestList->GetIteratorName());
        void *rCheckPtr = rDataLibrary->GetEntry(tBuffer);
        if(rCheckPtr)
        {
            rChest->mIsOpened = true;
        }
        //--If the chest is not opened, and it's a catalyst, flag the catalyst case if the player has the Platinum Compass.
        else if(tHasCompass && !strncasecmp(rChest->mContents, "CATALYST|", 9))
        {
            FlagCatalystTone();
        }

        //--Next.
        rChest = (DoorPackage *)mChestList->AutoIterate();
    }
}
void AdventureLevel::SetBasePath(const char *pPath)
{
    ResetString(mBasePath, pPath);
}
void AdventureLevel::SetControlEntityID(uint32_t pID)
{
    //--If the entity ID is not the current ID, locate the current entity and unflag it as
    //  a party member. This mostly affects lighting.
    EntityManager *rEntityManager = EntityManager::Fetch();
    if(pID != mControlEntityID && mControlEntityID != 0)
    {
        //--If the Actor exists, unset their flag.
        RootEntity *rEntity = rEntityManager->GetEntityI(mControlEntityID);
        if(rEntity && rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rEntity;
            rActor->SetPartyEntityFlag(false);
        }
    }

    //--Note: Set to 0 to disable player NPC control.
    mControlEntityID = pID;

    //--Locate the actor and set them as a party member.
    RootEntity *rEntity = rEntityManager->GetEntityI(mControlEntityID);
    if(rEntity && rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        TilemapActor *rActor = (TilemapActor *)rEntity;
        rActor->SetPartyEntityFlag(true);
    }
}
void AdventureLevel::AddFollowEntityID(uint32_t pID)
{
    //--Baseline.
    SetMemoryData(__FILE__, __LINE__);
    uint32_t *nPtr = (uint32_t *)starmemoryalloc(sizeof(uint32_t));
    *nPtr = pID;
    mFollowEntityIDList->AddElementAsTail("X", nPtr, &FreeThis);

    //--If the Actor exists, set their following flag. This is used for some rendering.
    RootEntity *rEntity = EntityManager::Fetch()->GetEntityI(pID);
    if(rEntity && rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        //--Cast, set.
        TilemapActor *rActor = (TilemapActor *)rEntity;
        rActor->SetPartyEntityFlag(true);
    }
}
void AdventureLevel::RemoveFollowEntityName(const char *pName)
{
    //--Try to find the named NPC on the follower list and remove them from it if found.
    if(!pName) return;
    EntityManager *rEntityManager = EntityManager::Fetch();

    //--Iterate.
    uint32_t *rPtr = (uint32_t *)mFollowEntityIDList->SetToHeadAndReturn();
    while(rPtr)
    {
        //--Find the associated entity. Must be a TilemapActor.
        RootEntity *rEntity = rEntityManager->GetEntityI(*rPtr);
        if(rEntity && rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Cast, check the name.
            TilemapActor *rActor = (TilemapActor *)rEntity;
            if(!strcasecmp(rActor->GetName(), pName))
            {
                rActor->SetPartyEntityFlag(false);
                mFollowEntityIDList->RemoveRandomPointerEntry();
                return;
            }
        }

        //--Next.
        rPtr = (uint32_t *)mFollowEntityIDList->IncrementAndGetRandomPointerEntry();
    }
}
void AdventureLevel::AddLockout(const char *pName)
{
    //--Check if the lockout already exists. If it does, don't re-add it.
    if(mControlLockList->GetElementByName(pName)) return;

    //--Otherwise, add it with a dummy pointer.
    static int xDummyValue = 0;
    mControlLockList->AddElement(pName, &xDummyValue);
}
void AdventureLevel::RemoveLockout(const char *pName)
{
    //--Special: If the name is "ALL" or "CLEAR" then wipe the list.
    if(pName && (!strcasecmp(pName, "ALL") || !strcasecmp(pName, "CLEAR")))
    {
        mControlLockList->ClearList();
        return;
    }

    //--Remove it from the list.
    mControlLockList->RemoveElementS(pName);
}
void AdventureLevel::SetExaminationScript(const char *pPath)
{
    ResetString(mExaminableScript, pPath);
}
void AdventureLevel::AddExaminable(int pX, int pY, const char *pFiringString)
{
    //--Overload, assumes examinable is 1x1.
    AddExaminable(pX, pY, 1, 1, 0, pFiringString);
}
void AdventureLevel::AddExaminable(int pX, int pY, int pW, int pH, uint8_t pFlags, const char *pFiringString)
{
    //--Adds a new ExaminablePack to the list, or, if the pack already exists, modifies its position.
    //  Note that the name of the firing string is the name of the object on the list, it is not
    //  stored independently of the list.
    //--It is legal to register the same firing string twice, this means that the same "object" would
    //  have multiple positions that can be examined.
    SetMemoryData(__FILE__, __LINE__);
    ExaminePackage *nPackage = (ExaminePackage *)starmemoryalloc(sizeof(ExaminePackage));
    nPackage->Initialize();
    nPackage->mX = pX;
    nPackage->mY = pY;
    nPackage->mW = pW;
    nPackage->mH = pH;
    nPackage->mFlags = pFlags;
    mExaminablesList->AddElement(pFiringString, nPackage, &FreeThis);
}
void AdventureLevel::AddExaminableTransition(const char *pName, int pX, int pY, int pW, int pH, uint8_t pFlags, const char *pRoomDest, const char *pPosDest, const char *pPlaySound)
{
    //--Adds a new ExaminablePack to the list, as above. This one is pre-built to be a room transition.
    //  Room transitions can still occur in the examination file if needed, this just allows automation.
    if(!pName) return;
    SetMemoryData(__FILE__, __LINE__);
    ExaminePackage *nPackage = (ExaminePackage *)starmemoryalloc(sizeof(ExaminePackage));
    nPackage->Initialize();
    nPackage->mX = pX;
    nPackage->mY = pY;
    nPackage->mW = pW;
    nPackage->mH = pH;
    nPackage->mFlags = pFlags;
    mExaminablesList->AddElement(pName, nPackage, &FreeThis);

    //--Transition flags.
    if(!pRoomDest || !pPosDest || !pPlaySound) return;
    nPackage->mIsAutoTransition = true;
    strncpy(nPackage->mAutoRoomDest,     pRoomDest,  STD_MAX_LETTERS);
    strncpy(nPackage->mAutoLocationDest, pPosDest,   STD_MAX_LETTERS);
    strncpy(nPackage->mAutoSound,        pPlaySound, STD_MAX_LETTERS);
}
void AdventureLevel::AddPatrolNode(const char *pName, int pX, int pY, int pW, int pH)
{
    //--Adds a patrol node. Used by enemies to get around.
    if(!pName) return;
    SetMemoryData(__FILE__, __LINE__);
    TwoDimensionReal *nZone = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));
    nZone->SetWH(pX, pY, pW, pH);
    mPatrolNodes->AddElement(pName, nZone, &FreeThis);
}
void AdventureLevel::AddEnemy(const char *pName, int pX, int pY, const char *pParty, const char *pAppearance, const char *pScene, int pToughness, const char *pPatrolPath, const char *pFollow)
{
    //--Adds an enemy spawn package. This is normally done via the map object loader, but this function is used
    //  when enemy spawns are generated via scripts. Note that this generates the spawn package, NOT the enemy.
    //  When randomly generating levels, all spawn packages always spawn.
    if(!pName || !pParty || !pAppearance || !pScene || !pPatrolPath || !pFollow) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    SpawnPackage *nPackage = (SpawnPackage *)starmemoryalloc(sizeof(SpawnPackage));
    nPackage->Initialize();
    strncpy(nPackage->mRawName, pName, STD_MAX_LETTERS);

    //--Position.
    nPackage->mDimensions.SetWH(pX, pY, 16.0f, 16.0f);

    //--Properties.
    strcpy(nPackage->mPartyEnemy, pParty);
    strcpy(nPackage->mAppearancePath, pAppearance);
    strcpy(nPackage->mLoseCutscene, pScene);
    nPackage->mToughness = pToughness;
    nPackage->mDontRefaceFlag = true;
    strcpy(nPackage->mPatrolPathway, pPatrolPath);
    strcpy(nPackage->mFollowTarget, pFollow);
    sprintf(nPackage->mUniqueSpawnName, "%s", pName);
    RegisterEnemyPack(nPackage);
}
void AdventureLevel::AddChest(const char *pName, int pX, int pY, bool pIsFuture, const char *pContents)
{
    //--Spawns a chest. Usually used for the random level generator. Chests spawned in this manner
    //  are never already opened and are not saved to the variable listing.
    if(!pName || !pContents) return;

    //--Create a new package.
    SetMemoryData(__FILE__, __LINE__);
    DoorPackage *nChestPack = (DoorPackage *)starmemoryalloc(sizeof(DoorPackage));
    nChestPack->Initialize();

    //--Copy position data.
    nChestPack->mX = pX;
    nChestPack->mY = pY;

    //--Set images.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    nChestPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestC");
    nChestPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestO");
    if(pIsFuture)
    {
        nChestPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestFC");
        nChestPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestFO");
    }

    //--Properties.
    ResetString(nChestPack->mContents, pContents);

    //--Register.
    mChestList->AddElement(pName, nChestPack, &FreeThis);
}
void AdventureLevel::SetTransitionDestination(const char *pMapDestination, const char *pExitDestination)
{
    ResetString(mTransitionDestinationMap, pMapDestination);
    ResetString(mTransitionDestinationExit, pExitDestination);
}
void AdventureLevel::SetTransitionPostExec(const char *pScriptPath)
{
    //--Optional, sets the script that fires immediately after a transition is completed. Usually used for cutscenes.
    if(!pScriptPath || !strcasecmp(pScriptPath, "Null"))
    {
        ResetString(mTransitionPostScript, NULL);
        return;
    }
    ResetString(mTransitionPostScript, pScriptPath);
}
void AdventureLevel::SetTransitionPosition(int pDirection, float pPercentageOffset)
{
    mTransitionDirection = pDirection;
    mTransitionPercentage = pPercentageOffset;
}
void AdventureLevel::OpenDoor(const char *pName)
{
    //--Manually opens a door, usually called by scripts. Does not inherently play any sounds.
    DoorPackage *rDoor = (DoorPackage *)mDoorList->GetElementByName(pName);
    if(!rDoor) return;

    //--Flag as opened.
    rDoor->mIsOpened = true;

    //--Remove it from the RCEs.
    int tDoorNumber = mDoorList->GetSlotOfElementByPtr(rDoor);
    char tNameBuffers[4][256];
    for(int i = 0; i < 4; i ++)
    {
        sprintf(tNameBuffers[i], "%i|%i", tDoorNumber, i);
        for(int p = 0; p < mRayCollisionEnginesTotal; p ++)
        {
            if(!mRayCollisionEngines[p]) continue;
            mRayCollisionEngines[p]->UnsetVariableLine(tNameBuffers[i]);
        }
    }
}
void AdventureLevel::CloseDoor(const char *pName)
{
    //--Manually closes a door.
    DoorPackage *rDoor = (DoorPackage *)mDoorList->GetElementByName(pName);
    if(!rDoor) return;

    //--Flag as opened.
    rDoor->mIsOpened = false;

    //--Remove it from the RCEs.
    int tDoorNumber = mDoorList->GetSlotOfElementByPtr(rDoor);
    char tNameBuffers[4][256];
    for(int i = 0; i < 4; i ++)
    {
        sprintf(tNameBuffers[i], "%i|%i", tDoorNumber, i);
        for(int p = 0; p < mRayCollisionEnginesTotal; p ++)
        {
            if(!mRayCollisionEngines[p]) continue;
            mRayCollisionEngines[p]->ResetVariableLine(tNameBuffers[i]);
        }
    }
}
void AdventureLevel::SetSwitchState(const char *pName, bool pIsUp)
{
    DoorPackage *rSwitchPackage = (DoorPackage *)mSwitchList->GetElementByName(pName);
    if(rSwitchPackage)
    {
        rSwitchPackage->mIsOpened = pIsUp;
    }
}
void AdventureLevel::SetReinforcementPulseMode(bool pFlag)
{
    //--Flags.
    mIsPulseMode = pFlag;
    mPulseTimer = 0;

    //--When activating, all active field abilities run their touch-enemy code.
    if(mIsPulseMode)
    {
        //--Flag abilities to enter cancel mode.
        mFieldAbilitiesInCancelMode = true;

        //--Iterate across the active packs.
        FieldAbilityPack *rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->PushIterator();
        while(rFieldAbilityPack)
        {
            //--Mark the global package. This is the one referenced in lua scripts.
            rExecutingPackage = rFieldAbilityPack;

            //--Run the field ability touch-enemy code.
            if(rFieldAbilityPack->rParentAbility)
            {
                rFieldAbilityPack->rParentAbility->Execute(FIELDABILITY_EXEC_TOUCHENEMY);
            }

            //--Next.
            rExecutingPackage = NULL;
            rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->AutoIterate();
        }
    }
}
void AdventureLevel::RunFieldAbilitiesOnActor(TilemapActor *pActor)
{
    //--Runs all active field abilities on the given actor, allowing them to modify its internal state.
    if(!pActor) return;

    //--Iterate across the active packs.
    FieldAbilityPack *rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->PushIterator();
    while(rFieldAbilityPack)
    {
        //--Mark the global package. This is the one referenced in lua scripts.
        rExecutingPackage = rFieldAbilityPack;

        //--Run the field ability touch-enemy code.
        if(rFieldAbilityPack->rParentAbility)
        {
            rFieldAbilityPack->rParentAbility->ExecuteOn(FIELDABILITY_EXEC_MODIFYACTOR, pActor);
        }

        //--Next.
        rExecutingPackage = NULL;
        rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->AutoIterate();
    }
}
void AdventureLevel::BeginRestSequence()
{
    mIsRestSequence = true;
    mRestingTimer = 0;
}
void AdventureLevel::SetBackground(const char *pPath)
{
    rBackgroundImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void AdventureLevel::SetBackgroundPositions(float pX, float pY)
{
    mBackgroundStartX = pX;
    mBackgroundStartY = pY;
}
void AdventureLevel::SetBackgroundScrolls(float pX, float pY)
{
    mBackgroundScrollX = pX;
    mBackgroundScrollY = pY;
}
void AdventureLevel::FlagCatalystTone()
{
    mPlayCatalystTone = true;
    mCatalystToneCountdown = 15;
}
void AdventureLevel::SetLightBoost(float pValue)
{
    //--Set.
    xLightBoost = pValue;
    if(xLightBoost < 0.0f) xLightBoost = 0.0f;
    if(xLightBoost > 1.0f) xLightBoost = 1.0f;

    //--Store this in the DataLibrary.
    SysVar *rSystemVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Special/fAmbientLightBoost");
    if(rSystemVariable)
    {
        rSystemVariable->mNumeric = xLightBoost;
    }
}
void AdventureLevel::SetRenderingDisabled(const char *pLayerName, bool pFlag)
{
    TileLayer *rLayer = (TileLayer *)mTileLayers->GetElementByName(pLayerName);
    if(!rLayer) return;
    rLayer->SetDoNotRenderFlag(pFlag);
}
void AdventureLevel::SetAnimationDisabled(const char *pLayerName, bool pFlag)
{
    TileLayer *rLayer = (TileLayer *)mTileLayers->GetElementByName(pLayerName);
    if(!rLayer) return;
    rLayer->SetAnimationDisable(pFlag);
}
void AdventureLevel::SetCollision(int pX, int pY, int pZ, int pCollisionValue)
{
    //--Manually modifies a collision. Used for cutscenes, doesn't affect the Ray Collision Engine.
    //  pX and pY are in tiles, not pixels.
    if(pZ < 0 || pZ >= mCollisionLayersTotal) return;

    //--Edge check.
    if(pX < 0 || pY < 0 || pX >= mCollisionLayers[pZ].mCollisionSizeX || pY >= mCollisionLayers[pZ].mCollisionSizeY) return;

    //--Collision hull.
    mCollisionLayers[pZ].mCollisionData[pX][pY] = pCollisionValue;
}
void AdventureLevel::RegisterObjective(const char *pName)
{
    if(!pName) return;
    SetMemoryData(__FILE__, __LINE__);
    bool *nObjective = (bool *)starmemoryalloc(sizeof(bool));
    *nObjective = false;
    mObjectivesList->AddElement(pName, nObjective, &FreeThis);
}
void AdventureLevel::FlagObjectiveIncomplete(const char *pName)
{
    bool *rObjectivePtr = (bool *)mObjectivesList->GetElementByName(pName);
    if(!pName || !rObjectivePtr) return;
    *rObjectivePtr = false;
}
void AdventureLevel::FlagObjectiveComplete(const char *pName)
{
    bool *rObjectivePtr = (bool *)mObjectivesList->GetElementByName(pName);
    if(!pName || !rObjectivePtr) return;
    *rObjectivePtr = true;
}
void AdventureLevel::ClearObjectives()
{
    mObjectivesList->ClearList();
}
void AdventureLevel::RegisterDislocationPack(const char *pName, const char *pLayer, int pXStart, int pYStart, int pWid, int pHei, float pXTarget, float pYTarget)
{
    if(!pName || !pLayer || pWid < 1 || pHei < 1) return;

    SetMemoryData(__FILE__, __LINE__);
    DislocatedRenderPackage *nPackage = (DislocatedRenderPackage *)starmemoryalloc(sizeof(DislocatedRenderPackage));
    nPackage->Initialize();
    strcpy(nPackage->mLayerName, pLayer);
    nPackage->mOrigXStart = pXStart;
    nPackage->mOrigYStart = pYStart;
    nPackage->mWidth = pWid;
    nPackage->mHeight = pHei;
    nPackage->mNewXRender = pXTarget;
    nPackage->mNewYRender = pYTarget;
    mDislocatedRenderList->AddElement(pName, nPackage, &FreeThis);
}
void AdventureLevel::ModifyDislocationPack(const char *pName, float pXTarget, float pYTarget)
{
    DislocatedRenderPackage *rPackage = (DislocatedRenderPackage *)mDislocatedRenderList->GetElementByName(pName);
    if(!rPackage) return;
    rPackage->mNewXRender = pXTarget;
    rPackage->mNewYRender = pYTarget;
}
void AdventureLevel::SetMajorAnimationMode(const char *pAnimationName)
{
    //--Activates major animation mode, which shows a single animation front-and-center above the dialogue box with
    //  the environment greyed out. Pass NULL to clear the mode.
    //--If the named animation is not found, the mode is also disabled.
    mIsMajorAnimationMode = false;
    mMajorAnimationName[0] = '\0';
    if(!pAnimationName) return;

    //--Make sure the animation exists.
    void *rCheckPtr = mPerMapAnimationList->GetElementByName(pAnimationName);
    if(!rCheckPtr) return;

    //--Set flags.
    mIsMajorAnimationMode = true;
    strncpy(mMajorAnimationName, pAnimationName, sizeof(char) * (STD_MAX_LETTERS - 1));
}
void AdventureLevel::AddAnimation(const char *pName, float pX, float pY, float pZ)
{
    //--Adds a Per-Map Animation, which use tilesets to create complicated animations that can be displayed on the screen.
    if(!pName) return;
    SetMemoryData(__FILE__, __LINE__);
    PerMapAnimation *nAnimation = (PerMapAnimation *)starmemoryalloc(sizeof(PerMapAnimation));
    nAnimation->Initialize();
    nAnimation->mRenderPosX = pX;
    nAnimation->mRenderPosY = pY;
    nAnimation->mRenderDepth = pZ;
    nAnimation->mTicksPerFrame = 1.0f;
    nAnimation->mTotalFrames = 0;
    nAnimation->mrImagePtrs = NULL;
    nAnimation->mSizePerFrameX = 1.0f;

    //--Register it.
    mPerMapAnimationList->AddElement(pName, nAnimation, &FreeThis);
}
void AdventureLevel::SetAnimationFromPattern(const char *pName, const char *pPattern, int pFrames)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation) return;

    //--Arg check.
    if(!pPattern || pFrames < 1) return;

    //--Allocate space.
    rAnimation->Allocate(pFrames);

    //--Locate the DL Heading which holds the entire animation. Fail if it's not found.
    char cLocation[256];
    sprintf(cLocation, "Root/Images/TempImg/%s/", pPattern);
    SugarLinkedList *rHeading = DataLibrary::Fetch()->GetHeading(cLocation);
    if(!rHeading) return;

    //--Iterate. The entries are expected to be in order.
    int i = 0;
    void *rImage = rHeading->PushIterator();
    while(i < pFrames && rImage)
    {
        //--Set.
        rAnimation->mrImagePtrs[i] = (SugarBitmap *)rImage;

        //--Next.
        i ++;
        rImage = rHeading->AutoIterate();
    }
}
void AdventureLevel::SetAnimationRender(const char *pName, bool pIsRendering)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation) return;

    //--Flag.
    rAnimation->mIsRendering = pIsRendering;
}
void AdventureLevel::SetAnimationDestinationFrame(const char *pName, float pDestinationFrame)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation) return;

    //--Set.
    rAnimation->mLoopType = PMA_LOOP_NONE;
    rAnimation->mDestinationFrame = pDestinationFrame;
    rAnimation->mTickRate = 1.0f;
}
void AdventureLevel::SetAnimationDestinationFrame(const char *pName, float pDestinationFrame, float pFramerate)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation) return;

    //--Set.
    rAnimation->mLoopType = PMA_LOOP_NONE;
    rAnimation->mDestinationFrame = pDestinationFrame;
    rAnimation->mTickRate = pFramerate;
}
void AdventureLevel::SetAnimationLoop(const char *pName, float pLoopStartFrame, float pLoopEndFrame, const char *pLoopType, float pFramerateToTarget, float pTicksForWholeLoop)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation || !pLoopType || pTicksForWholeLoop < 1.0f) return;

    //--Sinusoidal looping.
    if(!strcasecmp(pLoopType, "Sin"))
    {
        rAnimation->mRebootLoopTimer = true;
        rAnimation->mLoopType = PMA_LOOP_SINUSOIDAL;
        rAnimation->mLowFrame = pLoopStartFrame;
        rAnimation->mHiFrame = pLoopEndFrame;
        rAnimation->mInternalLoopTimer = 0.0f;
        rAnimation->mTickRate = pFramerateToTarget;
        rAnimation->mTicksPerWholeLoop = pTicksForWholeLoop;
    }
    //--Linear looping.
    else if(!strcasecmp(pLoopType, "Loop"))
    {
        rAnimation->mRebootLoopTimer = true;
        rAnimation->mLoopType = PMA_LOOP_LINEAR;
        rAnimation->mLowFrame = pLoopStartFrame;
        rAnimation->mHiFrame = pLoopEndFrame;
        rAnimation->mInternalLoopTimer = 0.0f;
        rAnimation->mTickRate = pFramerateToTarget;
        rAnimation->mTicksPerWholeLoop = pTicksForWholeLoop;
    }
    //--Reverse looping.
    else if(!strcasecmp(pLoopType, "Reverse"))
    {
        rAnimation->mRebootLoopTimer = true;
        rAnimation->mLoopType = PMA_LOOP_REVERSE;
        rAnimation->mLowFrame = pLoopStartFrame;
        rAnimation->mHiFrame = pLoopEndFrame;
        rAnimation->mInternalLoopTimer = 0.0f;
        rAnimation->mTickRate = pFramerateToTarget;
        rAnimation->mTicksPerWholeLoop = pTicksForWholeLoop;
    }
    //--Error.
    else
    {

    }
}
void AdventureLevel::SetAnimationCurrentFrame(const char *pName, float pOverrideFrame)
{
    //--Locate.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(pName);
    if(!rAnimation) return;

    //--Set.
    rAnimation->mTimer = pOverrideFrame * rAnimation->mTicksPerFrame;
}
void AdventureLevel::ActivateUnderwaterShader()
{
    mIsUnderwaterMode = true;
}
void AdventureLevel::DeactivateUnderwaterShader()
{
    mIsUnderwaterMode = false;
}
void AdventureLevel::SetParallelCutsceneDuringDialogueFlag(bool pFlag)
{
    mRunParallelDuringDialogue = pFlag;
}
void AdventureLevel::SetParallelCutsceneDuringCutsceneFlag(bool pFlag)
{
    mRunParallelDuringCutscene = pFlag;
}
void AdventureLevel::SetScreenShake(int pTicks)
{
    mScreenShakeTimer = pTicks;
}
void AdventureLevel::SetScreenShakePeriodicity(int pShakeLength, int pTickPeriod, int pScatter)
{
    //--Variables.
    mShakePeriodLength = pShakeLength;
    mShakePeriodicityRoot = pTickPeriod;
    mShakePeriodicityScatter = pScatter;

    //--If the screen shake is currently zero, does a roll immediately.
    mScreenShakeTimer = -mShakePeriodicityRoot;
}
void AdventureLevel::AddShakeSoundEffect(const char *pSoundName)
{
    char *nSoundCopy = InitializeString(pSoundName);
    mShakeSoundList->AddElement("X", nSoundCopy, &FreeThis);
}
void AdventureLevel::SetRockFallChance(int pChance)
{
    mRockFallingFrequency = pChance;
}
void AdventureLevel::AddRockFallImage(const char *pDLPath)
{
    SugarBitmap *rImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    mrFallingRockImageList->AddElement("X", rImage);
}
void AdventureLevel::SetHideViewcones(bool pFlag)
{
    mHideAllViewcones = pFlag;
}
void AdventureLevel::ReceiveRunningMinigame(RunningMinigame *pRunningMinigame)
{
    //--Gives the level a RunningMinigame. Takes ownership implicitly. Pass NULL to deallocate and
    //  clear an existing class.
    delete mRunningMinigame;
    mRunningMinigame = pRunningMinigame;

    //--If the minigame exists, set music.
    if(mRunningMinigame)
    {
        AudioManager::Fetch()->PlayMusic("TimeSensitive");
    }
}
void AdventureLevel::ReceiveKPopMinigame(KPopDanceGame *pMinigame)
{
    //--Gives the level a KPopDanceGame. Takes ownership. Pass NULL to deallocate and clear.
    delete mKPopDanceGame;
    mKPopDanceGame = pMinigame;

    //--If the minigame exists, stop the music.
    if(mKPopDanceGame) AudioManager::Fetch()->StopAllMusic();
}
void AdventureLevel::RegisterNotice(ActorNotice *pNotice)
{
    mNotices->AddElement("X", pNotice, &RootObject::DeleteThis);
}
void AdventureLevel::RunPostCombatOnEntities()
{
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootObject *rEntity = (RootObject *)rEntityList->PushIterator();
    while(rEntity)
    {
        //--Must be of the POINTER_TYPE_TILEMAPACTOR type.
        if(rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rEntity;
            rActor->HandleUIPostBattle();
        }

        //--Next.
        rEntity = (RootObject *)rEntityList->AutoIterate();
    }
}
void AdventureLevel::SetRetreatTimer(int pTicks)
{
    mRetreatTimer = pTicks;
    if(mRetreatTimer < 1) mRetreatTimer = 0;
}

//========================================= Core Methods ==========================================
TilemapActor *AdventureLevel::LocatePlayerActor()
{
    //--Determines the NPC that the player is controlling and returns it. Can legally return NULL if
    //  the player is not controlling an NPC or the NPC doesn't exist.
    //--If the player's controls are locked, this still returns the TilemapActor in question.
    //--If the entity ID is the wrong type, NULL is returned.
    RootEntity *rActor = EntityManager::Fetch()->GetEntityI(mControlEntityID);
    if(!rActor || !rActor->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return NULL;

    //--Checks passed, return this as the right type.
    return (TilemapActor *)rActor;
}
TilemapActor *AdventureLevel::LocateFollowingActor(int pIndex)
{
    //--Determines the NPC that is following the player-controlled NPC and returns it. Can legally
    //  return NULL if fewer than pIndex number of entities are following the player. NPCs follow the
    //  player when in the player's party, or for cutscenes.
    uint32_t *rEntityIDPtr = (uint32_t *)mFollowEntityIDList->GetElementBySlot(pIndex);
    if(!rEntityIDPtr) return NULL;

    //--Check if the entity exists and is of the right type.
    RootEntity *rActor = EntityManager::Fetch()->GetEntityI(*rEntityIDPtr);
    if(!rActor || !rActor->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return NULL;

    //--Checks passed, return it.
    return (TilemapActor *)rActor;
}
int AdventureLevel::GetEffectiveDepth(float pX, float pY)
{
    //--Returns an integer code indicating which collision hull the entity caller should use if its center
    //  point is at X,Y. The return will INFLECTION_NO_CHANGE if no entity is doing anything.
    if(mInflectionList->GetListSize() < 1 && mClimbableList->GetListSize() < 1) return INFLECTION_NO_CHANGE;

    //--Scan the inflection cases.
    InflectionPackage *rPackage = (InflectionPackage *)mInflectionList->PushIterator();
    while(rPackage)
    {
        //--If the point is within the package...
        if(IsPointWithin(pX, pY, rPackage->mX, rPackage->mY, rPackage->mX + rPackage->mW, rPackage->mY + rPackage->mH))
        {
            //--In all cases pop the iterator.
            mInflectionList->PopIterator();

            //--If within the top half, use the upper inflection.
            if(pY <= rPackage->mY + (rPackage->mH * 0.5f))
            {
                return rPackage->mUpperDepth;
            }

            //--Otherwise, use the lower inflection.
            return rPackage->mLowerDepth;
        }

        //--Next.
        rPackage = (InflectionPackage *)mInflectionList->AutoIterate();
    }

    //--Scan the climbable cases. They are basically the same as the inflection cases. If they are not
    //  active, the player can't enter them anyway.
    ClimbablePackage *rClimbPackage = (ClimbablePackage *)mClimbableList->PushIterator();
    while(rClimbPackage)
    {
        //--If the point is within the package...
        if(IsPointWithin(pX, pY, rClimbPackage->mX, rClimbPackage->mY, rClimbPackage->mX + rClimbPackage->mW, rClimbPackage->mY + rClimbPackage->mH))
        {
            //--In all cases pop the iterator.
            mClimbableList->PopIterator();

            //--If within the top half, use the upper inflection.
            if(pY <= rClimbPackage->mY + (rClimbPackage->mH * 0.5f))
            {
                return rClimbPackage->mUpperDepth;
            }

            //--Otherwise, use the lower inflection.
            return rClimbPackage->mLowerDepth;
        }

        //--Next.
        rClimbPackage = (ClimbablePackage *)mClimbableList->AutoIterate();
    }

    //--All checks failed. Do not change the inflection.
    return INFLECTION_NO_CHANGE;
}
bool AdventureLevel::IsInClimbableZone(float pX, float pY)
{
    //--Checks if the given position happens to be within a climbable zone. This is used to cause
    //  actors to always face up in such zones so it looks like they're climbing.
    ClimbablePackage *rClimbPackage = (ClimbablePackage *)mClimbableList->PushIterator();
    while(rClimbPackage)
    {
        //--If the point is within the package...
        if(IsPointWithin(pX, pY, rClimbPackage->mX, rClimbPackage->mY, rClimbPackage->mX + rClimbPackage->mW, rClimbPackage->mY + rClimbPackage->mH))
        {
            mClimbableList->PopIterator();
            return true;
        }

        //--Next.
        rClimbPackage = (ClimbablePackage *)mClimbableList->AutoIterate();
    }

    //--Not in a climbable zone.
    return false;
}
void AdventureLevel::PulseIgnore(const char *pString)
{
    //--Orders all enemies in the level to check if they are ignoring the player using the provided string.
    //  This is triggered whenever the player enters the level or changes forms while in the level.
    ResetString(xLastIgnorePulse, pString);

    //--Iterate.
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Check.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            ((TilemapActor *)rCheckEntity)->PulseIgnore(pString);
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }
}
void AdventureLevel::RepositionParty(TilemapActor *pPlayerActor, float pX, float pY)
{
    //--Repositions the party to the given location. This handles moving the following actors as well.
    //  Note that the value is in world-pixels, not tiles.
    //--Note: If calling this via a script, set the facing *before* calling this function so the party
    //  members are facing the same direction as the player.
    //--If the Actor is not provided, we have to locate it. DO NOT use this during a level transition, even a "LASTSAVE"
    //  one, because the player's actor is liberated before the transition. You MUST pass in an Actor in these cases.
    int tFacing = TA_DIR_SOUTH;

    //--Actor Location.
    TilemapActor *rPlayerActor = pPlayerActor;
    if(!rPlayerActor) rPlayerActor = LocatePlayerActor();

    //--If the values are -700.0f and -700.0f, this is a "Fold" order. The position becomes wherever the
    //  party leader currently is.
    if(pX == -700.0f && pY == -700.0f && rPlayerActor)
    {
        pX = rPlayerActor->GetWorldX();
        pY = rPlayerActor->GetWorldY();
    }

    //--Set the player actor. Also get facing information.
    if(rPlayerActor)
    {
        //--Reposition.
        rPlayerActor->SetPositionByPixel(pX, pY);
        rPlayerActor->SetPartyEntityFlag(true);

        //--Get variables.
        tFacing = rPlayerActor->GetFacing();
    }

    //--Order all the followers to reposition.
    for(int i = 0; i < mFollowEntityIDList->GetListSize(); i ++)
    {
        TilemapActor *rFollower = LocateFollowingActor(i);
        if(rFollower)
        {
            rFollower->SetPositionByPixel(pX, pY);
            rFollower->SetFacing(tFacing);
        }
    }

    //--Set all of the trailing positions to this.
    for(int i = 0; i < PLAYER_MOVE_STORAGE_TOTAL; i ++)
    {
        mPlayerXPositions[i] = pX;
        mPlayerYPositions[i] = pY;
        mPlayerFacings[i] = tFacing;
        mPlayerTimers[i] = 0;
        mPlayerDepths[i] = 0;
        mPlayerRunnings[i] = false;
    }
}
bool AdventureLevel::IsLineClipped(float pX1, float pY1, float pX2, float pY2, int pDepth)
{
    //--Returns true if the given line has a collision across it. This is done via a primitive ray-tracer.
    //  The tracer is relatively fast at the cost of accuracy, and very much on purpose.
    //--Lines are not allowed to cross depths. All such attempts would be considered failures.

    //--Distance check. Very short checks always are unclipped.
    const float cDistance = GetPlanarDistance(pX1, pY1, pX2, pY2);
    //fprintf(stderr, "Checking line collision %f\n", cDistance);
    if(cDistance < 2.0f) return false;

    //--Fast-access variables. These speed up execution.
    float tCurrentX = pX1;
    float tCurrentY = pY1;
    const float cDifX = (pX2 - pX1) / cDistance;
    const float cDifY = (pY2 - pY1) / cDistance;
    //fprintf(stderr, "Dif values: %f %f\n", cDifX, cDifY);

    //--Last-check values. There is no need to recheck collisions if the X/Y values didn't change
    //  in integer math since only integers can be checked. This is only used when very high precisions
    //  are being used, comment it out for low precisions.
    //int tLastCheckedX = -1000;
    //int tLastCheckedY = -1000;

    //--Starting at X1/Y1, iterate until at X2/Y2.
    for(float i = 0.0f; i < cDistance; i = i + 1.0f)
    {
        //--Is this position identical in integer to the last check? If so, skip it.
        /*if(tLastCheckedX == (int)tCurrentX && tLastCheckedY == (int)tCurrentY)
        {
            //fprintf(stderr, " Skip.\n");
            tCurrentX = tCurrentX + cDifX;
            tCurrentY = tCurrentY + cDifY;
            continue;
        }*/

        //--If not, store the position and run the check.
        //fprintf(stderr, " Checking at %i %i\n", (int)tCurrentX, (int)tCurrentY);
        if(GetClipAt(tCurrentX, tCurrentY, 0.0f, pDepth))
        {
            //fprintf(stderr, "  Hit collision.\n");
            return true;
        }

        //--If we got this far, the position was not clipped. Store and continue.
        //tLastCheckedX = (int)tCurrentX;
        //tLastCheckedY = (int)tCurrentY;
        tCurrentX = tCurrentX + cDifX;
        tCurrentY = tCurrentY + cDifY;
    }

    //--The entire trace ran through. No clips were found.
    //fprintf(stderr, "No hits.\n");
    return false;
}

//===================================== Private Core Methods ======================================
bool AdventureLevel::CheckActorAgainstExits(TilemapActor *pActor, bool pOnlyEmulate)
{
    //--Subroutine called during the update cycle after the player moves. The given TilemapActor
    //  checks if it is currently wholly within any of the exits on the map. If so, begin transition
    //  to the target.
    //--For safety reasons, a successfully used exit will be removed from the list and deallocated.
    //  If there is an error, such as being unable to load its destination, this prevents the program
    //  from locking.
    //--Returns true if the actor is on an exit, false if not.
    if(!pActor) return false;
    DebugPush(true, "Checking actor against exits.\n");

    //--Flag.
    bool tIsActorOnAnyStaircase = false;

    //--Get the Actor's position.
    DebugPrint("Getting actor position.\n");
    TwoDimensionReal tActorDim;
    tActorDim.SetWH(pActor->GetWorldX(), pActor->GetWorldY(), TA_SIZE, TA_SIZE);

    //--Compare it to exits.
    DebugPrint("Checking exits.\n");
    ExitPackage *rPackage = (ExitPackage *)mExitList->SetToHeadAndReturn();
    while(rPackage)
    {
        //--Flag.
        bool tIsCollision = false;

        //--Staircases: These can have a partial collision. The player's hitbox needs to merely touch the middle.
        DebugPrint("Checking staircases and non-staircases.\n");
        if(rPackage->mIsStaircase && IsPointWithin2DReal(rPackage->mX + (rPackage->mW * 0.5f), rPackage->mY + (rPackage->mH * 0.5f), tActorDim))
        {
            tIsCollision = true;
        }
        //--Non-staircase. These need a full coverage.
        else if(!rPackage->mIsStaircase && rPackage->mX < tActorDim.mLft && rPackage->mX + rPackage->mW > tActorDim.mRgt &&
           rPackage->mY < tActorDim.mTop && rPackage->mY + rPackage->mH > tActorDim.mBot)
        {
            tIsCollision = true;
        }

        //--Hit?
        DebugPrint("Checking for collision.\n");
        if(tIsCollision)
        {
            //--When emulating, a collision just returns true.
            if(pOnlyEmulate) return true;

            //--If this is a staircase and the Actor is currently exiting a transition, then this pass is ignored.
            if(mIsPlayerOnStaircase && rPackage->mIsStaircase)
            {
                tIsActorOnAnyStaircase = true;
            }
            //--This is a transition case.
            else
            {
                //--Set strings.
                DebugPrint("Resetting strings.\n");
                ResetString(mTransitionDestinationMap,  rPackage->mMapDestination);
                ResetString(mTransitionDestinationExit, rPackage->mExitDestination);

                //--Store the direction the player was facing to exit.
                DebugPrint("And so on.\n");
                mTransitionDirection = pActor->GetFacing();

                //--Horizontal exit: Store percentage.
                mTransitionPercentage = 0.5f;
                if(rPackage->mW >= rPackage->mH && rPackage->mW > 0.0f)
                {
                    mTransitionPercentage = (tActorDim.mLft - rPackage->mX) / (float)rPackage->mW;
                }
                //--Vertical exit: Store percentage.
                else if(rPackage->mH > 0.0f)
                {
                    mTransitionPercentage = (tActorDim.mTop - rPackage->mY) / (float)rPackage->mH;
                }

                //--Flags.
                mIsTransitionOut = true;

                //--If currently in a transition, just roll the timer:
                if(mIsTransitionIn)
                {
                    mIsTransitionIn = false;
                    mTransitionTimer = TRANSITION_FADE_TICKS - mTransitionTimer;
                }
                //--Neutral case:
                else
                {
                    mTransitionTimer = 0;
                }

                //--If this is a staircase, play a sound.
                if(rPackage->mIsStaircase)
                    AudioManager::Fetch()->PlaySound("World|Stairs");

                //--Remove the exit and finish.
                //mExitList->RemoveRandomPointerEntry();
                DebugPop("Complete, match found.\n");
                return true;
            }
        }

        //--Next.
        rPackage = (ExitPackage *)mExitList->IncrementAndGetRandomPointerEntry();
    }

    //--Flag: If the player was not on a staircase, unset this flag so they can safely move off of
    //  a staircase after a transition.
    if(!tIsActorOnAnyStaircase)
    {
        mIsPlayerOnStaircase = false;
    }

    //--Player is not on any exit.
    DebugPop("Complete, no matches found.\n");
    return false;
}
void AdventureLevel::CompileAfterParse()
{
    //--[Documentation and Setup]
    //--After the SLF file has parsed the minimum data needed to create the level, we do the optional
    //  compilation and setup. Right now, RayCollisionEngine setup takes place here.
    SugarLumpManager *rSLM = SugarLumpManager::Fetch();

    //--Setup.
    bool tNeedsToExport = false;

    //--After the SLF is parsed, built ray collision engines for the collision layers.
    mRayCollisionEnginesTotal = mCollisionLayersTotal;
    SetMemoryData(__FILE__, __LINE__);
    mRayCollisionEngines = (RayCollisionEngine **)starmemoryalloc(sizeof(RayCollisionEngine *) * mRayCollisionEnginesTotal);
    for(int i = 0; i < mRayCollisionEnginesTotal; i ++)
    {
        //--Name buffer.
        char tBuffer[STD_MAX_LETTERS];
        sprintf(tBuffer, "RCE%02i", i);

        //--Create the RCE.
        mRayCollisionEngines[i] = new RayCollisionEngine();

        //--Check if an RCE data buffer already exists. If so, just read that.
        if(rSLM->DoesLumpExist(tBuffer))
        {
            //--Get the lump.
            rSLM->StandardSeek(tBuffer, "RCEDATA000");
            VirtualFile *rVFile = rSLM->GetVirtualFile();

            //--The file is already in position, so pass it to the RCE.
            mRayCollisionEngines[i]->ReadFromFile(rVFile);
        }
        //--No RCE data exists. Compile the RCE from collision data. Then, store the lump
        //  data and write it back to the file so we don't have to compile it again.
        else
        {
            //--Inform the routine that we need to write the lumps back.
            tNeedsToExport = true;

            //--Compile.
            mRayCollisionEngines[i]->BuildWith(mCollisionLayers[i]);

            //--Create a lump.
            RootLump *nNewLump = SugarLumpManager::CreateLump(tBuffer);

            //--Populate with the RCE's data. It also automatically sets the data size.
            nNewLump->mOwnsData = true;
            nNewLump->mData = mRayCollisionEngines[i]->CreateDataBuffer(nNewLump->mDataSize);
            rSLM->RegisterLump(nNewLump);
        }
    }

    //--Now spit out a copy from the SLM of the last opened file, if flagged.
    if(tNeedsToExport)
    {
        rSLM->MergeLumps();
        rSLM->Write(rSLM->GetOpenPath());
    }

    //--[Door Building]
    //--Doors get added to the RCEs. Their lines are toggled off when the door is opened.
    int i = 0;
    char tNameBuffers[4][256];
    DoorPackage *rDoorPackage = (DoorPackage *)mDoorList->PushIterator();
    while(rDoorPackage)
    {
        //--Name.
        sprintf(tNameBuffers[0], "%i|%i", i, 0);
        sprintf(tNameBuffers[1], "%i|%i", i, 1);
        sprintf(tNameBuffers[2], "%i|%i", i, 2);
        sprintf(tNameBuffers[3], "%i|%i", i, 3);

        //--Register to all RCEs.
        for(int p = 0; p < mRayCollisionEnginesTotal; p ++)
        {
            //--Error check.
            if(!mRayCollisionEngines[p]) continue;

            //--Construct lines.
            float cLft = rDoorPackage->mX;
            float cTop = rDoorPackage->mY;
            float cRgt = rDoorPackage->mX + TileLayer::cxSizePerTile;
            float cBot = rDoorPackage->mY + TileLayer::cxSizePerTile;
            if(rDoorPackage->mIsWide) cRgt = cRgt + TileLayer::cxSizePerTile;
            mRayCollisionEngines[p]->RegisterVariableLine(tNameBuffers[0], cLft, cTop, cRgt, cTop);
            mRayCollisionEngines[p]->RegisterVariableLine(tNameBuffers[1], cLft, cBot, cRgt, cBot);
            mRayCollisionEngines[p]->RegisterVariableLine(tNameBuffers[2], cLft, cTop, cLft, cBot);
            mRayCollisionEngines[p]->RegisterVariableLine(tNameBuffers[3], cRgt, cTop, cRgt, cBot);
        }

        //--Next.
        i ++;
        rDoorPackage = (DoorPackage *)mDoorList->AutoIterate();
    }
}

//============================================ Update =============================================
bool AdventureLevel::IsStaminaBarVisible()
{
    //--Don't show the bar if the player's controls are locked.
    if(AreControlsLocked()) return false;
    if(mAdventureMenu->NeedsToBlockControls()) return false;

    //--All checks passed, bar is visible.
    return true;
}
void AdventureLevel::Update()
{
    //--Debug.
    DebugPush(true, "AdventureLevel: Update begin.\n");

    //--Music update.
    UpdateMusicLayering();

    //--Flag reset each tick.
    xIsPlayerInInvisZone = false;

    //--[Running Minigame]
    //--If active, takes over updates. Other objects may still update.
    if(mRunningMinigame)
    {
        //--Run the update.
        mRunningMinigame->Update();

        //--If active, stop updates here.
        if(!mRunningMinigame->IsGameFinished())
        {
            return;
        }
        //--If the game is finished, and no longer visible, delete it.
        else if(!mRunningMinigame->IsVisible())
        {
            delete mRunningMinigame;
            mRunningMinigame = NULL;
        }
    }

    //--[KPop Minigame]
    //--Yes, really.
    if(mKPopDanceGame)
    {
        //--Update.
        mKPopDanceGame->Update();

        //--If active, stop updates here.
        if(mKPopDanceGame->IsActive())
        {
            return;
        }
        //--If not active and not visible, delete it.
        else if(!mKPopDanceGame->IsVisible())
        {
            delete mKPopDanceGame;
            mKPopDanceGame = NULL;
        }
    }

    //--Screen shake handling.
    if(mScreenShakeTimer > 0)
    {
        //--Decrement.
        mScreenShakeTimer --;

        //--If the timer reaches zero and the periodicity is non-zero, roll a new shake time.
        if(mScreenShakeTimer == 0 && mShakePeriodicityRoot > 0)
        {
            //--If the periodicity's scatter is zero, it plays at fixed intervals.
            if(mShakePeriodicityScatter  <= 0)
            {
                mScreenShakeTimer = -mShakePeriodicityRoot;
            }
            //--Otherwise, roll.
            else
            {
                int tRoll = mShakePeriodicityRoot + (rand() % mShakePeriodicityScatter);
                mScreenShakeTimer = tRoll * -1;
            }
        }
    }
    //--If the shake timer is less than zero, it is counting up to a positive.
    else if(mScreenShakeTimer < 0)
    {
        //--Increment.
        mScreenShakeTimer ++;

        //--If the timer reaches zero, start a new shake sequence.
        if(mScreenShakeTimer == 0)
        {
            //--Set the timer.
            mScreenShakeTimer = mShakePeriodLength;

            //--Get a sound effect and play it, if any exist.
            char *rSoundName = (char *)mShakeSoundList->GetEntryByRandomRoll();
            if(rSoundName) AudioManager::Fetch()->PlaySound(rSoundName);
        }
    }

    //--Spawn Falling Rocks
    if(mRockFallingFrequency > 0)
    {
        //--Roll, 100% with two-decimal accuracy. Spawn a rock if the roll passes.
        int tRoll = rand() % 10000;
        if(tRoll <= mRockFallingFrequency)
        {
            SetMemoryData(__FILE__, __LINE__);
            FallingRockPack *nNewRock = (FallingRockPack *)starmemoryalloc(sizeof(FallingRockPack));
            nNewRock->Initialize();
            nNewRock->mX = (float)(rand() & (int)VIRTUAL_CANVAS_X);
            nNewRock->mYSpeed = 6.0f;
            nNewRock->mTheta = rand() % 360;
            nNewRock->mThetaDelta = (rand() % 9) - 4;
            nNewRock->rImage = (SugarBitmap *)mrFallingRockImageList->GetEntryByRandomRoll();
            mFallingRockList->AddElement("X", nNewRock, &FreeThis);
        }
    }

    //--Update falling rocks.
    FallingRockPack *rRock = (FallingRockPack *)mFallingRockList->SetToHeadAndReturn();
    while(rRock)
    {
        //--Update Y position.
        rRock->mY = rRock->mY + rRock->mYSpeed;
        rRock->mTheta = rRock->mTheta + rRock->mThetaDelta;

        //--Y position exceeds bottom of screen, delete.
        if(rRock->mY >= VIRTUAL_CANVAS_Y) mFallingRockList->RemoveRandomPointerEntry();

        //--Next.
        rRock = (FallingRockPack *)mFallingRockList->IncrementAndGetRandomPointerEntry();
    }

    //--Per-map animations update.
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->PushIterator();
    while(rAnimation)
    {
        //--If the animation is not rendering, ignore it.
        if(!rAnimation->mIsRendering)
        {
        }
        //--Tick it.
        else
        {
            rAnimation->Tick();
        }

        //--Next.
        rAnimation = (PerMapAnimation *)mPerMapAnimationList->AutoIterate();
    }

    //--Stamina bar is visible, increase its vis timer.
    if(IsStaminaBarVisible())
    {
        if(mStaminaVisTimer < STAMINA_VIS_TICKS) mStaminaVisTimer ++;
    }
    //--Decrement.
    else
    {
        if(mStaminaVisTimer > 0) mStaminaVisTimer --;
    }

    //--Lighting update. This is done in case any of the sub-updates return out before entities update.
    //  Normally we want to update lighting after entities have moved.
    if(mAreLightsActive) UpdateEntityLightPositions();

    //--Catalyst Tone.
    if(mPlayCatalystTone)
    {
        mCatalystToneCountdown --;
        if(mCatalystToneCountdown < 1)
        {
            mPlayCatalystTone = false;
            AudioManager::Fetch()->PlaySound("World|Catalyst");
        }
    }

    //--Field Abilities
    bool tAreControlsLockedByFieldAbility = false;
    bool tBlockFieldAbilityUpdate = mAdventureMenu->NeedsToBlockControls();
    if(!tBlockFieldAbilityUpdate)
    {
        FieldAbilityPack *rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->SetToHeadAndReturn();
        while(rFieldAbilityPack)
        {
            //--Mark the global package. This is the one referenced in lua scripts.
            rExecutingPackage = rFieldAbilityPack;

            //--Run the field ability update.
            if(rFieldAbilityPack->rParentAbility)
            {
                //--In normal mode:
                if(!mFieldAbilitiesInCancelMode)
                {
                    rFieldAbilityPack->rParentAbility->Execute(FIELDABILITY_EXEC_RUN);
                }
                //--In cancel mode:
                else
                {
                    rFieldAbilityPack->rParentAbility->Execute(FIELDABILITY_EXEC_RUNCANCEL);
                }
            }
            //--Parent was not set correctly, remove it.
            else
            {
                rFieldAbilityPack->mIsExpired = true;
            }

            //--If the package was marked as expired, terminate it here.
            if(rFieldAbilityPack->mIsExpired)
            {
                mExecutingFieldAbilities->RemoveRandomPointerEntry();
            }
            //--Otherwise, if the field ability pack is blocking controls, mark that here.
            else if(rFieldAbilityPack->mIsLockingControls)
            {
                tAreControlsLockedByFieldAbility = true;
            }

            //--Increment the timer.
            rFieldAbilityPack->mTimer ++;

            //--Next.
            rExecutingPackage = NULL;
            rFieldAbilityPack = (FieldAbilityPack *)mExecutingFieldAbilities->IncrementAndGetRandomPointerEntry();
        }
    }

    //--If field abilities are in cancel mode, the flag is cleared as soon as all abilities are removed.
    if(mFieldAbilitiesInCancelMode && mExecutingFieldAbilities->GetListSize() == 0)
    {
        mFieldAbilitiesInCancelMode = false;
    }

    //--Music controls.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(rControlManager->IsFirstPress("LoopBack"))
    {
        AudioManager::Fetch()->AdvanceMusicToLoopPoint();
        AudioManager::Fetch()->ModifyLoopPointBackward();
    }
    if(rControlManager->IsFirstPress("LoopForw"))
    {
        AudioManager::Fetch()->AdvanceMusicToLoopPoint();
        AudioManager::Fetch()->ModifyLoopPointForward();
    }

    //--Underlays.
    for(int i = 0; i < mUnderlaysTotal; i ++)
    {
        //--Alpha change over time.
        if(mUnderlayPacks[i].mAlphaTimer < mUnderlayPacks[i].mAlphaTimerMax)
        {
            //--Increment timer.
            mUnderlayPacks[i].mAlphaTimer ++;

            //--Compute percentage.
            float tPercent = (float)mUnderlayPacks[i].mAlphaTimer / (float)mUnderlayPacks[i].mAlphaTimerMax;
            mUnderlayPacks[i].mAlphaCurrent = mUnderlayPacks[i].mAlphaStart + ((mUnderlayPacks[i].mAlphaEnd - mUnderlayPacks[i].mAlphaStart) * tPercent);
        }

        //--Autoscroll.
        mUnderlayPacks[i].mOffsetBaseX = mUnderlayPacks[i].mOffsetBaseX + mUnderlayPacks[i].mAutoscrollX;
        mUnderlayPacks[i].mOffsetBaseY = mUnderlayPacks[i].mOffsetBaseY + mUnderlayPacks[i].mAutoscrollY;
    }

    //--Overlays.
    for(int i = 0; i < mOverlaysTotal; i ++)
    {
        //--Alpha change over time.
        if(mOverlayPacks[i].mAlphaTimer < mOverlayPacks[i].mAlphaTimerMax)
        {
            //--Increment timer.
            mOverlayPacks[i].mAlphaTimer ++;

            //--Compute percentage.
            float tPercent = (float)mOverlayPacks[i].mAlphaTimer / (float)mOverlayPacks[i].mAlphaTimerMax;
            mOverlayPacks[i].mAlphaCurrent = mOverlayPacks[i].mAlphaStart + ((mOverlayPacks[i].mAlphaEnd - mOverlayPacks[i].mAlphaStart) * tPercent);
        }

        //--Autoscroll.
        mOverlayPacks[i].mOffsetBaseX = mOverlayPacks[i].mOffsetBaseX + mOverlayPacks[i].mAutoscrollX;
        mOverlayPacks[i].mOffsetBaseY = mOverlayPacks[i].mOffsetBaseY + mOverlayPacks[i].mAutoscrollY;
    }

    //--Notifications.
    ActorNotice *rNotice = (ActorNotice *)mNotices->SetToHeadAndReturn();
    while(rNotice)
    {
        //--Update.
        rNotice->Update();

        //--If the notice has expired, delete it.
        if(rNotice->IsExpired()) mNotices->RemoveRandomPointerEntry();

        //--Next.
        rNotice = (ActorNotice *)mNotices->IncrementAndGetRandomPointerEntry();
    }

    //--Check if the player's controls are locked before the update starts. This removes double-control
    //  issues from consideration.
    DebugPrint("Checking control locking.\n");
    mWasMenuUpLastTick = false;
    bool tAreControlsLocked = AreControlsLocked();
    DebugPrint(" Control lock state %i\n", tAreControlsLocked);

    //--Script fading always runs.
    DebugPrint("Updating fade script.\n");
    UpdateScriptFade();

    //--Resting sequence. Updates the timer. Locks out everything else.
    DebugPrint("Checking rest sequence.\n");
    if(mIsRestSequence)
    {
        //--Debug.
        DebugPrint("Resting sequence.\n");

        //--The resting timer goes until it hits RESTING_TICKS_TOTAL.
        if(mRestingTimer < RESTING_TICKS_TOTAL)
        {
            //--On this tick, restore the party and respawn enemies. This is done by the AdventureMenu.
            if(mRestingTimer == (RESTING_TICKS_FADEOUT))
            {
                mAdventureMenu->ExecuteRest();
            }
            //--Five ticks later, execute the rest resolve script. This also increments the timer by 1 to prevent
            //  an infinite event loop.
            else if(mRestingTimer == (RESTING_TICKS_FADEOUT + 5) && AdventureMenu::xRestResolveScript)
            {
                mRestingTimer ++;
                LuaManager::Fetch()->ExecuteLuaFile(AdventureMenu::xRestResolveScript);
            }

            //--If the dialogue is open, run that. This also prevents the resting timer from running while it's open!
            CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
            WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
            if(rWorldDialogue->IsVisible() || rCutsceneManager->HasAnyEvents())
            {
                //--Debug.
                DebugPrint("Updating world dialogue.\n");

                //--Run the dialogue.
                rWorldDialogue->Update();

                //--Cutscenes can also update here.
                if(rCutsceneManager->HasAnyEvents())
                {
                    //--Run the update.
                    rCutsceneManager->Update();
                }

                //--Debug.
                DebugPop("AdventureLevel: Update completes, resting sequence is running.\n");
                return;
            }

            //--Run timer.
            mRestingTimer ++;
            DebugPop("AdventureLevel: Update completes, resting sequence is running.\n");
            return;
        }
        //--Resting is over, resume operations.
        else
        {
            mRestingTimer = 0;
            mIsRestSequence = false;
        }
    }

    //--Transitions take priority.
    DebugPrint("Checking transitions.\n");
    if(mIsTransitionIn || mIsTransitionOut)
    {
        //--Timer.
        mTransitionTimer ++;

        //--Debug.
        DebugPrint("Transitioning.\n");

        //--Ending case.
        if(mTransitionTimer >= TRANSITION_FADE_TICKS + TRANSITION_HOLD_TICKS)
        {
            if(mIsTransitionIn) mTransitionTimer = 0;
            mIsTransitionIn = false;

            //--Stop the update during the hold part.
            if(mTransitionTimer < TRANSITION_HOLD_TICKS)
            {
                DebugPop("AdventureLevel: Update completes, transitioning in, holding.\n");
                return;
            }
        }
        //--Counting case. Stops execution for fade-outs.
        else
        {
            if(mIsTransitionOut)
            {
                if(mTransitionTimer < TRANSITION_FADE_TICKS + TRANSITION_HOLD_TICKS) mTransitionTimer = TRANSITION_FADE_TICKS + TRANSITION_HOLD_TICKS;
                DebugPop("AdventureLevel: Update completes, transitioning out.\n");
                return;
            }
        }
    }

    //--Update the WorldDialogue, if it's visible.
    DebugPrint("Checking world dialogue.\n");
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue->IsVisible())
    {
        //--Clear field abilities before continuing with the dialogue.
        if(mExecutingFieldAbilities->GetListSize() > 0)
        {
            mFieldAbilitiesInCancelMode = true;
            return;
        }

        //--Debug.
        DebugPrint("Updating world dialogue.\n");

        //--Run the dialogue.
        rWorldDialogue->Update();

        //--Cutscenes can also update here.
        CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
        if(rCutsceneManager->HasAnyEvents())
        {
            //--Run the update.
            rCutsceneManager->Update();
        }

        //--Timer.
        if(mControlLockTimer < AL_CONTROL_LOCK_TICKS) mControlLockTimer ++;

        //--If this flag is set, parallel cutscenes will update.
        if(mRunParallelDuringDialogue) rCutsceneManager->UpdateParallelEvents();

        //--Debug.
        mAdventureMenu->Update();
        DebugPop("AdventureLevel: Update completes, dialogue sequence is running.\n");
        return;
    }

    //--Pulsing. Player cannot act.
    DebugPrint("Checking pulsing.\n");
    if(mIsPulseMode)
    {
        //--Enemies are considered as close as possible.
        xClosestEnemy = 0.0f;

        //--Run the timer.
        mPulseTimer ++;
        if(mPulseTimer >= PULSE_TICKS + 30) mIsPulseMode = false;

        //--Debug.
        DebugPop("AdventureLevel: Update completes, pulsing.\n");
        return;
    }

    //--AdventureCombat handler.
    DebugPrint("Checking combat UI.\n");
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Run the update.
    DebugPrint("Updating Combat UI.\n");
    rAdventureCombat->Update();

    //--The combat UI will stop the update based on its internal logic.
    if(!rAdventureCombat->IsStoppingWorldUpdate())
    {
    }
    //--Otherwise, it does. Stop the update here.
    else
    {
        mFieldAbilitiesInCancelMode = false;
        if(mControlLockTimer < AL_CONTROL_LOCK_TICKS) mControlLockTimer ++;
        DebugPop("AdventureLevel: Update completes, combat handled the update.\n");
        return;
    }

    //--Update the AdventureMenu. It won't do anything if it's not visible except run timers.
    DebugPrint("Updating Adventure Menu.\n");
    mAdventureMenu->Update();

    //--If the menu needs to block controls, stop here.
    if(mAdventureMenu->NeedsToBlockControls())
    {
        DebugPop("AdventureLevel: Update completes, menu handled the update.\n");
        return;
    }

    //--Check if there is a pending transition. Exits set these, but they can also be set by cutscenes or combat.
    DebugPrint("Checking transitions.\n");
    if(HandleTransition())
    {
        DebugPop("AdventureLevel: Update completes, transitioning.\n");
        return;
    }

    //--Cutscenes!
    DebugPrint("Checking cutscenes.\n");
    CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
    if(rCutsceneManager->HasAnyEvents())
    {
        //--If there are any field abilities executing, mark them for removal and wait a tick.
        if(mExecutingFieldAbilities->GetListSize() > 0)
        {
            mFieldAbilitiesInCancelMode = true;
            return;
        }

        //--If waiting for field abilities to expire, skip this.
        if(mFieldAbilitiesInCancelMode)
        {
            DebugPop("AdventureLevel: Update completes, waiting for field abilities before cutscene fires.\n");
            return;
        }

        //--Run the update.
        DebugPrint("Updating cutscenes.\n");
        rCutsceneManager->Update();

        //--Timer.
        if(mControlLockTimer < AL_CONTROL_LOCK_TICKS) mControlLockTimer ++;

        //--If this flag is set, parallel cutscenes will update.
        if(mRunParallelDuringCutscene) rCutsceneManager->UpdateParallelEvents();

        //--Finish, player controls are not respected.
        DebugPop("AdventureLevel: Update completes, cutscene handled it.\n");
        return;
    }
    //--No cutscene is playing, so unlock the camera.
    else
    {
        SetCameraLocking(false);
    }

    //--Cutscene manager always runs parallel events.
    rCutsceneManager->UpdateParallelEvents();

    //--Run autofire cases for NPCs.
    if(TilemapActor::xAutofireCooldown > 0)
    {
        TilemapActor::xAutofireCooldown --;
    }
    if(TilemapActor::xAutofireCooldown < 1)
    {
        //--Iterate across the tilemap actor list.
        SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
        RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
        while(rCheckEntity)
        {
            //--Right type? Check.
            if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
            {
                //--If the actor reports true, it handled the activation and set the flag.
                if(((TilemapActor *)rCheckEntity)->HandleAutoActivation((int)mPlayerXPositions[0], (int)mPlayerYPositions[0]))
                {
                    rEntityList->PopIterator();
                    break;
                }
            }

            //--Next.
            rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
        }
    }

    //--Control lockout: Ignore player controls.
    if(tAreControlsLocked || tAreControlsLockedByFieldAbility)
    {
        //--If controls are locked due to a cutscene or somesuch, run this timer.
        //  This takes priority over field abilities.
        if(tAreControlsLocked)
        {
            DebugPrint("Controls are locked.\n");
            if(mControlLockTimer < AL_CONTROL_LOCK_TICKS) mControlLockTimer ++;
        }
        //--If running a field ability, we still need to check against exits and triggers.
        //  If a trigger is hit, a special mode is activated.
        else
        {
            //--Get the actor in question.
            TilemapActor *rActor = LocatePlayerActor();
            if(!rActor) return;

            //--Check if the actor in question moved into any exits. If so, set the variables for those exits.
            DebugPrint("Checking exits and triggers.\n");
            if(CheckActorAgainstExits(rActor, true))
            {
                mFieldAbilitiesInCancelMode = true;
            }
            //--If that didn't go through, check the actor against triggers.
            else if(CheckActorAgainstTriggers(rActor, false))
            {
                //--Check if anything to lock controls got added, such as cutscene instructions. If so, activate cancel mode.
                if(rCutsceneManager->HasAnyEvents())
                {
                    mFieldAbilitiesInCancelMode = true;
                }
            }
        }
    }
    //--Not locked, pass instructions to the TilemapActor.
    else
    {
        //--Setup.
        ControlManager *rControlManager = ControlManager::Fetch();

        //--Timers.
        if(mControlLockTimer > 0) mControlLockTimer --;

        //--If nothing else was stealing control, then the DebugMenu can steal it now. Hah!
        DebugPrint("Checking debug menu.\n");
        if(mDebugMenu->IsVisible())
        {
            mWasMenuUpLastTick = true;
            mDebugMenu->Update();
            DebugPop("AdventureLevel: Update completes, debug menu handled it.\n");
            return;
        }

        //--Retreat timer.
        if(mRetreatTimer > 0) mRetreatTimer --;

        //--The player may press a key to open the debug menu at any time their controls are not locked.
        if(rControlManager->IsFirstPress("OpenDebug"))
        {
            //--Setup.
            const char *rCheckPassword = OptionsManager::Fetch()->GetOptionS("Patron Password");

            //--If this flag is set, open the debug menu. This is activated by the password menu.
            if(AdventureDebug::xManualActivation)
            {
                mDebugMenu->Show();
            }
            //--Otherwise, the player needs to enter the correct patron password before the game starts.
            else if(!rCheckPassword || strcasecmp(rCheckPassword, "Ares Defense Industries"))
            {
            }
            //--Correct password.
            else
            {
                mDebugMenu->Show();
            }
        }
        //--Menu keypress.
        else if(rControlManager->IsFirstPress("Cancel") && mCannotOpenMenuTimer < 1 && !mAdventureMenu->StartedHidingThisTick())
        {
            mAdventureMenu->Show();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Field Abilities menu.
        else if(rControlManager->IsFirstPress("OpenFieldAbilityMenu"))
        {
            mAdventureMenu->OpenFieldAbilities();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Field Ability 1:
        else if(rControlManager->IsFirstPress("FieldAbility0"))
        {
            ExecuteFieldAbility(0);
        }
        //--Field Ability 2:
        else if(rControlManager->IsFirstPress("FieldAbility1"))
        {
            ExecuteFieldAbility(1);
        }
        //--Field Ability 3:
        else if(rControlManager->IsFirstPress("FieldAbility2"))
        {
            ExecuteFieldAbility(2);
        }
        //--Field Ability 4:
        else if(rControlManager->IsFirstPress("FieldAbility3"))
        {
            ExecuteFieldAbility(3);
        }
        //--Field Ability 5:
        else if(rControlManager->IsFirstPress("FieldAbility4"))
        {
            ExecuteFieldAbility(4);
        }
        //--Check for entity control handling.
        else
        {
            //--Debug.
            DebugPrint("Checking actor existence.\n");

            //--Make sure the entity exists and is of the right type.
            TilemapActor *rActor = LocatePlayerActor();
            if(rActor)
            {
                //--Check whether or not the player can run.
                bool tCanRun = true;
                if(mBlockRunningToRegen) tCanRun = false;

                //--Let the player move.
                DebugPrint("Handling player controls.\n");
                bool tPlayerPressedMove = rActor->HandlePlayerControls(tCanRun);
                float tAmountMovedLastTick = rActor->GetMovementLastTick();

                //--If the player ran last tick, check for sound effects. This occurs even if no
                //  stamina is being drained. If the result of the function is NULL, no sound
                //  will be played.
                //--Note that, if there are any followers, the footstep sound plays anyway.
                if(rActor->mRanLastTick && tAmountMovedLastTick >= 1.0f)
                {
                    //--Check for a footstep sound. Flying forms have no footsteps.
                    char *rFootstepSound = rActor->GetFootstepSFX(mFollowEntityIDList->GetListSize() > 0);
                    if(rFootstepSound) AudioManager::Fetch()->PlaySound(rFootstepSound);
                }

                //--If the player ran last tick, drain stamina.
                if(rActor->mRanLastTick && tPlayerPressedMove && tAmountMovedLastTick >= 1.0f && xEntitiesDrainStamina)
                {
                    //--Decrement.
                    mPlayerStamina = mPlayerStamina - STAMINA_CONSUME_TICK;

                    //--At zero, block running.
                    if(mPlayerStamina < 1.0f)
                    {
                        mBlockRunningToRegen = true;
                    }
                }
                //--Stamina regen while stationary.
                else if(!tPlayerPressedMove && tAmountMovedLastTick < 1.0f)
                {
                    //--Regen.
                    mPlayerStamina = mPlayerStamina + STAMINA_REGEN_STANDING;

                    //--If the player's stamina is under the threshold while not running, block running until it regens.
                    if(mPlayerStamina < STAMINA_RUN_LOWER) mBlockRunningToRegen = true;
                }
                //--Stamina regen while walking.
                else
                {
                    //--Regen.
                    mPlayerStamina = mPlayerStamina + STAMINA_REGEN_TICK;

                    //--If the player's stamina is under the threshold while not running, block running until it regens.
                    if(mPlayerStamina < STAMINA_RUN_LOWER) mBlockRunningToRegen = true;
                }

                //--Stamina clamps.
                if(mPlayerStamina < 0.0f) mPlayerStamina = 0.0f;
                if(mPlayerStamina > STAMINA_MAX) mPlayerStamina = STAMINA_MAX;

                //--Field ability cooldowns.
                AdvCombat *rCombat = AdvCombat::Fetch();
                for(int i = 0; i < ADVCOMBAT_FIELD_ABILITY_SLOTS; i ++)
                {
                    FieldAbility *rAbility = rCombat->GetFieldAbility(i);
                    if(!rAbility) continue;

                    int tCooldown = rAbility->GetCooldown();
                    if(tCooldown > 0) rAbility->SetCooldownCur(tCooldown - 1);
                }

                //--Remove the blocking flag if the player's stamina goes over this constant.
                if(mBlockRunningToRegen && mPlayerStamina >= STAMINA_RUN_ALLOW) mBlockRunningToRegen = false;

                //--If the position is different from the 0th element on the list, shift them down one.
                if(tPlayerPressedMove)
                {
                    MovePartyMembers();
                }
                //--Player did not move. All followers stop movement.
                else
                {
                    //--Iterate.
                    int i = 0;
                    TilemapActor *rFollower = LocateFollowingActor(i);
                    while(rFollower)
                    {
                        //--Animation.
                        rFollower->StopMoving();

                        //--Next.
                        i ++;
                        rFollower = LocateFollowingActor(i);
                    }
                }

                //--Check if the actor is in any of the invisible zones.
                CheckActorAgainstInvisZones(rActor);

                //--Check if the actor in question moved into any exits. If so, set the variables for those exits. This is
                //  not an emulation.
                DebugPrint("Checking exits and triggers.\n");
                if(CheckActorAgainstExits(rActor, false))
                {
                }
                //--If that didn't go through, check the actor against triggers. This is not an emulation, the trigger
                //  should execute.
                else if(CheckActorAgainstTriggers(rActor, false))
                {
                }
            }
        }
    }

    //--Update entity lighting positions after entities have moved.
    if(mAreLightsActive) UpdateEntityLightPositions();

    //--Decrement this timer.
    if(mCannotOpenMenuTimer > 0) mCannotOpenMenuTimer --;

    //--At the end of a normal control tick, reset this flag. When Enemies go to update, they can flip
    //  it back to true. It should be flipped AFTER the player uses it.
    xEntitiesDrainStamina = false;

    //--Debug.
    DebugPop("AdventureLevel: Update completes at end of function.\n");
}
void AdventureLevel::ExecuteFieldAbility(int pIndex)
{
    //--Fetch the ability.
    AdvCombat *rCombat = AdvCombat::Fetch();
    FieldAbility *rAbility = rCombat->GetFieldAbility(pIndex);
    if(!rAbility) return;

    //--Ability is on cooldown.
    if(rAbility->GetCooldown() > 0)
    {
        rAbility->Execute(FIELDABILITY_EXEC_RUNWHILECOOLING);
        return;
    }

    //--Create.
    FieldAbilityPack *nPackage = (FieldAbilityPack *)starmemoryalloc(sizeof(FieldAbilityPack));
    nPackage->Initialize();
    nPackage->rParentAbility = rAbility;

    //--Execute.
    rExecutingPackage = nPackage;
    rAbility->ActivateCooldown();
    rAbility->Execute(FIELDABILITY_EXEC_RUN);

    //--Clean up.
    nPackage->mTimer = 1;
    rExecutingPackage = NULL;

    //--If the package was flagged for termination, don't save it!
    if(!nPackage->mIsExpired)
    {
        mExecutingFieldAbilities->AddElement("X", nPackage, &FreeThis);
    }
    else
    {
        free(nPackage);
    }
}
void AdventureLevel::MovePartyMembers()
{
    //--All party members move to follow the player's position. This causes the party to organically follow
    //  behind the party leader. It can also be used in a cutscene.
    TilemapActor *rActor = LocatePlayerActor();
    if(!rActor) return;

    //--Variables.
    float tPlayerX = rActor->GetWorldX();
    float tPlayerY = rActor->GetWorldY();

    //--Downshift.
    for(int i = PLAYER_MOVE_STORAGE_TOTAL - 1; i >= 1; i --)
    {
        mPlayerXPositions[i] = mPlayerXPositions[i-1];
        mPlayerYPositions[i] = mPlayerYPositions[i-1];
        mPlayerFacings[i]    = mPlayerFacings[i-1];
        mPlayerTimers[i]     = mPlayerTimers[i-1];
        mPlayerDepths[i]     = mPlayerDepths[i-1];
        mPlayerRunnings[i]   = mPlayerRunnings[i-1];
    }

    //--Zeroth becomes the current position.
    mPlayerXPositions[0] = tPlayerX;// + rActor->GetRemainderX();
    mPlayerYPositions[0] = tPlayerY;// + rActor->GetRemainderY();
    mPlayerFacings[0] = rActor->GetFacing();
    mPlayerTimers[0] = rActor->GetMoveTimer();
    mPlayerDepths[0] = rActor->GetCollisionDepth();
    mPlayerRunnings[0] = rActor->mRanLastTick;

    //--Scan the collision depths of all actors. We use the highest one later on.
    int tHighestDepth = rActor->GetCollisionDepth();
    if(LocateFollowingActor(0) && mPlayerDepths[12] > tHighestDepth) tHighestDepth = mPlayerDepths[12];
    if(LocateFollowingActor(1) && mPlayerDepths[24] > tHighestDepth) tHighestDepth = mPlayerDepths[24];
    if(LocateFollowingActor(2) && mPlayerDepths[36] > tHighestDepth) tHighestDepth = mPlayerDepths[36];

    //--Player character's depth override.
    if(tHighestDepth != rActor->GetCollisionDepth())
    {
        float tOffsetY = mPlayerYPositions[0];
        float tZPosition = DEPTH_MIDGROUND + ((mPlayerYPositions[0] + tOffsetY) / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
        tZPosition = tZPosition + (DEPTH_PER_TILE * (float)tHighestDepth * 2.0f);
        rActor->SetOverrideDepth(tZPosition);
    }
    //--Normal case.
    else
    {
        rActor->SetOverrideDepth(-2.0f);
    }

    //--Order followers to move to the provided positions as necessary.
    //fprintf(stderr, "Calibrating follower offset.\n");
    int i = 0;
    TilemapActor *rFollower = LocateFollowingActor(i);
    while(rFollower)
    {
        //--Compute the spacing cap.
        int tUsePos = PLAYER_MOVE_STORAGE_TOTAL -1;
        if(i == 0) tUsePos = 12;
        if(i == 1) tUsePos = 24;
        if(i == 2) tUsePos = 36;

        //--Offset.
        float tOffsetX = mPlayerXPositions[tUsePos] - mPlayerXPositions[0];
        float tOffsetY = mPlayerYPositions[tUsePos] - mPlayerYPositions[0];
        CalibrateChaseOffsets(i, mPlayerRunnings[tUsePos], tOffsetX, tOffsetY);
        //fprintf(stderr, " %i - %5.2f %5.2f\n", i, tOffsetX, tOffsetY);

        //--Order.
        rFollower->SetPositionByPixel(mPlayerXPositions[0] + tOffsetX, mPlayerYPositions[0] + tOffsetY);
        rFollower->SetFacing(mPlayerFacings[tUsePos]);
        rFollower->ForceMoving(mPlayerTimers[tUsePos]);
        rFollower->SetCollisionDepth(mPlayerDepths[tUsePos]);
        rFollower->mRanLastTick = mPlayerRunnings[tUsePos];

        //--Compute the relative depth. Followers use the current depth of the leader, not their own collision depth, when computing
        //  depth. This makes them not re-order when on steps.
        float tZPosition = DEPTH_MIDGROUND + ((mPlayerYPositions[0] + tOffsetY) / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
        tZPosition = tZPosition + (DEPTH_PER_TILE * (float)tHighestDepth * 2.0f);
        rFollower->SetOverrideDepth(tZPosition);

        //--Next.
        i ++;
        rFollower = LocateFollowingActor(i);
    }
}
bool AdventureLevel::AutofoldPartyMembers()
{
    //--Orders all party members to move towards the party leader's position at walking speed. Returns
    //  true if all party members did not move (and folding is done), false if any one did move.
    TilemapActor *rActor = LocatePlayerActor();
    if(!rActor) return true;

    //--Variables.
    bool tEveryoneDoneMoving = true;
    float tPlayerX = rActor->GetWorldX();
    float tPlayerY = rActor->GetWorldY();

    //--Order followers to move to the provided positions as necessary.
    //fprintf(stderr, "Calibrating follower offset.\n");
    int i = 0;
    TilemapActor *rFollower = LocateFollowingActor(i);
    while(rFollower)
    {
        //--Order follower to move to the player's position.
        bool tAtLocation = rFollower->HandleMoveTo(tPlayerX, tPlayerY, -1.0f);
        if(!tAtLocation) tEveryoneDoneMoving = false;

        //--Next.
        i ++;
        rFollower = LocateFollowingActor(i);
    }

    //--Return whether or not everyone was done moving.
    return tEveryoneDoneMoving;
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void AdventureLevel::AddToRenderList(SugarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void AdventureLevel::Render()
{
    //--[Setup]
    //--Flip this flag.
    MapManager::xHasRenderedMenus = true;
    if(!Images.mIsReady) return;

    //--Error check.
    if(mCurrentScale < 0.01f) return;

    //--[Minigame]
    //--If the running minigame exists and is fully opaque, render it and stop here.
    if(mRunningMinigame && mRunningMinigame->IsFullyOpaque())
    {
        mRunningMinigame->Render();
        return;
    }

    //--Debug.
    DebugPush(true, "AdventureLevel: Render begins.\n");

    //--Screen shake.
    float cShakeXOff = 0.0f;
    float cShakeYOff = 0.0f;
    if(mScreenShakeTimer > 0)
    {
        //--Rolls.
        float cRadius = 0.7f;
        float cAngle = (float)(rand() % 360) / 360.0f * 3.1415925f * 2.0f;
        cShakeXOff = cosf(cAngle) * cRadius;
        cShakeYOff = sinf(cAngle) * cRadius;
    }

    //--Water rendering. Requires OpenGL 3.0 or higher, since it uses framebuffers.
    if(mIsUnderwaterMode)
    {
        //--Order the program to target the framebuffer.
        sglBindFramebuffer(GL_FRAMEBUFFER, mUnderwaterFramebuffer);
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

        //--Error check.
        if(sglCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            fprintf(stderr, "Error! Could not set up the framebuffers.\n");
            sglBindFramebuffer(GL_FRAMEBUFFER, 0);
        }
        //--Proceed.
        else
        {
            glViewport(0, 0, VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
        }
    }

    //--[Background Rendering]
    //--Only renders if a background exists. Most levels don't use this!
    if(rBackgroundImg)
    {
        //--Compute position.
        float tRenderX = mBackgroundStartX + (mCameraDimensions.mLft * mBackgroundScrollX);
        float tRenderY = mBackgroundStartY + (mCameraDimensions.mTop * mBackgroundScrollY);

        //--Render.
        glTranslatef(0.0f, 0.0f, -1.0f);
        rBackgroundImg->Draw(tRenderX, tRenderY);

        //--If the render position plus the width is less than the virtual canvas, tile it horizontally.
        if(tRenderX + rBackgroundImg->GetTrueWidth() < VIRTUAL_CANVAS_X)
        {
            rBackgroundImg->Draw(tRenderX + rBackgroundImg->GetTrueWidth(), tRenderY);
        }

        //--If the render position plus the height is less than the virtual canvas, tile it vertically.
        if(tRenderY + rBackgroundImg->GetTrueHeight() < VIRTUAL_CANVAS_Y)
        {
            rBackgroundImg->Draw(tRenderX, tRenderY + rBackgroundImg->GetTrueHeight());
        }

        //--Both cases are true:
        if(tRenderX + rBackgroundImg->GetTrueWidth() < VIRTUAL_CANVAS_X && tRenderY + rBackgroundImg->GetTrueHeight() < VIRTUAL_CANVAS_Y)
        {
            rBackgroundImg->Draw(tRenderX + rBackgroundImg->GetTrueWidth(), tRenderY + rBackgroundImg->GetTrueHeight());
        }

        //--Clean.
        glTranslatef(0.0f, 0.0f, 1.0f);
    }

    //--[Underlays]
    //--Renders over the background and under the world.
    glTranslatef(0.0f, 0.0f, -1.0f);
    for(int i = 0; i < mUnderlaysTotal; i ++)
    {
        //--Skip rendering if the alpha is 0.0f, or no image exists.
        if(!mUnderlayPacks[i].rImage || mUnderlayPacks[i].mAlphaCurrent <= 0.0f) continue;

        //--Set the alpha value.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, mUnderlayPacks[i].mAlphaCurrent);

        //--Fast-access variables.
        float cTexWid = mUnderlayPacks[i].rImage->GetTrueWidth();
        float cTexHei = mUnderlayPacks[i].rImage->GetTrueHeight();

        //--Determine the starting X/Y positions.
        float cStartX = mUnderlayPacks[i].mOffsetBaseX + (mCameraDimensions.mLft * mUnderlayPacks[i].mScrollFactorX);
        float cStartY = mUnderlayPacks[i].mOffsetBaseY + (mCameraDimensions.mTop * mUnderlayPacks[i].mScrollFactorY);
        float cTxL = (cStartX / cTexWid);
        float cTxT = (cStartY / cTexHei);
        float cTxR = (cStartX / cTexWid) + (CANX / cTexWid * mUnderlayPacks[i].mScaleFactor);
        float cTxB = (cStartY / cTexHei) - (CANY / cTexHei * mUnderlayPacks[i].mScaleFactor);

        //--Bind, and render.
        mUnderlayPacks[i].rImage->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxB); glVertex2f(0.0f, 0.0f);
            glTexCoord2f(cTxR, cTxB); glVertex2f(CANX, 0.0f);
            glTexCoord2f(cTxR, cTxT); glVertex2f(CANX, CANY);
            glTexCoord2f(cTxL, cTxT); glVertex2f(0.0f, CANY);
        glEnd();
    }
    glTranslatef(0.0f, 0.0f, 1.0f);

    //--Scaling.
    glTranslatef(cShakeXOff, cShakeYOff, 0.0f);
    glScalef(mCurrentScale, mCurrentScale, 1.0f);

    //--[Camera Handling]
    //--If the Camera is not currently locked, it will attempt to focus on the player's character.
    //  The camera usually gets locked as a result of cutscenes, but they can modify the locking state
    //  at any time with SetCameraLocking().
    //--The camera will automatically unlock if no cutscene is active.
    DebugPrint("Updating camera position.\n");
    UpdateCameraPosition();

    //--Camera.
    glTranslatef(-mCameraDimensions.mLft, -mCameraDimensions.mTop, 0.0f);

    //--[Layer Rendering]
    //--Compute which subsection should be rendered. Clamping is done by the TileLayers.
    int tLft = (mCameraDimensions.mLft / TileLayer::cxSizePerTile) - 1;
    int tTop = (mCameraDimensions.mTop / TileLayer::cxSizePerTile) - 1;
    int tRgt = (mCameraDimensions.mRgt / TileLayer::cxSizePerTile) + 1;
    int tBot = (mCameraDimensions.mBot / TileLayer::cxSizePerTile) + 1;

    //--Patrol node debug.
    if(false)
    {
        RenderPatrolNodes();
    }

    //--Render the fade if it's flagged to be over the tiles but under the entities.
    DebugPrint("Render script fade.\n");
    RenderScriptFade(SCRIPT_FADE_UNDER_CHARACTERS);

    //--Activate the lighting shader.
    if(mAreLightsActive) RenderLights();

    //--Sub-functions. These render various objects or areas.
    if(Images.mIsReady)
    {
        //--Basic rendering.
        RenderEntities(false);
        RenderTilemap(tLft, tTop, tRgt, tBot);
        RenderDoors();
        RenderClimbables();
        RenderChests();
        RenderExits();
        RenderSwitches();
        RenderSavePoints();
        RenderViewcones();
        RenderPerMapAnimations();
        RenderEntityUILayer();
    }

    //--Un-set underwater effect rendering.
    if(mIsUnderwaterMode)
    {
        sglBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(DisplayManager::xHorizontalOffset, DisplayManager::xVerticalOffset, DisplayManager::xViewportW, DisplayManager::xViewportH);
    }

    //--Deactivate the lighting/underwater shader.
    DisplayManager::Fetch()->ActivateProgram(NULL);

    //--Clean.
    DebugPrint("Cleaning.\n");
    TilemapActor::xAllowRender = false;
    glTranslatef(mCameraDimensions.mLft, mCameraDimensions.mTop, 0.0f);

    //--Falling rocks. Ignores the camera position when rendering.
    FallingRockPack *rRock = (FallingRockPack *)mFallingRockList->SetToHeadAndReturn();
    while(rRock)
    {
        //--Check image.
        if(rRock->rImage)
        {
            rRock->rImage->Draw(rRock->mX, rRock->mY);
        }

        //--Next.
        rRock = (FallingRockPack *)mFallingRockList->IncrementAndGetRandomPointerEntry();
    }

    //--If underwater, render the framebuffer.
    if(mIsUnderwaterMode)
    {
        //--Place the texture in slot 1. The shader looks for it there.
        sglActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, mUnderwaterTexture);
        sglActiveTexture(GL_TEXTURE0);

        //--Activate shader.
        DisplayManager::Fetch()->ActivateProgram("Underwater");
        GLint cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
        ShaderUniform1i(cShaderHandle, Global::Shared()->gTicksElapsed, "uTimer");

        //--Render.
        glScalef(1.0f / mCurrentScale, 1.0f / mCurrentScale, 1.0f);
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 1.0f); glVertex2f(            0.0f,             0.0f);
            glTexCoord2f(1.0f, 1.0f); glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glTexCoord2f(0.0f, 0.0f); glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();
        glScalef(mCurrentScale, mCurrentScale, 1.0f);

        //--Clean.
        DisplayManager::Fetch()->ActivateProgram(NULL);

        //--Remove the Texture1 case.
        sglActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        sglActiveTexture(GL_TEXTURE0);
    }

    //--Clean the mixer.
    StarlightColor::ClearMixer();
    glTranslatef(-mCameraDimensions.mLft, -mCameraDimensions.mTop, 0.0f);
        RenderEntities(true);
    glTranslatef(mCameraDimensions.mLft, mCameraDimensions.mTop, 0.0f);
    TilemapActor::xAllowRender = false;

    //--Undo scaling. This is done *after* the overlay renders.
    glScalef(1.0f / mCurrentScale, 1.0f / mCurrentScale, 1.0f);
    glTranslatef(-cShakeXOff, -cShakeYOff, 0.0f);

    //--[Overlays]
    //--If set, render an overlay over the world but under the UI.
    for(int i = 0; i < mOverlaysTotal; i ++)
    {
        //--Skip rendering if the alpha is 0.0f, or no image exists.
        if(!mOverlayPacks[i].rImage || mOverlayPacks[i].mAlphaCurrent <= 0.0f) continue;

        //--Set the alpha value.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, mOverlayPacks[i].mAlphaCurrent);

        //--Fast-access variables.
        float cTexWid = mOverlayPacks[i].rImage->GetTrueWidth();
        float cTexHei = mOverlayPacks[i].rImage->GetTrueHeight();

        //--Determine the starting X/Y positions.
        float cCenterX = (mCameraDimensions.mLft + mCameraDimensions.mRgt) / 2.0f;
        float cCenterY = (mCameraDimensions.mTop + mCameraDimensions.mBot) / 2.0f;
        float cStartX = mOverlayPacks[i].mOffsetBaseX + (cCenterX * mOverlayPacks[i].mScrollFactorX);
        float cStartY = mOverlayPacks[i].mOffsetBaseY + (cCenterY * mOverlayPacks[i].mScrollFactorY);
        float cTxCX = (cStartX / cTexWid);
        float cTxCY = (cStartY / cTexHei);
        float cTxL = cTxCX - (CANX / cTexWid * mOverlayPacks[i].mScaleFactor * 0.50f / mCurrentScale);
        float cTxT = cTxCY + (CANY / cTexHei * mOverlayPacks[i].mScaleFactor * 0.50f / mCurrentScale);
        float cTxR = cTxCX + (CANX / cTexWid * mOverlayPacks[i].mScaleFactor * 0.50f / mCurrentScale);
        float cTxB = cTxCY - (CANY / cTexHei * mOverlayPacks[i].mScaleFactor * 0.50f / mCurrentScale);

        //--Bind, and render.
        mOverlayPacks[i].rImage->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxB); glVertex2f(0, 0);
            glTexCoord2f(cTxR, cTxB); glVertex2f(CANX*1.0f, 0);
            glTexCoord2f(cTxR, cTxT); glVertex2f(CANX*1.0f, CANY*1.0f);
            glTexCoord2f(cTxL, cTxT); glVertex2f(0, CANY*1.0f);
        glEnd();
    }
    DebugPrint("Overlays done.\n");

    //--[GUI]
    //--Stamina bar setup. Only renders if the vis timer is above zero.
    RenderWorldUI();
    DebugPrint("Stamina bar done.\n");

    //--[Objectives]
    //--Doesn't render while control is locked out.
    if(mObjectivesList->GetListSize() > 0 && mControlLockTimer < 1)
    {
        float tYPos = 27.0f;
        bool *rObjectivePtr = (bool *)mObjectivesList->PushIterator();
        while(rObjectivePtr)
        {
            //--Name of objective:
            const char *rObjectiveName = mObjectivesList->GetIteratorName();

            //--Render it.
            if(!(*rObjectivePtr))
            {
                Images.Data.rUIFont->DrawText(0.0f, tYPos, 1.0f, 0, rObjectiveName);
            }
            else
            {
                StarlightColor::SetMixer(0.0f, 1.0f, 0.0f, 1.0f);
                Images.Data.rUIFont->DrawTextArgs(0.0f, tYPos, 1.0f, 0, "%s: Complete", rObjectiveName);
                StarlightColor::ClearMixer();
            }

            //--Next.
            tYPos = tYPos + Images.Data.rUIFont->GetTextHeight();
            rObjectivePtr = (bool *)mObjectivesList->AutoIterate();
        }
    }
    DebugPrint("Objectives done.\n");

    //--[Borders]
    //--These render during cutscenes or when the player's control is locked.
    if(mControlLockTimer > 0)
    {
        //--Calculations.
        float cRgt = VIRTUAL_CANVAS_X;
        float tPercent = mControlLockTimer / (float)AL_CONTROL_LOCK_TICKS;

        //--Mixer.
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, tPercent);
        glDisable(GL_TEXTURE_2D);

        //--Top border.
        float cTop = AL_CONTROL_LOCK_PIXELS * tPercent;
        glBegin(GL_QUADS);
            glVertex2f(0.0f, 0.0f);
            glVertex2f(cRgt, 0.0f);
            glVertex2f(cRgt, cTop);
            glVertex2f(0.0f, cTop);
        glEnd();

        //--Bottom border.
        cTop = VIRTUAL_CANVAS_Y - (AL_CONTROL_LOCK_PIXELS * tPercent);
        float cBot = VIRTUAL_CANVAS_Y;
        glBegin(GL_QUADS);
            glVertex2f(0.0f, cBot);
            glVertex2f(cRgt, cBot);
            glVertex2f(cRgt, cTop);
            glVertex2f(0.0f, cTop);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        StarlightColor::ClearMixer();
    }

    //--[Last Seed]
    //--If this is a randomly-generated level, render the seed value and floor information.
    if(mIsRandomLevel)
    {
        //--Rendering position is right below the stamina bar.
        float cXPos = 10.0f;
        float cYPos = Images.Data.rStaminaBarMiddle->GetHeight() + 8.0f;

        //--Render the floor number.
        int tFloorNumber = 0;
        SysVar *rFloorNumberVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/Chapter5/Scenes/iCurrentMinesFloor");
        if(rFloorNumberVar) tFloorNumber = (int)rFloorNumberVar->mNumeric;
        Images.Data.rUIFont->DrawTextArgs(cXPos, cYPos, 0, 1.0f, "Floor %i", tFloorNumber);
        cYPos = cYPos + Images.Data.rUIFont->GetTextHeight();

        //--Render the seed number.
        Images.Data.rUIFont->DrawTextArgs(cXPos, cYPos, 0, 1.0f, "Seed: %i", AdventureLevelGenerator::xLastSeed);
    }
    DebugPrint("Random Seed done.\n");

    //--Render the fade if it's flagged to be over the characters but under the GUI.
    DebugPrint("Render fade script under UI.\n");
    RenderScriptFade(SCRIPT_FADE_UNDER_UI);

    //--[Major UI Objects]
    //--AdventureCombat handler. It always calls the render, but may render nothing if not active.
    DebugPrint("Render combat UI.\n");
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    rAdventureCombat->Render();

    //--Major animations. These render under the dialogue and also darken the whole screen.
    if(mIsMajorAnimationMode)
    {
        //--Darken the screen.
        StarlightColor cDarkenCol = StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.75f);
        SugarBitmap::DrawFullColor(cDarkenCol);

        //--Make sure the major animation actually exists.
        PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->GetElementByName(mMajorAnimationName);
        if(!rAnimation || !rAnimation->mrImagePtrs || !rAnimation->mrImagePtrs[0])
        {
            //--Error goes here when detecting such things. Nominally this is logically impossible.
        }
        //--Rendering.
        else
        {
            //--Determine the centering point. Animations are expected to have all frames of constant sizes.
            //--The size can be modified by cWorldScale if the animation needs to be scaled up.
            float cSizePerFrameX = rAnimation->mrImagePtrs[0]->GetTrueWidth();
            float cSizePerFrameY = rAnimation->mrImagePtrs[0]->GetTrueWidth();

            //float cCalibration = 555.0f;
            float cWorldScale = 1.0f;
            float cRenderX = (VIRTUAL_CANVAS_X - (cSizePerFrameX * cWorldScale)) / 2.0f;
            float cRenderY = (VIRTUAL_CANVAS_Y - (cSizePerFrameY * cWorldScale)) / 2.0f;

            //--Determine which frame we're playing.
            float tFrame = rAnimation->mTimer / rAnimation->mTicksPerFrame;
            if(tFrame >= rAnimation->mTotalFrames) tFrame = rAnimation->mTotalFrames - 1;
            if(tFrame < 1) tFrame = 0;

            //--Convert to an integer so frames don't scroll.
            int tIntegerFrame = (int)tFrame;

            //--Compute the coordinates for this frame.
            rAnimation->mrImagePtrs[tIntegerFrame]->Draw(cRenderX, cRenderY);
        }
    }

    //--Render WorldDialogue GUI if it's visible.
    DebugPrint("Render dialogue.\n");
    WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
    if(rWorldDialogue->IsVisible() && !mIsRestSequence && !rAdventureCombat->IsActive())
    {
        rWorldDialogue->Render();
    }

    //--Adventure Menu, if it's visible.
    DebugPrint("Render menu.\n");
    if(mAdventureMenu->NeedsToRender()) mAdventureMenu->Render();

    //--Debug menu renders over everything else if it is visible.
    DebugPrint("Render debug menu.\n");
    if(mDebugMenu->IsVisible())
    {
        mDebugMenu->Render();
    }
    //--If the DebugMenu was up the previous tick, render an overlay. This just prevents an annoying discontinuity
    //  when the menu is hidden.
    else if(mWasMenuUpLastTick)
    {
        AdventureDebug::RenderOverlay();
    }

    //--Extra debug stuff.
    #ifdef ALLOW_SPEED_TUNING
    {
        RootEntity *rActor = LocatePlayerActor();
        SugarFont *rFont = rDataLibrary->GetFont("Adventure Debug Font");
        if(!rFont || !rActor) return;
        rFont->DrawTextArgs(0.0f,  0.0f, 0, 1.0f, "Walk Speed:  %5.3f. O decrease, P increase.", rActor->GetMoveSpeed());
        rFont->DrawTextArgs(0.0f, 20.0f, 0, 1.0f, "Frame Speed: %5.3f. K decrease, L increase.", rActor->GetFrameSpeed());
    }
    #endif

    //--Overlay.
    DebugPrint("Render transition.\n");
    if(mIsTransitionIn || mIsTransitionOut || mTransitionHoldTimer > -1)
    {
        //--Setup.
        float tAlpha = 0.0f;
        glDisable(GL_TEXTURE_2D);

        //--Compute alpha.
        if(mTransitionHoldTimer == -1)
        {
            //--Transitioning in uses a different alpha for the hold duration. Updates stop during this part as well.
            if(mIsTransitionIn)
            {
                //--Full alpha, ignore controls.
                if(mTransitionTimer < TRANSITION_HOLD_TICKS)
                {
                    tAlpha = 1.0f;
                }
                //--Fade alpha, controls restored.
                else
                {
                    tAlpha = 1.0f - ((mTransitionTimer - TRANSITION_HOLD_TICKS) / (float)TRANSITION_FADE_TICKS);
                }
            }
            //--Transition out.
            else
            {
                tAlpha = 0.0f;//mTransitionTimer / (float)TRANSITION_FADE_TICKS;
            }
        }
        //--Transition hold timer uses different math.
        else if(mTransitionHoldTimer > 0)
        {
            tAlpha = 1.0f - (mTransitionHoldTimer / (float)TRANSITION_HOLD);
        }
        //--At exactly zero, alpha is 1 on the hold timer.
        else if(mTransitionHoldTimer == 0)
        {
            tAlpha = 1.0f;
        }

        //--Render.
        glColor4f(0.0f, 0.0f, 0.0f, tAlpha);
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
    }

    //--Enemy pulse transition: Fades the screen and shows a spinning animation.
    DebugPrint("Render pulse mode.\n");
    if(mIsPulseMode && PULSE_TICKS != 0)
    {
        //--Setup. Compute color. It becomes green if the player got an initiative bonus.
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.0f, 0.0f, 0.0f, 0.75f);
        if(rAdventureCombat->DoesPlayerHaveInitiative())
        {
            float cPercent = (mPulseTimer - 15) / (float)(PULSE_TICKS - 30);
            float cGreenliness = 0.5f * (1.0f - cPercent);
            glColor4f(0.0f, cGreenliness, cGreenliness - 0.30f, 0.75f);
        }

        //--Central positioning.
        glTranslatef(VIRTUAL_CANVAS_X * 0.5f, VIRTUAL_CANVAS_Y * 0.5f, 0.0f);

        //--Angle. Increases slowly over the run of the pulse.
        float cStartAngle = (mPulseTimer / ((float)PULSE_TICKS * 1.5f)) * 3.1415926f * 2.0f;
        if(mPulseTimer > 30) cStartAngle = cStartAngle + (((mPulseTimer-30) / ((float)PULSE_TICKS * 0.05f)) * 3.1415926f * 2.0f);

        //--There are ten triangles, each rotating at 36 degrees to one another.
        float tPercentage = EasingFunction::QuadraticIn(mPulseTimer, PULSE_TICKS);
        float cRadius = (tPercentage * (VIRTUAL_CANVAS_X * 1.65f)) + 15.0f;
        glBegin(GL_TRIANGLES);
        for(int i = 0; i < 10; i ++)
        {
            //--Get the angle, compute the position.
            float tAngleA = cStartAngle + ((36.0f * TORADIAN) * (float)i);
            float tAngleB = cStartAngle + ((36.0f * TORADIAN) * ((float)i + 1.001000f));

            //--Render.
            glVertex2f(0.0f,                           0.0f);
            glVertex2f(cosf(tAngleA) * cRadius,        sinf(tAngleA) * cRadius);
            glVertex2f(cosf(tAngleB) * cRadius * 0.5f, sinf(tAngleB) * cRadius * 0.5f);
        }
        glEnd();

        //--Clean.
        glTranslatef(VIRTUAL_CANVAS_X * -0.5f, VIRTUAL_CANVAS_Y * -0.5f, 0.0f);
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
    }

    //--Resting overlay.
    DebugPrint("Render rest sequence.\n");
    if(mIsRestSequence)
    {
        //--Setup.
        float tAlpha = 0.0f;

        //--Fade out. Alpha increases.
        if(mRestingTimer < RESTING_TICKS_FADEOUT)
        {
            tAlpha = mRestingTimer / (float)RESTING_TICKS_FADEOUT;
        }
        //--Hold. Alpha stays at 1.0f.
        else if(mRestingTimer < RESTING_TICKS_FADEOUT + RESTING_TICKS_HOLD)
        {
            tAlpha = 1.0f;
        }
        //--Fade in. Alpha decreases.
        else
        {
            tAlpha = 1.0f - ((mRestingTimer - RESTING_TICKS_FADEOUT - RESTING_TICKS_HOLD) / (float)RESTING_TICKS_FADEIN);
        }

        //--Setup.
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.0f, 0.0f, 0.0f, tAlpha);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);

        //--During the rest sequence, the dialogue can render above the fadeout. This is the only thing that does this!
        if(rWorldDialogue->IsVisible())
        {
            rWorldDialogue->Render();
        }
    }

    //--Render the fade if it's flagged to be over the GUI. It will render over everything, including other transitions.
    DebugPrint("Render script fade over UI.\n");
    RenderScriptFade(SCRIPT_FADE_OVER_UI);

    //--Order all Ray Collision Engines to render for debug.
    if(true)
    {
        for(int i = 0; i < mRayCollisionEnginesTotal; i ++)
        {
            mRayCollisionEngines[i]->RenderLines(-mCameraDimensions.mLft, -mCameraDimensions.mTop, GetCameraScale());
        }
    }

    //--Debug.
    DebugPop("AdventureLevel: Render completes at end of function.\n");

    //--[Minigame]
    //--If the minigame exists and is not fully opaque, render it here.
    if(mRunningMinigame && !mRunningMinigame->IsFullyOpaque())
    {
        mRunningMinigame->Render();
    }
    //--KPop minigame.
    else if(mKPopDanceGame)
    {
        mKPopDanceGame->Render();
    }
}

//======================================= Pointer Routing =========================================
AdventureMenu *AdventureLevel::GetMenu()
{
    return mAdventureMenu;
}
AdventureDebug *AdventureLevel::GetDebugMenu()
{
    return mDebugMenu;
}
KPopDanceGame *AdventureLevel::GetDanceMinigame()
{
    return mKPopDanceGame;
}
FieldAbilityPack *AdventureLevel::GetActiveFieldAbilityPack()
{
    return rExecutingPackage;
}

//====================================== Static Functions =========================================
AdventureLevel *AdventureLevel::Fetch()
{
    RootLevel *rCheckLevel = MapManager::Fetch()->GetActiveLevel();
    if(!rCheckLevel || !rCheckLevel->IsOfType(POINTER_TYPE_ADVENTURELEVEL)) return NULL;
    return (AdventureLevel *)rCheckLevel;
}
void AdventureLevel::SetMusic(const char *pName, bool pSlowly)
{
    //--Sets the music which should be playing. If the music changes then we handle fading here.
    //  Pass NULL to stop playing music.
    if(xLevelMusic)
    {
        //--Music activated? Save it.
        if(pName && strcasecmp(pName, "Null"))
        {
            //--If the music is the same as last time, do nothing.
            if(!strcasecmp(pName, xLevelMusic))
            {

            }
            //--Layered music? Activate that, crossfade the current music.
            else if(!strncasecmp(pName, "LAYER|", 6))
            {
                //--In all cases, store the name.
                ResetString(xLevelMusic, &pName[6]);

                //--Crossfade old music if present.
                AudioManager::Fetch()->FadeMusic(-30);

                //--If layered music was already playing...
                if(xIsLayeringMusic)
                {

                }
                //--Layered music is not playing. Activate it.
                else
                {
                    ActivateMusicLayering();
                }
            }
            //--Otherwise, crossfade.
            else
            {
                //--Turn off layering if it was on.
                DeactivateMusicLayering();

                //--Fade the previous music out.
                if(!pSlowly)
                {
                    AudioManager::Fetch()->FadeMusic(-30);
                }
                //--Fade it out over 3 seconds. Used for cutscenes.
                else
                {
                    AudioManager::Fetch()->FadeMusic(-180);
                }

                //--Set the name, play the music and fade it in.
                ResetString(xLevelMusic, pName);
                AudioManager::Fetch()->PlayMusic(xLevelMusic);
            }
        }
        //--Passed NULL while music was playing. Fade the music out.
        else
        {
            //--Fade the previous music out.
            if(!pSlowly)
            {
                AudioManager::Fetch()->FadeMusic(-30);
            }
            //--Fade it out over 3 seconds. Used for cutscenes.
            else
            {
                AudioManager::Fetch()->FadeMusic(-180);
            }

            //--Flags.
            DeactivateMusicLayering();
            ResetString(xLevelMusic, NULL);
        }
    }
    //--No music is playing.
    else
    {
        //--Music activated? Save it.
        if(pName && strcasecmp(pName, "Null"))
        {
            //--Layered music?
            if(!strncasecmp(pName, "LAYER|", 6))
            {
                ResetString(xLevelMusic, &pName[6]);
                ActivateMusicLayering();
            }
            //--Regular music.
            else
            {
                ResetString(xLevelMusic, pName);
                DeactivateMusicLayering();
                AudioManager::Fetch()->PlayMusic(xLevelMusic);
            }
        }
        //--Passed null, do nothing.
        else
        {
            DeactivateMusicLayering();
            ResetString(xLevelMusic, NULL);
        }
    }
}
