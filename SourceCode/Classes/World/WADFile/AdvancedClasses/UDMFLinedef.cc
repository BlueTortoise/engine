//--Base
#include "UDMFLinedef.h"

//--Classes
//--CoreClasses
//--Definitions
#include "WADFileMacros.h"
#include "UDMFStructures.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//=========================================== System ==============================================
void UDMFLinedef::Initialize()
{
    //--[UDMFLinedef]
    //--System
    mID = 0;
    mIsVerified = false;

    //--World Properties
    mVertex0 = -1;
    mVertex1 = -1;
    mSidedefL = -1;
    mSidedefR = -1;

    //--Texture Properties
    mIsLowerUnpegged = false;
    mIsUpperUnpegged = false;

    //--Flags
    mSpecialFlag = LINE_SPECIAL_NONE;

    //--3D Floor Flags
    mHas3DFloor = false;
    m3DFloorTag = -1;
    m3DFloorType = 0;
    m3DFloorOpacity = 1.0f;

    //--Slope Tags
    mPlaneAlignFloor = PLANE_ALIGN_NONE;
    mPlaneAlignCeiling = PLANE_ALIGN_NONE;

    //--Construction Properties
    memset(mArgs, 0, sizeof(int) * UDMF_MAX_ARGS);
}

//====================================== Property Queries =========================================
bool UDMFLinedef::IsVerified()
{
    return mIsVerified;
}

//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
bool UDMFLinedef::Verify(int pVertexMax, int pSidedefMax, UDMFSidedef *pSidedefArray, int pSectorMax)
{
    //--Once all level information has been loaded, the linedef can check to make sure its vertices
    //  and sidedefs all point at legal information. Since sidedefs also point at sectors, we can
    //  verify that our sidedefs are legal. This speeds up some checks later since the level information
    //  is expected to stay static once it's read.
    //--Returns false if the verification failed, true if it passed.
    mIsVerified = false;

    //--A level with no sidedef data cannot be checked against.
    if(!pSidedefArray) return false;

    //--Verify that the three vital parts of the linedef are legal. A linedef must always have a
    //  sidedef on its right side or the map has an illegal/unused linedef. Some map utilities
    //  may trim these during compilation.
    if(mVertex0 < 0 || mVertex0 >= pVertexMax) return false;
    if(mVertex1 < 0 || mVertex1 >= pVertexMax) return false;
    if(mSidedefR < 0 || mSidedefR >= pSidedefMax) return false;

    //--A linedef can have its SidedefL be -1 legally (as long as its SidedefR is not!).
    //  Here, we only check the upper bound.
    if(mSidedefL >= pSidedefMax) return false;

    //--Check to make sure the sectors pointed at by the sidedefs are legal. If not, the linedef
    //  is implicitly illegal, and the mapper should fix that problem.
    if(pSidedefArray[mSidedefR].mSector < 0 || pSidedefArray[mSidedefR].mSector >= pSectorMax) return false;

    //--Left sidedef is only checked if it was valid.
    if(mSidedefL >= 0 && (pSidedefArray[mSidedefL].mSector < 0 || pSidedefArray[mSidedefL].mSector >= pSectorMax)) return false;

    //--All checks passed, the linedef is now verified.
    mIsVerified = true;
    return true;
}
void UDMFLinedef::ResolveSpecials()
{
    //--During level loading, linedefs are read in a text format, which means their properties can
    //  be loaded in any order and some might be loaded after the special tag they rely on is set.
    //  This routine switches the internal flags on based on the matching special, and verifies them
    //  if that is necessary.
    //--This should be called around the same time as linedef verification.
    if(mSpecialFlag == LINE_SPECIAL_NONE) return;

    //--Flag for 3D floors. Requires some arguments.
    if(mSpecialFlag == LINE_SPECIAL_3D_FLOOR)
    {
        mHas3DFloor = true;
        m3DFloorTag = mArgs[0];
        m3DFloorType = mArgs[1];
        m3DFloorOpacity = 1.0f;//Stored, but not used in the engine yet.
    }
    //--Flag for plane alignment. Requires some arguments; if they're not present, disables plane align.
    else if(mSpecialFlag == LINE_SPECIAL_PLANE_ALIGN)
    {
        //--If Arg0 is 1, this is a front-to-back alignment.
        if(mArgs[0] == 1)
        {
            mPlaneAlignFloor = PLANE_ALIGN_FRONT_TO_BACK;
        }
        //--If Arg0 is 2, this is a back-to-front alignment.
        else if(mArgs[0] == 2)
        {
            mPlaneAlignFloor = PLANE_ALIGN_BACK_TO_FRONT;
        }

        //--Arg1 is 1, front-to-back on the ceiling.
        if(mArgs[1] == 1)
        {
            mPlaneAlignCeiling = PLANE_ALIGN_FRONT_TO_BACK;
        }
        //--If Arg1 is 2, this is a back-to-front alignment.
        else if(mArgs[1] == 2)
        {
            mPlaneAlignCeiling = PLANE_ALIGN_BACK_TO_FRONT;
        }
    }
    //--Unhandled special flag. Print a warning, but don't stop execution.
    else
    {
        #ifndef UDMFLINEDEF_IGNORE_SPECIAL_WARNINGS
            DebugManager::ForcePrint("UDMFLinedef:ResolveSpecials - Warning, unknown special %i.\n", mSpecialFlag);
        #endif
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
void UDMFLinedef::HandleProperty(char *pField, char *pOperator, char *pValue)
{
    //--Called during level loading, provides a set of three strings. Taken together, the strings are,
    //  when seen in the UDMF file:
    //  [Field] [Operator] [Value];
    //  Example:
    //  StartVertex = 6;
    if(!pField || !pOperator || !pValue) return;
    if(pField[0] == '\0' || pOperator[0] == '\0' || pValue[0] == '\0') return;

    //--Unique ID. Not used much by this engine.
    if(!strcasecmp(pField, "id"))
    {
        mID = atoi(pValue);
    }
    //--Starting vertex.
    else if(!strcasecmp(pField, "v1"))
    {
        mVertex0 = atoi(pValue);
    }
    //--Ending vertex.
    else if(!strcasecmp(pField, "v2"))
    {
        mVertex1 = atoi(pValue);
    }
    //--Two-sided. We don't care about this property, since the existence of a sideback implies it's two-sided.
    else if(!strcasecmp(pField, "twosided"))
    {
    }
    //--Blocking. Used for collisions, entities cannot cross a blocking linedef. We don't use that in this engine.
    else if(!strcasecmp(pField, "blocking"))
    {
    }
    //--Sidedef R. Called the "front" sidedef.
    else if(!strcasecmp(pField, "sidefront"))
    {
        mSidedefR = atoi(pValue);
    }
    //--Sidedef L. Called the "back" sidedef. Not always present.
    else if(!strcasecmp(pField, "sideback"))
    {
        mSidedefL = atoi(pValue);
    }
    //--Upper unpegged.
    else if(!strcasecmp(pField, "dontpegtop"))
    {
        mIsUpperUnpegged = true;
    }
    //--Lower unpegged.
    else if(!strcasecmp(pField, "dontpegbottom"))
    {
        mIsLowerUnpegged = true;
    }
    //--Special. Handled elsewhere, used for things like 3D floors and slopes.
    else if(!strcasecmp(pField, "special"))
    {
        mSpecialFlag = atoi(pValue);
    }
    //--Args. Stores generic data which is processed later on. These are integers. There can only
    //  be 5 in the UDMF standard.
    else if(!strncasecmp(pField, "arg", 3))
    {
        int tSlot = pField[3] - '0';
        if(tSlot >= 0 && tSlot < UDMF_MAX_ARGS) mArgs[tSlot] = atoi(pValue);
    }
    //--Unhandled property. Prints a warning if the debug flag is set.
    #ifndef UDMFLINEDEF_IGNORE_PROPERTY_WARNINGS
    else
    {
        DebugManager::ForcePrint("UDMFLinedef:HandleProperty - Warning, unknown property %s.\n", pField);
    }
    #endif
}

//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
