//--[UDMFLinedef]
//--A structure which represents a linedef under UDMF guidelines, which is much more expandable than
//  the standard WAD_Linedef. This structure contains some functions helpful for resolving its internal
//  properties, and has useful getters/setters.
//--Because the UDMF file stores information as a string, there is no guarantee as to which order the
//  information is put into the linedef. Therefore, a post-process function is provided once the object
//  has received all its properties.
//--The Starlight version of the linedef could theoretically contain many special tags, but current mapping
//  tools do not allow for this. Therefore, these don't have extended special listings.

#pragma once

//--[Includes]
struct UDMFSidedef;
#include <stdint.h>

//--[Local Structures]
//--[Local Definitions]
//--Max number of arguments, defined by the UDMF standard.
#define UDMF_MAX_ARGS 5

//--Plane alignment flags. Used in Starlight Engine.
#define PLANE_ALIGN_NONE 0
#define PLANE_ALIGN_FRONT_TO_BACK 1
#define PLANE_ALIGN_BACK_TO_FRONT 2

//--Special values for linedefs. Determined by the UDMF standard.
#define LINE_SPECIAL_NONE 0
#define LINE_SPECIAL_3D_FLOOR 160
#define LINE_SPECIAL_PLANE_ALIGN 181

//--Debug Flag. Set this to true to speed up the engine, as it will assume it does not need
//  to check the linedefs after verifying them. If every linedef passes the verify test, then
//  checking them again slows down map compilation. If any one linedef did not pass the test,
//  it may cause a crash.
//--It is the job of the map designer to deal with all console warnings before this flag is
//  defined and the map is released to the public.
//#define UDMFLINEDEF_TRUST_VERIFY

//--Debug Flag. Set this to true to ignore unhandled linedef specials. The handled ones are
//  defined above. These are just warnings, the map will function just fine if an unknown special
//  is found, but will look (and possibly play) differently than the equivalent ZDoom map would.
//#define UDMFLINEDEF_IGNORE_SPECIAL_WARNINGS

//--Debug Flag. Define this flag to ignore unhandled property warnings. As above, the map
//  may look different if warnings are barked but it's still usable.
//#define UDMFLINEDEF_IGNORE_PROPERTY_WARNINGS

//--[Classes]
struct UDMFLinedef
{
    public:
    //--System
    int32_t mID;
    bool mIsVerified;

    //--World Properties
    union{int32_t mVertex0; int32_t mStartVertex; };
    union{int32_t mVertex1; int32_t mEndVertex; };
    int32_t mSidedefL, mSidedefR;

    //--Texture Properties
    bool mIsLowerUnpegged;
    bool mIsUpperUnpegged;

    //--Flags
    int mSpecialFlag;

    //--3D Floor Flags
    bool mHas3DFloor;
    int m3DFloorTag;
    int m3DFloorType;
    float m3DFloorOpacity;

    //--Slope Tags
    int mPlaneAlignFloor;
    int mPlaneAlignCeiling;

    //--Construction Properties
    int mArgs[UDMF_MAX_ARGS];

    protected:

    public:
    //--System
    void Initialize();

    //--Public Variables
    //--Property Queries
    bool IsVerified();

    //--Manipulators
    //--Core Methods
    bool Verify(int pVertexMax, int pSidedefMax, UDMFSidedef *pSidedefArray, int pSectorMax);
    void ResolveSpecials();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    void HandleProperty(char *pField, char *pOperator, char *pValue);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

//--Hooking Functions

