//--Base
#include "WorldPolygon.h"

//--Classes
#include "WADFile.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarBitmap.h"
#include "SugarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"
#include "GlDfn.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//#define WP_DEBUG

//=========================================== System ==============================================
void WorldPolygon::Initialize()
{
    //--[WorldPolygon]
    //--System
    mIsSkyPolygon = false;

    //--Points
    mPointsTotal = 0;
    mPoints = NULL;
    mDotProduct = 0.0f;

    //--Texturing
    mTexCoords = NULL;
    mLightmapCoords = NULL;
    rTexture = NULL;
    mLightmap = NULL;
    uint32_t tPixel = 0xFFFF;
    mLightmap = new SugarBitmap((uint8_t *)&tPixel, 1, 1);

    //--Lightmap Storage
    mLightmapXSize = 0;
    mLightmapYSize = 0;
    mLightmapData = NULL;

    //--Temporary Data
    mTemporaryTextureIndex = -1;
    mLightmapLoadX = 0;
    mLightmapLoadY = 0;
}
void WorldPolygon::DeleteThis(void *pPtr)
{
    WorldPolygon *rPolygon = (WorldPolygon *)pPtr;
    delete rPolygon->mLightmap;
    free(rPolygon->mPoints);
    free(rPolygon->mTexCoords);
    free(rPolygon->mLightmapCoords);
    free(rPolygon->mLightmapData);
    free(rPolygon);
}

//--[Public Statics]
//--For every X/Y pixels along the polygon, a luxel is created. This value cannot be zero or no lightmap
//  will be created. No matter what the ratio, all lightmaps will have at least 1 luxel in both directions.
//--This value can go below zero, meaning the lightmap would have more precision than the base texture
//  would be expected to, at least in world units. Hi-resolution textures might benefit from these lightmaps.
//--The lower these values are, the longer lightmaps take to build, and the better they look.
//--The lower clamp for these is 0.001f, at which point you probably wouldn't be able to see anything anyway.
float WorldPolygon::xLightmapRatioX = 1.0f;
float WorldPolygon::xLightmapRatioY = 1.0f;

//--Once the ratio is calculated, these clamps are applied to force sizing of the lightmaps within
//  their bounds. The lightmap minimum size must be at least 1 (and that value cannot be changed)
//  but there is no strict upper limit on the max size of the lightmap. Set the max sizes to 0 to
//  unset the high clamps.
//--Obviously, not all textures will use the max clamps. Setting the clamps high will slow down building
//  and make the lightmaps look better, if and only if they needed the extra precision.
int WorldPolygon::xLightmapSizeMinX = 2;
int WorldPolygon::xLightmapSizeMinY = 2;
int WorldPolygon::xLightmapSizeMaxX = 0;
int WorldPolygon::xLightmapSizeMaxY = 0;

//--Alternately, you can use this variable to set the maximum sizes of the lightmap in terms of area
//  (total luxels) rather than by a particular dimension. Note that the minimum clamp still applies.
//  Also, sometimes the lightmap will exceed this parameter slightly due to area considerations, so this
//  is more of an advisory.
//--This is only used if the max x/y size was set to 0! Set to 0 and this value will be ignored.
int WorldPolygon::xLightmapMaxArea = 256;

//--This is the starting value for a lightmap. The light value can never go below this amount. Even in a room
//  with no lights whatsoever, this ambient lighting will still be present.
int WorldPolygon::xLightmapAmbient = 64;

//====================================== Property Queries =========================================
int WorldPolygon::GetTriangleCount()
{
    //--Returns how many triangles this object is composed of. Should be 1 or 2 after decomposition.
    if(mPointsTotal < 3) return 0;
    return mPointsTotal - 2;
}
void WorldPolygon::PopulateTriangleData(int &sCursor, float *pData)
{
    //--Populate in order: VertexX, VertexY, VertexZ, TexCoordX, TexCoordY.
    if(!pData || mPointsTotal < 3) return;

    //--Iterate across the points and populate.
    for(int i = 1; i < mPointsTotal - 1; i ++)
    {
        //--Get the next points.
        int p = (i + 1) % mPointsTotal;

        //--Create a triangle out of these points.
        pData[sCursor+0] = mPoints[0].mX;
        pData[sCursor+1] = mPoints[0].mY;
        pData[sCursor+2] = mPoints[0].mZ;
        pData[sCursor+3] = mTexCoords[0].mX;
        pData[sCursor+4] = mTexCoords[0].mY;
        pData[sCursor+5] = (float)mTemporaryTextureIndex + WP_GFX_CARD_OFFSET;
        pData[sCursor+6] = mLightmapCoords[0].mX;
        pData[sCursor+7] = mLightmapCoords[0].mY;
        sCursor += 8;
        pData[sCursor+0] = mPoints[i].mX;
        pData[sCursor+1] = mPoints[i].mY;
        pData[sCursor+2] = mPoints[i].mZ;
        pData[sCursor+3] = mTexCoords[i].mX;
        pData[sCursor+4] = mTexCoords[i].mY;
        pData[sCursor+5] = (float)mTemporaryTextureIndex + WP_GFX_CARD_OFFSET;
        pData[sCursor+6] = mLightmapCoords[i].mX;
        pData[sCursor+7] = mLightmapCoords[i].mY;
        sCursor += 8;
        pData[sCursor+0] = mPoints[p].mX;
        pData[sCursor+1] = mPoints[p].mY;
        pData[sCursor+2] = mPoints[p].mZ;
        pData[sCursor+3] = mTexCoords[p].mX;
        pData[sCursor+4] = mTexCoords[p].mY;
        pData[sCursor+5] = (float)mTemporaryTextureIndex + 0.100000f;
        pData[sCursor+6] = mLightmapCoords[p].mX;
        pData[sCursor+7] = mLightmapCoords[p].mY;
        sCursor += 8;
    }
}
bool WorldPolygon::IsFacingPoint(Point3D pPoint)
{
    //--Returns whether or not the given point is facing this polygon. We use Normals for this.
    //  If the DotProduct has not been calculated yet, this routine always fails.
    float tDistanceToMiddle = Get3DDistance(pPoint, mAverage);
    float tDistanceToNormal = Get3DDistance(pPoint, mAverage.mX + mNormal.mX, mAverage.mY + mNormal.mY, mAverage.mZ + mNormal.mZ);
    if(tDistanceToMiddle < tDistanceToNormal) return false;
    return true;

    if(mDotProduct == 0.0f) return false;
    return ((mNormal.mX * pPoint.mX) + (mNormal.mY * pPoint.mY * -1.0f) + (mNormal.mZ * pPoint.mZ * -1.0f) + mDotProduct) < 0.0f;
}
bool WorldPolygon::IsPointInPolygon(float pX, float pY, float pZ)
{
    //--Returns whether or not the provided point intersects exactly with the given polygon.
    if(mPointsTotal < 3) return false;

    //--Setup.
    bool tIsWithinPoly = false;

    //--Does the X have the highest magnitude in our normal?
    if(fabsf(mNormal.mX) > fabsf(mNormal.mY) && fabsf(mNormal.mX) > fabsf(mNormal.mZ))
    {
        //--For each vertex on the polygon, check it against the next vertex.
        for(int i = 0; i < mPointsTotal; i ++)
        {
            //--Get the slot of the next vertex.
            int j = ((i + 1) % mPointsTotal);

            //--Checks.
            if(((mPoints[i].mZ > pZ) != (mPoints[j].mZ > pZ)) &&
               (pY < (mPoints[j].mY - mPoints[i].mY) * (pZ - mPoints[i].mZ) / (mPoints[j].mZ - mPoints[i].mZ) + mPoints[i].mY))
                tIsWithinPoly = !tIsWithinPoly;
        }

        //--Done.
        return tIsWithinPoly;
    }
    //--Does the Y have the highest magnitude in our normal?
    else if(fabsf(mNormal.mY) > fabsf(mNormal.mX) && fabsf(mNormal.mY) > fabsf(mNormal.mZ))
    {
        //--For each vertex on the polygon, check it against the next vertex.
        for(int i = 0; i < mPointsTotal; i ++)
        {
            //--Get the slot of the next vertex.
            int j = ((i + 1) % mPointsTotal);

            //--Checks.
            if(((mPoints[i].mZ > pZ) != (mPoints[j].mZ > pZ)) &&
               (pX < (mPoints[j].mX - mPoints[i].mX) * (pZ - mPoints[i].mZ) / (mPoints[j].mZ - mPoints[i].mZ) + mPoints[i].mX))
                tIsWithinPoly = !tIsWithinPoly;
        }

        //--Done.
        return tIsWithinPoly;
    }
    //--Otherwise, the Z must have the highest magnitude (and wins in case of ties).
    else
    {
        for(int i = 0; i < mPointsTotal; i ++)
        {
            //--Get the slot of the next vertex.
            int j = ((i + 1) % mPointsTotal);

            //--Checks.
            if(((mPoints[i].mY > pY) != (mPoints[j].mY > pY)) &&
               (pX < (mPoints[j].mX - mPoints[i].mX) * (pY - mPoints[i].mY) / (mPoints[j].mY - mPoints[i].mY) + mPoints[i].mX))
                tIsWithinPoly = !tIsWithinPoly;
        }

        //--Done.
        return tIsWithinPoly;
    }

    //--Tie case. Fail.
    return false;
}
bool WorldPolygon::IsPointInPlanarPolygon(float pX, float pY)
{
    //--Checks if the point is within the polygon, but ignoring the Z dimension. This is used
    //  exclusively for height checking. Polygons with no Z normal component will not even run
    //  this query, and won't be on the height listing either.
    bool tIsWithinPoly = false;
    for(int i = 0; i < mPointsTotal; i ++)
    {
        //--Get the slot of the next vertex.
        int j = ((i + 1) % mPointsTotal);

        //--Checks.
        if(((mPoints[i].mY > pY) != (mPoints[j].mY > pY)) &&
           (pX < (mPoints[j].mX - mPoints[i].mX) * (pY - mPoints[i].mY) / (mPoints[j].mY - mPoints[i].mY) + mPoints[i].mX))
            tIsWithinPoly = !tIsWithinPoly;
    }

    return tIsWithinPoly;
}
float WorldPolygon::GetHeightAt(float pX, float pY)
{
    //--Returns the height of this polygon at a given point on it, ignoring the Z. This point can be outside the polygon,
    //  though that might yield some absurd results.
    if(mPointsTotal < 3)
    {
        return 0.0f;
    }
    //--Polygons with exactly three points use this triangular formula for high-precision heights.
    if(mPointsTotal == 3)
    {
        //--Compute the height of the point assuming all three have legal slope values.
        float tVecUX = mPoints[1].mX - mPoints[0].mX;
        float tVecUY = mPoints[1].mY - mPoints[0].mY;
        float tVecUZ = mPoints[1].mZ - mPoints[0].mZ;
        float tVecVX = mPoints[2].mX - mPoints[0].mX;
        float tVecVY = mPoints[2].mY - mPoints[0].mY;
        float tVecVZ = mPoints[2].mZ - mPoints[0].mZ;
        float tNormalX = (tVecUY * tVecVZ) - (tVecUZ * tVecVY);
        float tNormalY = (tVecUZ * tVecVX) - (tVecUX * tVecVZ);
        float tNormalZ = (tVecUX * tVecVY) - (tVecUY * tVecVX);

        //--Find the Z coordinate at the given point.
        if(tNormalZ != 0.0f)
        {
            float tVectorX = pX - mPoints[0].mX;
            float tVectorY = pY - mPoints[0].mY;
            float tVectorZ = -((tNormalY * tVectorY) + (tNormalX * tVectorX)) / tNormalZ;
            float tCheckHeight = mPoints[0].mZ + tVectorZ; //Z coordinate is flipped, so add it here. Normally it's a subtraction.
            return tCheckHeight;
        }

        //--Error, the normal Z was zero. That means this slope is perpindicular to gravity and should not be queried.
        return 0.0f;
    }
    //--Quads are allowed to use a single slope across the polygon.
    else if(mPointsTotal == 4)
    {
        //TODO: That.
        return mPoints[0].mZ;
    }
    //--Anything with more points will return the Z of the zeroth point, as they are assumed to be flat.
    else
    {
        return mPoints[0].mZ;
    }
}

//========================================= Manipulators ==========================================
void WorldPolygon::Allocate(int pPointsTotal)
{
    //--Deallocate old data.
    mPointsTotal = 0;
    free(mPoints);
    free(mTexCoords);
    mPoints = NULL;
    mTexCoords = NULL;

    //--A polygon without 3 points is not a polygon!
    if(pPointsTotal < 3) return;

    //--Allocate.
    mPointsTotal = pPointsTotal;
    SetMemoryData(__FILE__, __LINE__);
    mPoints = (Point3D *)starmemoryalloc(sizeof(Point3D) * mPointsTotal);
    SetMemoryData(__FILE__, __LINE__);
    mTexCoords = (Point3D *)starmemoryalloc(sizeof(Point3D) * mPointsTotal);
    SetMemoryData(__FILE__, __LINE__);
    mLightmapCoords = (Point3D *)starmemoryalloc(sizeof(Point3D) * mPointsTotal);
}
void WorldPolygon::SetPoint(int pSlot, float pX, float pY, float pZ)
{
    if(pSlot < 0 || pSlot >= mPointsTotal) return;
    mPoints[pSlot].Set(pX, pY, pZ);
}
void WorldPolygon::SetTexCoord(int pSlot, float pX, float pY)
{
    if(pSlot < 0 || pSlot >= mPointsTotal) return;
    mTexCoords[pSlot].Set(pX, pY, 0.0f);
}
void WorldPolygon::ComputeNormal()
{
    //--Recalculates the normal vector for this polygon. Also recalculates the average position of the polygon,
    //  since that is often used for rendering.
    mNormal.Set(0.0f, 0.0f, 0.0f);
    mAverage.Set(0.0f, 0.0f, 0.0f);

    //--Fill normal information.
    for(int p = 0; p < mPointsTotal; p ++)
    {
        //--Average.
        mAverage.mX = mAverage.mX + (mPoints[p].mX / (float)mPointsTotal);
        mAverage.mY = mAverage.mY + (mPoints[p].mY / (float)mPointsTotal);
        mAverage.mZ = mAverage.mZ + (mPoints[p].mZ / (float)mPointsTotal);

        //--Resolve the next point.
        int o = (p + 1) % mPointsTotal;

        //--Compute the normal.
        mNormal.mX = mNormal.mX + ((mPoints[p].mY - mPoints[o].mY) * (mPoints[p].mZ + mPoints[o].mZ));
        mNormal.mY = mNormal.mY + ((mPoints[p].mZ - mPoints[o].mZ) * (mPoints[p].mX + mPoints[o].mX));
        mNormal.mZ = mNormal.mZ + ((mPoints[p].mX - mPoints[o].mX) * (mPoints[p].mY + mPoints[o].mY));
    }

    //--Normalize the vector to simplify the calculations.
    float tNormalTotal = mNormal.mX + mNormal.mY + mNormal.mZ;
    if(tNormalTotal < 0.0f) tNormalTotal = tNormalTotal * -1.0f;

    //--Divide. It shouldn't be possible for a normal total to compute to exactly zero unless there was an error.
    if(tNormalTotal == 0.0f) return;
    mNormal.mX = mNormal.mX / tNormalTotal;
    mNormal.mY = mNormal.mY / tNormalTotal;
    mNormal.mZ = mNormal.mZ / tNormalTotal;
}
void WorldPolygon::ComputeDotProduct()
{
    //--Compute and stores the DotProduct of the plane represented by this polygon. Should be called
    //  every time the normal is recalculated.
    if(mPointsTotal < 3) return;
    mDotProduct = (mNormal.mX * mPoints[0].mX) + (mNormal.mY * mPoints[0].mY) + (mNormal.mZ * mPoints[0].mZ) * -1.0f;
}

//========================================= Core Methods ==========================================
void WorldPolygon::Compress()
{
    //--Removes any vertices from the polygon that bisect the line they're on. These just slow down rendering.
    if(mPointsTotal < 5) return;

    //--Create a set of lists and clone the original data.
    SugarLinkedList *tPointsList = new SugarLinkedList(true);
    SugarLinkedList *tTexCoordList = new SugarLinkedList(true);
    SugarLinkedList *tLightmapList = new SugarLinkedList(true);
    for(int i = 0; i < mPointsTotal; i ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        Point3D *nNewPoint = (Point3D *)starmemoryalloc(sizeof(Point3D));
        memcpy(nNewPoint, &mPoints[i], sizeof(Point3D));
        tPointsList->AddElement("X", nNewPoint, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nNewPoint = (Point3D *)starmemoryalloc(sizeof(Point3D));
        memcpy(nNewPoint, &mTexCoords[i], sizeof(Point3D));
        tTexCoordList->AddElement("X", nNewPoint, &FreeThis);

        SetMemoryData(__FILE__, __LINE__);
        nNewPoint = (Point3D *)starmemoryalloc(sizeof(Point3D));
        memcpy(nNewPoint, &mLightmapCoords[i], sizeof(Point3D));
        tLightmapList->AddElement("X", nNewPoint, &FreeThis);
    }

    //--No go and start decomposing!
    while(true)
    {
        //--Flag.
        bool tDeletedAnyPoints = false;

        //--Iterate across the list.
        for(int i = 0; i < tPointsList->GetListSize(); i ++)
        {
            //--Calc the previous and next slots.
            int tPrevSlot = i - 1;
            int tNextSlot = (i + 1) % tPointsList->GetListSize();
            if(tPrevSlot < 0) tPrevSlot = tPointsList->GetListSize() - 1;

            //--Get the previous, current, and next points.
            Point3D *rPrev = (Point3D *)tPointsList->GetElementBySlot(tPrevSlot);
            Point3D *rCurr = (Point3D *)tPointsList->GetElementBySlot(i);
            Point3D *rNext = (Point3D *)tPointsList->GetElementBySlot(tNextSlot);

            //--If any of these points are identical, the current point can be safely removed.
            if(rPrev->mX == rCurr->mX && rPrev->mY == rCurr->mY && rPrev->mZ == rCurr->mZ)
            {
                tDeletedAnyPoints = true;
                tPointsList->RemoveElementI(i);
                tTexCoordList->RemoveElementI(i);
                tLightmapList->RemoveElementI(i);
                break;
            }
            if(rNext->mX == rCurr->mX && rNext->mY == rCurr->mY && rNext->mZ == rCurr->mZ)
            {
                tDeletedAnyPoints = true;
                tPointsList->RemoveElementI(i);
                tTexCoordList->RemoveElementI(i);
                tLightmapList->RemoveElementI(i);
                break;
            }

            //--If the angle from PtoC is the same as the angle from CtoN, then C bisects PN.
            //  This is only true if the Z values are the same.
            float tAnglePC = atan2f(rCurr->mY - rPrev->mY, rCurr->mX - rPrev->mX);
            float tAngleCN = atan2f(rNext->mY - rCurr->mY, rNext->mX - rCurr->mX);
            if(fabsf(tAngleCN - tAnglePC) < 0.0001f && rPrev->mZ == rCurr->mZ && rNext->mZ == rCurr->mZ)
            {
                tDeletedAnyPoints = true;
                tPointsList->RemoveElementI(i);
                tTexCoordList->RemoveElementI(i);
                tLightmapList->RemoveElementI(i);
                break;
            }
        }

        //--If no points were deleted, we're done.
        if(!tDeletedAnyPoints) break;
    }

    //--Now reconstitute the polygon.
    mPointsTotal = tPointsList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mPoints = (Point3D *)starmemoryalloc(sizeof(Point3D) * mPointsTotal);
    SetMemoryData(__FILE__, __LINE__);
    mTexCoords = (Point3D *)starmemoryalloc(sizeof(Point3D) * mPointsTotal);
    SetMemoryData(__FILE__, __LINE__);
    mLightmapCoords = (Point3D *)starmemoryalloc(sizeof(Point3D) * mPointsTotal);
    for(int i = 0; i < mPointsTotal; i ++)
    {
        Point3D *rCurrPoint = (Point3D *)tPointsList->GetElementBySlot(i);
        Point3D *rCurrTexCoord = (Point3D *)tTexCoordList->GetElementBySlot(i);
        Point3D *rCurrLightmap = (Point3D *)tLightmapList->GetElementBySlot(i);
        memcpy(&mPoints[i], rCurrPoint, sizeof(Point3D));
        memcpy(&mTexCoords[i], rCurrTexCoord, sizeof(Point3D));
        memcpy(&mLightmapCoords[i], rCurrLightmap, sizeof(Point3D));
    }

    //--Clean.
    delete tPointsList;
    delete tTexCoordList;
    delete tLightmapList;
}
void RotateAroundAxis(Point3D &sPoint, Point3D pAxis, float pAngle)
{
    //--[Documenation]
    //--Given a point in 3D space (assumed to be a distance from the origin), rotates that point around
    //  the provided normal axis by pAngle. pAngle is expected to be in radians.

    //--[Setup]
    //--Get squared values for quick-access.
    float u = pAxis.mX;
    float v = pAxis.mY;
    float w = pAxis.mZ;
    float u2 = (pAxis.mX * pAxis.mX);
    float v2 = (pAxis.mY * pAxis.mY);
    float w2 = (pAxis.mZ * pAxis.mZ);

    //--Renormalize, in case the normal was somehow not set to 1.0f.
    float L = (u2 + v2 + w2);
    if(L == 0.0f) return;

    //--[Starting Point]
    //--Fill the input matrix.
    float tInputMatrix[4];
    tInputMatrix[0] = sPoint.mX;
    tInputMatrix[1] = sPoint.mY;
    tInputMatrix[2] = sPoint.mZ;
    tInputMatrix[3] = 1.0f;

    //--[Matrix Fill]
    //--Setup.
    float tRotationMatrix[4][4];

    //--Top row.
    tRotationMatrix[0][0] = (u2 + (v2 + w2) * cosf(pAngle)) / L;
    tRotationMatrix[0][1] = (u * v * (1 - cosf(pAngle)) - w * sqrtf(L) * sinf(pAngle)) / L;
    tRotationMatrix[0][2] = (u * w * (1 - cosf(pAngle)) + v * sqrtf(L) * sinf(pAngle)) / L;
    tRotationMatrix[0][3] = 0.0;

    //--Middle row.
    tRotationMatrix[1][0] = (u * v * (1 - cosf(pAngle)) + w * sqrtf(L) * sinf(pAngle)) / L;
    tRotationMatrix[1][1] = (v2 + (u2 + w2) * cosf(pAngle)) / L;
    tRotationMatrix[1][2] = (v * w * (1 - cosf(pAngle)) - u * sqrtf(L) * sinf(pAngle)) / L;
    tRotationMatrix[1][3] = 0.0;

    //--Bottom row.
    tRotationMatrix[2][0] = (u * w * (1 - cosf(pAngle)) - v * sqrtf(L) * sinf(pAngle)) / L;
    tRotationMatrix[2][1] = (v * w * (1 - cosf(pAngle)) + u * sqrtf(L) * sinf(pAngle)) / L;
    tRotationMatrix[2][2] = (w2 + (u2 + v2) * cosf(pAngle)) / L;
    tRotationMatrix[2][3] = 0.0;

    //--Extraneous row.
    tRotationMatrix[3][0] = 0.0;
    tRotationMatrix[3][1] = 0.0;
    tRotationMatrix[3][2] = 0.0;
    tRotationMatrix[3][3] = 1.0;

    //--[Matrix Multiplication]
    float tOutputMatrix[4];
    for(int i = 0; i < 4; i++ )
    {
        tOutputMatrix[i] = 0.0f;
        for(int k = 0; k < 4; k++)
        {
            tOutputMatrix[i] += tRotationMatrix[i][k] * tInputMatrix[k];
        }
    }

    //--Debug report.
    #ifdef WP_DEBUG
        DebugManager::Print("[Rotation Algorithm]\n");
        DebugManager::Print(" Start:\n");
        DebugManager::Print("  X: %f\n", sPoint.mX);
        DebugManager::Print("  Y: %f\n", sPoint.mY);
        DebugManager::Print("  Z: %f\n", sPoint.mZ);
        DebugManager::Print(" End:\n");
        DebugManager::Print("  X: %f\n", tOutputMatrix[0]);
        DebugManager::Print("  Y: %f\n", tOutputMatrix[1]);
        DebugManager::Print("  Z: %f\n", tOutputMatrix[2]);
    #endif

    //--[Output]
    //--Fill.
    sPoint.mX = tOutputMatrix[0];
    sPoint.mY = tOutputMatrix[1];
    sPoint.mZ = tOutputMatrix[2];
    /*

    //--Setup.
    float cCosValue = cosf(pAngle);
    float cSinValue = sinf(pAngle);
    float cInvValue = 1.0f - cCosValue;

    //--Matrix multiplication.
    float tMatrix[3][3];
    tMatrix[0][0] = (pAxis.mX * pAxis.mX * cInvValue) + cCosValue;
    tMatrix[0][1] = (pAxis.mY * pAxis.mX * cInvValue) + (pAxis.mZ * cSinValue);
    tMatrix[0][2] = (pAxis.mZ * pAxis.mX * cInvValue) - (pAxis.mY * cSinValue);

    tMatrix[1][0] = (pAxis.mY * pAxis.mX * cInvValue) - (pAxis.mZ * cSinValue);
    tMatrix[1][1] = (pAxis.mY * pAxis.mY * cInvValue) + cCosValue;
    tMatrix[1][2] = (pAxis.mZ * pAxis.mY * cInvValue) + (pAxis.mX * cSinValue);

    tMatrix[2][0] = (pAxis.mX * pAxis.mZ * cInvValue) + (pAxis.mY * cSinValue);
    tMatrix[2][1] = (pAxis.mZ * pAxis.mY * cInvValue) - (pAxis.mX * cSinValue);
    tMatrix[2][2] = (pAxis.mZ * pAxis.mZ * cInvValue) + cCosValue;

    //--Debug.
    #ifdef WP_DEBUG
        DebugManager::Print("[Rotation Algorithm]\n");
        DebugManager::Print(" Start:\n");
        DebugManager::Print("  X: %f\n", sPoint.mX);
        DebugManager::Print("  Y: %f\n", sPoint.mY);
        DebugManager::Print("  Z: %f\n", sPoint.mZ);
        DebugManager::Print(" Values:\n");
        DebugManager::Print("  Cos: %f\n", cCosValue);
        DebugManager::Print("  Sin: %f\n", cSinValue);
        DebugManager::Print("  Inv: %f\n", cInvValue);
        DebugManager::Print(" Array:\n");
        DebugManager::Print("  %f %f %f\n", tMatrix[0][0], tMatrix[0][1], tMatrix[0][1]);
        DebugManager::Print("  %f %f %f\n", tMatrix[1][0], tMatrix[1][1], tMatrix[1][1]);
        DebugManager::Print("  %f %f %f\n", tMatrix[2][0], tMatrix[2][1], tMatrix[2][1]);
    #endif

    sPoint.mX = (sPoint.mX * tMatrix[0][0]) + (sPoint.mX * tMatrix[0][1]) + (sPoint.mX * tMatrix[0][2]);
    sPoint.mY = (sPoint.mY * tMatrix[1][0]) + (sPoint.mY * tMatrix[1][1]) + (sPoint.mY * tMatrix[1][2]);
    sPoint.mZ = (sPoint.mZ * tMatrix[2][0]) + (sPoint.mZ * tMatrix[2][1]) + (sPoint.mZ * tMatrix[2][2]);
    #ifdef WP_DEBUG
        DebugManager::Print(" End:\n");
        DebugManager::Print("  X: %f\n", sPoint.mX);
        DebugManager::Print("  Y: %f\n", sPoint.mY);
        DebugManager::Print("  Z: %f\n", sPoint.mZ);
        DebugManager::Print("[Rotation Algorithm Complete]\n");
    #endif*/
}

void WorldPolygon::SetupLightmap()
{
    //--Begins the lightmap setup, which uses a temporary array and allows the SetLightmap() function
    //  to actually do something. Call FinalizeLightmap() once all light computations are done.
    //--Lightmaps use GL_REPEAT on their edges and render much like flats do in the original Doom.
    if(mPointsTotal < 3) return;

    //--These values cannot be less than one. If they are, don't do any calculations.
    if(xLightmapRatioX < 0.001f || xLightmapRatioY < 0.001f) return;

    //--Check if the 0th point is identical to the last point. If so, reduce the point count by 1, this
    //  is a leftover hack to make wrapping vertices faster.
    if(mPointsTotal >= 4 && mPoints[0].mX == mPoints[mPointsTotal-1].mX && mPoints[0].mY == mPoints[mPointsTotal-1].mY && mPoints[0].mZ == mPoints[mPointsTotal-1].mZ)
    {
        #ifdef WP_DEBUG
            DebugManager::ForcePrint("Removing unnecessary vertex.\n");
        #endif
        mPointsTotal --;
    }

    #ifdef WP_DEBUG
        bool tFlag = false;
        if(mPointsTotal == 4 && mPoints[0].mZ == 16.0f && mPoints[1].mZ == 32.0f && mPoints[2].mZ == 32.0f && mPoints[3].mZ == 16.0f) tFlag = true;
        DebugManager::PushPrint(tFlag, "Building lightmap on polygon with %i sides.\n", mPointsTotal);
        for(int i = 0; i < mPointsTotal; i ++)
        {
            DebugManager::Print(" %3i: %6.2f %6.2f %6.2f\n", i, mPoints[i].mX, mPoints[i].mY, mPoints[i].mZ);
        }
    #endif

    //--Storage variables.
    Point3D tLightmapTR;
    Point3D tLightmapBL;

    //--Zero off the lightmap coordinates.
    for(int i = 0; i < mPointsTotal; i ++)
    {
        mLightmapCoords[i].Set(0.0f, 0.0f, -100.0f);
    }

    //--[General Purpose]
    //--Not the most efficient, and tends to produce a skewed lightmap.
    {
        //--The average position on the polygon should already have been computed. It is the center of our lightmap,
        //  and all other points are mapped based on where they are relative to the average point.
        /*float tXDif = fabsf(mPoints[0].mX - mAverage.mX);
        float tYDif = fabsf(mPoints[0].mY - mAverage.mY);
        float tZDif = fabsf(mPoints[0].mY - mAverage.mZ);
        for(int i = 1; i < mPointsTotal; i ++)
        {
            float tNewXDif = fabsf(mPoints[i].mX - mAverage.mX);
            float tNewYDif = fabsf(mPoints[i].mY - mAverage.mY);
            float tNewZDif = fabsf(mPoints[i].mZ - mAverage.mZ);
            if(tNewXDif > tXDif) tXDif = tNewXDif;
            if(tNewYDif > tYDif) tYDif = tNewYDif;
            if(tNewZDif > tZDif) tZDif = tNewZDif;
        }

        //--The leftmost edge of the lightmap is now the middle minus the XDif.
        mLightmapTL.Set(mAverage.mX - tXDif, mAverage.mY - tYDif, mAverage.mZ - tZDif);
        mLightmapBR.Set(mAverage.mX + tXDif, mAverage.mY + tYDif, mAverage.mZ + tZDif);*/




        //--Find the northwest bottommost point, and the southeast topmost point. Those are the lightmap
        //  edges. Note that they don't actually have to be on the polygon in question, though for any
        //  3-sided polygon at least one of them will.
        memcpy(&mLightmapTL, &mPoints[0], sizeof(Point3D));
        memcpy(&mLightmapBR, &mPoints[0], sizeof(Point3D));
        for(int i = 1; i < mPointsTotal; i ++)
        {
            //--Top-left check.
            if(mPoints[i].mX < mLightmapTL.mX) mLightmapTL.mX = mPoints[i].mX;
            if(mPoints[i].mY < mLightmapTL.mY) mLightmapTL.mY = mPoints[i].mY;
            if(mPoints[i].mZ < mLightmapTL.mZ) mLightmapTL.mZ = mPoints[i].mZ;

            //--Bottom-right check.
            if(mPoints[i].mX > mLightmapBR.mX) mLightmapBR.mX = mPoints[i].mX;
            if(mPoints[i].mY > mLightmapBR.mY) mLightmapBR.mY = mPoints[i].mY;
            if(mPoints[i].mZ > mLightmapBR.mZ) mLightmapBR.mZ = mPoints[i].mZ;
        }

        //--Debug.
        #ifdef WP_DEBUG
            DebugManager::Print("Set boundary positions.\n");
            DebugManager::Print(" TL: %6.2f %6.2f %6.2f\n", mLightmapTL.mX, mLightmapTL.mY, mLightmapTL.mZ);
            DebugManager::Print(" BR: %6.2f %6.2f %6.2f\n", mLightmapBR.mX, mLightmapBR.mY, mLightmapBR.mZ);
        #endif

        //--To get the TR, mirror the TL across the central axis. Since we specified that the TL is always the
        //  NW side, we can just flip to get the TR.
        /*
        float tAngleOfRotation = 90.0f * TORADIAN;
        memcpy(&tLightmapTR, &mLightmapTL, sizeof(Point3D));
        if(fabsf(mNormal.mX) > fabsf(mNormal.mY) && fabsf(mNormal.mX) > fabsf(mNormal.mZ))
        {
        }
        else if(fabsf(mNormal.mY) > fabsf(mNormal.mX) && fabsf(mNormal.mY) > fabsf(mNormal.mZ))
        {
        }
        else if(fabsf(mNormal.mZ) > fabsf(mNormal.mX) && fabsf(mNormal.mZ) > fabsf(mNormal.mY))
        {
            float tAngleTo = fabsf(atan2f(mLightmapTL.mY - mAverage.mY, mLightmapTL.mX - mAverage.mX));
            tAngleOfRotation = (180.0f * TORADIAN) - (tAngleTo * 2.0f);
        }

        //--Rotate.
        {


            tLightmapTR.mX = (mLightmapBR.mX - mLightmapTL.mX) * -0.5f;
            tLightmapTR.mY = (mLightmapBR.mY - mLightmapTL.mY) * -0.5f;
            tLightmapTR.mZ = (mLightmapBR.mZ - mLightmapTL.mZ) * -0.5f;
            RotateAroundAxis(tLightmapTR, mNormal, tAngleOfRotation);
            tLightmapTR.mX = ((mLightmapTL.mX + mLightmapBR.mX) / 2.0f) + tLightmapTR.mX;
            tLightmapTR.mY = ((mLightmapTL.mY + mLightmapBR.mY) / 2.0f) + tLightmapTR.mY;
            tLightmapTR.mZ = ((mLightmapTL.mZ + mLightmapBR.mZ) / 2.0f) + tLightmapTR.mZ;
        }*/

        //--Compute the temporary top-right point. We need to rotate around the normals. First, compute the slopes
        //  and distances.
        tLightmapTR.mX = (mLightmapBR.mX - mLightmapTL.mX) * -0.5f;
        tLightmapTR.mY = (mLightmapBR.mY - mLightmapTL.mY) * -0.5f;
        tLightmapTR.mZ = (mLightmapBR.mZ - mLightmapTL.mZ) * -0.5f;
        tLightmapBL.mX = (mLightmapBR.mX - mLightmapTL.mX) * -0.5f;
        tLightmapBL.mY = (mLightmapBR.mY - mLightmapTL.mY) * -0.5f;
        tLightmapBL.mZ = (mLightmapBR.mZ - mLightmapTL.mZ) * -0.5f;

        //--Angle.
        //float tAngleDouble = (atan2f(tLightmapTR.mY, tLightmapTR.mX)) * 1.0f;
        RotateAroundAxis(tLightmapTR, mNormal,  90.0f * TORADIAN);
        RotateAroundAxis(tLightmapBL, mNormal, -90.0f * TORADIAN);

        //--Reposition from the center.
        tLightmapTR.mX = ((mLightmapTL.mX + mLightmapBR.mX) / 2.0f) + tLightmapTR.mX;
        tLightmapTR.mY = ((mLightmapTL.mY + mLightmapBR.mY) / 2.0f) + tLightmapTR.mY;
        tLightmapTR.mZ = ((mLightmapTL.mZ + mLightmapBR.mZ) / 2.0f) + tLightmapTR.mZ;
        tLightmapBL.mX = ((mLightmapTL.mX + mLightmapBR.mX) / 2.0f) + tLightmapBL.mX;
        tLightmapBL.mY = ((mLightmapTL.mY + mLightmapBR.mY) / 2.0f) + tLightmapBL.mY;
        tLightmapBL.mZ = ((mLightmapTL.mZ + mLightmapBR.mZ) / 2.0f) + tLightmapBL.mZ;
    }

    //--Debug.
    #ifdef WP_DEBUG
        DebugManager::Print("Set temporary top-right position.\n");
        //DebugManager::Print(" Angle: %f\n", -90.0f);
        DebugManager::Print(" TR: %6.2f %6.2f %6.2f\n", tLightmapTR.mX, tLightmapTR.mY, tLightmapTR.mZ);
        //DebugManager::Print(" BL: %6.2f %6.2f %6.2f\n", tLightmapBL.mX, tLightmapBL.mY, tLightmapBL.mZ);
    #endif

    //--Calculate how big the lightmap should be. Every xLightmapRatioX is 1 luxel. While the static variable can vary,
    //  once this calculation goes through, changing it has no effect.
    float tXDist = Get3DDistance(mLightmapTL.mX, mLightmapTL.mY, mLightmapTL.mZ, tLightmapTR.mX, tLightmapTR.mY, tLightmapTR.mZ);
    float tYDist = Get3DDistance(tLightmapTR.mX, tLightmapTR.mY, tLightmapTR.mZ, mLightmapBR.mX, mLightmapBR.mY, mLightmapBR.mZ);
    mLightmapXSize = (int)(tXDist / xLightmapRatioX);
    mLightmapYSize = (int)(tYDist / xLightmapRatioY);

    //--Clamping. We also hard-force the lightmap to contain at least 1 luxel since otherwise, why are you building them?
    if(mLightmapXSize < 1) mLightmapXSize = 1;
    if(mLightmapYSize < 1) mLightmapYSize = 1;
    if(mLightmapXSize < xLightmapSizeMinX) mLightmapXSize = xLightmapSizeMinX;
    if(mLightmapYSize < xLightmapSizeMinY) mLightmapXSize = xLightmapSizeMinY;

    //--These clamps only apply if the value is non-zero. Set to 0 to unclamp the max luxel size of a lightmap.
    if(xLightmapSizeMaxX > 0 && xLightmapSizeMaxY > 0)
    {
        if(mLightmapXSize > xLightmapSizeMaxX && xLightmapSizeMaxX > 1) mLightmapXSize = xLightmapSizeMaxX;
        if(mLightmapYSize > xLightmapSizeMaxY && xLightmapSizeMaxY > 1) mLightmapXSize = xLightmapSizeMaxY;
    }
    //--If the max area clamp is nonzero, then we use that instead. This allows lightmaps for polygons
    //  that are considerably longer than they are wide to maintain accuracy without requiring a lot
    //  of extra computations.
    else if(xLightmapMaxArea > 0)
    {
        int tLightmapArea = mLightmapXSize * mLightmapYSize;
        while(tLightmapArea > xLightmapMaxArea)
        {
            if(mLightmapXSize > 1) mLightmapXSize --;
            if(mLightmapYSize > 1) mLightmapYSize --;
            tLightmapArea = mLightmapXSize * mLightmapYSize;
        }
    }

    //--The last upper limit for the lightmaps is hard-set at 1024 since older OpenGL implementations may
    //  force this. This cannot be bypassed.
    {
        if(mLightmapXSize > 1024) mLightmapXSize = 1024;
        if(mLightmapYSize > 1024) mLightmapYSize = 1024;
        if(mLightmapXSize < 1) mLightmapXSize = 1;
        if(mLightmapYSize < 1) mLightmapYSize = 1;
    }

    //--Debug.
    #ifdef WP_DEBUG
        DebugManager::Print("Computed lightmap pixel sizes.\n");
        DebugManager::Print(" DifX: %f\n", tXDist);
        DebugManager::Print(" DifY: %f\n", tYDist);
        DebugManager::Print(" X: %i\n", mLightmapXSize);
        DebugManager::Print(" Y: %i\n", mLightmapYSize);
    #endif

    //--Tolerances.
    float cTol = 0.001000f;

    //--Calculate the lightmap step rate across the polygon. Note that some of the values can be
    //  negative and still be legal.
    mLightmapXStep.mX = (tLightmapTR.mX - mLightmapTL.mX) / (float)mLightmapXSize;
    mLightmapXStep.mY = (tLightmapTR.mY - mLightmapTL.mY) / (float)mLightmapXSize;
    mLightmapXStep.mZ = (tLightmapTR.mZ - mLightmapTL.mZ) / (float)mLightmapXSize;
    if(mLightmapXStep.mX > -cTol && mLightmapXStep.mX < cTol) mLightmapXStep.mX = 0.0f;
    if(mLightmapXStep.mY > -cTol && mLightmapXStep.mY < cTol) mLightmapXStep.mY = 0.0f;
    if(mLightmapXStep.mZ > -cTol && mLightmapXStep.mZ < cTol) mLightmapXStep.mZ = 0.0f;

    //--Y Step rate.
    mLightmapYStep.mX = (mLightmapBR.mX - tLightmapTR.mX) / (float)mLightmapYSize;
    mLightmapYStep.mY = (mLightmapBR.mY - tLightmapTR.mY) / (float)mLightmapYSize;
    mLightmapYStep.mZ = (mLightmapBR.mZ - tLightmapTR.mZ) / (float)mLightmapYSize;
    if(mLightmapYStep.mX > -cTol && mLightmapYStep.mX < cTol) mLightmapYStep.mX = 0.0f;
    if(mLightmapYStep.mY > -cTol && mLightmapYStep.mY < cTol) mLightmapYStep.mY = 0.0f;
    if(mLightmapYStep.mZ > -cTol && mLightmapYStep.mZ < cTol) mLightmapYStep.mZ = 0.0f;

    //--Debug.
    #ifdef WP_DEBUG
        DebugManager::Print("Set step rates.\n");
        DebugManager::Print(" X: %6f %6f %6f\n", mLightmapXStep.mX, mLightmapXStep.mY, mLightmapXStep.mZ);
        DebugManager::Print(" Y: %6f %6f %6f\n", mLightmapYStep.mX, mLightmapYStep.mY, mLightmapYStep.mZ);
    #endif

    //--Store the lightmap positions for fast-access. Because the geometry is static, these don't
    //  change and this increases rendering speed.
    float tLftMost = 0.0f;
    float tTopMost = 0.0f;
    float tRgtMost = 0.0f;
    float tBotMost = 0.0f;
    if(false)
    {
        #ifdef WP_DEBUG
            DebugManager::Print("Precomputing lightmap coordinates.\n");
        #endif
        for(int i = 0; i < mPointsTotal; i ++)
        {
            //--If the step rate is zero, we have to ignore that as it doesn't contribute to the calculation.
            mLightmapCoords[i].mX = 0.0f;
            if(mLightmapXStep.mX != 0.0f) mLightmapCoords[i].mX = mLightmapCoords[i].mX + ((mPoints[i].mX - mLightmapTL.mX) / mLightmapXStep.mX / (float)mLightmapXSize);
            if(mLightmapXStep.mY != 0.0f) mLightmapCoords[i].mX = mLightmapCoords[i].mX + ((mPoints[i].mY - mLightmapTL.mY) / mLightmapXStep.mY / (float)mLightmapXSize);
            if(mLightmapXStep.mZ != 0.0f) mLightmapCoords[i].mX = mLightmapCoords[i].mX + ((mPoints[i].mZ - mLightmapTL.mZ) / mLightmapXStep.mZ / (float)mLightmapXSize);

            //--Ditto for the Y calc.
            mLightmapCoords[i].mY = 0.0f;
            if(mLightmapYStep.mX != 0.0f) mLightmapCoords[i].mY = mLightmapCoords[i].mY + ((mPoints[i].mX - mLightmapTL.mX) / mLightmapYStep.mX / (float)mLightmapYSize);
            if(mLightmapYStep.mY != 0.0f) mLightmapCoords[i].mY = mLightmapCoords[i].mY + ((mPoints[i].mY - mLightmapTL.mY) / mLightmapYStep.mY / (float)mLightmapYSize);
            if(mLightmapYStep.mZ != 0.0f) mLightmapCoords[i].mY = mLightmapCoords[i].mY + ((mPoints[i].mZ - mLightmapTL.mZ) / mLightmapYStep.mZ / (float)mLightmapYSize);

            //--Store.
            if(i == 0 || tLftMost > mLightmapCoords[i].mX) tLftMost = mLightmapCoords[i].mX;
            if(i == 0 || tTopMost > mLightmapCoords[i].mY) tTopMost = mLightmapCoords[i].mY;
            if(i == 0 || tRgtMost < mLightmapCoords[i].mX) tRgtMost = mLightmapCoords[i].mX;
            if(i == 0 || tBotMost < mLightmapCoords[i].mY) tBotMost = mLightmapCoords[i].mY;

            //--Debug.
            #ifdef WP_DEBUG
                //DebugManager::Print(" %3i: %f %f becomes %f %f\n", i, mPoints[i].mX, mPoints[i].mY, mLightmapCoords[i].mX, mLightmapCoords[i].mY);
            #endif
        }
    }

    //--Renormalize the coordinates.
    if(false)
    {
        float tXDif = tRgtMost - tLftMost;
        float tYDif = tBotMost - tTopMost;
        for(int i = 0; i < mPointsTotal; i ++)
        {
            if(tXDif > 0.0f) mLightmapCoords[i].mX = ((mLightmapCoords[i].mX - tLftMost) / tXDif);
            if(tYDif > 0.0f) mLightmapCoords[i].mY = ((mLightmapCoords[i].mY - tTopMost) / tYDif);

            //--Debug.
            #ifdef WP_DEBUG
                DebugManager::Print(" Renormalized %3i: %f %f\n", i, mLightmapCoords[i].mX, mLightmapCoords[i].mY);
            #endif
        }
    }


    //--Allocate.
    #ifdef WP_DEBUG
        DebugManager::Print("Allocating space.\n");
    #endif
    SetMemoryData(__FILE__, __LINE__);
    mLightmapData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * mLightmapXSize * mLightmapYSize * 4);
    for(int x = 0; x < mLightmapXSize; x ++)
    {
        for(int y = 0; y < mLightmapYSize; y ++)
        {
            int tCalcSlot = ((y * mLightmapXSize) + x) * 4;
            mLightmapData[tCalcSlot+0] = xLightmapAmbient;
            mLightmapData[tCalcSlot+1] = xLightmapAmbient;
            mLightmapData[tCalcSlot+2] = xLightmapAmbient;
            mLightmapData[tCalcSlot+3] = 255;
        }
    }

    //--Debug.
    #ifdef WP_DEBUG
        DebugManager::PopPrint("Completed setting lightmap sizes.\n");
    #endif
}
void WorldPolygon::AddLightmap(int pX, int pY, int pIntensity)
{
    //--Modifies the lightmap at the given position. Auto-checks for clamp cases.
    if(!mLightmapData) return;

    //--Calculate where the info goes.
    int tCalcSlot = ((pY * mLightmapXSize) + pX) * 4;

    //--Clamp intensity at 255.
    int tNewIntensity = mLightmapData[tCalcSlot+0] + pIntensity;
    if(tNewIntensity > 255) tNewIntensity = 255;

    //--Set.
    mLightmapData[tCalcSlot+0] = tNewIntensity;
    mLightmapData[tCalcSlot+1] = tNewIntensity;
    mLightmapData[tCalcSlot+2] = tNewIntensity;
    mLightmapData[tCalcSlot+3] = 255;
}
void WorldPolygon::FinalizeLightmap()
{
    //--Upload the lightmap as a greyscale luminance bitmap.
    SugarBitmap::xStaticFlags.mUplMagFilter = GL_NEAREST;
    SugarBitmap::xStaticFlags.mUplMinFilter = GL_NEAREST;
    SugarBitmap::xStaticFlags.mUplWrapS = GL_CLAMP_TO_EDGE;
    SugarBitmap::xStaticFlags.mUplWrapT = GL_CLAMP_TO_EDGE;

    //--Upload.
    mLightmap = new SugarBitmap(mLightmapData, mLightmapXSize, mLightmapYSize);

    #ifdef WP_DEBUG
        bool tFlag = false;
        if(mPointsTotal == 4 && mPoints[0].mZ == 16.0f && mPoints[1].mZ == 32.0f && mPoints[2].mZ == 32.0f && mPoints[3].mZ == 16.0f) tFlag = true;
        DebugManager::PushPrint(tFlag, "Finalizing lightmap.\n", mPointsTotal);
        for(int i = 0; i < mPointsTotal; i ++)
        {
            DebugManager::Print(" %3i: %6.2f %6.2f %6.2f\n", i, mPoints[i].mX, mPoints[i].mY, mPoints[i].mZ);
            DebugManager::Print("     %6.2f %6.2f\n", mLightmapCoords[i].mX, mLightmapCoords[i].mY);
        }
        DebugManager::PopPrint("Done.\n");
    #endif

    //--Clean.
    free(mLightmapData);
    mLightmapData = NULL;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
void WorldPolygon::WriteToBuffer(WADFile *pCaller, SugarAutoBuffer *pBuffer, bool pWriteTextureName)
{
    //--Write the internal information of the polygon to the provided SugarAutoBuffer.
    if(!pBuffer) return;

    //--Check if this polygon is a sky texture.
    mIsSkyPolygon = pCaller->IsSkyTexture(rTexture);

    //--Append how many points we have. This is as an 8-bit integer, since polygons never have
    //  256 or more points in normal circumstances.
    pBuffer->AppendUInt8(mPointsTotal);

    //--Append the positions of each point, along with their vertices. We also append the lightmap
    //  coordinates, even if there were no lightmaps built.
    for(int i = 0; i < mPointsTotal; i ++)
    {
        //--Position.
        pBuffer->AppendFloat(mPoints[i].mX);
        pBuffer->AppendFloat(mPoints[i].mY);
        pBuffer->AppendFloat(mPoints[i].mZ);

        //--Texture coordinates.
        pBuffer->AppendFloat(mTexCoords[i].mX);
        pBuffer->AppendFloat(mTexCoords[i].mY);

        //--Lightmap coordinates.
        pBuffer->AppendFloat(mLightmapCoords[i].mX);
        pBuffer->AppendFloat(mLightmapCoords[i].mY);
    }

    //--Texture information. If pWriteTextureName is flagged, we need to write the name of the texture
    //  from the WADFile. Otherwise, we write its index in the texture storage list.
    //--If no caller is provided, -1 is written for the texture's index.
    if(!pCaller)
    {
        pBuffer->AppendInt16(-1);
    }
    //--Append the index of the texture, which is backtraced from the caller. It's a 16-bit since it's unlikely we
    //  will run into more than 32,000 textures in one level.
    else if(!pWriteTextureName)
    {
        //--Normal case:
        if(!mIsSkyPolygon)
        {
            int16_t tIndex = pCaller->GetIndexOfCompressedTexture(rTexture);
            pBuffer->AppendInt16(tIndex);
        }
        //--Write -2 to indicate this is a sky texture.
        else
        {
            pBuffer->AppendInt16(-2);
        }
    }
    //--Append the name of the texture as a string. Under standard Doom storage, it will be 8 characters
    //  at most. We always append exactly 8, and pad with NULL if needed.
    else
    {
        pBuffer->AppendCharacter('D');
        pBuffer->AppendCharacter('u');
        pBuffer->AppendCharacter('m');
        pBuffer->AppendCharacter('m');
        pBuffer->AppendCharacter('y');
        pBuffer->AppendCharacter('T');
        pBuffer->AppendCharacter('e');
        pBuffer->AppendCharacter('x');
    }
}
void WorldPolygon::WriteLightmapToBuffer(SugarAutoBuffer *pBuffer)
{
    //--Appends the data of the lightmap to the provided buffer. This may not always be called. It also
    //  appends the X/Y size of the data, since they might not be the same size as the texture.
    if(!pBuffer) return;

    //--No local lightmap. Write zero for the sizes. Sky textures also don't store lightmaps.
    if(!mLightmap)
    {
        pBuffer->AppendInt16(0);
        pBuffer->AppendInt16(0);
        return;
    }

    //--Size an array out.
    int16_t tWid = mLightmap->GetWidth();
    int16_t tHei = mLightmap->GetHeight();
    SetMemoryData(__FILE__, __LINE__);
    void *tBuffer = starmemoryalloc(sizeof(uint8_t) * tWid * tHei * 4);

    //--Bind, get the data out.
    mLightmap->Bind();
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, tBuffer);

    //--Write.
    pBuffer->AppendInt16(tWid);
    pBuffer->AppendInt16(tHei);
    pBuffer->AppendVoidData(tBuffer, sizeof(uint8_t) * tWid * tHei * 4);

    //--Clean up.
    free(tBuffer);
}
void WorldPolygon::ReadFromFile(VirtualFile *fInfile)
{
    //--Given a file that is in position to read geometry data, reads that into this structure.
    if(!fInfile) return;

    //--Read how many points are expected.
    uint8_t tExpectedPoints = 0;
    fInfile->Read(&tExpectedPoints, sizeof(uint8_t), 1);
    Allocate(tExpectedPoints);

    //--Read the point data along with tex coordinates and lightmap coordinates.
    for(int i = 0; i < mPointsTotal; i ++)
    {
        //--Position.
        fInfile->Read(&mPoints[i].mX, sizeof(float), 1);
        fInfile->Read(&mPoints[i].mY, sizeof(float), 1);
        fInfile->Read(&mPoints[i].mZ, sizeof(float), 1);

        //--Texture coordinates.
        fInfile->Read(&mTexCoords[i].mX, sizeof(float), 1);
        fInfile->Read(&mTexCoords[i].mY, sizeof(float), 1);

        //--Lightmap coordinates.
        fInfile->Read(&mLightmapCoords[i].mX, sizeof(float), 1);
        fInfile->Read(&mLightmapCoords[i].mY, sizeof(float), 1);
    }

    //--Read the texture index we expect.
    fInfile->Read(&mTemporaryTextureIndex, sizeof(int16_t), 1);
}
void WorldPolygon::ReadLightmapFromFile(VirtualFile *fInfile, bool pPreserveRawData)
{
    //--Given a file that is in position to read lightmap data, reads and creates the needed lightmap.
    if(!fInfile) return;

    //--Read the X/Y size. If either is zero, we don't have a lightmap.
    int16_t tLightmapXSize = 0;
    int16_t tLightmapYSize = 0;
    fInfile->Read(&tLightmapXSize, sizeof(int16_t), 1);
    fInfile->Read(&tLightmapYSize, sizeof(int16_t), 1);
    if(tLightmapXSize == 0 || tLightmapYSize == 0)
    {
        //--Default pixel.
        SetMemoryData(__FILE__, __LINE__);
        mLightmapData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * 4);
        mLightmapData[0] = 192;
        mLightmapData[1] = 192;
        mLightmapData[2] = 192;
        mLightmapData[3] = 255;

        //--Upload.
        SugarBitmap::xStaticFlags.mUplMagFilter = GL_NEAREST;
        SugarBitmap::xStaticFlags.mUplMinFilter = GL_NEAREST;
        SugarBitmap::xStaticFlags.mUplWrapS = GL_REPEAT;
        SugarBitmap::xStaticFlags.mUplWrapT = GL_REPEAT;
        mLightmap = new SugarBitmap((uint8_t *)mLightmapData, 1, 1);
        return;
    }

    //--Debug lightmap.
    if(true)
    {
        //--Skip pixels.
        fInfile->SeekRelative(sizeof(uint8_t) * tLightmapXSize * tLightmapYSize * 4);

        //--Default pixel.
        SetMemoryData(__FILE__, __LINE__);
        mLightmapData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * 4);
        mLightmapData[0] = 192;
        mLightmapData[1] = 192;
        mLightmapData[2] = 192;
        mLightmapData[3] = 255;

        //--Upload.
        SugarBitmap::xStaticFlags.mUplMagFilter = GL_NEAREST;
        SugarBitmap::xStaticFlags.mUplMinFilter = GL_NEAREST;
        SugarBitmap::xStaticFlags.mUplWrapS = GL_REPEAT;
        SugarBitmap::xStaticFlags.mUplWrapT = GL_REPEAT;
        mLightmap = new SugarBitmap((uint8_t *)mLightmapData, 1, 1);
        return;
    }

    //--Read out the lightmap data.
    SetMemoryData(__FILE__, __LINE__);
    void *tDataBuffer = starmemoryalloc(sizeof(uint8_t) * tLightmapXSize * tLightmapYSize * 4);
    fInfile->Read(tDataBuffer, sizeof(uint8_t), tLightmapXSize * tLightmapYSize * 4);
    free(tDataBuffer);

    //--Create a new bitmap to represent the lightmap. It is stored internally.
   // mLightmap = new SugarBitmap((uint8_t *)tDataBuffer, tLightmapXSize, tLightmapYSize);

    //--Clean up. If flagged, we keep the lightmap data for later.
    if(pPreserveRawData)
    {
        mLightmapData = (uint8_t *)tDataBuffer;
    }
    else
    {
        free(tDataBuffer);
    }
}

//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
