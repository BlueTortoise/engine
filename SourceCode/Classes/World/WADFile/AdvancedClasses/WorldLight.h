//--[WorldLight]
//--Represents a light in the world. At present, lights are 3D points and not a lot else, not capable
//  of pointing in specific directions.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class WorldLight : public RootObject
{
    private:
    //--System
    //--Position
    Point3D mPosition;

    //--Properties
    float mIntensity;

    //--Shadow Lookups
    SugarLinkedList *mShadowCasterList;

    protected:

    public:
    //--System
    WorldLight();
    ~WorldLight();

    //--Public Variables
    static bool xSkipShadows;
    static float cxMinIntensity;
    static float cxShadowThreshold;
    static float cxLightDistanceScaler;
    static float cxShadowDistanceScaler;
    static float cxBackfaceDistanceScaler;

    //--Property Queries
    Point3D GetPosition();
    float GetIntensity();
    float GetIntensityByDistance(float pDistance);
    float GetIntensityAtPoint(float pX, float pY, float pZ, float pScaler);
    float GetIntensityAtPoint(Point3D pPoint, float pScaler);

    //--Manipulators
    void SetPosition(float pX, float pY, float pZ);
    void SetIntensity(float pIntensity);

    //--Core Methods
    void StoreShadowRefs(SugarLinkedList *pFaceList);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    SugarLinkedList *GetShadowCasterList();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

