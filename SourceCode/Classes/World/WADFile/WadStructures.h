//--[WADStructures]
//--Header containing structures commonly used in the 3D WAD format.

#pragma once

//--[Includes]
#include "WADFileMacros.h"

//--[Stock Structures]
//--These parts of the structure are read directly out of the WAD's file. Their sizes are specific.
typedef struct
{
    bool mIsFlat;
    int8_t mBelongsToMap;
    uint32_t mOffset;
    uint32_t mSize;
    char mType[9];
    uint8_t *mData;
}WAD_Directory_Entry;

typedef struct
{
    uint16_t mStartVertex;
    uint16_t mEndVertex;
    uint16_t mFlags;
    uint16_t mTypes;
    uint16_t mTag;
    int16_t mRightSidedef;
    int16_t mLeftSidedef;
}WAD_Linedef;

typedef struct
{
    int16_t mXOffset;
    int16_t mYOffset;
    char mUpperTex[8];
    char mLowerTex[8];
    char mMiddlTex[8];
    int16_t mSector;
}WAD_Sidedef;

typedef struct
{
    int16_t mX;
    int16_t mY;
}WAD_Vertex16i;

typedef struct
{
    //--Base Members
    int16_t mFloorHeight;
    int16_t mCeilingHeight;
    char mFloorName[8];
    char mCeilingName[8];
    int16_t mLightLevel;
    int16_t mSpecialFlag;
    int16_t mTag;

    //--Extension Functions
    bool IsCeilingSky()
    {
        return CompareEight(mCeilingName, "F_SKY1\0\0");
    }
    bool IsFloorSky()
    {
        return CompareEight(mFloorName, "F_SKY1\0\0");
    }
}WAD_Sector;

typedef struct
{
    int16_t mXOffset;
    int16_t mYOffset;
    int16_t mLumpNameIndex;
    int16_t mAlways1;
    int16_t mAlways0;
}WAD_Patch_Descriptor;

typedef struct
{
    char mName[8];
    int16_t mZeroA;
    int16_t mZeroB;
    int16_t mWidth;
    int16_t mHeight;
    int16_t mZeroC;
    int16_t mZeroD;
    int16_t mPatchesTotal;
    WAD_Patch_Descriptor *mPatches;
}WAD_Texture_Definition;

typedef struct
{
    int16_t mVertexStart;
    int16_t mVertexEnd;
    int16_t mAngle;
    int16_t mLinedef;
    int16_t mDirection;
    int16_t mOffset;
}WAD_Segment;

typedef struct
{
    int16_t mXPosition;
    int16_t mYPosition;
    int16_t mAngle;
    int16_t mType;
    int16_t mFlags;
}WAD_Thing;

//--[Advanced Structures]
//--These are not read directly out of the WAD file and instead have new variable types and methods
//  to make using them easier.

//--WAD_Level
//  Fast-access structure that points to everything else in the WAD file.
typedef struct
{
    bool mIsUDMF;
    int32_t mLevelNumber;
    WAD_Directory_Entry *rTextmapLump;
    WAD_Directory_Entry *rThingsLump;
    WAD_Directory_Entry *rLinedefLump;
    WAD_Directory_Entry *rSidedefLump;
    WAD_Directory_Entry *rVertexLump;
    WAD_Directory_Entry *rSegmentLump;
    WAD_Directory_Entry *rSSectorLump;
    WAD_Directory_Entry *rNodeLump;
    WAD_Directory_Entry *rSectorLump;
    WAD_Directory_Entry *rRejectLump;
    WAD_Directory_Entry *rBlockmapLump;
}WAD_Level;

//--WAD_FloorPoly
//--Represents a polygon on the floor. This is not the same as a sector, as floor polygons must be monotone
//  to be rendered correctly and sectors are just vertex listings that aren't guaranteed to be contiguous.
//--A WAD_FloorPoly still has an integer that associates it with the original sector if something needs to be
//  cross-referenced, such as timed lighting flags or animation.
#include "UDMFStructures.h"
typedef struct WAD_FloorPoly
{
    //--[Variables]
    //--System
    bool mHasBeenReprocessed;
    int32_t mPolygonCode;

    //--From Wad File
    int32_t mAssociatedSector;
    int16_t mFloorHeight;
    int16_t mCeilingHeight;

    //--Data
    int mVertexesTotal;
    UDMFVertex *mVertices;
    StarlightColor *mVertexColorsF;
    StarlightColor *mVertexColorsC;
    SugarLinkedList *mInternalPolygons;

    //--External References
    SugarBitmap *rFloorTexture;
    SugarBitmap *rCeilingTexture;
    WAD_FloorPoly *rEsconcingPolygon;

    //--3D Floors
    int m3DFloorsTotal;
    int *m3DFloorReferenceSlots;
    int *m3DFloorLinedefIndex;

    //--[Functions]
    void Initialize()
    {
        mHasBeenReprocessed = false;
        mAssociatedSector = -1;
        mPolygonCode = 0;
        mVertexesTotal = 0;
        mFloorHeight = 0;
        mCeilingHeight = 0;
        mVertices = NULL;
        mVertexColorsF = NULL;
        mVertexColorsC = NULL;
        rFloorTexture = NULL;
        rCeilingTexture = NULL;
        mInternalPolygons = NULL;
        rEsconcingPolygon = NULL;
        m3DFloorsTotal = 0;
        m3DFloorReferenceSlots = NULL;
        m3DFloorLinedefIndex = NULL;
    }
    void AllocVertices(int pCount)
    {
        mVertexesTotal = pCount;
        SetMemoryData(__FILE__, __LINE__);
        mVertices = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex) * mVertexesTotal);
        SetMemoryData(__FILE__, __LINE__);
        mVertexColorsF = (StarlightColor *)starmemoryalloc(sizeof(StarlightColor) * mVertexesTotal);
        SetMemoryData(__FILE__, __LINE__);
        mVertexColorsC = (StarlightColor *)starmemoryalloc(sizeof(StarlightColor) * mVertexesTotal);
        for(int i = 0; i < mVertexesTotal; i ++)
        {
            mVertices[i].Initialize();
            mVertexColorsF[i].SetRGBF(1.0f, 1.0f, 1.0f);
            mVertexColorsC[i].SetRGBF(1.0f, 1.0f, 1.0f);
        }
    }

    WAD_FloorPoly *Clone()
    {
        SetMemoryData(__FILE__, __LINE__);
        WAD_FloorPoly *nNewPoly = (WAD_FloorPoly *)starmemoryalloc(sizeof(WAD_FloorPoly));
        memcpy(nNewPoly, this, sizeof(WAD_FloorPoly));

        //--3D Floor reference data must be reallocated.
        if(nNewPoly->m3DFloorsTotal > 0)
        {
            SetMemoryData(__FILE__, __LINE__);
            nNewPoly->m3DFloorReferenceSlots = (int *)starmemoryalloc(sizeof(int) * nNewPoly->m3DFloorsTotal);
            memcpy(nNewPoly->m3DFloorReferenceSlots, m3DFloorReferenceSlots, sizeof(int) * nNewPoly->m3DFloorsTotal);
            SetMemoryData(__FILE__, __LINE__);
            nNewPoly->m3DFloorLinedefIndex = (int *)starmemoryalloc(sizeof(int) * nNewPoly->m3DFloorsTotal);
            memcpy(nNewPoly->m3DFloorLinedefIndex, m3DFloorLinedefIndex, sizeof(int) * nNewPoly->m3DFloorsTotal);
        }

        //--The vertex data must be reallocated.
        SetMemoryData(__FILE__, __LINE__);
        nNewPoly->mVertices = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex) * mVertexesTotal);
        memcpy(nNewPoly->mVertices, mVertices, sizeof(UDMFVertex) * mVertexesTotal);

        //--Color data must also be reallocated. It matches the size of the vertices.
        SetMemoryData(__FILE__, __LINE__);
        nNewPoly->mVertexColorsF = (StarlightColor *)starmemoryalloc(sizeof(StarlightColor) * mVertexesTotal);
        memcpy(nNewPoly->mVertexColorsF, mVertexColorsF, sizeof(StarlightColor) * mVertexesTotal);
        SetMemoryData(__FILE__, __LINE__);
        nNewPoly->mVertexColorsC = (StarlightColor *)starmemoryalloc(sizeof(StarlightColor) * mVertexesTotal);
        memcpy(nNewPoly->mVertexColorsC, mVertexColorsC, sizeof(StarlightColor) * mVertexesTotal);
        return nNewPoly;
    }
    static void DeleteThis(void *pPtr)
    {
        WAD_FloorPoly *rPtr = (WAD_FloorPoly *)pPtr;
        free(rPtr->mVertices);
        free(rPtr->mVertexColorsF);
        free(rPtr->mVertexColorsC);
        free(rPtr->m3DFloorReferenceSlots);
        free(rPtr->m3DFloorLinedefIndex);
        free(rPtr);
    }
}WAD_FloorPoly;

//--WAD_WallPoly
//--A polygon representing a wall in the game world. Always consists of exactly two vertices since
//  all wall polygons in Doom's renderer are quads.
//--Also contains some easy-access booleans to help rendering cases.
typedef struct
{
    //--Base Members
    bool mIs3DWall;
    float mXTexOffset;
    float mYTexOffset;
    float mVertX0;
    float mVertY0;
    float mVertZ0;
    float mVertX1;
    float mVertY1;
    float mVertZ1;

    //--Normal Vector
    bool mIsNormalComputed;
    float mNormalX, mNormalY, mNormalZ;

    //--Lighting
    StarlightColor mLights[4];

    //--Storage. Does not get overwritten for HD textures.
    float mTexStoreW;
    float mTexStoreH;

    //--Rendering Texture
    char mTexName[9];
    SugarBitmap *rTexture;

    //--Sector Association
    int32_t mAssociatedSector;

    //--Flags
    bool mIsTopAligned;
    bool mIsBotAligned;

    //--Functions
    void Initialize()
    {
        //--Base Members
        mIs3DWall = false;
        mXTexOffset = 0.0f;
        mYTexOffset = 0.0f;
        mVertX0 = 0.0f;
        mVertY0 = 0.0f;
        mVertZ0 = 0.0f;
        mVertX1 = 1.0f;
        mVertY1 = 1.0f;
        mVertZ1 = 1.0f;

        //--Normal Vector
        mIsNormalComputed = false;
        mNormalX = 0.0f;
        mNormalY = 0.0f;
        mNormalZ = 0.0f;

        //--Lighting
        mLights[0].SetRGBF(1.0f, 1.0f, 1.0f);
        mLights[1].SetRGBF(1.0f, 1.0f, 1.0f);
        mLights[2].SetRGBF(1.0f, 1.0f, 1.0f);
        mLights[3].SetRGBF(1.0f, 1.0f, 1.0f);

        //--Storage
        mTexStoreW = 0.0f;
        mTexStoreH = 0.0f;

        //--Rendering Texture
        memset(mTexName, 0, sizeof(char) * 9);
        rTexture = NULL;

        //--Sector Association
        mAssociatedSector = -1;

        //--Flags
        mIsTopAligned = false;
        mIsBotAligned = false;
    }
}WAD_WallPoly;

//--WAD_Texture
//--Wrapper around a SugarBitmap pointer. Is considered to own its own data.
typedef struct
{
    //--Members
    char mName[9];
    SugarBitmap *mImage;

    //--Functions
    void Initialize()
    {
        memset(mName, 0, sizeof(char) * 9);
        mImage = NULL;
    }
}WAD_Texture;

//--WAD_ThingPackage
//--Represents a lookup used in the THINGS lump. Contains the 4-letter sprite information and some information
//  on positioning.
typedef struct
{
    //--Mandatory
    int mTypeCode;
    char mNameCode[9];

    //--Flags
    bool mHangsFromCeiling;

    //--Functions
    void Set(int pCode, const char *pString, uint16_t pFlags)
    {
        if(!pString) return;
        mTypeCode = pCode;
        memset(mNameCode, 0, sizeof(char) * 9);
        mNameCode[0] = pString[0];
        mNameCode[1] = pString[1];
        mNameCode[2] = pString[2];
        mNameCode[3] = pString[3];
        mHangsFromCeiling = pFlags & 0x01;
    }
}WAD_ThingLookup;

//--WAD_WorldSprite
//--Represents a sprite in the world, which can be things like lamps, bodies, gore, and so on.
typedef struct WAD_WorldSprite
{
    //--Position
    float mX;
    float mY;
    float mZ;

    //--Rendering
    char mName[9];
    SugarBitmap *rImage;

    //--Lighting. Optional.
    float mLightValue;

    //--Additional data. May be post-processed back later to represent objects other than sprites.
    int mUniversalType;
    float mArgs[5];

    //--Functions
    void Initialize()
    {
        //--Position
        mX = 0.0f;
        mY = 0.0f;
        mZ = 0.0f;

        //--Rendering
        CopyEight(mName, "-\0\0\0\0\0\0\0");
        mName[8] = '\0';
        rImage = NULL;

        //--Lighting. Optional.
        mLightValue = 1.0f;

        //--Additional Data.
        mUniversalType = 0;
        memset(mArgs, 0, sizeof(float) * 5);
    }
    static WAD_WorldSprite *Construct(float pX, float pY, float pZ, const char *pName)
    {
        //--Base.
        SetMemoryData(__FILE__, __LINE__);
        WAD_WorldSprite *nSprite = (WAD_WorldSprite *)starmemoryalloc(sizeof(WAD_WorldSprite));

        //--Position
        nSprite->mX = pX;
        nSprite->mY = pY;
        nSprite->mZ = pZ;

        //--Rendering
        memcpy(nSprite->mName, pName, sizeof(char) * 8);
        nSprite->rImage = NULL;

        //--Lighting.
        nSprite->mLightValue = 1.0f;

        //--Pass back.
        return nSprite;
    }
}WAD_WorldSprite;
