//--Base
#include "WADFile.h"

//--Classes
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

//--Subroutine for rendering a 3D floor in a given sector. This subroutine performs all necessary
//  verification, as in, a malformed level should not cause a crash here.
//--3D floors are done during the floor/ceiling sequence, but they do contain wall polygons of their
//  own which are done during the wall poly update. This part only handles floors/ceilings. This
//  file contains the routine responsible for building those 3D wall polygons, though.

//--[Rendering Function]
void WADFile::Render3DFloor(WAD_FloorPoly *pFloorPolygon)
{
    //--Given a floor polygon, checks if the polygon contains a legal 3D floor and renders it, if so.
    //  This can safely be done inside a glList execution.
    if(!pFloorPolygon) return;

    //--Loop.
    for(int i = 0; i < pFloorPolygon->m3DFloorsTotal; i ++)
    {
        //--Check to make sure the sector identifier is within range.
        if(pFloorPolygon->m3DFloorReferenceSlots[i] < 0 || pFloorPolygon->m3DFloorReferenceSlots[i] >= mSectorsTotal) continue;

        //--Get the control sector.
        UDMFSector *rControlSector = &mSectors[pFloorPolygon->m3DFloorReferenceSlots[i]];

        //--Render the top of the 3D floor. The ceiling of the control sector is the upper floor of the 3D plane.
        SugarBitmap *rFlat = GetFlatTexture(rControlSector->mTexCeiling);
        SetSkyboxStencilState(rFlat == rSkyTexture);
        if(rFlat)
        {
            //--Render. We never use the vertex Z's for this, 3D floors cannot have slopes.
            rFlat->Bind();
            glBegin(GL_POLYGON);
                for(int p = 0; p < pFloorPolygon->mVertexesTotal; p ++)
                {
                    glTexCoord2f(pFloorPolygon->mVertices[p].mX / 64.0f, pFloorPolygon->mVertices[p].mY / 64.0f);
                    glVertex3f(pFloorPolygon->mVertices[p].mX, pFloorPolygon->mVertices[p].mY, rControlSector->mHeightCeiling);
                }
            glEnd();
        }

        //--Render the bottom of the 3D floor. The floor of the control sector is the lower ceiling of the 3D plane.
        rFlat = GetFlatTexture(rControlSector->mTexFloor);
        SetSkyboxStencilState(rFlat == rSkyTexture);
        if(rFlat)
        {
            //--Render. We never use the vertex Z's for this, 3D floors cannot have slopes.
            rFlat->Bind();
            glBegin(GL_POLYGON);
                for(int p = 0; p < pFloorPolygon->mVertexesTotal; p ++)
                {
                    glTexCoord2f(pFloorPolygon->mVertices[p].mX / 64.0f, pFloorPolygon->mVertices[p].mY / 64.0f);
                    glVertex3f(pFloorPolygon->mVertices[p].mX, pFloorPolygon->mVertices[p].mY, rControlSector->mHeightFloor);
                }
            glEnd();
        }
    }
}

//--[Builder Function]
void WADFile::Build3DFloorInfo()
{
    //--Entry point. Once all polygons have been decomposed into their smallest members, this routine checks for 3D floor
    //  references in all floor polygons. New wall polygons will be built as necessary.
    //--In UDMF mode this will work. It will still work in normal WAD mode, but the linedef specials necessary will never
    //  be set, so the routine won't do anything.

    //--No go through all the floor polygons.
    for(int i = 0; i < mFloorPolysTotal; i ++)
    {
        //--Clear.
        free(mFloorPolys[i].m3DFloorReferenceSlots);
        mFloorPolys[i].m3DFloorReferenceSlots = NULL;
        mFloorPolys[i].m3DFloorsTotal = 0;

        //--Check the associated sector of this floor poly. It must have a non-zero tag, or it's not of interest.
        if(mFloorPolys[i].mAssociatedSector < 0 || mFloorPolys[i].mAssociatedSector >= mSectorsTotal || mSectors[mFloorPolys[i].mAssociatedSector].mID == 0) continue;

        //--Sector for fast-access.
        UDMFSector *rBaseSector = &mSectors[mFloorPolys[i].mAssociatedSector];

        //--The sector has something of interest. We need to check if there is a linedef pointing to this sector
        //  and having a 3D floor flagged on it. Multiple linedefs can do this!
        for(int p = 0; p < mLinedefsTotal; p ++)
        {
            //--If we don't have a 3D floor, or if not pointing at this sector, we don't care about it.
            if(!mLinedefs[p].mHas3DFloor || mLinedefs[p].m3DFloorTag != rBaseSector->mID) continue;

            //--This linedef matches. Get the sector it points to. It must be legal!
            if(mLinedefs[p].mSidedefR < 0 || mLinedefs[p].mSidedefR >= mSidedefsTotal) continue;
            if(mSidedefs[mLinedefs[p].mSidedefR].mSector < 0 || mSidedefs[mLinedefs[p].mSidedefR].mSector >= mSectorsTotal) continue;

            //--This is a legal match. Allocate new space.
            mFloorPolys[i].m3DFloorsTotal ++;
            mFloorPolys[i].m3DFloorReferenceSlots = (int *)realloc(mFloorPolys[i].m3DFloorReferenceSlots, sizeof(int) * mFloorPolys[i].m3DFloorsTotal);
            mFloorPolys[i].m3DFloorLinedefIndex   = (int *)realloc(mFloorPolys[i].m3DFloorLinedefIndex,   sizeof(int) * mFloorPolys[i].m3DFloorsTotal);

            //--Put the ID in.
            //fprintf(stderr, "Floor poly %i has a 3D floor in sector %i\n", i, mSidedefs[mLinedefs[p].mSidedefR].mSector);
            mFloorPolys[i].m3DFloorReferenceSlots[mFloorPolys[i].m3DFloorsTotal - 1] = mSidedefs[mLinedefs[p].mSidedefR].mSector;
            mFloorPolys[i].m3DFloorLinedefIndex[mFloorPolys[i].m3DFloorsTotal - 1] = p;

            //--Build extra wall polys.
            BuildWallsFor3DFloors(&mFloorPolys[i], &mLinedefs[p], &mSectors[mSidedefs[mLinedefs[p].mSidedefR].mSector]);
        }
    }

    //--Do the same thing on the global texture list. This is the list that is checked for some height operations.
    //  We don't build walls with this list though.
    WAD_FloorPoly *rPolygon = (WAD_FloorPoly *)mGlobalTexturePolyList->PushIterator();
    while(rPolygon)
    {
        //--Check the associated sector of this floor poly. It must have a non-zero tag, or it's not of interest.
        if(rPolygon->mAssociatedSector < 0 || rPolygon->mAssociatedSector >= mSectorsTotal || mSectors[rPolygon->mAssociatedSector].mID == 0)
        {
            rPolygon = (WAD_FloorPoly *)mGlobalTexturePolyList->AutoIterate();
            continue;
        }

        //--Sector for fast-access.
        UDMFSector *rBaseSector = &mSectors[rPolygon->mAssociatedSector];

        //--The sector has something of interest. We need to check if there is a linedef pointing to this sector
        //  and having a 3D floor flagged on it. Multiple linedefs can do this!
        for(int p = 0; p < mLinedefsTotal; p ++)
        {
            //--Ignore all linedefs not flagged for 3D, or not pointing at this sector.
            if(!mLinedefs[p].mHas3DFloor || mLinedefs[p].m3DFloorTag != rBaseSector->mID) continue;

            //--This linedef matches. Get the sector it points to. It must be legal!
            if(mLinedefs[p].mSidedefR < 0 || mLinedefs[p].mSidedefR >= mSidedefsTotal) continue;
            if(mSidedefs[mLinedefs[p].mSidedefR].mSector < 0 || mSidedefs[mLinedefs[p].mSidedefR].mSector >= mSectorsTotal) continue;

            //--This is a legal match. Allocate new space.
            rPolygon->m3DFloorsTotal ++;
            rPolygon->m3DFloorReferenceSlots = (int *)realloc(rPolygon->m3DFloorReferenceSlots, sizeof(int) * rPolygon->m3DFloorsTotal);

            //--Put the ID in.
            //fprintf(stderr, "Floor poly %i has a 3D floor in sector %i\n", i, mSidedefs[mLinedefs[p].mSidedefR].mSector);
            rPolygon->m3DFloorReferenceSlots[rPolygon->m3DFloorsTotal - 1] = mSidedefs[mLinedefs[p].mSidedefR].mSector;
        }

        rPolygon = (WAD_FloorPoly *)mGlobalTexturePolyList->AutoIterate();
    }
}
void WADFile::BuildWallsFor3DFloors(WAD_FloorPoly *pSourcePoly, UDMFLinedef *pSourceLine, UDMFSector *pControlSector)
{
    //--Builds walls that are suspended in the air and appends them to the wall poly array. Because the polygons
    //  have probably already undergone decomposition, a wall poly is not created unless it happens to coincide
    //  with a linedef (though the vertices might be backwards).
    if(!pSourcePoly || !pSourceLine || !pControlSector) return;

    //--Make sure the sidedef on the right side is legal.
    if(pSourceLine->mSidedefR < 0 || pSourceLine->mSidedefR >= mSidedefsTotal) return;

    //--Start scanning along the source poly's vertices.
    for(int i = 0; i < pSourcePoly->mVertexesTotal; i ++)
    {
        //--Zeroth and Oneth vertex. If we're at the last vertex, loop back around.
        UDMFVertex *rVertex0 = &pSourcePoly->mVertices[i];
        UDMFVertex *rVertex1 = &pSourcePoly->mVertices[(i+1) % pSourcePoly->mVertexesTotal];

        //--Compare this against all of the original linedefs.
        for(int p = 0; p < mLinedefsTotal; p ++)
        {
            //--Get the matching vertices. Check that they're not out of range.
            if(mLinedefs[p].mStartVertex < 0 || mLinedefs[p].mStartVertex >= mVerticesTotal) continue;
            if(mLinedefs[p].mEndVertex   < 0 || mLinedefs[p].mEndVertex   >= mVerticesTotal) continue;

            //--Compare to see if the vertices match the linedef exactly. This can be backwards!
            if((rVertex0->CompareTo(mVertices[mLinedefs[p].mStartVertex]) && rVertex1->CompareTo(mVertices[mLinedefs[p].mEndVertex])) ||
               (rVertex1->CompareTo(mVertices[mLinedefs[p].mStartVertex]) && rVertex0->CompareTo(mVertices[mLinedefs[p].mEndVertex])))
            {
                //--Need a new wall poly here. Allocate space.
                int h = mWallPolysTotal;
                mWallPolysTotal ++;
                mWallPolys = (WAD_WallPoly *)realloc(mWallPolys, sizeof(WAD_WallPoly) * mWallPolysTotal);

                //--Init.
                mWallPolys[h].Initialize();
                mWallPolys[h].mIs3DWall = true;

                //--Fill data.
                mWallPolys[h].mVertX0 = mVertices[mLinedefs[p].mVertex0].mX;
                mWallPolys[h].mVertY0 = mVertices[mLinedefs[p].mVertex0].mY;
                mWallPolys[h].mVertZ0 = pControlSector->mHeightCeiling;
                mWallPolys[h].mVertX1 = mVertices[mLinedefs[p].mVertex1].mX;
                mWallPolys[h].mVertY1 = mVertices[mLinedefs[p].mVertex1].mY;
                mWallPolys[h].mVertZ1 = pControlSector->mHeightFloor;

                //--Texturing.
                mWallPolys[h].mXTexOffset = mSidedefs[pSourceLine->mSidedefR].mOffsetX;
                mWallPolys[h].mYTexOffset = mSidedefs[pSourceLine->mSidedefR].mOffsetY;
                mWallPolys[h].rTexture = GetWallTexture(mSidedefs[pSourceLine->mSidedefR].mTexMid);
                CopyEight(mWallPolys[h].mTexName, mSidedefs[pSourceLine->mSidedefR].mTexMid);

                //--Texture sizing.
                if(mWallPolys[h].rTexture)
                {
                    mWallPolys[h].mTexStoreW = mWallPolys[h].rTexture->GetTrueWidth();
                    mWallPolys[h].mTexStoreH = mWallPolys[h].rTexture->GetTrueHeight();
                }

                //--Sector association.
                mWallPolys[h].mAssociatedSector = mSidedefs[pSourceLine->mSidedefR].mSector;
                break;
            }
        }
    }
}
