//--Base
#include "WADFile.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

int WADFile::GetLineIntersection(float pX0, float pY0, float pX1, float pY1, float pX2, float pY2, float pX3, float pY3, float *pInterceptX, float *pInterceptY)
{
    //--Worker function, returns whether or not two lines intersect. If they do, pInterceptX and pInterceptY will be populated with their
    //  intersection point. The intercept pointers are optional, pass NULL if you don't need them.
    //--Returns 0 if there's no interception at all.
    //--Returns 1 if there is an interception.
    //--Returns 2 if the lines are colinear (which normally is not an interception).

    //--[Base Computations]
    //--This gets the dot-product of the lines.
    float s02_x, s02_y, s10_x, s10_y, s32_x, s32_y, s_numer, t_numer, denom, t;
    s10_x = pX1 - pX0;
    s10_y = pY1 - pY0;
    s32_x = pX3 - pX2;
    s32_y = pY3 - pY2;

    //--[Colinearity]
    //--If the result is colinear, we need to check if any one point actually collides.
    denom = s10_x * s32_y - s32_x * s10_y;
    if(denom == 0)
    {
        //--First, we need to check that the angle between point 0 and 1 is the same as the angle between
        //  point 0 and 2. If it's not, the lines can't intersect.
        float cEpsilon = 0.00010f;
        float tAngle01 = atan2f(pY1 - pY0, pX1 - pX0);
        float tAngle02 = atan2f(pY2 - pY0, pX2 - pX0);

        //--If the start points for both lines happen to be the same, we need to use the end points instead.
        if(pX0 == pX2 && pY0 == pY2)
        {
            tAngle02 = atan2f(pY3 - pY0, pX3 - pX0);
        }

        //--Renormalize the angles. 180 degrees is the same as 0 degrees!
        if(tAngle01 < 0.0f) tAngle01 = tAngle01 + (3.1415926f*2.0f);
        if(tAngle02 < 0.0f) tAngle02 = tAngle02 + (3.1415926f*2.0f);
        if(tAngle01 > 3.1415926f - cEpsilon) tAngle01 = tAngle01 - 3.1415926f;
        if(tAngle02 > 3.1415926f - cEpsilon) tAngle02 = tAngle02 - 3.1415926f;

        //--If they aren't exactly equal, then they can't intersect. Return 0, no hit.
        if(fabsf(tAngle01 - tAngle02) > cEpsilon) return 0;

        //--If we got this far, it's possible there's a collision. If the distance between either
        //  0-2 or 0-3 is less than the distance from 0-1, then we have a collision!
        float tDistance01 = sqrtf(((pX0 - pX1) * (pX0 - pX1)) + ((pY0 - pY1) * (pY0 - pY1)));
        float tDistance02 = sqrtf(((pX0 - pX2) * (pX0 - pX2)) + ((pY0 - pY2) * (pY0 - pY2)));
        float tDistance03 = sqrtf(((pX0 - pX3) * (pX0 - pX3)) + ((pY0 - pY3) * (pY0 - pY3)));

        //--Point 2 is on the line to between 0 and 1. Return the location of point 2.
        if(tDistance02 <= tDistance01)
        {
            if(pInterceptX) *pInterceptX = pX2;
            if(pInterceptY) *pInterceptY = pY2;
            return 2;
        }

        //--Point 3 is on the line to between 0 and 1. Return the location of point 3.
        if(tDistance03 <= tDistance01)
        {
            if(pInterceptX) *pInterceptX = pX3;
            if(pInterceptY) *pInterceptY = pY3;
            return 2;
        }

        //--Neither is on the line, so return 0 to indicate colinear non-interception.
        return 0;
    }

    //--[Point Edge Cases]
    //--The dot-product denom case will miss if the start point of the lines is directly in
    //  the other line, so we check that here. This loses priority against colinearity.
    float tAngleA = atan2f(pY1 - pY0, pX1 - pX0);
    float tAngleB = atan2f(pY2 - pY0, pX2 - pX0);
    float tAngleC = atan2f(pY3 - pY0, pX3 - pX0);
    if(fabsf(tAngleA - tAngleB) < 0.0001f)
    {
        float tDistanceA = sqrtf(((pX0 - pX1) * (pX0 - pX1)) + ((pY0 - pY1) * (pY0 - pY1)));
        float tDistanceB = sqrtf(((pX0 - pX2) * (pX0 - pX2)) + ((pY0 - pY2) * (pY0 - pY2)));
        if(tDistanceA >= tDistanceB)
        {
            if(pInterceptX) *pInterceptX = pX2;
            if(pInterceptY) *pInterceptY = pY2;
            return 1;
        }
    }
    if(fabsf(tAngleA - tAngleC) < 0.0001f)
    {
        float tDistanceA = sqrtf(((pX0 - pX1) * (pX0 - pX1)) + ((pY0 - pY1) * (pY0 - pY1)));
        float tDistanceB = sqrtf(((pX0 - pX3) * (pX0 - pX3)) + ((pY0 - pY3) * (pY0 - pY3)));
        if(tDistanceA >= tDistanceB)
        {
            if(pInterceptX) *pInterceptX = pX3;
            if(pInterceptY) *pInterceptY = pY3;
            return 1;
        }
    }

    //--Check if the start point of the first line lies along the second line.
    tAngleA = atan2f(pY3 - pY2, pX3 - pX2);
    tAngleB = atan2f(pY0 - pY2, pX0 - pX2);
    tAngleC = atan2f(pY1 - pY2, pX1 - pX2);
    if(fabsf(tAngleA - tAngleB) < 0.0001f)
    {
        float tDistanceA = sqrtf(((pX2 - pX3) * (pX2 - pX3)) + ((pY2 - pY3) * (pY2 - pY3)));
        float tDistanceB = sqrtf(((pX2 - pX0) * (pX2 - pX0)) + ((pY2 - pY0) * (pY2 - pY0)));
        if(tDistanceA >= tDistanceB)
        {
            if(pInterceptX) *pInterceptX = pX0;
            if(pInterceptY) *pInterceptY = pY0;
            return 1;
        }
    }
    if(fabsf(tAngleA - tAngleC) < 0.0001f)
    {
        float tDistanceA = sqrtf(((pX2 - pX3) * (pX2 - pX3)) + ((pY2 - pY3) * (pY2 - pY3)));
        float tDistanceB = sqrtf(((pX2 - pX1) * (pX2 - pX1)) + ((pY2 - pY1) * (pY2 - pY1)));
        if(tDistanceA >= tDistanceB)
        {
            if(pInterceptX) *pInterceptX = pX1;
            if(pInterceptY) *pInterceptY = pY1;
            return 1;
        }
    }

    //--Positive denominator check.
    bool denomPositive = (denom > 0);

    s02_x = pX0 - pX2;
    s02_y = pY0 - pY2;
    s_numer = s10_x * s02_y - s10_y * s02_x;
    if ((s_numer < 0) == denomPositive) return 0;

    t_numer = s32_x * s02_y - s32_y * s02_x;
    if ((t_numer < 0) == denomPositive) return 0;

    //--No collision.
    if (((s_numer > denom) == denomPositive) || ((t_numer > denom) == denomPositive)) return 0;

    //--If we got this far, there's a collision.
    t = t_numer / denom;
    if(pInterceptX) *pInterceptX = pX0 + (t * s10_x);
    if(pInterceptY) *pInterceptY = pY0 + (t * s10_y);

    return 1;
}
WAD_FloorPoly *WADFile::FindContainingFloorPoly(float pXPoint, float pYPoint, SugarLinkedList *pPolyList, bool pIsSubCall)
{
    //--Worker function, finds and returns the WAD_FloorPoly that is in the polygon list provided that contains the given point.
    //  Note that while multiple polygons can legally contain the same point, a polygon can only contain that point if it is
    //  the "lowest". If a point is contained by multiple polygons, the polygon that contains no other polygons is the true owner.
    //--Will return NULL if no polygons own this point.
    if(!pPolyList || pPolyList->GetListSize() < 1) return NULL;

    //--Debug.
    bool tFlag = false;
    DebugManager::PushPrint(tFlag, "Scanning for associated sector information, %.0f %.0f.\n", pXPoint, pYPoint);

    //--First, create a list that will store all the polygons that own this point.
    WAD_FloorPoly *rReturnPoly = NULL;
    SugarLinkedList *tMatchList = new SugarLinkedList(false);

    //--Begin iterating.
    WAD_FloorPoly *rPolygon = (WAD_FloorPoly *)pPolyList->PushIterator();
    while(rPolygon)
    {
        //--Debug.
        if(pIsSubCall) DebugManager::Print(" Check polygon %i/%i\n", rPolygon->mPolygonCode, rPolygon->mAssociatedSector);

        //--Check for a collision.
        bool tIsWithinPoly = false;
        for(int i = 0; i < rPolygon->mVertexesTotal; i ++)
        {
            //--Get the slot of the next vertex.
            int j = ((i + 1) % rPolygon->mVertexesTotal);

            //--Checks.
            if(((rPolygon->mVertices[i].mY > pYPoint) != (rPolygon->mVertices[j].mY > pYPoint)) &&
               (pXPoint < (rPolygon->mVertices[j].mX - rPolygon->mVertices[i].mX) * (pYPoint - rPolygon->mVertices[i].mY) / (rPolygon->mVertices[j].mY - rPolygon->mVertices[i].mY) + rPolygon->mVertices[i].mX))
            {
                tIsWithinPoly = !tIsWithinPoly;
            }
        }

        //--After this, if tIsWithinPoly is true (it must qualify an odd number of times) then we're within the polygon. Add it.
        if(tIsWithinPoly)
        {
            //--If this was a sub-call, we don't need to do anything further. We're only checking if the point is within our bounds,
            //  and we don't care exactly which sub-poly contains it.
            if(pIsSubCall)
            {
                DebugManager::PopPrint("During sub-call, poly %i/%i contains the point.\n", rPolygon->mPolygonCode, rPolygon->mAssociatedSector);
                delete tMatchList;
                return rPolygon;
            }

            //--If this polygon contains the point, but one of its sub-polygons also contains the point, it cannot be added.
            DebugManager::Print("Point is within polygon %i/%i, checking sub-polygons...\n", rPolygon->mPolygonCode, rPolygon->mAssociatedSector);
            WAD_FloorPoly *rSubPoly = FindContainingFloorPoly(pXPoint, pYPoint, rPolygon->mInternalPolygons, true);
            if(rSubPoly)
            {
                DebugManager::Print("Polygon %i/%i contained the point, but a sub-polygon %i also contained it. Failing.\n", rPolygon->mPolygonCode, rPolygon->mAssociatedSector, rSubPoly->mPolygonCode);
            }
            //--Success!
            else
            {
                tMatchList->AddElement("X", rPolygon);
                DebugManager::Print("Adding polygon %i/%i to the match list.\n", rPolygon->mPolygonCode, rPolygon->mAssociatedSector);
            }
        }

        //--Next.
        rPolygon = (WAD_FloorPoly *)pPolyList->AutoIterate();
    }

    //--There were no owning polygons.
    if(tMatchList->GetListSize() < 1)
    {
        DebugManager::Print("No matches for internal sector.\n");
    }
    //--There was exactly one owning poly, so that's the one.
    else if(tMatchList->GetListSize() == 1)
    {
        rReturnPoly = (WAD_FloorPoly *)tMatchList->GetElementBySlot(0);
        DebugManager::Print("Exactly one match for internal sector: %i\n", rReturnPoly->mAssociatedSector);
    }
    //--If we got more than one match...
    else
    {
        DebugManager::Print("Got %i matches. Scanning.\n", tMatchList->GetListSize());
        WAD_FloorPoly *rPolygon = (WAD_FloorPoly *)tMatchList->PushIterator();
        while(rPolygon)
        {
            //--If we have polygons
            if(!rPolygon->rEsconcingPolygon)
            {
                DebugManager::Print(" Polygon %i/%i somehow had no esconcing sector. Returning.\n", rPolygon->mPolygonCode, rPolygon->mAssociatedSector);
                rReturnPoly = rPolygon;
                tMatchList->PopIterator();
                break;
            }
            //--Having two matches means that this is an internal duplicate case. If that happens, then one of the polys
            //  will be esconced by a bigger poly matching its sector. Don't pick that one.
            else if(rPolygon->rEsconcingPolygon->mAssociatedSector != rPolygon->mAssociatedSector)
            {
                DebugManager::Print(" Polygon %i/%i has a different esconcing sector code. Returning.\n", rPolygon->mPolygonCode, rPolygon->mAssociatedSector);
                rReturnPoly = rPolygon;
                tMatchList->PopIterator();
                break;
            }

            rPolygon = (WAD_FloorPoly *)tMatchList->AutoIterate();
        }

        //DebugManager::ForcePrint("WADFile:FindContainingFloorPoly - Error, a point should not be within two polygons not within each other!\n");
    }

    //--Debug.
    DebugManager::PopPrint("Scan complete.\n");

    //--Finish up.
    delete tMatchList;
    return rReturnPoly;
}
WAD_FloorPoly *WADFile::FindContainingFloorPoly(float pXPoint, float pYPoint)
{
    //--Identical to the above function, but doesn't check against a given polygon list. Instead, checks against decomposed floor
    //  polygons in a statically sized list.
    //--In the finalized list, a floor poly can never contain another polygon (the binary mesh fixes that).
    for(int p = 0; p < mFloorPolysTotal; p ++)
    {
        //--Check for a collision.
        bool tIsWithinPoly = false;
        for(int i = 0; i < mFloorPolys[p].mVertexesTotal; i ++)
        {
            //--Get the slot of the next vertex.
            int j = ((i + 1) % mFloorPolys[p].mVertexesTotal);

            //--Checks.
            if(((mFloorPolys[p].mVertices[i].mY > pYPoint) != (mFloorPolys[p].mVertices[j].mY > pYPoint)) &&
               (pXPoint < (mFloorPolys[p].mVertices[j].mX - mFloorPolys[p].mVertices[i].mX) * (pYPoint - mFloorPolys[p].mVertices[i].mY) / (mFloorPolys[p].mVertices[j].mY - mFloorPolys[p].mVertices[i].mY) + mFloorPolys[p].mVertices[i].mX))
            {
                tIsWithinPoly = !tIsWithinPoly;
            }
        }

        //--After this, if tIsWithinPoly is true (it must qualify an odd number of times) then we're within the polygon. Return it.
        if(tIsWithinPoly)
        {
            return &mFloorPolys[p];
        }
    }

    //--No matches.
    return NULL;
}
bool WADFile::MarkDuplicateInternals(SugarLinkedList *pPolygonList)
{
    //--Given a list of WAD_FloorPolys, scans the list and removes any two polygons who are exact
    //  duplicates. The list is modified in the process, so iterators will become unstable.
    //--In normal composition, this *must* be done before polygons check for enclosed polygons, since
    //  two polygons with identical vertices will be said to enclose one another.
    //--Returns true if anything got removed, false otherwise.
    if(!pPolygonList) return false;

    //--Debug,
    DebugManager::PushPrint(false, "Marking and removing exact duplicates...\n");

    //--Loop through all the polygons...
    WAD_FloorPoly *rPolygon = (WAD_FloorPoly *)pPolygonList->PushIterator();
    while(rPolygon)
    {
        //--Scan through the polygons inside this one.
        WAD_FloorPoly *rInsidePolygon = (WAD_FloorPoly *)rPolygon->mInternalPolygons->SetToHeadAndReturn();
        while(rInsidePolygon)
        {
            //--If the interior polygon points at the same sector as the exterior one, it's redundant. Mark for purge.
            //  However, we can't purge a polygon if it has internal polys of its own.
            if(rInsidePolygon->mAssociatedSector == rPolygon->mAssociatedSector)
            {
                MarkPoly(rInsidePolygon->mPolygonCode, pPolygonList);

                rInsidePolygon->rCeilingTexture = NULL;
                rInsidePolygon->rFloorTexture = NULL;
                DebugManager::Print("Polygon %i/%i surrounds %i/%i and they point at the same sector.\n", rPolygon->mPolygonCode, rPolygon->mAssociatedSector, rInsidePolygon->mPolygonCode, rInsidePolygon->mAssociatedSector);
            }

            //--If the interior polygon was marked (by this poly or another), remove it. This keeps the list from
            //  pointing at deallocated polygons.
            if(rInsidePolygon->rCeilingTexture == NULL && rInsidePolygon->rFloorTexture == NULL)
            {
                rPolygon->mInternalPolygons->RemoveRandomPointerEntry();
            }

            //--Next.
            rInsidePolygon = (WAD_FloorPoly *)rPolygon->mInternalPolygons->IncrementAndGetRandomPointerEntry();
        }

        //--If our esconcing polygon was somehow marked...
        if(rPolygon->rEsconcingPolygon && rPolygon->rEsconcingPolygon->rCeilingTexture == NULL) rPolygon->rEsconcingPolygon = NULL;

        //--Next.
        rPolygon = (WAD_FloorPoly *)pPolygonList->AutoIterate();
    }

    //--Now go and purge any and all marked polygons.
    bool tRemovedAnything = false;
    WAD_FloorPoly *rCheckPolygon = (WAD_FloorPoly *)pPolygonList->SetToHeadAndReturn();
    while(rCheckPolygon)
    {
        if(!rCheckPolygon->rCeilingTexture && !rCheckPolygon->rFloorTexture)
        {
            tRemovedAnything = true;
            DebugManager::Print("Removing polygon %i/%i\n", rCheckPolygon->mPolygonCode, rCheckPolygon->mAssociatedSector);
            pPolygonList->RemoveRandomPointerEntry();
        }
        rCheckPolygon = (WAD_FloorPoly *)pPolygonList->IncrementAndGetRandomPointerEntry();
    }

    //--Debug.
    DebugManager::PopPrint("Finished marking and removing exact duplicates.\n");
    return tRemovedAnything;
}
bool WADFile::CompareVertices(WAD_FloorPoly *pPolyA, WAD_FloorPoly *pPolyB)
{
    //--Given two polygons, compares their vertices and returns true if they all match, false if not. Note that
    //  the vertices may be in a different order and right-handedness is not enforced. Therefore, if a vertex in A
    //  matches ANY vertex in B, it's legal.
    if(!pPolyA || !pPolyB) return false;

    //--If there are a different number of vertices, they can't possibly match.
    if(pPolyA->mVertexesTotal != pPolyB->mVertexesTotal) return false;

    //--Scan across A...
    for(int i = 0; i < pPolyA->mVertexesTotal; i ++)
    {
        //--Scan across B. As soon as a match is found, move to the next vertex.
        bool tNoMatchesFound = true;
        for(int p = 0; p < pPolyB->mVertexesTotal; p ++)
        {
            if(pPolyA->mVertices[i].mX == pPolyB->mVertices[p].mX && pPolyA->mVertices[i].mY == pPolyB->mVertices[p].mY)
            {
                tNoMatchesFound = false;
                break;
            }
        }

        //--If we got this far, no match for this vertex was found. Exit.
        if(tNoMatchesFound) return false;
    }

    //--All checks passed, the polygon vertices are identical.
    return true;
}
void WADFile::MarkPoly(int pPolyCode, SugarLinkedList *pSearchList)
{
    //--Given a list of polygons, marks the polygon with the matching code for removal.
    if(!pSearchList) return;
    WAD_FloorPoly *rPolygon = (WAD_FloorPoly *)pSearchList->PushIterator();
    while(rPolygon)
    {
        if(rPolygon->mPolygonCode == pPolyCode)
        {
            rPolygon->rCeilingTexture = NULL;
            rPolygon->rFloorTexture = NULL;
        }

        rPolygon = (WAD_FloorPoly *)pSearchList->AutoIterate();
    }
}
