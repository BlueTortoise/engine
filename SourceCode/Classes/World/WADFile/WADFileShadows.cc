//--Base
#include "WADFile.h"

//--Classes
#include "WorldPolygon.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--[Worker Functions]
float DotProduct(float pX1, float pY1, float pZ1, float pX2, float pY2, float pZ2)
{
    //--Computes the dot product of two vectors.
    return (pX1 * pX2) + (pY1 * pY2) + (pZ1 * pZ2);
}
int WADFile::DoesSegmentIntersectPolygon(float pX1, float pY1, float pZ1, float pX2, float pY2, float pZ2, WorldPolygon *pPolygon, float &sInterceptX, float &sInterceptY, float &sInterceptZ)
{
    //--Returns whether or not the provided line segment hits the provided polygon. A 0 means no hit,
    //  a 1 means a hit, and a 2 means the segment lies within the plane.
    //--In the event of a 1, the exact position of the hit is passed back through the ref variables.
    if(!pPolygon || pPolygon->mPointsTotal < 3) return 0;

    //--Build some vectors we need.
    //Vector    u = S.P1 - S.P0;
    sInterceptX = -1.0f;
    sInterceptY = -1.0f;
    sInterceptZ = -1.0f;
    float tUX = pX2 - pX1;
    float tUY = pY2 - pY1;
    float tUZ = pZ2 - pZ1;

    //Vector    w = S.P0 - Pn.V0;
    float tWX = pX1 - pPolygon->mPoints[0].mX;
    float tWY = pY1 - pPolygon->mPoints[0].mY;
    float tWZ = pZ1 - pPolygon->mPoints[0].mZ;

    //--Compute some Dot Products.
    //float     D = dot(Pn.n, u);
    float tDotProduct = DotProduct(pPolygon->mNormal.mX, pPolygon->mNormal.mY, pPolygon->mNormal.mZ, tUX, tUY, tUZ);
    //float     N = -dot(Pn.n, w);
    float tNotProduct = DotProduct(pPolygon->mNormal.mX, pPolygon->mNormal.mY, pPolygon->mNormal.mZ, tWX, tWY, tWZ) * -1.0f;

    //--Check if the planes are parallel.
    if (fabs(tDotProduct) < 0.00001f)
    {
        //--Planes are parallel, but the segment lies in the plane.
        if (tNotProduct == 0.0f)
        {
            return 2;
        }
        //--No intersection at all.
        else
        {
            return 0;
        }
    }

    //--Planes are not parallel. Find out where the intersection is.
    float sI = tNotProduct / tDotProduct;

    //--Negative or greater than 1.0f = no intersection.
    if (sI < 0 || sI > 1) return 0;

    //--There is an intersection somewhere along the plane. Compute where it is.
    //*I = S.P0 + sI * u;
    sInterceptX = pX1 + (sI * tUX);
    sInterceptY = pY1 + (sI * tUY);
    sInterceptZ = pZ1 + (sI * tUZ);
    return 1;
}

bool WADFile::DoesLineCrossFace(float pX1, float pY1, float pZ1, float pX2, float pY2, float pZ2, void *pExceptThis)
{
    //--Scans all the polygons to see if the given ray intersects the polygon. The ray cannot hit the void pointer
    //  passed in, as that is assumed to be the polygon that's calling this routine for its lightmap.
    //--We iterate across rActiveShadowList, which is normally held by the WorldLight which is calling this routine.
    //  If that list is NULL, fail immediately.
    if(!rActiveShadowList) return false;

    //--Iterate.
    WorldPolygon *rPolygon = (WorldPolygon *)rActiveShadowList->PushIterator();
    while(rPolygon)
    {
        //--Skip the exception polygon.
        if(rPolygon == pExceptThis)
        {
            rPolygon = (WorldPolygon *)rActiveShadowList->AutoIterate();
            continue;
        }

        //--Run this routine to check if the plane got hit.
        float tHitX, tHitY, tHitZ;
        int tResult = DoesSegmentIntersectPolygon(pX1, pY1, pZ1, pX2, pY2, pZ2, rPolygon, tHitX, tHitY, tHitZ);

        //--On a 0, no impact. We ignore a 2 since that doesn't affect shadows.
        if(tResult == 0 || tResult == 2)
        {
        }
        //--On a 1, we have an impact but it might not be in the polygon's area.
        else
        {
            //--Hit! This line crosses our face.
            if(rPolygon->IsPointInPolygon(tHitX, tHitY, tHitZ))
            {
                rActiveShadowList->PopIterator();
                return true;
            }
            //--Miss, point was outside the polygon.
            else
            {
            }
        }

        //--Next polygon.
        rPolygon = (WorldPolygon *)rActiveShadowList->AutoIterate();
    }

    //--All checks passed, the line does not intersect anything.
    return false;
}
