//--Base
#include "WADFile.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--[Note]
//--All methods in this file are private, as they must be called in a specific order.

void WADFile::ClearPrecache()
{
    //--Clears and deallocates all precached data.
    for(int i = 0; i < mFloorPolysTotal; i ++)
    {
        free(mFloorPolys[i].mVertices);
    }
    free(mFloorPolys);

    //--Clear walls.
    free(mWallPolys);

    //--Reset all flags.
    mIsPrecacheReady = false;
    mFloorPolysTotal = 0;
    mFloorPolys = NULL;
    mWallPolysTotal = 0;
    mWallPolys = NULL;
}
void WADFile::PrecacheFloors()
{
    //--Allocates space for the floor data that GL will use. ID1 uses a set of linedefs which enclose
    //  a given sector, but do not specifically refer to it as a polygon. In addition, two different
    //  polygons could use the same floor texture (and be the same sector!) and they must be split up.
    //--This must be called after a ClearPrecache() or we may cause a memory leak.
    DebugManager::PushPrint(false, "[Precache Floor Polys] Begin with %i linedefs, %i sidedefs, %i sectors.\n", mLinedefsTotal, mSidedefsTotal, mSectorsTotal);

    //--We need to scan all linedefs. Each linedef is connected by vertices to the next linedef
    //  in sequence, and all linedefs which point to the same sector are part of the same def.
    //--In order to prevent duplication, all linedefs have an involvement of 1 or 2. 2-sided linedefs
    //  get an involvement of 2, and each time a linedef is used in a floor poly, its involvement
    //  decreases. Linedefs with an involvement of zero were handled by another poly and are ignored
    //  by the GetNextLinedef() function.
    DebugManager::Print("Building involvement array.\n");
    SetMemoryData(__FILE__, __LINE__);
    int *tInvolvementArray = (int *)starmemoryalloc(sizeof(int) * mLinedefsTotal);
    for(int i = 0; i < mLinedefsTotal; i ++)
    {
        tInvolvementArray[i] = 1;
        if(mLinedefs[i].mSidedefL != -1) tInvolvementArray[i] = 2;

        //--If a linedef has the same sector on both sides, eliminate it.
        if(mLinedefs[i].mSidedefL != -1 && mLinedefs[i].mSidedefR != -1)
        {
            int tSectorRight = mSidedefs[mLinedefs[i].mSidedefR].mSector;
            int tSectorLeft = mSidedefs[mLinedefs[i].mSidedefL].mSector;
            if(tSectorLeft == tSectorRight) tInvolvementArray[i] = 0;
        }

        //if(mLinedefs[i].mLeftSidedef == mLinedefs[i].mRightSidedef) tInvolvementArray = 0;
    }

    //--Setup.
    DebugManager::Print("Setting up temporary lists.\n");
    SugarLinkedList *tPolygonList = new SugarLinkedList(false);
    SugarLinkedList *tUnprocessedPolygons = new SugarLinkedList(false);

    //--With involvement scanned, begin parsing linedefs.
    int tPolyCountTotal = 0;
    bool cUseReprocessing = true;

    //--Iterate.
    DebugManager::Print("Iterating across linedefs.\n");
    for(int i = 0; i < mLinedefsTotal; i ++)
    {
        //--If a linedef's involvement is zero, skip it.
        if(tInvolvementArray[i] == 0) continue;

        //--Get the involved sectors.
        int tSectorLeft = -1;
        int tSectorRight = -1;

        //--Check the right side-def. Reprocess if that hasn't been done yet.
        if(mLinedefs[i].mSidedefR != -1)
        {
            tSectorRight = mSidedefs[mLinedefs[i].mSidedefR].mSector;
            if(tSectorRight != -1)
            {
                //--This linedef is involved in a new polygon. Register it.
                WAD_FloorPoly *nPoly = ScanPolygon(tPolyCountTotal, i, tSectorRight, tInvolvementArray);
                if(nPoly)
                {
                    //fprintf(stderr, "Created polygon %i of sector %i\n", tPolyCountTotal, tSectorRight);
                    tPolyCountTotal ++;
                    tUnprocessedPolygons->AddElement("X", nPoly);
                }
            }
        }

        //--Check the left side-def. Reprocess if that hasn't been done yet.
        if(mLinedefs[i].mSidedefL != -1)
        {
            tSectorLeft = mSidedefs[mLinedefs[i].mSidedefL].mSector;
            if(tSectorLeft != -1 && tSectorLeft != tSectorRight)
            {
                //--This linedef is involved in a new polygon.
                WAD_FloorPoly *nPoly = ScanPolygon(tPolyCountTotal, i, tSectorLeft, tInvolvementArray);
                if(nPoly)
                {
                    //fprintf(stderr, "Created polygon %i of sector %i\n", tPolyCountTotal, tSectorLeft);
                    tPolyCountTotal ++;
                    tUnprocessedPolygons->AddElement("X", nPoly);
                }
            }
        }
    }

    //--We've now stored all the unique polygons in the area. Next, we scan them to see if any
    //  are wholly surrounded by any others. This will need to be dealt with during decomposition.
    DebugManager::Print("%i unique polygons stored. Begin checking for internal polys.\n", tUnprocessedPolygons->GetListSize());
    int tPolyCount = 0;
    int tPolyMax = tUnprocessedPolygons->GetListSize();
    if(tPolyMax < 1) tPolyMax = 1;
    WAD_FloorPoly *rPoly = (WAD_FloorPoly *)tUnprocessedPolygons->PushIterator();
    while(rPoly)
    {
        //--Run this algorithm, it will modify the polygon's structure to track all the polygons it
        //  has within it. It can be more than one.
        CheckInternalPolygons(rPoly, tUnprocessedPolygons);
        if(tPolyMax > 10 && tPolyCount % (tPolyMax / 10) == 0 && tPolyCount > 0) DebugManager::Print("%i ", (tPolyCount * 100 / tPolyMax) + 1);

        //--Next.
        tPolyCount ++;
        rPoly = (WAD_FloorPoly *)tUnprocessedPolygons->AutoIterate();
    }
    DebugManager::Print("\n");

    //--Mark and remove any and all duplicate polygons.
    DebugManager::Print("Mark and remove unused internal polygons.\n");
    //while(MarkDuplicateInternals(tUnprocessedPolygons));
    DebugManager::Print("Marked and removed unused internal polygons.\n");

    //--Build the binary mesh sample, and only the binary mesh sample.
    if(!cUseReprocessing)
    {
        //--Run the mesh builder.
        DebugManager::Print("Run binary mesh builder.\n");
        SugarLinkedList *tMeshPolyList = BuildBinaryMesh(tPolygonList);

        //--The mesh list becomes the new polygon list.
        delete tPolygonList;
        tPolygonList = tMeshPolyList;
    }
    //--Reprocess polygons, then run the mesh over any polygons that could not be reprocessed.
    else
    {
        //--Clone the overall polygon list for texture referencing.
        DebugManager::Print("Run reprocess on %i polygons...\n", tUnprocessedPolygons->GetListSize());
        mGlobalTexturePolyList->ClearList();

        //--Run reprocess. The results get dumped into tPolygonList.
        int i = 0;
        int p = tUnprocessedPolygons->GetListSize();
        if(p < 1) p = 1;
        WAD_FloorPoly *rPoly = (WAD_FloorPoly *)tUnprocessedPolygons->SetToHeadAndReturn();
        while(rPoly)
        {
            //--Clone to global list.
            WAD_FloorPoly *nClonePoly = rPoly->Clone();
            mGlobalTexturePolyList->AddElement("X", nClonePoly, &WAD_FloorPoly::DeleteThis);

            //--Reprocess here.
            //DebugManager::Print(" Reprocessing polygon %i.\n", i);
            ReprocessPolygon(rPoly, tPolygonList);
            //DebugManager::Print(" State");
            if(rPoly->mHasBeenReprocessed)
            {
                nClonePoly->rFloorTexture = NULL;
                nClonePoly->rCeilingTexture = NULL;
                tUnprocessedPolygons->RemoveRandomPointerEntry();
            }
            //DebugManager::Print(" Finished.\n");
            if(p > 10 && i % (p / 10) == 0 && i > 0) DebugManager::Print("%i ", (i * 100 / p) + 1);

            //--Next.
            i ++;
            rPoly = (WAD_FloorPoly *)tUnprocessedPolygons->IncrementAndGetRandomPointerEntry();
        }
        DebugManager::Print("\n");

        //--Run the mesh builder. It will ignore any polygons that were already reprocessed.
        DebugManager::Print("Run binary mesh builder on %i polygons.\n", tUnprocessedPolygons->GetListSize());
        SugarLinkedList *tMeshPolyList = BuildBinaryMesh(tUnprocessedPolygons);

        //--Merge the mesh list onto the polygon list.
        DebugManager::Print("Merging mesh list.\n");
        if(tMeshPolyList)
        {
            tMeshPolyList->SetDeallocation(false);
            WAD_FloorPoly *rNewPolygon = (WAD_FloorPoly *)tMeshPolyList->PushIterator();
            while(rNewPolygon)
            {
                tPolygonList->AddElementAsTail("X", rNewPolygon, &WAD_FloorPoly::DeleteThis);
                rNewPolygon = (WAD_FloorPoly *)tMeshPolyList->AutoIterate();
            }
        }

        //--The mesh list gets deleted.
        DebugManager::Print("Cleaning up after binary mesh builder.\n");
        delete tMeshPolyList;
    }

    //--Clean up the unprocessed polygons. They were moved and the list no longer contains master copies.
    delete tUnprocessedPolygons;

    //--Remove any illegal polygons.
    DebugManager::Print("Removing illegal polygons.\n");
    WAD_FloorPoly *rPolygon = (WAD_FloorPoly *)tPolygonList->SetToHeadAndReturn();
    while(rPolygon)
    {
        if(!rPolygon->rFloorTexture) tPolygonList->RemoveRandomPointerEntry();
        rPolygon = (WAD_FloorPoly *)tPolygonList->IncrementAndGetRandomPointerEntry();
    }

    //--Store the polygon list.
    DebugManager::Print("Cloning and storing polygons.\n");
    mFloorPolysTotal = tPolygonList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mFloorPolys = (WAD_FloorPoly *)starmemoryalloc(sizeof(WAD_FloorPoly) * mFloorPolysTotal);
    for(int i = 0; i < mFloorPolysTotal; i ++)
    {
        WAD_FloorPoly *rPoly = (WAD_FloorPoly *)tPolygonList->GetElementBySlot(i);
        mFloorPolys[i].Initialize();
        mFloorPolys[i].mAssociatedSector = rPoly->mAssociatedSector;
        mFloorPolys[i].mVertexesTotal = rPoly->mVertexesTotal;
        mFloorPolys[i].mFloorHeight = rPoly->mFloorHeight;
        mFloorPolys[i].mCeilingHeight = rPoly->mCeilingHeight;
        mFloorPolys[i].AllocVertices(mFloorPolys[i].mVertexesTotal);
        for(int p = 0; p < mFloorPolys[i].mVertexesTotal; p ++)
        {
            memcpy(&mFloorPolys[i].mVertices[p], &rPoly->mVertices[p], sizeof(UDMFVertex));
        }

        mFloorPolys[i].rFloorTexture = rPoly->rFloorTexture;
        mFloorPolys[i].rCeilingTexture = rPoly->rCeilingTexture;
    }
    DebugManager::Print("Floor polys scanned: %i\n", mFloorPolysTotal);

    //--Only mark precaching as ready if the polys are more than zero.
    if(mFloorPolysTotal > 0) mIsPrecacheReady = true;

    //--Debug report.
    if(false)
    {
        FILE *fOutfile = fopen("FloorPolys.txt", "w");
        fprintf(fOutfile, "Floor Poly data:\n");
        for(int i = 0; i < mFloorPolysTotal; i ++)
        {
            fprintf(fOutfile, " %i: %i\n", i, mFloorPolys[i].mVertexesTotal);
            for(int p = 0; p < mFloorPolys[i].mVertexesTotal; p ++)
            {
                fprintf(fOutfile, "  %.0f %.0f\n", mFloorPolys[i].mVertices[p].mX, mFloorPolys[i].mVertices[p].mY);
            }
        }
        fclose(fOutfile);
    }

    //--Clean up.
    delete tPolygonList;
    free(tInvolvementArray);
    DebugManager::PopPrint("[Precache Floor Polys] Completed\n");
}
WAD_FloorPoly *WADFile::ScanPolygon(int pPolygonCode, int pStartLinedef, int pSector, int *pInvolvementLookups)
{
    //--Given a specific linedef that is involved in a polygon, scan all other linedefs
    //  which match it on the right side. However, if no connected linedefs can be found which
    //  look at the same sector on the right side, check the left side instead.
    //--It is entirely possible that a WAD author screwed up and has a linedef that does not
    //  enclose an area, in which case, decrement its involvement and return NULL.
    WAD_FloorPoly *nPolygon = NULL;
    if(pStartLinedef < 0 || pSector < 0 || pStartLinedef >= mLinedefsTotal || !pInvolvementLookups) return nPolygon;

    //--Linked list of involved linedefs.
    SugarLinkedList *tLinedefList = new SugarLinkedList(true);
    tLinedefList->AddElement("X", &pStartLinedef);

    //--Output
    DebugManager::PushPrint(false, "Scanning linedef polygon, start at %i\n", pStartLinedef);

    //--Special.
    bool tAllWereLeft = true;
    bool tAllWereRight = true;
    int tSectorNotUs = -1;
    bool tAllWereSameSectorExternal = true;

    //--Begin following linedefs until we get back to the original one.
    int tLastLine = -1;
    bool tWasSuccessful = false;
    bool tDone = false;
    int tCurLinedef = pStartLinedef;
    while(!tDone)
    {
        //--Get a connecting linedef based on the 0 vertex.
        int tVertex = mLinedefs[tCurLinedef].mStartVertex;
        int tCheckDef = GetConnectingLinedef(tVertex, tCurLinedef, tLastLine, pSector, pInvolvementLookups);

        //--If the linedef failed, check the other vertex.
        if(tCheckDef == -1)
        {
            tVertex = mLinedefs[tCurLinedef].mEndVertex;
            tCheckDef = GetConnectingLinedef(tVertex, tCurLinedef, tLastLine, pSector, pInvolvementLookups);
        }

        //--If the returned linedef was the start linedef, then we've come full circle.
        if(tCheckDef == pStartLinedef)
        {
            //--Flags.
            tWasSuccessful = true;
            pInvolvementLookups[pStartLinedef] --;
            break;
        }

        //--Iterate up to the next linedef.
        if(tCheckDef != -1)
        {
            //--Next linedef.
            tLastLine = tCurLinedef;
            tCurLinedef = tCheckDef;

            //--Involvement.
            pInvolvementLookups[tCurLinedef] --;

            //--Set the sector on the matching side to -1 so we don't re-use it later.
            if(mLinedefs[tCurLinedef].mSidedefR != -1)
            {
                int tSectorRight = mSidedefs[mLinedefs[tCurLinedef].mSidedefR].mSector;
                if(tSectorRight == pSector)
                {
                    mLinedefs[tCurLinedef].mSidedefR = -1;
                    tAllWereLeft = false;
                }
                else if(tSectorNotUs == -1)
                {
                    tSectorNotUs = tSectorRight;
                }
                else if(tSectorRight != tSectorNotUs)
                {
                    tAllWereSameSectorExternal = false;
                }
            }

            //--Also check the left side, which may not always exist.
            if(mLinedefs[tCurLinedef].mSidedefL != -1)
            {
                int tSectorLeft = mSidedefs[mLinedefs[tCurLinedef].mSidedefL].mSector;
                if(tSectorLeft == pSector)
                {
                    mLinedefs[tCurLinedef].mSidedefL = -1;
                    tAllWereRight = false;
                }
                else if(tSectorNotUs == -1)
                {
                    tSectorNotUs = tSectorLeft;
                }
                else if(tSectorLeft != tSectorNotUs)
                {
                    tAllWereSameSectorExternal = false;
                }
            }
            //--Having any one one-side linedef means this cannot be a fully esconced polygon.
            else
            {
                tSectorNotUs = -2;
                tAllWereSameSectorExternal = false;
            }

            //--Save the linedef in the list.
            SetMemoryData(__FILE__, __LINE__);
            int *nLinedefPtr = (int *)starmemoryalloc(sizeof(int));
            *nLinedefPtr = tCurLinedef;
            tLinedefList->AddElement("X", nLinedefPtr, &FreeThis);
            DebugManager::Print(" Next line is %i, sharing vertex %i, inv %i.\n", tCurLinedef, tVertex, pInvolvementLookups[tCurLinedef]);
        }
        //--No linedef came back. The linedef chain breaks here because it may have already been
        //  handled by another set.
        else
        {
            break;
        }
    }

    //--Once the loop is complete, if we were successful, then assemble a floor poly and pass it back.
    if(tWasSuccessful)
    {
        //--Allocate polygon space.
        SetMemoryData(__FILE__, __LINE__);
        nPolygon = (WAD_FloorPoly *)starmemoryalloc(sizeof(WAD_FloorPoly));
        nPolygon->Initialize();
        nPolygon->mPolygonCode = pPolygonCode;
        nPolygon->mVertexesTotal = tLinedefList->GetListSize() + 1;
        DebugManager::Print("Created polygon %2i/%2i with properties %i %i, %i - %i\n", pPolygonCode, pSector, tAllWereLeft, tAllWereRight, tAllWereSameSectorExternal, tSectorNotUs);

        //--Allocate space for vertices.
        DebugManager::Print(" Polygon has %i lines and %i sides\n", tLinedefList->GetListSize(), nPolygon->mVertexesTotal);
        nPolygon->AllocVertices(nPolygon->mVertexesTotal);

        //--For the 0th linedef, the 0th and 1st vertices are added.
        int *rIndexPtr = (int *)tLinedefList->GetElementBySlot(0);
        int tIndex = *rIndexPtr;
        UDMFLinedef *rLinedef = &mLinedefs[tIndex];
        UDMFLinedef *rOrigLinedef = rLinedef;
        memcpy(&nPolygon->mVertices[0], &mVertices[rLinedef->mStartVertex], sizeof(UDMFVertex));
        memcpy(&nPolygon->mVertices[1], &mVertices[rLinedef->mEndVertex], sizeof(UDMFVertex));
        int tLastVertex = rOrigLinedef->mEndVertex;

        //--Iterate across the linedefs and store the vertices.
        for(int i = 1; i < tLinedefList->GetListSize(); i ++)
        {
            //--Get the linedef.
            rIndexPtr = (int *)tLinedefList->GetElementBySlot(i);
            tIndex = *rIndexPtr;
            rLinedef = &mLinedefs[tIndex];

            //--If the linedef's 0 vertex matches the previous...
            if(rLinedef->mStartVertex == tLastVertex)
            {
                memcpy(&nPolygon->mVertices[i+1], &mVertices[rLinedef->mEndVertex], sizeof(UDMFVertex));
                tLastVertex = rLinedef->mEndVertex;
            }
            //--Otherwise, if the 1 vertex matches the previous...
            else if(rLinedef->mEndVertex == tLastVertex)
            {
                memcpy(&nPolygon->mVertices[i+1], &mVertices[rLinedef->mStartVertex], sizeof(UDMFVertex));
                tLastVertex = rLinedef->mStartVertex;
            }
            //--This chain is going backwards, so reset the for loop with a diffent start.
            else if(i == 1)
            {
                i --;
                tLastVertex = rOrigLinedef->mStartVertex;

                //--Flip. The 0 and 1 points are therefore also backwards.
                UDMFVertex tTempVertex;
                memcpy(&tTempVertex,            &nPolygon->mVertices[0], sizeof(UDMFVertex));
                memcpy(&nPolygon->mVertices[0], &nPolygon->mVertices[1], sizeof(UDMFVertex));
                memcpy(&nPolygon->mVertices[1], &tTempVertex,            sizeof(UDMFVertex));
            }
        }

        //--Store the sector.
        nPolygon->mAssociatedSector = pSector;
        DebugManager::Print(" Iterated across the polygon!\n");

        //--Locate the texture. Theoretically it should always be found, but the returned bitmap
        //  could still be NULL if its data was invalid.
        DebugManager::Print(" Looking up floor for %i\n", pSector);
        const char *rFloorName = mSectors[pSector].mTexFloor;
        nPolygon->rFloorTexture = GetFlatTexture(rFloorName);
        if(!nPolygon->rFloorTexture)
        {
            fprintf(stderr, "Warning: Flat %s was not found!\n", rFloorName);
        }

        //--Ceiling.
        DebugManager::Print(" Looking up ceiling for %i\n", pSector);
        const char *rCeilingName = mSectors[pSector].mTexCeiling;
        nPolygon->rCeilingTexture = GetFlatTexture(rCeilingName);
        if(!nPolygon->rCeilingTexture)
        {
            fprintf(stderr, "Warning: Flat %s was not found!\n", rCeilingName);
        }
        else
        {
            //fprintf(stderr, "Ceiling has flat %s\n", rCeilingName);
        }

        //--Store height data.
        DebugManager::Print(" Storing height data.\n");
        nPolygon->mFloorHeight = mSectors[pSector].mHeightFloor;
        nPolygon->mCeilingHeight = mSectors[pSector].mHeightCeiling;

        //--Force right-handedness in the polygon.
        DebugManager::Print(" Forcing right-handedness.\n");
        ForceRightHandednessFP(nPolygon);

        //--Report
        //fprintf(stderr, " Vertex report:\n");
        //for(int i = 0; i < nPolygon->mVertexesTotal; i ++)
        //{
        //    fprintf(stderr, "  %i: %i %i\n", i, nPolygon->mVertices[i].mX, nPolygon->mVertices[i].mY);
        //}
        DebugManager::Print("Polygon completed.\n");
    }

    //--All done, return.
    DebugManager::PopPrint("Polygon completed.\n");
    delete tLinedefList;
    return nPolygon;
}
int WADFile::GetConnectingLinedef(int pStartVertex, int pStartLinedef, int pPrevLinedef, int pSectorNeeded, int *pInvolvementLookups)
{
    //--Given a vertex, locates all linedefs that are (1) using this vertex and (2) looking at
    //  the same sector (on either side) and (3) are not already in use.
    //--If no valid linedef can be found, return -1.
    if(pStartVertex < 0 || pStartVertex >= mVerticesTotal) return -1;
    if(pStartLinedef < 0 || pStartLinedef >= mLinedefsTotal) return -1;
    if(pSectorNeeded < 0 || pSectorNeeded >= mSectorsTotal) return -1;
    if(!pInvolvementLookups) return -1;

    //--Begin scanning linedefs.
    for(int i = 0; i < mLinedefsTotal; i ++)
    {
        //--Same as start def. Fail.
        if(i == pStartLinedef) continue;

        //--Linedef has already been scanned. Fail.
        if(pInvolvementLookups[i] == 0) continue;

        //--Linedef was the same as the previous one. Fail.
        if(pPrevLinedef == i) continue;

        //--If either vertex is this one...
        if(mLinedefs[i].mStartVertex == pStartVertex || mLinedefs[i].mEndVertex == pStartVertex)
        {
            //--If the linedef has the requested sector on one side, then we have a match.
            int tSectorRight = mSidedefs[mLinedefs[i].mSidedefR].mSector;
            if(tSectorRight == pSectorNeeded) return i;

            //--Not right side. Left doesn't always exist.
            if(mLinedefs[i].mSidedefL != -1)
            {
                int tSectorLeft = mSidedefs[mLinedefs[i].mSidedefL].mSector;
                if(tSectorLeft == pSectorNeeded) return i;
            }
        }
    }

    //--There were no matches. Return -1.
    return -1;
}
void WADFile::PrecacheWalls()
{
    //--Construct prebuilt polygons (quads, in this case) for all walls. Unlike the complicated floors,
    //  wall polygons are based on linedefs/sidedefs and are relatively simple. Diagonal geometry is
    //  not supported by WAD files, use a .bsp if you need that functionality.
    //--This is private, and should only be called once the level is ready and the precache is cleared.
    if(mLinedefsTotal < 1 || mSidedefsTotal < 1) return;

    //--Debug
    DebugManager::PushPrint(false, "[Precache Walls] Begin.\n");

    //--RLL to store new walls.
    SugarLinkedList *tPolyList = new SugarLinkedList(true);

    //--All linedefs are going to result in at least one wall poly, except in the case where the
    //  linedef bisects two sectors of the same height in both floor and ceiling.
    for(int i = 0; i < mLinedefsTotal; i ++)
    {
        //--Debug.
        DebugManager::Print("Scanning linedef %i, %i %i\n", i, mLinedefs[i].mSidedefR, mLinedefs[i].mSidedefL);

        //--The right sidedef must always be declared or the linedef is in error.
        if(mLinedefs[i].mSidedefR == -1) continue;

        //--Get the sidedefs.
        UDMFSidedef *rLftSidedef = NULL;
        UDMFSidedef *rRgtSidedef = &mSidedefs[mLinedefs[i].mSidedefR];
        if(mLinedefs[i].mSidedefL != -1) rLftSidedef = &mSidedefs[mLinedefs[i].mSidedefL];

        //--Get the sectors.
        UDMFSector *rLftSector = NULL;
        UDMFSector *rRgtSector = &mSectors[rRgtSidedef->mSector];
        if(rLftSidedef) rLftSector = &mSectors[rLftSidedef->mSector];

        //--If the two sectors are identical, then this is an exception, ignore it.
        //if(rLftSector == rRgtSector) continue;

        //--If we got this far, it's time to build some polys. First, handle the case where the
        //  linedef is one-sided.
        if(!rLftSidedef)
        {
            //--The vertexes are such that 0 is the top left and 1 is the bottom right.
            SetMemoryData(__FILE__, __LINE__);
            WAD_WallPoly *nPolygon = (WAD_WallPoly *)starmemoryalloc(sizeof(WAD_WallPoly));
            nPolygon->Initialize();

            //--Get offset data from the sidedef.
            nPolygon->mXTexOffset = rRgtSidedef->mOffsetX + rRgtSidedef->mOffsetMX;
            nPolygon->mYTexOffset = rRgtSidedef->mOffsetY + rRgtSidedef->mOffsetMY;

            //--Polygon always associated with the right sidedef's sector.
            nPolygon->mAssociatedSector = rRgtSidedef->mSector;

            //--Alignment. Normal is "Top Down" for a middle texture. This can be switched with the
            //  Upper-Unpegged flag.
            nPolygon->mIsTopAligned = true;
            nPolygon->mIsBotAligned = false;
            if(mLinedefs[i].mIsUpperUnpegged)
            {
                nPolygon->mIsTopAligned = false;
                nPolygon->mIsBotAligned = true;
            }

            //--Store vertex data.
            nPolygon->mVertX0 = mVertices[mLinedefs[i].mStartVertex].mX;
            nPolygon->mVertY0 = mVertices[mLinedefs[i].mStartVertex].mY;
            nPolygon->mVertZ0 = rRgtSector->mHeightCeiling;
            nPolygon->mVertX1 = mVertices[mLinedefs[i].mEndVertex].mX;
            nPolygon->mVertY1 = mVertices[mLinedefs[i].mEndVertex].mY;
            nPolygon->mVertZ1 = rRgtSector->mHeightFloor;

            //--Normal flipping: If the Y1 is north of the Y0, flip the heights to get the winding order correct.
            if(nPolygon->mVertY1 > nPolygon->mVertY0)
            {
                /*nPolygon->mVertX0 = mVertices[mLinedefs[i].mEndVertex].mX;
                nPolygon->mVertY0 = mVertices[mLinedefs[i].mEndVertex].mY;
                nPolygon->mVertZ0 = rRgtSector->mHeightFloor;
                nPolygon->mVertX1 = mVertices[mLinedefs[i].mStartVertex].mX;
                nPolygon->mVertY1 = mVertices[mLinedefs[i].mStartVertex].mY;
                nPolygon->mVertZ1 = rRgtSector->mHeightCeiling;*/
            }
            //--If the Y values are exactly equal, we check the X values instead. Remember floats need epsilon checks.
            else if(fabsf(nPolygon->mVertY0 - nPolygon->mVertY1) < 0.00001f && nPolygon->mVertX0 > nPolygon->mVertX1)
            {
            }

            //--Locate the matching texture. It might come back NULL, in which case a solid color
            //  will render to indicate an error.
            nPolygon->rTexture = GetWallTexture(rRgtSidedef->mTexMid);
            CopyEight(nPolygon->mTexName, rRgtSidedef->mTexMid);

            //--Storage.
            if(nPolygon->rTexture)
            {
                nPolygon->mTexStoreW = nPolygon->rTexture->GetTrueWidth();
                nPolygon->mTexStoreH = nPolygon->rTexture->GetTrueHeight();
            }

            //--Invert the offset on the Y.
            nPolygon->mYTexOffset = nPolygon->mTexStoreH - nPolygon->mYTexOffset;

            //--Alignment. By default, the texture will be top-aligned. The Lower-Unpegged flag will
            //  make it bottom aligned instead. The Upper-Unpegged flag is ignored.
            nPolygon->mIsBotAligned = false;
            nPolygon->mIsTopAligned = true;
            if(mLinedefs[i].mIsLowerUnpegged)
            {
                nPolygon->mIsTopAligned = false;
                nPolygon->mIsBotAligned = true;
            }

            //--Add it to the list.
            tPolyList->AddElement("X", nPolygon, &FreeThis);
            DebugManager::Print(" Linedef %i %s added a polygon: %0.0f to %0.0f\n", i, nPolygon->mTexName, nPolygon->mVertZ0, nPolygon->mVertZ1);
        }
        //--This is a two-sided linedef
        else
        {
            //--Up to three polygons can result from a two-sided linedef. Top, middle, and bottom.
            //  The winding order is determined by the direction they face.

            //--Top section: Same heights means skip the top def.
            if(rLftSector->mHeightCeiling != rRgtSector->mHeightCeiling)
            {
                //--Common setup.
                SetMemoryData(__FILE__, __LINE__);
                WAD_WallPoly *nPolygon = (WAD_WallPoly *)starmemoryalloc(sizeof(WAD_WallPoly));
                nPolygon->Initialize();

                //--Get offset data from the sidedef.
                nPolygon->mXTexOffset = rRgtSidedef->mOffsetX + rRgtSidedef->mOffsetUX;
                nPolygon->mYTexOffset = rRgtSidedef->mOffsetY + rRgtSidedef->mOffsetUY;

                //--Polygon might associate with either sector. Set to -1 for safety.
                nPolygon->mAssociatedSector = -1;

                //--Alignment. By default, the texture will be bottom-aligned. The Upper-Unpegged flag will
                //  instead make it top-aligned.
                nPolygon->mIsBotAligned = true;
                nPolygon->mIsTopAligned = false;
                if(mLinedefs[i].mIsLowerUnpegged)
                {
                    nPolygon->mIsBotAligned = false;
                    nPolygon->mIsTopAligned = false;
                }
                else if(mLinedefs[i].mIsUpperUnpegged)
                {
                    nPolygon->mIsBotAligned = false;
                    nPolygon->mIsTopAligned = true;
                }

                //--If the right sector is higher...
                if(rRgtSector->mHeightCeiling > rLftSector->mHeightCeiling)
                {
                    nPolygon->mVertX0 = mVertices[mLinedefs[i].mStartVertex].mX;
                    nPolygon->mVertY0 = mVertices[mLinedefs[i].mStartVertex].mY;
                    nPolygon->mVertZ0 = rRgtSector->mHeightCeiling;
                    nPolygon->mVertX1 = mVertices[mLinedefs[i].mEndVertex].mX;
                    nPolygon->mVertY1 = mVertices[mLinedefs[i].mEndVertex].mY;
                    nPolygon->mVertZ1 = rLftSector->mHeightCeiling;
                    nPolygon->mAssociatedSector = rRgtSidedef->mSector;
                }
                //--If the left sector is higher...
                else
                {
                    nPolygon->mVertX0 = mVertices[mLinedefs[i].mStartVertex].mX;
                    nPolygon->mVertY0 = mVertices[mLinedefs[i].mStartVertex].mY;
                    nPolygon->mVertZ0 = rLftSector->mHeightCeiling;
                    nPolygon->mVertX1 = mVertices[mLinedefs[i].mEndVertex].mX;
                    nPolygon->mVertY1 = mVertices[mLinedefs[i].mEndVertex].mY;
                    nPolygon->mVertZ1 = rRgtSector->mHeightCeiling;
                    nPolygon->mAssociatedSector = rLftSidedef->mSector;

                    //--Get offset data from the sidedef.
                    nPolygon->mXTexOffset = rLftSidedef->mOffsetX + rLftSidedef->mOffsetUX;
                    nPolygon->mYTexOffset = rLftSidedef->mOffsetY + rLftSidedef->mOffsetUY;

                    //--Normal flipping: If the left sector is higher, this wall is facing the other way. Flip it.
                    {
                        nPolygon->mVertX0 = mVertices[mLinedefs[i].mEndVertex].mX;
                        nPolygon->mVertY0 = mVertices[mLinedefs[i].mEndVertex].mY;
                        nPolygon->mVertX1 = mVertices[mLinedefs[i].mStartVertex].mX;
                        nPolygon->mVertY1 = mVertices[mLinedefs[i].mStartVertex].mY;
                    }
                }

                //--Locate the matching texture. It might come back NULL, in which case a solid color
                //  will render to indicate an error.
                nPolygon->rTexture = GetWallTexture(rRgtSidedef->mTexTop);
                CopyEight(nPolygon->mTexName, rRgtSidedef->mTexTop);
                if(!nPolygon->rTexture)
                {
                    nPolygon->rTexture = GetWallTexture(rLftSidedef->mTexTop);
                    CopyEight(nPolygon->mTexName, rLftSidedef->mTexTop);
                }

                //--Special Case: If the flag xAllowDoomSkyHack is true, and both the left and right sectors
                //  have sky textures for their ceiling, the wall texture is replaced with a sky texture.
                //--This is done because of a shortcut used by the BSP rendering for Doom's sky.
                if(xAllowDoomSkyHack && rLftSector->IsCeilingSky() && rRgtSector->IsCeilingSky())
                {
                    nPolygon->mTexName[0] = '\0';
                    nPolygon->rTexture = rSkyTexture;
                }

                //--If neither texture resolved, then fail here.
                if(!nPolygon->rTexture)
                {
                    delete nPolygon;
                }
                //--Store the texture sizing.
                else
                {
                    //--Set texture info.
                    nPolygon->mTexStoreW = nPolygon->rTexture->GetTrueWidth();
                    nPolygon->mTexStoreH = nPolygon->rTexture->GetTrueHeight();

                    //--Invert the offset on the Y.
                    nPolygon->mYTexOffset = nPolygon->mTexStoreH - nPolygon->mYTexOffset;

                    //--Add it to the list.
                    tPolyList->AddElement("X", nPolygon, &FreeThis);
                    DebugManager::Print(" Linedef %i %s added a polygon: %0.0f to %0.0f\n", i, nPolygon->mTexName, nPolygon->mVertZ0, nPolygon->mVertZ1);
                }
            }

            //--Bottom section: Same heights means skip the top def.
            if(rLftSector->mHeightFloor != rRgtSector->mHeightFloor)
            {
                //--Common setup.
                SetMemoryData(__FILE__, __LINE__);
                WAD_WallPoly *nPolygon = (WAD_WallPoly *)starmemoryalloc(sizeof(WAD_WallPoly));
                nPolygon->Initialize();

                //--Get offset data from the sidedef.
                nPolygon->mXTexOffset = rRgtSidedef->mOffsetX + rRgtSidedef->mOffsetLX;
                nPolygon->mYTexOffset = rRgtSidedef->mOffsetY + rRgtSidedef->mOffsetLY;

                //--Polygon might associate with either sector. Set to -1 for safety.
                nPolygon->mAssociatedSector = -1;

                //--Locate the matching texture. It might come back NULL, in which case a solid color
                //  will render to indicate an error.
                nPolygon->rTexture = GetWallTexture(rRgtSidedef->mTexBot);
                CopyEight(nPolygon->mTexName, rRgtSidedef->mTexBot);
                if(!nPolygon->rTexture)
                {
                    nPolygon->rTexture = GetWallTexture(rLftSidedef->mTexBot);
                    CopyEight(nPolygon->mTexName, rLftSidedef->mTexBot);
                }

                //--If neither texture resolved, then fail here.
                if(!nPolygon->rTexture)
                {
                    delete nPolygon;
                }
                //--Store the texture sizing.
                else
                {
                    //--Set texture sizing.
                    nPolygon->mTexStoreW = nPolygon->rTexture->GetTrueWidth();
                    nPolygon->mTexStoreH = nPolygon->rTexture->GetTrueHeight();

                    //--If the right sector is higher...
                    float tHighestCeiling = 0.0f;
                    if(rRgtSector->mHeightFloor > rLftSector->mHeightFloor)
                    {
                        nPolygon->mVertX0 = mVertices[mLinedefs[i].mStartVertex].mX;
                        nPolygon->mVertY0 = mVertices[mLinedefs[i].mStartVertex].mY;
                        nPolygon->mVertZ0 = rRgtSector->mHeightFloor;
                        nPolygon->mVertX1 = mVertices[mLinedefs[i].mEndVertex].mX;
                        nPolygon->mVertY1 = mVertices[mLinedefs[i].mEndVertex].mY;
                        nPolygon->mVertZ1 = rLftSector->mHeightFloor;
                        tHighestCeiling = rLftSector->mHeightCeiling;
                        nPolygon->mAssociatedSector = rLftSidedef->mSector;

                        //--Get offset data from the sidedef.
                        nPolygon->mXTexOffset = rLftSidedef->mOffsetX + rLftSidedef->mOffsetLX;
                        nPolygon->mYTexOffset = rLftSidedef->mOffsetY + rLftSidedef->mOffsetLY;

                        //--Normal flipping: If the right sector is higher, this wall is facing the other way. Flip it.
                        {
                            nPolygon->mVertX0 = mVertices[mLinedefs[i].mEndVertex].mX;
                            nPolygon->mVertY0 = mVertices[mLinedefs[i].mEndVertex].mY;
                            nPolygon->mVertX1 = mVertices[mLinedefs[i].mStartVertex].mX;
                            nPolygon->mVertY1 = mVertices[mLinedefs[i].mStartVertex].mY;
                        }
                    }
                    //--If the left sector is higher...
                    else
                    {
                        nPolygon->mVertX0 = mVertices[mLinedefs[i].mStartVertex].mX;
                        nPolygon->mVertY0 = mVertices[mLinedefs[i].mStartVertex].mY;
                        nPolygon->mVertZ0 = rLftSector->mHeightFloor;
                        nPolygon->mVertX1 = mVertices[mLinedefs[i].mEndVertex].mX;
                        nPolygon->mVertY1 = mVertices[mLinedefs[i].mEndVertex].mY;
                        nPolygon->mVertZ1 = rRgtSector->mHeightFloor;
                        tHighestCeiling = rRgtSector->mHeightCeiling;
                        nPolygon->mAssociatedSector = rRgtSidedef->mSector;

                        //--No need to flip the normal, the wall is facing the right way.
                    }

                    //--Invert the offset on the Y.
                    nPolygon->mYTexOffset = nPolygon->mTexStoreH - nPolygon->mYTexOffset;

                    //--Alignment. By default, the texture will be top-aligned. The Lower-Unpegged flag will
                    //  make it top-aligned to the higher ceiling instead, which modifies the Y offset.
                    nPolygon->mIsBotAligned = false;
                    nPolygon->mIsTopAligned = true;
                    if(mLinedefs[i].mIsUpperUnpegged)
                    {
                        nPolygon->mYTexOffset = nPolygon->mYTexOffset - ((tHighestCeiling - nPolygon->mVertZ0));
                    }
                    else if(mLinedefs[i].mIsLowerUnpegged)
                    {
                        nPolygon->mIsTopAligned = false;
                        nPolygon->mIsBotAligned = true;
                    }

                    //--Add it to the list.
                    tPolyList->AddElement("X", nPolygon, &FreeThis);
                    DebugManager::Print(" Linedef %i %s added a polygon: %0.0f to %0.0f\n", i, nPolygon->mTexName, nPolygon->mVertZ0, nPolygon->mVertZ1);
                }
            }

            //--Middle section. Can always potentially exist, but the middle texture is often missing
            //  since fully-missing is fully-transparent.
            if(true)
            {
                //--Common setup.
                SetMemoryData(__FILE__, __LINE__);
                WAD_WallPoly *nPolygon = (WAD_WallPoly *)starmemoryalloc(sizeof(WAD_WallPoly));
                nPolygon->Initialize();

                //--Get offset data from the sidedef.
                nPolygon->mXTexOffset = rRgtSidedef->mOffsetX + rRgtSidedef->mOffsetMX;
                nPolygon->mYTexOffset = rRgtSidedef->mOffsetY + rRgtSidedef->mOffsetMY;

                //--Alignment. Normal is "Top Down" for an upper texture.
                nPolygon->mIsTopAligned = true;
                if(mLinedefs[i].mIsLowerUnpegged) nPolygon->mIsTopAligned = false;

                //--If the right sector is higher...
                if(rRgtSector->mHeightCeiling > rLftSector->mHeightCeiling)
                {
                    nPolygon->mVertX0 = mVertices[mLinedefs[i].mStartVertex].mX;
                    nPolygon->mVertY0 = mVertices[mLinedefs[i].mStartVertex].mY;
                    nPolygon->mVertZ0 = rRgtSector->mHeightCeiling;
                    nPolygon->mVertX1 = mVertices[mLinedefs[i].mEndVertex].mX;
                    nPolygon->mVertY1 = mVertices[mLinedefs[i].mEndVertex].mY;
                    nPolygon->mVertZ1 = rRgtSector->mHeightFloor;
                }
                //--If the left sector is higher...
                else
                {
                    nPolygon->mVertX0 = mVertices[mLinedefs[i].mStartVertex].mX;
                    nPolygon->mVertY0 = mVertices[mLinedefs[i].mStartVertex].mY;
                    nPolygon->mVertZ0 = rLftSector->mHeightCeiling;
                    nPolygon->mVertX1 = mVertices[mLinedefs[i].mEndVertex].mX;
                    nPolygon->mVertY1 = mVertices[mLinedefs[i].mEndVertex].mY;
                    nPolygon->mVertZ1 = rLftSector->mHeightFloor;
                }

                //--These always associate with the right sector, regardless of height.
                nPolygon->mAssociatedSector = rRgtSidedef->mSector;

                //--Locate the matching texture. It might come back NULL, in which case a solid color
                //  will render to indicate an error.
                nPolygon->rTexture = GetWallTexture(rRgtSidedef->mTexMid);
                CopyEight(nPolygon->mTexName, rRgtSidedef->mTexMid);
                if(!nPolygon->rTexture)
                {
                    nPolygon->rTexture = GetWallTexture(rLftSidedef->mTexMid);
                    CopyEight(nPolygon->mTexName, rLftSidedef->mTexMid);
                }

                //--If neither texture resolved, then fail here.
                if(!nPolygon->rTexture)
                {
                    delete nPolygon;
                }
                //--If the texture is found, it cannot repeat vertically. Lower it.
                else
                {
                    float cTexHeight = nPolygon->rTexture->GetHeight();
                    if(nPolygon->mVertZ0 - nPolygon->mVertZ1 > cTexHeight)
                    {
                        nPolygon->mVertZ0 = nPolygon->mVertZ1 + cTexHeight;
                    }

                    //--Store the texture sizing.
                    nPolygon->mTexStoreW = nPolygon->rTexture->GetTrueWidth();
                    nPolygon->mTexStoreH = nPolygon->rTexture->GetTrueHeight();

                    //--Invert the offset on the Y.
                    nPolygon->mYTexOffset = nPolygon->mTexStoreH - nPolygon->mYTexOffset;

                    //--Add it to the list.
                    tPolyList->AddElement("X", nPolygon, &FreeThis);
                    DebugManager::Print(" Linedef %i %s added a polygon: %0.0f to %0.0f\n", i, nPolygon->mTexName, nPolygon->mVertZ0, nPolygon->mVertZ1);
                }
            }

        }
    }

    //--Create the quick-access polygons list.
    mWallPolysTotal = tPolyList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mWallPolys = (WAD_WallPoly *)starmemoryalloc(sizeof(WAD_WallPoly) * mWallPolysTotal);
    for(int i = 0; i < mWallPolysTotal; i ++)
    {
        WAD_WallPoly *rPolygon = (WAD_WallPoly *)tPolyList->GetElementBySlot(i);
        memcpy(&mWallPolys[i], rPolygon, sizeof(WAD_WallPoly));
    }
    DebugManager::Print("There are %i wall polygons total\n", mWallPolysTotal);

    //--Clean up.
    delete tPolyList;
    DebugManager::PopPrint("[Precache Walls] Complete.\n");
}
