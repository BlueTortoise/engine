//--[BlueSphereLevel]
//--Minigame used in Chapter 5 to win money and have some fun. It's Blue Sphere! From Sonic 3!

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"

//--[Local Structures]
//--[Local Definitions]
#define BSL_EMPTY 0
#define BSL_RED_SPHERE 1
#define BSL_RED_SPHERE_WAS_BLUE 2
#define BSL_BLUE_SPHERE 3
#define BSL_RING 4
#define BSL_BUMPER 5
#define BSL_LAUNCHER 6

#define BSL_TURN_NONE 0
#define BSL_TURN_LEFT 1
#define BSL_TURN_RIGHT 2

#define BSL_OBJ_X 30
#define BSL_OBJ_Y 60

#define BSL_VICTORY_NONE 0
#define BSL_VICTORY_BURST 1
#define BSL_VICTORY_SHOWRESULTS 2
#define BSL_VICTORY_TRANSITIONOUT 3

#define BSL_RESULT_INCOMPLETE 0
#define BSL_RESULT_COMPLETE 1
#define BSL_RESULT_PERFECT 2

#define BSL_TURN_TICKS 25
#define BSL_JUMP_DISTANCE 120
#define BSL_LAUNCH_DISTANCE 520
#define BSL_BUMPER_DISTANCE 300
#define BSL_BUMPER_BACKING_TIME 60.0f

//--[Classes]
class SugarCamera3D;
class BlueSphereLevel : public RootLevel
{
    private:
    //--System
    int mWaitTimer;
    int mBlueSpheresRemaining;
    RootLevel *mSuspendedLevel;

    //--Level Select
    bool mIsLevelSelectMode;
    int mLastLevelSelected;
    int mLevelsTotal;
    int mLevelSelectPage;
    int mLevelSelectCursor;
    int *mLevelResults;

    //--Map
    int mXSize;
    int mYSize;
    int **mMapData;

    //--Object Grid
    int mObjectGrid[BSL_OBJ_X][BSL_OBJ_Y];

    //--Player Data
    float mPlayerX;
    float mPlayerY;
    int mPlayerFacing;

    //--Moving
    int mTotalPlayerMoveTicks;
    int mColorCountOffset;
    float mPlayerMoveTicks;
    int mPlayerMoveTicksTotal;
    int mPlayerMoveTicksMax;
    float mPlayerSpeed;
    float mPlayerMoveProgress;

    //--Bumper Backwards Moving
    int mBumperTurnCooldown;
    int mBumperBackingTicksLeft;
    int mBumperBackingTicksMax;
    int mBumperBackingProgress;

    //--Turning
    int mTurnDirection;
    int mTurnTimer;
    float mTurnProgress;
    float mTurnBase;

    //--Jumping
    int mJumpDistanceLeft;
    int mJumpDistanceMax;
    int mJumpCooldown;

    //--Victory
    bool mIsDefeat;
    int mVictoryMode;
    int mVictoryTimer;

    //--Camera Position
    SugarCamera3D *mLocalCamera;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            SugarFont *rSystemFont;
            SugarBitmap *rPlayerForward;
            SugarBitmap *rPlayerWalkImages[4];
            SugarBitmap *rPlayerJumpImage;
            SugarBitmap *rPlayerShadowImage;
            SugarBitmap *rBlueSphere;
            SugarBitmap *rRedSphere;
            SugarBitmap *rYellowSphere;
            SugarBitmap *rBumperSphere;
        }Data;
    }Images;

    protected:

    public:
    //--System
    BlueSphereLevel();
    virtual ~BlueSphereLevel();

    //--Public Variables
    //--Property Queries
    bool IsReady();
    bool IsPointRed(int pX, int pY);
    bool IsValidCenter(int pX, int pY);
    bool IsValidRemove(int pX, int pY);

    //--Manipulators
    void SizeMap(int pXSize, int pYSize);
    void SetMapElement(int pX, int pY, int pElement);
    void SetPlayerPosition(float pX, float pY);
    void SetPlayerFacing(int pDirection);
    void PlayerEnterPosition(int pX, int pY);
    void ProvideSuspendedLevel(RootLevel *pLevel);

    //--Core Methods
    void RemoveLoopedSpheres(int pStartX, int pStartY);

    //--Level Select
    void ActivateLevelSelect(int pLevelsTotal);
    void SetLevelState(int pSlot, int pResult);
    void UpdateLevelSelect();
    void RenderLevelSelect();

    private:
    //--Private Core Methods
    void ClampPosition(int &sX, int &sY);
    void ClampPositionF(float &sX, float &sY);
    void RunRedChecker(int pOriginalX, int pOriginalY, int pX, int pY, int pDirection, int pTurnFlag, int pDepth, SugarLinkedList *pResultList, TwoDimensionReal pExtent);

    public:
    //--Update
    virtual void Update();

    //--File I/O
    void ReadFromFile(const char *pPath);

    //--Drawing
    virtual void AddToRenderList(SugarLinkedList *pRenderList);
    virtual void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_BSL_Create(lua_State *L);
int Hook_BSL_CreateAndStore(lua_State *L);
int Hook_BSL_SetProperty(lua_State *L);
