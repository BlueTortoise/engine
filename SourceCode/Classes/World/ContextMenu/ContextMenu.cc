//--Base
#include "ContextMenu.h"

//--Classes
#include "Actor.h"
#include "CorrupterMenu.h"
#include "InventoryItem.h"

//--CoreClasses
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
ContextMenu::ContextMenu()
{
    //--[ContextMenu]
    //--System
    mHandledUpdate = false;
    mHasPendingClose = false;

    //--Rendering
    mIndent = 3.0f;
    mTextSize = 1.0f;
    mTextSpacing = 0.0f;
    mTextColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    mBorderColor.SetRGBI(192, 192, 192);
    mInnerColor.SetRGBAI(0, 0, 0, 192);
    rRenderFont = NULL;

    //--Position
    mLft = 0.0f;
    mTop = 0.0f;
    mWid = 10.0f;
    mHei = 10.0f;

    //--Storage
    mSelectionCursor = -1;
    mCommandList = new SugarLinkedList(true);

    //--[Construction]
    //--Resolve the font.
    rRenderFont = DataLibrary::Fetch()->GetFont("Context Menu Main");
    if(!rRenderFont) return;

    //--Spacing is based on the font.
    mTextSpacing = (rRenderFont->GetTextHeight() * mTextSize) + 2.0f;
}
ContextMenu::~ContextMenu()
{
    delete mCommandList;
}

//====================================== Property Queries =========================================
bool ContextMenu::HasContents()
{
    return (mCommandList->GetListSize() > 0);
}
bool ContextMenu::HasPendingClose()
{
    return mHasPendingClose;
}
bool ContextMenu::HandledUpdate()
{
    return mHandledUpdate;
}
float ContextMenu::GetWidth()
{
    return mWid;
}
float ContextMenu::GetHeight()
{
    return mHei;
}

//========================================= Manipulators ==========================================
void ContextMenu::ClearMenu()
{
    mCommandList->ClearList();
}
void ContextMenu::RegisterLuaCommand(const char *pString, const char *pScript)
{
    if(!pString || !pScript) return;
    SetMemoryData(__FILE__, __LINE__);
    ContextOption *nOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
    nOption->Setup(pString, pScript);
    mCommandList->AddElement("X", nOption, &ContextOption::DeleteThis);
}
void ContextMenu::RegisterFunctionCommand(const char *pString, LogicFnPtr pFunction)
{
    if(!pString || !pFunction) return;
    SetMemoryData(__FILE__, __LINE__);
    ContextOption *nOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
    nOption->Setup(pString, pFunction, false, NULL);
    mCommandList->AddElement("X", nOption, &ContextOption::DeleteThis);
}
void ContextMenu::RegisterCommand(ContextOption *pOption)
{
    if(!pOption) return;
    mCommandList->AddElement("X", pOption, &ContextOption::DeleteThis);
}
void ContextMenu::SetPosition(float pLft, float pTop)
{
    mLft = pLft;
    mTop = pTop;
}
void ContextMenu::Reset()
{
    //--[Basics]
    //--System
    mHandledUpdate = false;
    mHasPendingClose = false;

    //--Rendering
    mIndent = 3.0f;
    mTextSize = 1.0f;
    mTextSpacing = 0.0f;
    mTextColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    mBorderColor.SetRGBI(192, 192, 192);
    mInnerColor.SetRGBAI(0, 0, 0, 192);
    rRenderFont = NULL;

    //--Position
    mLft = 0.0f;
    mTop = 0.0f;
    mWid = 10.0f;
    mHei = 10.0f;

    //--Storage
    mSelectionCursor = -1;
    mCommandList->ClearList();

    //--[Construction]
    //--Resolve the font.
    rRenderFont = DataLibrary::Fetch()->GetFont("Context Menu Main");
    if(!rRenderFont) return;

    //--Spacing is based on the font.
    mTextSpacing = (rRenderFont->GetTextHeight() * mTextSize) + 2.0f;
}

//========================================= Core Methods ==========================================
void ContextMenu::PopulateAroundActor(Actor *pTarget)
{
    //--Routing function. Populates the ContextMenu based on the passed in target.
    //  Always clears the menu even if the target is NULL.
    mCommandList->ClearList();
    mWid = 10.0f;
    mHei = 10.0f;
    if(!pTarget) return;

    //--Get the player character. This is who the menu is relative to.
    Actor *rPlayerActor = EntityManager::Fetch()->GetLastPlayerEntity();

    //--Run the local population command. Oftentimes the population will require inventory access,
    //  so for simplicity the Actor does it by itself.
    rPlayerActor->PopulateContextMenuBy(pTarget, this);

    //--If the CorrupterMenu exists, ask it to append its options.
    CorrupterMenu *rCorrupterMenu = CorrupterMenu::Fetch();
    if(rCorrupterMenu) rCorrupterMenu->PopulateContextAroundActor(pTarget, this);

    //--Now calculate the needed sizes.
    CalculateAndResize();
}
void ContextMenu::PopulateAroundItem(InventoryItem *pItem, bool pActorCantPickup)
{
    //--Populates the menu around an Item, which can always be either examined or picked up. Note
    //  that picking up a weapon or armor will equip it automatically in some cases, and also force-drop
    //  the replaced piece (if it exists) if the flag is set to do so.
    //--Don't let that distract you, on with the function!
    mCommandList->ClearList();
    mWid = 10.0f;
    mHei = 10.0f;
    if(!pItem) return;

    //--The first command is always the item-examine command. Don't confuse this with Actor-examine,
    //  because that will cause a crash.
    SetMemoryData(__FILE__, __LINE__);
    ContextOption *nOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
    nOption->Setup("Examine", ContextMenu::ExamineTargetItem, false, pItem);
    RegisterCommand(nOption);

    //--If we're unable to pickup items, because we're stunned or a monster, examine is all we can do.
    if(!pActorCantPickup)
    {
        //--The second command is named based on what it does, though it still picks up the item.
        SetMemoryData(__FILE__, __LINE__);
        nOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
        if(pItem->GetItemType() == ITEM_TYPE_CONSUMABLE)
        {
            nOption->Setup("Take", ContextMenu::PickUpItem, false, pItem);
        }
        //--Weapons and Armor will equip instead. It does the same thing, though.
        else if(pItem->GetItemType() == ITEM_TYPE_ARMOR || pItem->GetItemType() == ITEM_TYPE_WEAPON)
        {
            nOption->Setup("Equip", ContextMenu::PickUpItem, false, pItem);
        }
        //--Error case!
        else
        {
            free(nOption);
            nOption = NULL;
            fprintf(stderr, "Error no type %i known!\n", pItem->GetItemType());
        }

        //--If the item came back NULL, it was in error. Don't register it!
        if(nOption) RegisterCommand(nOption);

        //--Some items will have a 'use' case when on the ground. If this flag is true, do that.
        if(pItem->CanUseOffGround())
        {
            SetMemoryData(__FILE__, __LINE__);
            nOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
            nOption->Setup("Use", ContextMenu::UseGroundItem, false, pItem);
            RegisterCommand(nOption);
        }
    }

    //--If the CorrupterMenu exists, ask it to append its options.
    CorrupterMenu *rCorrupterMenu = CorrupterMenu::Fetch();
    if(rCorrupterMenu) rCorrupterMenu->PopulateContextAroundItem(pItem, this);

    //--Now calculate the needed sizes.
    CalculateAndResize();
}
void ContextMenu::PopulateAroundContainer(WorldContainer *pContainer, bool pActorCantPickup)
{
    //--Populates around a WorldContainer, which can be opened and examined. That's it.
    mCommandList->ClearList();
    mWid = 10.0f;
    mHei = 10.0f;
    if(!pContainer) return;

    //--The first command is always the item-examine command. It has its own function pointer.
    SetMemoryData(__FILE__, __LINE__);
    ContextOption *nOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
    nOption->Setup("Examine", ContextMenu::ExamineTargetContainer, false, pContainer);
    RegisterCommand(nOption);

    //--If the Actor can pickup items, they can open containers.
    if(!pActorCantPickup)
    {
        //--The second command is named based on what it does, though it still picks up the item.
        SetMemoryData(__FILE__, __LINE__);
        nOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
        nOption->Setup("Open", ContextMenu::OpenContainer, false, pContainer);
        RegisterCommand(nOption);
    }

    //--If the CorrupterMenu exists, ask it to append its options.
    CorrupterMenu *rCorrupterMenu = CorrupterMenu::Fetch();
    if(rCorrupterMenu) rCorrupterMenu->PopulateContextAroundContainer(pContainer, this);

    //--Now calculate the needed sizes.
    CalculateAndResize();
}

//===================================== Private Core Methods ======================================
void ContextMenu::CalculateAndResize()
{
    //--Once the menu has been populated, figures out what the longest string in the menu is and
    //  resizes the window to accomodate it.
    if(!rRenderFont) return;

    //--Get the longest display string.
    ContextOption *rOption = (ContextOption *)mCommandList->PushIterator();
    while(rOption)
    {
        //--Get the length. If it's longer, store it. Note that it's possible for a smaller number
        //  of letters to be wider.
        int tTextLen = rRenderFont->GetTextWidth(rOption->mString) * mTextSize;
        if(tTextLen > mWid) mWid = tTextLen;

        rOption = (ContextOption *)mCommandList->AutoIterate();
    }

    //--The width is padded out to include the borders and some edge space.
    mWid = mWid + (mIndent * 2.0f) + (15.0f);

    //--Place the height based on how many options there were.
    mHei = (mIndent * 3.0f) + (mCommandList->GetListSize() * mTextSpacing);
}

//============================================ Update =============================================
void ContextMenu::Update(int pMouseX, int pMouseY)
{
    //--Runs an update. This is subservient to the owning class, and does not always run.
    mSelectionCursor = -1;
    mHandledUpdate = false;
    mHasPendingClose = false;

    //--Updates are only considered handled if the mouse is over this object. If the user left-clicks
    //  outside the menu, it should be hidden (as is standard windows behavior). This should be
    //  handled by the caller.
    if(pMouseX < mLft || pMouseX >= mLft + mWid + 1.0f || pMouseY < mTop || pMouseY >= mTop + mHei + 1.0f) return;
    mHandledUpdate = true;

    //--Change the selection cursor based on the mouse position.
    pMouseY = pMouseY - mTop - mIndent;
    mSelectionCursor = (pMouseY / mTextSpacing);
    if(mSelectionCursor >= mCommandList->GetListSize()) mSelectionCursor = -1;

    //--If the user left-clicks, then execute the given instruction and close this window.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        mHasPendingClose = true;
        ExecuteContextOption((ContextOption *)mCommandList->GetElementBySlot(mSelectionCursor));
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void ContextMenu::Render()
{
    //--Render this menu. This is subservient to the owning class, and does not always run. Also
    //  doesn't render if there's nothing on the command list.
    if(mCommandList->GetListSize() < 1) return;

    //--Position setup.
    float cInd = mIndent;
    glTranslatef(mLft, mTop, 0.0f);

    //--[Border]
    //--Grey edge.
    glDisable(GL_TEXTURE_2D);
    mBorderColor.SetAsMixer();
    glBegin(GL_QUADS);
        //--Top bar
        glVertex2f(0.0f, 0.0f);
        glVertex2f(mWid, 0.0f);
        glVertex2f(mWid, cInd);
        glVertex2f(0.0f, cInd);

        //--Bot bar
        glVertex2f(0.0f, mHei - cInd);
        glVertex2f(mWid, mHei - cInd);
        glVertex2f(mWid, mHei);
        glVertex2f(0.0f, mHei);

        //--Lft bar
        glVertex2f(0.0f, cInd);
        glVertex2f(cInd, cInd);
        glVertex2f(cInd, mHei - cInd);
        glVertex2f(0.0f, mHei - cInd);

        //--Rgt bar
        glVertex2f(mWid - cInd, cInd);
        glVertex2f(mWid,        cInd);
        glVertex2f(mWid,        mHei - cInd);
        glVertex2f(mWid - cInd, mHei - cInd);
    glEnd();

    //--Window.
    mInnerColor.SetAsMixer();
    glBegin(GL_QUADS);
        glVertex2f(       mIndent,        mIndent);
        glVertex2f(mWid - mIndent,        mIndent);
        glVertex2f(mWid - mIndent, mHei - mIndent);
        glVertex2f(       mIndent, mHei - mIndent);
    glEnd();


    //--[Selection Underlay]
    //--If there is a selection in progress, render a grey box under it.
    if(mSelectionCursor >= 0)
    {
        //--Color.
        glColor3f(0.25f, 0.25f, 0.25f);

        //--Position
        float tTop = ((mIndent * 1.0f)) + (mTextSpacing * (mSelectionCursor));
        float tBot = tTop + mTextSpacing;
        glBegin(GL_QUADS);
            glVertex2f(       mIndent, tTop);
            glVertex2f(mWid - mIndent, tTop);
            glVertex2f(mWid - mIndent, tBot);
            glVertex2f(       mIndent, tBot);
        glEnd();
    }

    //--Clean up.
    glEnable(GL_TEXTURE_2D);

    //--[Contents]
    float tCursor = mIndent;
    if(rRenderFont)
    {
        //--Text coloration.
        mTextColor.SetAsMixer();

        //--Loop through the options and render them. Render the highlighted option in the inverted color.
        int tCount = 0;
        ContextOption *rOption = (ContextOption *)mCommandList->PushIterator();
        while(rOption)
        {
            //--Render.
            rRenderFont->DrawText(mIndent, tCursor, 0, 1.0f, rOption->mString);

            //--Reposition.
            tCursor = tCursor + mTextSpacing;

            //--Next.
            tCount ++;
            rOption = (ContextOption *)mCommandList->AutoIterate();
        }
    }

    //--Final clean up.
    StarlightColor::ClearMixer();
    glTranslatef(-mLft, -mTop, 0.0f);
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
void ContextMenu::ExecuteContextOption(ContextOption *pOption)
{
    //--Executes a context option. This may involve calling the LuaManager or the encapsulated
    //  function. This is a static method and can be used anywhere in the program.
    if(!pOption) return;

    //--Lua-version.
    if(pOption->mRunLua)
    {
        LuaManager::Fetch()->ExecuteLuaFile(pOption->mLuaScript);
    }
    //--Function version.
    else if(pOption->mRunFunction && pOption->rLogicFunction)
    {
        pOption->rLogicFunction(pOption->mData);
    }
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
