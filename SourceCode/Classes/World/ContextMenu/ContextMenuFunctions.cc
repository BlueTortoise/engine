//--Base
#include "ContextMenu.h"

//--Classes
#include "Actor.h"
#include "InventoryItem.h"
#include "LuaContextOption.h"
#include "PandemoniumLevel.h"
#include "PandemoniumRoom.h"
#include "VisualLevel.h"
#include "WorldContainer.h"

//--CoreClasses
#include "DataList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "EntityManager.h"

void ContextMenu::ExamineTargetActor(void *pTargetPtr)
{
    //--Examines the target. This doesn't consume a turn, it just prints off a message describing
    //  what was clicked on.
    if(!pTargetPtr) return;

    //--The target should be an Actor.
    Actor *rTarget = (Actor *)pTargetPtr;

    //--Activate story display if a VisualLevel exists.
    VisualLevel *rVisualLevel = VisualLevel::Fetch();
    if(rVisualLevel) rVisualLevel->ActivateStoryMode();

    //--Print the description to the console.
    PandemoniumLevel::AppendToConsoleStatic(rTarget->GetDescription());

    //--If the Actor has an examination image, show it.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(rActiveLevel && rTarget->GetActiveImage())
    {
        rActiveLevel->SetActivePortrait(rTarget->GetActiveImage());
        rActiveLevel->SetActivePortraitName(rTarget->GetName());
    }

    //--If the Actor has combat statistics, print those.
    if(rTarget->HasCombatStatistics())
    {
        //--Base.
        char tBuffer[128];
        CombatStats tCombatStats = rTarget->GetCombatStatistics();

        //--Print.
        sprintf(tBuffer, "%s has %i/%i health left.", rTarget->GetName(), tCombatStats.mHP, tCombatStats.mHPMax);
        PandemoniumLevel::AppendToConsoleStatic(tBuffer);
        sprintf(tBuffer, "%s has %i/%i willpower left.", rTarget->GetName(), tCombatStats.mWillPower, tCombatStats.mWillPowerMax);
        PandemoniumLevel::AppendToConsoleStatic(tBuffer);
        sprintf(tBuffer, "%s has %i/%i stamina left.", rTarget->GetName(), tCombatStats.mStamina, tCombatStats.mStaminaMax);
        PandemoniumLevel::AppendToConsoleStatic(tBuffer);

        //--If infected, note that here.
        void *rSlimeTFPtr = rTarget->GetDataList()->FetchDataEntry("iSlimeTFTurns");
        if(rSlimeTFPtr && rTarget->GetTeam() == TEAM_PLAYER)
        {
            //--Cast.
            float *rSlimeTFTurnsPtr = (float *)rSlimeTFPtr;
            int tValue = (int)(*rSlimeTFTurnsPtr);
            if(tValue > 0)
            {
                sprintf(tBuffer, "Infected by a slime! (%i turns left)", 30 - tValue);
                PandemoniumLevel::AppendToConsoleStatic(tBuffer);
            }
        }

        //--If stunned, note that here.
        if(rTarget->GetTurnsToRecovery() > 0)
        {
            sprintf(tBuffer, "Stunned! (%i turns left)", rTarget->GetTurnsToRecovery());
            PandemoniumLevel::AppendToConsoleStatic(tBuffer);
        }

        //--Padding line.
        if(Actor::xPrintPaddingLines) PandemoniumLevel::AppendToConsoleStatic(" ");
    }
}
void ContextMenu::ExamineTargetItem(void *pTargetPtr)
{
    //--Examines the target, assuming it's an InventoryItem. This will print a message describing it.
    //  If the item happens to have combat statistics, those are displayed as well.
    if(!pTargetPtr) return;

    //--The target should be an InventoryItem.
    InventoryItem *rTarget = (InventoryItem *)pTargetPtr;

    //--Print the description to the console.
    PandemoniumLevel::AppendToConsoleStatic(rTarget->GetDescription());

    //--If the item is a piece of equipment, also print its stats out.
    if(rTarget->GetItemType() == ITEM_TYPE_ARMOR || rTarget->GetItemType() == ITEM_TYPE_WEAPON)
    {
        //--Buffer.
        char tBuffer[128];

        //--Attack power.
        sprintf(tBuffer, "Damage: %+i", rTarget->GetAttackPower());
        PandemoniumLevel::AppendToConsoleStatic(tBuffer);

        //--Accuracy power.
        sprintf(tBuffer, "Accuracy: %+i", rTarget->GetAccuracyBonus());
        PandemoniumLevel::AppendToConsoleStatic(tBuffer);

        //--Defense power.
        sprintf(tBuffer, "Defence: %+i", rTarget->GetDefensePower());
        PandemoniumLevel::AppendToConsoleStatic(tBuffer);
    }

    //--Padding line.
    if(Actor::xPrintPaddingLines) PandemoniumLevel::AppendToConsoleStatic(" ");
}
void ContextMenu::ExamineTargetContainer(void *pTargetPtr)
{
    //--Examines the target, assuming it's a WorldContainer.
    if(!pTargetPtr) return;
    WorldContainer *rContainer = (WorldContainer *)pTargetPtr;

    //--Print the description to the console.
    PandemoniumLevel::AppendToConsoleStatic(rContainer->GetDescription());

    //--Padding line.
    if(Actor::xPrintPaddingLines) PandemoniumLevel::AppendToConsoleStatic(" ");
}
void ContextMenu::PickUpItem(void *pTargetPtr)
{
    //--The acting entity will pick up the target item, de-registering it from the room they are in
    //  and re-registering it in their own inventory.
    if(!pTargetPtr) return;

    //--Get the current player.
    Actor *rActingEntity = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rActingEntity) return;

    //--Send the pick-up instruction. It will handle the rest.
    rActingEntity->PickUpItemByPtr((InventoryItem *)pTargetPtr);

    //--Sound effects.
    InventoryItem::PlayPickupSound((InventoryItem *)pTargetPtr);
}
void ContextMenu::UseGroundItem(void *pTargetPtr)
{
    //--The acting entity will pick up the target item and immediately attempt to use it. Only available
    //  for some items with a special flag.
    if(!pTargetPtr) return;

    //--Get the current player.
    Actor *rActingEntity = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rActingEntity) return;

    //--Send the pick-up instruction.
    rActingEntity->PickUpItemByPtr((InventoryItem *)pTargetPtr);

    //--Send the use instruction.
    rActingEntity->UseItemByPtr((InventoryItem *)pTargetPtr);

    //--Sound effects.
    InventoryItem::PlayPickupSound((InventoryItem *)pTargetPtr);
}
void ContextMenu::AttackTargetActor(void *pTargetPtr)
{
    //--As expected, the acting entity will use their weapon (if any) to attack the target.
    //--Note: The flagging of turn-completion is handled by the subroutine.
    Actor *rActorPtr = (Actor *)pTargetPtr;
    if(!rActorPtr) return;

    //--Get the current player.
    Actor *rActingEntity = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rActingEntity) return;

    //--Attack!
    rActingEntity->AttackTarget(rActorPtr);
}
void ContextMenu::ExecuteLuaContextOption(void *pStoragePackPtr)
{
    //--The pointer is expected to be an LCOStoragePack type. It will contain the data we need.
    LCOStoragePack *rPack = (LCOStoragePack *)pStoragePackPtr;
    if(!rPack) return;

    //--Execute it.
    rPack->rContextOption->ExecuteOnTarget(rPack->rCallerPtr, rPack->rTargetPtr);
}
void ContextMenu::OpenContainer(void *pTargetPtr)
{
    //--The pointer is expected to be a WorldContainer. Open it and dump out its contents.
    WorldContainer *rContainer = (WorldContainer *)pTargetPtr;
    if(!rContainer) return;

    //--Fail if the entity is unable to act.
    Actor *rActingEntity = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rActingEntity) return;
    if(rActingEntity->IsControlled() || rActingEntity->IsStunned()) return;

    //--Printing.
    char *nConsoleString = rContainer->AssembleOpenString("You", true);
    PandemoniumLevel::AppendToConsoleStatic(nConsoleString);
    free(nConsoleString);

    //--Actually open it.
    rContainer->Open();

    //--Player ends their turn.
    rActingEntity->HandleInstruction(INSTRUCTION_COMPLETETURN);

    //--Sound effects. The player can always hear themself open a container, obviously.
    WorldContainer::PlayOpenSound();

    //--Create a new ExtraEntityRenderPack if there is a VisualLevel to handle it. This is used in 3D
    //  mode to cause a container to fade out.
    VisualLevel *rVisualLevel = VisualLevel::Fetch();
    if(!rVisualLevel || rContainer->GetVisibilityTimer() > 0) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    ExtraEntityRenderPack *nRenderPack = (ExtraEntityRenderPack *)starmemoryalloc(sizeof(ExtraEntityRenderPack));
    memset(nRenderPack, 0, sizeof(ExtraEntityRenderPack));
    nRenderPack->mParentID = 0;
    nRenderPack->mRenderSlot = rContainer->mLastRenderSlot;
    nRenderPack->mRenderTicksLeft = VL_ENTITY_FADE_TICKS;
    nRenderPack->rRenderImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/3DUI/TreasureChest");

    //--Register.
    rVisualLevel->RegisterExtraEntityPack(nRenderPack);
}
