//--[TileLayer]
//--Represents a layer of tiles, usually composited together by the TiledLevel to produce a coherent world.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "TiledLevel.h"

//--[Local Structures]
typedef struct
{
    SugarBitmap *rUseTileset;
    uint16_t mOriginalIndex;
    float mTexL;
    float mTexT;
    float mTexR;
    float mTexB;
    uint8_t mFlipFlags;
}TilesetTile;

//--[Local Definitions]
//--[Class]
class TileLayer
{
    private:
    //--System
    //--Renderable
    bool mDisableAnimation;
    bool mIsAnimated;
    bool mIsAnimatedSlowly;
    bool mIsOscillating;
    bool mDoNotRender;
    bool mIsMidground;
    float mDepth;
    bool mDisableDepthMark;

    //--Special Animation
    int mAnimationFrames;
    int mAnimationOffset;
    int mAnimationTPF;

    //--Tile Data.
    int mXSize, mYSize;
    int mXOffset, mYOffset;
    int **mData;

    //--Tile Storage.
    TilesetTile **mTiles;

    //--External.
    int mExternalTilesetsTotal;
    TilesetInfoPack *rTilesetPacks;

    //--Public Variables
    public:
    bool mIsUsingPaddedTiles;

    protected:

    public:
    //--System
    TileLayer();
    ~TileLayer();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    static float cxSizePerTile;

    //--Property Queries
    float GetDepth();
    int GetXSize();
    int GetYSize();
    int GetTileIndexAt(int pX, int pY);

    //--Manipulators
    void SetDepth(float pDepth);
    void SetDepthMaskFlag(bool pFlag);
    void SetDoNotRenderFlag(bool pFlag);
    void SetMidgroundFlag(bool pFlag);
    void SetAnimationDisable(bool pFlag);
    void SetAnimatedFlag(bool pFlag);
    void SetAnimatedSlowlyFlag(bool pFlag);
    void AnimatedManual(int pFrames, int pOffset, int pTicksPerFrame);
    void SetOscillationFlag(bool pFlag);
    void ProvideTileReferences(int pTilesetsTotal, TilesetInfoPack *pTileData);
    void ProvideTileData(int pXSize, int pYSize, int16_t *pData, bool pFlipTileTx);
    void ProvideFlipData(int pXSize, int pYSize, int16_t *pData);

    //--Core Methods
    void Clear();
    uint8_t **ProcessCollision(int &sXSize, int &sYSize);
    uint16_t **ProcessCollisionFlips(int &sXSize, int &sYSize);

    private:
    //--Private Core Methods
    void SaveTileInformation(int pTileIndex, TilesetTile &sTileData, bool pIsFlipped);

    public:
    //--Update
    //--File I/O
    //--Drawing
    void Render();
    void RenderRange(int pLft, int pTop, int pRgt, int pBot);
    void RenderTile(int pX, int pY);
    void RenderDislocated(int pLft, int pTop, int pRgt, int pBot, float pXRender, float pYRender);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

