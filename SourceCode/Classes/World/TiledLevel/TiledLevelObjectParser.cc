//--Base
#include "TiledLevel.h"

//--Classes
#include "Tiled/TiledRawData.h"

//--CoreClasses
#include "SugarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

void TiledLevel::ReadObjectData(VirtualFile *pVFile)
{
    //--Given a VirtualFile positioned just after reading the layer type ("Object"), store all the
    //  objects and order them to be parsed for useful data.
    if(!pVFile) return;

    //--First, bypass the unnecessary data. Don't store it.
    char *nName = pVFile->ReadLenString();
    free(nName);

    //--Custom properties. Uses a 16 bit integer. We don't store or parse these.
    int tCustomPropertiesTotal = 0;
    pVFile->Read(&tCustomPropertiesTotal, sizeof(int16_t), 1);

    //--Read out the properties as key-value strings. We don't store them yet.
    for(int i = 0; i < tCustomPropertiesTotal; i ++)
    {
        //--Get the key/value pair.
        char *tKey = pVFile->ReadLenString();
        char *tVal = pVFile->ReadLenString();

        //--Property types (UNUSED).

        //--Clean.
        free(tKey);
        free(tVal);
    }

    //--Collect all Object data. Store it for later.
    int tExpectedObjects = 0;
    pVFile->Read(&tExpectedObjects, sizeof(int16_t), 1);
    for(int i = 0; i < tExpectedObjects; i ++)
    {
        //--Create a storage pack.
        SetMemoryData(__FILE__, __LINE__);
        ObjectInfoPack *nPack = (ObjectInfoPack *)starmemoryalloc(sizeof(ObjectInfoPack));
        memset(nPack, 0, sizeof(ObjectInfoPack));
        nPack->mTileID = 0;

        //--Name/Type/Bye
        nPack->mName = pVFile->ReadLenString();
        nPack->mType = pVFile->ReadLenString();

        //--Position and tile data.
        pVFile->Read(&nPack->mX, sizeof(float), 1);
        pVFile->Read(&nPack->mY, sizeof(float), 1);
        pVFile->Read(&nPack->mW, sizeof(float), 1);
        pVFile->Read(&nPack->mH, sizeof(float), 1);
        pVFile->Read(&nPack->mTileID, sizeof(int16_t), 1);
        if(nPack->mTileID >= 100) nPack->mTileID --;

        //--Properties.
        nPack->mProperties = TiledRawData::ReadProperties(pVFile);

        //--Add it to the list.
        mObjectData->AddElement("X", nPack, &ObjectInfoPack::DeleteThis);
    }
}
void TiledLevel::ParseObjectData()
{
    //--Once the data is read and stored, we can parse through it to pull out the parts we want.
    //  Note that this usually is not done right when the objects are read out, as there may be limiters
    //  set or objects might be discarded by script values.
    //--Derived class does the work. Base class doesn't know about any specific object types.
}
