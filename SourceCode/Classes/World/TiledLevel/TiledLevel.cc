//--Base
#include "TiledLevel.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "SugarLumpManager.h"

//=========================================== System ==============================================
TiledLevel::TiledLevel()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_TILEDLEVEL;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System

    //--[TiledLevel]
    //--System
    //--Sizing
    mXSize = 0;
    mYSize = 0;
    mCurrentScale = 3.0f;

    //--Tileset Packs
    mTilesetsTotal = 0;
    mTilesetPacks = NULL;

    //--Raw Lists
    mTileLayers = new SugarLinkedList(true);
    mObjectData = new SugarLinkedList(true);
    mImageData = new SugarLinkedList(true);

    //--Collision Storage
    mCollisionLayersTotal = 0;
    mCollisionLayers = NULL;
}
TiledLevel::~TiledLevel()
{
    //--Lists.
    delete mTileLayers;
    delete mObjectData;
    delete mImageData;

    //--Collision.
    for(int i = 0; i < mCollisionLayersTotal; i ++)
    {
        CollisionPack::FreeThis(&mCollisionLayers[i]);
    }
    free(mCollisionLayers);
}

//====================================== Property Queries =========================================
bool TiledLevel::GetClipAt(float pX, float pY, float pZ, int pLayer)
{
    //--Range check: Must be within an existing collision layer.
    if(pLayer < 0 || pLayer >= mCollisionLayersTotal) return true;

    //--Edge check.
    if(pX < 0.0f || pY < 0.0f || pX >= mCollisionLayers[pLayer].mCollisionSizeX || pY >= mCollisionLayers[pLayer].mCollisionSizeY) return true;

    //--Return from the collision hull.
    return mCollisionLayers[pLayer].mCollisionData[(int)pX][(int)pY];
}
bool TiledLevel::GetClipAt(float pX, float pY, float pZ)
{
    return GetClipAt(pX, pY, pZ, 0);
}

//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
void TiledLevel::CompileAfterParse()
{
    //--Called after ParseSLF(), compiles any data that wasn't in the SLF. Does nothing in the default
    //  case, but can be overridden.
}

//============================================ Update =============================================
void TiledLevel::Update()
{
    //--Does nothing by itself.
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void TiledLevel::AddToRenderList(SugarLinkedList *pRenderList)
{
    pRenderList->AddElementAsTail("X", this);
}
void TiledLevel::Render()
{
    //--Does nothing by itself. Derived classes should do something here.
}

//======================================= Pointer Routing =========================================
SugarLinkedList *TiledLevel::GetObjectData()
{
    return mObjectData;
}

//====================================== Static Functions =========================================
TiledLevel *TiledLevel::Fetch()
{
    //--Note: Will always return a valid TiledLevel, or NULL. Never returns an object of the wrong type.
    RootLevel *rCheckLevel = MapManager::Fetch()->GetActiveLevel();
    if(!rCheckLevel || !rCheckLevel->IsOfType(POINTER_TYPE_TILEDLEVEL)) return NULL;
    return (TiledLevel *)rCheckLevel;
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
