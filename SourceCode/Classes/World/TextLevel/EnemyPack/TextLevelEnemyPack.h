//--[Combat Enemy Packs]
//--These are the storage structures for enemies in Text Adventure Mode. Both types of combat use the same pack
//  but might not use the same variables.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "TextLevelStructures.h"
#include "DeletionFunctions.h"
#include "SugarLinkedList.h"

//--[Local Structures]
//--[Local Definitions]
//--[CombatEnemyAttackPack]
//--Represents an attack done by an enemy. Enemies can have multiple attacks to choose from based
//  on a weighted roll. Attacks can have an elemental flag and associated attack power. Prep Ticks
//  is how many ticks until the attack goes off, giving a rough idea of DPS.
typedef struct CombatEnemyAttackPack
{
    int mRollWeight;
    int mAttackPower;
    int mPrepTicks;
    uint8_t mElementFlags; //Uses the TL_ELEMENT_FIRE series.
}CombatEnemyAttackPack;

//--[CombatEnemyPack]
//--Represents an enemy in combat in Text Adventure mode. The update and render routines are in WordCombat.cc
//  in the update/render subcategories.
#define CEP_STUN_TICKS 540
#define CEP_HP_TICKS 30
#define CEP_AP_TICKS 5
typedef struct CombatEnemyPack
{
    //--Common Variables
    int mHP;
    int mHPMax;
    int mFlashTimer;
    int mStunTimer;
    SugarBitmap *rImage;

    //--HP Falloff
    int mHPTimer;
    float mHPRender;
    float mHPStart;
    float mHPEnd;

    //--Typing-Mode Variables
    int mAttackPower;
    int mDefensePower;

    //--Sentence-Mode Variables
    int mTotalAttackWeight;
    int mCurrentAttack;
    int mPrepTicksLeft;
    int mPrepTicksMax;
    int mAttackTicks;
    int mAttackTicksNeeded;
    float mAPRender;
    SugarLinkedList *mAttackList;

    //--Elemental Weaknesses
    int mElementWeakness[TL_ELEMENTS_TOTAL];

    //--System
    void Initialize()
    {
        //--Common Variables
        mHP = 1;
        mHPMax = 1;
        mFlashTimer = 0;
        mStunTimer = 0;
        rImage = NULL;

        //--HP Falloff
        mHPTimer = CEP_HP_TICKS;
        mHPRender = 1.0f;
        mHPStart = 1.0f;
        mHPEnd = 1.0f;

        //--Typing-Mode Variables
        mAttackPower = 2;
        mDefensePower = 1;

        //--Sentence-Mode Variables
        mTotalAttackWeight = 0;
        mCurrentAttack = -1;
        mPrepTicksLeft = 0;
        mPrepTicksMax = 1;
        mAttackTicks = 0;
        mAttackTicksNeeded = 0;
        mAPRender = 0.0f;
        mAttackList = NULL;

        //--Elemental Weaknesses
        memset(mElementWeakness, 0, sizeof(int) * TL_ELEMENTS_TOTAL);
    }
    static void DeleteThis(void *pPtr)
    {
        CombatEnemyPack *rPtr = (CombatEnemyPack *)pPtr;
        delete rPtr->mAttackList;
        free(rPtr);
    }

    //--Common
    void SetHP(int pHP, int pMaxHP)
    {
        mHP = pHP;
        mHPMax = pMaxHP;
        if(mHPMax < 1) mHPMax = 1;
        if(mHP > mHPMax) mHP = mHPMax;
        if(mHP < 1) mHP = 1;
    }
    void SetWeakness(int pSlot, int pAmount)
    {
        if(pSlot < 0 || pSlot >= TL_ELEMENTS_TOTAL) return;
        mElementWeakness[pSlot] = pAmount;
    }

    //--Sentence Mode
    void BootSentenceMode()
    {
        //--Activates variables for use in sentence-typing combat.
        mAttackList = new SugarLinkedList(true);
    }
    void AddSentenceAttack(int pWeight, int pPower, int pTicks, uint8_t pElementalFlags)
    {
        //--Error check.
        if(!mAttackList) return;

        //--Argument check.
        if(pWeight < 1) pWeight = 1;
        if(pPower < 1) pPower = 1;
        if(pTicks < 1) pTicks = 1;

        //--Create and add.
        SetMemoryData(__FILE__, __LINE__);
        CombatEnemyAttackPack *nAttackPack = (CombatEnemyAttackPack *)starmemoryalloc(sizeof(CombatEnemyAttackPack));
        nAttackPack->mRollWeight = pWeight;
        nAttackPack->mAttackPower = pPower;
        nAttackPack->mPrepTicks = pTicks;
        nAttackPack->mElementFlags = pElementalFlags;
        mAttackList->AddElement("X", nAttackPack, &FreeThis);

        //--Increase total weight.
        mTotalAttackWeight += pWeight;
    }
}CombatEnemyPack;
