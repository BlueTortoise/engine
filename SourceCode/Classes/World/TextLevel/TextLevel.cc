//--Base
#include "TextLevel.h"

//--Classes
#include "DeckEditor.h"
#include "PortableLuaState.h"
#include "TextLevOptions.h"
#include "TypingCombat.h"
#include "StringEntry.h"
#include "WordCombat.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFileSystem.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "HitDetection.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioPackage.h"
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "OptionsManager.h"

//=========================================== System ==============================================
TextLevel::TextLevel()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_TEXTLEVEL;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System

    //--[TiledLevel]
    //--System
    //--Sizing
    mCurrentScale = 1.0f;

    //--Tileset Packs
    //--Raw Lists
    //--Collision Storage

    //--[TextLevel]
    //--System
    mIDCounter = 0;
    mSuspendedLevel = NULL;

    //--UI Flags
    mNoSettingsButton = false;
    mNoZoomButtons = false;
    mNoNavigationButtons = false;

    //--Deck Editor
    mDeckEditor = new DeckEditor();

    //--Options Menu
    mOptionsScreen = new TextLevOptions();

    //--Savefile Storage
    memset(mSavefileNames, 0, sizeof(char) * TL_MOST_RECENT_SAVES * STD_PATH_LEN);
    for(int i = 0; i < TL_MOST_RECENT_SAVES; i ++)
    {
        strcpy(mSavefileNames[i], "Null");
    }

    //--Music
    mPlayTextTick = false;
    mMusicTracksList = new SugarLinkedList(true);

    //--Images
    mIsDarkMode = true;
    mImgTransitionTimer = 0;
    mPlayerStoredImgPath = NULL;
    mStandardName = NULL;
    mOverrideName = NULL;
    memset(rStandardRenderImg, 0, sizeof(SugarBitmap *) * TL_PLAYER_LAYERS_MAX);
    memset(rOverrideRenderImg, 0, sizeof(SugarBitmap *) * TL_PLAYER_LAYERS_MAX);
    memset(rPreviousRenderImg, 0, sizeof(SugarBitmap *) * TL_PLAYER_LAYERS_MAX);

    //--Dialogue Display
    mUseLargeText = true;
    mTextScrollOffset = 0;
    cTxtStartX = 15.0f;
    cTxtEndX = 940.0f;
    cTxtWidth = cTxtEndX - cTxtStartX;
    mAppendTimer = TL_TIME_PER_LINE * -1;
    mTextLog = new SugarLinkedList(true);
    mFontLog = new SugarLinkedList(false);

    //--Player Information
    mPlayerMoveTimer = 1;
    mPlayerMoveTimerMax = 1;
    mPlayerWorldX = -1000;
    mPlayerWorldY = -1000;
    mPlayerWorldZ = -100;
    strcpy(mPlayerWorldClickHandler, "Null");
    cDoorNorth.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    cDoorEast .SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    cDoorSouth.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    cDoorWest .SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    cDoorDetectNorth.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    cDoorDetectEast .SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    cDoorDetectSouth.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    cDoorDetectWest .SetWH(0.0f, 0.0f, 1.0f, 1.0f);

    //--Map Storage
    mRebuildIndicators = true;
    mWorldOffsetX = 0;
    mWorldOffsetY = 0;
    mWorldOffsetZ = 0;
    mWorldSizeX = 0;
    mWorldSizeY = 0;
    mWorldSizeZ = 0;
    mCurrentRenderingZ = 0;
    mUnderRenderingZLo = -1000;
    mUnderRenderingZHi = -1000;
    mrFixedWorld = NULL;
    mVisitedRoomList = new SugarLinkedList(false);
    mAutomapList = new SugarLinkedList(true);
    mConnectionList = new SugarLinkedList(true);

    //--Player Map
    mPlayerMapActive = false;
    mPlayerMapTimer = 0;
    mPlayerMapLayers = new SugarLinkedList(true);

    //--Map Storage
    mActiveZLevel = 0;
    mActiveXOff = 0;
    mActiveYOff = 0;
    mZLevels = new SugarLinkedList(true);

    //--Map Builder Properties
    mLftMost = 100;
    mTopMost = 100;
    mRgtMost = -100;
    mBotMost = -100;
    mZLoMost = 100;
    mZHiMost = -100;
    mBuilderWorldX = 0;
    mBuilderWorldY = 0;
    mBuilderWorldZ = 0;
    mrBuilderWorld = NULL;
    mTempDoorList = new SugarLinkedList(true);
    mTempAudioList = new SugarLinkedList(true);
    mTempPatrolList = new SugarLinkedList(true);
    mTempLocationList = new SugarLinkedList(true);
    mMapPropertyBuffer[0] = '\0';

    //--Tileset for Automap
    mScreenTranslationX = 0.0f;
    mScreenTranslationY = 0.0f;
    mUseBlankAutomap = true;
    rAutomapTileset = NULL;
    mTilemapXSize = 1.0f;
    mTilemapYSize = 1.0f;
    mTilemapXSizePerTile = 1.0f;
    mTilemapYSizePerTile = 1.0f;
    mLineShadeX = 0.0f;
    mLineShadeY = 0.0f;
    mLineCornerShadeX = 0.0f;
    mLineCornerShadeY = 0.0f;
    mLineAlleyShadeX = 0.0f;
    mLineAlleyShadeY = 0.0f;
    mLineBoxShadeX = 0.0f;
    mLineBoxShadeY = 0.0f;
    mLineInsetShadeX = 0.0f;
    mLineInsetShadeY = 0.0f;
    mExclamationX = 0.0f;
    mExclamationY = 0.0f;
    mQuestionX = 0.0f;
    mQuestionY = 0.0f;

    //--Tileset Animations
    mTilesetAnimations = new SugarLinkedList(true);

    //--Harsh Overlay
    mIsHarshOverlayMode = false;
    mHarshOverlayTimer = 0;
    mHarshOverlayList = new SugarLinkedList(true);

    //--Fade-in
    mIsFadingIn = false;
    mIsFadingOut = false;
    mFadeTimer = 0;
    mExecScriptOnFadeCompletion = NULL;

    //--Background
    mOldMouseZ = -700.0f;
    mBGFocusX = 0.0f;
    mBGFocusY = 0.0f;
    mScaleIndex = 4;
    mScaleListing[0] = 3.7f;
    mScaleListing[1] = 2.5f;
    mScaleListing[2] = 2.0f;
    mScaleListing[3] = 1.5f;
    mScaleListing[4] = 1.0f;
    mScaleListing[5] = 1.0f / 1.25f;
    mScaleListing[6] = 1.0f / 1.50f;
    mScaleListing[7] = 1.0f / 1.75f;
    mScaleListing[8] = 1.0f / 2.00f;
    mBackgroundScale = mScaleListing[mScaleIndex];
    mMapFadeTimer = 0; //0 means "No Fade"
    mMapFadeDelta = 0;

    //--Text Input
    mLastEnteredString = InitializeString(" ");
    mLocalStringEntry = new StringEntry();
    mLocalStringEntry->mIgnoreMouse = true;
    mInputLogScroll = -1;
    mInputLog = new SugarLinkedList(false);

    //--Handling
    mScriptExecPath = NULL;

    //--Instructions
    mIsBlockPending = false;
    mInstructionList = new SugarLinkedList(true);

    //--Combat
    mIsCombatMode = TL_COMBAT_NONE;
    mUseCombatType = TL_COMBAT_INVALID;
    mUseCombatActiveMode = false;
    mPlayerCombatRenderPath = NULL;
    mTypingCombatHandler = NULL;
    mWordCombatHandler = NULL;
    mCombatVictoryHandler = NULL;
    mCombatDefeatHandler = NULL;
    mPostCombatTimer = TL_MUSIC_RESTART_POST_COMBAT + 1000;

    //--Persistent Player Stats
    mHidePlayerStats = false;
    mPlayerHP = 20;
    mPlayerHPMax = 20;
    mPlayerAtk = 5;
    mPlayerDef = 3;
    mPotionCount = 0;

    //--Additional Text
    memset(mStorageLines, 0, sizeof(char) * TL_EXTRA_LINES * STD_MAX_LETTERS);

    //--Locality Window
    mLocalityUsesIcons = false;
    mLocalityNeedsSort = false;
    mLocalityHighlight = -1;
    mLocalityScroll = 0;
    mLocalityWindow.SetWH(982.0f, 485.0f, 382.0f, 281.0f);;
    mCurrentLocalityList = new SugarLinkedList(true);
    mPreviousLocalityList = NULL;
    mIsDraggingLocalityBar = false;
    mIsDraggingTextScrollbar = false;
    mOriginalDragPoint = 0.0f;

    //--Popup Command Listing
    mPopupWindowDim.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    mPopupSelectionDim.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    mPopupCommandList = new SugarLinkedList(true);

    //--Navigation Buttons
    float cSizeX = 48.0f;
    float cSizeY = 48.0f;
    mNavButtonFlags = 0x0000;
    mSettingsDim.SetWH(6.0f, 4.0f, 42.0f, 42.0f);
    mMapAreaDim.SetWH(2.0f, 3.0f, 968.0f, 397.0f);
    memset(mNavButtonDim, 0, sizeof(TwoDimensionReal) * TL_NAV_INDEX_TOTAL);
    mNavButtonDim[TL_NAV_INDEX_LOOK].SetWH (mMapAreaDim.mLft + (cSizeX * 0.0f), mMapAreaDim.mBot - (cSizeY * 3.0f), cSizeX, cSizeY);
    mNavButtonDim[TL_NAV_INDEX_NORTH].SetWH(mMapAreaDim.mLft + (cSizeX * 1.0f), mMapAreaDim.mBot - (cSizeY * 3.0f), cSizeX, cSizeY);
    mNavButtonDim[TL_NAV_INDEX_UP].SetWH   (mMapAreaDim.mLft + (cSizeX * 2.0f), mMapAreaDim.mBot - (cSizeY * 3.0f), cSizeX, cSizeY);
    mNavButtonDim[TL_NAV_INDEX_WEST].SetWH (mMapAreaDim.mLft + (cSizeX * 0.0f), mMapAreaDim.mBot - (cSizeY * 2.0f), cSizeX, cSizeY);
    mNavButtonDim[TL_NAV_INDEX_WAIT].SetWH (mMapAreaDim.mLft + (cSizeX * 1.0f), mMapAreaDim.mBot - (cSizeY * 2.0f), cSizeX, cSizeY);
    mNavButtonDim[TL_NAV_INDEX_EAST].SetWH (mMapAreaDim.mLft + (cSizeX * 2.0f), mMapAreaDim.mBot - (cSizeY * 2.0f), cSizeX, cSizeY);
    mNavButtonDim[TL_NAV_INDEX_THINK].SetWH(mMapAreaDim.mLft + (cSizeX * 0.0f), mMapAreaDim.mBot - (cSizeY * 1.0f), cSizeX, cSizeY);
    mNavButtonDim[TL_NAV_INDEX_SOUTH].SetWH(mMapAreaDim.mLft + (cSizeX * 1.0f), mMapAreaDim.mBot - (cSizeY * 1.0f), cSizeX, cSizeY);
    mNavButtonDim[TL_NAV_INDEX_DOWN].SetWH (mMapAreaDim.mLft + (cSizeX * 2.0f), mMapAreaDim.mBot - (cSizeY * 1.0f), cSizeX, cSizeY);
    mNavButtonDim[TL_NAV_INDEX_ZOOMUP].SetWH(954.0f, 374.0f, 17.0f, 17.0f);
    mNavButtonDim[TL_NAV_INDEX_ZOOMDN].SetWH(828.0f, 374.0f, 17.0f, 17.0f);

    //--Ending Handler
    mIsMajorDialogueMode = false;
    mIsMajorDialogueModeExit = false;
    mActivateMajorDialogueWhenQueueEmpties = false;
    mMajorDialogueTimer = 0;
    mMajorDialogueFadeOut = false;
    mMajorDialogueEndsGame = false;

    //--Images
    memset(&Images, 0, sizeof(Images));
}
TextLevel::~TextLevel()
{
    delete mDeckEditor;
    delete mOptionsScreen;
    delete mMusicTracksList;
    free(mPlayerStoredImgPath);
    delete mSuspendedLevel;
    free(mStandardName);
    free(mOverrideName);
    delete mTilesetAnimations;
    delete mHarshOverlayList;
    free(mExecScriptOnFadeCompletion);
    delete mTextLog;
    delete mFontLog;
    delete mInputLog;
    delete mVisitedRoomList;
    delete mAutomapList;
    delete mConnectionList;
    delete mPlayerMapLayers;
    free(mLastEnteredString);
    delete mLocalStringEntry;
    free(mScriptExecPath);
    delete mInstructionList;
    delete mCurrentLocalityList;
    delete mPreviousLocalityList;
    delete mPopupCommandList;
    free(mPlayerCombatRenderPath);
    for(int x = 0; x < mWorldSizeX; x ++)
    {
        for(int y = 0; y < mWorldSizeY; y ++)
        {
            free(mrFixedWorld[x][y]);
        }
        free(mrFixedWorld[x]);
    }
    free(mrFixedWorld);
    delete mTempDoorList;
    delete mTempAudioList;
    delete mTempPatrolList;
    delete mTempLocationList;

    for(int x = 0; x < mBuilderWorldX; x ++)
    {
        for(int y = 0; y < mBuilderWorldY; y ++)
        {
            free(mrBuilderWorld[x][y]);
        }
        free(mrBuilderWorld[x]);
    }
    free(mrBuilderWorld);
}
void TextLevel::ConstructElectrosprite()
{
    //--Resolve images used when in Electrosprite Adventure mode.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--UI Flags
    mNoSettingsButton = true;
    mNoZoomButtons = true;
    mNoNavigationButtons = true;

    //--Fonts.
    //DataLibrary::xGetEntryDebug = true;
    Images.Data.rFontMenuSize20 = rDataLibrary->GetFont("Text Level Medium");
    Images.Data.rFontMenuSize30 = rDataLibrary->GetFont("Text Level Large");

    //--Images.
    Images.Data.rMapParts        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/EA|TextAdventureMapParts");
    Images.Data.rScrollbar       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/EA|TextAdventureScrollbar");
    Images.Data.rPopupBorderCard = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/EA|PopupBorderCard");
    Images.Data.rTextInput       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/EA|TextInput");
    Images.Data.rTextInputDark   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/EA|TextInputDark");
    Images.Data.rMask_World      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/EA|Mask_WorldRender");
    Images.Data.rMask_Text       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/EA|Mask_TextRender");
    Images.Data.rMask_Character  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/EA|Mask_CharacterRender");
    Images.Data.rMask_Locality   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/EA|Mask_LocalityRender");
    Images.Data.rHPIndicatorF    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Bar_Heart");
    Images.Data.rHPIndicatorH    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Bar_Half");
    Images.Data.rHPIndicatorE    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Bar_Empty");
    Images.Data.rOverlayMove     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Overlays/Move");
    Images.Data.rOverlayDoorH    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Overlays/DoorH");
    Images.Data.rOverlayDoorV    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Overlays/DoorV");

    //--Navigation.
    Images.Data.rNavLook     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/Look");
    Images.Data.rNavNorth    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/North");
    Images.Data.rNavUp       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/Up");
    Images.Data.rNavWest     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/West");
    Images.Data.rNavWait     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/Wait");
    Images.Data.rNavEast     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/East");
    Images.Data.rNavThink    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/Think");
    Images.Data.rNavSouth    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/South");
    Images.Data.rNavDown     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/Down");
    Images.Data.rNavZoomBar  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/ZoomBar");
    Images.Data.rNavZoomTick = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/ZoomTick");
    Images.Data.rNavImg[TL_NAV_INDEX_LOOK]  = Images.Data.rNavLook;
    Images.Data.rNavImg[TL_NAV_INDEX_NORTH] = Images.Data.rNavNorth;
    Images.Data.rNavImg[TL_NAV_INDEX_UP]    = Images.Data.rNavUp;
    Images.Data.rNavImg[TL_NAV_INDEX_WEST]  = Images.Data.rNavWest;
    Images.Data.rNavImg[TL_NAV_INDEX_WAIT]  = Images.Data.rNavWait;
    Images.Data.rNavImg[TL_NAV_INDEX_EAST]  = Images.Data.rNavEast;
    Images.Data.rNavImg[TL_NAV_INDEX_THINK] = Images.Data.rNavThink;
    Images.Data.rNavImg[TL_NAV_INDEX_SOUTH] = Images.Data.rNavSouth;
    Images.Data.rNavImg[TL_NAV_INDEX_DOWN]  = Images.Data.rNavDown;
    Images.Data.rNavImg[TL_NAV_INDEX_ZOOMUP] = Images.Data.rNavLook; //Not actually used
    Images.Data.rNavImg[TL_NAV_INDEX_ZOOMDN] = Images.Data.rNavLook; //Not actually used
    //DataLibrary::xGetEntryDebug = false;

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));
    if(!Images.mIsReady) return;

    //--[Door World Sizes]
    //--Sizings
    mSizePerRoomX = 64.0f;
    mSizePerRoomY = 64.0f;
    float cDoorHWid = 21.0f / mSizePerRoomX * 0.50f;
    float cDoorHHei =  8.0f / mSizePerRoomY * 0.50f;
    float cDoorVWid =  8.0f / mSizePerRoomX * 0.50f;
    float cDoorVHei = 21.0f / mSizePerRoomY * 0.50f;

    //--Door Positions.
    cDoorNorth.SetWH(0.50f - cDoorHWid, 0.00f,             cDoorHWid, cDoorHHei);
    cDoorEast .SetWH(1.00f - cDoorVWid, 0.50f - cDoorVHei, cDoorVWid, cDoorVHei);
    cDoorSouth.SetWH(0.50f - cDoorHWid, 1.00f - cDoorHHei, cDoorHWid, cDoorHHei);
    cDoorWest .SetWH(0.00f,             0.50f - cDoorVHei, cDoorVWid, cDoorVHei);

    //--[Options Manager]
    //--Dark Mode and Text Size are resolved from the Options Manager.
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    int tDarkMode = rOptionsManager->GetOptionI("String Tyrant Dark Mode");
    int tLargeText = rOptionsManager->GetOptionI("String Tyrant Large Text");
    SetDarkMode(tDarkMode == 1);
    SetLargeText(tLargeText == 1);

    //--[Options Menu]
    mOptionsScreen->Construct(this);

    //--[Clear]
    //--World dialogue disappears.
    WorldDialogue::Fetch()->Hide();
}
void TextLevel::ConstructStringTyrant()
{
    //--Resolve images used when in String Tyrant mode.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--UI Flags
    mNoSettingsButton = false;
    mNoZoomButtons = false;
    mNoNavigationButtons = false;

    //--Fonts.
    //DataLibrary::xGetEntryDebug = true;
    Images.Data.rFontMenuSize20 = rDataLibrary->GetFont("Text Level Medium");
    Images.Data.rFontMenuSize30 = rDataLibrary->GetFont("Text Level Large");

    //--Images.
    Images.Data.rMapParts        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/ST|TextAdventureMapParts");
    Images.Data.rScrollbar       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/ST|TextAdventureScrollbar");
    Images.Data.rPopupBorderCard = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/ST|PopupBorderCard");
    Images.Data.rTextInput       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/ST|TextInput");
    Images.Data.rTextInputDark   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/ST|TextInputDark");
    Images.Data.rMask_World      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/ST|Mask_WorldRender");
    Images.Data.rMask_Text       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/ST|Mask_TextRender");
    Images.Data.rMask_Character  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/ST|Mask_CharacterRender");
    Images.Data.rMask_Locality   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/ST|Mask_LocalityRender");
    Images.Data.rHPIndicatorF    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Bar_Heart");
    Images.Data.rHPIndicatorH    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Bar_Half");
    Images.Data.rHPIndicatorE    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Bar_Empty");
    Images.Data.rOverlayMove     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Overlays/Move");
    Images.Data.rOverlayDoorH    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Overlays/DoorH");
    Images.Data.rOverlayDoorV    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Overlays/DoorV");

    //--Navigation.
    Images.Data.rNavLook     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/Look");
    Images.Data.rNavNorth    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/North");
    Images.Data.rNavUp       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/Up");
    Images.Data.rNavWest     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/West");
    Images.Data.rNavWait     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/Wait");
    Images.Data.rNavEast     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/East");
    Images.Data.rNavThink    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/Think");
    Images.Data.rNavSouth    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/South");
    Images.Data.rNavDown     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/Down");
    Images.Data.rNavZoomBar  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/ZoomBar");
    Images.Data.rNavZoomTick = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/Navigation/ZoomTick");
    Images.Data.rNavImg[TL_NAV_INDEX_LOOK]  = Images.Data.rNavLook;
    Images.Data.rNavImg[TL_NAV_INDEX_NORTH] = Images.Data.rNavNorth;
    Images.Data.rNavImg[TL_NAV_INDEX_UP]    = Images.Data.rNavUp;
    Images.Data.rNavImg[TL_NAV_INDEX_WEST]  = Images.Data.rNavWest;
    Images.Data.rNavImg[TL_NAV_INDEX_WAIT]  = Images.Data.rNavWait;
    Images.Data.rNavImg[TL_NAV_INDEX_EAST]  = Images.Data.rNavEast;
    Images.Data.rNavImg[TL_NAV_INDEX_THINK] = Images.Data.rNavThink;
    Images.Data.rNavImg[TL_NAV_INDEX_SOUTH] = Images.Data.rNavSouth;
    Images.Data.rNavImg[TL_NAV_INDEX_DOWN]  = Images.Data.rNavDown;
    Images.Data.rNavImg[TL_NAV_INDEX_ZOOMUP] = Images.Data.rNavLook; //Not actually used
    Images.Data.rNavImg[TL_NAV_INDEX_ZOOMDN] = Images.Data.rNavLook; //Not actually used
    //DataLibrary::xGetEntryDebug = false;

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));
    if(!Images.mIsReady) return;

    //--[Door World Sizes]
    //--Sizings
    mSizePerRoomX = 64.0f;
    mSizePerRoomY = 64.0f;
    float cDoorHWid = 21.0f / mSizePerRoomX * 0.50f;
    float cDoorHHei =  8.0f / mSizePerRoomY * 0.50f;
    float cDoorVWid =  8.0f / mSizePerRoomX * 0.50f;
    float cDoorVHei = 21.0f / mSizePerRoomY * 0.50f;

    //--Door Positions.
    cDoorNorth.SetWH(0.50f - cDoorHWid, 0.00f,             cDoorHWid, cDoorHHei);
    cDoorEast .SetWH(1.00f - cDoorVWid, 0.50f - cDoorVHei, cDoorVWid, cDoorVHei);
    cDoorSouth.SetWH(0.50f - cDoorHWid, 1.00f - cDoorHHei, cDoorHWid, cDoorHHei);
    cDoorWest .SetWH(0.00f,             0.50f - cDoorVHei, cDoorVWid, cDoorVHei);

    //--[Options Manager]
    //--Dark Mode and Text Size are resolved from the Options Manager.
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    int tDarkMode = rOptionsManager->GetOptionI("String Tyrant Dark Mode");
    int tLargeText = rOptionsManager->GetOptionI("String Tyrant Large Text");
    SetDarkMode(tDarkMode == 1);
    SetLargeText(tLargeText == 1);

    //--[Options Menu]
    mOptionsScreen->Construct(this);

    //--[Clear]
    //--World dialogue disappears.
    WorldDialogue::Fetch()->Hide();
}

//--[Public Statics]
//--This variable is used when building popup lists. It tells a given script that it should only
//  build popup information when flagged as true.
bool TextLevel::xIsBuildingCommands = false;

//--Base string for commands. Contains the display string of whatever word was displayed on the
//  locality menu when it was clicked. This is usually the name of the object.
char *TextLevel::xCommandHeadingString = InitializeString("Null");
char *TextLevel::xCommandBaseString = InitializeString("Null");

//====================================== Property Queries =========================================
bool TextLevel::CanRunInstructions()
{
    //--Waiting on a keypress.
    if(mIsBlockPending) return false;

    //--All checks passed.
    return true;
}
bool TextLevel::IsCombatActive()
{
    return (mIsCombatMode != TL_COMBAT_NONE);
}
bool TextLevel::IsLargeTextMode()
{
    return mUseLargeText;
}
bool TextLevel::IsDarkTheme()
{
    return mIsDarkMode;
}
int TextLevel::GetPlayerHP()
{
    return mPlayerHP;
}
int TextLevel::GetPotions()
{
    return mPotionCount;
}
int TextLevel::GetCurrentZoomIndex()
{
    return mScaleIndex;
}
const char *TextLevel::GetSaveIndex(int pIndex)
{
    if(pIndex < 0 || pIndex >= TL_MOST_RECENT_SAVES) return "Null";
    return mSavefileNames[pIndex];
}
int TextLevel::GetDeckEditorCardCount(int pCardType)
{
    return mDeckEditor->GetHandCount(pCardType);
}

//========================================= Manipulators ==========================================
void TextLevel::SetDarkMode(bool pFlag)
{
    mIsDarkMode = pFlag;
}
void TextLevel::SetLargeText(bool pFlag)
{
    mUseLargeText = pFlag;
}
void TextLevel::StartFadeIn(const char *pPostExec)
{
    if(!pPostExec || !strcasecmp(pPostExec, "Null"))
    {
        ResetString(mExecScriptOnFadeCompletion, NULL);
    }
    else
    {
        ResetString(mExecScriptOnFadeCompletion, pPostExec);
    }
    mIsFadingIn = true;
    mFadeTimer = 0;
}
void TextLevel::StartFadeOut(const char *pPostExec)
{
    if(!pPostExec || !strcasecmp(pPostExec, "Null"))
    {
        ResetString(mExecScriptOnFadeCompletion, NULL);
    }
    else
    {
        ResetString(mExecScriptOnFadeCompletion, pPostExec);
    }
    mIsFadingOut = true;
    mFadeTimer = 0;
}
void TextLevel::SetPlayerWorldTile(int pX, int pY, int pZ)
{
    mPlayerWorldX = pX;
    mPlayerWorldY = pY;
    mPlayerWorldZ = pZ;
}
void TextLevel::ActivateDeckEditor()
{
    mDeckEditor->Activate();
}
void TextLevel::ActivateOptionsMenu()
{
    mOptionsScreen->Activate();
    mOptionsScreen->RefreshValues(this);
}
void TextLevel::SetDeckEditorPostExec(const char *pPath)
{
    mDeckEditor->SetPostExec(pPath);
}
void TextLevel::SetDeckEditorHelpExec(const char *pPath)
{
    mDeckEditor->SetHelpExec(pPath);
}
void TextLevel::SetDeckEditorHandCount(int pType, int pCurrent, int pMinimum, int pMaximum)
{
    mDeckEditor->SetHandCounters(pType, pCurrent, pMinimum, pMaximum);
}
void TextLevel::SetDeckEditorSizeCounts(int pMinimum, int pMaximum)
{
    mDeckEditor->SetDeckMinMaxSize(pMinimum, pMaximum);
}
void TextLevel::SetDeckEditorBonus(int pType, int pValue)
{
    mDeckEditor->SetBonus(pType, pValue);
}
void TextLevel::SetDeckEditorSentenceBonus(const char *pValue)
{
    mDeckEditor->SetSentenceLenBonus(pValue);
}
void TextLevel::SetDeckEditorHelpHeader(const char *pString)
{
    mDeckEditor->SetHelpHeader(pString);
}
void TextLevel::SetDeckEditorHelpLine(int pIndex, const char *pString)
{
    mDeckEditor->SetHelpString(pIndex, pString);
}
void TextLevel::ProvideSuspendedLevel(RootLevel *pLevel)
{
    delete mSuspendedLevel;
    mSuspendedLevel = pLevel;
}
void TextLevel::SetZoomIndex(int pIndex)
{
    if(pIndex < 0 || pIndex >= TL_SCALES_TOTAL) return;
    mScaleIndex = pIndex;
    mBackgroundScale = mScaleListing[mScaleIndex];
}
void TextLevel::SetNavFlags(uint16_t pFlags)
{
    mNavButtonFlags = pFlags;
}
void TextLevel::RegisterAmbientMusicTrack(const char *pAudioManagerName, const char *pInternalName, float pStartingVolume)
{
    //--Registers a new ambient music track and begins playing it immediately. The set volume can be zero.
    if(!pAudioManagerName || !pInternalName || AudioManager::xBlockMusic) return;

    //--Make sure there isn't a music track with that name already.
    if(mMusicTracksList->GetElementByName(pInternalName)) return;

    //--Attempt to find the audio package. If it doesn't exist, don't bother registering a pack for it.
    AudioPackage *rAudioPack = AudioManager::Fetch()->GetMusicPack(pAudioManagerName);
    if(!rAudioPack) return;

    //--Create pack.
    SetMemoryData(__FILE__, __LINE__);
    TextLevMusicPack *nPack = (TextLevMusicPack *)starmemoryalloc(sizeof(TextLevMusicPack));
    nPack->Initialize();
    nPack->mCurrentVolume = pStartingVolume;
    nPack->mDesiredVolume = pStartingVolume;
    nPack->rSourcePackage = rAudioPack;
    mMusicTracksList->AddElement(pInternalName, nPack, &FreeThis);

    //--Play the song if it's not playing already.
    rAudioPack->Play();
    rAudioPack->SetVolume(pStartingVolume);
}
void TextLevel::ModifyAmbientMusicTrack(const char *pInternalName, float pTargetVolume)
{
    //--Modifies an ambient music pack. This just sets the desired volume, and the pack will tick towards it
    //  during the update cycle.
    TextLevMusicPack *rMusicPack = (TextLevMusicPack *)mMusicTracksList->GetElementByName(pInternalName);
    if(!rMusicPack) return;

    //--Set it, and forget it!
    rMusicPack->mDesiredVolume = pTargetVolume;
}
void TextLevel::SetStringHandler(const char *pPath)
{
    ResetString(mScriptExecPath, pPath);
}
void TextLevel::SetDefaultImage(const char *pName, const char *pPath, int pLayer)
{
    //--Error check.
    if(!pName || !pPath) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    TextImagePack *nPack = (TextImagePack *)starmemoryalloc(sizeof(TextImagePack));
    nPack->Initialize();
    strcpy(nPack->mLocalName, pName);
    if(!strcasecmp(pPath, "Null"))
    {
        nPack->rImage = NULL;
    }
    else
    {
        nPack->rImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
    }
    nPack->mLayer = pLayer;

    //--Now store this in an instruction pack so we can add it to the queue.
    SetMemoryData(__FILE__, __LINE__);
    TextInstructionPack *nInstructionPack = (TextInstructionPack *)starmemoryalloc(sizeof(TextInstructionPack));
    nInstructionPack->Initialize();
    nInstructionPack->mTypeCode = TEXT_INSTRUCTION_DEFAULTIMAGE;
    nInstructionPack->mDataBlock = nPack;
    RegisterInstruction(nInstructionPack);
}
void TextLevel::RegisterImage(const char *pName, const char *pDLPath, int pLayer)
{
    //--Error check.
    if(!pName || !pDLPath) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    TextImagePack *nPack = (TextImagePack *)starmemoryalloc(sizeof(TextImagePack));
    nPack->Initialize();
    strcpy(nPack->mLocalName, pName);
    if(!strcasecmp(pDLPath, "Null"))
    {
        nPack->rImage = NULL;
    }
    else
    {
        nPack->rImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    }
    nPack->mLayer = pLayer;

    //--Now store this in an instruction pack so we can add it to the queue.
    SetMemoryData(__FILE__, __LINE__);
    TextInstructionPack *nInstructionPack = (TextInstructionPack *)starmemoryalloc(sizeof(TextInstructionPack));
    nInstructionPack->Initialize();
    nInstructionPack->mTypeCode = TEXT_INSTRUCTION_REGISTERIMAGE;
    nInstructionPack->mDataBlock = nPack;
    RegisterInstruction(nInstructionPack);

    //--If this is the zeroth layer, store the path.
    if(pLayer == 0)
    {
        ResetString(mPlayerStoredImgPath, pDLPath);
    }
}
void TextLevel::UnregisterImage()
{
    //--Removes whatever override image is present, if any. This is done in a queue format.
    SetMemoryData(__FILE__, __LINE__);
    TextInstructionPack *nInstructionPack = (TextInstructionPack *)starmemoryalloc(sizeof(TextInstructionPack));
    nInstructionPack->Initialize();
    nInstructionPack->mTypeCode = TEXT_INSTRUCTION_UNREGISTERIMAGE;
    nInstructionPack->mDataBlock = NULL;
    RegisterInstruction(nInstructionPack);
}
void TextLevel::SetMapFocus(float pX, float pY)
{
    mBGFocusX = pX;
    mBGFocusY = pY;
}
void TextLevel::SetLocalityIconMode(bool pFlag)
{
    mLocalityUsesIcons = pFlag;
}
void TextLevel::RegisterLocalityToBase(const char *pName, const char *pBuilderScript)
{
    //--Error check.
    if(!pName) return;

    //--Create, register.
    SetMemoryData(__FILE__, __LINE__);
    LocalityCategory *nCategory = (LocalityCategory *)starmemoryalloc(sizeof(LocalityCategory));
    nCategory->Initialize();
    ResetString(nCategory->mLocalName, pName);
    ResetString(nCategory->mPopupBuilder, pBuilderScript);
    mCurrentLocalityList->AddElementAsTail(pName, nCategory, &LocalityCategory::DeleteThis);
    mLocalityNeedsSort = true;
}
void TextLevel::RegisterLocalityToBaseAsList(const char *pName, const char *pImgPathUp, const char *pImgPathDn)
{
    //--Error check.
    if(!pName) return;

    //--Create, register.
    SetMemoryData(__FILE__, __LINE__);
    LocalityCategory *nCategory = (LocalityCategory *)starmemoryalloc(sizeof(LocalityCategory));
    nCategory->Initialize();
    ResetString(nCategory->mLocalName, pName);
    nCategory->mEntryList = new SugarLinkedList(true);
    mCurrentLocalityList->AddElementAsTail(pName, nCategory, &LocalityCategory::DeleteThis);
    mLocalityNeedsSort = true;

    //--If the image path is not NULL, resolve it.
    if(pImgPathUp) nCategory->rImageUp = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImgPathUp);
    if(pImgPathDn) nCategory->rImageDn = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImgPathDn);
}
void TextLevel::RegisterLocalityToSub(const char *pName, const char *pSublistName, const char *pBuilderScript)
{
    RegisterLocalityToSub(pName, pSublistName, pBuilderScript, 0);
}
void TextLevel::RegisterLocalityToSub(const char *pName, const char *pSublistName, const char *pBuilderScript, int pPriority)
{
    //--Error check.
    if(!pName || !pSublistName) return;

    //--Locate the sublist.
    LocalityCategory *rBaseList = (LocalityCategory *)mCurrentLocalityList->GetElementByName(pSublistName);
    if(!rBaseList) return;

    //--If the base list hasn't had its linked-list created yet, do that.
    if(!rBaseList->mEntryList) rBaseList->mEntryList = new SugarLinkedList(true);

    //--Create, register.
    SetMemoryData(__FILE__, __LINE__);
    LocalityCategory *nCategory = (LocalityCategory *)starmemoryalloc(sizeof(LocalityCategory));
    nCategory->Initialize();
    nCategory->mDisplayPriority = pPriority;
    ResetString(nCategory->mLocalName, pName);
    ResetString(nCategory->mPopupBuilder, pBuilderScript);
    rBaseList->mEntryList->AddElementAsTail(pName, nCategory, &LocalityCategory::DeleteThis);
    mLocalityNeedsSort = true;
}
void TextLevel::ClearLocalityList()
{
    //--Stores the locality list for later use and creates a new one.
    delete mPreviousLocalityList;
    mPreviousLocalityList = mCurrentLocalityList;
    mCurrentLocalityList = new SugarLinkedList(true);
    mLocalityNeedsSort = false;
}
void TextLevel::ResumeLocalityOpenStates()
{
    //--After locality info is built, goes over the base lists. If a base list entry shares a name with
    //  one on the previous list, copies its open status.
    if(!mPreviousLocalityList || !mCurrentLocalityList) return;

    //--Iterate across the current list.
    LocalityCategory *rBaseCategory = (LocalityCategory *)mCurrentLocalityList->PushIterator();
    while(rBaseCategory)
    {
        //--Check for a match on the previous list.
        LocalityCategory *rCheckCategory = (LocalityCategory *)mPreviousLocalityList->GetElementByName(rBaseCategory->mLocalName);
        if(rCheckCategory)
        {
            rBaseCategory->mIsOpened = rCheckCategory->mIsOpened;
        }

        //--Next.
        rBaseCategory = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
    }
}
void TextLevel::SetLocalityOpen(const char *pName, bool pIsOpen)
{
    //--Sets the given locality category to open or not.
    if(!pName || !mCurrentLocalityList) return;
    LocalityCategory *rCategory = (LocalityCategory *)mCurrentLocalityList->GetElementByName(pName);
    if(rCategory) rCategory->mIsOpened = pIsOpen;
}
void TextLevel::RegisterPopupCommand(const char *pDisplayName, const char *pExecutionString)
{
    //--Registers a new command package for the popup window.
    if(!pDisplayName || !pExecutionString) return;
    SetMemoryData(__FILE__, __LINE__);
    PopupCommand *nPackage = (PopupCommand *)starmemoryalloc(sizeof(PopupCommand));
    nPackage->Initialize();
    ResetString(nPackage->mDisplayName, pDisplayName);
    ResetString(nPackage->mExecutionString, pExecutionString);
    mPopupCommandList->AddElement("X", nPackage, &PopupCommand::DeleteThis);
}
void TextLevel::ActivateMajorDialogueMode()
{
    //--Uses the major dialogue from Adventure Mode. Mostly used for endings or special CGs.
    if(mIsMajorDialogueMode || mActivateMajorDialogueWhenQueueEmpties) return;
    mActivateMajorDialogueWhenQueueEmpties = true;
}
void TextLevel::ActivateEndingMode()
{
    //--Overload, activates major dialogue and flags the ending.
    mMajorDialogueEndsGame = true;
    ActivateMajorDialogueMode();
}
void TextLevel::RegisterAnimation(const char *pName, int pTPF, int pStartX, int pStartY, int pTotalFrames, int pFramesPerRow)
{
    //--Error check.
    if(!pName || pTPF < 1 || pTotalFrames < 1) return;
    if(!pName || pTPF < 1 || pTotalFrames < 1) return;

    //--Allocate, set, register.
    SetMemoryData(__FILE__, __LINE__);
    TilesetAnimation *nAnimation = (TilesetAnimation *)starmemoryalloc(sizeof(TilesetAnimation));
    nAnimation->Initialize();
    nAnimation->mTicksPerFrame = pTPF;
    nAnimation->mFrameStartX = pStartX;
    nAnimation->mFrameStartY = pStartY;
    nAnimation->mTotalFrames = pTotalFrames;
    nAnimation->mYChangeEveryXFrames = pFramesPerRow;
    nAnimation->mActiveFrameX = pStartX;
    nAnimation->mActiveFrameY = pStartY;
    mTilesetAnimations->AddElement(pName, nAnimation, &FreeThis);
}
void TextLevel::SetMapFadeTimer(float pTimer)
{
    mMapFadeTimer = pTimer;
}
void TextLevel::SetMapFadeDelta(float pDelta)
{
    mMapFadeDelta = pDelta;
}
void TextLevel::SetStorageLine(int pLine, const char *pText)
{
    if(pLine < 0 || pLine >= TL_EXTRA_LINES) return;
    strncpy(mStorageLines[pLine], pText, sizeof(mStorageLines[pLine]));
}

//========================================= Core Methods ==========================================
void TextLevel::ConstructDeckEditor()
{
    //--Once the needed graphics are loaded, call this to order the Deck Editor to crossload its images.
    mDeckEditor->Construct();
}
void TextLevel::RunCommand(const char *pCommand)
{
    //--Immediately runs the requested command, assuming it exists. Also handles UI clearing duties.
    //  Note that this destabilizes the popup command listing.
    if(!pCommand) return;

    //--Resolve the font in use.
    SugarFont *rUseFont = Images.Data.rFontMenuSize20;
    if(mUseLargeText) rUseFont = Images.Data.rFontMenuSize30;

    //--Before doing anything else, remove spaces from the command's beginning. Seek through the
    //  string and stop at the first non-space.
    int tBegin = 0;
    int tCommandLen = (int)strlen(pCommand);
    for(int i = 0; i < tCommandLen; i ++)
    {
        if(pCommand[i] == ' ')
        {
            tBegin ++;
        }
        else
        {
            break;
        }
    }

    //--Reconstitute the string at the first non-space.
    const char *rCommand = &pCommand[tBegin];

    //--Create a text package.
    SetMemoryData(__FILE__, __LINE__);
    TextPack *nPack = (TextPack *)starmemoryalloc(sizeof(TextPack));
    nPack->Initialize();
    nPack->mText = InitializeString("> %s", rCommand);
    nPack->mRenderTimer = 1;
    mTextLog->AddElementAsHead("X", nPack, &TextPack::DeleteThis);
    mTextScrollOffset = 0;

    //--Add a copy of the used font.
    mFontLog->AddElementAsHead("X", rUseFont);

    //--Add another copy to the input log. This only happens if something actually got typed.
    static int xDummyPtr = 100;
    if((int)strlen(rCommand) > 1)
    {
        mInputLog->AddElementAsHead(rCommand, &xDummyPtr);
        while(mInputLog->GetListSize() > TL_INPUT_MAX_LOG)
        {
            mInputLog->DeleteTail();
        }
    }

    //--Create a copy of the string.
    ResetString(mLastEnteredString, rCommand);
    int tLen = (int)strlen(mLastEnteredString);
    for(int i = 0; i < tLen; i ++)
    {
        mLastEnteredString[i] = tolower(mLastEnteredString[i]);
    }

    //--Order the string entry to clear itself now that we have our copy out of it.
    mLocalStringEntry->SetString(NULL);

    //--Clear any lingering popup window stuff.
    mPopupCommandList->ClearList();

    //--Now fire the handler script.
    if(mScriptExecPath) LuaManager::Fetch()->ExecuteLuaFile(mScriptExecPath, 1, "S", mLastEnteredString);
}
uint32_t TextLevel::GetLastID()
{
    return mIDCounter;
}
uint32_t TextLevel::GenerateID()
{
    mIDCounter ++;
    return mIDCounter;
}
void TextLevel::RegisterInstruction(TextInstructionPack *pPack)
{
    mInstructionList->AddElementAsTail("X", pPack, &TextInstructionPack::DeleteThis);
}
void TextLevel::RegisterBlocker()
{
    //--Create and register.
    SetMemoryData(__FILE__, __LINE__);
    TextInstructionPack *nPack = (TextInstructionPack *)starmemoryalloc(sizeof(TextInstructionPack));
    nPack->Initialize();
    nPack->mTypeCode = TEXT_INSTRUCTION_BLOCK;
    RegisterInstruction(nPack);
}
void TextLevel::RegisterAppend(const char *pDialogue)
{
    //--Error check.
    if(!pDialogue) return;

    //--Create a local copy.
    char *nCopy = InitializeString(pDialogue);

    //--Create and register.
    SetMemoryData(__FILE__, __LINE__);
    TextInstructionPack *nPack = (TextInstructionPack *)starmemoryalloc(sizeof(TextInstructionPack));
    nPack->Initialize();
    nPack->mTypeCode = TEXT_INSTRUCTION_APPEND;
    nPack->mDataBlock = nCopy;
    RegisterInstruction(nPack);
}
void TextLevel::RegisterExecScript(const char *pPath, int pArgs, const char **pArgList)
{
    //--Error check.
    if(!pPath) return;

    //--Create a storage pack.
    SetMemoryData(__FILE__, __LINE__);
    TextScriptExecPack *nPackage = (TextScriptExecPack *)starmemoryalloc(sizeof(TextScriptExecPack));
    nPackage->Initialize();

    //--The path is simple to set.
    nPackage->SetPath(pPath);

    //--Assemble arguments. There may be zero!
    if(pArgs > 0)
    {
        for(int i = 0; i < pArgs; i ++)
        {
            nPackage->AddArgument(pArgList[i]);
        }
    }

    //--Create and register.
    SetMemoryData(__FILE__, __LINE__);
    TextInstructionPack *nPack = (TextInstructionPack *)starmemoryalloc(sizeof(TextInstructionPack));
    nPack->Initialize();
    nPack->mTypeCode = TEXT_INSTRUCTION_EXECSCRIPT;
    nPack->mDataBlock = nPackage;
    RegisterInstruction(nPack);
}
void TextLevel::RegisterPlaySFX(const char *pSFXName)
{
    //--Error check.
    if(!pSFXName) return;

    //--Create and register.
    SetMemoryData(__FILE__, __LINE__);
    TextInstructionPack *nPack = (TextInstructionPack *)starmemoryalloc(sizeof(TextInstructionPack));
    nPack->Initialize();
    nPack->mTypeCode = TEXT_INSTRUCTION_SFX;
    nPack->mDataBlock = InitializeString(pSFXName);
    RegisterInstruction(nPack);
}
void TextLevel::RegisterFadeOut(const char *pPostExec)
{
    //--Create and register.
    SetMemoryData(__FILE__, __LINE__);
    TextInstructionPack *nPack = (TextInstructionPack *)starmemoryalloc(sizeof(TextInstructionPack));
    nPack->Initialize();
    nPack->mTypeCode = TEXT_INSTRUCTION_FADEOUT;
    RegisterInstruction(nPack);

    //--If nothing was passed in, make it a "Null" instead.
    if(!pPostExec)
    {
        nPack->mDataBlock = InitializeString("Null");
    }
    //--Otherwise, register a copy.
    else
    {
        nPack->mDataBlock = InitializeString(pPostExec);
    }
}
void TextLevel::RegisterFadeIn(const char *pPostExec)
{
    //--Create and register.
    SetMemoryData(__FILE__, __LINE__);
    TextInstructionPack *nPack = (TextInstructionPack *)starmemoryalloc(sizeof(TextInstructionPack));
    nPack->Initialize();
    nPack->mTypeCode = TEXT_INSTRUCTION_FADEIN;
    RegisterInstruction(nPack);

    //--If nothing was passed in, make it a "Null" instead.
    if(!pPostExec)
    {
        nPack->mDataBlock = InitializeString("Null");
    }
    //--Otherwise, register a copy.
    else
    {
        nPack->mDataBlock = InitializeString(pPostExec);
    }
}
LocalityCategory *TextLevel::GetNthParent(int pSlot)
{
    //--Returns the requested LocalityCategory's parent, or NULL. If the entry is on the base list, returns
    //  that, otherwise returns the base list member that holds the requested entry.
    if(pSlot < 0) return NULL;

    //--Iterate.
    LocalityCategory *rCategory = (LocalityCategory *)mCurrentLocalityList->PushIterator();
    while(rCategory)
    {
        //--If this is the selected entry, return it.
        pSlot --;
        if(pSlot < 0)
        {
            mCurrentLocalityList->PopIterator();
            return rCategory;
        }

        //--If this category has a sublist, and that list is open, we need to iterate across its members.
        if(rCategory->mEntryList && rCategory->mIsOpened)
        {
            LocalityCategory *rSubCategory = (LocalityCategory *)rCategory->mEntryList->PushIterator();
            while(rSubCategory)
            {
                //--Selected entry.
                pSlot --;
                if(pSlot < 0)
                {
                    rCategory->mEntryList->PopIterator();
                    mCurrentLocalityList->PopIterator();
                    return rCategory;
                }

                //--Next.
                rSubCategory = (LocalityCategory *)rCategory->mEntryList->AutoIterate();
            }
        }

        //--Next.
        rCategory = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
    }

    //--Out of range. Fail.
    return NULL;
}
LocalityCategory *TextLevel::GetNthCategory(int pSlot)
{
    //--Returns the requested LocalityCategory, or NULL. Not as simple as it sounds because categories
    //  that are open increase the slot counter. This function is used to retrieve them in the
    //  order they are currently rendering.
    if(pSlot < 0) return NULL;

    //--Iterate.
    LocalityCategory *rCategory = (LocalityCategory *)mCurrentLocalityList->PushIterator();
    while(rCategory)
    {
        //--If this is the selected entry, return it.
        pSlot --;
        if(pSlot < 0)
        {
            mCurrentLocalityList->PopIterator();
            return rCategory;
        }

        //--If this category has a sublist, and that list is open, we need to iterate across its members.
        if(rCategory->mEntryList && rCategory->mIsOpened)
        {
            LocalityCategory *rSubCategory = (LocalityCategory *)rCategory->mEntryList->PushIterator();
            while(rSubCategory)
            {
                //--Selected entry.
                pSlot --;
                if(pSlot < 0)
                {
                    rCategory->mEntryList->PopIterator();
                    mCurrentLocalityList->PopIterator();
                    return rSubCategory;
                }

                //--Next.
                rSubCategory = (LocalityCategory *)rCategory->mEntryList->AutoIterate();
            }
        }

        //--Next.
        rCategory = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
    }

    //--Out of range. Fail.
    return NULL;
}
void TextLevel::ComputePopupWindowDimensions()
{
    //--Once all commands are registered to the popup window, figure out how big it is and where the
    //  window should be positioned.
    if(mPopupCommandList->GetListSize() < 1 || !Images.mIsReady) return;

    //--Constants.
    float cBorderSize = 4.0f;
    float cBorderPad = 2.0f;

    //--Vertical size is based directly on entry count.
    float cHei = (mPopupCommandList->GetListSize() * Images.Data.rFontMenuSize20->GetTextHeight()) + ((cBorderSize) * 2.0f) - 4.0f;

    //--Horizontal size requires finding the widest entry.
    float cWid = 1.0f;
    PopupCommand *rCommand = (PopupCommand *)mPopupCommandList->PushIterator();
    while(rCommand)
    {
        //--If it's wider, store it.
        float cCheckWid = Images.Data.rFontMenuSize20->GetTextWidth(rCommand->mDisplayName);
        if(cCheckWid > cWid) cWid = cCheckWid;

        //--Next.
        rCommand = (PopupCommand *)mPopupCommandList->AutoIterate();
    }

    //--Final width.
    cWid = (cWid) + ((cBorderPad + cBorderSize) * 2.0f);

    //--Now assume the center position is where the mouse is. We line the first command up with the mouse if we can.
    ControlManager *rControlManager = ControlManager::Fetch();
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);
    float cLft = tMouseX - (cWid * 0.50f);
    float cTop = tMouseY - (Images.Data.rFontMenuSize20->GetTextHeight() * 0.50f);

    //--Make sure none of the popup window goes offscreen.
    if(cLft + cWid >= CANX) cLft = CANX - cWid;
    if(cLft        <     0) cLft = 0.0f;
    if(cTop + cHei >= CANY) cTop = CANY - cHei;
    if(cTop        <     0) cTop = 0.0f;

    //--Now set the positions.
    mPopupWindowDim.SetWH(cLft, cTop, cWid, cHei);

    //--This is the mouse-out dimension. If the mouse leaves this are, the window despawns.
    float cMouseOutBuffer = 30.0f;
    mPopupSelectionDim.Set(mPopupWindowDim.mLft - cMouseOutBuffer, mPopupWindowDim.mTop - cMouseOutBuffer, mPopupWindowDim.mRgt + cMouseOutBuffer, mPopupWindowDim.mBot + cMouseOutBuffer);
}
int TextLevel::PopulateSavePaths(const char *pDirectory)
{
    //--Given a directory, populates all savefiles found in that directory. Only cares about .tls
    //  extensions. Data is populated in order of most recent to least recent.
    int tSaveFiles = 0;
    for(int i = 0; i < TL_MOST_RECENT_SAVES; i ++)
    {
        strcpy(mSavefileNames[i], "Null");
    }
    if(!pDirectory) return 0;

    //--Create a file system to handle this.
    //fprintf(stderr, "Filesystem scans directory %s\n", pDirectory);
    SugarFileSystem *tFileSystem = new SugarFileSystem();
    tFileSystem->SetRecursiveFlag(false);
    tFileSystem->ScanDirectory(pDirectory);
    tFileSystem->SortDirectoryByLastAccess();

    //--Scan across the entries.
    int tTotalEntries = tFileSystem->GetTotalEntries();
    //fprintf(stderr, "Found %i files.\n", tTotalEntries);
    for(int i = 0; i < tTotalEntries; i ++)
    {
        //--Get the file, check if it's a .tls file.
        FileInfo *rFileInfo = tFileSystem->GetEntry(i);
        int tNameLen = (int)strlen(rFileInfo->mPath);
        if(strcasecmp(&rFileInfo->mPath[tNameLen-4], ".tls"))
        {
            //fprintf(stderr, " File %s is not a savefile.\n", rFileInfo->mPath);
            continue;
        }

        //--Figure out where the last '/' is in the file name. Trim the file to make it easier to read.
        int tStartAt = 0;
        for(int p = tNameLen-1; p >= 0; p --)
        {
            if(rFileInfo->mPath[p] == '/' || rFileInfo->mPath[p] == '\\')
            {
                tStartAt = p+1;
                break;
            }
        }

        //--Savefile. Store it.
        strncpy(mSavefileNames[tSaveFiles], &rFileInfo->mPath[tStartAt], sizeof(mSavefileNames[tSaveFiles]));
        tSaveFiles ++;
        //fprintf(stderr, " Stored %i %s\n", tSaveFiles-1, mSavefileNames[tSaveFiles-1]);

        //--Strip off the tls.
        tNameLen = (int)strlen(mSavefileNames[tSaveFiles-1]);
        for(int p = 0; p < 4; p ++)
        {
            mSavefileNames[tSaveFiles-1][tNameLen-p-1] = '\0';
        }

        //--If we reach the path cap, stop here.
        if(tSaveFiles >= TL_MOST_RECENT_SAVES) break;
    }

    //--Clean up.
    delete tFileSystem;

    //--Pass back how many files we found.
    return tSaveFiles;
}
void TextLevel::SortLocalityWindow()
{
    //--Run the sort on each list.
    LocalityCategory *rBaseList = (LocalityCategory *)mCurrentLocalityList->PushIterator();
    while(rBaseList)
    {
        //--No linked-list. Ignore it.
        if(!rBaseList->mEntryList)
        {
            rBaseList = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
            continue;
        }

        //--Iterate across the sublists and sort them.
        LocalityCategory *rSublist = (LocalityCategory *)mCurrentLocalityList->PushIterator();
        while(rSublist)
        {
            //--If there's a sublist, sort it.
            if(rSublist->mEntryList)
            {
                //--Sort.
                rSublist->mEntryList->SortListUsing(TextLevel::SortLocality);

                //--Debug.
                /*
                fprintf(stderr, "Sorted.\n");
                LocalityCategory *rEntry = (LocalityCategory *)rSublist->mEntryList->PushIterator();
                while(rEntry)
                {
                    fprintf(stderr, " %s %i\n", rEntry->mLocalName, rEntry->mDisplayPriority);
                    rEntry = (LocalityCategory *)rSublist->mEntryList->AutoIterate();
                }
                */

            }
            rSublist = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
        }

        //--Next.
        rBaseList = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
    }
}
int TextLevel::SortLocality(const void *pEntryA, const void *pEntryB)
{
    //--Comparison function used to sort locality entries.
    SugarLinkedListEntry **rEntryA = (SugarLinkedListEntry **)pEntryA;
    SugarLinkedListEntry **rEntryB = (SugarLinkedListEntry **)pEntryB;

    //--The entries have a LocalityCategory attached.
    LocalityCategory *rCategoryA = (LocalityCategory *)(*rEntryA)->rData;
    LocalityCategory *rCategoryB = (LocalityCategory *)(*rEntryB)->rData;

    //--If they have different priorities:
    if(rCategoryA->mDisplayPriority != rCategoryB->mDisplayPriority)
    {
        return (rCategoryB->mDisplayPriority - rCategoryA->mDisplayPriority);
    }
    //--Otherwise, return the name comparison.
    else
    {
        return strcmp(rCategoryA->mLocalName, rCategoryB->mLocalName);
    }
}
void TextLevel::ComputeIndicatorPositions()
{
    //--[Documentation and Setup]
    //--Each time entities move, we need to figure out where each entity indicator on the map should
    //  be located. This is not just which tile they are on, but which part of the tile. When many
    //  entities are on the same tile, they need to stack in an ordered way.
    float cFriendlyX = -22.0f;
    float cFriendlyY = -22.0f;
    float cHostileX = 22.0f;
    float cHostileY = -22.0f;
    float cItemX = -22.0f;
    float cItemY =  22.0f;

    //--Render steps. Entities stack vertically, items horizontally.
    float cRenderStepX = 14.0f;
    float cRenderStepY = 14.0f;

    //--[Iterate]
    //--Run across the list of all indicators in all tiles. It can safely be assumed every turn that
    //  entities move.
    AutomapPack *rPackage = (AutomapPack *)mAutomapList->PushIterator();
    while(rPackage)
    {
        //--Compute this tile's position.
        float cXPos = (rPackage->mX * mSizePerRoomX);
        float cYPos = (rPackage->mY * mSizePerRoomY);

        //--Counts.
        int tFriendlyCount = 0;
        int tHostileCount = 0;
        int tItemCount = 0;

        //--Iterate across all the indicators on the tile. For most tiles, this list is empty.
        AutomapPackTilemapLayer *rLayer = (AutomapPackTilemapLayer *)rPackage->mEntityIndicators->PushIterator();
        while(rLayer)
        {
            //--Positions relative to the center of the tile.
            float cOffX = 0.0f;
            float cOffY = 0.0f;
            float mTargetAlpha = 1.0f;
            float cZPos = cDepthPlayer;

            //--Alpha computations. If the room the indicator is in is not visible, we fade out.
            //  Otherwise, the entity must fade in if not already visible.
            if(rPackage->mIsVisibleNow)
            {
                mTargetAlpha = 1.0f;
            }
            //--Room is not visible. Target alpha is 0.
            else
            {
                mTargetAlpha = 0.0f;
            }

            //--If there is exactly one entity on this tile, non-items get centered. Items
            //  still appear stacked in the corner.
            if(rPackage->mEntityIndicators->GetListSize() == 1)
            {
                //--If it's an item, corner it.
                if(rLayer->mPriority == AMTL_PRIORITY_ITEM)
                {
                    cOffX = cItemX;
                    cOffY = cItemY;
                    cZPos = cDepthHostile;
                }
            }
            //--Friendlies. Stack on the left side.
            else if(rLayer->mPriority == AMTL_PRIORITY_FRIENDLY)
            {
                cOffX = cFriendlyX;
                cOffY = cFriendlyY + (cRenderStepY * tFriendlyCount);
                cZPos = cDepthFriendly;
                tFriendlyCount ++;
            }
            //--Enemies. Right side.
            else if(rLayer->mPriority == AMTL_PRIORITY_ENEMY)
            {
                cOffX = cHostileX;
                cOffY = cHostileY + (cRenderStepY * tHostileCount);
                cZPos = cDepthHostile;
                tHostileCount ++;
            }
            //--Sound indicators. Centered, don't scroll with camera. Always 100% opacity. Uses Player's depth.
            else if(rLayer->mPriority == AMTL_PRIORITY_SOUND)
            {
                mTargetAlpha = 1.0f;
            }
            //--Item. Bottom-left to Bottom-right.
            else if(rLayer->mPriority == AMTL_PRIORITY_ITEM)
            {
                cOffX = cItemX + (cRenderStepX * tItemCount);
                cOffY = cItemY;
                cZPos = cDepthHostile;
                tItemCount ++;
            }
            //--Player's indicator. Always visible.
            else
            {
                mTargetAlpha = 1.0f;
            }

            //--Once the position is resolved, order the indicator to move to it. The depth
            //  does not slide, it is instant.
            rLayer->SetPositionTarget(cXPos + cOffX, cYPos + cOffY, mTargetAlpha);
            rLayer->mZRender = cZPos;

            //--Sound indicators immediately arrive at their destination. This is because they despawn and
            //  respawn every movement.
            if(rLayer->mPriority == AMTL_PRIORITY_SOUND) rLayer->mMoveTimer = rLayer->mMoveTimerMax - 1;

            //--If this flag is set, instantly move to the target.
            if(rLayer->mInstantMoveToDest)
            {
                rLayer->mInstantMoveToDest = false;
                rLayer->mMoveTimer = rLayer->mMoveTimerMax - 1;
            }

            //--Next.
            rLayer = (AutomapPackTilemapLayer *)rPackage->mEntityIndicators->AutoIterate();
        }

        //--Next.
        rPackage = (AutomapPack *)mAutomapList->AutoIterate();
    }
}
void TextLevel::ClampLocalityScroll()
{
    //--Subroutine that clamps the locality scroll bar. First, compute how many lines can be visible max.
    int tUseMax = TL_LOCALITY_LINES_MAX;
    if(mLocalityUsesIcons) tUseMax -= 3;

    //--See how many are actually visible.
    int tCheckEntires = ComputeVisibleLocalityEntries();
    if(tCheckEntires <= tUseMax)
    {
        mLocalityScroll = 0.0f;
    }

    //--Compuite the sizes.
    float cFontSize = Images.Data.rFontMenuSize20->GetTextHeight();
    float tLocalityMax = (cFontSize * ((ComputeVisibleLocalityEntries() - tUseMax) + 0.70f));

    //--Clamp.
    if(mLocalityScroll > tLocalityMax)
    {
        mLocalityScroll = tLocalityMax;
    }
    if(mLocalityScroll < 0.0f) mLocalityScroll = 0.0f;
}
void TextLevel::HandleWorldMouse()
{
    //--[Documentation and Setup]
    //--When the player's mouse is over the game map, they can click to move/open doors/look. This handles
    //  figuring out where the player's mouse is.
    //--Does nothing if the nav buttons are disabled, which is the case for "no mouse input" game modes.
    if(mNoNavigationButtons) return;

    //--Setup.
    strcpy(mPlayerWorldClickHandler, "Null");
    int tMouseX, tMouseY, tMouseZ;
    ControlManager *rControlManager = ControlManager::Fetch();
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--[Range Check]
    //--Reset to neutral.
    if(!IsPointWithin2DReal(tMouseX, tMouseY, mMapAreaDim)) return;

    //--Map Center.
    float cMapCenterX = mMapAreaDim.mXCenter + (mBGFocusX * mSizePerRoomX * mBackgroundScale) - mPlayerTranslationX;
    float cMapCenterY = mMapAreaDim.mYCenter + (mBGFocusY * mSizePerRoomY * mBackgroundScale) - mPlayerTranslationY;

    //--Tile Position.
    float tFloatTileX = (((tMouseX - mMapAreaDim.mLft - cMapCenterX) / (mSizePerRoomX * mBackgroundScale)) + 0.50f);
    float tFloatTileY = (((tMouseY - mMapAreaDim.mTop - cMapCenterY) / (mSizePerRoomY * mBackgroundScale)) + 0.50f);
    int tTileX = (int)tFloatTileX;
    int tTileY = (int)tFloatTileY;

    //--Check if we're over any automap packages.
    AutomapPack *rPackage = (AutomapPack *)mAutomapList->PushIterator();
    while(rPackage)
    {
        //--If this matches the mouse position:
        if(rPackage->mZ == mCurrentRenderingZ && (int)rPackage->mX == tTileX && (int)rPackage->mY == tTileY)
        {
            //--Fraction.
            float tXFraction = tFloatTileX - (float)tTileX;
            float tYFraction = tFloatTileY - (float)tTileY;

            //--If the player is standing here:
            if(rPackage->mX == mPlayerWorldX && rPackage->mY == mPlayerWorldY)
            {
                //--Set.
                rPackage->mIsHighlighted = true;
                rPackage->mDoorHighlight = AMP_NONE;
                strcpy(mPlayerWorldClickHandler, "look");

                //--Edge checking.
                if(IsPointWithin2DReal(tXFraction, tYFraction, cDoorDetectNorth) && rPackage->mDoorHighlightPossible & AMP_NORTH)
                {
                    rPackage->mDoorHighlight = AMP_NORTH;
                    strcpy(mPlayerWorldClickHandler, "openclose n");
                }
                else if(IsPointWithin2DReal(tXFraction, tYFraction, cDoorDetectEast) && rPackage->mDoorHighlightPossible & AMP_EAST)
                {
                    rPackage->mDoorHighlight = AMP_EAST;
                    strcpy(mPlayerWorldClickHandler, "openclose e");
                }
                else if(IsPointWithin2DReal(tXFraction, tYFraction, cDoorDetectSouth) && rPackage->mDoorHighlightPossible & AMP_SOUTH)
                {
                    rPackage->mDoorHighlight = AMP_SOUTH;
                    strcpy(mPlayerWorldClickHandler, "openclose s");
                }
                else if(IsPointWithin2DReal(tXFraction, tYFraction, cDoorDetectWest) && rPackage->mDoorHighlightPossible & AMP_WEST)
                {
                    rPackage->mDoorHighlight = AMP_WEST;
                    strcpy(mPlayerWorldClickHandler, "openclose w");
                }
            }
            //--Move west:
            else if(rPackage->mX == mPlayerWorldX-1 && rPackage->mY == mPlayerWorldY)
            {
                //--Base.
                rPackage->mIsHighlighted = true;
                rPackage->mDoorHighlight = AMP_NONE;
                strcpy(mPlayerWorldClickHandler, "w");

                //--Fraction.
                float tXFraction = tFloatTileX - (float)tTileX;
                float tYFraction = tFloatTileY - (float)tTileY;

                //--East door check.
                if(IsPointWithin2DReal(tXFraction, tYFraction, cDoorDetectEast) && rPackage->mDoorHighlightPossible & AMP_EAST)
                {
                    rPackage->mDoorHighlight = AMP_EAST;
                    strcpy(mPlayerWorldClickHandler, "openclose w");
                }
            }
            //--Move east:
            else if(rPackage->mX == mPlayerWorldX+1 && rPackage->mY == mPlayerWorldY)
            {
                //--Base.
                rPackage->mIsHighlighted = true;
                rPackage->mDoorHighlight = AMP_NONE;
                strcpy(mPlayerWorldClickHandler, "e");

                //--West door check.
                if(IsPointWithin2DReal(tXFraction, tYFraction, cDoorDetectWest) && rPackage->mDoorHighlightPossible & AMP_WEST)
                {
                    rPackage->mDoorHighlight = AMP_WEST;
                    strcpy(mPlayerWorldClickHandler, "openclose e");
                }
            }
            //--Move north:
            else if(rPackage->mX == mPlayerWorldX && rPackage->mY == mPlayerWorldY-1)
            {
                //--Base.
                rPackage->mIsHighlighted = true;
                rPackage->mDoorHighlight = AMP_NONE;
                strcpy(mPlayerWorldClickHandler, "n");

                //--South door check.
                if(IsPointWithin2DReal(tXFraction, tYFraction, cDoorDetectSouth) && rPackage->mDoorHighlightPossible & AMP_SOUTH)
                {
                    rPackage->mDoorHighlight = AMP_SOUTH;
                    strcpy(mPlayerWorldClickHandler, "openclose n");
                }
            }
            //--Move south:
            else if(rPackage->mX == mPlayerWorldX && rPackage->mY == mPlayerWorldY+1)
            {
                //--Base.
                rPackage->mIsHighlighted = true;
                rPackage->mDoorHighlight = AMP_NONE;
                strcpy(mPlayerWorldClickHandler, "s");

                //--North door check.
                if(IsPointWithin2DReal(tXFraction, tYFraction, cDoorDetectNorth) && rPackage->mDoorHighlightPossible & AMP_NORTH)
                {
                    rPackage->mDoorHighlight = AMP_NORTH;
                    strcpy(mPlayerWorldClickHandler, "openclose s");
                }
            }
        }
        else
        {
            rPackage->mIsHighlighted = false;
            rPackage->mDoorHighlight = AMP_NONE;
        }

        //--[Next]
        rPackage = (AutomapPack *)mAutomapList->AutoIterate();
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void TextLevel::Update()
{
    //--[Documentation and Setup]
    //--Handles the update cycle for the text level, including player controls and animation timers.
    //  Has a bunch of sub objects that can all handle controls. If any of them handle the controls,
    //  then tControlsHandled gets set to true.
    bool tControlsHandled = false;

    //--[Timers]
    //--Crossfade. Always runs.
    if(mImgTransitionTimer > 0) mImgTransitionTimer --;

    //--Fade-in/out.
    if(mIsFadingIn || mIsFadingOut)
    {
        //--Timer.
        mFadeTimer ++;

        //--On completion:
        if(mFadeTimer >= TL_FADE_TICKS)
        {
            //--Reset flags.
            mIsFadingOut = false;
            mIsFadingIn = false;

            //--If flagged, run a script on completion. If NULL do nothing.
            if(mExecScriptOnFadeCompletion)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mExecScriptOnFadeCompletion);
                ResetString(mExecScriptOnFadeCompletion, NULL);
            }
        }
    }

    //--Map fade timers. Always runs.
    if(mMapFadeDelta != 0)
    {
        //--Increment.
        mMapFadeTimer = mMapFadeTimer + mMapFadeDelta;

        //--Clamps. Passing a clamp zeroes the delta.
        if(mMapFadeTimer <= 0.0f)
        {
            if(mMapFadeDelta < 0.0) mMapFadeDelta = 0.0f;
            mMapFadeTimer = 0.0f;
        }
        else if(mMapFadeTimer >= 1.0f)
        {
            if(mMapFadeDelta > 0.0) mMapFadeDelta = 0.0f;
            mMapFadeTimer = 1.0f;
        }
    }

    //--Harsh overlays. Always updates, just runs internal timers.
    UpdateOverlays();

    //--Player's movement timer.
    if(mPlayerMoveTimer < mPlayerMoveTimerMax) mPlayerMoveTimer ++;

    //--Music fading.
    mPlayTextTick = false;
    float cDelta = 0.005f;
    TextLevMusicPack *rMusicPack = (TextLevMusicPack *)mMusicTracksList->PushIterator();
    while(rMusicPack)
    {
        //--Identical, do nothing.
        if(rMusicPack->mCurrentVolume == rMusicPack->mDesiredVolume)
        {
        }
        //--Louder than desired, decrement.
        else if(rMusicPack->mCurrentVolume > rMusicPack->mDesiredVolume + cDelta)
        {
            rMusicPack->mCurrentVolume = rMusicPack->mCurrentVolume - cDelta;
            rMusicPack->rSourcePackage->SetVolume(rMusicPack->mCurrentVolume);
        }
        //--Quieter than desired, increment.
        else if(rMusicPack->mCurrentVolume < rMusicPack->mDesiredVolume - cDelta)
        {
            rMusicPack->mCurrentVolume = rMusicPack->mCurrentVolume + cDelta;
            rMusicPack->rSourcePackage->SetVolume(rMusicPack->mCurrentVolume);
        }
        //--Close enough, lock to target.
        else
        {
            rMusicPack->mCurrentVolume = rMusicPack->mDesiredVolume;
            rMusicPack->rSourcePackage->SetVolume(rMusicPack->mCurrentVolume);
        }

        //--Next.
        rMusicPack = (TextLevMusicPack *)mMusicTracksList->AutoIterate();
    }

    //--Locality sorting.
    if(mLocalityNeedsSort)
    {
        mLocalityNeedsSort = false;
        SortLocalityWindow();
    }

    //--If any indicators changed position or disappeared, we need to rebuild their positions here.
    if(mRebuildIndicators)
    {
        mRebuildIndicators = false;
        ComputeIndicatorPositions();
    }

    //--Entity indicator timers, fading timers.
    AutomapPack *rCheckPack = (AutomapPack *)mAutomapList->PushIterator();
    while(rCheckPack)
    {
        //--Run.
        if(rCheckPack->mDiscoveryTimer < AM_FADE_TICKS) rCheckPack->mDiscoveryTimer ++;
        if(rCheckPack->mFadeTimer      < AM_FADE_TICKS) rCheckPack->mFadeTimer ++;
        if(rCheckPack->mInsetTimer     < AM_FADE_TICKS) rCheckPack->mInsetTimer ++;
        if(rCheckPack->mPrevFadeTimer  > 0)             rCheckPack->mPrevFadeTimer --;
        if(rCheckPack->mPrevInsetTimer > 0)             rCheckPack->mPrevInsetTimer --;

        //--Run across the layers. Update any timers.
        AutomapPackTilemapLayer *rLayerPack = (AutomapPackTilemapLayer *)rCheckPack->mEntityIndicators->PushIterator();
        while(rLayerPack)
        {
            //--Timer.
            if(rLayerPack->mMoveTimer < rLayerPack->mMoveTimerMax)
            {
                //--Increment.
                rLayerPack->mMoveTimer ++;

                //--Compute position.
                float tPercent = EasingFunction::QuadraticOut(rLayerPack->mMoveTimer, rLayerPack->mMoveTimerMax);
                rLayerPack->mXRender = rLayerPack->mXStart + ((rLayerPack->mXTarget - rLayerPack->mXStart) * tPercent);
                rLayerPack->mYRender = rLayerPack->mYStart + ((rLayerPack->mYTarget - rLayerPack->mYStart) * tPercent);
                rLayerPack->mARender = rLayerPack->mAStart + ((rLayerPack->mATarget - rLayerPack->mAStart) * tPercent);
            }

            //--Next.
            rLayerPack = (AutomapPackTilemapLayer *)rCheckPack->mEntityIndicators->AutoIterate();
        }

        //--Next.
        rCheckPack = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--Animations.
    TilesetAnimation *rAnimation = (TilesetAnimation *)mTilesetAnimations->PushIterator();
    while(rAnimation)
    {
        //--Timer.
        rAnimation->mTimer ++;
        if(rAnimation->mTimer >= rAnimation->mTicksPerFrame)
        {
            //--Reset timer.
            rAnimation->mTimer = 0;

            //--Increment.
            rAnimation->mTotalFramesRun ++;
            rAnimation->mActiveFrameX ++;
            if(rAnimation->mActiveFrameX >= rAnimation->mYChangeEveryXFrames)
            {
                //--Move one row down.
                rAnimation->mActiveFrameX = rAnimation->mFrameStartX;
                rAnimation->mActiveFrameY ++;

                //--Overrun. Reset to start.
                if(rAnimation->mTotalFramesRun >= rAnimation->mTotalFrames)
                {
                    rAnimation->mTotalFramesRun = 0;
                    rAnimation->mActiveFrameX = rAnimation->mFrameStartX;
                    rAnimation->mActiveFrameY = rAnimation->mFrameStartY;
                }
            }
        }

        //--Next.
        rAnimation = (TilesetAnimation *)mTilesetAnimations->AutoIterate();
    }

    //--Handle the combat update. This doesn't stop the update here, it can continue to run. However,
    //  inputs will be ignored.
    if(mIsCombatMode != TL_COMBAT_NONE)
    {
        mPostCombatTimer = 0;
        UpdateCombat();
    }
    //--Otherwise, run the music timer.
    else
    {
        //--Tick.
        mPostCombatTimer ++;

        //--First tick. Start fading combat music.
        if(mPostCombatTimer == 1)
        {
        }

        //--Hit the threshold. Reset music to full volume.
        if(mPostCombatTimer == TL_MUSIC_RESTART_POST_COMBAT)
        {
            TextLevMusicPack *rPack = (TextLevMusicPack *)mMusicTracksList->PushIterator();
            while(rPack)
            {
                rPack->mDesiredVolume = rPack->mStoredVolume;
                rPack = (TextLevMusicPack *)mMusicTracksList->AutoIterate();
            }
        }
    }

    //--In ending mode, ignore all inputs and send them to the world dialogue. This is exclusive with
    //  combat, which must resolve before the ending does.
    if(mIsMajorDialogueMode && mIsCombatMode == TL_COMBAT_NONE)
    {
        //--Fast-access pointers.
        CutsceneManager *rCutsceneManager = CutsceneManager::Fetch();
        WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();

        //--If the dialogue is still visible, the ending is playing out.
        if(rWorldDialogue->IsVisible() || rCutsceneManager->HasAnyEvents())
        {
            //--Timer.
            mMajorDialogueFadeOut = false;
            if(mMajorDialogueTimer < TL_ENDING_TICKS) mMajorDialogueTimer ++;

            //--Run the dialogue.
            rWorldDialogue->SetAnyKeyMode(true);
            rWorldDialogue->Update();

            //--Cutscenes can also update here.
            if(rCutsceneManager->HasAnyEvents())
            {
                //--Run the update.
                rCutsceneManager->Update();
            }

            //--Debug.
            return;
        }
        //--Ending fades out.
        else
        {
            //--Decrement timer.
            mMajorDialogueFadeOut = true;
            if(mMajorDialogueTimer > 0) mMajorDialogueTimer --;

            //--Ending case.
            if(mMajorDialogueTimer < 1)
            {
                rWorldDialogue->SetAnyKeyMode(false);

                //--If flagged, head back to the title screen.
                if(mMajorDialogueEndsGame)
                {
                    MapManager::Fetch()->mBackToTitle = true;
                }
                //--Otherwise, unfade.
                else
                {
                    mIsMajorDialogueMode = false;
                    mMajorDialogueFadeOut = false;
                    mIsMajorDialogueModeExit = true;
                    mMajorDialogueTimer = TL_ENDING_TICKS;
                }
            }
        }
    }
    else
    {
        if(mMajorDialogueTimer > 0) mMajorDialogueTimer --;
        if(mMajorDialogueTimer == 0) mIsMajorDialogueModeExit = false;
    }

    //--All text log packages update their timers here.
    if(mAppendTimer < TL_TIME_PER_LINE * -1) mAppendTimer ++;
    TextPack *rPack = (TextPack *)mTextLog->PushIterator();
    while(rPack)
    {
        rPack->mRenderTimer ++;
        rPack = (TextPack *)mTextLog->AutoIterate();
    }

    //--Variables for locality window.
    int tUseMax = TL_LOCALITY_LINES_MAX;
    if(mLocalityUsesIcons) tUseMax -= 3;

    //--Clamp the locality scroll.
    ClampLocalityScroll();

    //--Combat stops the update here. Events will not run since combat stops events anyway.
    //  This means combat can theoretically be put in the middle of an event queue.
    if(mIsCombatMode != TL_COMBAT_NONE) return;

    //--[Sub-Objects]
    //--Deck Editor.
    mDeckEditor->Update();
    if(mDeckEditor->IsActive()) tControlsHandled = true;

    //--Options Menu.
    mOptionsScreen->Update(!tControlsHandled);
    if(mOptionsScreen->IsActive()) tControlsHandled = true;

    //--Player map.
    tControlsHandled = UpdatePlayerMap(tControlsHandled);

    //--If any of the sub-objects handled the controls, stop here.
    if(tControlsHandled) return;

    //--[Input Handling]
    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get mouse positions.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Location of the text window.
    TwoDimensionReal cTextFrame;
    cTextFrame.SetWH(2.0f, 404.0f, 968.0f, 362.0f);

    //--[Mousewheel]
    //--If the mouse is over the locality window, the mouse wheel affects the scrollbar there.
    if(IsPointWithin(tMouseX, tMouseY, 980.0f, 485.0f, 1360.0f, 760.0f))
    {
        //--Get the font size of each entry. This is one "scroll unit".
        float cFontSize = Images.Data.rFontMenuSize20->GetTextHeight();

        //--No change.
        if(tMouseZ == -700)
        {
            mOldMouseZ = tMouseZ;
        }
        //--Increment.
        else if(mOldMouseZ > tMouseZ)
        {
            mOldMouseZ = tMouseZ;
            mLocalityScroll = mLocalityScroll + cFontSize;
            ClampLocalityScroll();
        }
        //--Decrement.
        else if(mOldMouseZ < tMouseZ)
        {
            mOldMouseZ = tMouseZ;
            mLocalityScroll = mLocalityScroll - cFontSize;
            ClampLocalityScroll();
        }
    }
    //--Mouse is over the text display. Wheel scrolls up and down.
    else if(IsPointWithin2DReal(tMouseX, tMouseY, cTextFrame))
    {
        //--No change.
        if(tMouseZ == -700)
        {
            mOldMouseZ = tMouseZ;
        }
        //--Modify.
        else
        {
            mTextScrollOffset += (tMouseZ - mOldMouseZ);
            if(mTextScrollOffset < 0) mTextScrollOffset = 0;
            if(mTextScrollOffset > TL_MAX_TEXTLOG_LINES - 10) mTextScrollOffset = TL_MAX_TEXTLOG_LINES - 10;
            mOldMouseZ = tMouseZ;
        }
    }
    //--Normal, mouse wheel zooms the camera in and out.
    else
    {
        //--No change.
        if(tMouseZ == -700)
        {
            mOldMouseZ = tMouseZ;
        }
        //--Zoom in.
        else if(mOldMouseZ > tMouseZ)
        {
            mScaleIndex ++;
            if(mScaleIndex >= TL_SCALES_TOTAL) mScaleIndex --;
            mBackgroundScale = mScaleListing[mScaleIndex];
            mOldMouseZ = tMouseZ;
        }
        //--Zoom out.
        else if(mOldMouseZ < tMouseZ)
        {
            mScaleIndex --;
            if(mScaleIndex < 1) mScaleIndex  = 0;
            mBackgroundScale = mScaleListing[mScaleIndex];
            mOldMouseZ = tMouseZ;
        }
    }

    //--If the mouse is over the map window, compute its world coordinates.
    HandleWorldMouse();

    //--If nothing is blocking, run the instructions.
    bool tIgnoreKeypress = mIsBlockPending || HandleEvents(false);
    int tEventsRightNow = mInstructionList->GetListSize();

    //--Run the update on the local StringEntry form.
    if(!tIgnoreKeypress)
    {
        //--If the player presses up, move the input log value up by one:
        if(rControlManager->IsFirstPress("GUI|Up"))
        {
            //--Increment.
            mInputLogScroll ++;

            //--Check if there's a string available. If not, back up.
            const char *rString = mInputLog->GetNameOfElementBySlot(mInputLogScroll);
            if(!rString)
            {
                mLocalStringEntry->SetString(NULL);
                mInputLogScroll --;
            }
            //--There's a string. Set it.
            else
            {
                mLocalStringEntry->SetString(rString);
            }
        }
        else if(rControlManager->IsFirstPress("GUI|Dn"))
        {
            //--Decrement.
            mInputLogScroll --;

            //--Check if there's a string available. If not, back up.
            const char *rString = mInputLog->GetNameOfElementBySlot(mInputLogScroll);
            if(!rString)
            {
                mLocalStringEntry->SetString(NULL);
                if(mInputLogScroll < -1) mInputLogScroll ++;
            }
            //--There's a string. Set it.
            else
            {
                mLocalStringEntry->SetString(rString);
            }
        }

        //--Run.
        mLocalStringEntry->Update();

        //--If the entry completed, we need to execute the handler script.
        if(mLocalStringEntry->IsComplete())
        {
            //--Unset the flag.
            mLocalStringEntry->SetCompleteFlag(false);

            //--Extract the string.
            mInputLogScroll = -1;
            const char *rString = mLocalStringEntry->GetString();
            RunCommand(rString);
            return;
        }

        //--Flag reset.
        mPopupHighlight = -1;
        mLocalityHighlight = -1;

        //--If the popup window is open, that takes command over the mouse.
        if(mPopupCommandList->GetListSize() > 0)
        {
            //--If the mouse is not within this area, cancel popups.
            if(!IsPointWithin2DReal(tMouseX, tMouseY, mPopupSelectionDim))
            {
                mPopupCommandList->ClearList();
                return;
            }

            //--Compute highlight.
            if(tMouseX >= mPopupWindowDim.mLft && tMouseX <= mPopupWindowDim.mRgt)
            {
                mPopupHighlight = (tMouseY - mPopupWindowDim.mTop) / (Images.Data.rFontMenuSize20->GetTextHeight());
            }

            //--Left-click.
            if(rControlManager->IsFirstPress("MouseLft"))
            {
                //--Locate the associated popup. If it exists, execute it.
                PopupCommand *rPackage = (PopupCommand *)mPopupCommandList->GetElementBySlot(mPopupHighlight);
                if(rPackage)
                {
                    //--Note: This clears the mPopupCommandList, so we need to return out immediately.
                    RunCommand(rPackage->mExecutionString);
                    return;
                }
            }
            //--Mouse over.
            else
            {
            }
        }
        //--Other mouse stuff.
        else
        {
            //--Update the locality list information. Right side is subtracted by the scrollbar.
            if(tMouseX >= mLocalityWindow.mLft && tMouseX <= mLocalityWindow.mRgt - 25.0f)
            {
                //--No icons:
                if(!mLocalityUsesIcons)
                {
                    mLocalityHighlight = (tMouseY - mLocalityWindow.mTop + mLocalityScroll) / (Images.Data.rFontMenuSize20->GetTextHeight());
                }
                //--Icons:
                else
                {
                    //--Compute.
                    mLocalityHighlight = (tMouseY - mLocalityWindow.mTop + mLocalityScroll - TL_LOCALITY_SIZE - 10) / (Images.Data.rFontMenuSize20->GetTextHeight());
                    mLocalityHighlight --;

                    //--Get position.
                    float tY = tMouseY - mLocalityWindow.mTop;

                    //--Don't change locality highlight or handle clicks if over the icons.
                    if(tY <= TL_LOCALITY_SIZE + 5.0f)
                    {
                        mLocalityHighlight = -1;
                    }
                }
            }

            //--If the mouse is not down, stop dragging.
            if(!rControlManager->IsDown("MouseLft"))
            {
                mIsDraggingLocalityBar = false;
                mIsDraggingTextScrollbar = false;
            }

            //--Dragging the locality bar.
            if(mIsDraggingLocalityBar)
            {
                //--Determine how much each pixel is in terms of locality scroll.
                float cFontSize = Images.Data.rFontMenuSize20->GetTextHeight();
                float tLocalityMax = (cFontSize * (ComputeVisibleLocalityEntries() - tUseMax));
                float cLocalityRange = tLocalityMax - 2.0f;
                float cSeverity = (cLocalityRange / 221.0f) * 2.50f;

                //--Set.
                mLocalityScroll = mLocalityScroll + ((tMouseY - mOriginalDragPoint) * cSeverity);
                mOriginalDragPoint = tMouseY;

                //--Clamp the locality scroll.
                ClampLocalityScroll();
            }

            //--Dragging the text scrollbar.
            if(mIsDraggingTextScrollbar)
            {
                //--Determine how much each pixel is in terms of locality scroll.
                float cMax = TL_MAX_TEXTLOG_LINES - 10;

                //--Set.
                mTextScrollOffset = cMax - (((tMouseY - 427.0f) / 300.0f) * cMax);
                mOriginalDragPoint = tMouseY;

                //--Clamp.
                if(mTextScrollOffset <    0) mTextScrollOffset = 0;
                if(mTextScrollOffset > cMax) mTextScrollOffset = cMax;
            }

            //--Left-click.
            if(rControlManager->IsFirstPress("MouseLft"))
            {
                //--Settings button.
                if(mSettingsDim.IsPointWithin(tMouseX, tMouseY) && !mNoSettingsButton)
                {
                    mOptionsScreen->Activate();
                    mOptionsScreen->RefreshValues(this);
                    AudioManager::Fetch()->PlaySound("Menu|Select");
                    return;
                }
                //--Navigation Buttons.
                else if(IsPointWithin2DReal(tMouseX, tMouseY, mNavButtonDim[TL_NAV_INDEX_LOOK]) && !mNoNavigationButtons)
                {
                    RunCommand("look");
                }
                else if(IsPointWithin2DReal(tMouseX, tMouseY, mNavButtonDim[TL_NAV_INDEX_NORTH]) && !mNoNavigationButtons)
                {
                    RunCommand("north");
                }
                else if(IsPointWithin2DReal(tMouseX, tMouseY, mNavButtonDim[TL_NAV_INDEX_UP]) && !mNoNavigationButtons)
                {
                    RunCommand("up");
                }
                else if(IsPointWithin2DReal(tMouseX, tMouseY, mNavButtonDim[TL_NAV_INDEX_WEST]) && !mNoNavigationButtons)
                {
                    RunCommand("west");
                }
                else if(IsPointWithin2DReal(tMouseX, tMouseY, mNavButtonDim[TL_NAV_INDEX_WAIT]) && !mNoNavigationButtons)
                {
                    RunCommand("wait");
                }
                else if(IsPointWithin2DReal(tMouseX, tMouseY, mNavButtonDim[TL_NAV_INDEX_EAST]) && !mNoNavigationButtons)
                {
                    RunCommand("east");
                }
                else if(IsPointWithin2DReal(tMouseX, tMouseY, mNavButtonDim[TL_NAV_INDEX_THINK]) && !mNoNavigationButtons)
                {
                    RunCommand("think");
                }
                else if(IsPointWithin2DReal(tMouseX, tMouseY, mNavButtonDim[TL_NAV_INDEX_SOUTH]) && !mNoNavigationButtons)
                {
                    RunCommand("south");
                }
                else if(IsPointWithin2DReal(tMouseX, tMouseY, mNavButtonDim[TL_NAV_INDEX_DOWN]) && !mNoNavigationButtons)
                {
                    RunCommand("down");
                }
                //--Zoom buttons.
                else if(IsPointWithin2DReal(tMouseX, tMouseY, mNavButtonDim[TL_NAV_INDEX_ZOOMDN]) && !mNoZoomButtons)
                {
                    mScaleIndex ++;
                    if(mScaleIndex >= TL_SCALES_TOTAL) mScaleIndex --;
                    mBackgroundScale = mScaleListing[mScaleIndex];
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                }
                else if(IsPointWithin2DReal(tMouseX, tMouseY, mNavButtonDim[TL_NAV_INDEX_ZOOMUP]) && !mNoZoomButtons)
                {
                    mScaleIndex --;
                    if(mScaleIndex < 1) mScaleIndex  = 0;
                    mBackgroundScale = mScaleListing[mScaleIndex];
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                }

                //--If we clicked within the map area, but didn't hit any of the nav buttons, see if the click handler
                //  has a command on it. This does not occur if the nav buttons are disabled.
                if(IsPointWithin2DReal(tMouseX, tMouseY, mMapAreaDim) && strcasecmp(mPlayerWorldClickHandler, "Null") && !mNoNavigationButtons)
                {
                    RunCommand(mPlayerWorldClickHandler);
                }

                //--If the click was on the text scrollbar:
                if(IsPointWithin(tMouseX, tMouseY, 947.0f, 404.0f, 970.0f, 751.0f))
                {
                    //--Arrows.
                    float tEffectiveY = tMouseY - 404.0f;
                    if(tEffectiveY < 24.0f)
                    {
                        mTextScrollOffset ++;
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    }
                    else if(tEffectiveY >= 348 - 24.0f)
                    {
                        mTextScrollOffset --;
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    }
                    //--Neither arrow:
                    else
                    {
                        //--Compute the position of the bar.
                        float cBarHei = 300.0f;
                        float cMax = (float)TL_MAX_TEXTLOG_LINES;
                        float cUsed = 10.0f;
                        float cTop = (((float)mTextScrollOffset / cMax) * cBarHei);
                        float cBot = cTop + ((cUsed / cMax) * cBarHei);

                        //--Reverse the top and bottom. The scrollbar goes up.
                        cTop = 728.0f - cTop;
                        cBot = 728.0f - cBot;

                        //--Clicked above the bar: Move down by 3 entries.
                        if(tMouseY < cBot)
                        {
                            mTextScrollOffset = mTextScrollOffset + 3;
                            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                        }
                        //--Below the bar. Move down 3 entries.
                        else if(tMouseY > cTop)
                        {
                            mTextScrollOffset = mTextScrollOffset - 3;
                            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                        }
                        //--On the bar. Begin drag.
                        else
                        {
                            mIsDraggingTextScrollbar = true;
                            mOriginalDragPoint = tMouseY;
                            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                        }
                    }

                    //--Clamp.
                    if(mTextScrollOffset < 0) mTextScrollOffset = 0;
                    if(mTextScrollOffset > TL_MAX_TEXTLOG_LINES - 10) mTextScrollOffset = TL_MAX_TEXTLOG_LINES - 10;
                }

                //--If the click was on the locality scrollbar:
                int tExpectedLocalityEntries = ComputeVisibleLocalityEntries();
                if(tExpectedLocalityEntries > tUseMax && tMouseX >= mLocalityWindow.mRgt - 25.0f)
                {
                    //--Sizes.
                    float cFontSize = Images.Data.rFontMenuSize20->GetTextHeight();

                    //--Check if it's on either the up arrow:
                    float tEffectiveY = tMouseY - mLocalityWindow.mTop;
                    if(tEffectiveY <= 24.0f)
                    {
                        mLocalityScroll = mLocalityScroll - cFontSize;
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    }
                    //--Down arrow:
                    else if(tEffectiveY >= 258.0f)
                    {
                        mLocalityScroll = mLocalityScroll + cFontSize;
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    }
                    //--Neither up nor down arrow.
                    else
                    {
                        //--Compute the position of the bar.
                        float cBarStart = 503.0f;
                        float cBarHei = 239.0f;
                        float cTextHei = Images.Data.rFontMenuSize20->GetTextHeight();
                        float cMax = tExpectedLocalityEntries * cTextHei;
                        float cUsed = tUseMax * cTextHei;
                        float cTop = cBarStart + ((mLocalityScroll / cMax) * cBarHei);
                        float cBot = cTop + ((cUsed / cMax) * cBarHei);

                        //--Clicked above the bar: Move down by 3 entries.
                        if(tMouseY < cTop)
                        {
                            mLocalityScroll = mLocalityScroll - (cFontSize * 3.0f);
                        }
                        //--Below the bar. Move down 3 entries.
                        else if(tMouseY > cBot)
                        {
                            mLocalityScroll = mLocalityScroll + (cFontSize * 3.0f);
                        }
                        //--On the bar. Begin drag.
                        else
                        {
                            mIsDraggingLocalityBar = true;
                            mOriginalDragPoint = tMouseY;
                            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                        }
                    }
                    //--Clamp the locality scroll.
                    ClampLocalityScroll();
                }

                //--If we're using the locality icons, handle that.
                if(mLocalityUsesIcons)
                {
                    //--Get position.
                    float tX = tMouseX - mLocalityWindow.mLft;
                    float tY = tMouseY - mLocalityWindow.mTop;

                    //--Don't change locality highlight or handle clicks if over the icons.
                    bool tBypassFromIcons = (tY <= TL_LOCALITY_SIZE);

                    //--Less than zero in any direction.
                    if(tX < 0 || tY < 0)
                    {

                    }
                    //--If we're inside the headings, this click sets the visible category.
                    else if(tY <= TL_LOCALITY_SIZE && tX < 224)
                    {
                        //--Range check.
                        int tEntry = (int)(tX / TL_LOCALITY_SIZE);
                        if(tEntry >= 0 && tEntry < mCurrentLocalityList->GetListSize())
                        {
                            //--Get the entry in question.
                            LocalityCategory *rCategory = (LocalityCategory *)mCurrentLocalityList->GetElementBySlot(tEntry);
                            if(rCategory)
                            {
                                rCategory->mIsOpened = !rCategory->mIsOpened;
                            }
                        }

                        //--SFX.
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    }
                    //--Otherwise, we're in the sublist. We use the open list. If none is open, nothing happens.
                    else if(!tBypassFromIcons)
                    {
                        //--Iterate across the locality headings.
                        bool tIsDone = false;
                        int i = 0;
                        LocalityCategory *rCategory = (LocalityCategory *)mCurrentLocalityList->PushIterator();
                        while(rCategory)
                        {
                            //--Heading is closed, skip it.
                            SugarLinkedList *rSublist = rCategory->mEntryList;
                            if(!rCategory->mIsOpened || !rSublist)
                            {
                                rCategory = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
                                continue;
                            }

                            //--Iterate across the category's subheadings.
                            LocalityCategory *rActiveCategory = (LocalityCategory *)rSublist->PushIterator();
                            while(rActiveCategory)
                            {
                                //--If this is not the highlighted entry, go to the next one.
                                if(i != mLocalityHighlight)
                                {
                                    i ++;
                                    rActiveCategory = (LocalityCategory *)rSublist->AutoIterate();
                                    continue;
                                }

                                //--If we got this far, this is the item being clicked. First, ignore entries with sub-headings.
                                if(rActiveCategory->mEntryList)
                                {
                                }
                                //--This has a popup builder, so run that and start the popup window.
                                else if(rActiveCategory->mPopupBuilder)
                                {
                                    //--If the popup builder is "INSTANT" then we just run it immediately. Note that this will
                                    //  likely destabilize the rActiveCategory so we return out immediately.
                                    if(!strcasecmp(rActiveCategory->mPopupBuilder, "INSTANT"))
                                    {
                                        RunCommand(rActiveCategory->mLocalName);
                                        return;
                                    }
                                    //--Otherwise, build the popup menu.
                                    else
                                    {
                                        xIsBuildingCommands = true;
                                        ResetString(xCommandHeadingString, rCategory->mLocalName);
                                        ResetString(xCommandBaseString, rActiveCategory->mLocalName);
                                        mPopupCommandList->ClearList();
                                        LuaManager::Fetch()->ExecuteLuaFile(rActiveCategory->mPopupBuilder);
                                        ComputePopupWindowDimensions();
                                        xIsBuildingCommands = false;
                                    }
                                }

                                //--We handled the click.
                                tIsDone = true;
                                rSublist->PopIterator();
                                break;
                            }

                            //--If we're done, break out.
                            if(tIsDone)
                            {
                                mCurrentLocalityList->PopIterator();
                                break;
                            }

                            //--Otherwise, go to the next highlight.
                            i ++;
                            rCategory = (LocalityCategory *)mCurrentLocalityList->AutoIterate();
                        }
                    }
                }
                //--Locality without icons.
                else
                {
                    LocalityCategory *rUseParent = GetNthParent(mLocalityHighlight);
                    LocalityCategory *rUseCategory = GetNthCategory(mLocalityHighlight);
                    if(rUseCategory && rUseParent)
                    {
                        //--This has a list, so open/close it.
                        if(rUseCategory->mEntryList)
                        {
                            rUseCategory->mIsOpened = !rUseCategory->mIsOpened;
                        }
                        else if(rUseCategory->mPopupBuilder)
                        {
                            //--If the popup builder is "INSTANT" then we just run it immediately. Note that this will
                            //  likely destabilize the rUseCategory so we return out immediately.
                            if(!strcasecmp(rUseCategory->mPopupBuilder, "INSTANT"))
                            {
                                RunCommand(rUseCategory->mLocalName);
                                return;
                            }
                            //--Otherwise, build the popup menu.
                            else
                            {
                                xIsBuildingCommands = true;
                                ResetString(xCommandHeadingString, rUseParent->mLocalName);
                                ResetString(xCommandBaseString, rUseCategory->mLocalName);
                                mPopupCommandList->ClearList();
                                LuaManager::Fetch()->ExecuteLuaFile(rUseCategory->mPopupBuilder);
                                ComputePopupWindowDimensions();
                                xIsBuildingCommands = false;
                            }
                        }
                    }
                }
            }
            //--Mouse over.
            else
            {
            }
        }
    }
    //--Otherwise, if a block is pending, clear it on keypress.
    else if(mIsBlockPending)
    {
        //--Any key will do.
        if(rControlManager->IsAnyKeyPressed())
        {
            mIsBlockPending = false;
        }
    }

    //--If, after keypresses have ended and there are no blocks pending, run events again. This means the
    //  events run as soon as the player has pressed a key assuming no blocks were already in place.
    if(tEventsRightNow != mInstructionList->GetListSize() && !mIsBlockPending)
    {
        HandleEvents(true);
    }

    //--If there are no events whatsoever, and we're flagged, starting major dialogue mode.
    if(mActivateMajorDialogueWhenQueueEmpties && mInstructionList->GetListSize() < 1)
    {
        mActivateMajorDialogueWhenQueueEmpties = false;
        mIsMajorDialogueMode = true;
        mMajorDialogueTimer = 0;
        WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
        rWorldDialogue->Show();
        rWorldDialogue->SetAllKeysHasten(true);
    }

    //--At the end of the tick, play the dialogue SFX if needed.
    if(mPlayTextTick) AudioManager::Fetch()->PlaySound("Doll|TextRoll");
}
bool TextLevel::HandleEvents(bool pBypass)
{
    //--Handles running events, such as changing images or adding text. Called at least once per update cycle.
    //  Gets called again if the player does some sort of input.
    //--Returns whether or not a block is in place. Blocks prevent the events from running until the player
    //  presses a key.
    if(!pBypass && !CanRunInstructions()) return false;

    //--Begin iterating.
    TextInstructionPack *rPack = (TextInstructionPack *)mInstructionList->SetToHeadAndReturn();
    while(rPack)
    {
        //--If this is a blocker, remove it and stop.
        if(rPack->mTypeCode == TEXT_INSTRUCTION_BLOCK)
        {
            mIsBlockPending = true;
            mInstructionList->RemoveRandomPointerEntry();
            return true;
        }
        //--If this is an append, append the text.
        else if(rPack->mTypeCode == TEXT_INSTRUCTION_APPEND)
        {
            //--Get the string and append it to the local dialogue.
            char *rString = (char *)rPack->mDataBlock;

            //--Get which font we are using.
            SugarFont *rUseFont = Images.Data.rFontMenuSize20;
            if(mUseLargeText) rUseFont = Images.Data.rFontMenuSize30;

            //--Length parsing. We need to subdivide the string. We don't know how many entries we will have!
            if(rString)
            {
                //--SFX.
                mPlayTextTick = true;

                //--Setup.
                int tCharsParsed = 0;
                int tCharsParsedTotal = 0;
                int tStringLen = (int)strlen(rString);
                SugarLinkedList *tSubdivideList = new SugarLinkedList(false);

                //--Loop until the whole description has been parsed out.
                while(tCharsParsedTotal < tStringLen)
                {
                    //--Run the subdivide.
                    char *tDescriptionLine = Subdivide::SubdivideString(tCharsParsed, &rString[tCharsParsedTotal], -1, cTxtWidth, rUseFont, 1.0f);
                    tSubdivideList->AddElementAsTail("X", tDescriptionLine);

                    //--Move to the next line.
                    tCharsParsedTotal += tCharsParsed;
                }

                //--Now liberate them and append them to the text log.
                tSubdivideList->SetRandomPointerToHead();
                char *nLiberatedString = (char *)tSubdivideList->LiberateRandomPointerEntry();
                while(nLiberatedString)
                {
                    //--Migrate to a text package.
                    SetMemoryData(__FILE__, __LINE__);
                    TextPack *nPack = (TextPack *)starmemoryalloc(sizeof(TextPack));
                    nPack->Initialize();
                    nPack->mText = nLiberatedString;
                    nPack->mRenderTimer = mAppendTimer;

                    //--If the line starts with [POS:XXX] then it does not affect the timer.
                    if(!strncasecmp(nLiberatedString, "[POS:", 5))
                    {
                        nPack->mRenderTimer = mAppendTimer + 1;
                    }
                    //--Check the line. If it contains one or fewer characters, it doesn't affect the timer.
                    else if((int)strlen(nLiberatedString) > 1)
                    {
                        mAppendTimer = mAppendTimer - TL_TIME_PER_LINE;
                    }

                    //--Store.
                    mTextLog->AddElementAsHead("X", nPack, &TextPack::DeleteThis);
                    mFontLog->AddElementAsHead("X", rUseFont);
                    mTextScrollOffset = 0;
                    while(mTextLog->GetListSize() > TL_MAX_TEXTLOG_LINES)
                    {
                        mTextLog->DeleteTail();
                        mFontLog->DeleteTail();
                    }

                    //--Next.
                    tSubdivideList->SetRandomPointerToHead();
                    nLiberatedString = (char *)tSubdivideList->LiberateRandomPointerEntry();
                }
            }

            //--Delete this instruction.
            mInstructionList->RemoveRandomPointerEntry();
        }
        //--If this is image registration, do that.
        else if(rPack->mTypeCode == TEXT_INSTRUCTION_REGISTERIMAGE)
        {
            //--Setup.
            TextImagePack *rImagePack = (TextImagePack *)rPack->mDataBlock;

            //--Set timer.
            mImgTransitionTimer = (int)TL_PLAYER_TRANSITION_TICKS;

            //--Cross-copy. If there were no override images found, use the defaults.
            int p = rImagePack->mLayer;
            if(p >= 0 && p < TL_PLAYER_LAYERS_MAX)
            {
                //--Store.
                rPreviousRenderImg[p] = rOverrideRenderImg[p];
                if(!rPreviousRenderImg[p]) rPreviousRenderImg[p] = rStandardRenderImg[p];

                //--Write the new image pointer.
                rOverrideRenderImg[p] = rImagePack->rImage;
            }

            //--If the name is "NOMOD" then don't change it.
            if(strcasecmp(rImagePack->mLocalName, "NOMOD"))
            {
                ResetString(mOverrideName, rImagePack->mLocalName);
            }

            //--Remove.
            mInstructionList->RemoveRandomPointerEntry();
        }
        //--Image unregistration.
        else if(rPack->mTypeCode == TEXT_INSTRUCTION_UNREGISTERIMAGE)
        {
            //--Set timer.
            mImgTransitionTimer = (int)TL_PLAYER_TRANSITION_TICKS;

            //--Cross-copy. If there were no override images found, use the defaults.
            for(int i = 0; i < TL_PLAYER_LAYERS_MAX; i ++)
            {
                //--Store.
                rPreviousRenderImg[i] = rOverrideRenderImg[i];
            }

            //--Clean and remove.
            memset(rOverrideRenderImg, 0, sizeof(void *) * TL_PLAYER_LAYERS_MAX);
            ResetString(mOverrideName, NULL);
            mInstructionList->RemoveRandomPointerEntry();
        }
        //--Changes default image.
        else if(rPack->mTypeCode == TEXT_INSTRUCTION_DEFAULTIMAGE)
        {
            //--Setup.
            TextImagePack *rImagePack = (TextImagePack *)rPack->mDataBlock;
            if(rImagePack->mLayer >= 0 && rImagePack->mLayer < TL_PLAYER_LAYERS_MAX)
            {
                rStandardRenderImg[rImagePack->mLayer] = rImagePack->rImage;
            }

            //--If the name is "NOMOD" then don't change it.
            if(strcasecmp(rImagePack->mLocalName, "NOMOD"))
            {
                ResetString(mStandardName, rImagePack->mLocalName);
            }

            //--Remove.
            mInstructionList->RemoveRandomPointerEntry();
        }
        //--Executes a script.
        else if(rPack->mTypeCode == TEXT_INSTRUCTION_EXECSCRIPT)
        {
            //--Setup.
            TextScriptExecPack *rExecPack = (TextScriptExecPack *)rPack->mDataBlock;
            LuaManager *rLuaManager = LuaManager::Fetch();

            //--Set arguments.
            rLuaManager->ClearArgumentList();
            rLuaManager->SetArgumentListSize(rExecPack->mArgsTotal);
            for(int i = 0; i < rExecPack->mArgsTotal; i ++)
            {
                rLuaManager->AddArgument((const char *)rExecPack->mArguments[i]);
            }

            //--Execute the file.
            rLuaManager->ExecuteLuaFileBypass(rExecPack->mScriptPath);

            //--Remove.
            mInstructionList->RemoveRandomPointerEntry();
        }
        //--Plays a sound effect.
        else if(rPack->mTypeCode == TEXT_INSTRUCTION_SFX)
        {
            //--Play the SFX.
            AudioManager::Fetch()->PlaySound((const char *)rPack->mDataBlock);

            //--Remove.
            mInstructionList->RemoveRandomPointerEntry();
        }
        //--Begins a fade-out.
        else if(rPack->mTypeCode == TEXT_INSTRUCTION_FADEOUT)
        {
            StartFadeOut((const char *)rPack->mDataBlock);
            mInstructionList->RemoveRandomPointerEntry();
        }
        //--Begins a fade-in.
        else if(rPack->mTypeCode == TEXT_INSTRUCTION_FADEOUT)
        {
            StartFadeIn((const char *)rPack->mDataBlock);
            mInstructionList->RemoveRandomPointerEntry();
        }

        //--Next.
        rPack = (TextInstructionPack *)mInstructionList->IncrementAndGetRandomPointerEntry();
    }

    //--If no blockers were hit, not blocking.
    return false;
}

//=========================================== File I/O ============================================
void TextLevel::SaveFile(const char *pFileName)
{
    //--Base.
    if(!pFileName) return;

    //--Setup.
    lua_State *rLuaState = LuaManager::Fetch()->GetLuaState();

    //--Create a portable Lua state. Order it to save "gzTextVar", the table storing most of the data.
    PortableLuaState *nPortableState = new PortableLuaState();
    nPortableState->OperateOn(rLuaState, "gzTextVar");
    nPortableState->WriteToFile(pFileName);

    //--Clean.
    delete nPortableState;
}
void TextLevel::LoadFile(const char *pFileName)
{
    //--Base.
    if(!pFileName) return;

    //--Create a portable Lua state. We can then reconstruct it from the savefile.
    PortableLuaState *nPortableState = new PortableLuaState();
    nPortableState->ReadFromFile(pFileName);

    //--Clean.
    delete nPortableState;
}

//======================================= Pointer Routing =========================================
RootLevel *TextLevel::LiberateSuspendedLevel()
{
    RootLevel *rLevel = mSuspendedLevel;
    mSuspendedLevel = NULL;
    return rLevel;
}
TypingCombat *TextLevel::GetTypingCombatHandler()
{
    //--Note: Can legally return NULL if not in Typing combat mode! Activate the combat
    //  before calling this.
    return mTypingCombatHandler;
}
WordCombat *TextLevel::GetWordCombatHandler()
{
    //--Note: As above, can return NULL if not in Word combat mode.
    return mWordCombatHandler;
}

//====================================== Static Functions =========================================
TextLevel *TextLevel::Fetch()
{
    RootLevel *rCheckLevel = MapManager::Fetch()->GetActiveLevel();
    if(!rCheckLevel || !rCheckLevel->IsOfType(POINTER_TYPE_TEXTLEVEL)) return NULL;
    return (TextLevel *)rCheckLevel;
}
