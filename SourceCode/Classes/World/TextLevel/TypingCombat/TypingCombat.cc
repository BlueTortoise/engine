//--Base
#include "TypingCombat.h"

//--Classes
#include "WorldDialogue.h"
#include "TextLevelEnemyPack.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"
#include "StringEntry.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "DisplayManager.h"

//=========================================== System ==============================================
TypingCombat::TypingCombat()
{
    //--[TypingCombat]
    //--System
    mCombatResolution = TL_COMBAT_UNRESOLVED;
    mLocalStringEntry = new StringEntry();
    mLocalStringEntry->mIgnoreMouse = true;

    //--Combat
    mDifficulty = TL_DIFFICULTY_NORMAL;
    mCombatTransitionTimer = 0;
    mCombatTransitionMode = TL_COMBAT_MODE_NONE;
    mCombatTypeTimer = 0;
    mWordExplodeTimer = 0;
    mTurnChangeTimer = 0;
    mTurnResolveTimer = 1;
    mTurnResolveTimerMax = 1;
    mLastDamageTaken = 0;
    mCombatEnemyList = new SugarLinkedList(true);

    //--Combat Ending
    mEndingPhase = TL_COMBAT_ENDING_NONE;
    mTargetDyingTimer = 0;

    //--Player Statistics
    mPlayerFlashTimer = 0;
    mPlayerHP = 20;
    mPlayerHPMax = 20;
    mPlayerAtk = 5;
    mPlayerDef = 3;
    mPlayerLettersCurrent = 0;
    mPlayerLettersPerHit = 5;
    mPlayerStumbleTimer = 0;
    rPlayerCombatImage = NULL;
    mLastDamageStringA = InitializeString("None");
    mLastDamageStringB = InitializeString("None");

    //--Combat Text Pool
    mCombatActiveWord = -1;
    mCombatActiveLetter = 0;
    mTotalWordPower = 0.0f;
    mCombatTypeTimer = 0;
    mWordExplodeTimer = 0;
    mCombatTypeTimerMax = 0;
    mWordAngleOffset = 0.0f;
    mActiveCenterX = 0.0f;
    mActiveCenterY = 0.0f;
    mCombatAttackWords = new SugarLinkedList(false);
    mCombatDefendWords = new SugarLinkedList(false);
    mrActiveWordList = new SugarLinkedList(false);
    mrCompletedWordList = new SugarLinkedList(false);
    mFlyingLetterList = new SugarLinkedList(true);

    //--Images
    memset(&Images, 0, sizeof(Images));

    //--[Construction]
    //--Fast Access Pointers
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Fonts.
    Images.Data.rFontMenuHeader  = rDataLibrary->GetFont("Typing Combat Header");
    Images.Data.rFontMenuSize    = rDataLibrary->GetFont("Typing Combat Medium");
    Images.Data.rFontMenuCommand = rDataLibrary->GetFont("Typing Combat Command");

    //--Images.
    Images.Data.rWordBorderCard  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/TxtAdv/UI/CombatWordBorderCard");
    Images.Data.rBorderCard      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/BorderCard");
    Images.Data.rMapParts        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/TextAdventureMapParts");
    Images.Data.rScrollbar       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/TextAdventureScrollbar");
    Images.Data.rPopupBorderCard = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/PopupBorderCard");
    Images.Data.rTextInput       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/TextInput");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));
}
TypingCombat::~TypingCombat()
{
    delete mLocalStringEntry;
    delete mCombatEnemyList;
    free(mLastDamageStringA);
    free(mLastDamageStringB);
    delete mCombatAttackWords;
    delete mCombatDefendWords;
    delete mrActiveWordList;
    delete mrCompletedWordList;
    delete mFlyingLetterList;
}
void TypingCombat::Initialize()
{
    //--Re-initializes the combat engine for a new battle. Should be called before anything else.
    //  Variables will be reset and lists will be cleared to their neutral states.
    mCombatResolution = TL_COMBAT_UNRESOLVED;

    //--Combat Flags.
    mCombatTransitionTimer = 0;
    mCombatTransitionMode = TL_COMBAT_MODE_FADEIN;
    mCombatEnemyList->ClearList();
    mTurnChangeTimer = 0;
    mTurnResolveTimer = 1;
    mTurnResolveTimerMax = 1;
    mLastDamageTaken = 0;

    //--Word Flags.
    mCombatActiveWord = -1;
    mCombatActiveLetter = 0;
    mTotalWordPower = 0.0f;
    mCombatTypeTimer = 0;
    mWordExplodeTimer = 0;
    mCombatTypeTimerMax = 0;
    mCombatAttackWords->ClearList();
    mCombatDefendWords->ClearList();
    mrActiveWordList->ClearList();
    mrCompletedWordList->ClearList();
}

//====================================== Property Queries =========================================
int TypingCombat::GetPlayerHP()
{
    return mPlayerHP;
}
int TypingCombat::GetCombatResolution()
{
    //--Reports how combat has resolved. TL_COMBAT_UNRESOLVED means the battle is continuing.
    return mCombatResolution;
}
float TypingCombat::ComputeLetterX(int pActiveWord, int pActiveLetter)
{
    //--Computes and returns the X position of the given letter of the given word. This routine can
    //  be quite complex and is thus given its own function.
    //--This is NOT used for the renderer, that would be inefficient. This is only used when spawning
    //  flying letters.
    const char *rWordInQuestion = mrActiveWordList->GetNameOfElementBySlot(pActiveWord);
    if(!rWordInQuestion) return 0.0f;

    //--Error check.
    int cWordsTotal = mrActiveWordList->GetListSize();

    //--Constants.
    float cTicks = 15.0f;
    float cMaxExtension = 150.0f;
    float cDegPerWord = 100.0f;
    if(cWordsTotal > 0) cDegPerWord = 360.0f / (float)cWordsTotal * TORADIAN;

    //--Resolve position.
    float cPercent = EasingFunction::QuadraticInOut(mCombatTypeTimer, cTicks);
    float cExtension = cPercent * cMaxExtension;
    if(cExtension > cMaxExtension) cExtension = cMaxExtension;
    float tXPos = mActiveCenterX + (cosf(mWordAngleOffset + (pActiveWord * cDegPerWord)) * cExtension);

    //--Resolve scale.
    float cScale = (cPercent * 0.5f) + 0.5f;
    if(cScale < 0.5f) cScale = 0.5f;
    if(cScale > 1.0f) cScale = 1.0f;

    //--Word position.
    float cLft = tXPos - ((Images.Data.rFontMenuHeader->GetTextWidth(rWordInQuestion) * 0.5f) * cScale);

    //--Iterate across the letters.
    for(int p = 0; p < pActiveLetter; p ++)
    {
        cLft = cLft + Images.Data.rFontMenuHeader->GetKerningBetween(rWordInQuestion[p], rWordInQuestion[p+1]);
    }
    return cLft;
}
float TypingCombat::ComputeLetterY(int pActiveWord, int pActiveLetter)
{
    //--As above for the Y value.
    const char *rWordInQuestion = mrActiveWordList->GetNameOfElementBySlot(pActiveWord);
    if(!rWordInQuestion) return 0.0f;

    //--Error check.
    int cWordsTotal = mrActiveWordList->GetListSize();

    //--Constants.
    float cTicks = 15.0f;
    float cMaxExtension = 150.0f;
    float cDegPerWord = 100.0f;
    if(cWordsTotal > 0) cDegPerWord = 360.0f / (float)cWordsTotal * TORADIAN;

    //--Resolve position.
    float cPercent = EasingFunction::QuadraticInOut(mCombatTypeTimer, cTicks);
    float cExtension = cPercent * cMaxExtension;
    if(cExtension > cMaxExtension) cExtension = cMaxExtension;
    float tYPos = mActiveCenterY + (sinf(mWordAngleOffset + (pActiveWord * cDegPerWord)) * cExtension);
    return tYPos;
}

//========================================= Manipulators ==========================================
void TypingCombat::SetDifficulty(int pValue)
{
    mDifficulty = pValue;
    if(mDifficulty < TL_DIFFICULTY_EASY) mDifficulty = TL_DIFFICULTY_EASY;
    if(mDifficulty > TL_DIFFICULTY_HARD) mDifficulty = TL_DIFFICULTY_HARD;
}
void TypingCombat::RegisterAttackWord(const char *pWord)
{
    static int xDummy = 1;
    mCombatAttackWords->AddElement(pWord, &xDummy);
}
void TypingCombat::RegisterDefendWord(const char *pWord)
{
    static int xDummy = 1;
    mCombatDefendWords->AddElement(pWord, &xDummy);
}
void TypingCombat::SetPlayerStats(int pHP, int pHPMax, int pAtk, int pDef, int pLettersPerHit, const char *pImagePath)
{
    //--Set.
    mPlayerHP = pHP;
    mPlayerHPMax = pHPMax;
    mPlayerAtk = pAtk;
    mPlayerDef = pDef;
    mPlayerLettersPerHit = pLettersPerHit;
    rPlayerCombatImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);

    //--Clamps.
    if(mPlayerHPMax < 1) mPlayerHPMax = 1;
    if(mPlayerAtk < 1) mPlayerAtk = 1;
    if(mPlayerDef < 1) mPlayerDef = 1;
    if(mPlayerLettersPerHit < 1) mPlayerLettersPerHit = 1;
}
void TypingCombat::RegisterEnemy(int pHP, int pAtk, int pDef, const char *pImagePath)
{
    if(pHP < 1 || !pImagePath) return;
    SetMemoryData(__FILE__, __LINE__);
    CombatEnemyPack *nPack = (CombatEnemyPack *)starmemoryalloc(sizeof(CombatEnemyPack));
    nPack->Initialize();
    nPack->mHP = pHP;
    nPack->mHPMax = pHP;
    nPack->mAttackPower = pAtk;
    nPack->mDefensePower = pDef;
    nPack->rImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
    mCombatEnemyList->AddElement("X", nPack, &FreeThis);
}

//========================================= Core Methods ==========================================
void TypingCombat::BeginPlayerAttackTurn()
{
    //--Flags.
    mIsPlayerAttackCase = true;
    mIsTimerStarted = false;
    mCombatActiveWord = -1;
    mCombatActiveLetter = 0;
    mTotalWordPower = 0.0f;
    mCombatTypeTimer = 0;
    if(mDifficulty == TL_DIFFICULTY_EASY)
    {
        mIsTimerStarted = false;
        mCombatTypeTimerMax = COMBAT_TURN_TICKS_EASY;
    }
    else if(mDifficulty == TL_DIFFICULTY_NORMAL)
    {
        mIsTimerStarted = false;
        mCombatTypeTimerMax = COMBAT_TURN_TICKS_NORMAL;
    }
    else
    {
        mIsTimerStarted = true;
        mCombatTypeTimerMax = COMBAT_TURN_TICKS_HARD;
    }
    mTurnChangeTimer = 0;

    //--Scatter.
    mWordAngleOffset = (rand() % 360) * TORADIAN;

    //--Run this subroutine to select words.
    SelectCombatWordsFromList(mCombatAttackWords);
}
void TypingCombat::BeginPlayerDefendTurn()
{
    //--Flags.
    mIsPlayerAttackCase = false;
    mIsTimerStarted = false;
    mCombatActiveWord = -1;
    mCombatActiveLetter = 0;
    mTotalWordPower = 0.0f;
    mCombatTypeTimer = 0;
    mWordExplodeTimer = 0;
    if(mDifficulty == TL_DIFFICULTY_EASY)
    {
        mIsTimerStarted = false;
        mCombatTypeTimerMax = COMBAT_TURN_TICKS_EASY;
    }
    else if(mDifficulty == TL_DIFFICULTY_NORMAL)
    {
        mIsTimerStarted = false;
        mCombatTypeTimerMax = COMBAT_TURN_TICKS_NORMAL;
    }
    else
    {
        mIsTimerStarted = true;
        mCombatTypeTimerMax = COMBAT_TURN_TICKS_HARD;
    }
    mTurnChangeTimer = 0;

    //--Scatter.
    mWordAngleOffset = (rand() % 360) * TORADIAN;

    //--Run this subroutine to select words.
    SelectCombatWordsFromList(mCombatDefendWords);
}
void TypingCombat::SelectCombatWordsFromList(SugarLinkedList *pWordList)
{
    //--Given a list of words, selects five words for use in combat. The words can never start with
    //  the same letter as another word.
    if(!pWordList) return;

    //--Constants.
    static int xDummyVar = 1;

    //--Setup.
    int tListSize = pWordList->GetListSize();
    if(tListSize < 5) return;

    //--Begin rolling.
    int tAttemptsLeft = 100;
    mrActiveWordList->ClearList();
    mrCompletedWordList->ClearList();
    while(mrActiveWordList->GetListSize() < 5 && tAttemptsLeft > 0)
    {
        //--Decrement.
        tAttemptsLeft --;

        //--Roll a slot.
        int tSlot = rand() % tListSize;
        const char *rString = pWordList->GetNameOfElementBySlot(tSlot);

        //--Check the rest of the active word list to see if any of them start with the same letter.
        bool tFailure = false;
        void *rDummyPtr = mrActiveWordList->PushIterator();
        while(rDummyPtr)
        {
            //--Get.
            const char *rCompareString = mrActiveWordList->GetIteratorName();
            if(rCompareString[0] == rString[0])
            {
                tFailure = true;
                mrActiveWordList->PopIterator();
                break;
            }

            //--Next.
            rDummyPtr = mrActiveWordList->AutoIterate();
        }

        //--If this flag was set, then the word is illegal. Try again.
        if(tFailure) continue;

        //--Success! Add it to the word list.
        mrActiveWordList->AddElementAsTail(rString, &xDummyVar);
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void TypingCombat::Update()
{
    //--Handles the update of the combat cycle. Should not be called if not in combat, obviously.
    if(mCombatTransitionMode == TL_COMBAT_MODE_FADEIN)
    {
        mCombatTransitionTimer ++;
        if(mCombatTransitionTimer >= COMBAT_TRANSITION_TICKS)
        {
            mCombatTransitionMode = TL_COMBAT_MODE_BATTLE;
        }
        return;
    }
    //--Victory.
    else if(mCombatTransitionMode == TL_COMBAT_MODE_VICTORY)
    {
        //--Initializer.
        if(mEndingPhase == TL_COMBAT_ENDING_NONE)
        {
            mCombatTransitionTimer = 0;
            mEndingPhase = TL_COMBAT_ENDING_VICTORY_JINGLE;
            AudioManager::Fetch()->PlayMusicNoFade("CombatVictory");
        }
        //--Jingle plays, display Victory.
        else
        {
            //--Timer.
            mCombatTransitionTimer ++;

            //--At this point, end combat.
            if(mCombatTransitionTimer >= TL_VICTORY_TIME_TOTAL)
            {
                mCombatResolution = TL_COMBAT_PLAYER_WINS;
            }
        }

        return;
    }
    //--Defeat.
    else if(mCombatTransitionMode == TL_COMBAT_MODE_DEFEAT)
    {
        //--Initializer.
        if(mEndingPhase == TL_COMBAT_ENDING_NONE)
        {
            mCombatTransitionTimer = 0;
            mEndingPhase = TL_COMBAT_ENDING_VICTORY_JINGLE;
            AudioManager::Fetch()->PlayMusicNoFade("DesolateShort");
        }
        //--Jingle plays, display Defeat.
        else
        {
            //--Timer.
            mCombatTransitionTimer ++;

            //--At this point, end combat.
            if(mCombatTransitionTimer >= TL_VICTORY_TIME_TOTAL)
            {
                mCombatResolution = TL_COMBAT_PLAYER_DEFEATED;
            }
        }
        return;
    }

    //--If the player is defeated, end.
    if(mPlayerHP < 1)
    {
        mCombatTransitionTimer = 0;
        mCombatTransitionMode = TL_COMBAT_MODE_DEFEAT;
        return;
    }

    //--If all enemies are defeated, end.
    if(mCombatEnemyList->GetListSize() < 1)
    {
        mCombatTransitionTimer = 0;
        mCombatTransitionMode = TL_COMBAT_MODE_VICTORY;
        return;
    }

    //--[Flying Letters]
    FlyingLetterPack *rPack = (FlyingLetterPack *)mFlyingLetterList->SetToHeadAndReturn();
    while(rPack)
    {
        //--Run timer.
        rPack->mTimer ++;
        if(rPack->mTimer >= rPack->mTimerMax) mFlyingLetterList->RemoveRandomPointerEntry();

        //--Next.
        rPack = (FlyingLetterPack *)mFlyingLetterList->IncrementAndGetRandomPointerEntry();
    }

    //--[Other Timers]
    bool tAnyFlashes = false;
    if(mPlayerFlashTimer > 0) mPlayerFlashTimer --;
    CombatEnemyPack *rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->SetToHeadAndReturn();
    while(rEnemyPack)
    {
        //--Run timer.
        if(rEnemyPack->mFlashTimer > 0)
        {
            tAnyFlashes = true;
            rEnemyPack->mFlashTimer --;
        }

        //--Next.
        rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->IncrementAndGetRandomPointerEntry();
    }

    //--If an enemy is dying, run this timer and stop.
    if(mTargetDyingTimer > 0)
    {
        //--Timer.
        if(tAnyFlashes == false)
        {
            if(mTargetDyingTimer == ENEMY_DIE_TICKS_TOTAL - 2) AudioManager::Fetch()->PlaySound("Combat|MonsterDie");
            mTargetDyingTimer --;
        }

        //--Enemy fully dies.
        if(mTargetDyingTimer < 1)
        {
            mCombatEnemyList->DeleteHead();
        }
        return;
    }

    //--[Turn Handlers]
    //--Turn is resolving.
    if(mTurnResolveTimer < mTurnResolveTimerMax)
    {
        //--Timer.
        mTurnResolveTimer ++;

        //--Intro. Do nothing.
        if(mTurnResolveTimer < COMBAT_RESULT_TICKS_INTRO)
        {

        }
        //--For each word result, play a sound effect.
        else if(mTurnResolveTimer < COMBAT_RESULT_TICKS_INTRO + (COMBAT_RESULT_TICKS_PERWORD * mrCompletedWordList->GetListSize()))
        {
            int tUseTimer = mTurnResolveTimer - COMBAT_RESULT_TICKS_INTRO;
            if(tUseTimer % COMBAT_RESULT_TICKS_PERWORD == COMBAT_RESULT_TICKS_PERWORD - 1)
            {
                AudioManager::Fetch()->PlaySound("World|ButtonClick");
            }
        }
        //--Wait a bit.
        else if(mTurnResolveTimer < COMBAT_RESULT_TICKS_INTRO + (COMBAT_RESULT_TICKS_PERWORD * mrCompletedWordList->GetListSize()) + COMBAT_RESULT_TICKS_WAIT)
        {

        }
        //--Execute damage, show the tally.
        else if(mTurnResolveTimer < COMBAT_RESULT_TICKS_INTRO + (COMBAT_RESULT_TICKS_PERWORD * mrCompletedWordList->GetListSize()) + COMBAT_RESULT_TICKS_WAIT + COMBAT_RESULT_TICKS_DAMAGETOTAL)
        {
            //--Zeroth tick: Apply damage to whoever it's coming to.
            int tUseTimer = mTurnResolveTimer - (COMBAT_RESULT_TICKS_INTRO + (COMBAT_RESULT_TICKS_PERWORD * mrCompletedWordList->GetListSize()) + COMBAT_RESULT_TICKS_WAIT);
            if(tUseTimer == 0)
            {
                //--SFX.
                AudioManager::Fetch()->PlaySound("Combat|Impact_Pierce");

                //--Compute bonus power.
                int tBaseBonus = 0;
                void *rDummyPtr = mrCompletedWordList->PushIterator();
                while(rDummyPtr)
                {
                    //--Words give +10% per letter, and a flat +30% no matter what.
                    tBaseBonus = tBaseBonus + 3 + (int)strlen(mrCompletedWordList->GetIteratorName());

                    //--Next.
                    rDummyPtr = mrCompletedWordList->AutoIterate();
                }

                //--Attack Case:
                if(mIsPlayerAttackCase)
                {
                    //--Get the zeroth enemy.
                    int tDamage = 0;
                    int tEnemyDefense = 0;
                    CombatEnemyPack *rZeroEnemy = (CombatEnemyPack *)mCombatEnemyList->GetElementBySlot(0);
                    if(rZeroEnemy)
                    {
                        //--Damage computation.
                        float tAttackBonus = tBaseBonus * 0.10f;
                        tEnemyDefense = rZeroEnemy->mDefensePower;
                        tDamage = (mPlayerAtk * tAttackBonus) - rZeroEnemy->mDefensePower;
                        if(tDamage < 1) tDamage = 1;

                        //--Deal damage, kill if the enemy dropped.
                        rZeroEnemy->mHP -= tDamage;
                        rZeroEnemy->mFlashTimer = 30;
                        mLastDamageTaken = tDamage;

                        //--Begin death sequence.
                        if(rZeroEnemy->mHP < 1)
                        {
                            mTargetDyingTimer = ENEMY_DIE_TICKS_TOTAL;
                        }
                    }

                    //--Store.
                    free(mLastDamageStringA);
                    mLastDamageStringA = InitializeString("Attack Bonus: %i%%", tBaseBonus * 10);
                    free(mLastDamageStringB);
                    mLastDamageStringB = InitializeString("Total: %i (%i Defended)", tDamage, tEnemyDefense);
                }
                //--Defend Case:
                else
                {
                    //--Cut the bonus in half. Cap at 75%.
                    float tDefenseBonus = tBaseBonus * 0.10f * 0.50f;
                    if(tDefenseBonus >= 0.75f) tDefenseBonus = 0.75f;

                    //--All enemies attack at the same time.
                    int tTotalAttackPower = 0;
                    CombatEnemyPack *rEnemy = (CombatEnemyPack *)mCombatEnemyList->PushIterator();
                    while(rEnemy)
                    {
                        //--Damage computation.
                        tTotalAttackPower = tTotalAttackPower + rEnemy->mAttackPower;

                        //--Next.
                        rEnemy = (CombatEnemyPack *)mCombatEnemyList->AutoIterate();
                    }

                    //--Player takes that much damage.
                    int tDamage = tTotalAttackPower * (1.0f - tDefenseBonus);
                    if(tDamage < 1) tDamage = 1;
                    mPlayerHP -= tDamage;
                    mLastDamageTaken = tDamage;
                    mPlayerFlashTimer = 30;

                    //--Store.
                    free(mLastDamageStringA);
                    mLastDamageStringA = InitializeString("Defense Bonus: %i%%", tBaseBonus * 10);
                    free(mLastDamageStringB);
                    mLastDamageStringB = InitializeString("Taken: %i (Defended %i)", tDamage, tTotalAttackPower - tDamage);
                }
            }
        }
        //--Outro.
        else
        {

        }

        //--Ending case.
        if(mTurnResolveTimer >= mTurnResolveTimerMax)
        {
            //--Switch to defense.
            if(mIsPlayerAttackCase)
            {
                BeginPlayerDefendTurn();
            }
            //--Switch to attack.
            else
            {
                BeginPlayerAttackTurn();
            }
        }
        return;
    }
    //--Indicate to the player whether they are attacking or defending.
    if(mTurnChangeTimer < COMBAT_TURN_TICKS_DISPLAY)
    {
        mTurnChangeTimer ++;
        return;
    }
    //--Otherwise the turn is running.
    else
    {
        //--Run.
        if(mIsTimerStarted) mCombatTypeTimer ++;
        mWordExplodeTimer ++;

        //--Ending case.
        if(mCombatTypeTimer >= mCombatTypeTimerMax)
        {
            //--Begin the turn resolve sequence.
            mTurnResolveTimer = 0;
            mTurnResolveTimerMax = COMBAT_RESULT_TICKS_INTRO + (COMBAT_RESULT_TICKS_PERWORD * mrCompletedWordList->GetListSize()) + COMBAT_RESULT_TICKS_WAIT + COMBAT_RESULT_TICKS_DAMAGETOTAL + COMBAT_RESULT_TICKS_OUTTRO;
            return;
        }
    }

    //--[Typing Handler]
    //--Get what key, if any, was last pressed.
    mLocalStringEntry->Update();
    const char *rCurrentString = mLocalStringEntry->GetString();
    if(rCurrentString[0] == '\0') return;

    //--Player has not started typing a word.
    if(mCombatActiveWord == -1)
    {
        //--Scan across the active words. If the letter matches the first letter of any of them,
        //  focus on that word.
        int i = 0;
        void *rDummyPtr = mrActiveWordList->PushIterator();
        while(rDummyPtr)
        {
            //--Get name, check first letter.
            const char *rCheckWord = mrActiveWordList->GetIteratorName();
            if(rCheckWord[0] == rCurrentString[0])
            {
                //--Match! Set this as the active word.
                mCombatActiveWord = i;
                mCombatActiveLetter = 1;
                mIsTimerStarted = true;
                AudioManager::Fetch()->PlaySound("World|ButtonClick");

                //--Spawn a flying letter.
                SetMemoryData(__FILE__, __LINE__);
                FlyingLetterPack *nPack = (FlyingLetterPack *)starmemoryalloc(sizeof(FlyingLetterPack));
                nPack->Initialize();
                nPack->mX = ComputeLetterX(i, 0);
                nPack->mY = ComputeLetterY(i, 0);
                nPack->mXTarget = mActiveCenterX;
                nPack->mYTarget = mActiveCenterY;
                nPack->mTimer = 0;
                nPack->mTimerMax = 30;
                nPack->mLetter = rCurrentString[0];
                mFlyingLetterList->AddElement("X", nPack, &FreeThis);

                //--Finish up.
                mrActiveWordList->PopIterator();
                break;
            }

            //--Next.
            i ++;
            rDummyPtr = mrActiveWordList->AutoIterate();
        }
    }
    //--Player is typing a word.
    else
    {
        //--Get the word in question.
        const char *rCheckWord = mrActiveWordList->GetNameOfElementBySlot(mCombatActiveWord);
        if(rCheckWord)
        {
            //--Check if the letter typed is the next one.
            bool tHit = false;
            if(rCheckWord[mCombatActiveLetter] == rCurrentString[0])
            {
                //--Match! Score a hit.
                mCombatActiveLetter ++;
                tHit = true;

                //--Spawn a flying letter.
                SetMemoryData(__FILE__, __LINE__);
                FlyingLetterPack *nPack = (FlyingLetterPack *)starmemoryalloc(sizeof(FlyingLetterPack));
                nPack->Initialize();
                nPack->mX = ComputeLetterX(mCombatActiveWord, mCombatActiveLetter-1);
                nPack->mY = ComputeLetterY(mCombatActiveWord, mCombatActiveLetter-1);
                nPack->mXTarget = mActiveCenterX;
                nPack->mYTarget = mActiveCenterY;
                nPack->mTimer = 0;
                nPack->mTimerMax = 30;
                nPack->mLetter = rCheckWord[mCombatActiveLetter-1];
                mFlyingLetterList->AddElement("X", nPack, &FreeThis);
            }

            //--Check if the word was completed. If so, remove it and score a hit.
            if(mCombatActiveLetter >= (int)strlen(rCheckWord))
            {
                static int xDummyInt = 1;
                mrCompletedWordList->AddElement(rCheckWord, &xDummyInt);
                mrActiveWordList->RemoveElementI(mCombatActiveWord);
                mCombatActiveWord = -1;
                mCombatActiveLetter = 0;
                mTotalWordPower = mTotalWordPower + 1.0f;
                AudioManager::Fetch()->PlaySound("Combat|Impact_Strike");
            }
            //--SFX for a normal letter.
            else if(tHit)
            {
                AudioManager::Fetch()->PlaySound("World|ButtonClick");
            }
        }
    }

    //--In all cases, clear the input string.
    mLocalStringEntry->SetString(NULL);
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void TypingCombat::Render()
{
    //--GL Setup.
    glTranslatef(0.0f, 0.0f, TL_DEPTH_GUI);

    //--Renders the combat overlay. First, handle the to-battle animation.
    if(mCombatTransitionMode == TL_COMBAT_MODE_FADEIN)
    {
        //--GL Setup.
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.0f, 0.0f, 0.0f, 0.75f);

        //--Central positioning.
        glTranslatef(VIRTUAL_CANVAS_X * 0.5f, VIRTUAL_CANVAS_Y * 0.5f, 0.0f);

        //--Angle. Increases slowly over the run of the pulse.
        float cStartAngle = (mCombatTransitionTimer / ((float)COMBAT_TRANSITION_TICKS * 1.5f)) * 3.1415926f * 2.0f;
        if(mCombatTransitionTimer > 30) cStartAngle = cStartAngle + (((mCombatTransitionTimer-30) / ((float)COMBAT_TRANSITION_TICKS * 0.05f)) * 3.1415926f * 2.0f);

        //--There are ten triangles, each rotating at 36 degrees to one another.
        float tPercentage = EasingFunction::QuadraticIn(mCombatTransitionTimer, COMBAT_TRANSITION_TICKS);
        float cRadius = (tPercentage * (VIRTUAL_CANVAS_X * 1.65f)) + 15.0f;
        glBegin(GL_TRIANGLES);
        for(int i = 0; i < 10; i ++)
        {
            //--Get the angle, compute the position.
            float tAngleA = cStartAngle + ((36.0f * TORADIAN) * (float)i);
            float tAngleB = cStartAngle + ((36.0f * TORADIAN) * ((float)i + 1.001000f));

            //--Render.
            glVertex2f(0.0f,                           0.0f);
            glVertex2f(cosf(tAngleA) * cRadius,        sinf(tAngleA) * cRadius);
            glVertex2f(cosf(tAngleB) * cRadius * 0.5f, sinf(tAngleB) * cRadius * 0.5f);
        }
        glEnd();

        //--Clean.
        glTranslatef(VIRTUAL_CANVAS_X * -0.5f, VIRTUAL_CANVAS_Y * -0.5f, 0.0f);
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
        return;
    }

    //--Fade handler.
    float tAlphaMixer = 1.0f;
    if(mCombatTransitionMode == TL_COMBAT_MODE_VICTORY)
    {
        //--Compute alpha. Only happens if past a certain point.
        if(mCombatTransitionTimer > TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)
        {
            tAlphaMixer = 1.0f - ((mCombatTransitionTimer - (TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)) / (float)TL_VICTORY_TIME_BIGBANNER_FADE);
        }
    }

    //--Otherwise, this is normal battle mode. Render a transparent overlay.
    SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.75f * tAlphaMixer));
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlphaMixer);

    //--Render the player's graphic on the left.
    float cPlayerMiddleX = COMBAT_PARTY_CNTX;
    float cPlayerMiddleY = COMBAT_PARTY_CNTY;
    if(rPlayerCombatImage)
    {
        //--Compute position.
        float cIdealX = COMBAT_PARTY_CNTX - (rPlayerCombatImage->GetTrueWidth() * 0.5f);
        float cIdealY = COMBAT_PARTY_CNTY - (rPlayerCombatImage->GetTrueHeight() * 0.5f);

        if(mPlayerFlashTimer % 6 < 3) rPlayerCombatImage->Draw(cIdealX, cIdealY);

        //--Damage Indicator
        if(mPlayerFlashTimer > 0)
        {
            float cPercent = 1.0f - EasingFunction::QuadraticInOut(mPlayerFlashTimer, 30.0f);
            float cTextXPos = COMBAT_PARTY_CNTX;
            float cTextYPos = (VIRTUAL_CANVAS_Y * 0.30f) - (VIRTUAL_CANVAS_Y * 0.15f * cPercent);
            StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            Images.Data.rFontMenuSize->DrawTextArgs(cTextXPos, cTextYPos, 0, 3.0f, "%i", mLastDamageTaken);
            StarlightColor::ClearMixer();
        }

        //--HP
        float cTextX = COMBAT_PARTY_CNTX;
        Images.Data.rFontMenuSize->DrawTextArgs(cTextX, cIdealY - 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "HP: %i / %i", mPlayerHP, mPlayerHPMax);
    }

    //--Render enemy graphics.
    float cEnemyMiddleX = -1.0f;
    float cEnemyMiddleY = -1.0f;
    int i = 0;
    CombatEnemyPack *rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->PushIterator();
    while(rEnemyPack)
    {
        //--Compute position.
        if(rEnemyPack->rImage)
        {
            //--Position.
            float cIdealX = COMBAT_ENEMY_CNTX - (rEnemyPack->rImage->GetTrueWidth()  * 0.5f) + (i * VIRTUAL_CANVAS_X * 0.10f);
            float cIdealY = COMBAT_ENEMY_CNTY - (rEnemyPack->rImage->GetTrueHeight() * 0.5f);

            //--Store.
            if(cEnemyMiddleX == -1.0f)
            {
                cEnemyMiddleX = cIdealX + (rEnemyPack->rImage->GetTrueWidth()  * 0.5f);
                cEnemyMiddleY = cIdealY + (rEnemyPack->rImage->GetTrueHeight() * 0.5f);
            }

            //--Enemy is dying.
            if(i == 0 && mTargetDyingTimer > 0)
            {
                //--Fading up to white.
                if(mTargetDyingTimer > ENEMY_DIE_WAIT_TICKS + ENEMY_DIE_FADEOUT_TICKS)
                {
                    //--Set the GL State up for the first stencil pass.
                    glEnable(GL_STENCIL_TEST);
                    glColorMask(true, true, true, true);
                    glDepthMask(true);
                    glStencilFunc(GL_ALWAYS, TL_STENCIL_CODE_COMBAT, 0xFF);
                    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
                    glStencilMask(0xFF);

                    //--Render.
                    if(rEnemyPack->rImage) rEnemyPack->rImage->Draw(cIdealX, cIdealY);

                    //--Now switch to white blending.
                    glColorMask(true, true, true, true);
                    glDepthMask(true);
                    glStencilFunc(GL_EQUAL, TL_STENCIL_CODE_COMBAT, 0xFF);
                    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
                    glStencilMask(0xFF);

                    //--Blend based on the timing.
                    int tUseTimer = mTargetDyingTimer - (ENEMY_DIE_WAIT_TICKS + ENEMY_DIE_FADEOUT_TICKS);
                    float tPercent = 1.0f - EasingFunction::QuadraticOut(tUseTimer, ENEMY_DIE_WHITEOUT_TICKS);
                    glDisable(GL_TEXTURE_2D);
                    glColor4f(1.0f, 1.0f, 1.0f, tPercent);
                    if(rEnemyPack->rImage) rEnemyPack->rImage->Draw(cIdealX, cIdealY);

                    //--Clean up.
                    glEnable(GL_TEXTURE_2D);
                    glDisable(GL_STENCIL_TEST);
                    StarlightColor::ClearMixer();
                }
                //--Fading from white down to black.
                else if(mTargetDyingTimer > ENEMY_DIE_WAIT_TICKS)
                {
                    //--Setup.
                    glEnable(GL_STENCIL_TEST);
                    glColorMask(false, false, false, false);
                    glDepthMask(false);
                    glStencilFunc(GL_ALWAYS, TL_STENCIL_CODE_COMBAT, 0xFF);
                    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
                    glStencilMask(0xFF);

                    //--Render.
                    if(rEnemyPack->rImage) rEnemyPack->rImage->Draw(cIdealX, cIdealY);

                    //--Now switch to rendering mode.
                    glColorMask(true, true, true, true);
                    glDepthMask(true);
                    glStencilFunc(GL_EQUAL, TL_STENCIL_CODE_COMBAT, 0xFF);
                    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
                    glStencilMask(0xFF);

                    //--Render based on the timing.
                    int tUseTimer = mTargetDyingTimer - ENEMY_DIE_WAIT_TICKS;
                    float tPercent = EasingFunction::QuadraticInOut(tUseTimer, ENEMY_DIE_FADEOUT_TICKS);
                    glDisable(GL_TEXTURE_2D);
                    glColor4f(0.0f, 0.0f, 0.0f, tPercent);
                    if(rEnemyPack->rImage) rEnemyPack->rImage->Draw(cIdealX, cIdealY);

                    //--Clean up.
                    glEnable(GL_TEXTURE_2D);
                    glDisable(GL_STENCIL_TEST);
                    StarlightColor::ClearMixer();
                }
                //--Don't render during the cooling off.
                else
                {

                }
            }
            //--Normal render, flashes when damaged.
            else if(rEnemyPack->mFlashTimer % 6 < 3)
            {
                rEnemyPack->rImage->Draw(cIdealX, cIdealY);
            }

            //--Damage Indicator
            if(rEnemyPack->mFlashTimer > 0)
            {
                float cPercent = 1.0f - EasingFunction::QuadraticInOut(rEnemyPack->mFlashTimer, 30.0f);
                float cTextXPos = COMBAT_ENEMY_CNTX + (i * COMBAT_ENEMY_SPCX);
                float cTextYPos = (VIRTUAL_CANVAS_Y * 0.30f) - (VIRTUAL_CANVAS_Y * 0.15f * cPercent);
                StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
                Images.Data.rFontMenuSize->DrawTextArgs(cTextXPos, cTextYPos, 0, 3.0f, "%i", mLastDamageTaken);
                StarlightColor::ClearMixer();
            }

            //--HP
            float cTextX = COMBAT_ENEMY_CNTX + (i * COMBAT_ENEMY_SPCX);
            Images.Data.rFontMenuSize->DrawTextArgs(cTextX, cIdealY - 25.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "HP: %i / %i", rEnemyPack->mHP, rEnemyPack->mHPMax);
        }

        //--Next.
        i ++;
        rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->AutoIterate();
    }

    //--[Combat Display]
    //--Don't render anything during turn changes.
    if((mTurnChangeTimer < COMBAT_TURN_TICKS_DISPLAY) || (mTurnResolveTimer < mTurnResolveTimerMax) || mCombatTransitionMode == TL_COMBAT_MODE_VICTORY)
    {

    }
    else
    {
        //--Show the words around the enemy or player, depending on who is acting.
        mActiveCenterX = cPlayerMiddleX;
        mActiveCenterY = cPlayerMiddleY;
        if(mIsPlayerAttackCase)
        {
            mActiveCenterX = cEnemyMiddleX;
            mActiveCenterY = cEnemyMiddleY;
        }

        //--Render instructions.
        if(mIsPlayerAttackCase)
            Images.Data.rFontMenuHeader->DrawText(VIRTUAL_CANVAS_X * 0.50f, 10.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Type the words to attack!");
        else
            Images.Data.rFontMenuHeader->DrawText(VIRTUAL_CANVAS_X * 0.50f, 10.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Type the words to defend!");

        //--[Time Bar]
        //--Render a bar showing how much time is left in this round.
        float cBarPercent = 1.0f - ((float)mCombatTypeTimer / (float)mCombatTypeTimerMax);
        {
            //--Setup.
            glDisable(GL_TEXTURE_2D);

            //--Backing.
            float cLft = 0.0f;
            float cTop = VIRTUAL_CANVAS_Y * 0.75f;
            float cRgt = VIRTUAL_CANVAS_X;
            float cBot = cTop + 32.0f;
            StarlightColor::SetMixer(1.0f, 1.0f, 0.0f, tAlphaMixer);
            glBegin(GL_QUADS);
                glVertex2f(cLft, cTop);
                glVertex2f(cRgt, cTop);
                glVertex2f(cRgt, cBot);
                glVertex2f(cLft, cBot);
            glEnd();

            //--Bar.
            cLft = 0.0f + 3.0f;
            cTop = VIRTUAL_CANVAS_Y * 0.75f + 3.0f;
            cRgt = (VIRTUAL_CANVAS_X - 6.0f) * cBarPercent;
            cBot = cTop + 32.0f - 6.0f;
            StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, tAlphaMixer);
            glBegin(GL_QUADS);
                glVertex2f(cLft, cTop);
                glVertex2f(cRgt, cTop);
                glVertex2f(cRgt, cBot);
                glVertex2f(cLft, cBot);
            glEnd();

            //--Clean.
            glEnable(GL_TEXTURE_2D);
            StarlightColor::ClearMixer();
        }

        //--Iterate.
        i = 0;
        void *rDummyPtr = mrActiveWordList->PushIterator();
        while(rDummyPtr)
        {
            //--Error check.
            int cWordsTotal = mrActiveWordList->GetListSize();

            //--Constants.
            float cTicks = 15.0f;
            float cMaxExtension = 150.0f;
            float cDegPerWord = 100.0f;
            if(cWordsTotal > 0) cDegPerWord = 360.0f / (float)cWordsTotal * TORADIAN;

            //--Resolve position.
            float cPercent = EasingFunction::QuadraticInOut(mWordExplodeTimer, cTicks);
            float cExtension = cPercent * cMaxExtension;
            if(cExtension > cMaxExtension) cExtension = cMaxExtension;
            float tXPos = mActiveCenterX + (cosf(mWordAngleOffset + (i * cDegPerWord)) * cExtension);
            float tYPos = mActiveCenterY + (sinf(mWordAngleOffset + (i * cDegPerWord)) * cExtension);

            //--Resolve scale.
            float cScale = (cPercent * 0.5f) + 0.5f;
            if(cScale < 0.5f) cScale = 0.5f;
            if(cScale > 1.0f) cScale = 1.0f;

            //--Render the word.
            const char *rWord = mrActiveWordList->GetIteratorName();
            float cLft = tXPos - ((Images.Data.rFontMenuHeader->GetTextWidth(rWord) * 0.5f) * cScale);

            //--Backing.
            float cBrd = 27.0f;
            float cBkLft = cLft - cBrd;
            float cBkTop = tYPos - cBrd + 7.0f;
            float cBkRgt = cBkLft + cBrd + cBrd + (Images.Data.rFontMenuHeader->GetTextWidth(rWord) * cScale);
            float cBkBot = cBkTop + cBrd + cBrd - 7.0f;
            RenderWordBorderCard(cBkLft, cBkTop, cBkRgt, cBkBot);

            //--If this is the active word:
            if(mCombatActiveWord == i)
            {
                //--Iterate across the letters.
                StarlightColor::SetMixer(0.0f, 0.0f, 1.0f, tAlphaMixer);
                for(int p = 0; p < mCombatActiveLetter; p ++)
                {
                    cLft = cLft + Images.Data.rFontMenuHeader->GetKerningBetween(rWord[p], rWord[p+1]);
                }
                for(int p = mCombatActiveLetter; p < (int)strlen(rWord); p ++)
                {
                    cLft = cLft + Images.Data.rFontMenuHeader->DrawLetter(cLft, tYPos, SUGARFONT_AUTOCENTER_Y, cScale, rWord[p], rWord[p+1]);
                    StarlightColor::ClearMixer();
                }
            }
            //--Otherwise, render it normally.
            else
            {
                Images.Data.rFontMenuHeader->DrawText(cLft, tYPos, SUGARFONT_AUTOCENTER_Y, cScale, rWord);
            }

            //--Next.
            i ++;
            rDummyPtr = mrActiveWordList->AutoIterate();
        }
    }

    //--[Flying Letters]
    FlyingLetterPack *rFlyingLetterPack = (FlyingLetterPack *)mFlyingLetterList->PushIterator();
    while(rFlyingLetterPack)
    {
        //--Compute position.
        float cPercent = EasingFunction::QuadraticInOut(rFlyingLetterPack->mTimer, rFlyingLetterPack->mTimerMax);
        float cRenderX = rFlyingLetterPack->mX + ((rFlyingLetterPack->mXTarget - rFlyingLetterPack->mX) * cPercent);
        float cRenderY = rFlyingLetterPack->mY + ((rFlyingLetterPack->mYTarget - rFlyingLetterPack->mY) * cPercent);
        Images.Data.rFontMenuHeader->DrawLetter(cRenderX, cRenderY, SUGARFONT_AUTOCENTER_XY, 1.0f, rFlyingLetterPack->mLetter);

        //--Next.
        rFlyingLetterPack = (FlyingLetterPack *)mFlyingLetterList->AutoIterate();
    }

    //--[Turn Change Overlay]
    //--Turn results.
    if(mTurnResolveTimer < mTurnResolveTimerMax && mCombatTransitionMode != TL_COMBAT_MODE_VICTORY)
    {
        //--Variables.
        float cXPos = COMBAT_PARTY_CNTX;
        float tYPos = VIRTUAL_CANVAS_Y * 0.25f;

        //--Position it over the enemy if the player is defending.
        if(!mIsPlayerAttackCase) cXPos = COMBAT_ENEMY_CNTX;

        //--Offsets.
        float cCenteringOffset = (Images.Data.rFontMenuHeader->GetTextWidth("Results:") * 2.0f * 0.5f);
        cXPos = cXPos - cCenteringOffset;

        //--Backing.
        float cBrd = 27.0f;
        float cLft = cXPos - cBrd;
        float cTop = tYPos - cBrd;
        float cRgt = cLft + cBrd + cBrd + (Images.Data.rFontMenuHeader->GetTextWidth("Results:") * 2.0f);
        float cBot = cTop + cBrd + cBrd + (Images.Data.rFontMenuHeader->GetTextHeight() * 2.0f) + (Images.Data.rFontMenuCommand->GetTextHeight() * (mrCompletedWordList->GetListSize() + 3));
        WorldDialogue::RenderDialogueBorderCard(Images.Data.rBorderCard, cLft, cTop, cRgt, cBot);

        //--Results indicator. Always renders.
        Images.Data.rFontMenuHeader->DrawText(cXPos, tYPos, 0, 2.0f, "Results:");
        tYPos = tYPos + Images.Data.rFontMenuHeader->GetTextHeight() * 2.0f;

        //--For each word result, display it.
        if(mTurnResolveTimer >= COMBAT_RESULT_TICKS_INTRO)
        {
            //--Compute.
            int tUseTimer = mTurnResolveTimer - COMBAT_RESULT_TICKS_INTRO;
            int tResultsToShow = tUseTimer / COMBAT_RESULT_TICKS_PERWORD;

            //--Iterate.
            for(int i = 0; i < tResultsToShow; i ++)
            {
                //--Get the word.
                const char *rWord = mrCompletedWordList->GetNameOfElementBySlot(i);
                if(!rWord) break;

                //--Render it and the bonus for it.
                if(mIsPlayerAttackCase)
                {
                    Images.Data.rFontMenuCommand->DrawTextArgs(cXPos, tYPos, 0, 1.0f, "%s: +30%% + %i%%", rWord, (int)strlen(rWord) * 10);
                }
                else
                {
                    Images.Data.rFontMenuCommand->DrawTextArgs(cXPos, tYPos, 0, 1.0f, "%s: +15%% + %i%%", rWord, (int)strlen(rWord) * 5);
                }
                tYPos = tYPos + Images.Data.rFontMenuCommand->GetTextHeight() * 1.0f;
            }
        }

        //--Execute damage, show the tally.
        if(mTurnResolveTimer >= COMBAT_RESULT_TICKS_INTRO + (COMBAT_RESULT_TICKS_PERWORD * mrCompletedWordList->GetListSize()) + COMBAT_RESULT_TICKS_WAIT)
        {
            //--Move down a bit for offsetting.
            tYPos = tYPos + Images.Data.rFontMenuCommand->GetTextHeight() * 1.0f;

            //--Render. The string will be prepared ahead of time.
            if(mLastDamageStringA && mLastDamageStringB)
            {
                Images.Data.rFontMenuCommand->DrawText(cXPos, tYPos, 0, 1.0f, mLastDamageStringA);
                tYPos = tYPos + Images.Data.rFontMenuCommand->GetTextHeight() * 1.0f;
                Images.Data.rFontMenuCommand->DrawText(cXPos, tYPos, 0, 1.0f, mLastDamageStringB);
                tYPos = tYPos + Images.Data.rFontMenuCommand->GetTextHeight() * 1.0f;
            }
        }
    }
    //--Indicates what you should do (attack/defend) and gives a brief cooldown between cycles.
    else if(mTurnChangeTimer < COMBAT_TURN_TICKS_DISPLAY)
    {
        //--Percentage of fade.
        float tAlpha = 0.65f;
        if(mTurnChangeTimer < COMBAT_TURN_TICKS_DISPLAY * 0.25f)
        {
            tAlpha = mTurnChangeTimer / (COMBAT_TURN_TICKS_DISPLAY * 0.25f) * 0.65f;
        }
        else if(mTurnChangeTimer > COMBAT_TURN_TICKS_DISPLAY * 0.75f)
        {
            tAlpha = 0.65f - ((mTurnChangeTimer - (COMBAT_TURN_TICKS_DISPLAY * 0.75f)) / (COMBAT_TURN_TICKS_DISPLAY * 0.25f) * 0.65f);
        }

        //--Backing.
        SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, tAlpha));

        //--Timer Constants.
        float cTimers[4];
        cTimers[0] = COMBAT_TURN_TICKS_DISPLAY * 0.15f;
        cTimers[1] = COMBAT_TURN_TICKS_DISPLAY * 0.25f;
        cTimers[2] = COMBAT_TURN_TICKS_DISPLAY * 0.75f;
        cTimers[3] = COMBAT_TURN_TICKS_DISPLAY * 0.85f;

        //--In the middle, show this text.
        if(mTurnChangeTimer >= cTimers[0] && mTurnChangeTimer <= cTimers[3])
        {
            //--Compute position.
            float cXPosition = 0.0f;

            //--Position Constants.
            float cXDestination[4];
            cXDestination[0] = (VIRTUAL_CANVAS_X * 1.3f);
            cXDestination[1] = (VIRTUAL_CANVAS_X * 0.52f);
            cXDestination[2] = (VIRTUAL_CANVAS_X * 0.48f);
            cXDestination[3] = (VIRTUAL_CANVAS_X * -1.30f);

            //--First slide-in.
            if(mTurnChangeTimer <= cTimers[1])
            {
                float tPercent = EasingFunction::QuadraticIn(mTurnChangeTimer - cTimers[0], cTimers[1] - cTimers[0]);
                cXPosition = cXDestination[0] + ((cXDestination[1] - cXDestination[0]) * tPercent);
            }
            //--Stall in the middle.
            else if(mTurnChangeTimer <= cTimers[2])
            {
                float tPercent = EasingFunction::Linear(mTurnChangeTimer - cTimers[1], cTimers[2] - cTimers[1]);
                cXPosition = cXDestination[1] + ((cXDestination[2] - cXDestination[1]) * tPercent);
            }
            //--Accelerate to the edges.
            else if(mTurnChangeTimer <= cTimers[3])
            {
                float tPercent = EasingFunction::QuadraticOut(mTurnChangeTimer - cTimers[2], cTimers[3] - cTimers[2]);
                cXPosition = cXDestination[2] + ((cXDestination[3] - cXDestination[2]) * tPercent);
            }

            //--Whatever the computed position, render this string.
            if(mIsPlayerAttackCase)
            {
                Images.Data.rFontMenuHeader->DrawText(cXPosition, VIRTUAL_CANVAS_Y * 0.50f, SUGARFONT_AUTOCENTER_XY, 3.0f, "Attack Phase");
            }
            else
            {
                Images.Data.rFontMenuHeader->DrawText(cXPosition, VIRTUAL_CANVAS_Y * 0.50f, SUGARFONT_AUTOCENTER_XY, 3.0f, "Defend Phase");
            }
        }
    }

    //--[Victory]
    if(mCombatTransitionMode == TL_COMBAT_MODE_VICTORY)
    {
        //--Compute display percentage.
        float tPercent = mCombatTransitionTimer / (float)TL_VICTORY_TIME_BIGBANNER_IN;

        //--Compute alpha.
        float tAlpha = 1.0f;
        if(mCombatTransitionTimer > TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)
        {
            tAlpha = 1.0f - ((mCombatTransitionTimer - (TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)) / (float)TL_VICTORY_TIME_BIGBANNER_FADE);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);
        }

        //--Subroutine render.
        //SugarBitmap::DrawHorizontalPercent(Images.Victory.rBannerBig, tPercent);

        //--At certain points, render letters:
        char tBuffer[32];
        float cTimeStart   = 0.30f;
        float cTimeSpacing = 0.08f;
        if(tPercent >= cTimeStart + (cTimeSpacing * 0.0f))
        {
            strcpy(tBuffer, "V");
            if(tPercent >= cTimeStart + (cTimeSpacing * 1.0f)) strcat(tBuffer, "I");
            if(tPercent >= cTimeStart + (cTimeSpacing * 2.0f)) strcat(tBuffer, "C");
            if(tPercent >= cTimeStart + (cTimeSpacing * 3.0f)) strcat(tBuffer, "T");
            if(tPercent >= cTimeStart + (cTimeSpacing * 4.0f)) strcat(tBuffer, "O");
            if(tPercent >= cTimeStart + (cTimeSpacing * 5.0f)) strcat(tBuffer, "R");
            if(tPercent >= cTimeStart + (cTimeSpacing * 6.0f)) strcat(tBuffer, "Y");
            if(tPercent >= cTimeStart + (cTimeSpacing * 7.0f)) strcat(tBuffer, "!");
            Images.Data.rFontMenuHeader->DrawText(180.0f, 250.0f, 0, 4.0f, tBuffer);
        }
    }
    //--[Defeat]
    //--Uses the same timers but displays DEFEAT instead.
    else if(mCombatTransitionMode == TL_COMBAT_MODE_DEFEAT)
    {
        //--Compute display percentage.
        float tPercent = mCombatTransitionTimer / (float)TL_VICTORY_TIME_BIGBANNER_IN;

        //--Compute alpha.
        float tAlpha = 1.0f;
        if(mCombatTransitionTimer > TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)
        {
            tAlpha = 1.0f - ((mCombatTransitionTimer - (TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)) / (float)TL_VICTORY_TIME_BIGBANNER_FADE);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);
        }

        //--Subroutine render.
        //SugarBitmap::DrawHorizontalPercent(Images.Victory.rBannerBig, tPercent);

        //--At certain points, render letters:
        char tBuffer[32];
        float cTimeStart   = 0.30f;
        float cTimeSpacing = 0.08f;
        if(tPercent >= cTimeStart + (cTimeSpacing * 0.0f))
        {
            strcpy(tBuffer, "D");
            if(tPercent >= cTimeStart + (cTimeSpacing * 1.0f)) strcat(tBuffer, "E");
            if(tPercent >= cTimeStart + (cTimeSpacing * 2.0f)) strcat(tBuffer, "F");
            if(tPercent >= cTimeStart + (cTimeSpacing * 3.0f)) strcat(tBuffer, "E");
            if(tPercent >= cTimeStart + (cTimeSpacing * 4.0f)) strcat(tBuffer, "A");
            if(tPercent >= cTimeStart + (cTimeSpacing * 5.0f)) strcat(tBuffer, "T");
            if(tPercent >= cTimeStart + (cTimeSpacing * 6.0f)) strcat(tBuffer, "!");
            Images.Data.rFontMenuHeader->DrawText(190.0f, 250.0f, 0, 4.0f, tBuffer);
        }
    }

    //--Clean.
    StarlightColor::ClearMixer();
    glTranslatef(0.0f, 0.0f, -TL_DEPTH_GUI);
}
void TypingCombat::RenderWordBorderCard(float pLft, float pTop, float pRgt, float pBot)
{
    //--Render a border card around words to help them stand out from the backing.
    if(!Images.mIsReady) return;

    //--Texture sizings.
    float cTxLL =  0.0f / 45.0f;
    float cTxLR =  8.0f / 45.0f;
    float cTxRL = 37.0f / 45.0f;
    float cTxRR = 45.0f / 45.0f;
    float cTxTT =  0.0f / 45.0f;
    float cTxTB =  8.0f / 45.0f;
    float cTxBT = 37.0f / 45.0f;
    float cTxBB = 45.0f / 45.0f;
    float cWid = 8.0f;
    float cHei = 8.0f;

    //--Border card.
    Images.Data.rWordBorderCard->Bind();
    glBegin(GL_QUADS);

        //--[Top Vertically]
        //--Top left.
        glTexCoord2f(cTxLL, cTxTT); glVertex2f(pLft + (cWid * 0.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxLR, cTxTT); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxLR, cTxTB); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxLL, cTxTB); glVertex2f(pLft + (cWid * 0.0f), pTop + (cHei * 1.0f));

        //--Top bar.
        glTexCoord2f(cTxLR, cTxTT); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxRL, cTxTT); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxRL, cTxTB); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxTB); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 1.0f));

        //--Top right.
        glTexCoord2f(cTxRL, cTxTT); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxRR, cTxTT); glVertex2f(pRgt - (cWid * 0.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxRR, cTxTB); glVertex2f(pRgt - (cWid * 0.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxTB); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 1.0f));

        //--[Middle Vertically]
        //--Middle left.
        glTexCoord2f(cTxLL, cTxTB); glVertex2f(pLft + (cWid * 0.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxTB); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxBT); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxLL, cTxBT); glVertex2f(pLft + (cWid * 0.0f), pBot - (cHei * 1.0f));

        //--Middle bar.
        glTexCoord2f(cTxLR, cTxTB); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxTB); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxBT); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxBT); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 1.0f));

        //--Middle right.
        glTexCoord2f(cTxRL, cTxTB); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxRR, cTxTB); glVertex2f(pRgt - (cWid * 0.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxRR, cTxBT); glVertex2f(pRgt - (cWid * 0.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxBT); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 1.0f));

        //--[Bottom Vertically]
        //--Bottom left.
        glTexCoord2f(cTxLL, cTxBT); glVertex2f(pLft + (cWid * 0.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxBT); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxLR, cTxBB); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 0.0f));
        glTexCoord2f(cTxLL, cTxBB); glVertex2f(pLft + (cWid * 0.0f), pBot - (cHei * 0.0f));

        //--Bottom bar.
        glTexCoord2f(cTxLR, cTxBT); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxBT); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxRL, cTxBB); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 0.0f));
        glTexCoord2f(cTxLR, cTxBB); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 0.0f));

        //--Bottom right.
        glTexCoord2f(cTxRL, cTxBT); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxRR, cTxBT); glVertex2f(pRgt - (cWid * 0.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxRR, cTxBB); glVertex2f(pRgt - (cWid * 0.0f), pBot - (cHei * 0.0f));
        glTexCoord2f(cTxRL, cTxBB); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 0.0f));

    glEnd();
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
