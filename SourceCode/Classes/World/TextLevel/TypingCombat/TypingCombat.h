//--[TypingCombat]
//--Combat used in TextLevel mode which involves the player typing words to attack and defend.
//  Deprecated, might get revived at some point in the future.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
#include "TextLevelStructures.h"

//--[Classes]
class TypingCombat
{
    private:
    //--System
    int mCombatResolution;
    StringEntry *mLocalStringEntry;

    //--Properties
    bool mWaitForKeypressBeforeTimerStarts;
    int mDifficulty;

    //--Timers
    int mCombatTransitionTimer;
    int mCombatTransitionMode;
    int mTurnChangeTimer;
    int mTurnResolveTimer;
    int mTurnResolveTimerMax;
    int mLastDamageTaken;

    //--Enemy List
    SugarLinkedList *mCombatEnemyList; //CombatEnemyPack

    //--Combat Ending
    int mEndingPhase;
    int mTargetDyingTimer;

    //--Player Statistics
    int mPlayerFlashTimer;
    int mPlayerHP;
    int mPlayerHPMax;
    int mPlayerAtk;
    int mPlayerDef;
    int mPlayerLettersCurrent;
    int mPlayerLettersPerHit;
    int mPlayerStumbleTimer;
    SugarBitmap *rPlayerCombatImage;
    char *mLastDamageStringA;
    char *mLastDamageStringB;

    //--Combat Text Pool
    bool mIsPlayerAttackCase;
    bool mIsTimerStarted;
    int mCombatActiveWord;
    int mCombatActiveLetter;
    float mTotalWordPower;
    int mCombatTypeTimer;
    int mWordExplodeTimer;
    int mCombatTypeTimerMax;
    float mWordAngleOffset;
    float mActiveCenterX;
    float mActiveCenterY;
    SugarLinkedList *mCombatAttackWords; //DummyPtr
    SugarLinkedList *mCombatDefendWords; //DummyPtr
    SugarLinkedList *mrActiveWordList; //DummyPtr, Reference
    SugarLinkedList *mrCompletedWordList;
    SugarLinkedList *mFlyingLetterList; //FlyingLetterPack

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            SugarFont *rFontMenuHeader;
            SugarFont *rFontMenuSize;
            SugarFont *rFontMenuCommand;

            //--UI Parts
            SugarBitmap *rWordBorderCard;
            SugarBitmap *rBorderCard;
            SugarBitmap *rMapParts;
            SugarBitmap *rScrollbar;
            SugarBitmap *rPopupBorderCard;
            SugarBitmap *rTextInput;
        }Data;
    }Images;

    protected:

    public:
    //--System
    TypingCombat();
    ~TypingCombat();
    void Initialize();

    //--Public Variables
    //--Property Queries
    int GetPlayerHP();
    int GetCombatResolution();
    float ComputeLetterX(int pActiveWord, int pActiveLetter);
    float ComputeLetterY(int pActiveWord, int pActiveLetter);

    //--Manipulators
    void SetDifficulty(int pDifficulty);
    void RegisterAttackWord(const char *pWord);
    void RegisterDefendWord(const char *pWord);
    void SetPlayerStats(int pHP, int pHPMax, int pAtk, int pDef, int pLettersPerHit, const char *pImagePath);
    void RegisterEnemy(int pHP, int pAtk, int pDef, const char *pImagePath);

    //--Core Methods
    void BeginPlayerAttackTurn();
    void BeginPlayerDefendTurn();
    void SelectCombatWordsFromList(SugarLinkedList *pWordList);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();
    void RenderWordBorderCard(float pLft, float pTop, float pRgt, float pBot);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

