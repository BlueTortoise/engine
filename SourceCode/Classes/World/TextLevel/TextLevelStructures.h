//--[Text Level Structures]
//--Structures used by the TextLevel. Duh.
#pragma once
#include "Definitions.h"
#include "Structures.h"
#include "TiledLevel.h"

//--[Definitions]
//--Instruction Handlers
#define TEXT_INSTRUCTION_ERROR 0
#define TEXT_INSTRUCTION_BLOCK 1
#define TEXT_INSTRUCTION_APPEND 2
#define TEXT_INSTRUCTION_REGISTERIMAGE 3
#define TEXT_INSTRUCTION_UNREGISTERIMAGE 4
#define TEXT_INSTRUCTION_DEFAULTIMAGE 5
#define TEXT_INSTRUCTION_EXECSCRIPT 6
#define TEXT_INSTRUCTION_SFX 7
#define TEXT_INSTRUCTION_FADEOUT 8
#define TEXT_INSTRUCTION_FADEIN 9

//--Navigation Flags
#define TL_NAV_LOOK 0x0001
#define TL_NAV_NORTH 0x0002
#define TL_NAV_UP 0x0004
#define TL_NAV_WEST 0x0008
#define TL_NAV_WAIT 0x0010
#define TL_NAV_EAST 0x0020
#define TL_NAV_THINK 0x0040
#define TL_NAV_SOUTH 0x0080
#define TL_NAV_DOWN 0x0100
#define TL_NAV_SHOWNOTHING 0x0200

//--Navigation Indices
#define TL_NAV_INDEX_LOOK 0
#define TL_NAV_INDEX_NORTH 1
#define TL_NAV_INDEX_UP 2
#define TL_NAV_INDEX_WEST 3
#define TL_NAV_INDEX_WAIT 4
#define TL_NAV_INDEX_EAST 5
#define TL_NAV_INDEX_THINK 6
#define TL_NAV_INDEX_SOUTH 7
#define TL_NAV_INDEX_DOWN 8
#define TL_NAV_INDEX_NORMAL_TOTAL 9
#define TL_NAV_INDEX_ZOOMUP 9
#define TL_NAV_INDEX_ZOOMDN 10
#define TL_NAV_INDEX_TOTAL 11

//--Locality Window
#define TL_LOCALITY_LINES_MAX 14

//--Player Display
#define TL_PLAYER_LAYERS_MAX 8
#define TL_PLAYER_TRANSITION_TICKS 15.0f

//--Command Listing
#define TL_COMMAND_LIST_TICKS 15.0f
#define TL_COMMAND_ARROW_X 820.0f
#define TL_COMMAND_ARROW_Y   3.0f
#define TL_COMMAND_ARROW_W  20.0f
#define TL_COMMAND_ARROW_H  41.0f
#define TL_COMMAND_ARROW_SCROLL 189.0f

//--Zoom Scales
#define TL_SCALES_TOTAL 9

//--Input Log
#define TL_INPUT_MAX_LOG 50

//--Connectivity Flags.
#define TL_CONNECT_N 0x0001
#define TL_CONNECT_E 0x0002
#define TL_CONNECT_S 0x0004
#define TL_CONNECT_W 0x0008
#define TL_CONNECT_NW 0x0010
#define TL_CONNECT_NE 0x0020
#define TL_CONNECT_SE 0x0040
#define TL_CONNECT_SW 0x0080
#define TL_CONNECT_UP 0x0100
#define TL_CONNECT_DN 0x0200

//--[Combat Handlers]
//--Difficulties.
#define TL_DIFFICULTY_EASY 0
#define TL_DIFFICULTY_NORMAL 1
#define TL_DIFFICULTY_HARD 2

//--Combat flags.
#define TL_COMBAT_MODE_NONE -1
#define TL_COMBAT_MODE_FADEIN 0
#define TL_COMBAT_MODE_BATTLE 1
#define TL_COMBAT_MODE_VICTORY 2
#define TL_COMBAT_MODE_DEFEAT 3

#define TL_COMBAT_ENDING_NONE -1
#define TL_COMBAT_ENDING_VICTORY_JINGLE 0

//--Resolution Flags
#define TL_COMBAT_UNRESOLVED 0
#define TL_COMBAT_TURN_HAS_ENDED 1
#define TL_COMBAT_PLAYER_WINS 2
#define TL_COMBAT_PLAYER_DEFEATED 3

//--Combat victory timers.
#define TL_VICTORY_TIME_BIGBANNER_IN 35
#define TL_VICTORY_TIME_BIGBANNER_HOLD 30
#define TL_VICTORY_TIME_BIGBANNER_FADE 45
#define TL_VICTORY_TIME_TOTAL (TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD + TL_VICTORY_TIME_BIGBANNER_FADE)

//--[Combat Constants]
//--Transitions
#define COMBAT_TRANSITION_TICKS 90

//--Difficulty values for Typing Combat
#define COMBAT_TURN_TICKS_EASY 330
#define COMBAT_TURN_TICKS_NORMAL 210
#define COMBAT_TURN_TICKS_HARD 210
#define COMBAT_TURN_TICKS_DISPLAY 90

//--Timing Constants for Typing Combat
#define COMBAT_RESULT_TICKS_INTRO 15
#define COMBAT_RESULT_TICKS_PERWORD 25
#define COMBAT_RESULT_TICKS_WAIT 35
#define COMBAT_RESULT_TICKS_DAMAGETOTAL 45
#define COMBAT_RESULT_TICKS_OUTTRO 45

//--Positions for Typing Combat
#define COMBAT_PARTY_CNTX (VIRTUAL_CANVAS_X * 0.25f)
#define COMBAT_PARTY_CNTY (VIRTUAL_CANVAS_Y * 0.50f)
#define COMBAT_ENEMY_CNTX (VIRTUAL_CANVAS_X * 0.75f)
#define COMBAT_ENEMY_CNTY (VIRTUAL_CANVAS_Y * 0.50f)
#define COMBAT_ENEMY_SPCX (VIRTUAL_CANVAS_X * 0.10f)

//--Ace Timer for Word Combat
#define WC_ACE_TICKS 1200

//--Timer for Potions
#define WC_HEALING_TICKS_PER_HEART 15

//--How enemies flash when dying
#define ENEMY_DIE_WHITEOUT_TICKS 10
#define ENEMY_DIE_FADEOUT_TICKS 10
#define ENEMY_DIE_WAIT_TICKS 20
#define ENEMY_DIE_TICKS_TOTAL (ENEMY_DIE_WHITEOUT_TICKS + ENEMY_DIE_FADEOUT_TICKS + ENEMY_DIE_WAIT_TICKS)

//--How enemies flash when attacking.
#define WC_TICKS_PER_FLASH 4
#define WC_TICKS_PER_SEQUENCE (WC_TICKS_PER_FLASH * 5)

//--[Rendering Codes]
//--Constants.
#define TL_TIME_PER_LINE 3
#define TL_ENDING_TICKS 60

#define TL_COMBAT_INVALID -1
#define TL_COMBAT_NONE 0
#define TL_COMBAT_TYPING 1
#define TL_COMBAT_WORD 2

//--Codes.
#define TL_STENCIL_CODE_TEXT 2
#define TL_STENCIL_CODE_PORTRAIT 3
#define TL_STENCIL_CODE_LOCALITY 4
#define TL_STENCIL_CODE_COMBAT 5
#define TL_STENCIL_CODE_MAP 11
#define TL_STENCIL_CODE_OVERLAY 10

//--Depths.
#define TL_DEPTH_ENTITIES -0.490000f
#define TL_DEPTH_ENTITIES_PER_Y 0.000001f
#define TL_DEPTH_ROOM -0.600000f
#define TL_DEPTH_ROOM_RANGE 0.050000f
#define TL_DEPTH_CONNECTION -0.500001f
#define TL_DEPTH_BACKGROUND -0.500002f
#define TL_DEPTH_GUI 0.000000f

//--[Map Definitions]
#define MAP_PART_ROOM 0
#define MAP_PART_UPSTAIR 1
#define MAP_PART_DNSTAIR 2
#define MAP_PART_PLAYER 3
#define MAP_PART_HOSTILE 4
#define MAP_PART_FRIENDLY 5
#define MAP_PART_EXAMINABLE 6
#define MAP_PART_ITEM 7
#define MAP_PART_CONNECT_N 8
#define MAP_PART_CONNECT_E 9
#define MAP_PART_CONNECT_S 10
#define MAP_PART_CONNECT_W 11
#define MAP_PART_CONNECT_NW 12
#define MAP_PART_CONNECT_NE 13
#define MAP_PART_CONNECT_SE 14
#define MAP_PART_CONNECT_SW 15
#define MAP_PARTS_TOTAL 16

//--[Elemental Flags]
//--There are 7 elements. "None" is the physical element.
#define TL_ELEMENT_NONE 0x00
#define TL_ELEMENT_FIRE 0x01
#define TL_ELEMENT_WATER 0x02
#define TL_ELEMENT_WIND 0x04
#define TL_ELEMENT_EARTH 0x08
#define TL_ELEMENT_LIFE 0x10
#define TL_ELEMENT_DEATH 0x20

//--Slots of the elements.
#define TL_ELEMENT_SLOT_PHYSICAL 0
#define TL_ELEMENT_SLOT_FIRE 1
#define TL_ELEMENT_SLOT_WATER 2
#define TL_ELEMENT_SLOT_WIND 3
#define TL_ELEMENT_SLOT_EARTH 4
#define TL_ELEMENT_SLOT_LIFE 5
#define TL_ELEMENT_SLOT_DEATH 6
#define TL_ELEMENTS_TOTAL 7

//--[Structures]
//--Text Package. Stores text strings plus rendering data, if needed.
typedef struct TextPack
{
    //--Variables
    char *mText;
    int mRenderTimer;

    //--Functions
    void Initialize()
    {
        mText = NULL;
        mRenderTimer = 0;
    }
    static void DeleteThis(void *pPtr)
    {
        TextPack *rPtr = (TextPack *)pPtr;
        free(rPtr->mText);
    }
}TextPack;

//--Stores image data. Images are displayed behind the text boxes.
typedef struct TextImagePack
{
    //--Variables
    char mLocalName[80];
    SugarBitmap *rImage;
    int mLayer;

    //--Functions
    void Initialize()
    {
        mLocalName[0] = '\0';
        rImage = NULL;
        mLayer = 0;
    }
}TextImagePack;

//--Stores a script execution instruction, including arguments.
#define TSEP_MAXARGS 100
typedef struct TextScriptExecPack
{
    //--Variables
    char mScriptPath[256];
    int mArgsTotal;
    char mArguments[TSEP_MAXARGS][256];

    //--Functions
    void Initialize()
    {
        mScriptPath[0] = '\0';
        mArgsTotal = 0;
        for(int i = 0; i < TSEP_MAXARGS; i ++) mArguments[i][0] = '\0';
    }
    void SetPath(const char *pPath)
    {
        if(!pPath) return;
        strcpy(mScriptPath, pPath);
    }
    void AddArgument(const char *pArgument)
    {
        if(mArgsTotal >= TSEP_MAXARGS) return;
        strcpy(mArguments[mArgsTotal], pArgument);
        mArgsTotal ++;
    }
}TextScriptExecPack;

//--Instruction handler. Bypasses the normal CutsceneManager and handles everything internally
//  to prevent possible conflicts with adventure mode.
typedef struct TextInstructionPack
{
    //--Variables
    int mTypeCode;
    void *mDataBlock;

    //--Functions
    void Initialize()
    {
        mTypeCode = TEXT_INSTRUCTION_ERROR;
        mDataBlock = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        TextInstructionPack *rPtr = (TextInstructionPack *)pPtr;
        free(rPtr->mDataBlock);
        free(rPtr);
    }
}TextInstructionPack;

//--Fade Codes
#define AM_FADE_NONE 0
#define AM_FADE_LINE_N 1
#define AM_FADE_LINE_E 2
#define AM_FADE_LINE_S 3
#define AM_FADE_LINE_W 4
#define AM_FADE_CORN_NW 5
#define AM_FADE_CORN_NE 6
#define AM_FADE_CORN_SE 7
#define AM_FADE_CORN_SW 8
#define AM_FADE_TWOLINE_V 9
#define AM_FADE_TWOLINE_H 10
#define AM_FADE_ALLEY_N 11
#define AM_FADE_ALLEY_E 12
#define AM_FADE_ALLEY_S 13
#define AM_FADE_ALLEY_W 14
#define AM_FADE_FULL 16
#define AM_FADE_OVER 17
#define AM_FADE_FULLBLACK 18

//--Inset flags
#define AM_FADE_INSET_NONE 0x00
#define AM_FADE_INSET_NW 0x01
#define AM_FADE_INSET_NE 0x02
#define AM_FADE_INSET_SE 0x04
#define AM_FADE_INSET_SW 0x08

//--Automap Package. Shows a room and connections.
#include "SugarLinkedList.h"
#define AM_NO_MOD 0
#define AM_HFLIP 0x01
#define AM_VFLIP 0x02
#define AM_ROTATE90 0x04
#define AM_ROTATE180 0x08
#define AM_ROTATE270 0x10
#define AM_HALFSIZE 0x20
#define AMP_MAX_RENDER_OBJECTS 32

//--Room Slot Offsets
#define AM_SLOT_NW 0
#define AM_SLOT_NE 1
#define AM_SLOT_SW 2
#define AM_SLOT_SE 3
#define AM_SLOT_DOOR_S_CLOSED 4
#define AM_SLOT_DOOR_S_OPEN 5
#define AM_SLOT_DOOR_E_CLOSED 6
#define AM_SLOT_DOOR_E_OPEN 7

//--Fading Timer
#define AM_FADE_TICKS 15

//--Priority Flags
#define AMTL_PRIORITY_SOUND -2
#define AMTL_PRIORITY_ENEMY -1
#define AMTL_PRIORITY_PLAYER 0
#define AMTL_PRIORITY_FRIENDLY 1
#define AMTL_PRIORITY_ITEM 2

//--Rendering Properties
#define AM_RENDER_BELOW 0x0001
#define AM_RENDER_TILE 0x0002
#define AM_RENDER_INDICATORS 0x0004
#define AM_RENDER_ANIMATIONS 0x0008
#define AM_RENDER_FADES 0x0010
#define AM_RENDER_DOORS 0x0020
#define AM_RENDER_ALL (AM_RENDER_TILE | AM_RENDER_INDICATORS | AM_RENDER_ANIMATIONS | AM_RENDER_FADES)

//--Visibility Overrides
#define OVERRIDE_VISIBILITY_NONE 0
#define OVERRIDE_VISIBILITY_ALWAYS 1
#define OVERRIDE_VISIBILITY_ALWAYSNARROW 2
#define OVERRIDE_VISIBILITY_SHORT 3
#define OVERRIDE_VISIBILITY_NEVER 4

//--Move Speeds
#define APTL_MOVE_TICKS 15

typedef struct AutomapPackTilemapLayer
{
    //--Common
    int mX;
    int mY;
    int mSlotOffset;
    uint8_t mRotationFlags;

    //--Entity-Indicators Only
    bool mInstantMoveToDest;
    int mPriority; //Indicates which "team" the entity is on. 0 is the player.
    int mMoveTimer;
    int mMoveTimerMax;
    float mXRender;
    float mYRender;
    float mZRender;
    float mARender;

    //--Entity Movement
    float mXStart;
    float mYStart;
    float mAStart;
    float mXTarget;
    float mYTarget;
    float mATarget;
    void *rPreviousRoom;

    //--Hostility
    bool mHasHostilityMarker;
    int mHostilityX;
    int mHostilityY;

    //--Rendering Data
    TwoDimensionReal mTxDim;
    TwoDimensionReal mHsDim;

    //--Functions
    void Initialize()
    {
        //--Common
        mX = 0;
        mY = 0;
        mSlotOffset = 0;
        mRotationFlags = 0;

        //--Entity-Indicators Only
        mInstantMoveToDest = false;
        mPriority = 0;
        mMoveTimer = 1;
        mMoveTimerMax = 1;
        mXRender = 0.0f;
        mYRender = 0.0f;
        mZRender = 0.0f;
        mARender = 1.0f;

        //--Entity Movement
        mXStart = 0.0f;
        mYStart = 0.0f;
        mAStart = 0.0f;
        mXTarget = 0.0f;
        mYTarget = 0.0f;
        mATarget = 0.0f;
        rPreviousRoom = NULL;

        //--Hostility
        mHasHostilityMarker = false;
        mHostilityX = 0;
        mHostilityY = 0;

        //--Rendering Data
        mTxDim.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
        mHsDim.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
    }
    void SetPositionTarget(float pX, float pY, float pA)
    {
        mMoveTimer = 0;
        mMoveTimerMax = APTL_MOVE_TICKS;
        mXStart = mXRender;
        mYStart = mYRender;
        mAStart = mARender;
        mXTarget = pX;
        mYTarget = pY;
        mATarget = pA;
    }
}AutomapPackTilemapLayer;

//--Represents a room on the automap. Can be either a simple auto-generated square, or an elaborate
//  object built in Tiled.
#define AMP_NONE 0x00
#define AMP_NORTH 0x01
#define AMP_EAST 0x02
#define AMP_SOUTH 0x04
#define AMP_WEST 0x08
typedef struct AutomapPack
{
    //--Variables.
    char mInternalName[STD_MAX_LETTERS];
    char mIdentity[STD_MAX_LETTERS];
    bool mIsUnenterable;
    bool mNoSeePlayer;
    float mX;
    float mY;
    float mZ;
    float mXEnd;
    float mYEnd;
    float mLastRenderX;
    float mLastRenderY;

    //--Visibility
    bool mIsHighlighted;
    int mDoorHighlight;
    int mDoorHighlightPossible;
    bool mIsDiscovered;
    bool mIsVisibleNow;
    bool mHasEnemyIndicator;
    bool mHasFriendlyIndicator;
    bool mHasExaminableIndicator;
    bool mHasItemIndicator;
    int mDiscoveryTimer;
    int mFadeTimer;
    int mFadeCode;
    int mInsetTimer;
    uint8_t mInsetCode;
    int mPrevFadeTimer;
    int mPrevFadeCode;
    int mPrevInsetTimer;
    uint8_t mPrevInsetCode;
    int mOverrideVisN;
    int mOverrideVisE;
    int mOverrideVisS;
    int mOverrideVisW;
    int mVisibilityDown;

    //--Connections
    void *rReferenceA;
    void *rReferenceB;
    uint16_t mConnectionFlags;
    uint16_t mOpenFlags;

    //--Automap Rendering
    int mAnimationRandomOffset;
    int mTilemapLayersTotal;
    AutomapPackTilemapLayer mTilemapLayers[AMP_MAX_RENDER_OBJECTS];
    SugarLinkedList *mEntityIndicators; //AutomapPackTilemapLayer *
    SugarLinkedList *mrAnimationsList; //TilesetAnimation *, reference
    int mDoorSXTile;
    int mDoorSYTile;
    int mDoorEXTile;
    int mDoorEYTile;

    //--Other Flags
    char mStrangerGroup[STD_MAX_LETTERS];

    //--Functions
    void Initialize()
    {
        mInternalName[0] = '\0';
        strcpy(mIdentity, "No Identity");
        mIsUnenterable = false;
        mX = 0.0f;
        mY = 0.0f;
        mZ = 0.0f;
        mLastRenderX = 0.0f;
        mLastRenderY = 0.0f;
        mXEnd = 0.0f;
        mYEnd = 0.0f;
        mIsHighlighted = false;
        mDoorHighlight = 0;
        mDoorHighlightPossible = 0;
        mIsDiscovered = false;
        mIsVisibleNow = false;
        mHasEnemyIndicator = false;
        mHasFriendlyIndicator = false;
        mHasExaminableIndicator = false;
        mHasItemIndicator = false;
        mDiscoveryTimer = 0;
        mFadeTimer = AM_FADE_TICKS;
        mFadeCode = AM_FADE_NONE;
        mInsetTimer = AM_FADE_TICKS;
        mInsetCode = AM_FADE_INSET_NONE;
        mPrevFadeTimer = AM_FADE_TICKS;
        mPrevFadeCode = AM_FADE_NONE;
        mPrevInsetTimer = AM_FADE_TICKS;
        mPrevInsetCode = AM_FADE_INSET_NONE;
        mOverrideVisN = OVERRIDE_VISIBILITY_NONE;
        mOverrideVisE = OVERRIDE_VISIBILITY_NONE;
        mOverrideVisS = OVERRIDE_VISIBILITY_NONE;
        mOverrideVisW = OVERRIDE_VISIBILITY_NONE;
        mVisibilityDown = 0;
        rReferenceA = NULL;
        rReferenceB = NULL;
        mConnectionFlags = 0x0000;
        mOpenFlags = 0x0000;
        mTilemapLayersTotal = 0;
        mAnimationRandomOffset = rand() % 100;
        memset(mTilemapLayers, 0, sizeof(AutomapPackTilemapLayer) * AMP_MAX_RENDER_OBJECTS);
        mEntityIndicators = new SugarLinkedList(true);
        mrAnimationsList = new SugarLinkedList(false);
        mDoorSXTile = -1;
        mDoorSYTile = -1;
        mDoorEXTile = -1;
        mDoorEYTile = -1;
        strcpy(mStrangerGroup, "Null");
    }
    static void DeleteThis(void *pPtr)
    {
        AutomapPack *rPtr = (AutomapPack *)pPtr;
        delete rPtr->mEntityIndicators;
        delete rPtr->mrAnimationsList;
        free(rPtr);
    }
}AutomapPack;

//--Flying Letter. When a correct key is pushed, a letter spawns and flies towards the center of the circle.
typedef struct FlyingLetterPack
{
    //--Variables
    float mX;
    float mY;
    float mXTarget;
    float mYTarget;
    int mTimer;
    int mTimerMax;
    char mLetter;

    //--Functions
    void Initialize()
    {
        mX = 0.0f;
        mY = 0.0f;
        mXTarget = 0.0f;
        mYTarget = 0.0f;
        mTimer = 0;
        mTimerMax = 1;
        mLetter = 'a';
    }
}FlyingLetterPack;

//--[Locality Window]
//--Locality Category. Has a name, opened/closed state, and a list of entries. The entries are further categories.
//  If a given category has no entries list, it's a leaf.
#include "SugarLinkedList.h"
typedef struct LocalityCategory
{
    //--Variables
    char *mLocalName;
    bool mIsOpened;
    SugarBitmap *rImageUp;
    SugarBitmap *rImageDn;
    SugarLinkedList *mEntryList;
    char *mPopupBuilder;
    int mDisplayPriority;

    //--Functions
    void Initialize()
    {
        mLocalName = InitializeString("Unnamed");
        mIsOpened = false;
        rImageUp = NULL;
        rImageDn = NULL;
        mEntryList = NULL;
        mPopupBuilder = NULL;
        mDisplayPriority = 0;
    }
    static void DeleteThis(void *pPtr)
    {
        LocalityCategory *rPtr = (LocalityCategory *)pPtr;
        free(rPtr->mLocalName);
        delete rPtr->mEntryList;
        free(rPtr->mPopupBuilder);
        free(rPtr);
    }
}LocalityCategory;

//--Represents a command on the popup window.
typedef struct PopupCommand
{
    //--Variables
    char *mDisplayName;
    char *mExecutionString;

    //--Functions
    void Initialize()
    {
        mDisplayName = InitializeString("Unnamed");
        mExecutionString = InitializeString("Unnamed");
    }
    static void DeleteThis(void *pPtr)
    {
        PopupCommand *rPtr = (PopupCommand *)pPtr;
        free(rPtr->mDisplayName);
        free(rPtr->mExecutionString);
        free(rPtr);
    }
}PopupCommand;

//--Tileset Animation, renders over a tile.
typedef struct TilesetAnimation
{
    //--Variables
    int mTimer;
    int mTicksPerFrame;
    int mFrameStartX;
    int mFrameStartY;
    int mTotalFrames;
    int mYChangeEveryXFrames;
    int mActiveFrameX;
    int mActiveFrameY;
    int mTotalFramesRun;

    //--Functions
    void Initialize()
    {
        mTimer = 0;
        mTicksPerFrame = 1;
        mFrameStartX = 0;
        mFrameStartY = 0;
        mTotalFrames = 0;
        mYChangeEveryXFrames = 0;
        mActiveFrameX = 0;
        mActiveFrameY = 0;
        mTotalFramesRun = 0;
    }
}TilesetAnimation;

//--Ambient Music Packs. Used to change music volumes.
typedef struct
{
    //--Base
    AudioPackage *rSourcePackage;

    //--Modifications
    float mStoredVolume;
    float mCurrentVolume;
    float mDesiredVolume;

    //--Functions
    void Initialize()
    {
        rSourcePackage = NULL;
        mStoredVolume = 0.0f;
        mCurrentVolume = 0.0f;
        mDesiredVolume = 0.0f;
    }
}TextLevMusicPack;

//--Z-level.
typedef struct TextLevZLevel
{
    //--Position
    int mZLevel;

    //--Sizes
    int mXSize, mYSize;
    int mTileOffX, mTileOffY;

    //--Tileset Packs
    int mTilesetsTotal;
    TilesetInfoPack *mTilesetPacks;

    //--Raw Lists
    SugarLinkedList *mTileLayers;
    SugarLinkedList *mObjectData;
    SugarLinkedList *mImageData;

    //--Functions
    void Initialize()
    {
        mZLevel = 0;
        mXSize = 0;
        mYSize = 0;
        mTilesetsTotal = 0;
        mTilesetPacks = NULL;
        mTileLayers = NULL;
        mObjectData = NULL;
        mImageData = NULL;
    }
    static void DeleteThis(void *pPtr); //TextLevelParser.cc
}TextLevZLevel;
