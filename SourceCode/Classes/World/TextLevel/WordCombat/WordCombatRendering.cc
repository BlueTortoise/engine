//--Base
#include "WordCombat.h"

//--Classes
#include "IndicatorPack.h"
#include "WordCard.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DisplayManager.h"

//=========================================== Drawing =============================================
void WordCombat::Render()
{
    //--====== Setup ==========
    //--GL Setup.
    glTranslatef(0.0f, 0.0f, TL_DEPTH_GUI);

    //--Renders the combat overlay. First, handle the to-battle animation.
    if(mCombatTransitionMode == TL_COMBAT_MODE_FADEIN)
    {
        //--GL Setup.
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.0f, 0.0f, 0.0f, 0.85f);

        //--Central positioning.
        glTranslatef(VIRTUAL_CANVAS_X * 0.5f, VIRTUAL_CANVAS_Y * 0.5f, 0.0f);

        //--Angle. Increases slowly over the run of the pulse.
        float cStartAngle = (mCombatTransitionTimer / ((float)COMBAT_TRANSITION_TICKS * 1.5f)) * 3.1415926f * 2.0f;
        if(mCombatTransitionTimer > 30) cStartAngle = cStartAngle + (((mCombatTransitionTimer-30) / ((float)COMBAT_TRANSITION_TICKS * 0.05f)) * 3.1415926f * 2.0f);

        //--There are ten triangles, each rotating at 36 degrees to one another.
        float tPercentage = EasingFunction::QuadraticIn(mCombatTransitionTimer, COMBAT_TRANSITION_TICKS);
        float cRadius = (tPercentage * (VIRTUAL_CANVAS_X * 1.65f)) + 15.0f;
        glBegin(GL_TRIANGLES);
        for(int i = 0; i < 10; i ++)
        {
            //--Get the angle, compute the position.
            float tAngleA = cStartAngle + ((36.0f * TORADIAN) * (float)i);
            float tAngleB = cStartAngle + ((36.0f * TORADIAN) * ((float)i + 1.001000f));

            //--Render.
            glVertex2f(0.0f,                           0.0f);
            glVertex2f(cosf(tAngleA) * cRadius,        sinf(tAngleA) * cRadius);
            glVertex2f(cosf(tAngleB) * cRadius * 0.5f, sinf(tAngleB) * cRadius * 0.5f);
        }
        glEnd();

        //--Clean.
        glTranslatef(VIRTUAL_CANVAS_X * -0.5f, VIRTUAL_CANVAS_Y * -0.5f, 0.0f);
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
        return;
    }

    //--Fade handler.
    float tAlphaMixer = 1.0f;
    if(mCombatTransitionMode == TL_COMBAT_MODE_VICTORY)
    {
        //--Compute alpha. Only happens if past a certain point.
        if(mCombatTransitionTimer > TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)
        {
            tAlphaMixer = 1.0f - ((mCombatTransitionTimer - (TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)) / (float)TL_VICTORY_TIME_BIGBANNER_FADE);
        }
    }
    else if(mCombatTransitionMode == TL_COMBAT_MODE_DEFEAT)
    {
        //--Compute alpha. Only happens if past a certain point.
        if(mCombatTransitionTimer > 0)
        {
            tAlphaMixer = 1.0f - (mCombatTransitionTimer / (float)TL_VICTORY_TIME_TOTAL);
        }
    }

    //--Otherwise, this is normal battle mode. Render a transparent overlay.
    SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.85f * tAlphaMixer));
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlphaMixer);

    //--====== Enemy Rendering ==========
    //--Compute enemy/UI alpha.
    float cUIAlpha = mUITransitionTimer / 60.0f;
    if(cUIAlpha > 1.0f) cUIAlpha = 1.0f;
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cUIAlpha * tAlphaMixer);

    //--Render the enemies.
    RenderEnemies(cUIAlpha);

    //--====== UI Rendering ==========
    //--Offset the screen by the scatter.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cUIAlpha * tAlphaMixer);
    glTranslatef(mDamageScreenScatterX, mDamageScreenScatterY, 0.0f);

    //--Render static parts.
    Images.Data.rStaticDivider->Draw();

    //--Render highlights. These go behind the cards and all render at once.
    WordCard *rHandCard = (WordCard *)mrPlayerHand->PushIterator();
    while(rHandCard)
    {
        //--Render.
        rHandCard->RenderHighlight();

        //--Next.
        rHandCard = (WordCard *)mrPlayerHand->AutoIterate();
    }

    //--Render the words that are on the field. They're responsible for knowing where they are.
    rHandCard = (WordCard *)mrPlayerHand->PushIterator();
    while(rHandCard)
    {
        //--Highlight flag.
        int tHighlightFlag = rHandCard->GetHighlightFlag();

        //--Determine if the card needs to be darkened.
        float tAlphaFactor = 1.0f;
        bool tIsDarkened = (rClickedCard == rHandCard) && (mClickTimer % WC_CLICK_BLINK_TICKS_MOD) < WC_CLICK_BLINK_TICKS_HALF;

        //--Card is greyed out. Override darkening flag.
        if(tHighlightFlag == WC_GREY_OUT || tHighlightFlag == WC_DO_NOT_HIGHLIGHT)
        {
            tIsDarkened = true;
            tAlphaFactor = 0.50f;
        }

        //--Render.
        rHandCard->Render(tIsDarkened, false, cUIAlpha * tAlphaMixer * tAlphaFactor);

        //--Next.
        rHandCard = (WordCard *)mrPlayerHand->AutoIterate();
    }

    //--Clean up.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cUIAlpha * tAlphaMixer);

    //--Buttons.
    if(rSentenceHand[WC_DESTINATION_ACTION])
    {
        //--Render.
        Images.Data.rBtnAct->Draw(mActButton.mLft, mActButton.mTop);

        //--Power Computation.
        if(rSentenceHand[WC_DESTINATION_ACTION]->GetEffectType() == WORDCARD_EFFECT_DAMAGE && mDisplayPower > 0)
        {
            float cLft = mActButton.mXCenter;
            float cTop = mActButton.mTop - 22.0f;
            Images.Data.rFontMenuCards->DrawTextArgs(cLft, cTop, SUGARFONT_AUTOCENTER_X, 1.0f, "Damage: %i", mDisplayPower);
        }
        //--Defense Computation.
        else if(rSentenceHand[WC_DESTINATION_ACTION]->GetEffectType() == WORDCARD_EFFECT_DEFEND && mDisplayPower > 0)
        {
            float cLft = mActButton.mXCenter;
            float cTop = mActButton.mTop - 22.0f;
            Images.Data.rFontMenuCards->DrawTextArgs(cLft, cTop, SUGARFONT_AUTOCENTER_X, 1.0f, "Shields: %i", mDisplayPower);
        }
    }

    //--Ace button.
    if(mAceCard && mAceTimer >= WC_ACE_TICKS)
    {
        Images.Data.rBtnAce->Draw();
    }
    //--Not available, draw the timer.
    else
    {
        //--Variables.
        float cXPos =  95.0f;
        float cYPos = 718.0f;
        float tProgress = mAceTimer / (float)WC_ACE_TICKS;
        float cStepRate = 3.0f * TORADIAN;
        float cRadius = 32.0f;
        float tFirstProgress = tProgress;
        if(tFirstProgress > 0.50f) tFirstProgress = 0.50f;

        //--Render a circle over the area to indicate shuffling progress.
        glDisable(GL_TEXTURE_2D);
        glTranslatef(cXPos, cYPos, 0.0f);
        glBegin(GL_POLYGON);
            glVertex2f(0.0f, 0.0f);
            for(float cAngle = 0.0f; cAngle < 3.1415926f * tFirstProgress * 2.0f; cAngle = cAngle + cStepRate)
            {
                glVertex2f(cosf(cAngle) * cRadius, sinf(cAngle) * cRadius);
            }
            glVertex2f(0.0f, 0.0f);
        glEnd();
        if(tProgress > 0.50f)
        {
            glBegin(GL_POLYGON);
            glVertex2f(0.0f, 0.0f);
            for(float cAngle = 3.1415926f; cAngle < 3.1415926f * tProgress * 2.0f; cAngle = cAngle + cStepRate)
            {
                glVertex2f(cosf(cAngle) * cRadius, sinf(cAngle) * cRadius);
            }
            glVertex2f(0.0f, 0.0f);
            glEnd();
        }
        glTranslatef(-cXPos, -cYPos, 0.0f);
        glEnable(GL_TEXTURE_2D);
    }

    //--Card itself always renders if it exists.
    if(mAceCard) mAceCard->Render(false, false, cUIAlpha * tAlphaMixer);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cUIAlpha * tAlphaMixer);

    //--Shuffle Highlight. Only appears if the timer is over 0. Goes under the shuffle button.
    if(mShuffleHighlightTimer > 0)
    {
        //--Compute rotation.
        float cRotationAngle = (mShuffleRotateTimer / (float)WC_SHUFFLE_HIGHLIGHT_ROTATE_TICKS) * 360.0f;
        float cScale = (mShuffleHighlightTimer / (float)WC_SHUFFLE_HIGHLIGHT_TICKS);

        //--Position.
        float cCenterX = mShuffleButton.mXCenter;
        float cCenterY = mShuffleButton.mYCenter;
        glTranslatef(cCenterX, cCenterY, 0.0f);
        glRotatef(cRotationAngle, 0.0f, 0.0f, 1.0f);
        glScalef(cScale, cScale, 1.0f);

        //--Render.
        float cHfX = Images.Data.rBtnShuffleHighlight->GetTrueWidth()  * -0.50f;
        float cHfY = Images.Data.rBtnShuffleHighlight->GetTrueHeight() * -0.50f;
        Images.Data.rBtnShuffleHighlight->Draw(cHfX, cHfY);

        //--Clean.
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        glRotatef(-cRotationAngle, 0.0f, 0.0f, 1.0f);
        glTranslatef(-cCenterX, -cCenterY, 0.0f);
    }

    //--In wait mode:
    if(mIsWaitMode)
    {
        //--The draw card appears if the player has at least one space open in their hand, and one card in their deck.
        if(mrPlayerDeck->GetListSize() > 0 && mrPlayerHand->GetListSize() < mPlayerMaxHandSize)
        {
            //--The button itself.
            Images.Data.rBtnDraw->Draw();

            //--If the player has "Free Draws", this indicator appears.
            if(mFreeDrawsLeft > 0)
            {
                Images.Data.rFontMenuCards->DrawTextArgs(mDrawButton.mRgt, mDrawButton.mTop - 25.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "Free Draws: %i", mFreeDrawsLeft);
            }
        }

        //--The shuffle button always appears.
        Images.Data.rBtnShuffle->Draw();
    }
    //--In active mode:
    else
    {
        //--If the player has cards left to draw, show the draw button. AutoDraw also prevents this.
        if(mrPlayerDeck->GetListSize() > 0 && !mAutoDraw)
        {
            Images.Data.rBtnDraw->Draw();
        }
        //--Otherwise show the shuffle button.
        else
        {
            Images.Data.rBtnShuffle->Draw();
        }
    }


    //--Render the surrender button.
    Images.Data.rBtnSurrender->Draw();

    //--Potions
    if(mPotions > 0)
    {
        //--Render the potion indicator.
        Images.Data.rBtnPotion->Draw();

        //--Render how many potions are left.
        StarlightColor::SetMixer(0.4f, 0.8f, 1.0f, cUIAlpha * tAlphaMixer);
        Images.Data.rFontMenuCards->DrawTextArgs(205.0f, 720.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i", mPotions);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cUIAlpha * tAlphaMixer);
    }

    //--Render guides.
    /*
    for(int i = 0; i < WC_DESTINATIONS_MAX; i ++)
    {
        //--Skip inactive guides.
        if(!mDestinationActivities[i]) continue;

        //--Render a rectangle around it.
        glLineWidth(5.0f);
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_LINE_LOOP);
            glVertex2f(mDestinationButtons[i].mLft, mDestinationButtons[i].mTop);
            glVertex2f(mDestinationButtons[i].mRgt, mDestinationButtons[i].mTop);
            glVertex2f(mDestinationButtons[i].mRgt, mDestinationButtons[i].mBot);
            glVertex2f(mDestinationButtons[i].mLft, mDestinationButtons[i].mBot);
        glEnd();
        glEnable(GL_TEXTURE_2D);
        glLineWidth(1.0f);
    }*/

    //--Flashing Timers.
    int tFlashBlack = -1;
    if(mClickState == WC_CLICK_STATE_PLAYEDCARD && mSwapCardTimer < WC_WORD_FLASH_TICKS / 2)
    {
        tFlashBlack = mSwapCard;
    }

    //--Render any cards that are in the current sentence.
    for(int i = 0; i < WC_DESTINATIONS_MAX; i ++)
    {
        //--Skip inactive guides.
        if(!rSentenceHand[i]) continue;

        //--Flashing card.
        rSentenceHand[i]->Render((rClickedCard == rSentenceHand[i]) && (mClickTimer % WC_CLICK_BLINK_TICKS_MOD) < WC_CLICK_BLINK_TICKS_HALF, (tFlashBlack == i), cUIAlpha * tAlphaMixer);
    }

    //--====== Sentence Results ==========
    //--Render sentence results.
    if(mSentenceResultList->GetListSize() > 0)
    {
        //--Setup.
        float tYPosition = 10.0f;
        float cYStepRate = 20.0f;

        //--Iterate across the list.
        int i = 0;
        SentencePack *rPackage = (SentencePack *)mSentenceResultList->PushIterator();
        while(rPackage)
        {
            //--Determine the X position.
            float tStartX = (Images.Data.rFontMenuCards->GetTextWidth(rPackage->mSentence) + 30.0f) * -1.0f;
            float tEndX = 10.0f;
            float tPercent = EasingFunction::QuadraticInOut(mSentenceResultTimer - (i * 15), 10.0f);
            float tXPosition = tStartX + ((tEndX - tStartX) * tPercent);
            glTranslatef(tXPosition, tYPosition, 0.0f);

            //--Render the icon.
            if(rPackage->rDisplayImg)
            {
                glScalef(0.2f, 0.2f, 1.0f);
                rPackage->rDisplayImg->Draw();
                glScalef(5.0f, 5.0f, 1.0f);
            }

            //--Render the associated text.
            if(rPackage->mSentence[0] != '\0')
            {
                Images.Data.rFontMenuCards->DrawText(30.0f, 0.0f, 0, 1.0f, rPackage->mSentence);
            }

            //--Clean.
            glTranslatef(-tXPosition, -tYPosition, 0.0f);

            //--Next.
            i ++;
            tYPosition = tYPosition + cYStepRate;
            rPackage = (SentencePack *)mSentenceResultList->AutoIterate();
        }
    }

    //--====== Hearts and Shields ==========
    //--Handled by a subroutine.
    RenderUIObjects(cUIAlpha * tAlphaMixer);

    //--====== Deck, Graveyard, and Shuffling ==========
    //--Render the player's deck power and deck stats.
    //Images.Data.rFontMenuCards->DrawTextArgs(1210.0f, 649.0f, 0, 1.0f, "Power: %i", (int)roundf(mDeckPower * 100.0f));
    //Images.Data.rFontMenuCards->DrawTextArgs(1210.0f, 669.0f, 0, 1.0f, "Deck:  %i / %i", mrPlayerDeck->GetListSize(), mGlobalCardDeck->GetListSize());

    //--Render the player's deck.
    WordCard *rDeckCard = (WordCard *)mrPlayerDeck->PushIterator();
    while(rDeckCard)
    {
        rDeckCard->RenderNoText(true, false, cUIAlpha * tAlphaMixer);
        rDeckCard = (WordCard *)mrPlayerDeck->AutoIterate();
    }

    //--Graveyard cards only render when moving, otherwise they are offscreen.
    WordCard *rGraveCard = (WordCard *)mrPlayerGraveyard->PushIterator();
    while(rGraveCard)
    {
        if(rGraveCard->IsMoving()) rGraveCard->Render(true, false, cUIAlpha * tAlphaMixer);
        rGraveCard = (WordCard *)mrPlayerGraveyard->AutoIterate();
    }

    //--Shuffling!
    if(mIsShuffling)
    {
        //--Variables.
        float cXPos = (VIRTUAL_CANVAS_X) * 0.50f;
        float cYPos = 593.0f;
        float tProgress = mShuffleTimer / (float)mShuffleTimerMax;
        float cStepRate = 3.0f * TORADIAN;
        float cRadius = 64.0f;
        float tFirstProgress = tProgress;
        if(tFirstProgress > 0.50f) tFirstProgress = 0.50f;

        //--Render a circle over the area to indicate shuffling progress.
        glDisable(GL_TEXTURE_2D);
        glTranslatef(cXPos, cYPos, 0.0f);
        glBegin(GL_POLYGON);
            glVertex2f(0.0f, 0.0f);
            for(float cAngle = 0.0f; cAngle < 3.1415926f * tFirstProgress * 2.0f; cAngle = cAngle + cStepRate)
            {
                glVertex2f(cosf(cAngle) * cRadius, sinf(cAngle) * cRadius);
            }
            glVertex2f(0.0f, 0.0f);
        glEnd();
        if(tProgress > 0.50f)
        {
            glBegin(GL_POLYGON);
            glVertex2f(0.0f, 0.0f);
            for(float cAngle = 3.1415926f; cAngle < 3.1415926f * tProgress * 2.0f; cAngle = cAngle + cStepRate)
            {
                glVertex2f(cosf(cAngle) * cRadius, sinf(cAngle) * cRadius);
            }
            glVertex2f(0.0f, 0.0f);
            glEnd();
        }
        glTranslatef(-cXPos, -cYPos, 0.0f);
        glEnable(GL_TEXTURE_2D);

        //--Text overlay.
        cXPos = (VIRTUAL_CANVAS_X - Images.Data.rFontMenuHeader->GetTextWidth("Shuffling!")) * 0.50f;
        Images.Data.rFontMenuHeader->DrawText(cXPos, 593.0f, 0, 1.0f, "Shuffling!");
    }

    //--Render indicator packs.
    IndicatorPack *rIndPack = (IndicatorPack *)mTextIndicatorList->PushIterator();
    while(rIndPack)
    {
        rIndPack->Render();
        rIndPack = (IndicatorPack *)mTextIndicatorList->AutoIterate();
    }

    //--Clean scatter.
    glTranslatef(-mDamageScreenScatterX, -mDamageScreenScatterY, 0.0f);
    StarlightColor::ClearMixer();

    //--====== Victory and Defeat ==========
    //--Render Victory/Defeat cases.
    RenderOutOverlays();

    //--====== Clean Up ==========
    StarlightColor::ClearMixer();
    glTranslatef(0.0f, 0.0f, -TL_DEPTH_GUI);
}
void WordCombat::RenderOutOverlays()
{
    //--Renders Victory or Defeat overlays if those are active. Does nothing otherwise.
    if(mCombatTransitionMode == TL_COMBAT_MODE_VICTORY)
    {
        //--Compute display percentage.
        float tPercent = mCombatTransitionTimer / (float)TL_VICTORY_TIME_BIGBANNER_IN;

        //--Compute alpha.
        float tAlpha = 1.0f;
        if(mCombatTransitionTimer > TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)
        {
            tAlpha = 1.0f - ((mCombatTransitionTimer - (TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)) / (float)TL_VICTORY_TIME_BIGBANNER_FADE);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);
        }

        //--Subroutine render.
        //SugarBitmap::DrawHorizontalPercent(Images.Victory.rBannerBig, tPercent);

        //--At certain points, render letters:
        char tBuffer[32];
        float cTimeStart   = 0.30f;
        float cTimeSpacing = 0.08f;
        if(tPercent >= cTimeStart + (cTimeSpacing * 0.0f))
        {
            strcpy(tBuffer, "V");
            if(tPercent >= cTimeStart + (cTimeSpacing * 1.0f)) strcat(tBuffer, "I");
            if(tPercent >= cTimeStart + (cTimeSpacing * 2.0f)) strcat(tBuffer, "C");
            if(tPercent >= cTimeStart + (cTimeSpacing * 3.0f)) strcat(tBuffer, "T");
            if(tPercent >= cTimeStart + (cTimeSpacing * 4.0f)) strcat(tBuffer, "O");
            if(tPercent >= cTimeStart + (cTimeSpacing * 5.0f)) strcat(tBuffer, "R");
            if(tPercent >= cTimeStart + (cTimeSpacing * 6.0f)) strcat(tBuffer, "Y");
            if(tPercent >= cTimeStart + (cTimeSpacing * 7.0f)) strcat(tBuffer, "!");
            Images.Data.rFontMenuHeader->DrawText(351.0f, 250.0f, 0, 4.0f, tBuffer);
        }
    }
    //--Uses the same timers but displays DEFEAT instead.
    else if(mCombatTransitionMode == TL_COMBAT_MODE_DEFEAT)
    {
        //--Compute display percentage.
        float tPercent = mCombatTransitionTimer / (float)TL_VICTORY_TIME_BIGBANNER_IN;

        //--Compute alpha.
        float tAlpha = 1.0f;
        if(mCombatTransitionTimer > TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)
        {
            tAlpha = 1.0f - ((mCombatTransitionTimer - (TL_VICTORY_TIME_BIGBANNER_IN + TL_VICTORY_TIME_BIGBANNER_HOLD)) / (float)TL_VICTORY_TIME_BIGBANNER_FADE);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);
        }

        //--Subroutine render.
        //SugarBitmap::DrawHorizontalPercent(Images.Victory.rBannerBig, tPercent);

        //--At certain points, render letters:
        char tBuffer[32];
        float cTimeStart   = 0.30f;
        float cTimeSpacing = 0.08f;
        if(tPercent >= cTimeStart + (cTimeSpacing * 0.0f))
        {
            strcpy(tBuffer, "D");
            if(tPercent >= cTimeStart + (cTimeSpacing * 1.0f)) strcat(tBuffer, "E");
            if(tPercent >= cTimeStart + (cTimeSpacing * 2.0f)) strcat(tBuffer, "F");
            if(tPercent >= cTimeStart + (cTimeSpacing * 3.0f)) strcat(tBuffer, "E");
            if(tPercent >= cTimeStart + (cTimeSpacing * 4.0f)) strcat(tBuffer, "A");
            if(tPercent >= cTimeStart + (cTimeSpacing * 5.0f)) strcat(tBuffer, "T");
            if(tPercent >= cTimeStart + (cTimeSpacing * 6.0f)) strcat(tBuffer, "!");
            Images.Data.rFontMenuHeader->DrawText(361.0f, 250.0f, 0, 4.0f, tBuffer);
        }
    }
}
