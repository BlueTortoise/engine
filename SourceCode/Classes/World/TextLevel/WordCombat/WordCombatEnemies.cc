//--Base
#include "WordCombat.h"

//--Classes
#include "IndicatorPack.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"

void WordCombat::CreateEnemyProgressIndicator(int pTicks)
{
    //--Creates an indicator pack showing movement in the enemy action bar.
    if(pTicks == 0 || !Images.mIsReady) return;

    //--Get the 0th enemy.
    CombatEnemyPack *rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->GetElementBySlot(0);
    if(!rEnemyPack) return;

    //--Get which slot it's in.
    int tUseSlot = mCombatEnemyList->GetListSize() - 1;
    if(tUseSlot < 1) tUseSlot = 0;
    if(tUseSlot > 4) tUseSlot = 4;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    IndicatorPack *nPack = (IndicatorPack *)starmemoryalloc(sizeof(IndicatorPack));
    nPack->Initialize();
    mTextIndicatorList->AddElementAsTail("X", nPack, &FreeThis);

    //--Set basic properties.
    nPack->SetFont(Images.Data.rFontMenuCards);
    nPack->SetMaxTimer(85);
    nPack->SetUpdateFloatFadeout(-15.0f);

    //--Set the string.
    char tBuffer[32];
    float cXOffset = 0.0f;
    if(pTicks >= 0)
    {
        nPack->SetColor(1.0f, 0.2f, 0.2f, 1.0f);
        sprintf(tBuffer, "+%i", pTicks);
    }
    else
    {
        nPack->SetColor(0.2f, 1.0f, 0.2f, 1.0f);
        sprintf(tBuffer, "%i", pTicks);
        cXOffset = 40.0f;
    }
    nPack->SetString(tBuffer);

    //--Constants.
    float cAPFrameY = 60.0f;

    //--Compute position.
    float tXPos = cEnemyRenderPos[tUseSlot][0] - (rEnemyPack->rImage->GetTrueWidth() * 0.50f) + 300.0f + cXOffset;
    float cBot = (cAPFrameY + 3.0f) + (Images.Data.rAttackBarFill->GetHeight());
    float cTop = cBot - (Images.Data.rAttackBarFill->GetHeight() * rEnemyPack->mAPRender) + 5.0f;
    nPack->SetPosition(tXPos + 35.0f, cTop - 22.0f);
}
void WordCombat::UpdateEnemies(bool pAllowTimeAdvance)
{
    //--Handles updating all enemies on the field. Enemies will roll an attack, then tick a timer
    //  down to zero at which point they perform the attack. This all takes place in real-time.
    CombatEnemyPack *rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->PushIterator();
    while(rEnemyPack)
    {
        //--If a tick reversal was applied, decrement readiness here.
        if(mTicksReverse > 0)
        {
            rEnemyPack->mPrepTicksLeft += mTicksReverse;
            if(rEnemyPack->mPrepTicksLeft > rEnemyPack->mPrepTicksMax) rEnemyPack->mPrepTicksLeft = rEnemyPack->mPrepTicksMax;
        }

        //--Enemy HP timer.
        if(rEnemyPack->mHPTimer < CEP_HP_TICKS)
        {
            rEnemyPack->mHPTimer ++;
            rEnemyPack->mHPRender = rEnemyPack->mHPStart + ((rEnemyPack->mHPEnd - rEnemyPack->mHPStart) * EasingFunction::QuadraticOut(rEnemyPack->mHPTimer, CEP_HP_TICKS));
        }

        //--If at least one enemy is dying, break out.
        if(mIsAnEnemyDying) break;

        //--If the enemy is flashing, handle that here.
        if(rEnemyPack->mFlashTimer > 0)
        {
            rEnemyPack->mFlashTimer --;
        }

        //--If the enemy is stunned, it does nothing here. Stunned enemies also restart attack timers.
        if(rEnemyPack->mStunTimer > 0)
        {
            rEnemyPack->mCurrentAttack = -1;
            rEnemyPack->mStunTimer --;
            rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->AutoIterate();
            continue;
        }

        //--Enemy has no valid attacks, or is dead. Do nothing.
        if(rEnemyPack->mAttackList->GetListSize() < 1 || rEnemyPack->mTotalAttackWeight < 1 || rEnemyPack->mHP < 1)
        {
        }
        //--If an enemy does not have an active attack, roll one.
        else if(rEnemyPack->mCurrentAttack == -1)
        {
            //--Roll a new attack.
            int tAttackRoll = rand() % rEnemyPack->mTotalAttackWeight;

            //--Set attack percentage to zero.
            rEnemyPack->mAPRender = 0.0f;

            //--Run through the attacks and select the one matching the roll.
            int i = 0;
            CombatEnemyAttackPack *rAttackPack = (CombatEnemyAttackPack *)rEnemyPack->mAttackList->PushIterator();
            while(rAttackPack)
            {
                //--Decrement.
                tAttackRoll -= rAttackPack->mRollWeight;

                //--This is the attack to use!
                if(tAttackRoll < 0)
                {
                    rEnemyPack->mCurrentAttack = i;
                    rEnemyPack->mPrepTicksLeft = rAttackPack->mPrepTicks;
                    rEnemyPack->mPrepTicksMax = rAttackPack->mPrepTicks;
                    if(rEnemyPack->mPrepTicksMax < 1.0f) rEnemyPack->mPrepTicksMax = 1.0f;
                    rEnemyPack->mAttackTicks = 0;
                    rEnemyPack->mAttackTicksNeeded = WC_TICKS_PER_SEQUENCE;
                    break;
                }

                //--Next.
                i ++;
                rAttackPack = (CombatEnemyAttackPack *)rEnemyPack->mAttackList->AutoIterate();
            }
        }
        //--Otherwise, run the prep timer down. If time is not allowed to advance, this doesn't happen!
        else if(rEnemyPack->mPrepTicksLeft > 0)
        {
            if(pAllowTimeAdvance) rEnemyPack->mPrepTicksLeft --;
            rEnemyPack->mAPRender = 1.0f - ((float)rEnemyPack->mPrepTicksLeft / (float)rEnemyPack->mPrepTicksMax);
        }
        //--Enemy is currently attacking. Print their attack phrase out and flash them.
        else if(rEnemyPack->mAttackTicks < rEnemyPack->mAttackTicksNeeded)
        {
            //--Increment.
            rEnemyPack->mAttackTicks ++;
            if(rEnemyPack->mAttackTicks == 1) AudioManager::Fetch()->PlaySound("Combat|AttackMiss");

            //--Show the attack bar as full.
            rEnemyPack->mAPRender = 1.0f;

            //--On the final tick, the player takes damage and the attack gets re-rolled.
            if(rEnemyPack->mAttackTicks >= rEnemyPack->mAttackTicksNeeded)
            {
                //--Attack the player using this subroutine.
                CombatEnemyAttackPack *rAttackPack = (CombatEnemyAttackPack *)rEnemyPack->mAttackList->GetElementBySlot(rEnemyPack->mCurrentAttack);
                AttackPlayer(rAttackPack);
                AudioManager::Fetch()->PlaySound("Combat|Impact_Slash");

                //--Reset.
                rEnemyPack->mCurrentAttack = -1;
            }
        }

        //--Next.
        rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->AutoIterate();
    }

    //--If an enemy is dying, run this timer.
    if(mIsAnEnemyDying)
    {
        //--Timer.
        mDyingEnemyTimer --;

        //--Ending case:
        if(mDyingEnemyTimer < 1)
        {
            //--Flag.
            mIsAnEnemyDying = false;

            //--Remove the enemy in question.
            mCombatEnemyList->RemoveElementI(mDyingEnemySlot);

            //--Swap the target if it was on that enemy.
            if(mEnemyTarget == mDyingEnemySlot) mEnemyTarget --;
            if(mEnemyTarget < 0) mEnemyTarget = 0;
        }
    }

    //--Tick reversal is reduced if nonzero.
    if(mTicksReverse > 0) mTicksReverse = 0;
}
void WordCombat::RenderEnemies(float pAlpha)
{
    //--Compute which slot to use. Caps out at 4.
    int i = 0;
    int tUseSlot = mCombatEnemyList->GetListSize() - 1;
    if(tUseSlot < 1) tUseSlot = 0;
    if(tUseSlot > 4) tUseSlot = 4;

    //--Render.
    CombatEnemyPack *rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->PushIterator();
    while(rEnemyPack)
    {
        //--Skip if the enemy has no image.
        if(!rEnemyPack->rImage)
        {
            i ++;
            if(i > 4) break;
            rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->AutoIterate();
            continue;
        }

        //--Resolve rendering positions.
        float tXPos = cEnemyRenderPos[tUseSlot][i] - (rEnemyPack->rImage->GetTrueWidth() * 0.50f);
        float tYPos = 250.0f - (rEnemyPack->rImage->GetTrueHeight() * 0.50f);

        //--Enemy is dying.
        if(mIsAnEnemyDying && mDyingEnemySlot == i && rEnemyPack->rImage && mDyingEnemyTimer > 0)
        {
            //--Fading up to white.
            if(mDyingEnemyTimer > ENEMY_DIE_WAIT_TICKS + ENEMY_DIE_FADEOUT_TICKS)
            {
                //--Set the GL State up for the first stencil pass.
                glEnable(GL_STENCIL_TEST);
                glColorMask(true, true, true, true);
                glDepthMask(true);
                glStencilFunc(GL_ALWAYS, TL_STENCIL_CODE_COMBAT, 0xFF);
                glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
                glStencilMask(0xFF);

                //--Render.
                if(rEnemyPack->rImage) rEnemyPack->rImage->Draw(tXPos, tYPos);

                //--Now switch to white blending.
                glColorMask(true, true, true, true);
                glDepthMask(true);
                glStencilFunc(GL_EQUAL, TL_STENCIL_CODE_COMBAT, 0xFF);
                glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
                glStencilMask(0xFF);

                //--Blend based on the timing.
                int tUseTimer = mDyingEnemyTimer - (ENEMY_DIE_WAIT_TICKS + ENEMY_DIE_FADEOUT_TICKS);
                float tPercent = 1.0f - EasingFunction::QuadraticOut(tUseTimer, ENEMY_DIE_WHITEOUT_TICKS);
                glDisable(GL_TEXTURE_2D);
                glColor4f(1.0f, 1.0f, 1.0f, tPercent);
                if(rEnemyPack->rImage) rEnemyPack->rImage->Draw(tXPos, tYPos);

                //--Clean up.
                glEnable(GL_TEXTURE_2D);
                glDisable(GL_STENCIL_TEST);
                StarlightColor::ClearMixer();
            }
            //--Fading from white down to black.
            else if(mDyingEnemyTimer > ENEMY_DIE_WAIT_TICKS)
            {
                //--Setup.
                glEnable(GL_STENCIL_TEST);
                glColorMask(false, false, false, false);
                glDepthMask(false);
                glStencilFunc(GL_ALWAYS, TL_STENCIL_CODE_COMBAT, 0xFF);
                glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
                glStencilMask(0xFF);

                //--Render.
                if(rEnemyPack->rImage) rEnemyPack->rImage->Draw(tXPos, tYPos);

                //--Now switch to rendering mode.
                glColorMask(true, true, true, true);
                glDepthMask(true);
                glStencilFunc(GL_EQUAL, TL_STENCIL_CODE_COMBAT, 0xFF);
                glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
                glStencilMask(0xFF);

                //--Render based on the timing.
                int tUseTimer = mDyingEnemyTimer - ENEMY_DIE_WAIT_TICKS;
                float tPercent = EasingFunction::QuadraticInOut(tUseTimer, ENEMY_DIE_FADEOUT_TICKS);
                glDisable(GL_TEXTURE_2D);
                glColor4f(0.0f, 0.0f, 0.0f, tPercent);
                if(rEnemyPack->rImage) rEnemyPack->rImage->Draw(tXPos, tYPos);

                //--Clean up.
                glEnable(GL_TEXTURE_2D);
                glDisable(GL_STENCIL_TEST);
                StarlightColor::ClearMixer();
            }
            //--Don't render during the cooling off.
            else
            {

            }
        }

        //--When flashing from taking damage, the enemy will not render.
        if(rEnemyPack->mFlashTimer > 0 && rEnemyPack->mFlashTimer % 10 < 5)
        {
            i ++;
            if(i > 4) break;
            rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->AutoIterate();
            continue;
        }

        //--If the enemy is in the attack flashing phase, they might not render, or may render fullblack.
        if(rEnemyPack->mPrepTicksLeft < 1 && rEnemyPack->mAttackTicks > rEnemyPack->mAttackTicksNeeded - WC_TICKS_PER_SEQUENCE)
        {
            //--Fullblack happens on the 2nd and 4th parts of the sequence.
            bool tIsFullBlack = false;
            float tPercentComplete = (rEnemyPack->mAttackTicks - (rEnemyPack->mAttackTicksNeeded - WC_TICKS_PER_SEQUENCE)) / (float)WC_TICKS_PER_SEQUENCE;
            if(tPercentComplete >= 0.20f && tPercentComplete <= 0.40f) tIsFullBlack = true;
            if(tPercentComplete >= 0.60f && tPercentComplete <= 0.80f) tIsFullBlack = true;

            //--When handling a fullblack, simply set the mixer to all zeroes.
            if(tIsFullBlack) StarlightColor::cxBlack.SetAsMixerAlpha(pAlpha);
        }

        //--Render the enemy, centered on this position.
        if(rEnemyPack->rImage)
        {
            rEnemyPack->rImage->Draw(tXPos, tYPos);
        }

        //--Clean.
        StarlightColor::cxWhite.SetAsMixerAlpha(pAlpha);

        //--HP Bar rendering.
        if(rEnemyPack->mHPMax > 0)
        {
            //--Frame.
            float cHPFrameX = tXPos - (Images.Data.rHealthBarFrame->GetTrueWidth() * 0.50f) + 188.0f;
            float cHPFrameY = 15.0f;
            Images.Data.rHealthBarFrame->Draw(cHPFrameX, cHPFrameY);

            //--Fill of the HP Bar. Uses partial rendering.
            float cTXL = 0.0f;
            float cTXR = rEnemyPack->mHPRender;
            float cTXT = 0.0f;
            float cTXB = 1.0f;
            float cLft = cHPFrameX + 3.0f;
            float cTop = cHPFrameY + 3.0f;
            float cRgt = cLft + (Images.Data.rHealthBarFill->GetWidth() * rEnemyPack->mHPRender);
            float cBot = cTop + Images.Data.rHealthBarFill->GetHeight();
            Images.Data.rHealthBarFill->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(cTXL, cTXB); glVertex2f(cLft, cTop);
                glTexCoord2f(cTXR, cTXB); glVertex2f(cRgt, cTop);
                glTexCoord2f(cTXR, cTXT); glVertex2f(cRgt, cBot);
                glTexCoord2f(cTXL, cTXT); glVertex2f(cLft, cBot);
            glEnd();

            //--Numerical display.
            Images.Data.rFontMenuCards->DrawTextArgs(cHPFrameX + 80.0f, 3.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%i/%i", rEnemyPack->mHP, rEnemyPack->mHPMax);
        }

        //--Attack Bar rendering.
        float cAPFrameX = tXPos + 300.0f;
        float cAPFrameY = 60.0f;
        Images.Data.rAttackBarFrame->Draw(cAPFrameX, cAPFrameY);
        if(rEnemyPack->mAPRender > 0.0f)
        {
            float cTXL = 0.0f;
            float cTXR = 1.0f;
            float cTXT = 1.0f - rEnemyPack->mAPRender;
            float cTXB = 1.0f;
            float cLft = cAPFrameX + 3.0f;
            float cBot = (cAPFrameY + 3.0f) + (Images.Data.rAttackBarFill->GetHeight());
            float cRgt = cLft + (Images.Data.rAttackBarFill->GetWidth());
            float cTop = cBot - (Images.Data.rAttackBarFill->GetHeight() * rEnemyPack->mAPRender);
            Images.Data.rAttackBarFill->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(cTXL, cTXT); glVertex2f(cLft, cTop);
                glTexCoord2f(cTXR, cTXT); glVertex2f(cRgt, cTop);
                glTexCoord2f(cTXR, cTXB); glVertex2f(cRgt, cBot);
                glTexCoord2f(cTXL, cTXB); glVertex2f(cLft, cBot);
            glEnd();
        }

        //--Next.
        i ++;
        if(i > 4) break;
        rEnemyPack = (CombatEnemyPack *)mCombatEnemyList->AutoIterate();
    }
}
