//--[IndicatorPack]
//--Structure which contains a block of text. Used to render temporary text pulses to the screen.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
#define IP_UPDATE_NONE 0
#define IP_UPDATE_FADE 1
#define IP_UPDATE_FLOAT_UP 2
#define IP_UPDATE_FLOAT_UP_FADE 3

//--[Classes]
typedef struct IndicatorPack
{
    public:
    //--[Members]
    //--Text Render
    StarlightColor mColor;
    SugarFont *rRenderFont;
    float mX;
    float mY;
    int mFlags;
    float mSize;
    char mSentence[256];

    //--Update
    int mTimer;
    int mTimerMax;
    int mUpdateType;

    //--Floating up
    float mYFloatUpMax;

    //--[Functions]
    //--System
    void Initialize();

    //--Public Variables
    //--Property Queries
    bool IsComplete();

    //--Manipulators
    void SetColor(StarlightColor pColor);
    void SetColor(float pRed, float pGreen, float pBlue, float pAlpha);
    void SetFont(SugarFont *pFont);
    void SetPosition(float pX, float pY);
    void SetFlags(int pFlags);
    void SetSize(float pSize);
    void SetString(const char *pString);
    void SetCurTimer(int pTicks);
    void SetMaxTimer(int pTicks);
    void SetUpdateNone();
    void SetUpdateFade();
    void SetUpdateFloat(float pYDist);
    void SetUpdateFloatFadeout(float pYDist);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
}IndicatorPack;

