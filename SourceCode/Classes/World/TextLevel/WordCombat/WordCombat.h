//--[WordCombat]
//--Combat used for Text Adventure mode. The player constructs attacks out of sentences using words
//  pulled from a deck. Makes use of the mouse mostly.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "TextLevelStructures.h"
#include "TextLevelEnemyPack.h"

//--[Local Structures]
//--Represents a shield on the player's HP bar.
#define SHIELD_MOVESPEED 15.0f
typedef struct PlayerUIObject
{
    //--Basics.
    int mElementalTimer;
    uint8_t mElementalType;
    SugarBitmap *rImage;

    //--Position
    float mXPos;
    float mYPos;

    //--Movement Sliding
    int mMoveTicks;
    int mMoveTicksMax;
    float mStartX;
    float mStartY;
    float mTargetX;
    float mTargetY;

    //--Movement Gravity
    bool mIsGravityMovement;
    float mXSpeed;
    float mYSpeed;

    //--Functions
    void Initialize()
    {
        //--Basics.
        mElementalTimer = 0;
        mElementalType = 0x00;
        rImage = NULL;

        //--Position
        mXPos = 0.0f;
        mYPos = 0.0f;

        //--Movement Sliding
        mMoveTicks = 1;
        mMoveTicksMax = 1;
        mStartX = 0.0f;
        mStartY = 0.0f;
        mTargetX = 0.0f;
        mTargetY = 0.0f;

        //--Movement Gravity
        mIsGravityMovement = false;
        mXSpeed = 0.0f;
        mYSpeed = 0.0f;
    }
    int ComputeValue();
    void MoveToInstant(float pX, float pY);
    void MoveToSlide(float pX, float pY);
    void MoveGravity();
    void Update();
}PlayerUIObject;

//--Represents a result indicator once damage/defense is computed by a sentence.
typedef struct
{
    //--Members
    SugarBitmap *rDisplayImg;
    char mSentence[256];

    //--Functions
    void Init()
    {
        rDisplayImg = NULL;
        mSentence[0] = '\0';
    }
}SentencePack;

//--[Local Definitions]
//--Shield Constants
#define WC_SHIELD_MAX 15

//--Max Cards In hand
#define WC_MAX_CARDS_IN_HAND 11

//--Rendering Constants
#define WC_WORD_FLASH_TICKS 16
#define WC_WORD_STANDARD_Y 590.0f
#define WC_WORD_CARD_SPACING 85.0f

//--Click State
#define WC_CLICK_STATE_NONE 0
#define WC_CLICK_STATE_HANDCARD 1
#define WC_CLICK_STATE_PLAYEDCARD 2

#define WC_CLICK_BLINK_TICKS_MOD 20
#define WC_CLICK_BLINK_TICKS_HALF 10

#define WC_HIGHLIGHT_IGNORE -1
#define WC_DO_NOT_HIGHLIGHT 0
#define WC_GREY_OUT 1
#define WC_HIGHLIGHT 2

//--Destination Buttons
#define WC_DESTINATION_MODSLOTS 5
#define WC_DESTINATION_ACTION 5
#define WC_DESTINATION_OFSLOT 6
#define WC_DESTINATION_TRAILINGSLOT 7
#define WC_DESTINATIONS_MAX 8

//--How much the screen gyrates when the player gets hit.
#define WC_SCATTER_SEVERITY 30.0f
#define WC_SCATTER_LENGTH 30
#define WC_SCATTER_INTERVAL 3

//--Charging Rates
#define SECONDS_FOR_FULL_CHARGE 30.0f
#define FULL_CHARGE_POWER 1.0f

//--Shuffle Display
#define WC_SHUFFLE_MIN_CARDS 3
#define WC_SHUFFLE_HIGHLIGHT_TICKS 30
#define WC_SHUFFLE_HIGHLIGHT_ROTATE_TICKS 60

//--Special Effects
#define WC_SPECIAL_EFFECT_TICKS 180

//--[Classes]
class WordCombat
{
    private:
    //--System
    int mCombatResolution;
    int mDamageScreenScatterTimer;
    float mDamageScreenScatterX;
    float mDamageScreenScatterY;

    //--Wait Flags
    bool mLastReplaceWasFree;
    bool mIsWaitMode;
    int mTicksAllowed;
    int mTicksReverse;
    int mTicksPerCardMove;
    int mTicksPerCardDraw;
    int mTicksPerAct;
    int mTicksPerShuffle;
    int mTicksPerPotion;
    int mFreeDrawsLeft;

    //--String Handler
    SugarLinkedList *mTextIndicatorList; //IndicatorPack *, master

    //--Options
    bool mAutoDraw;
    bool mAutoPlace;

    //--Timers
    int mUITransitionTimer;
    int mCombatTransitionTimer;
    int mCombatTransitionMode;

    //--Enemy List
    bool mIsAnEnemyDying;
    int mEnemyTarget;
    int mDyingEnemyTimer;
    int mDyingEnemySlot;
    float cEnemyRenderPos[5][5];
    SugarLinkedList *mCombatEnemyList; //CombatEnemyPack

    //--Combat Ending
    int mEndingPhase;
    int mTargetDyingTimer;

    //--Player Properties
    int mPlayerHP;
    int mPlayerHPMax;
    int mPotions;
    int mHealingTimer;
    bool mLastObjectWasTail;
    PlayerUIObject *rLastHeart;
    SugarLinkedList *mPlayerShieldList;
    SugarLinkedList *mPlayerHeartList;
    SugarLinkedList *mDiscardedUIObjects;
    StarlightColor mElementalColors[TL_ELEMENTS_TOTAL];

    //--Player's Deck
    float mDeckPower;
    float mDeckPowerGainPerTick;
    float mDeckPowerMax;
    float mDeckPowerPerCard;
    int mPlayerMaxHandSize;
    int mAceTimer;
    WordCard *mAceCard;
    SugarLinkedList *mGlobalCardDeck;//Holds master copies of the below cards
    SugarLinkedList *mrPlayerHand;
    SugarLinkedList *mrPlayerDeck;
    SugarLinkedList *mrPlayerGraveyard;
    float mHandPositionsX[WC_MAX_CARDS_IN_HAND];

    //--Shuffle Handling
    bool mIsShuffling;
    int mShuffleTimer;
    int mShuffleTimerMax;
    int mShuffleHighlightTimer;
    int mShuffleRotateTimer;

    //--Click Handling
    int mClickState;
    int mClickTimer;
    int mSwapCard;
    int mSwapCardTimer;
    int mDisplayPower;
    WordCard *rClickedCard;
    TwoDimensionReal mAceButton;
    TwoDimensionReal mActButton;
    TwoDimensionReal mDrawButton;
    TwoDimensionReal mShuffleButton;
    TwoDimensionReal mSurrenderButton;
    TwoDimensionReal mPotionButton;

    //--Sentence Handler
    bool mDestinationActivities[WC_DESTINATIONS_MAX];
    TwoDimensionReal mDestinationButtons[WC_DESTINATIONS_MAX];
    float mDestinationPositions[WC_DESTINATIONS_MAX];
    WordCard *rSentenceHand[WC_DESTINATIONS_MAX];
    int mSentenceResultTimer;
    SugarLinkedList *mSentenceResultList;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            SugarFont *rFontMenuSize;
            SugarFont *rFontMenuHeader;
            SugarFont *rFontMenuCards;

            //--UI Parts
            SugarBitmap *rAttackBarFrame;
            SugarBitmap *rAttackBarFill;
            SugarBitmap *rBarHeart;
            SugarBitmap *rBarShield;
            SugarBitmap *rBarShieldOrbit;
            SugarBitmap *rBtnAce;
            SugarBitmap *rBtnAct;
            SugarBitmap *rBtnDraw;
            SugarBitmap *rBtnShuffle;
            SugarBitmap *rBtnShuffleHighlight;
            SugarBitmap *rBtnSurrender;
            SugarBitmap *rBtnPotion;
            SugarBitmap *rCardAnd;
            SugarBitmap *rCardOf;
            SugarBitmap *rCardAndOfHighlight;
            SugarBitmap *rCardAttack;
            SugarBitmap *rCardDeath;
            SugarBitmap *rCardDefend;
            SugarBitmap *rCardEarth;
            SugarBitmap *rCardFire;
            SugarBitmap *rCardJoker;
            SugarBitmap *rCardHighlight;
            SugarBitmap *rCardLife;
            SugarBitmap *rCardWater;
            SugarBitmap *rCardWind;
            SugarBitmap *rExpandableL;
            SugarBitmap *rExpandableM;
            SugarBitmap *rExpandableR;
            SugarBitmap *rHealthBarFrame;
            SugarBitmap *rHealthBarFill;
            SugarBitmap *rIconAttack;
            SugarBitmap *rIconDeath;
            SugarBitmap *rIconDefend;
            SugarBitmap *rIconEarth;
            SugarBitmap *rIconFire;
            SugarBitmap *rIconLife;
            SugarBitmap *rIconWater;
            SugarBitmap *rIconWind;
            SugarBitmap *rStaticDivider;
        }Data;
    }Images;

    protected:

    public:
    //--System
    WordCombat();
    ~WordCombat();
    void Initialize();
    void Finalize();

    //--Public Variables
    //--Property Queries
    int GetCombatResolution();
    int GetPlayerHP();
    int GetPotions();

    //--Manipulators
    void SetActiveCombat();
    void SetTurnCombat();
    void SetPlayerProperties(int pHP, int pHPMax, int pPotions);
    void RegisterEnemy(int pHP, int pHPMax, const char *pImagePath);
    void RegisterAttackToLastEnemy(int pRollWeight, int pPower, int pWindupTicks, uint8_t pElementalFlags);
    void RegisterWeaknessToLastEnemy(int pWeakness, int pAmount);
    void RegisterNewCard(int pType, int pEffect, int pSeverity, const char *pWord, uint8_t pElementalFlags, bool pIsAce);

    //--Core Methods
    void BuildDestinationMarkers(WordCard *pCard);
    int ComputeActionPower(bool pExecuteAction);
    void ClickShuffleButton();
    void ClickDrawButton();
    void ClickActButton();
    void CreateWaterSlowdownIndicator(int pSlowTicks);
    void CreateFireDamageIndicator(int pFireDamage);
    void CreateWindDrawIndicator(int pFreeDraws);
    void CreateEarthShieldIndicator(int pShields);
    void CreateEarthSlowdownIndicator(int pSlowdown);

    //--Deck Handlers
    int CanPlaceCard(WordCard *pCard);
    int GetFirstUnusedSlot();
    bool AutoplaceCard(WordCard *pCard);
    void DrawCard();
    void BeginShuffle();
    void RegisterSentencePack(const char *pString, SugarBitmap *pImage, ...);
    void MoveSentenceCardToHand(int pIndex);
    void ShuffleDeck();
    void PositionCardsInHand();
    void RecomputeHighlights();

    //--Enemies
    void CreateEnemyProgressIndicator(int pTicks);
    void UpdateEnemies(bool pAllowTimeAdvance);
    void RenderEnemies(float pAlpha);

    //--UI Object Handling
    void AttackPlayer(CombatEnemyAttackPack *pAttackingPack);
    void GainShield(uint8_t pElementalFlags);
    void GainHearts(int pAmount, bool pHandleMoving);
    void TrimShields();
    void PositionShields();
    void PositionHearts();
    void UpdateUIObjects();
    void RenderUIObjects(float pAlpha);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();
    void RenderOutOverlays();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

