//--[WordCard]
//--A word in the game. The player can make sentences out of these to attack enemies or defend herself.
//  Each card on the field can be selected and moved around. They also have elemental properties that
//  modify the results when a sentence is built out of them.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
//--Types of Cards
#define WORDCARD_TYPE_LOWERBND 0
#define WORDCARD_TYPE_ACTION 0
#define WORDCARD_TYPE_MODIFIER 1
#define WORDCARD_TYPE_OF 2
#define WORDCARD_TYPE_AND 3
#define WORDCARD_TYPE_UPPERBND 3

//--Card Effects
#define WORDCARD_EFFECT_LOWERBND 0
#define WORDCARD_EFFECT_DAMAGE 0
#define WORDCARD_EFFECT_DEFEND 1
#define WORDCARD_EFFECT_HEAL 2
#define WORDCARD_EFFECT_UPPERBND 2

//--Movement Properties
#define WORDCARD_MOVESPEED 25.0f

//--[Classes]
class WordCard
{
    private:
    //--System
    int mCardType;
    int mHighlightFlag;
    SugarBitmap *rHighlightImg;
    char *mDisplayedWord;
    SugarBitmap *rBackingImg;
    SugarFont *rRenderFont;
    SugarBitmap *rBackingL;
    SugarBitmap *rBackingM;
    SugarBitmap *rBackingR;

    //--Joker Card!
    bool mIsJokerCard;

    //--Properties
    bool mAndOfFlag;
    uint8_t mElementalFlag;
    int mEffectType;
    int mEffectSeverity;

    //--Position
    int mMoveTicks;
    int mMoveTicksMax;
    int mAssignedIndex;
    float mStartX;
    float mStartY;
    float mTargetX;
    float mTargetY;
    float mXPad;
    float mYPad;
    TwoDimensionReal mDimensions;

    protected:

    public:
    //--System
    WordCard();
    ~WordCard();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    bool IsMoving();
    int GetHighlightFlag();
    int GetCardType();
    uint8_t GetElementalFlag();
    int GetEffectType();
    int GetEffectSeverity();
    float GetWidth();
    int GetAssignedIndex();
    TwoDimensionReal GetDimensions();
    const char *GetDisplayString();
    bool IsPointWithin(float pX, float pY);

    //--Manipulators
    void SetCardType(int pFlag);
    void SetHighlightFlag(int pFlag, SugarBitmap *pHighlightImg);
    void SetAndOfFlag();
    void SetDisplayedWord(const char *pWord);
    void SetBackingImage(SugarBitmap *pBitmap);
    void SetExpandableNameplates(SugarBitmap *pLft, SugarBitmap *pCnt, SugarBitmap *pRgt);
    void SetFont(SugarFont *pUseFont);
    void SetElements(uint8_t pElementalFlag);
    void SetEffectType(int pFlag);
    void SetEffectSeverity(int pSeverity);
    void SetAssignedIndex(int pIndex);
    void MoveToInstantly(float pX, float pY);
    void MoveToSlide(float pX, float pY);

    //--Core Methods
    void ComputeDimensions();

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render(bool pIsDarkened, bool pIsBlackout, float pAlpha);
    void RenderNoText(bool pIsDarkened, bool pIsBlackout, float pAlpha);
    void RenderHighlight();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

