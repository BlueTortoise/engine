//--Base
#include "PandemoniumLevel.h"

//--Classes
#include "Actor.h"
#include "ContextMenu.h"
#include "InventorySubClass.h"
#include "PandemoniumRoom.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "EntityManager.h"

void PandemoniumLevel::ShowContextMenu()
{
    mShowContextMenu = true;
}
void PandemoniumLevel::HideContextMenu()
{
    mShowContextMenu = false;
    mContextMenu->Reset();
}
void PandemoniumLevel::SetContextVisibility(bool pFlag)
{
    mShowContextMenu = pFlag;
    if(!pFlag) mContextMenu->Reset();
}
void PandemoniumLevel::SetContextPosition(int pLft, int pTop)
{
    mContextMenu->SetPosition(pLft, pTop);
}
void PandemoniumLevel::SetContextPositionAround(int pLft, int pTop)
{
    //--Given a location, positions the ContextMenu around the given flags. It will automatically
    //  position itself to avoid going off the screen's edge.
    //--Note: You should have already assembled the menu's contents so it knows how big it is. Call
    //  this last!
    float tWidth = mContextMenu->GetWidth();
    float tHeight = mContextMenu->GetHeight();
    if(pLft + tWidth  >= VIRTUAL_CANVAS_X) pLft = VIRTUAL_CANVAS_X - tWidth;
    if(pTop + tHeight >= VIRTUAL_CANVAS_Y) pTop = VIRTUAL_CANVAS_Y - tHeight;

    //--Edges. Call this second to make sure a super-wide context menu doesn't go off the screen.
    //  (It could happen...)
    if(pLft < 0) pLft = 0;
    if(pTop < 0) pTop = 0;

    //--Now position the menu.
    SetContextPosition(pLft, pTop);
}
void PandemoniumLevel::ShowContextMenuAround(int pIndex)
{
    //--Given a slot in the entities list, calls the ShowContextMenuAround() with that Actor.
    mContextMenu->ClearMenu();
    if(pIndex == -1) return;

    //--Make sure the player has an instance. They can't appear on their own entity list.
    Actor *rActingEntity = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rActingEntity) return;

    //--We need to know which room the entity is in to use the entity pane.
    PandemoniumRoom *rActiveRoom = rActingEntity->GetCurrentRoom();
    if(!rActiveRoom) return;

    //--Loop through the entities.
    SugarLinkedList *rEntityList = rActiveRoom->GetEntityList();
    Actor *rActor = (Actor *)rEntityList->PushIterator();
    while(rActor)
    {
        //--If the actor is NOT the acting one, decrement the index. Also, we can't target anything
        //  that has been obscured (for any reason).
        if(rActor != rActingEntity && !rActor->IsObscured())
        {
            //--Decrement.
            pIndex --;

            //--If the index goes negative, then this is the target.
            if(pIndex < 0)
            {
                ShowContextMenuAroundActor(rActor);
                rEntityList->PopIterator();
                return;
            }
        }

        //--Next.
        rActor = (Actor *)rEntityList->AutoIterate();
    }

    //--If we got this far, then the entity in question was not an Actor, but an InventoryPack.
    for(int i = 0; i < rActiveRoom->GetCompressedInventorySize(); i ++)
    {
        //--Decrement.
        InventoryPack *rItemPack = rActiveRoom->GetItemPack(i);
        if(!rItemPack || !rItemPack->rItem) continue;
        pIndex --;

        //--If the index goes negative, this is the target. It needs a different function since
        //  it's of a different type.
        if(pIndex < 0)
        {
            ShowContextMenuAroundItem(rItemPack->rItem);
            return;
        }
    }

    //--If we got this far, it's a WorldContainer, not an item.
    SugarLinkedList *rContainerList = rActiveRoom->GetContainerList();
    WorldContainer *rContainer = (WorldContainer *)rContainerList->PushIterator();
    while(rContainer)
    {
        //--Decrement.
        pIndex --;

        //--If the index goes negative, this is the target.
        if(pIndex < 0)
        {
            ShowContextMenuAroundContainer(rContainer);
            return;
        }
        rContainer = (WorldContainer *)rContainerList->AutoIterate();
    }

    //--If we got this far, it's an effect. This never has a context menu.
}
void PandemoniumLevel::ShowContextMenuAroundActor(Actor *pTarget)
{
    //--Given an Actor, builds a context menu around it. This is done internally. It then shows
    //  the context menu, or hides it if something went wrong.
    if(!pTarget) return;

    //--First, run the population routine.
    mContextMenu->PopulateAroundActor(pTarget);

    //--Now, if the list has no contents, hide everything.
    if(!mContextMenu->HasContents())
    {
        HideContextMenu();
        return;
    }

    //--If we got this far, the context menu has at least one entry. If so, we can now display it.
    ShowContextMenu();

    //--Place the context position around the left-most edge of the entity pane.
    SetContextPositionAround(EntityPane.cLft, EntityPane.cTop + (cFontSpacing * mSelectedEntity));
}
void PandemoniumLevel::ShowContextMenuAroundItem(InventoryItem *pTarget)
{
    //--If the selected Entity is actually an Item, then we need to put a different set of instructions
    //  in the context window. Strictly, these are to examine or pick the item up. That's it.
    if(!pTarget) return;

    //--Get the Player.
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerCharacter) return;

    //--Run the population routine. It's a constant routine since all items have the same
    //  basic properties (though the command itself may not be the same).
    bool tCannotTakeItems = (rPlayerCharacter->IsStunned() || rPlayerCharacter->IgnoresItems() || rPlayerCharacter->GetTeam() != TEAM_PLAYER);
    mContextMenu->PopulateAroundItem(pTarget, tCannotTakeItems);

    //--If it had no contents because of an error, hide everything.
    if(!mContextMenu->HasContents())
    {
        HideContextMenu();
        return;
    }

    //--If we got this far, the context menu has at least one entry. If so, we can now display it.
    ShowContextMenu();

    //--Place the context position around the left-most edge of the entity pane.
    SetContextPositionAround(EntityPane.cLft, EntityPane.cTop + (cFontSpacing * mSelectedEntity));
}
void PandemoniumLevel::ShowContextMenuAroundContainer(WorldContainer *pContainer)
{
    //--Given a WorldContainer, shows a context menu around it. This is different from the above two.
    //  Fortunately, containers can only be examined and opened, that's about it.
    if(!pContainer) return;

    //--Get the Player.
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerCharacter) return;

    //--Run the population routine. A constant routine like the others. It follows the same
    //  logic for taking items as items, so a monster player cannot open containers.
    bool tCannotTakeItems = (rPlayerCharacter->IsStunned() || rPlayerCharacter->IgnoresItems() || rPlayerCharacter->GetTeam() != TEAM_PLAYER);
    mContextMenu->PopulateAroundContainer(pContainer, tCannotTakeItems);

    //--If it had no contents because of an error, hide everything.
    if(!mContextMenu->HasContents())
    {
        HideContextMenu();
        return;
    }

    //--If we got this far, the context menu has at least one entry. If so, we can now display it.
    ShowContextMenu();

    //--Place the context position around the left-most edge of the entity pane.
    SetContextPositionAround(EntityPane.cLft, EntityPane.cTop + (cFontSpacing * mSelectedEntity));
}
