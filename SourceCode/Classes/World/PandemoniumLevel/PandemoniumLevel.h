//--[PandemoniumLevel]
//--A level in the game. Contains a series of rooms. Also contains some display
//  information.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"
#include "DisplayManager.h"

//--[Local Structures]
//--Represents a virtual button.
#define PL_VIS_TIMER_MAX 16
typedef struct
{
    //--Variables
    bool mIsDisabled;
    int mInstruction;
    float mX, mY;
    int mVisTimer;
    SugarBitmap *rRenderImg;

    //--Setup Function
    void Set(int pInstructionCode, float pX, float pY, SugarBitmap *pImage)
    {
        mIsDisabled = false;
        mInstruction = pInstructionCode;
        mX = pX;
        mY = pY;
        mVisTimer = 0;
        rRenderImg = pImage;
    }
}ButtonStruct;

//--Represents a virtual window.
typedef struct
{
    float cIndent;
    float cLft;
    float cTop;
    float cWid;
    float cHei;
}WindowStruct;

//--Represents a string on the console.
typedef struct ConsoleString
{
    char *mString;
    bool mHasBreak;
    float mTextSize;
    StarlightColor mColor;

    void Initialize()
    {
        mString = NULL;
        mHasBreak = false;
        mTextSize = 0.0f;
        mColor.SetRGBAF(0.0f, 0.0f, 0.0f, 1.0f);
    }
    static void DeleteThis(void *pPtr)
    {
        ConsoleString *rStringPack = (ConsoleString *)pPtr;
        free(rStringPack->mString);
        free(rStringPack);
    }
}ConsoleString;

//--Represents an Actor icon on the map.
typedef struct ActorIconPack
{
    //--System
    int mTimer;
    uint32_t mActorID;

    //--Rendering
    bool mIsVisible;
    bool mWasVisible;
    SugarBitmap *rActorIcon;
    StarlightColor mActorColor;

    //--Position
    float mCurX;
    float mCurY;
    float mStartX;
    float mStartY;
    int mStartZ;
    float mEndX;
    float mEndY;
    int mEndZ;

    //--System
    void Initialize()
    {
        //--System
        mTimer = 0;
        mActorID = 0;

        //--Rendering
        mIsVisible = false;
        mWasVisible = false;
        rActorIcon = NULL;
        mActorColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);

        //--Position
        mCurX = 0.0f;
        mCurY = 0.0f;
        mStartX = 0.0f;
        mStartY = 0.0f;
        mStartZ = -100;
        mEndX = 0.0f;
        mEndY = 0.0f;
        mEndZ = -100;
    }
}ActorIconPack;

//--Represents damage animation on the damage animation queue.
typedef struct
{
    //--Members
    bool mAnimatePoof;
    int mDamageTimer;
    int mDamageHP;
    int mDamageWP;
    Actor *rTargetRefPtr;
    SugarBitmap *rDamageImage;

    //--Functions
    void Clear()
    {
        mAnimatePoof = false;
        mDamageTimer = 0;
        mDamageHP = -1;
        mDamageWP = -1;
        rTargetRefPtr = NULL;
        rDamageImage = NULL;
    }
}DamageAnimPack;

//--[Local Definitions]
#define VIRTUAL_BUTTONS_TOTAL 7
#define MAP_VIEW_SIZE_X (VIRTUAL_CANVAS_X * 0.75f)
#define MAP_VIEW_SIZE_Y (VIRTUAL_CANVAS_Y * 0.75f)
#define ENTITIES_DISPLAY_PER_GROUP 7 //Note: The 0 counts for rendering, this actually renders 8 entities.

#define FOG_ACCY 48
#define FOG_CROSSFADE_TICKS 20
#define FOG_MAP 1500.0f

//--Damage Animation
#define PL_DAM_FLASH_TICKS 15
#define PL_DAM_TICKS_TOTAL 35
#define PL_DAM_POOF_WHITE_TICKS 15
#define PL_DAM_POOF_BLACK_TICKS 30
#define PL_DAM_POOF_TICKS (PL_DAM_POOF_WHITE_TICKS + PL_DAM_POOF_BLACK_TICKS + 5)

//--[Classes]
class PandemoniumLevel : public RootLevel
{
    protected:
    //--System
    //--Color Constants
    StarlightColor cTextColor;
    StarlightColor cSelectColor;
    StarlightColor cBorderColor;
    StarlightColor cInnerColor;

    //--Font Size Constants
    float cFontSize;
    float cFontSpacing;

    //--Button Constants
    TwoDimensionReal mSystemMenuBtn;
    TwoDimensionReal mCorrupterMenuBtn;

    //--Window Sizing Constants.
    float cUseScale;
    float cMapViewFactorX;
    float cMapViewFactorY;
    WindowStruct CharPane;
    WindowStruct EntityPane;
    WindowStruct ConsolePane;

    //--Inventory Sizing. Not all constants.
    int mInventoryMembers;
    static const float cInvOffset;

    //--Construction
    SugarLinkedList *mRoomPrototypeList;

    //--Final Layout
    int mRoomsTotal;
    PandemoniumRoom **mRooms;

    //--Virtual Buttons
    bool mHasSetupButtons;
    ButtonStruct mVirtualButtons[VIRTUAL_BUTTONS_TOTAL];

    //--Console Strings
    bool mIsConsoleWaitingOnKeypress;
    char *mPostExecScript;
    int mPostExecFiringCode;
    SugarLinkedList *mConsoleStrings;

    //--Status Strings
    int mStatusStringsTotal;
    char **mStatusStrings;

    //--Context Menu
    bool mShowContextMenu;
    ContextMenu *mContextMenu;

    //--Character Pane
    bool mExtraIsWeapon;
    bool mIsUseSelected;
    bool mIsDropSelected;
    int mSelectedInventorySlot;
    int mMaxInventorySlots;
    int mExtraInventorySlots;
    char *mActivePortraitName;
    SugarBitmap *rActivePortrait;

    //--Inventory Command
    SugarLinkedList *mInventoryList;

    //--Entity Pane
    int mSelectedEntity;
    int mMaxEntities;
    int mEntityScrollOffset;
    int mTotalValidEntities;
    bool mIsScrollbarClicked;
    int mScrollbarClickStart;
    int mScrollbarClickY;

    //--Menu Activity Stack
    bool mHasPendingClose;
    char *mSelfMenuPopulationScript;
    char *mSystemMenuPopulationScript;

    //--Corrupter Menu
    CorrupterMenu *mCorrupterMenu;
    char *mMagicMenuPopulationScript;
    TwoDimensionReal mUnrolledMenuDim;

    //--Actor Icon Animation Storage
    SugarLinkedList *mActorIconList;

    //--Damage Animation
    bool mIsDamageAnimating;
    SugarLinkedList *mDamageAnimationQueue;

    //--Underlay Maps
    int mUnderlayMapsTotal;
    int *mUnderlayZLevels;
    float *mUnderMapOffsetX;
    float *mUnderMapOffsetY;
    SugarBitmap **rUnderlayMaps;

    //--Fog of War Mesh
    int mFogTimer;
    float mOldFogIndices[FOG_ACCY][FOG_ACCY];
    float mFogIndices[FOG_ACCY][FOG_ACCY];

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            SugarBitmap *rNavBase;
            SugarBitmap *rNorth;
            SugarBitmap *rEast;
            SugarBitmap *rSouth;
            SugarBitmap *rWest;
            SugarBitmap *rUp;
            SugarBitmap *rDown;
            SugarBitmap *rSkip;
            SugarBitmap *rScrollBtnDn;
            SugarBitmap *rBackground;
            SugarBitmap *rHideConsole;
            SugarBitmap *rShowConsole;
            SugarFont *rUIFontMedium;
            SugarFont *rUIFontSmall;
            SugarFont *rConsoleFont;
        }Data;
    }Images;

    public:
    //--System
    PandemoniumLevel();
    virtual ~PandemoniumLevel();

    //--Public Variables
    static bool xNextLineHasBreak;
    static float xMapZoom;
    static bool xActorIconsMove;
    static int xActorMoveTicks;
    static bool xAnimateDamage;
    static bool xShow2DUnderlays;
    static bool xUseSmallConsoleFont;
    static bool xDisableDynamicFog;

    //--Property Queries
    bool IsWaitingOnKeypress();
    bool IsBlockingTurnChange();

    //--Manipulators
    void RegisterRoom(PandemoniumRoom *pRoom);
    void WaitOnKeypress();
    void WaitOnKeypress(const char *pCallScript, int pFiringCode);
    void SetSelfMenuScript(const char *pScriptPath);
    void SetSystemMenuScript(const char *pScriptPath);
    void SetMagicScript(const char *pScript);
    void SetTotalSystemLines(int pTotal);
    void SetSystemLine(int pSlot, const char *pString);

    //--Actor Animation
    ActorIconPack *FindIconPack(uint32_t pID);
    void AddActorToList(Actor *pActor, float pX, float pY);
    void RemoveActorPack(void *pPtr);
    bool RecheckVisibility(ActorIconPack *pPack);
    void UpdateActorIcons();
    void RenderActorIcons();

    //--Character Pane
    void SetupUIConstants();
    void SetActivePortraitName(const char *pName);
    void SetActivePortrait(SugarBitmap *pImage);
    void CharacterHandleItemUse(int pItemSlot, bool pIsUse, bool pIsDrop);
    virtual void RenderCharacterPane();
    void RenderCharacterPaneItem(const char *pName, const char *pUseString, bool pIsSelected, bool pHighlightUse, bool pHighlightDrop, float &sCursor);
    void RenderCharacterPaneItemNoDrop(const char *pName, const char *pUseString, bool pIsSelected, bool pHighlightUse, bool pHighlightDrop, float &sCursor);

    //--Console Pane
    void AppendToConsole(const char *pString);
    virtual void AppendToConsole(const char *pString, float pTextSize, StarlightColor pColor);
    static void AppendToConsoleStatic(const char *pString);
    static void AppendToConsoleStatic(const char *pString, float pTextSize, StarlightColor pColor);
    virtual void RenderConsolePane();

    //--Context Menu
    void ShowContextMenu();
    void HideContextMenu();
    void SetContextVisibility(bool pFlag);
    void SetContextPosition(int pLft, int pTop);
    void SetContextPositionAround(int pLft, int pTop);
    virtual void ShowContextMenuAround(int pIndex);
    void ShowContextMenuAroundActor(Actor *pTarget);
    void ShowContextMenuAroundItem(InventoryItem *pTarget);
    void ShowContextMenuAroundContainer(WorldContainer *pContainer);

    //--Damage Animation
    bool IsDamageAnimating();
    void BeginDamageAnimation(Actor *pTarget, int pHPLoss, int pWPLoss);
    void UpdateDamageAnimation();
    void RenderDamageAnimation();
    void SubRenderDamageAnimation(float pRenderX, float pRenderY);

    //--EntitiesPane
    virtual void RenderEntitiesPane();

    //--Fog of War
    void ClearFog();
    void AddBrightnessAt(float pX, float pY, float pAddValue);
    void RebuildFogInfo();
    void RenderFogOverlay();

    //--GUI Handling
    void SetupButtons();
    void RecheckDisabledCases();
    int EmulateClick(int pXStart, int pYStart, int pXEnd, int pYEnd);
    virtual void RenderNavigation();

    //--Inventory Handling
    void ReconstituteInventory();
    void RenderInventory(float &sCursorY);

    //--Menu Handling
    void FlagForClose();
    void PushSelfMenu();

    //--Underlays
    void AllocateUnderlays(int pTotal);
    void SetUnderlay(int pSlot, int pZLevel, float pOffsetX, float pOffsetY, const char *pBitmapPath);
    void RenderUnderlay(int pZLevel);

    //--Core Methods
    void FinalizeRooms();
    void SpawnItemIn(InventoryItem *pItem, const char *pRoomName);
    virtual void HandleNewTurn();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();
    void EffectPulse();

    //--File I/O
    //--Drawing
    void AddToRenderList(SugarLinkedList *pRenderList);
    virtual void Render();
    virtual void RenderWorld();
    static void RenderCircle(float pX, float pY, float pRadius, StarlightColor pInner, StarlightColor pOuter);
    static void RenderBorder(float pLft, float pTop, float pRgt, float pBot, float pBorderWid, StarlightColor pBorderCol, StarlightColor pInteriorColor);

    //--Pointer Routing
    PandemoniumRoom *GetRoom(const char *pName);
    CorrupterMenu *GetCorrupterMenu();

    //--Static Functions
    static PandemoniumLevel *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_PL_BeginLevelConstruction(lua_State *L);
int Hook_PL_AddRoom(lua_State *L);
int Hook_PL_FinalizeRooms(lua_State *L);
int Hook_PL_EndLevelConstruction(lua_State *L);
int Hook_PL_PushRoom(lua_State *L);
int Hook_PL_WriteToConsole(lua_State *L);
int Hook_PL_SetWaitForKeypress(lua_State *L);
int Hook_PL_SetMagicMenuScript(lua_State *L);
int Hook_PL_SetSelfMenuScript(lua_State *L);
int Hook_PL_SetSystemMenuScript(lua_State *L);
int Hook_PL_PopMenuStack(lua_State *L);
int Hook_PL_SetSystemLines(lua_State *L);
int Hook_PL_IsPlayerPresent(lua_State *L);
int Hook_PL_RunEffectPulse(lua_State *L);
int Hook_PL_BeginDamageAnimation(lua_State *L);
int Hook_PL_AllocateUnderlays(lua_State *L);
int Hook_PL_SetUnderlay(lua_State *L);
int Hook_PL_RebuildFogInfo(lua_State *L);
