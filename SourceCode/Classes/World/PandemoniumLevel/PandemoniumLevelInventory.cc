//--Base
#include "PandemoniumLevel.h"

//--Classes
#include "Actor.h"
#include "InventoryItem.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "EntityManager.h"

void PandemoniumLevel::ReconstituteInventory()
{
    //--Rebuilds the inventory display information. To save processor speed, this should only be called when
    //  an item is added or removed from the player's inventory.
    mInventoryMembers = 0;
    mInventoryList->ClearList();

    //--Setup.
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerCharacter) return;

    //--Unset the flag.
    rPlayerCharacter->SetReconstituteFlag(false);

    //--Go through the player's inventory...
    mMaxInventorySlots = rPlayerCharacter->GetInventorySize();
    for(int i = 0; i < mMaxInventorySlots; i ++)
    {
        //--Get the item in question.
        InventoryItem *rItem = rPlayerCharacter->GetItemBySlot(i);
        if(!rItem) continue;

        //--Scan the inventory list. Is there one with a matching name?
        InventoryPack *rCheckPack = (InventoryPack *)mInventoryList->GetElementByName(rItem->GetName());
        if(rCheckPack)
        {
            rCheckPack->mQuantity ++;
        }
        //--No match, create a new one.
        else
        {
            mInventoryMembers ++;
            SetMemoryData(__FILE__, __LINE__);
            InventoryPack *nNewPack = (InventoryPack *)starmemoryalloc(sizeof(InventoryPack));
            nNewPack->mQuantity = 1;
            nNewPack->rItem = rItem;
            nNewPack->mIsVisibleTimer = 0;
            nNewPack->mLastRenderSlot = -1;
            nNewPack->mLastSetVisible = 0;
            mInventoryList->AddElement(rItem->GetName(), nNewPack, &FreeThis);
        }
    }
}
void PandemoniumLevel::RenderInventory(float &sCursorY)
{
    //--Actually renders the inventory. This goes through the InventoryPacks on mInventoryList, not the player's actual inventory.

    //--Setup. Check if we need to reconstitute the inventory.
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
    if(rPlayerCharacter && rPlayerCharacter->GetInventoryReconstituteFlag() == true) ReconstituteInventory();

    //--Rendering.
    int i = 0;
    InventoryPack *rPack = (InventoryPack *)mInventoryList->PushIterator();
    while(rPack)
    {
        //--Buffering.
        char tBuffer[128];
        if(rPack->mQuantity > 1)
            sprintf(tBuffer, "%s x%i", rPack->rItem->GetName(), rPack->mQuantity);
        else
            sprintf(tBuffer, "%s", rPack->rItem->GetName());

        //--Render it.
        RenderCharacterPaneItem(tBuffer, rPack->rItem->GetUseString(), (i+mExtraInventorySlots == mSelectedInventorySlot), mIsUseSelected, mIsDropSelected, sCursorY);

        //--Next.
        i ++;
        rPack = (InventoryPack *)mInventoryList->AutoIterate();
    }
}
