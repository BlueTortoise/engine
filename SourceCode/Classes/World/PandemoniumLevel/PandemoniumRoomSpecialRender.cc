//--Base
#include "PandemoniumRoom.h"

//--Classes
#include "Actor.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//--[Property Queries]
bool PandemoniumRoom::IsActorWithTagPresent(int pTag)
{
    //--Returns true if at least one actor with the requested GetRenderFlag() is present. Does not check tag
    //  validity, just for a match.
    Actor *rActor = (Actor *)mEntityList->PushIterator();
    while(rActor)
    {
        if(rActor->GetRenderFlag() == pTag)
        {
            mEntityList->PopIterator();
            return true;
        }

        //--Next.
        rActor = (Actor *)mEntityList->AutoIterate();
    }

    //--No matches.
    return false;
}

//--[Drawing]
void PandemoniumRoom::RenderSpecialIcons()
{
    //--Renders symbols that are local to a room and are not subject to visibility. These include chest indicators,
    //  items, weapons, and static features like Harpy Nests and such.
    float cFactor = 0.37f; //For rendering just inside the room icon.
    float cOffset = 8.0f;

    //--Glyph of Power. Renders under everything else.
    if(IsActorWithTagPresent(RENDER_GLYPH))
    {
        RenderGlyphSymbol(0.0f, 0.0f);
    }

    //--At least one container is present. Render the container icon.
    if(GetContainersTotal() > 0)
    {
        RenderChestSymbol(cSquareInner * -cFactor, cSquareInner * -cFactor);
    }

    //--At least one item is present. Render the item icon.
    if(IsOneItemPresent())
    {
        RenderItemSymbol((cSquareInner * -cFactor) + cOffset, cSquareInner * -cFactor);
    }

    //--At least one weapon is present.
    if(IsOneWeaponPresent())
    {
        RenderWeaponSymbol((cSquareInner * -cFactor) + (cOffset*2.0f), cSquareInner * -cFactor);
    }

    //--Check for Golem Tube.
    if(IsActorWithTagPresent(RENDER_TUBE))
    {
        RenderTubeSymbol((cSquareInner * cFactor) + (cOffset*0.0f) - 1.0f, cSquareInner * cFactor + 1.0f);
    }

    //--Rilmani mirror.
    if(IsActorWithTagPresent(RENDER_MIRROR))
    {
        RenderMirrorSymbol((cSquareInner * cFactor) + (cOffset*0.0f) - 1.0f, cSquareInner * cFactor + 1.0f);
    }

    //--Harpy nest.
    if(IsActorWithTagPresent(RENDER_NEST))
    {
        RenderNestSymbol((cSquareInner * cFactor) + (cOffset*0.0f) - 1.0f, cSquareInner * cFactor + 1.0f);
    }

    //--Vampire Coffin
    if(IsActorWithTagPresent(RENDER_COFFIN))
    {
        RenderCoffinSymbol((cSquareInner * cFactor) + (cOffset*0.0f) - 1.0f, cSquareInner * cFactor + 1.0f);
    }
}
