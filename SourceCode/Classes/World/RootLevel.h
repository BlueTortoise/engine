//--[RootLevel]
//--Root of the level inheritance structure. This is here to simplify the MapManager's interactions
//  with levels. These are not compatible with the RootEntity structure.

#pragma once

#include "Definitions.h"
#include "Structures.h"
#include "IRenderable.h"

class RootLevel : public IRenderable
{
    private:

    protected:
    //--System
    char *mName;

    public:
    //--System
    RootLevel();
    virtual ~RootLevel();

    //--Public Variables
    //--Property Queries
    char *GetName();
    virtual bool GetClipAt(float pX, float pY, float pZ);

    //--Manipulators
    virtual void SetName(const char *pName);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    static RootLevel *ReadFileType(const char *pPath, int pType);

    //--Drawing
    virtual void AddToRenderList(SugarLinkedList *pRenderList);
    virtual void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
