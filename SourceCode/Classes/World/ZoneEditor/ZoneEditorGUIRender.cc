//--Base
#include "ZoneEditor.h"

//--Classes
#include "ZoneNode.h"

//--CoreClasses
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers

void ZoneEditor::RenderBox(TwoDimensionReal pDimensions, StarlightColor pInteriorColor, StarlightColor pBorderColor)
{
    //--Worker function, renders a bordered box around the given dimensions.
    float cInd = 3.0f;
    float cWid = pDimensions.GetWidth();
    float cHei = pDimensions.GetHeight();

    //--GL Setup.
    glDisable(GL_TEXTURE_2D);
    glTranslatef(pDimensions.mLft, pDimensions.mTop, 0.0f);

    //--Main box.
    glColor4f(pInteriorColor.r, pInteriorColor.g, pInteriorColor.b, pInteriorColor.a);
    glBegin(GL_QUADS);
        glVertex2f(0.0f, 0.0f);
        glVertex2f(cWid, 0.0f);
        glVertex2f(cWid, cHei);
        glVertex2f(0.0f, cHei);
    glEnd();

    //--Border.
    glColor4f(pBorderColor.r, pBorderColor.g, pBorderColor.b, pBorderColor.a);
    glBegin(GL_QUADS);
        glVertex2f(0.0f, 0.0f);
        glVertex2f(cWid, 0.0f);
        glVertex2f(cWid, cInd);
        glVertex2f(0.0f, cInd);
    glEnd();
    glBegin(GL_QUADS);
        glVertex2f(0.0f, cHei);
        glVertex2f(cWid, cHei);
        glVertex2f(cWid, cHei - cInd);
        glVertex2f(0.0f, cHei - cInd);
    glEnd();
    glBegin(GL_QUADS);
        glVertex2f(0.0f, 0.0f + cInd);
        glVertex2f(cInd, 0.0f + cInd);
        glVertex2f(cInd, cHei - cInd);
        glVertex2f(0.0f, cHei - cInd);
    glEnd();
    glBegin(GL_QUADS);
        glVertex2f(cWid - 0.0f, 0.0f + cInd);
        glVertex2f(cWid - cInd, 0.0f + cInd);
        glVertex2f(cWid - cInd, cHei - cInd);
        glVertex2f(cWid - 0.0f, cHei - cInd);
    glEnd();

    //--Clean up.
    glColor3f(1.0f, 1.0f, 1.0f);
    glTranslatef(-pDimensions.mLft, -pDimensions.mTop, 0.0f);
    glEnable(GL_TEXTURE_2D);
}
void ZoneEditor::RenderCommands(uint8_t pFlag)
{
    //--Worker function, renders all commands that match the given flag. Passing 0 will render all commands that don't match any flags.
    if(!mRenderFont) return;
    CommandPack *rCommandPack = (CommandPack *)mCommandsList->PushIterator();
    while(rCommandPack)
    {
        //--Render any command which matches.
        if(pFlag == rCommandPack->mRenderFlag)
        {
            //--If this is the highlighted command...
            if(rCommandPack == rHighlightedCommand)
            {
                glColor3f(1.0f, 1.0f, 0.0f);
            }
            //--No special case, render blue.
            else
            {
                glColor3f(1.0f, 1.0f, 1.0f);
            }

            //--Render.
            mRenderFont->DrawTextArgs(rCommandPack->mXPos, rCommandPack->mYPos, 0, 1.0f, "%s", rCommandPack->mText);
        }

        //--Next.
        rCommandPack = (CommandPack *)mCommandsList->AutoIterate();
    }
}
void ZoneEditor::RenderConfirmDialog()
{
    //--Renders the "Are You Sure" confirmation dialogue. Not the most complicated thing in the world. This box has fixed proportions.
    if(!mShowConfirmDialog || !mRenderFont) return;

    //--Subroutine does the rendering.
    RenderBox(mConfirmDialogCoords, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f), StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f));

    //--Render the base text.
    float cFontSize = mFontSize / mRenderFont->GetTextHeight();
    glColor3f(1.0f, 1.0f, 1.0f);
    float cXPos = mConfirmDialogCoords.mLft + (mConfirmDialogCoords.GetWidth() / 2.0f) - (mRenderFont->GetTextWidth("Are you sure?") / 2.0f * cFontSize);
    mRenderFont->DrawText(cXPos, mConfirmDialogCoords.mTop + 30.0f, 0, cFontSize, "Are you sure?");

    //--Render the commands.
    RenderCommands(ZE_RENDER_CONFIRM);
}
void ZoneEditor::RenderNodeEditor(ZoneNode *pNode)
{
    //--Renders a box in the middle of the screen which can be used to edit the current node. If no
    //  node is passed in, does nothing.
    static bool xAllowLists = false;
    if(!pNode || !mRenderFont) return;

    //--If we have a list, call that and be done with it.
    if(mNodeEditorHandle && !mIsRenamingMode && xAllowLists)
    {
        glCallList(mNodeEditorHandle);
        return;
    }

    //--Begin building a GLList.
    if(!mIsRenamingMode && xAllowLists)
    {
        mNodeEditorHandle = glGenLists(1);
        glNewList(mNodeEditorHandle, GL_COMPILE_AND_EXECUTE);
    }

    //--Subroutine does the rendering.
    RenderBox(mBoxCoords, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.3f), StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f));

    //--Render the commands. Some commands have special rendering cases, so we don't use the usual rendering
    //  function. This is because the nodes have variable numbers and some are renamed dynamically.
    CommandPack *rNamePack = NULL;
    CommandPack *rCommandPack = (CommandPack *)mCommandsList->PushIterator();
    while(rCommandPack)
    {
        //--Don't render any commands that don't need the Node Editor, those have already been rendered.
        if(rCommandPack->mRenderFlag != ZE_RENDER_NODE_EDITOR)
        {
            rCommandPack = (CommandPack *)mCommandsList->AutoIterate();
            continue;
        }

        //--If this is the highlighted command, render it in yellow.
        if(rCommandPack == rHighlightedCommand)
        {
            glColor3f(1.0f, 1.0f, 0.0f);
        }
        //--No special case, render light blue.
        else
        {
            glColor3f(0.5f, 1.0f, 1.0f);
        }

        //--If the name of the command is "Name of Node", we actually render the name of the active node.
        //  This is used during the renaming sequence as well.
        if(!strcasecmp(rCommandPack->mText, "Name of Node"))
        {
            //--Store.
            rNamePack = rCommandPack;

            //--While a command, this always renders white.
            if(rCommandPack != rHighlightedCommand) glColor3f(1.0f, 1.0f, 1.0f);

            //--Normal case:
            if(!mIsRenamingMode)
            {
                mRenderFont->DrawTextArgs(rCommandPack->mXPos, rCommandPack->mYPos, 0, 1.0f, "%s", pNode->GetName());
            }
            //--Renaming case. Print the renaming string instead, and a '|'
            else
            {
                if(Global::Shared()->gTicksElapsed % 10 < 5)
                    mRenderFont->DrawTextArgs(rCommandPack->mXPos, rCommandPack->mYPos, 0, 1.0f, "%s", mRenameBuffer);
                else
                    mRenderFont->DrawTextArgs(rCommandPack->mXPos, rCommandPack->mYPos, 0, 1.0f, "%s|", mRenameBuffer);
            }
        }
        //--Normal case.
        else
        {
            //--Check if this is one of the specially named cases. If so, we change the name of the command.
            bool tRendered = false;
            char tFacingBuffer[32];
            int tFacingsTotal = pNode->GetFacingsTotal();
            for(int i = 0; i < tFacingsTotal; i ++)
            {
                //--Write, and check the pointer.
                FacingInfo *rFacingData = pNode->GetFacing(i);
                sprintf(tFacingBuffer, "Facing %c", i + 'A');

                //--Is this the same as the name of the command? If so, rename the command.
                if(!strcasecmp(tFacingBuffer, rCommandPack->mText) && rFacingData)
                {
                    //--Render as the rotations of the facing.
                    if(rCommandPack != rHighlightedCommand) glColor3f(1.0f, 1.0f, 1.0f);

                    //--Case: Doesn't go to a node.
                    if(rFacingData->mNodeDestination == 0)
                    {
                        sprintf(tFacingBuffer, "Facing: %3.0f %3.0f %3.0f", rFacingData->mXRotation, rFacingData->mYRotation, rFacingData->mZRotation);
                    }
                    //--Goes to a node, put that at the end.
                    else
                    {
                        sprintf(tFacingBuffer, "Facing: %3.0f %3.0f %3.0f -> %2i", rFacingData->mXRotation, rFacingData->mYRotation, rFacingData->mZRotation, rFacingData->mNodeDestination);
                    }

                    //--Render.
                    mRenderFont->DrawText(rCommandPack->mXPos, rCommandPack->mYPos, 0, tFacingBuffer);

                    //--Don't need to check the rest of the commands.
                    tRendered = true;
                    break;
                }
            }

            //--Check if this is one of the automatic transition commands.
            int tTransitionsTotal = pNode->GetAutoTransitionsTotal();
            if(tRendered) tTransitionsTotal = 0; //Skip the loop if we already rendered.
            for(int i = 0; i < tTransitionsTotal; i ++)
            {
                //--Write, and check.
                AutoTransitionInfo *rInfo = pNode->GetAutoTransition(i);
                sprintf(tFacingBuffer, "Trans %c", i + 'A');

                //--Is this the same as the name of the command? If so, rename the command.
                if(!strcasecmp(tFacingBuffer, rCommandPack->mText) && rInfo)
                {
                    //--Change color if this is the highlighted pack.
                    if(rCommandPack != rHighlightedCommand) glColor3f(1.0f, 1.0f, 1.0f);

                    //--Name.
                    sprintf(tFacingBuffer, "%i -> %i", rInfo->mPreviousNode, rInfo->mNodeDestination);

                    //--If this is a teleport transition, mark that.
                    if(rInfo->mIsTeleport) strcat(tFacingBuffer, " (T) ");

                    //--Render.
                    mRenderFont->DrawText(rCommandPack->mXPos, rCommandPack->mYPos, 0, tFacingBuffer);

                    //--Don't need to check the rest of the commands.
                    tRendered = true;
                    break;
                }
            }

            //--Not one of the special cases.
            if(!tRendered)
            {
                mRenderFont->DrawText(rCommandPack->mXPos, rCommandPack->mYPos, 0, rCommandPack->mText);
            }
        }

        //--Next.
        rCommandPack = (CommandPack *)mCommandsList->AutoIterate();
    }

    //--Render other information.
    float cFontSize = mFontSize / mRenderFont->GetTextHeight();
    if(rNamePack)
    {
        glColor3f(1.0f, 1.0f, 1.0f);
        mRenderFont->DrawTextArgs(rNamePack->mXPos, rNamePack->mYPos + (ZE_NAME_SIZE_PER*1.0f), 0, cFontSize * 0.75f, "Position: %5.0f %5.0f %5.0f", pNode->GetX(), pNode->GetY(), pNode->GetZ());
    }

    //--Render the auto-transition listing header.
    mRenderFont->DrawText(mBoxCoords.mRgt + ZE_NE_AUTOTRANS_X, mBoxCoords.mTop + ZE_NE_AUTOTRANS_Y - ZE_NAME_SIZE_PER, 0, cFontSize * 0.75f, "Auto Trans:");

    //--End the list. Don't capture the other routines here.
    if(!mIsRenamingMode && xAllowLists) glEndList();

    //--Subroutines that render notice boxes.
    RenderNodeSetFacing();
    RenderNodeSetTransition();
    RenderNodeDeletion();
}
void ZoneEditor::RenderNodeSetFacing()
{
    //--Renders a box advising the user to select either a facing or a node. Renders nothing if
    //  mIsNodeSelectionMode is ZE_NO_FACING_EDIT.
    if(mIsNodeSelectionMode == ZE_NO_FACING_EDIT || !mRenderFont) return;

    //--Set sizes.
    TwoDimensionReal tRenderBox;
    tRenderBox.SetWH(mBoxCoords.mLft - 75.0f, mBoxCoords.mTop - 25.0f, mBoxCoords.GetWidth() + 150.0f, 50.0f);

    //--Subroutine does the rendering.
    RenderBox(tRenderBox, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f), StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f));

    //--Text.
    glColor3f(1.0f, 1.0f, 1.0f);
    if(mIsNodeSelectionMode == ZE_MUST_SELECT_FACING)
    {
        mRenderFont->DrawText(tRenderBox.mLft + 15.0f, tRenderBox.mTop + 15.0f, 0, "Select the Facing to set...");
    }
    else if(mIsNodeSelectionMode == ZE_MUST_SELECT_NODE)
    {
        mRenderFont->DrawText(tRenderBox.mLft + 15.0f, tRenderBox.mTop + 15.0f, 0, "Select the Node to connect to...");
    }
}
void ZoneEditor::RenderNodeSetTransition()
{
    //--Renders a box advising the user to select a node as either the source or destination of an automatic
    //  transition. Does nothing if mIsAutoTransitionMode is ZE_AUTO_NONE.
    if(mIsAutoTransitionMode == ZE_AUTO_NONE || !mRenderFont) return;

    //--Set sizes.
    TwoDimensionReal tRenderBox;
    tRenderBox.SetWH(mBoxCoords.mLft - 75.0f, mBoxCoords.mTop - 25.0f, mBoxCoords.GetWidth() + 150.0f, 50.0f);

    //--Subroutine does the rendering.
    RenderBox(tRenderBox, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f), StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f));

    //--Text.
    glColor3f(1.0f, 1.0f, 1.0f);
    if(mIsAutoTransitionMode == ZE_AUTO_SELECT_SOURCE_NODE)
    {
        mRenderFont->DrawText(tRenderBox.mLft + 15.0f, tRenderBox.mTop + 15.0f, 0, "Select the Source Node...");
    }
    else if(mIsAutoTransitionMode == ZE_AUTO_SELECT_DEST_NODE)
    {
        mRenderFont->DrawText(tRenderBox.mLft + 15.0f, tRenderBox.mTop + 15.0f, 0, "Select the Destination Node...");
    }
}
void ZoneEditor::RenderNodeDeletion()
{
    //--Renders a box advising the user to select a node for deletion. Does nothing if mIsNodeDeletionMode is ZE_NDEL_NONE.
    if(mIsNodeDeletionMode == ZE_NDEL_NONE || !mRenderFont) return;

    //--Set sizes.
    TwoDimensionReal tRenderBox;
    tRenderBox.SetWH(mBoxCoords.mLft - 75.0f, mBoxCoords.mTop - 25.0f, mBoxCoords.GetWidth() + 150.0f, 50.0f);

    //--Subroutine does the rendering.
    RenderBox(tRenderBox, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f), StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f));

    //--Text.
    glColor3f(1.0f, 1.0f, 1.0f);
    mRenderFont->DrawText(tRenderBox.mLft + 15.0f, tRenderBox.mTop + 15.0f, 0, "Select the node to delete...");
}
