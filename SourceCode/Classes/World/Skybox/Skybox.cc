//--Base
#include "Skybox.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//=========================================== System ==============================================
Skybox::Skybox()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_SKYBOX;

    //--[Skybox]
    //--System
    //--Images
    memset(mOwnsImagesFlags, 0, sizeof(bool) * SKYBOX_TOTAL);
    memset(rCubeImages, 0, sizeof(SugarBitmap *) * SKYBOX_TOTAL);
}
Skybox::~Skybox()
{
    for(int i = 0; i < SKYBOX_TOTAL; i ++)
    {
        if(mOwnsImagesFlags[i]) delete rCubeImages[i];
    }
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void Skybox::SetImage(int pSlot, bool pTakeOwnership, SugarBitmap *pImage)
{
    //--Range check.
    if(pSlot < 0 || pSlot >= SKYBOX_TOTAL) return;

    //--Dealloc if image existed.
    if(mOwnsImagesFlags[pSlot]) delete rCubeImages[pSlot];
    mOwnsImagesFlags[pSlot] = false;
    rCubeImages[pSlot] = NULL;
    if(!pImage) return;

    //--Set.
    mOwnsImagesFlags[pSlot] = pTakeOwnership;
    rCubeImages[pSlot] = pImage;
}
void Skybox::SetImage(int pSlot, const char *pDLPath)
{
    //--Sets image as a reference from the DataLibrary. Cannot take ownership.
    SetImage(pSlot, false, (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath));
}
void Skybox::SetImagesFromPattern(const char *pPattern)
{
    //--Given a DL Path pattern, loads all six images from the DataLibrary.
    if(!pPattern) return;

    //--Pattern strings.
    const char *cStrings[] = {"N", "E", "S", "W", "U", "D"};

    //--Run.
    char tBuffer[256];
    for(int i = 0; i < SKYBOX_TOTAL; i ++)
    {
        sprintf(tBuffer, "%s%s", pPattern, cStrings[i]);
        SetImage(i, tBuffer);
    }
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void Skybox::Render()
{
    //--Basic skybox rendering, with no special frills. If stencilling is turned on, this function
    //  sure doesn't know it!
    glDepthMask(false);
    const float cDis = 2000.0f;
    const float cInd = 0.000f;

    //--North.
    if(rCubeImages[SKYBOX_N])
    {
        rCubeImages[SKYBOX_N]->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 0.0f); glVertex3f(-cDis, -cDis + cInd, -cDis);
            glTexCoord2f(1.0f, 0.0f); glVertex3f( cDis, -cDis + cInd, -cDis);
            glTexCoord2f(1.0f, 1.0f); glVertex3f( cDis, -cDis + cInd,  cDis);
            glTexCoord2f(0.0f, 1.0f); glVertex3f(-cDis, -cDis + cInd,  cDis);
        glEnd();
    }

    //--East.
    if(rCubeImages[SKYBOX_E])
    {
        rCubeImages[SKYBOX_E]->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 0.0f); glVertex3f( cDis - cInd, -cDis, -cDis);
            glTexCoord2f(1.0f, 0.0f); glVertex3f( cDis - cInd,  cDis, -cDis);
            glTexCoord2f(1.0f, 1.0f); glVertex3f( cDis - cInd,  cDis,  cDis);
            glTexCoord2f(0.0f, 1.0f); glVertex3f( cDis - cInd, -cDis,  cDis);
        glEnd();
    }

    //--South.
    if(rCubeImages[SKYBOX_S])
    {
        rCubeImages[SKYBOX_S]->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 0.0f); glVertex3f( cDis,  cDis - cInd, -cDis);
            glTexCoord2f(1.0f, 0.0f); glVertex3f(-cDis,  cDis - cInd, -cDis);
            glTexCoord2f(1.0f, 1.0f); glVertex3f(-cDis,  cDis - cInd,  cDis);
            glTexCoord2f(0.0f, 1.0f); glVertex3f( cDis,  cDis - cInd,  cDis);
        glEnd();
    }

    //--West.
    if(rCubeImages[SKYBOX_W])
    {
        rCubeImages[SKYBOX_W]->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 0.0f); glVertex3f(-cDis + cInd,  cDis, -cDis);
            glTexCoord2f(1.0f, 0.0f); glVertex3f(-cDis + cInd, -cDis, -cDis);
            glTexCoord2f(1.0f, 1.0f); glVertex3f(-cDis + cInd, -cDis,  cDis);
            glTexCoord2f(0.0f, 1.0f); glVertex3f(-cDis + cInd,  cDis,  cDis);
        glEnd();
    }

    //--Up.
    if(rCubeImages[SKYBOX_U])
    {
        rCubeImages[SKYBOX_U]->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 0.0f); glVertex3f(-cDis, -cDis, cDis - cInd);
            glTexCoord2f(1.0f, 0.0f); glVertex3f( cDis, -cDis, cDis - cInd);
            glTexCoord2f(1.0f, 1.0f); glVertex3f( cDis,  cDis, cDis - cInd);
            glTexCoord2f(0.0f, 1.0f); glVertex3f(-cDis,  cDis, cDis - cInd);
        glEnd();
    }

    //--Down.
    if(rCubeImages[SKYBOX_D])
    {
        rCubeImages[SKYBOX_D]->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 0.0f); glVertex3f(-cDis,  cDis, -cDis + cInd);
            glTexCoord2f(1.0f, 0.0f); glVertex3f( cDis,  cDis, -cDis + cInd);
            glTexCoord2f(1.0f, 1.0f); glVertex3f( cDis, -cDis, -cDis + cInd);
            glTexCoord2f(0.0f, 1.0f); glVertex3f(-cDis, -cDis, -cDis + cInd);
        glEnd();
    }

    //--Clean.
    glDepthMask(true);
}
void Skybox::RenderWithStencil(uint32_t pStencilFlag)
{
    //--Setup.
    glEnable(GL_STENCIL_TEST);
    glStencilFunc(GL_EQUAL, 0, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    glDisable(GL_DEPTH_TEST);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthMask(GL_TRUE);
    glStencilMask(0x00);

    //--Render.
    Render();

    //--Unset the stencils.
    glDisable(GL_STENCIL_TEST);
    glEnable(GL_DEPTH_TEST);
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
