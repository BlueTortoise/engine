//--Base
#include "AdventureLevelGenerator.h"

//--Classes
#include "ALGTile.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

void AdventureLevelGenerator::RollLocationNear(int pRollSlot, int &sX, int &sY)
{
    //--Rolls a location near the given point and returns it, or -1, -1 if no legal location could
    //  be found. A legal location must be an open floor.

    //--Make 20 attempts to roll a legal placement.
    for(int i = 0; i < 20; i ++)
    {
        //--Roll a location.
        int tRollX = (NormalizeRoll(rNoiseModule, X_QUERY + pRollSlot, Y_QUERY + i, Z_QUERY_ENEMY_PLACEMENT) * 10) - 5;
        int tRollY = (NormalizeRoll(rNoiseModule, X_QUERY + pRollSlot, Y_QUERY - i, Z_QUERY_ENEMY_PLACEMENT) * 10) - 5;

        //--Error check.
        int tQueryX = tRollX + sX;
        int tQueryY = tRollY + sY;
        if(tQueryX < 0 || tQueryX >= mXSize) continue;
        if(tQueryY < 0 || tQueryY >= mYSize) continue;
        if(!mMapData[tQueryX][tQueryY]->IsFloorTile()) continue;

        //--Success! Place and return.
        sX = tQueryX;
        sY = tQueryY;
        return;
    }

    //--Failed.
    sX = -1;
    sY = -1;
}
void AdventureLevelGenerator::PlaceEnemyNear(int pEnemySlot, int &sX, int &sY)
{
    //--Places an enemy near the given point, within a few tiles radially. Will fill in the sX and sY values with the final placement.
    //  Enemies will not be placed on clipped positions. It is possible for this function to fail.
    //--On failure, sX and sY will be populated with -1, -1.
    if(pEnemySlot < 0 || pEnemySlot >= PATHS_MAX) return;

    //--Roll the location.
    RollLocationNear(pEnemySlot, sX, sY);
    if(sX == -1 || sY == -1) return;

    //--Legal placement, place an enemy.
    mMapData[sX][sY]->mIsEnemySpawn[pEnemySlot] = true;
}
ALGEnemyPath *AdventureLevelGenerator::PlacePathsBetween(int pEnemySlot, int pStartX, int pStartY, int pEndX, int pEndY)
{
    //--[Documentation and Setup]
    //--Given a start and end point, builds the shortest path between these two points and marks it with path nodes.
    //  This process is similar to marking by range, but will stop as soon as it spots the end point.
    //--Creates and returns a path structure which the caller takes ownership of. The path structure is needed
    //  for building enemy path nodes when the map is in-engine.
    if(pEnemySlot < 0 || pEnemySlot >= PATHS_MAX) return NULL;
    SugarLinkedList *tPulseList = new SugarLinkedList(true);

    //--Increment the pulse counter.
    mPulse ++;
    mLongestDistanceLastPulse = 0;

    //--Get the X and Y positions of the room.
    if(pStartX < 0 || pStartX >= mXSize) return NULL;
    if(pStartY < 0 || pStartY >= mYSize) return NULL;

    //--Create the first point.
    SetMemoryData(__FILE__, __LINE__);
    IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
    nPulsePoint->mX = pStartX;
    nPulsePoint->mY = pStartY;
    nPulsePoint->mDistance = 1;
    tPulseList->AddElement("X", nPulsePoint, &FreeThis);

    //--Mark the zeroth room as the pulse start.
    mMapData[pStartX][pStartY]->mPulseCheck = mPulse;
    mMapData[pStartX][pStartY]->mPulseDistance = 0;

    //--[Iteration Cycle]
    //--Iterate until all pulses are extinguished.
    bool tFoundPath = false;
    IntPoint2D *rCurrentPosition = (IntPoint2D *)tPulseList->SetToHeadAndReturn();
    while(rCurrentPosition)
    {
        //--Fast-access variables.
        int cX = rCurrentPosition->mX;
        int cY = rCurrentPosition->mY;

        //--Clips fail to pulse.
        if(!mMapData[cX][cY]->IsFloorTile())
        {
            tPulseList->DeleteHead();
            rCurrentPosition = (IntPoint2D *)tPulseList->SetToHeadAndReturn();
            continue;
        }

        //--If the target point has been reached, stop iteration.
        if(cX == pEndX && cY == pEndY)
        {
            tFoundPath = true;
            break;
        }

        //--Check if this is greater than the previous max distance.
        if(rCurrentPosition->mDistance > mLongestDistanceLastPulse) mLongestDistanceLastPulse = rCurrentPosition->mDistance;

        //--North check:
        if(rCurrentPosition->mY > 0 && mMapData[cX][cY-1]->mPulseCheck < mPulse)
        {
            //--Mark the map.
            mMapData[cX][cY-1]->mPulseCheck = mPulse;
            mMapData[cX][cY-1]->mPulseDistance = rCurrentPosition->mDistance + 1;

            //--Generate a new point.
            SetMemoryData(__FILE__, __LINE__);
            IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
            nPulsePoint->mX = cX;
            nPulsePoint->mY = cY-1;
            nPulsePoint->mDistance = rCurrentPosition->mDistance + 1;
            tPulseList->AddElementAsTail("X", nPulsePoint, &FreeThis);
        }

        //--East check:
        if(rCurrentPosition->mX < mXSize - 1 && mMapData[cX+1][cY]->mPulseCheck < mPulse)
        {
            //--Mark the map.
            mMapData[cX+1][cY]->mPulseCheck = mPulse;
            mMapData[cX+1][cY]->mPulseDistance = rCurrentPosition->mDistance + 1;

            //--Generate a new point.
            SetMemoryData(__FILE__, __LINE__);
            IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
            nPulsePoint->mX = cX+1;
            nPulsePoint->mY = cY;
            nPulsePoint->mDistance = rCurrentPosition->mDistance + 1;
            tPulseList->AddElementAsTail("X", nPulsePoint, &FreeThis);
        }

        //--South check:
        if(rCurrentPosition->mY < mYSize - 1 && mMapData[cX][cY+1]->mPulseCheck < mPulse)
        {
            //--Mark the map.
            mMapData[cX][cY+1]->mPulseCheck = mPulse;
            mMapData[cX][cY+1]->mPulseDistance = rCurrentPosition->mDistance + 1;

            //--Generate a new point.
            SetMemoryData(__FILE__, __LINE__);
            IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
            nPulsePoint->mX = cX;
            nPulsePoint->mY = cY+1;
            nPulsePoint->mDistance = rCurrentPosition->mDistance + 1;
            tPulseList->AddElementAsTail("X", nPulsePoint, &FreeThis);
        }

        //--West check:
        if(rCurrentPosition->mX > 0 && mMapData[cX-1][cY]->mPulseCheck < mPulse)
        {
            //--Mark the map.
            mMapData[cX-1][cY]->mPulseCheck = mPulse;
            mMapData[cX-1][cY]->mPulseDistance = rCurrentPosition->mDistance + 1;

            //--Generate a new point.
            SetMemoryData(__FILE__, __LINE__);
            IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
            nPulsePoint->mX = cX-1;
            nPulsePoint->mY = cY;
            nPulsePoint->mDistance = rCurrentPosition->mDistance + 1;
            tPulseList->AddElementAsTail("X", nPulsePoint, &FreeThis);
        }

        //--Remove the 0th element.
        tPulseList->DeleteHead();
        rCurrentPosition = (IntPoint2D *)tPulseList->SetToHeadAndReturn();
    }

    //--Delete the pulse list.
    delete tPulseList;

    //--If no path was found, fail here.
    if(!tFoundPath) return NULL;

    //--[Path Builder]
    //--Create a linked-list to store the path information.
    SugarLinkedList *tPathList = new SugarLinkedList(true);

    //--A path was found. Starting at the end location, place path nodes and walk back to the start.
    int tCurX = pEndX;
    int tCurY = pEndY;
    int tPasses = 1000;
    while(tPasses > 0)
    {
        //--Subtract a pass.
        tPasses --;

        //--Set as path node.
        sprintf(mMapData[tCurX][tCurY]->mPathNodeName[pEnemySlot], "%c%02i", pEnemySlot + 'A', mMapData[tCurX][tCurY]->mPulseDistance);

        //--Store this position on the path list. We store it at the head, since iteration starts at the tail and goes backwards.
        SetMemoryData(__FILE__, __LINE__);
        IntPoint2D *nPoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
        nPoint->mX = tCurX;
        nPoint->mY = tCurY;
        tPathList->AddElementAsHead("X", nPoint, &FreeThis);

        //--Store distance.
        int tCurDistance = mMapData[tCurX][tCurY]->mPulseDistance;
        if(tCurDistance < 1) break;

        //--North:
        if(tCurY > 0 && mMapData[tCurX+0][tCurY-1]->IsFloorTile() && mMapData[tCurX+0][tCurY-1]->mPulseDistance <= tCurDistance - 1 && mMapData[tCurX+0][tCurY-1]->mPulseCheck == mPulse)
        {
            tCurY --;
        }
        //--East:
        else if(tCurX < mXSize-1 && mMapData[tCurX+1][tCurY+0]->IsFloorTile() && mMapData[tCurX+1][tCurY+0]->mPulseDistance <= tCurDistance - 1 && mMapData[tCurX+1][tCurY+0]->mPulseCheck == mPulse)
        {
            tCurX ++;
        }
        //--South:
        else if(tCurY < mYSize-1 && mMapData[tCurX+0][tCurY+1]->IsFloorTile() && mMapData[tCurX+0][tCurY+1]->mPulseDistance <= tCurDistance - 1 && mMapData[tCurX+0][tCurY+1]->mPulseCheck == mPulse)
        {
            tCurY ++;
        }
        //--West:
        else if(tCurX > 0 && mMapData[tCurX-1][tCurY+0]->IsFloorTile() && mMapData[tCurX-1][tCurY+0]->mPulseDistance <= tCurDistance - 1 && mMapData[tCurX-1][tCurY+0]->mPulseCheck == mPulse)
        {
            tCurX --;
        }
        //--No path!
        else
        {
            break;
        }
    }

    //--Allocate.
    SetMemoryData(__FILE__, __LINE__);
    ALGEnemyPath *nPathData = (ALGEnemyPath *)starmemoryalloc(sizeof(ALGEnemyPath));
    nPathData->mEntriesTotal = tPathList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    nPathData->mTilesX = (int *)starmemoryalloc(sizeof(int) * nPathData->mEntriesTotal);
    SetMemoryData(__FILE__, __LINE__);
    nPathData->mTilesY = (int *)starmemoryalloc(sizeof(int) * nPathData->mEntriesTotal);

    //--Now decompile the linked-list into a fixed-size structure.
    int i = 0;
    IntPoint2D *rPoint = (IntPoint2D *)tPathList->PushIterator();
    while(rPoint)
    {
        //--Copy across.
        nPathData->mTilesX[i] = rPoint->mX;
        nPathData->mTilesY[i] = rPoint->mY;

        //--Next.
        i ++;
        rPoint = (IntPoint2D *)tPathList->AutoIterate();
    }

    //--Clean.
    delete tPathList;
    return nPathData;
}
