//--[ALGRoom]
//--Represents a room in the AdventureLevelGenerator. These are purely abstract concepts, and are
//  not represented in the final tile product. Rooms are small, relatively open areas that can often
//  contain features such as items and enemies. Enemies usually patrol around rooms.
//--Rooms can connect to one another in some generation schemes. Rooms can also legally overlap. They
//  are usually circle-shaped but can agglomerate together to produce other shapes.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class ALGRoom
{
    public:
    //--System
    //--Data
    int mCenterXA;
    int mCenterYA;
    int mCenterXB;
    int mCenterYB;
    float mRadius;
    int mEnemyRoll;

    //--System
    ALGRoom();
    ~ALGRoom();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

