//--[AdventureLevelGenerator]
//--A random level generator that produces AdventureLevels. Requires a tilemap to be provided along
//  with tile UV coordinates to make everything look reasonable. Creates the level in much the same
//  way as importing a MapData.slf does.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"
#include "ALGTextureData.h"
#include "StarlightPerlin.h"

//--[Local Structures]
class ALGRoom;

//--Handles matching up textures to collisions.
typedef struct
{
    //--Members.
    bool mClipMatch[3][3];
    uint16_t mFastMatch;
    bool mIsFloor;
    TwoDimensionReal *rWallUVPtr;
    TwoDimensionReal *rWallHiUVPtr;
    TwoDimensionReal *rBlackoutUVPtr;

    //--Initialize.
    void Initialize()
    {
        for(int x = 0; x < 3; x ++)
        {
            for(int y = 0; y < 3; y ++)
            {
                mClipMatch[x][y] = true;
            }
        }
        mIsFloor = false;
        rWallUVPtr = NULL;
        rWallHiUVPtr = NULL;
        rBlackoutUVPtr = NULL;
    }

    //--Fast-Matcher
    static int BuildFastMatch(bool pGrid[3][3])
    {
        uint16_t tStartVal = 0x0000;
        if(pGrid[0][0]) tStartVal |= 0x0001;
        if(pGrid[1][0]) tStartVal |= 0x0002;
        if(pGrid[2][0]) tStartVal |= 0x0004;
        if(pGrid[0][1]) tStartVal |= 0x0008;
        if(pGrid[1][1]) tStartVal |= 0x0010;
        if(pGrid[2][1]) tStartVal |= 0x0020;
        if(pGrid[0][2]) tStartVal |= 0x0040;
        if(pGrid[1][2]) tStartVal |= 0x0080;
        if(pGrid[2][2]) tStartVal |= 0x0100;
        return tStartVal;
    }
}WallGrid;

//--Tile pathing structure
typedef struct
{
    int mX;
    int mY;
    int mDistance;
}IntPoint2D;

//--[Local Definitions]
//--Zeroing Cases
#define ZERO_DESTRUCTOR 0x01
#define ZERO_CONSTRUCTOR 0x02

//--Generation States
#define GENERATE_NONE 0
#define GENERATE_SIZES 1
#define GENERATE_BASELAYOUT 2
#define GENERATE_TUNNELER 3
#define GENERATE_PLACEEXITS 4
#define GENERATE_PLACETREASURE 5
#define GENERATE_PLACEENEMIES 6
#define GENERATE_BUILDTEXTURES 7

//--Pseudorandom roll offsets.
#define X_QUERY 0.55f
#define Y_QUERY 0.55f
#define Z_QUERY_X_SIZE 100.75
#define Z_QUERY_Y_SIZE 101.75
#define Z_QUERY_ROOM_COUNT 102.75f
#define Z_QUERY_ROOM_TYPE 103.75f
#define Z_QUERY_ROOM_POSITION 104.75f
#define Z_QUERY_ROOM_RADIUS 105.75f
#define Z_QUERY_FLOOR_TILE 106.75f
#define Z_QUERY_FLOOR_PIT 107.75f
#define Z_QUERY_SPAWNEXIT 108.75f
#define Z_QUERY_ISCAVERN 109.75f
#define Z_QUERY_ENEMY 110.75f
#define Z_QUERY_ENEMY_PLACEMENT 111.75f
#define Z_QUERY_ENEMY_PATHING 112.75f
#define Z_QUERY_CROSSBAR 113.75f
#define Z_QUERY_CROSSBAR_LEN 114.75f
#define Z_QUERY_CROSSBAR_ROTATION 115.75f
#define Z_QUERY_CROSSBAR_LINK 116.75f

//--Grid Cases
#define WALLGRIDS_TOTAL 256

//--Mark Tile Flags
#define ALLOW_PITS 0x01

//--Layer Definitions
#define LAYER_FLOOR_0 0
#define LAYER_FLOOR_1 1
#define LAYER_FLOOR_2 2
#define LAYER_WALLS_0 3
#define LAYER_WALLS_1 4
#define LAYER_WALLSHI_0 5
#define LAYER_WALLSHI_1 6
#define LAYER_BLACKOUT 7
#define LAYERS_TOTAL 8

//--[Structures]
typedef struct ALGEnemyPath
{
    int mEntriesTotal;
    int *mTilesX;
    int *mTilesY;

    static void DeleteThis(void *pPtr)
    {
        ALGEnemyPath *rPtr = (ALGEnemyPath *)pPtr;
        free(rPtr->mTilesX);
        free(rPtr->mTilesY);
        free(rPtr);
    }
}ALGEnemyPath;
typedef struct
{
    int mEntranceX;
    int mEntranceY;
    int mExitX;
    int mExitY;
    int mBonusObjectsTotal;
    int *mBonusObjectsX;
    int *mBonusObjectsY;
    int mEnemySpawnsTotal;
    int *mEnemySpawnsX;
    int *mEnemySpawnsY;
    int mEnemyPathsTotal;
    ALGEnemyPath *mEnemyPaths;
}ALGScriptData;

//--[Classes]
class AdventureLevelGenerator : public RootLevel
{
    private:
    //--System
    friend class AdventureLevel;
    friend class TiledLevel;
    int mPulse;
    int mGenerationState;
    int mLongestDistanceLastPulse;

    //--Noise Generator
    bool mIsEnteringSeed;
    char mSeedEntered[32];
    int mSeedFlashTimer;
    int32_t mSeedNumber;
    StarlightPerlin *rNoiseModule;

    //--Storage
    int mEntrancePosX;
    int mEntrancePosY;
    int mExitPosX;
    int mExitPosY;

    //--Room Listing
    int mRoomsTotal;
    ALGRoom **mRooms;

    //--Map Data
    int mXSize;
    int mYSize;
    ALGTile ***mMapData;

    //--Rendering
    int mPreviousMouseZ;
    float mRenderScale;
    float mXRenderOffset;
    float mYRenderOffset;

    //--Texturing
    SugarBitmap *rMapTexture;
    ALGTextureData mTextureData;
    WallGrid mWallGridData[WALLGRIDS_TOTAL];

    protected:

    public:
    //--System
    AdventureLevelGenerator();
    virtual ~AdventureLevelGenerator();
    void Reset();
    float NormalizeRoll(StarlightPerlin *pModule, float pX, float pY, float pZ);

    //--Public Variables
    static int xPathVis;
    static int xLastSeed;

    //--Static Storage
    static bool xIsScriptDataValid;
    static ALGScriptData xScriptData;

    //--Property Queries
    //--Manipulators
    void SizeMap(int pXSize, int pYSize);

    //--Core Methods
    void GenerateSizes();
    void GenerateBaseLayout();
    void GenerateTunnels();
    void PlaceExits();
    void PlaceTreasure();
    void PlaceEnemies();
    void DigBetween(int pXA, int pYA, int pXB, int pYB);

    //--Enemies
    void RollLocationNear(int pRollSlot, int &sX, int &sY);
    void PlaceEnemyNear(int pEnemySlot, int &sX, int &sY);
    ALGEnemyPath *PlacePathsBetween(int pEnemySlot, int pStartX, int pStartY, int pEndX, int pEndY);

    //--Exporter
    AdventureLevel *ExportToAdventureLevel();

    //--Texture Manipulators
    void SetTextureMap(const char *pPath);
    void SetPatternPosition(float pX, float pY);
    void AllocateFloors(int pTotal);
    void SetFloorUV(int pSlot, float pLft, float pTop);
    void SetBlackoutPosition(float pX, float pY);
    void SetLadderPosition(float pDnX, float pDnY, float pUpX, float pUpY);
    void SetCliffPosition(float pX, float pY);
    void SetFloorEdgingPosition(float pX, float pY);
    void SetTreasurePosition(float pX, float pY);
    void SetEnemyUV(float pX, float pY);

    //--Routines
    void MarkTilesByRange(int pX, int pY, uint8_t pSpecialFlag);
    void MarkTilesByRange(int pXA, int pYA, int pXB, int pYB, uint8_t pSpecialFlag);
    void MarkTilesByRange(SugarLinkedList *pList, uint8_t pSpecialFlag);
    void RangeIterator(SugarLinkedList *pIteratorList, uint8_t pSpecialFlag);
    bool AreAnyTilesDisconnected();

    //--Texturing
    void SetWallGridProperties(bool pGrid[3][3], int pWallCode, int pWallHiCode, int pBlackoutCode);
    void BuildTextures();
    void EdgeFloors();

    private:
    //--Private Core Methods

    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    virtual void AddToRenderList(SugarLinkedList *pRenderList);
    virtual void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdlevGenerator_Create(lua_State *L);
int Hook_AdlevGenerator_GetProperty(lua_State *L);
int Hook_AdlevGenerator_SetProperty(lua_State *L);

