//--Base
#include "AdventureLevelGenerator.h"

//--Classes
#include "ALGRoom.h"
#include "ALGTile.h"
#include "TiledLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"
#include "SugarFont.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"

//--[Local Definitions]
//--Rendering Data
#define ALGSCALE 1.00f

//--Padding
#define ALGPADDING 16

//=========================================== System ==============================================
AdventureLevelGenerator::AdventureLevelGenerator()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVENTURELEVELGENERATOR;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System
    mPulse = 0;
    mGenerationState = GENERATE_NONE;

    //--Noise Generator
    mIsEnteringSeed = false;
    mSeedFlashTimer = 0;
    mSeedEntered[0] = '\0';
    mSeedNumber = 0;
    if(!Global::Shared()->gDisallowPerlinNoise)
    {
        rNoiseModule = StarlightPerlin::xgStatic;
    }
    else
    {
        rNoiseModule = NULL;
    }

    //--Storage
    mEntrancePosX = 0;
    mEntrancePosY = 0;
    mExitPosX = 0;
    mExitPosY = 0;

    //--Room Listing
    mRoomsTotal = 0;
    mRooms = NULL;

    //--Map Data
    mXSize = 0;
    mYSize = 0;
    mMapData = NULL;

    //--Rendering
    mRenderScale = ALGSCALE;
    mPreviousMouseZ = -700;
    mXRenderOffset = 0.0f;
    mYRenderOffset = 0.0f;

    //--Texturing
    rMapTexture = NULL;
    mTextureData.Initialize();
    for(int i = 0; i < WALLGRIDS_TOTAL; i ++) mWallGridData[i].Initialize();

    //--[Construction]
    //--Build the wall grid listing.
    bool tFastAccessGrid[3][3];
    int tGridWrite = 0;
    for(int i = 0; i < 512; i ++)
    {
        //--Set the grid based on the i value.
        tFastAccessGrid[0][0] = i & 0x0001;
        tFastAccessGrid[1][0] = i & 0x0002;
        tFastAccessGrid[2][0] = i & 0x0004;
        tFastAccessGrid[0][1] = i & 0x0008;
        tFastAccessGrid[1][1] = i & 0x0010;
        tFastAccessGrid[2][1] = i & 0x0020;
        tFastAccessGrid[0][2] = i & 0x0040;
        tFastAccessGrid[1][2] = i & 0x0080;
        tFastAccessGrid[2][2] = i & 0x0100;

        //--Skip grids where the middle is open.
        if(tFastAccessGrid[1][1] == false) continue;

        //--Save this as a wall grid.
        mWallGridData[tGridWrite].mFastMatch = WallGrid::BuildFastMatch(tFastAccessGrid);
        tGridWrite ++;
        if(tGridWrite >= WALLGRIDS_TOTAL) break;
    }

    //--[Script Data]
    //--Clear the static data.
    if(xIsScriptDataValid)
    {
        //--Deallocate.
        free(xScriptData.mBonusObjectsX);
        free(xScriptData.mBonusObjectsY);
        free(xScriptData.mEnemySpawnsX);
        free(xScriptData.mEnemySpawnsY);
        for(int i = 0; i < xScriptData.mEnemyPathsTotal; i ++)
        {
            free(xScriptData.mEnemyPaths[i].mTilesX);
            free(xScriptData.mEnemyPaths[i].mTilesY);
        }
        free(xScriptData.mEnemyPaths);
    }

    //--Wipe data. It's now valid.
    memset(&xScriptData, 0, sizeof(ALGScriptData));
    xIsScriptDataValid = true;
}
AdventureLevelGenerator::~AdventureLevelGenerator()
{
    //--Room Listing.
    for(int i = 0; i < mRoomsTotal; i ++) delete mRooms[i];
    free(mRooms);

    //--Map Data.
    for(int x = 0; x < mXSize; x ++)
    {
        for(int y = 0; y < mYSize; y ++)
        {
            delete mMapData[x][y];
        }
        free(mMapData[x]);
    }
    free(mMapData);

    //--Texture data.
    free(mTextureData.mFloorUVs);
}
void AdventureLevelGenerator::Reset()
{
    //--[Documentation and Setup]
    //--Resets the class when prompted or when an illegal level is generated during automatic mode.
    //  The class reverts back to its state before any levels were generated, but after texture
    //  resolution occurred.

    //--[Deallocation]
    //--Room Listing.
    for(int i = 0; i < mRoomsTotal; i ++) delete mRooms[i];
    free(mRooms);

    //--Map Data.
    for(int x = 0; x < mXSize; x ++)
    {
        for(int y = 0; y < mYSize; y ++)
        {
            delete mMapData[x][y];
        }
        free(mMapData[x]);
    }
    free(mMapData);

    //--Clear the static data.
    if(xIsScriptDataValid)
    {
        //--Deallocate.
        free(xScriptData.mBonusObjectsX);
        free(xScriptData.mBonusObjectsY);
        free(xScriptData.mEnemySpawnsX);
        free(xScriptData.mEnemySpawnsY);
        for(int i = 0; i < xScriptData.mEnemyPathsTotal; i ++)
        {
            free(xScriptData.mEnemyPaths[i].mTilesX);
            free(xScriptData.mEnemyPaths[i].mTilesY);
        }
        free(xScriptData.mEnemyPaths);

        //--Wipe.
        memset(&xScriptData, 0, sizeof(ALGScriptData));
    }

    //--[Variable Reset]
    //--System
    mPulse = 0;
    mGenerationState = GENERATE_NONE;

    //--Noise Generator
    mSeedNumber = 0;
    if(!Global::Shared()->gDisallowPerlinNoise)
    {
        rNoiseModule = StarlightPerlin::xgStatic;
    }
    else
    {
        rNoiseModule = NULL;
    }

    //--Room Listing
    mRoomsTotal = 0;
    mRooms = NULL;

    //--Map Data
    mXSize = 0;
    mYSize = 0;
    mMapData = NULL;

    //--Rendering
    mXRenderOffset = 0.0f;
    mYRenderOffset = 0.0f;

    //--Texturing
    //rMapTexture = NULL;
    //mTextureData.Initialize();
}

//--[Worker Functions]
//--Normalizes a roll from the perlin generator. Normally these are -1.0f to 1.0f, this function
//  will normalize it to 0.0f to 1.0f.
float AdventureLevelGenerator::NormalizeRoll(StarlightPerlin *pModule, float pX, float pY, float pZ)
{
    //--Base.
    if(!pModule) return 0.0f;
    float tValue = (pModule->RollClamped(pX, pY, pZ));
    return tValue;
}

//--[Public Statics]
//--Which path is visible on the renderer. -1 means all paths.
int AdventureLevelGenerator::xPathVis = -1;

//--Last seed used by the generator.
int AdventureLevelGenerator::xLastSeed = 0;

//--Script data. Used by the constructor script to spawn enemies, treasure, and other hoo-hah.
bool AdventureLevelGenerator::xIsScriptDataValid = false;
ALGScriptData AdventureLevelGenerator::xScriptData;

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void AdventureLevelGenerator::SizeMap(int pXSize, int pYSize)
{
    //--Clear old data.
    for(int x = 0; x < mXSize; x ++)
    {
        for(int y = 0; y < mYSize; y ++)
        {
            delete mMapData[x][y];
        }
        free(mMapData[x]);
    }
    free(mMapData);

    //--Clear the static data.
    if(xIsScriptDataValid)
    {
        //--Deallocate.
        free(xScriptData.mBonusObjectsX);
        free(xScriptData.mBonusObjectsY);
        free(xScriptData.mEnemySpawnsX);
        free(xScriptData.mEnemySpawnsY);
        for(int i = 0; i < xScriptData.mEnemyPathsTotal; i ++)
        {
            free(xScriptData.mEnemyPaths[i].mTilesX);
            free(xScriptData.mEnemyPaths[i].mTilesY);
        }
        free(xScriptData.mEnemyPaths);

        //--Wipe.
        memset(&xScriptData, 0, sizeof(ALGScriptData));
    }

    //--Reset.
    mXSize = 0;
    mYSize = 0;
    mMapData = NULL;

    //--Minimum required level size is 10x10.
    if(pXSize < 10 || pYSize <  10) return;

    //--Size.
    mXSize = pXSize;
    mYSize = pYSize;

    //--Allocate and initialize.
    SetMemoryData(__FILE__, __LINE__);
    mMapData = (ALGTile ***)starmemoryalloc(sizeof(ALGTile **) * mXSize);
    for(int x = 0; x < mXSize; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        mMapData[x] = (ALGTile **)starmemoryalloc(sizeof(ALGTile *) * mYSize);
        for(int y = 0; y < mYSize; y ++)
        {
            mMapData[x][y] = new ALGTile();
        }
    }
}

//========================================= Core Methods ==========================================
void AdventureLevelGenerator::GenerateSizes()
{
    //--Error check.
    if(mGenerationState >= GENERATE_SIZES) return;

    //--If the seed value is nonzero, it was entered manually. So just use that.
    if(mSeedNumber != 0)
    {

    }
    //--Otherwise, pick a random number and use that.
    else
    {
        mSeedNumber = rand();
    }

    //--Boot the random generator with the given number.
    xLastSeed = mSeedNumber;
    if(rNoiseModule)
    {
        rNoiseModule->SetSeed(mSeedNumber);
        rNoiseModule->Boot();
    }

    //--Generate sizes based on the noise module. There is a certain range of acceptable sizes. We
    //  also pad the edges by two for camera and display reasons.
    float cMin = 25;
    float cMax = 75;
    int tXSize = cMin + (int)(NormalizeRoll(rNoiseModule, X_QUERY, Y_QUERY, Z_QUERY_X_SIZE) * (cMax - cMin));
    int tYSize = cMin + (int)(NormalizeRoll(rNoiseModule, X_QUERY, Y_QUERY, Z_QUERY_Y_SIZE) * (cMax - cMin));
    SizeMap(tXSize + ALGPADDING, tYSize + ALGPADDING);

    //--Store to stop repeats.
    mGenerationState = GENERATE_SIZES;

    //--Set offsets to center the level.
    float cScale = mRenderScale;
    mXRenderOffset = (VIRTUAL_CANVAS_X - ((mXSize * 16.0f) * cScale)) * 0.50f;
    mYRenderOffset = (VIRTUAL_CANVAS_Y - ((mYSize * 16.0f) * cScale)) * 0.50f;
}
void AdventureLevelGenerator::GenerateBaseLayout()
{
    //--[Documentation and Setup]
    //--Error check.
    if(mGenerationState <  GENERATE_SIZES)      return;
    if(mGenerationState >= GENERATE_BASELAYOUT) return;

    //--[Pit Creation]
    //--Iterate across the map and determine which parts are pit and which are not. This only
    //  affects clipped areas.
    float cSmoothingFactor = 0.20f;
    int cThreshold = 25;

    //--Roll for a cavern room. These rooms are almost entirely pits.
    int tCavernRoll = (int)(NormalizeRoll(rNoiseModule, X_QUERY, Y_QUERY, Z_QUERY_ISCAVERN) * 100.0f);
    if(tCavernRoll < 10)
    {
        cSmoothingFactor = 0.05f;
        cThreshold = 70;
    }

    //--Now place the pit properties.
    for(int x = 0; x < mXSize; x ++)
    {
        for(int y = 0; y < mYSize; y ++)
        {
            int tRoll = (int)(NormalizeRoll(rNoiseModule, (X_QUERY + x) * cSmoothingFactor, (Y_QUERY + y) * cSmoothingFactor, Z_QUERY_FLOOR_PIT) * 100.0f);
            if(tRoll < cThreshold)
            {
                mMapData[x][y]->mSpecialFloorFlag = SPECIAL_FLOOR_PIT;
            }
        }
    }

    //--[Room Creation]
    //--Constants.
    const int cIndentL = ALGPADDING / 2;
    const int cIndentR = ALGPADDING;

    //--Roll how many rooms we want.
    int cMinRooms = 12;
    int cMaxRooms = 25;
    mRoomsTotal = cMinRooms + (int)(NormalizeRoll(rNoiseModule, X_QUERY, Y_QUERY, Z_QUERY_ROOM_COUNT) * (float)(cMaxRooms - cMinRooms));
    SetMemoryData(__FILE__, __LINE__);
    mRooms = (ALGRoom **)starmemoryalloc(sizeof(ALGRoom *) * mRoomsTotal);
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        //--Create.
        mRooms[i] = new ALGRoom();

        //--Roll the room type.
        float tRoomType = NormalizeRoll(rNoiseModule, X_QUERY + i, Y_QUERY, Z_QUERY_ROOM_TYPE);

        //--Room is a circle.
        if(tRoomType >= 0.25f)
        {
            mRooms[i]->mCenterXA = cIndentL + (NormalizeRoll(rNoiseModule, X_QUERY + i, Y_QUERY + 0, Z_QUERY_ROOM_POSITION) * (mXSize - cIndentR));
            mRooms[i]->mCenterYA = cIndentL + (NormalizeRoll(rNoiseModule, X_QUERY + i, Y_QUERY + 1, Z_QUERY_ROOM_POSITION) * (mYSize - cIndentR));
            mRooms[i]->mCenterXB = mRooms[i]->mCenterXA;
            mRooms[i]->mCenterYB = mRooms[i]->mCenterYA;
            mRooms[i]->mRadius = 2.0f + NormalizeRoll(rNoiseModule, X_QUERY + i, Y_QUERY, Z_QUERY_ROOM_RADIUS) * 2.5f;
        }
        //--Room is an ellipse.
        else
        {
            //--Offsets.
            float tOffsetXRoll = (NormalizeRoll(rNoiseModule, X_QUERY + i, Y_QUERY + 2, Z_QUERY_ROOM_POSITION) * 5.0f);
            float tOffsetYRoll = (NormalizeRoll(rNoiseModule, X_QUERY + i, Y_QUERY + 3, Z_QUERY_ROOM_POSITION) * 5.0f);

            //--Normal.
            mRooms[i]->mCenterXA = cIndentL + (NormalizeRoll(rNoiseModule, X_QUERY + i, Y_QUERY + 0, Z_QUERY_ROOM_POSITION) * (mXSize - cIndentR));
            mRooms[i]->mCenterYA = cIndentL + (NormalizeRoll(rNoiseModule, X_QUERY + i, Y_QUERY + 1, Z_QUERY_ROOM_POSITION) * (mYSize - cIndentR));
            mRooms[i]->mCenterXB = mRooms[i]->mCenterXA + tOffsetXRoll;
            mRooms[i]->mCenterYB = mRooms[i]->mCenterYA + tOffsetYRoll;
            mRooms[i]->mRadius = 3.0f + NormalizeRoll(rNoiseModule, X_QUERY + i, Y_QUERY, Z_QUERY_ROOM_RADIUS) * 2.5f;
        }
    }

    //--Now that the rooms have been generated, run through the tile map and add their values together.
    for(int x = 0; x < mXSize; x ++)
    {
        for(int y = 0; y < mYSize; y ++)
        {
            //--Run across all rooms and check if any are within the radial distance.
            for(int i = 0; i < mRoomsTotal; i ++)
            {
                //--Check the A radius.
                float cRadialA = GetPlanarDistance(x, y, mRooms[i]->mCenterXA, mRooms[i]->mCenterYA);
                if(cRadialA < mRooms[i]->mRadius)
                {
                    mMapData[x][y]->mCollisionType = CLIP_NONE;
                    break;
                }

                //--Check the B radius.
                float cRadialB = GetPlanarDistance(x, y, mRooms[i]->mCenterXB, mRooms[i]->mCenterYB);
                if(cRadialB < mRooms[i]->mRadius)
                {
                    mMapData[x][y]->mCollisionType = CLIP_NONE;
                    break;
                }

                //--Check the midpoint radius.
                float cRadialC = GetPlanarDistance(x, y, (mRooms[i]->mCenterXA + mRooms[i]->mCenterXB) / 2.0f, (mRooms[i]->mCenterYA + mRooms[i]->mCenterYB) / 2.0f);
                if(cRadialC < mRooms[i]->mRadius)
                {
                    mMapData[x][y]->mCollisionType = CLIP_NONE;
                    break;
                }
            }
        }
    }

    //--[Cleanup]
    //--Clear the edges.
    for(int x = 0; x < mXSize; x ++)
    {
        //--Clip the walls.
        mMapData[x][0]->mCollisionType = CLIP_ALL;
        mMapData[x][1]->mCollisionType = CLIP_ALL;
        mMapData[x][mYSize-2]->mCollisionType = CLIP_ALL;
        mMapData[x][mYSize-1]->mCollisionType = CLIP_ALL;
    }
    for(int y = 0; y < mYSize; y ++)
    {
        //--Clip the walls.
        mMapData[0][y]->mCollisionType = CLIP_ALL;
        mMapData[1][y]->mCollisionType = CLIP_ALL;
        mMapData[mXSize-2][y]->mCollisionType = CLIP_ALL;
        mMapData[mXSize-1][y]->mCollisionType = CLIP_ALL;
    }

    //--Store to stop repeats.
    mGenerationState = GENERATE_BASELAYOUT;
}
void AdventureLevelGenerator::GenerateTunnels()
{
    //--Error check.
    if(mGenerationState <  GENERATE_BASELAYOUT) return;
    if(mGenerationState >= GENERATE_TUNNELER)   return;

    //--Storage. We will need these to generate crossbars later.
    SugarLinkedList *tTunnelStorageList = new SugarLinkedList(true);

    //--While all rooms are not connected:
    MarkTilesByRange(mRooms[0]->mCenterXA, mRooms[0]->mCenterYA, 0);
    while(AreAnyTilesDisconnected())
    {
        //--Variables.
        int tDisconnectedSlot = -1;
        int tConnectionSlot = -1;
        float tShortestDistance = 0.0f;

        //--Find the room which is disconnected.
        for(int i = 0; i < mRoomsTotal; i ++)
        {
            //--Fast-access variables.
            int cX = mRooms[i]->mCenterXA;
            int cY = mRooms[i]->mCenterYA;
            if(mMapData[cX][cY]->mPulseCheck < mPulse)
            {
                tDisconnectedSlot = i;
                break;
            }
        }

        //--If we somehow didn't have a disconnected room, stop here.
        if(tDisconnectedSlot == -1) break;

        //--Find the nearest room to the disconnected one which is connected.
        for(int i = 0; i < mRoomsTotal; i ++)
        {
            //--Fast-access variables.
            int cX = mRooms[i]->mCenterXA;
            int cY = mRooms[i]->mCenterYA;
            if(mMapData[cX][cY]->mPulseCheck < mPulse) continue;

            //--Get distance.
            float cDistance = GetPlanarDistance(mRooms[i]->mCenterXA, mRooms[i]->mCenterYA, mRooms[tDisconnectedSlot]->mCenterXA, mRooms[tDisconnectedSlot]->mCenterYA);

            //--If there are no other closer rooms, or the distance is shorted, mark this for connection.
            if(tConnectionSlot == -1 || cDistance < tShortestDistance)
            {
                tConnectionSlot = i;
                tShortestDistance = cDistance;
            }
        }

        //--If there was no possible connection or the slots were somehow the same, stop her.e
        if(tConnectionSlot == -1 || tConnectionSlot == tDisconnectedSlot) break;

        //--Fast access pointers.
        float cXA = mRooms[tConnectionSlot]->mCenterXA;
        float cYA = mRooms[tConnectionSlot]->mCenterYA;
        float cXB = mRooms[tDisconnectedSlot]->mCenterXA;
        float cYB = mRooms[tDisconnectedSlot]->mCenterYA;

        //--Store these variables in the linked-list.
        SetMemoryData(__FILE__, __LINE__);
        TwoDimensionReal *nTunnelDim = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));
        nTunnelDim->mLft = cXA;
        nTunnelDim->mTop = cYA;
        nTunnelDim->mRgt = cXB;
        nTunnelDim->mBot = cYB;
        tTunnelStorageList->AddElement("X", nTunnelDim, &FreeThis);

        //--Dig between the two spots.
        DigBetween(cXA, cYA, cXB, cYB);

        //--Run the marker again.
        MarkTilesByRange(mRooms[0]->mCenterXA, mRooms[0]->mCenterYA, 0);
    }

    //--[Crossbars]
    //--For each of the tunnels we dug, place crossbars. This must be done after all tunnels are placed
    //  to prevent the crossbars from becoming their own tunnels. This allows multiple paths to appear.
    //tTunnelStorageList->ClearList();
    TwoDimensionReal *rTunnelDim = (TwoDimensionReal *)tTunnelStorageList->PushIterator();
    while(rTunnelDim)
    {
        //--Fast-access pointers.
        float cDistancePerCrossbar = 12.0f;
        float cXA = rTunnelDim->mLft;
        float cYA = rTunnelDim->mTop;
        float cXB = rTunnelDim->mRgt;
        float cYB = rTunnelDim->mBot;
        float cDiffX = cXB - cXA;
        float cDiffY = cYB - cYA;
        float cDistance = GetPlanarDistance(cXA, cYA, cXB, cYB);
        float cAngle = atan2f(cDiffY, cDiffX);

        //--Roll some crossbars. We can legally roll zero.
        int tCrossbarsMax = cDistance / cDistancePerCrossbar;
        if(tCrossbarsMax < 1) tCrossbarsMax = 1;
        for(int i = 0; i < tCrossbarsMax; i ++)
        {
            //--Select a position along the line. This is not random, they are roughly evenly spaced.
            float cPercent = 1.0f / (float)(tCrossbarsMax + 2) * (float)(i + 1);
            float cRoughX = cXA + (cDiffX * cPercent);
            float cRoughY = cYA + (cDiffY * cPercent);

            //--Invert the angle with some scatter.
            float cAngleModPercent = (NormalizeRoll(rNoiseModule, X_QUERY, Y_QUERY, Z_QUERY_CROSSBAR_ROTATION) * 0.70f) + 0.60f;
            float cUseAngle = cAngle + (90.0f * TORADIAN * cAngleModPercent);

            //--Roll a length. Length is between 25% and 75% of the length of the hallway.
            float cLengthRoll = (NormalizeRoll(rNoiseModule, X_QUERY, Y_QUERY, Z_QUERY_CROSSBAR_LEN) * 0.30f) + 0.55f;
            float cLength = (cDistance * cLengthRoll);

            //--Start and end positions.
            float cXA = cRoughX - (cosf(cUseAngle) * cLength * 0.50f);
            float cYA = cRoughY - (sinf(cUseAngle) * cLength * 0.50f);
            float cXB = cRoughX + (cosf(cUseAngle) * cLength * 0.50f);
            float cYB = cRoughY + (sinf(cUseAngle) * cLength * 0.50f);

            //--Dig.
            DigBetween(cXA, cYA, cXB, cYB);

            //--Check if either of the end points are within cConnectDistance tiles of a room. If so,
            //  tunnel to them.
           // bool tLinkedToARoom = false;
            float cConnectDistance = 5.0f;
            for(int i = 0; i < mRoomsTotal; i ++)
            {
                //--Fast-access variables.
                int cXC = mRooms[i]->mCenterXA;
                int cYC = mRooms[i]->mCenterYA;
                float cDistanceToA = GetPlanarDistance(cXA, cYA, cXC, cYC);
                float cDistanceToB = GetPlanarDistance(cXB, cYB, cXC, cYC);
                if(cDistanceToA <= cConnectDistance)
                {
                    //tLinkedToARoom = true;
                    DigBetween(cXA, cYA, cXC, cYC);
                }
                if(cDistanceToB <= cConnectDistance)
                {
                    //tLinkedToARoom = true;
                    DigBetween(cXB, cYB, cXC, cYC);
                }
            }

            //--There is a 10% chance that the end of either part of the crossbar will join to the nearest room.
            float cALinksRoll = NormalizeRoll(rNoiseModule, X_QUERY, Y_QUERY, Z_QUERY_CROSSBAR_LINK);
            if(cALinksRoll <= 1.00f)
            {
                //--Iterate across the rooms. Find the closest one.
                int tClosestIndex = 0;
                float cClosestDistance = GetPlanarDistance(cXA, cYA, mRooms[0]->mCenterXA, mRooms[0]->mCenterYA);
                for(int i = 1; i < mRoomsTotal; i ++)
                {
                    //--Get distance.
                    float cDistance = GetPlanarDistance(cXA, cYA, mRooms[i]->mCenterXA, mRooms[i]->mCenterYA);

                    //--Compare.
                    if(cDistance < cClosestDistance)
                    {
                        cClosestDistance = cDistance;
                        tClosestIndex = i;
                    }
                }

                //--Dig.
                DigBetween(cXA, cYA, mRooms[tClosestIndex]->mCenterXA, mRooms[tClosestIndex]->mCenterYA);
            }

            //--B version.
            float cBLinksRoll = NormalizeRoll(rNoiseModule, X_QUERY, Y_QUERY, Z_QUERY_CROSSBAR_LINK);
            if(cBLinksRoll <= 1.0f)
            {
                //--Iterate across the rooms. Find the closest one.
                int tClosestIndex = 0;
                float cClosestDistance = GetPlanarDistance(cXB, cYB, mRooms[0]->mCenterXA, mRooms[0]->mCenterYA);
                for(int i = 1; i < mRoomsTotal; i ++)
                {
                    //--Get distance.
                    float cDistance = GetPlanarDistance(cXB, cYB, mRooms[i]->mCenterXA, mRooms[i]->mCenterYA);

                    //--Compare.
                    if(cDistance < cClosestDistance)
                    {
                        cClosestDistance = cDistance;
                        tClosestIndex = i;
                    }
                }

                //--Dig.
                DigBetween(cXB, cYB, mRooms[tClosestIndex]->mCenterXA, mRooms[tClosestIndex]->mCenterYA);
            }
        }

        //--Next.
        rTunnelDim = (TwoDimensionReal *)tTunnelStorageList->AutoIterate();
    }

    //--[Pit Trimming]
    //--Pits which never contact the main level need to be removed to simplify the layout. This
    //  function call will iterate over all floors and pits. Any disconnected pits are removed.
    MarkTilesByRange(mRooms[0]->mCenterXA, mRooms[0]->mCenterYA, ALLOW_PITS);
    for(int x = 0; x < mXSize; x ++)
    {
        for(int y = 0; y < mYSize; y ++)
        {
            if(mMapData[x][y]->IsPitTile() && mMapData[x][y]->mPulseCheck < mPulse)
            {
                mMapData[x][y]->mSpecialFloorFlag = SPECIAL_FLOOR_NONE;
            }
        }
    }

    //--[Clean and Finish]
    //--Deallocate.
    delete tTunnelStorageList;

    //--Store to stop repeats.
    mGenerationState = GENERATE_TUNNELER;
}
void AdventureLevelGenerator::DigBetween(int pXA, int pYA, int pXB, int pYB)
{
    //--Digs a line between the given points, clearing tiles. Always digs a path that is at least
    //  3 tiles wide.
    float cDiffX = pXB - pXA;
    float cDiffY = pYB - pYA;
    float cDistance = GetPlanarDistance(pXA, pYA, pXB, pYB);
    float cStepRate = 0.5f;
    float cAngle = atan2f(cDiffY, cDiffX);

    //--Compute how many moves and their starting point.
    float cMoves = cDistance / cStepRate;
    float tIterateX = pXA;
    float tIterateY = pYA;

    //--Iterate across and remove tiles.
    for(int p = 0; p < cMoves; p ++)
    {
        //--Compute the integer value of this point.
        int tSlotX = (int)tIterateX;
        int tSlotY = (int)tIterateY;

        //--Range check.
        if(tSlotX >= 0 && tSlotX < mXSize && tSlotY >= 0 && tSlotY < mYSize)
        {
            //--Unclip.
            mMapData[tSlotX][tSlotY]->UnclipForTunnel();

            //--Also unclips one adjacent slot on each side. This makes the line 'fat'.
            if(tSlotX-1 >= 0)     mMapData[tSlotX-1][tSlotY]->UnclipForTunnel();
            if(tSlotX+1 < mXSize) mMapData[tSlotX+1][tSlotY]->UnclipForTunnel();
            if(tSlotY-1 >= 0)     mMapData[tSlotX][tSlotY-1]->UnclipForTunnel();
            if(tSlotY+1 < mYSize) mMapData[tSlotX][tSlotY+1]->UnclipForTunnel();
        }

        //--Increment.
        tIterateX = tIterateX + (cosf(cAngle) * cStepRate);
        tIterateY = tIterateY + (sinf(cAngle) * cStepRate);
    }
}
void AdventureLevelGenerator::PlaceExits()
{
    //--[Documentation and Setup]
    //--Error check.
    if(mGenerationState <  GENERATE_TUNNELER)  return;
    if(mGenerationState >= GENERATE_PLACEEXITS) return;

    //--[Entrance Placement]
    //--Scan the level. Check the minimum and maximum X positions.
    int tMinimumX = -1;
    int tMaximumX = -1;
    for(int x = 0; x < mXSize; x ++)
    {
        //--If there is a Y space open, this is a valid location.
        for(int y = 0; y < mYSize; y ++)
        {
            if(mMapData[x][y]->mCollisionType == CLIP_NONE)
            {
                if(tMinimumX == -1) tMinimumX = x;
                tMaximumX = x;
            }
        }
    }
    if(tMaximumX - tMinimumX < 3) return;

    //--Randomly roll an X position. This is where the entrance is created.
    int tXPosition = tMinimumX + (NormalizeRoll(rNoiseModule, X_QUERY, Y_QUERY, Z_QUERY_FLOOR_TILE) * (float)(tMaximumX - tMinimumX));
    int tYPosition = 3;
    for(int y = 2; y < mYSize; y ++)
    {
        if(mMapData[tXPosition][y]->mCollisionType == CLIP_NONE)
        {
            //--Store.
            tYPosition = y;

            //--Mark this as the entrance tile.
            mMapData[tXPosition][y]->mIsEntrance = true;
            mEntrancePosX = tXPosition;
            mEntrancePosY = y;
            break;
        }
    }

    //--The tiles to the UL and UR of the entrance must be walls.
    mMapData[tXPosition-1][tYPosition-1]->mCollisionType = CLIP_ALL;
    mMapData[tXPosition+1][tYPosition-1]->mCollisionType = CLIP_ALL;

    //--All tiles within 3 radially to the exit cannot be pits.
    for(int x = tXPosition - 5; x < tXPosition + 5; x ++)
    {
        for(int y = tYPosition - 5; y < tYPosition + 5; y ++)
        {
            //--Range check to prevent crashes.
            if(x < 0 || y < 0 || x >= mXSize || y >= mYSize) continue;

            //--Radius check.
            if(GetPlanarDistance(tXPosition, tYPosition, x, y) < 3.0f)
            {
                mMapData[x][y]->mSpecialFloorFlag = SPECIAL_FLOOR_NONE;
            }
        }
    }

    //--[Exit Placement]
    //--Now mark all tiles by distance to this one. We need to place exits and items.
    MarkTilesByRange(tXPosition, tYPosition, 0);
    if(mLongestDistanceLastPulse < 1) return;

    //--Create a range for exits.
    int tRangeLow = (int)(mLongestDistanceLastPulse * 0.75f);
    if(tRangeLow < 0) return;

    //--Iterate across all tiles. Count all tiles that are greater than the minimum range away. These are
    //  the candidates to host an exit.
    int tCandidatesTotal = 0;
    for(int x = 0; x < mXSize; x ++)
    {
        for(int y = 0; y < mYSize-2; y ++)
        {
            //--Ignore.
            if(mMapData[x][y]->mCollisionType == CLIP_ALL) continue;

            //--A candidate must also have the tile beneath it, or two beneath it, be clipped.
            if(mMapData[x][y]->mPulseDistance >= tRangeLow && (mMapData[x][y+1]->mCollisionType == CLIP_ALL || mMapData[x][y+2]->mCollisionType == CLIP_ALL))
            {
                tCandidatesTotal ++;
            }
        }
    }
    if(tCandidatesTotal < 3) return;

    //--We may need to attempt this multiple times until an ideal candidate is found.
    int tAttempt = 0;
    while(true)
    {
        //--Roll a candidate.
        tAttempt ++;
        int tUseCandidate = (NormalizeRoll(rNoiseModule, X_QUERY, Y_QUERY + tAttempt, Z_QUERY_SPAWNEXIT) * (float)tCandidatesTotal);

        //--Iterate.
        bool tSuccess = true;
        for(int x = 2; x < mXSize-2; x ++)
        {
            for(int y = 2; y < mYSize-3; y ++)
            {
                //--Ignore illegal cases.
                if(mMapData[x][y]->mCollisionType == CLIP_ALL) continue;

                //--This is within range, so it's a candidate.
                if(mMapData[x][y]->mPulseDistance >= tRangeLow && (mMapData[x][y+1]->mCollisionType == CLIP_ALL || mMapData[x][y+2]->mCollisionType == CLIP_ALL))
                {
                    //--Decrement.
                    tUseCandidate --;

                    //--Ending case.
                    if(tUseCandidate < 1)
                    {
                        //--Scan across the area around the exit. If any of the tiles under it were tunnel
                        //  tiles, we can't use them since this might isolate part of the level.
                        bool tWasLegalCase = true;
                        for(int sx = x - 1; sx < x + 2; sx++)
                        {
                            for(int sy = y; sy < y + 3; sy++)
                            {
                                if(mMapData[sx][sy]->mWasTunnelTile)
                                {
                                    tWasLegalCase = false;
                                    sx = x + 2;
                                    sy = y + 3;
                                }
                            }
                        }

                        //--The exit must not have a collision to its north.
                        if(!mMapData[x][y-1]->IsFloorTile()) tWasLegalCase = false;

                        //--Failed.
                        if(!tWasLegalCase)
                        {
                            tSuccess = false;
                        }
                        //--Success! Spawn an exit.
                        else
                        {
                            //--Mark the tile.
                            mMapData[x][y]->mIsExit = true;
                            mExitPosX = x;
                            mExitPosY = y-1;

                            //--Mark the tiles beneath this one as pits.
                            for(int sx = x - 1; sx < x + 2; sx++)
                            {
                                for(int sy = y; sy < y + 3; sy++)
                                {
                                    mMapData[sx][sy]->mBackupCollisionType = mMapData[sx][sy]->mCollisionType;
                                    mMapData[sx][sy]->mBackupSpecialFloorFlag = mMapData[sx][sy]->mSpecialFloorFlag;
                                    mMapData[sx][sy]->mCollisionType = CLIP_ALL;
                                    mMapData[sx][sy]->mSpecialFloorFlag = SPECIAL_FLOOR_PIT;
                                }
                            }

                            //--Now run this algorithm to check if we cut any tiles off. If we did,
                            //  undo the effects.
                            MarkTilesByRange(mRooms[0]->mCenterXA, mRooms[0]->mCenterYA, 0);
                            if(AreAnyTilesDisconnected())
                            {
                                //--Unflag.
                                tSuccess = false;

                                //--Unmark the tile.
                                mMapData[x][y]->mIsExit = false;
                                mExitPosX = 0;
                                mExitPosY = 0;

                                //--Mark the tiles beneath this one as pits.
                                for(int sx = x - 1; sx < x + 2; sx++)
                                {
                                    for(int sy = y; sy < y + 3; sy++)
                                    {
                                        mMapData[sx][sy]->mCollisionType = mMapData[sx][sy]->mBackupCollisionType;
                                        mMapData[sx][sy]->mSpecialFloorFlag = mMapData[sx][sy]->mBackupSpecialFloorFlag;
                                    }
                                }
                            }
                        }

                        //--Break out of the loops if we placed a valid exit.
                        //if(tSuccess == true)
                        {
                            x = mXSize;
                            y = mYSize;
                        }
                    }
                }
            }
        }

        //--If the exit was placed, we're done.
        if(tSuccess) break;
    }

    //--[Clean Up]
    //--Store the results. The scripts will need these later.
    xScriptData.mEntranceX = mEntrancePosX;
    xScriptData.mEntranceY = mEntrancePosY;
    xScriptData.mExitX = mExitPosX;
    xScriptData.mExitY = mExitPosY;

    //--Store to stop repeats.
    mGenerationState = GENERATE_PLACEEXITS;
}
void AdventureLevelGenerator::PlaceTreasure()
{
    //--[Documentation and Setup]
    //--Error check.
    if(mGenerationState <  GENERATE_PLACEEXITS)    return;
    if(mGenerationState >= GENERATE_PLACETREASURE) return;

    //--[Distance Marker]
    //--Run the distance routine from the point of the exit.
    MarkTilesByRange(mExitPosX, mExitPosY, 0);

    //--Setup.
    int tCurX = mEntrancePosX;
    int tCurY = mEntrancePosY;
    SugarLinkedList *tShortestPathList = new SugarLinkedList(true);

    //--Now go to the entrance and walk downwards until we reach the exit position. We are creating a list
    //  that consitutes the shortest path between the two.
    int tPasses = 100000;
    while((tCurX != mExitPosX || tCurY != mExitPosY) && tPasses > 0)
    {
        //--Subtract a pass.
        tPasses --;

        //--Mark this position.
        SetMemoryData(__FILE__, __LINE__);
        IntPoint2D *nPoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
        nPoint->mX = tCurX;
        nPoint->mY = tCurY;
        tShortestPathList->AddElement("X", nPoint, &FreeThis);

        //--Find the adjacent tile which has this tile's distance -1.
        int tMyDistance = mMapData[tCurX][tCurY]->mPulseDistance;

        //--North:
        if(tCurY > 0 && mMapData[tCurX+0][tCurY-1]->IsFloorTile() && mMapData[tCurX+0][tCurY-1]->mPulseDistance <= tMyDistance - 1)
        {
            tCurY --;
        }
        //--East:
        else if(tCurX < mXSize-1 && mMapData[tCurX+1][tCurY+0]->IsFloorTile() && mMapData[tCurX+1][tCurY+0]->mPulseDistance <= tMyDistance - 1)
        {
            tCurX ++;
        }
        //--South:
        else if(tCurY < mYSize-1 && mMapData[tCurX+0][tCurY+1]->IsFloorTile() && mMapData[tCurX+0][tCurY+1]->mPulseDistance <= tMyDistance - 1)
        {
            tCurY ++;
        }
        //--West:
        else if(tCurX > 0 && mMapData[tCurX-1][tCurY+0]->IsFloorTile() && mMapData[tCurX-1][tCurY+0]->mPulseDistance <= tMyDistance - 1)
        {
            tCurX --;
        }
        //--No path!
        else
        {
            break;
        }
    }

    //--[Distance Marker Part Two]
    //--Now mark all the distances from the path. No treasure spawns along the shortest possible path.
    MarkTilesByRange(tShortestPathList, 0);

    //--[Candidate Placement]
    //--Run across the map and place treasure indicators on all tiles that are at least 17 tiles from the path.
    for(int x = 0; x < mXSize; x ++)
    {
        for(int y = 0; y < mYSize-2; y ++)
        {
            //--Ignore.
            if(mMapData[x][y]->mCollisionType == CLIP_ALL) continue;

            //--A candidate must also have the tile beneath it not be a wall. Pits are legal.
            if(mMapData[x][y]->mPulseDistance >= 17 && (mMapData[x][y+1]->mCollisionType == CLIP_NONE || mMapData[x][y+1]->mSpecialFloorFlag == SPECIAL_FLOOR_PIT))
            {
                mMapData[x][y]->mIsTreasureCandidate = true;
            }
        }
    }

    //--[Finalist Placement]
    //--Treasure candidates have been placed into "blocks" based on distance. Each block is basically
    //  an end point on the map away from the shortest path. We now need to settle on a single final
    //  treasure point within each contiguous block.
    SugarLinkedList *tTreasureList = new SugarLinkedList(true);
    while(true)
    {
        //--Run across the map and store the highest distance point with a candidate on it.
        int tHighestDistance = 0;
        int tFarX = 0;
        int tFarY = 0;
        for(int x = 2; x < mXSize-2; x ++)
        {
            for(int y = 2; y < mYSize-2; y ++)
            {
                //--Skip clipped tiles.
                if(!mMapData[x][y]->IsFloorTile()) continue;

                //--Must be a treasure candidate that is further than the current furthest.
                if(mMapData[x][y]->mIsTreasureCandidate && mMapData[x][y]->mPulseDistance > tHighestDistance && mMapData[x][y]->mPulseDistance >= mLongestDistanceLastPulse * 0.75f)
                {
                    tFarX = x;
                    tFarY = y;
                    tHighestDistance = mMapData[x][y]->mPulseDistance;
                }
            }
        }

        //--If no matches are found, we're done.
        if(tHighestDistance == 0) break;

        //--Place a finalist on this tile.
        mMapData[tFarX][tFarY]->mIsTreasureFinalist = true;

        //--Store it in the list.
        SetMemoryData(__FILE__, __LINE__);
        IntPoint2D *nTreasureLocation = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
        nTreasureLocation->mX = tFarX;
        nTreasureLocation->mY = tFarY;
        tTreasureList->AddElement("X", nTreasureLocation, &FreeThis);

        //--Now go to this tile and walk back to the shortest path. We add to the shortest path
        //  list as we do this.
        tCurX = tFarX;
        tCurY = tFarY;
        int tPasses = 1000;
        while(tPasses > 0)
        {
            //--Subtract a pass.
            tPasses --;

            //--Mark this position.
            SetMemoryData(__FILE__, __LINE__);
            IntPoint2D *nPoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
            nPoint->mX = tCurX;
            nPoint->mY = tCurY;
            tShortestPathList->AddElement("X", nPoint, &FreeThis);

            //--Store distance.
            int tCurDistance = mMapData[tCurX][tCurY]->mPulseDistance;
            if(tCurDistance < 1) break;

            //--North:
            if(tCurY > 0 && mMapData[tCurX+0][tCurY-1]->IsFloorTile() && mMapData[tCurX+0][tCurY-1]->mPulseDistance <= tCurDistance - 1)
            {
                tCurY --;
            }
            //--East:
            else if(tCurX < mXSize-1 && mMapData[tCurX+1][tCurY+0]->IsFloorTile() && mMapData[tCurX+1][tCurY+0]->mPulseDistance <= tCurDistance - 1)
            {
                tCurX ++;
            }
            //--South:
            else if(tCurY < mYSize-1 && mMapData[tCurX+0][tCurY+1]->IsFloorTile() && mMapData[tCurX+0][tCurY+1]->mPulseDistance <= tCurDistance - 1)
            {
                tCurY ++;
            }
            //--West:
            else if(tCurX > 0 && mMapData[tCurX-1][tCurY+0]->IsFloorTile() && mMapData[tCurX-1][tCurY+0]->mPulseDistance <= tCurDistance - 1)
            {
                tCurX --;
            }
            //--No path!
            else
            {
                break;
            }
        }

        //--Re-mark all the tiles by shortest path again.
        MarkTilesByRange(tShortestPathList, 0);
    }

    //--[Clean Up]
    //--Store to stop repeats.
    mGenerationState = GENERATE_PLACETREASURE;

    //--Allocate.
    xScriptData.mBonusObjectsTotal = tTreasureList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    xScriptData.mBonusObjectsX = (int *)starmemoryalloc(sizeof(int) * xScriptData.mBonusObjectsTotal);
    SetMemoryData(__FILE__, __LINE__);
    xScriptData.mBonusObjectsY = (int *)starmemoryalloc(sizeof(int) * xScriptData.mBonusObjectsTotal);

    //--Store the locations of the treasures in the static structure.
    int i = 0;
    IntPoint2D *rHolder = (IntPoint2D *)tTreasureList->PushIterator();
    while(rHolder)
    {
        //--Store.
        xScriptData.mBonusObjectsX[i] = rHolder->mX;
        xScriptData.mBonusObjectsY[i] = rHolder->mY;

        //--Next.
        i ++;
        rHolder = (IntPoint2D *)tTreasureList->AutoIterate();
    }
}
void AdventureLevelGenerator::PlaceEnemies()
{
    //--[Documentation and Setup]
    //--Error check.
    if(mGenerationState <  GENERATE_PLACETREASURE) return;
    if(mGenerationState >= GENERATE_PLACEENEMIES)  return;
    if(mRoomsTotal < 1) return;

    //--Setup.
    int tEnemyCounter = 0;

    //--[Roll]
    //--Roll how many enemies we should attempt to place. Not all will get placed if invalid spots
    //  get rolled, and the final version of the level may not use all of the spawns.
    int cMinEnemies = mRoomsTotal * 0.60f;
    int cMaxEnemies = mRoomsTotal * 1.20f;
    int tEnemiesToPlaceForRooms = cMinEnemies + (NormalizeRoll(rNoiseModule, X_QUERY, Y_QUERY, Z_QUERY_ENEMY) * (cMaxEnemies - cMinEnemies));
    if(tEnemiesToPlaceForRooms >= PATHS_MAX) tEnemiesToPlaceForRooms = PATHS_MAX - 1;

    //--Reset all room roll priorities.
    for(int i = 0; i < mRoomsTotal; i ++) mRooms[i]->mEnemyRoll = 100;

    //--[Placement]
    //--Linked list storing enemy positions.
    void *tDummyPtr = &tEnemyCounter;
    SugarLinkedList *tPathStorage = new SugarLinkedList(true);
    SugarLinkedList *tEnemyDataList = new SugarLinkedList(true);

    //--Place enemies that will patrol between rooms.
    int i = 0;
    while(tEnemyCounter < tEnemiesToPlaceForRooms && i < 1000)
    {
        //--Increment so we don't re-roll the same numbers over and over.
        i ++;

        //--Variables.
        int tRoomS = 0;
        int tRoomE = 0;

        //--Sum up the roll priorities between all rooms.
        int tRollCap = 0;
        for(int p = 0; p < mRoomsTotal; p ++)
        {
            tRollCap += mRooms[p]->mEnemyRoll;
        }

        //--If the roll cap is zero, the room is randomly rolled.
        if(tRollCap < 1)
        {
            //--Roll the starting room.
            tRoomS = (NormalizeRoll(rNoiseModule, X_QUERY + i, Y_QUERY - i, Z_QUERY_ENEMY) * (mRoomsTotal));
            if(tRoomS >= mRoomsTotal) tRoomS = mRoomsTotal - 1;
            if(tRoomS < 0) tRoomS = 0;

            //--Roll the ending room.
            tRoomE = (NormalizeRoll(rNoiseModule, X_QUERY - i, Y_QUERY + i, Z_QUERY_ENEMY) * (mRoomsTotal));
            if(tRoomE >= mRoomsTotal) tRoomE = mRoomsTotal - 1;
            if(tRoomE < 0) tRoomE = 0;
        }
        //--Otherwise, roll two values between 0 and the cap.
        else
        {
            //--Roll em!
            int tRollS = NormalizeRoll(rNoiseModule, X_QUERY + i, Y_QUERY - i, Z_QUERY_ENEMY) * tRollCap;
            int tRollE = NormalizeRoll(rNoiseModule, X_QUERY - i, Y_QUERY + i, Z_QUERY_ENEMY) * tRollCap;

            //--Determine the slots for these rooms.
            for(int p = 0; p < mRoomsTotal; p ++)
            {
                if(tRollS > 0)
                {
                    tRollS -= mRooms[p]->mEnemyRoll;
                    if(tRollS <= 0)
                    {
                        tRoomS = p;
                    }
                }
                if(tRollE > 0)
                {
                    tRollE -= mRooms[p]->mEnemyRoll;
                    if(tRollE <= 0)
                    {
                        tRoomE = p;
                    }
                }
            }

            //--Range check.
            if(tRoomS >= mRoomsTotal) tRoomS = mRoomsTotal - 1;
            if(tRoomS < 0) tRoomS = 0;
            if(tRoomE >= mRoomsTotal) tRoomE = mRoomsTotal - 1;
            if(tRoomE < 0) tRoomE = 0;
        }

        //--If the rooms are different, then this is a patrol between the two rooms:
        if(tRoomE != tRoomS)
        {
            //--Roll the destination location.
            int tDestinationX = mRooms[tRoomE]->mCenterXA;
            int tDestinationY = mRooms[tRoomE]->mCenterYA;
            RollLocationNear(tEnemyCounter, tDestinationX, tDestinationY);
            if(tDestinationX == -1) continue;

            //--Place the enemy.
            int tEnemyX = mRooms[tRoomS]->mCenterXA;
            int tEnemyY = mRooms[tRoomS]->mCenterYA;
            PlaceEnemyNear(tEnemyCounter, tEnemyX, tEnemyY);
            if(tEnemyX == -1) continue;

            //--Build a path between the rooms.
            ALGEnemyPath *nPathData = PlacePathsBetween(tEnemyCounter, tEnemyX, tEnemyY, tDestinationX, tDestinationY);
            if(!nPathData)
            {
                tPathStorage->AddElement("X", tDummyPtr, &DontDeleteThis);
            }
            else
            {
                tPathStorage->AddElement("X", nPathData, &ALGEnemyPath::DeleteThis);
            }

            //--Store the enemy on the linked list.
            SetMemoryData(__FILE__, __LINE__);
            int *nEnemyData = (int *)starmemoryalloc(sizeof(int) * 2);
            nEnemyData[0] = tEnemyX;
            nEnemyData[1] = tEnemyY;
            tEnemyDataList->AddElement("X", nEnemyData, &FreeThis);

            //--Increment the enemy counter.
            tEnemyCounter ++;

            //--Decrement the weights for the selected rooms.
            mRooms[tRoomS]->mEnemyRoll = mRooms[tRoomS]->mEnemyRoll * 0.5f;
            mRooms[tRoomE]->mEnemyRoll = mRooms[tRoomE]->mEnemyRoll * 0.5f;
            if(mRooms[tRoomS]->mEnemyRoll < 0) mRooms[tRoomS]->mEnemyRoll = 0;
            if(mRooms[tRoomE]->mEnemyRoll < 0) mRooms[tRoomE]->mEnemyRoll = 0;
        }
        //--If the rooms are the same, try again.
        else
        {
        }
    }

    //--[Clean Up]
    //--Store to stop repeats.
    mGenerationState = GENERATE_PLACEENEMIES;

    //--[Spawn Storage]
    //--Store this information in the static structure so the scripts can access it.
    xScriptData.mEnemySpawnsTotal = tEnemyDataList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    xScriptData.mEnemySpawnsX = (int *)starmemoryalloc(sizeof(int) * xScriptData.mEnemySpawnsTotal);
    SetMemoryData(__FILE__, __LINE__);
    xScriptData.mEnemySpawnsY = (int *)starmemoryalloc(sizeof(int) * xScriptData.mEnemySpawnsTotal);

    //--Path data.
    xScriptData.mEnemyPathsTotal = tEnemyDataList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    xScriptData.mEnemyPaths = (ALGEnemyPath *)starmemoryalloc(sizeof(ALGEnemyPath) * xScriptData.mEnemyPathsTotal);

    //--Iterate.
    i = 0;
    int *rPtr = (int *)tEnemyDataList->PushIterator();
    while(rPtr)
    {
        //--Store.
        xScriptData.mEnemySpawnsX[i] = rPtr[0];
        xScriptData.mEnemySpawnsY[i] = rPtr[1];

        //--Next.
        i ++;
        rPtr = (int *)tEnemyDataList->AutoIterate();
    }

    //--Store path information for each spawn. The script need this for path nodes.
    i = 0;
    ALGEnemyPath *rPathData = (ALGEnemyPath *)tPathStorage->PushIterator();
    while(rPathData)
    {
        //--Skip if there's a dummy pointer. An error occurred.
        if(rPathData == tDummyPtr)
        {
            //--Zero data.
            xScriptData.mEnemyPaths[i].mEntriesTotal = 0;
            xScriptData.mEnemyPaths[i].mTilesX = NULL;
            xScriptData.mEnemyPaths[i].mTilesY = NULL;

            //--Next.
            i ++;
            rPathData = (ALGEnemyPath *)tPathStorage->AutoIterate();
            continue;
        }

        //--Take the heap-allocated values out. No need to reallocate and copy.
        xScriptData.mEnemyPaths[i].mEntriesTotal = rPathData->mEntriesTotal;
        xScriptData.mEnemyPaths[i].mTilesX = rPathData->mTilesX;
        xScriptData.mEnemyPaths[i].mTilesY = rPathData->mTilesY;

        //--Remove the original references so they don't get deallocated.
        rPathData->mEntriesTotal = 0;
        rPathData->mTilesX = NULL;
        rPathData->mTilesY = NULL;

        //--Next.
        i ++;
        rPathData = (ALGEnemyPath *)tPathStorage->AutoIterate();
    }

    //--Clean.
    delete tPathStorage;
    delete tEnemyDataList;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void AdventureLevelGenerator::Update()
{
    //--Handles level tick updates when this object is being used in debug. Allows the user to manually
    //  specify how far along generation to go so we can better see errors.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(rControlManager->IsFirstPress("Escape"))
    {
        MapManager::Fetch()->mBackToTitle = true;
        return;
    }

    //--Static list of zoom values.
    static bool xHasBuiltZoomList = false;
    static int xZoomIndex = 2;
    static float xZoomList[5];
    if(!xHasBuiltZoomList)
    {
        //--Flags.
        xZoomIndex = 2;
        xHasBuiltZoomList = true;

        //--List.
        xZoomList[0] = 0.50f;
        xZoomList[1] = 0.75f;
        xZoomList[2] = 1.00f;
        xZoomList[3] = 1.50f;
        xZoomList[4] = 2.00f;
    }

    //--Mouse Wheel.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);
    if(mPreviousMouseZ != tMouseZ)
    {
        //--Storage.
        float tNewRenderScale = mRenderScale;

        //--First pass.
        if(mPreviousMouseZ == -700)
        {
        }
        //--Less.
        else if(mPreviousMouseZ > tMouseZ)
        {
            xZoomIndex --;
            if(xZoomIndex < 1) xZoomIndex = 0;
            tNewRenderScale = xZoomList[xZoomIndex];
        }
        //--More.
        else
        {
            xZoomIndex ++;
            if(xZoomIndex > 4) xZoomIndex = 4;
            tNewRenderScale = xZoomList[xZoomIndex];
        }

        //--Store.
        mPreviousMouseZ = tMouseZ;

        //--If the new and old render scale are different, recompute the center position.
        if(tNewRenderScale != mRenderScale)
        {
            //--Current center point.
            float cDiffX = (CANX * (1.0f / mRenderScale)) - (CANX * (1.0f / tNewRenderScale));
            float cDiffY = (CANY * (1.0f / mRenderScale)) - (CANY * (1.0f / tNewRenderScale));

            //--Compute the mouse position relative to the screen.
            float cMouseXRel = tMouseX / CANX;
            float cMouseYRel = tMouseY / CANY;
            if(tNewRenderScale < mRenderScale)
            {
                cMouseXRel = 0.50f;
                cMouseYRel = 0.50f;
            }

            //--Modify by the difference in scale so the center point emphasizes the mouse position.
            mXRenderOffset = mXRenderOffset - (cDiffX * cMouseXRel);
            mYRenderOffset = mYRenderOffset - (cDiffY * cMouseYRel);

            //--Set the new render scale.
            mRenderScale = tNewRenderScale;
        }
    }

    //--Seed entry. Only works if currently at step 0. Pushing enter toggles the mode.
    if(mGenerationState == GENERATE_NONE)
    {
        //--Player pushes enter.
        if(rControlManager->IsFirstPress("Enter"))
        {
            //--Not entering a seed. Begin!
            if(!mIsEnteringSeed)
            {
                mIsEnteringSeed = true;
                mSeedFlashTimer = 0;
                mSeedEntered[0] = '\0';
            }
            //--Entered a seed. Select.
            else
            {
                mIsEnteringSeed = false;
                mSeedNumber = atoi(mSeedEntered);
            }
        }
    }

    //--If in seed-entering mode, handle numeric keypresses. Stop if we're at 32 characters.
    if(mIsEnteringSeed)
    {
        //--Run this timer.
        mSeedFlashTimer ++;
        if(mSeedFlashTimer >= 30) mSeedFlashTimer = 0;

        //--Handle keypresses.
        int tLen = (int)strlen(mSeedEntered);
        if(tLen < 32)
        {
            if(rControlManager->IsFirstPress("Key_1")) strcat(mSeedEntered, "1");
            if(rControlManager->IsFirstPress("Key_2")) strcat(mSeedEntered, "2");
            if(rControlManager->IsFirstPress("Key_3")) strcat(mSeedEntered, "3");
            if(rControlManager->IsFirstPress("Key_4")) strcat(mSeedEntered, "4");
            if(rControlManager->IsFirstPress("Key_5")) strcat(mSeedEntered, "5");
            if(rControlManager->IsFirstPress("Key_6")) strcat(mSeedEntered, "6");
            if(rControlManager->IsFirstPress("Key_7")) strcat(mSeedEntered, "7");
            if(rControlManager->IsFirstPress("Key_8")) strcat(mSeedEntered, "8");
            if(rControlManager->IsFirstPress("Key_9")) strcat(mSeedEntered, "9");
            if(rControlManager->IsFirstPress("Key_0")) strcat(mSeedEntered, "0");
        }

        //--Backspace deletes a character.
        if(rControlManager->IsFirstPress("Backspace") && tLen > 0)
        {
            mSeedEntered[tLen-1] = '\0';
        }
    }
    //--Otherwise, check numeric keypresses to change if we need to run the generator.
    else
    {
        //--Check the numeric keypresses.
        int tHighestKeypress = 0;
        if(rControlManager->IsFirstPress("Key_1")) tHighestKeypress = 1;
        if(rControlManager->IsFirstPress("Key_2")) tHighestKeypress = 2;
        if(rControlManager->IsFirstPress("Key_3")) tHighestKeypress = 3;
        if(rControlManager->IsFirstPress("Key_4")) tHighestKeypress = 4;
        if(rControlManager->IsFirstPress("Key_5")) tHighestKeypress = 5;
        if(rControlManager->IsFirstPress("Key_6")) tHighestKeypress = 6;
        if(rControlManager->IsFirstPress("Key_7")) tHighestKeypress = 7;
        if(rControlManager->IsFirstPress("Key_8")) tHighestKeypress = 8;
        if(rControlManager->IsFirstPress("Key_9")) tHighestKeypress = 9;

        //--If the keypress is 8, rebuild texturing info.
        if(tHighestKeypress == 8)
        {
            if(mGenerationState == GENERATE_BUILDTEXTURES)
            {
                //--Get the path to the grids file.
                SysVar *rGeneratorPath = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Paths/System/Startup/sGridsPath");
                if(!rGeneratorPath) return;

                //--Re-run the grids file.
                for(int i = 0; i < WALLGRIDS_TOTAL; i ++) mWallGridData[i].Initialize();
                LuaManager::Fetch()->ExecuteLuaFile(rGeneratorPath->mAlpha);

                //--Reset generation state and build textures again.
                mGenerationState = GENERATE_PLACEENEMIES;
                BuildTextures();
            }
        }

        //--If the keypress is 9, reset the entire class.
        if(tHighestKeypress == 9)
        {
            Reset();
            return;
        }

        //--If the highest keypress is higher than the current generation state, run the generators.
        if(tHighestKeypress > mGenerationState)
        {
            //--Run the generators.
            for(int i = mGenerationState+1; i <= tHighestKeypress; i ++)
            {
                //--Scanner.
                if(i == GENERATE_SIZES) GenerateSizes();
                if(i == GENERATE_BASELAYOUT) GenerateBaseLayout();
                if(i == GENERATE_TUNNELER) GenerateTunnels();
                if(i == GENERATE_PLACEEXITS) PlaceExits();
                if(i == GENERATE_PLACETREASURE) PlaceTreasure();
                if(i == GENERATE_PLACEENEMIES) PlaceEnemies();
                if(i == GENERATE_BUILDTEXTURES) BuildTextures();
            }
        }
    }

    //--Arrow keys change map offset.
    const float cMoveSpeed = 12.0f;
    if(rControlManager->IsDown("Left"))
    {
        mXRenderOffset = mXRenderOffset + cMoveSpeed;
    }
    if(rControlManager->IsDown("Right"))
    {
        mXRenderOffset = mXRenderOffset - cMoveSpeed;
    }
    if(rControlManager->IsDown("Up"))
    {
        mYRenderOffset = mYRenderOffset + cMoveSpeed;
    }
    if(rControlManager->IsDown("Down"))
    {
        mYRenderOffset = mYRenderOffset - cMoveSpeed;
    }
    if(rControlManager->IsFirstPress("UpLevel"))
    {
        xPathVis --;
        if(xPathVis < -1) xPathVis = -1;
    }
    if(rControlManager->IsFirstPress("DnLevel"))
    {
        xPathVis ++;
        if(xPathVis >= PATHS_MAX) xPathVis = PATHS_MAX - 1;
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void AdventureLevelGenerator::AddToRenderList(SugarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void AdventureLevelGenerator::Render()
{
    //--[Setup]
    //--Flip this flag.
    MapManager::xHasRenderedMenus = true;

    //--Scale.
    float cScale = mRenderScale;

    //--[Layout]
    //--Renders the world using debug values, which are simple squares.
    if(mGenerationState < GENERATE_BUILDTEXTURES)
    {
        //--GL Setup.
        glDisable(GL_TEXTURE_2D);

        //--Position.
        glScalef(cScale, cScale, 1.0f);
        glTranslatef(mXRenderOffset, mYRenderOffset, 0.0f);

        //--Iterate.
        StarlightColor::SetMixer(0.0f, 0.0f, 1.0f, 1.0f);
        glBegin(GL_TRIANGLES);
        for(int x = 0; x < mXSize; x ++)
        {
            for(int y = 0; y < mYSize; y ++)
            {
                mMapData[x][y]->RenderDebug(x * 16.0f, y * 16.0f);
            }
        }
        glEnd();

        //--Render a red box to indicate the level's dimensions.
        StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
        glBegin(GL_LINE_LOOP);
            glVertex2f(          0.0f,           0.0f);
            glVertex2f(mXSize * 16.0f,           0.0f);
            glVertex2f(mXSize * 16.0f, mYSize * 16.0f);
            glVertex2f(          0.0f, mYSize * 16.0f);
        glEnd();

        //--Render green boxes to indicate room center points.
        StarlightColor::SetMixer(0.0f, 1.0f, 0.0f, 1.0f);
        for(int i = 0; i < mRoomsTotal; i ++)
        {
            //--Ellipse.
            if(mRooms[i]->mCenterXA != mRooms[i]->mCenterXB || mRooms[i]->mCenterYA != mRooms[i]->mCenterYB)
            {
                glBegin(GL_LINE_LOOP);
                    glVertex2f((mRooms[i]->mCenterXA * 16.0f) + 8.0f, (mRooms[i]->mCenterYA * 16.0f) +  8.0f);
                    glVertex2f((mRooms[i]->mCenterXB * 16.0f) + 8.0f, (mRooms[i]->mCenterYB * 16.0f) +  8.0f);
                    glVertex2f((mRooms[i]->mCenterXB * 16.0f) + 8.0f, (mRooms[i]->mCenterYB * 16.0f) + 16.0f);
                    glVertex2f((mRooms[i]->mCenterXA * 16.0f) + 8.0f, (mRooms[i]->mCenterYA * 16.0f) + 16.0f);
                glEnd();
            }
            //--Circle.
            else
            {
                glBegin(GL_LINE_LOOP);
                    glVertex2f((mRooms[i]->mCenterXA * 16.0f) +  6.0f, (mRooms[i]->mCenterYA * 16.0f) +  6.0f);
                    glVertex2f((mRooms[i]->mCenterXA * 16.0f) + 10.0f, (mRooms[i]->mCenterYA * 16.0f) +  6.0f);
                    glVertex2f((mRooms[i]->mCenterXA * 16.0f) + 10.0f, (mRooms[i]->mCenterYA * 16.0f) + 10.0f);
                    glVertex2f((mRooms[i]->mCenterXA * 16.0f) +  6.0f, (mRooms[i]->mCenterYA * 16.0f) + 10.0f);
                glEnd();
            }
        }

        //--Clean.
        glTranslatef(mXRenderOffset * -1.0f, mYRenderOffset * -1.0f, 0.0f);
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        StarlightColor::ClearMixer();
        glEnable(GL_TEXTURE_2D);
    }
    //--Textured rendering.
    else
    {
        //--Position.
        glScalef(cScale, cScale, 1.0f);
        glTranslatef(mXRenderOffset, mYRenderOffset, 0.0f);

        //--Render a red box to indicate the level's dimensions.
        glDisable(GL_TEXTURE_2D);
        StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
        glBegin(GL_LINE_LOOP);
            glVertex2f(          0.0f,           0.0f);
            glVertex2f(mXSize * 16.0f,           0.0f);
            glVertex2f(mXSize * 16.0f, mYSize * 16.0f);
            glVertex2f(          0.0f, mYSize * 16.0f);
        glEnd();
        glEnable(GL_TEXTURE_2D);
        StarlightColor::ClearMixer();

        //--Requires texture to be available.
        if(rMapTexture)
        {
            rMapTexture->Bind();
            glBegin(GL_QUADS);
            for(int x = 0; x < mXSize; x ++)
            {
                for(int y = 0; y < mYSize; y ++)
                {
                    mMapData[x][y]->RenderTextured(x * 16.0f, y * 16.0f);
                }
            }
            glEnd();
        }

        //--Clean.
        glTranslatef(mXRenderOffset * -1.0f, mYRenderOffset * -1.0f, 0.0f);
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
    }

    //--[Text]
    //--Shows status reports in the top left.
    SugarFont *rDisplayFont = DataLibrary::Fetch()->GetFont("Adlev Generator Main");
    SugarFont *rSmallerFont = DataLibrary::Fetch()->GetFont("Adlev Generator Small");
    if(!rDisplayFont || !rSmallerFont) return;

    //--Sizes.
    float cTxtHei = rDisplayFont->GetTextHeight();
    float cSmlHei = rSmallerFont->GetTextHeight();

    //--Render instructions if the user has not done any generation.
    if(mGenerationState == GENERATE_NONE)
    {
        rDisplayFont->DrawText(0.0f,                     0.0f, 0, 1.0f, "Instructions:");
        rSmallerFont->DrawText(0.0f, cTxtHei + cSmlHei * 0.0f, 0, 1.0f, "Push the numeric keys from 1-7 to run the generator to various points and see how it works.");
        rSmallerFont->DrawText(0.0f, cTxtHei + cSmlHei * 1.0f, 0, 1.0f, "Push the 9 key to reset the generator to a new seed.");
        rSmallerFont->DrawText(0.0f, cTxtHei + cSmlHei * 2.0f, 0, 1.0f, "Push Escape to return to the title.");
        rSmallerFont->DrawText(0.0f, cTxtHei + cSmlHei * 3.0f, 0, 1.0f, "Push Enter and type a number to manually specify the seed. Push Enter when finished.");

        //--Render the current seed.
        if(!mIsEnteringSeed)
        {
            rDisplayFont->DrawTextArgs(0.0f, VIRTUAL_CANVAS_Y - cTxtHei - 3.0f, 0, 1.0f, "Seed: %i", mSeedNumber);
        }
        //--Render it as a string.
        else
        {
            rDisplayFont->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cTxtHei * 2.0f) - 3.0f, 0, 1.0f, "Enter your seed number:");
            if(mSeedFlashTimer % 30 < 15)
                rDisplayFont->DrawTextArgs(0.0f, VIRTUAL_CANVAS_Y - cTxtHei - 3.0f, 0, 1.0f, "Seed: %s", mSeedEntered);
            else
                rDisplayFont->DrawTextArgs(0.0f, VIRTUAL_CANVAS_Y - cTxtHei - 3.0f, 0, 1.0f, "Seed: %s_", mSeedEntered);
        }
    }
    //--Usability data.
    else
    {
        //--Render the system data.
        rDisplayFont->DrawText    (0.0f, cTxtHei * 0.0f, 0, 1.0f, "System Data");
        rDisplayFont->DrawTextArgs(0.0f, cTxtHei * 1.0f, 0, 1.0f, " Generation State: %i", mGenerationState);
        rDisplayFont->DrawTextArgs(0.0f, cTxtHei * 2.0f, 0, 1.0f, " Sizes: %i x %i", mXSize, mYSize);
        rDisplayFont->DrawTextArgs(0.0f, cTxtHei * 3.0f, 0, 1.0f, " Paths: %i", xPathVis);

        //--Rendering data.
        rDisplayFont->DrawText    (800.0f, cTxtHei * 0.0f, 0, 1.0f, "Rendering Data");
        rDisplayFont->DrawTextArgs(800.0f, cTxtHei * 1.0f, 0, 1.0f, " Offsets: %i x %i", (int)mXRenderOffset, (int)mYRenderOffset);
        rDisplayFont->DrawTextArgs(800.0f, cTxtHei * 2.0f, 0, 1.0f, " Zoom: %5.2f", mRenderScale);

        //--Render the current seed.
        rDisplayFont->DrawTextArgs(0.0f, VIRTUAL_CANVAS_Y - cTxtHei - 3.0f, 0, 1.0f, "Seed: %i", mSeedNumber);

        //--Movement instructions.
        float cYPos = (VIRTUAL_CANVAS_Y - cTxtHei - 3.0f) - (cSmlHei * 4.0f);
        rSmallerFont->DrawText(0.0f, cYPos + (cSmlHei * 0.0f), 0, 1.0f, "Arrow keys: Scroll");
        rSmallerFont->DrawText(0.0f, cYPos + (cSmlHei * 1.0f), 0, 1.0f, "Mousewheel: Zoom");
        rSmallerFont->DrawText(0.0f, cYPos + (cSmlHei * 2.0f), 0, 1.0f, "PgUp/PgDn: Change monster path visibility");
        rSmallerFont->DrawText(0.0f, cYPos + (cSmlHei * 3.0f), 0, 1.0f, "Push 9 to reset generator");

        //--Room data.
        rDisplayFont->DrawTextArgs(800.0f, VIRTUAL_CANVAS_Y - (cTxtHei * 2.0f) - 3.0f, 0, 1.0f, "Room Data");
        rDisplayFont->DrawTextArgs(800.0f, VIRTUAL_CANVAS_Y - (cTxtHei * 1.0f) - 3.0f, 0, 1.0f, " Total Rooms: %i", mRoomsTotal);
    }
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void AdventureLevelGenerator::HookToLuaState(lua_State *pLuaState)
{
    /* AdlevGenerator_Create()
       Creates a level generator and provides it to the MapManager as the new active level. This
       allows the player to watch the generator do its thing with keyboard inputs. */
    lua_register(pLuaState, "AdlevGenerator_Create", &Hook_AdlevGenerator_Create);

    /* AdlevGenerator_GetProperty("Entrance Position") (2 integers) (Static)
       AdlevGenerator_GetProperty("Exit Position") (2 integers) (Static)
       AdlevGenerator_GetProperty("Enemy Spawns Total") (1 integer) (Static)
       AdlevGenerator_GetProperty("Enemy Spawn Position", iSlot) (2 integers) (Static)
       AdlevGenerator_GetProperty("Enemy Paths Total") (1 integer) (Static)
       AdlevGenerator_GetProperty("Enemy Path Length", iSlot) (1 integer) (Static)
       AdlevGenerator_GetProperty("Enemy Path Position", iSlot, iEntry) (2 integers) (Static)
       AdlevGenerator_GetProperty("Treasures Total") (1 integer) (Static)
       AdlevGenerator_GetProperty("Treasure Position", iSlot) (2 integers) (Static)
       AdlevGenerator_GetProperty("Last Seed") (1 integer) (Static)
       Returns the requested data. Most of these are static and used by the constructor script after
       the level has been generated. */
    lua_register(pLuaState, "AdlevGenerator_GetProperty", &Hook_AdlevGenerator_GetProperty);

    /* AdlevGenerator_SetProperty("Texture Path", sPath)
       AdlevGenerator_SetProperty("Pattern TL", iX, iY)
       AdlevGenerator_SetProperty("Floors Total", iTotal)
       AdlevGenerator_SetProperty("Floor Position", iSlot, iX, iY)
       AdlevGenerator_SetProperty("Wall Grid", b00, b10, b20, b01, b11, b21, b02, b12, b22, iWallCode, iWallHiCode, iBlackoutCode)
       AdlevGenerator_SetProperty("Blackout Pos", fX, fY)
       AdlevGenerator_SetProperty("Ladder Pos", fDnX, fDnY, fUpX, fUpY)
       AdlevGenerator_SetProperty("Cliff Pos", fX, fY)
       AdlevGenerator_SetProperty("Floor Edge Pos", fX, fY)
       AdlevGenerator_SetProperty("Treasure Pos", fX, fY)
       AdlevGenerator_SetProperty("Enemy Pos", fX, fY)
       Sets the requested property in the active object. */
    lua_register(pLuaState, "AdlevGenerator_SetProperty", &Hook_AdlevGenerator_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdlevGenerator_Create(lua_State *L)
{
    //AdlevGenerator_Create()
    AdventureLevelGenerator *nGenerator = new AdventureLevelGenerator();
    MapManager::Fetch()->ReceiveLevel(nGenerator);
    return 0;
}
int Hook_AdlevGenerator_GetProperty(lua_State *L)
{
    //AdlevGenerator_GetProperty("Entrance Position") (2 integers) (Static)
    //AdlevGenerator_GetProperty("Exit Position") (2 integers) (Static)
    //AdlevGenerator_GetProperty("Enemy Spawns Total") (1 integer) (Static)
    //AdlevGenerator_GetProperty("Enemy Spawn Position", iSlot) (2 integers) (Static)
    //AdlevGenerator_GetProperty("Enemy Paths Total") (1 integer) (Static)
    //AdlevGenerator_GetProperty("Enemy Path Length", iSlot) (1 integer) (Static)
    //AdlevGenerator_GetProperty("Enemy Path Position", iSlot, iEntry) (2 integers) (Static)
    //AdlevGenerator_GetProperty("Treasures Total") (1 integer) (Static)
    //AdlevGenerator_GetProperty("Treasure Position", iSlot) (2 integer) (Static)
    //AdlevGenerator_GetProperty("Last Seed") (1 integer) (Static)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdlevGenerator_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Static Values]
    //--X/Y of the entrance position, in tile coordinates.
    if(!strcasecmp("Entrance Position", rSwitchType))
    {
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mEntranceX);
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mEntranceY);
        return 2;
    }
    //--X/Y of the exit position, in tile coordinates.
    else if(!strcasecmp("Exit Position", rSwitchType))
    {
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mExitX);
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mExitY);
        return 2;
    }
    //--How many enemy spawn positions there are total.
    else if(!strcasecmp("Enemy Spawns Total", rSwitchType))
    {
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mEnemySpawnsTotal);
        return 1;
    }
    //--The X/Y of the requested spawn position.
    else if(!strcasecmp("Enemy Spawn Position", rSwitchType) && tArgs == 2)
    {
        //--Range check.
        int tSlot = lua_tointeger(L, 2);
        if(tSlot < 0 || tSlot >= AdventureLevelGenerator::xScriptData.mEnemyPathsTotal){ lua_pushinteger(L, 0); lua_pushinteger(L, 0); return 2; }

        //--Data.
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mEnemySpawnsX[tSlot]);
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mEnemySpawnsY[tSlot]);
        return 2;
    }
    //--How many enemy paths there are. Should be paralell with the enemy spawns.
    else if(!strcasecmp("Enemy Paths Total", rSwitchType))
    {
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mEnemyPathsTotal);
        return 1;
    }
    //--How many tiles the requested path goes.
    else if(!strcasecmp("Enemy Path Length", rSwitchType) && tArgs == 2)
    {
        //--Range check.
        int tSlot = lua_tointeger(L, 2);
        if(tSlot < 0 || tSlot >= AdventureLevelGenerator::xScriptData.mEnemyPathsTotal){ lua_pushinteger(L, 0); return 1; }

        //--Data.
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mEnemyPaths[tSlot].mEntriesTotal);
        return 1;
    }
    //--The X/Y coordinate of a given part of a given path.
    else if(!strcasecmp("Enemy Path Position", rSwitchType) && tArgs == 3)
    {
        //--Range check.
        int tSlot = lua_tointeger(L, 2);
        if(tSlot < 0 || tSlot >= AdventureLevelGenerator::xScriptData.mEnemyPathsTotal){ lua_pushinteger(L, 0); lua_pushinteger(L, 0); return 2; }

        //--Range check again.
        int tSpot = lua_tointeger(L, 3);
        if(tSpot < 0 || tSpot >= AdventureLevelGenerator::xScriptData.mEnemyPaths[tSlot].mEntriesTotal){ lua_pushinteger(L, 0); lua_pushinteger(L, 0); return 2; }

        //--Data.
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mEnemyPaths[tSlot].mTilesX[tSpot]);
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mEnemyPaths[tSlot].mTilesY[tSpot]);
        return 2;
    }
    //--How many treasures there are.
    else if(!strcasecmp("Treasures Total", rSwitchType))
    {
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mBonusObjectsTotal);
        return 1;
    }
    //--How many tiles the requested path goes.
    else if(!strcasecmp("Treasure Position", rSwitchType) && tArgs == 2)
    {
        //--Range check.
        int tSlot = lua_tointeger(L, 2);
        if(tSlot < 0 || tSlot >= AdventureLevelGenerator::xScriptData.mBonusObjectsTotal){ lua_pushinteger(L, 0); lua_pushinteger(L, 0); return 2; }

        //--Data.
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mBonusObjectsX[tSlot]);
        lua_pushinteger(L, AdventureLevelGenerator::xScriptData.mBonusObjectsY[tSlot]);
        return 2;
    }
    //--How many tiles the requested path goes.
    else if(!strcasecmp("Last Seed", rSwitchType) && tArgs == 1)
    {
        lua_pushinteger(L, AdventureLevelGenerator::xLastSeed);
        return 1;
    }

    //--[Dynamic Values]
    //--Active object.
    AdventureLevelGenerator *rGenerator = (AdventureLevelGenerator *)MapManager::Fetch()->GetActiveLevel();
    if(!rGenerator || !rGenerator->IsOfType(POINTER_TYPE_ADVENTURELEVELGENERATOR)) return LuaTypeError("AdlevGenerator_SetProperty");

    //--Finish up.
    return tReturns;
}
int Hook_AdlevGenerator_SetProperty(lua_State *L)
{
    //AdlevGenerator_SetProperty("Texture Path", sPath)
    //AdlevGenerator_SetProperty("Pattern TL", iX, iY)
    //AdlevGenerator_SetProperty("Floors Total", iTotal)
    //AdlevGenerator_SetProperty("Floor Position", iSlot, iX, iY)
    //AdlevGenerator_SetProperty("Wall Grid", b00, b10, b20, b01, b11, b21, b02, b12, b22, iWallCode, iWallHiCode, iBlackoutCode)
    //AdlevGenerator_SetProperty("Blackout Pos", fX, fY)
    //AdlevGenerator_SetProperty("Ladder Pos", fDnX, fDnY, fUpX, fUpY)
    //AdlevGenerator_SetProperty("Cliff Pos", fX, fY)
    //AdlevGenerator_SetProperty("Floor Edge Pos", fX, fY)
    //AdlevGenerator_SetProperty("Treasure Pos", fX, fY)
    //AdlevGenerator_SetProperty("Enemy Pos", fX, fY)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdlevGenerator_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Static Values]
    //--[Dynamic Values]
    //--Active object.
    AdventureLevelGenerator *rGenerator = (AdventureLevelGenerator *)MapManager::Fetch()->GetActiveLevel();
    if(!rGenerator || !rGenerator->IsOfType(POINTER_TYPE_ADVENTURELEVELGENERATOR)) return LuaTypeError("AdlevGenerator_SetProperty");

    //--DL Path to the rendering texture.
    if(!strcasecmp(rSwitchType, "Texture Path") && tArgs == 2)
    {
        rGenerator->SetTextureMap(lua_tostring(L, 2));
    }
    //--Position on the texture where the standard wall pattern begins.
    else if(!strcasecmp(rSwitchType, "Pattern TL") && tArgs == 3)
    {
        rGenerator->SetPatternPosition(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Position on the texture where the standard wall pattern begins.
    else if(!strcasecmp(rSwitchType, "Floors Total") && tArgs == 2)
    {
        rGenerator->AllocateFloors(lua_tointeger(L, 2));
    }
    //--Position on the texture where the standard wall pattern begins.
    else if(!strcasecmp(rSwitchType, "Floor Position") && tArgs == 4)
    {
        rGenerator->SetFloorUV(lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    //--Position on the texture where the standard wall pattern begins.
    else if(!strcasecmp(rSwitchType, "Wall Grid") && tArgs == 13)
    {
        bool tGrid[3][3];
        tGrid[0][0] = (lua_tonumber(L,  2) == 1.0f);
        tGrid[1][0] = (lua_tonumber(L,  3) == 1.0f);
        tGrid[2][0] = (lua_tonumber(L,  4) == 1.0f);
        tGrid[0][1] = (lua_tonumber(L,  5) == 1.0f);
        tGrid[1][1] = true;
        tGrid[2][1] = (lua_tonumber(L,  7) == 1.0f);
        tGrid[0][2] = (lua_tonumber(L,  8) == 1.0f);
        tGrid[1][2] = (lua_tonumber(L,  9) == 1.0f);
        tGrid[2][2] = (lua_tonumber(L, 10) == 1.0f);
        rGenerator->SetWallGridProperties(tGrid, lua_tointeger(L, 11), lua_tointeger(L, 12), lua_tointeger(L, 13));
    }
    //--UV position of the blackout tiles.
    else if(!strcasecmp(rSwitchType, "Blackout Pos") && tArgs == 3)
    {
        rGenerator->SetBlackoutPosition(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--UV position of the ladder down and up tiles.
    else if(!strcasecmp(rSwitchType, "Ladder Pos") && tArgs == 5)
    {
        rGenerator->SetLadderPosition(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--UV position of the cliff tiles.
    else if(!strcasecmp(rSwitchType, "Cliff Pos") && tArgs == 3)
    {
        rGenerator->SetCliffPosition(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--UV position of the floor edging tiles.
    else if(!strcasecmp(rSwitchType, "Floor Edge Pos") && tArgs == 3)
    {
        rGenerator->SetFloorEdgingPosition(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--UV position of the treasure indicator.
    else if(!strcasecmp(rSwitchType, "Treasure Pos") && tArgs == 3)
    {
        rGenerator->SetTreasurePosition(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--UV position of the enemy indicator.
    else if(!strcasecmp(rSwitchType, "Enemy Pos") && tArgs == 3)
    {
        rGenerator->SetEnemyUV(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Error.
    else
    {
        LuaPropertyError("AL_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
