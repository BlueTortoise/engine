//--Base
#include "AdventureLevelGenerator.h"

//--Classes
#include "ALGTile.h"
#include "AdventureLevel.h"
#include "TileLayer.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

//--[Local Definitions]

//--[Function]
AdventureLevel *AdventureLevelGenerator::ExportToAdventureLevel()
{
    //--[Documentation and Setup]
    //--Exports the generated level to an AdventureLevel. The level needs to run a specialized constructor
    //  afterwards to place items and enemies.
    AdventureLevel *nLevel = new AdventureLevel();
    nLevel->mXSize = mXSize;
    nLevel->mYSize = mYSize;

    //--Allocate tile packs. There's only one.
    nLevel->mTilesetsTotal = 1;
    SetMemoryData(__FILE__, __LINE__);
    nLevel->mTilesetPacks = (TilesetInfoPack *)starmemoryalloc(sizeof(TilesetInfoPack) * nLevel->mTilesetsTotal);
    nLevel->mTilesetPacks[0].mName = InitializeString("Standard");
    nLevel->mTilesetPacks[0].mTilesToThisPoint = 0;
    nLevel->mTilesetPacks[0].mTilesTotal = 144;
    nLevel->mTilesetPacks[0].mTileset = rMapTexture;

    //--[Tile Layers]
    //--Setup
    TileLayer *tTileLayers[LAYERS_TOTAL];
    int16_t *tTileLayerData[LAYERS_TOTAL];

    //--Collision Data.
    SetMemoryData(__FILE__, __LINE__);
    uint8_t **tCollisionData = (uint8_t **)starmemoryalloc(sizeof(uint8_t *) * mXSize);
    SetMemoryData(__FILE__, __LINE__);
    uint16_t **tCollisionFlipData = (uint16_t **)starmemoryalloc(sizeof(uint16_t *) * mXSize);
    for(int x = 0; x < mXSize; x ++)
    {
        //--Allocate.
        SetMemoryData(__FILE__, __LINE__);
        tCollisionData[x] = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * mYSize);
        SetMemoryData(__FILE__, __LINE__);
        tCollisionFlipData[x] = (uint16_t *)starmemoryalloc(sizeof(uint16_t) * mYSize);

        //--Clear.
        memset(tCollisionData[x], 0, sizeof(uint8_t) * mYSize);
        memset(tCollisionFlipData[x], 0, sizeof(uint16_t) * mYSize);
    }

    //--Set layer data.
    for(int i = 0; i < LAYERS_TOTAL; i ++)
    {
        //--Name.
        char tNameBuf[32];
        sprintf(tNameBuf, "Layer%i", i);

        //--Allocation.
        tTileLayers[i] = new TileLayer();
        SetMemoryData(__FILE__, __LINE__);
        tTileLayerData[i] = (int16_t *)starmemoryalloc(sizeof(int16_t) * mXSize * mYSize);
        memset(tTileLayerData[i], 0, sizeof(int16_t) * mXSize * mYSize);
        tTileLayers[i]->SetMidgroundFlag(false);
        if(i >= LAYER_WALLS_0 && i <= LAYER_WALLSHI_1) tTileLayers[i]->SetMidgroundFlag(true);
        tTileLayers[i]->SetAnimatedFlag(false);
        tTileLayers[i]->SetAnimatedSlowlyFlag(false);
        tTileLayers[i]->SetOscillationFlag(false);
        tTileLayers[i]->mIsUsingPaddedTiles = false;
        tTileLayers[i]->ProvideTileReferences(nLevel->mTilesetsTotal, nLevel->mTilesetPacks);
        nLevel->mTileLayers->AddElement(tNameBuf, tTileLayers[i]);
    }

    //--Set depths.
    tTileLayers[LAYER_FLOOR_0]->SetDepth(-1.000000f);
    tTileLayers[LAYER_FLOOR_1]->SetDepth(-0.999999f);
    tTileLayers[LAYER_FLOOR_2]->SetDepth(-0.999998f);
    tTileLayers[LAYER_WALLS_0]->SetDepth(-0.499930f);
    tTileLayers[LAYER_WALLS_1]->SetDepth(-0.499929f);
    tTileLayers[LAYER_WALLSHI_0]->SetDepth(-0.499810f);
    tTileLayers[LAYER_WALLSHI_1]->SetDepth(-0.499809f);
    tTileLayers[LAYER_BLACKOUT]->SetDepth(-0.000001f);

    //--Collision layer. Handled specially.
    nLevel->mCollisionLayersTotal = 1;
    SetMemoryData(__FILE__, __LINE__);
    nLevel->mCollisionLayers = (CollisionPack *)starmemoryalloc(sizeof(CollisionPack) * nLevel->mCollisionLayersTotal);
    nLevel->mCollisionLayers[0].Initialize();

    //--Now go through the tiles and tell them to dump their data into these arrays.
    for(int x = 0; x < mXSize; x ++)
    {
        for(int y = 0; y < mYSize; y ++)
        {
            mMapData[x][y]->CompileDataTo(x, y, mXSize, mYSize, tTileLayerData, tCollisionData);
        }
    }

    //--Order the layers to parse the data.
    for(int i = 0; i < LAYERS_TOTAL; i ++)
    {
        tTileLayers[i]->ProvideTileData(mXSize, mYSize, tTileLayerData[i], true);
    }

    nLevel->mCollisionLayers[0].mCollisionSizeX = mXSize;
    nLevel->mCollisionLayers[0].mCollisionSizeY = mYSize;
    nLevel->mCollisionLayers[0].mCollisionData = tCollisionData;
    nLevel->mCollisionLayers[0].mCollisionFlipData = tCollisionFlipData;

    //--Compile after parsing:
    nLevel->CompileAfterParse();

    //--Place player start.
    nLevel->mPlayerStartX = (int)(mEntrancePosX * 1.0f);
    nLevel->mPlayerStartY = (int)(mEntrancePosY * 1.0f);

    return nLevel;
}
