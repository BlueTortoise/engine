//--[ALGTile]
//--Represents a single tile in the AdventureLevelGenerator. These tiles can get quite complex, so
//  they get their own class. All members are public, and these are intended only to be used with
//  the AdventureLevelGenerator.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
//--Rendering Flags
#define ALGTILE_HAS_FLOORA 0x0001
#define ALGTILE_HAS_FLOORB 0x0002
#define ALGTILE_HAS_FLOORC 0x0004
#define ALGTILE_HAS_WALLA 0x0008
#define ALGTILE_HAS_WALLB 0x0010
#define ALGTILE_HAS_WALLHIA 0x0020
#define ALGTILE_HAS_WALLHIB 0x0040
#define ALGTILE_HAS_BLACKOUT 0x0080
#define ALGTILE_HAS_TREASURE 0x0100
#define ALGTILE_HAS_ENEMYA 0x0200
#define ALGTILE_HAS_ENEMYB 0x0400
#define ALGTILE_HAS_ENEMYC 0x0800

//--Rendering Slots
#define ALGUV_FLOORA 0
#define ALGUV_FLOORB 1
#define ALGUV_FLOORC 2
#define ALGUV_WALLA 3
#define ALGUV_WALLB 4
#define ALGUV_WALLHIA 5
#define ALGUV_WALLHIB 6
#define ALGUV_BLACKOUT 7
#define ALGUV_TREASURE 8
#define ALGUV_STANDARD 9
#define ALGUV_ENEMYA 9
#define ALGUV_ENEMYB 10
#define ALGUV_ENEMYC 11
#define ALGUV_TOTAL 12

//--Rendering Depths
#define ALGDEPTH_FLOOR -1.000000f
#define ALGDEPTH_WALL -0.900000f
#define ALGDEPTH_WALLHI -0.800000f
#define ALGDEPTH_BLACKOUT -0.700000f

//--Special Floor Flags
#define SPECIAL_FLOOR_NONE 0
#define SPECIAL_FLOOR_PIT 1

//--Pathing
#define PATHS_MAX 26

//--[Classes]
class ALGTile
{
    public:
    //--System
    int mPulseCheck;
    int mPulseDistance;
    bool mWasTunnelTile;

    //--Collision
    int mCollisionType;
    int mBackupCollisionType;

    //--Special Texture Information
    int mSpecialFloorFlag;
    int mBackupSpecialFloorFlag;

    //--Texture UVs
    uint16_t mUVFlags;
    float mTextureWid;
    float mTextureHei;
    TwoDimensionReal mUVSlots[ALGUV_TOTAL];

    //--Entity Stuff
    bool mIsEntrance;
    bool mIsExit;
    bool mIsTreasureCandidate;
    bool mIsTreasureFinalist;
    bool mIsEnemySpawn[PATHS_MAX];
    char mPathNodeName[PATHS_MAX][32];

    //--System
    ALGTile();
    ~ALGTile();

    //--Public Variables
    //--Property Queries
    bool IsFloorTile();
    bool IsPitTile();
    bool IsWallTile();
    bool IsEnemySpawn();
    bool IsPathNode();

    //--Manipulators
    void ClearUVs();
    void UnclipForTunnel();
    void SetTextureRefValues(float pWidth, float pHeight);
    void SetUV(int pSlot, float pLft, float pTop, float pRgt, float pBot);
    void SetUV(int pSlot, TwoDimensionReal pDim);

    //--Core Methods
    void CompileDataTo(int pX, int pY, int pXSize, int pYSize, int16_t **pTileArray, uint8_t **pCollisionArray);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    void RenderDebug(float pX, float pY);
    void RenderTextured(float pX, float pY);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

