//--Base
#include "RootEvent.h"

//--Classes
#include "PairanormalLevel.h"

//--CoreClasses
#include "DataList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

//=========================================== System ==============================================
RootEvent::RootEvent()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_EVENT_ROOT;

    //--[RootEvent]
    //--System
    mCheckpointState = PairanormalLevel::xIsRunningToCheckpoint;
    mIsMalformed = true;
    mLocalName = InitializeString("Event");
    mIsBlocker = false;

    //--Script Execution
    mIsScriptExecutor = false;
    mIsScriptRawInstruction = false;
    mScriptPath = NULL;

    //--Data Storage
    mDataList = new DataList();
    mDataList->SetStrictFlag(true);
}
RootEvent::~RootEvent()
{
    free(mLocalName);
    free(mScriptPath);
    delete mDataList;
}

//--[Public Statics]
//--Boolean used by script execution cases to check if the script has completed. For example, if a
//  script executes until an entity has finished walking somewhere, it would flip this variable to true
//  when that entity has finished, at which point the event is removed from the execution list.
bool RootEvent::xIsScriptComplete = false;

//--Whether or not this event was created when running to a checkpoint. The state gets set right before
//  the script executes, if it is an instruction event.
//--This is only used for event systems in Pairanormal.
bool RootEvent::xCurrentCheckpointState = false;

//====================================== Property Queries =========================================
bool RootEvent::IsOfType(int pType)
{
    return (pType == POINTER_TYPE_EVENT_ROOT);
}
const char *RootEvent::GetName()
{
    return (const char *)mLocalName;
}
bool RootEvent::IsBlocker()
{
    return mIsBlocker;
}
bool RootEvent::IsScriptExecutionCase()
{
    return mIsScriptExecutor;
}
bool RootEvent::IsInstructionExecutionCase()
{
    return mIsScriptRawInstruction;
}

//========================================= Manipulators ==========================================
void RootEvent::SetName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void RootEvent::SetBlockingFlag(bool pFlag)
{
    mIsMalformed = false;
    mIsBlocker = pFlag;
}
void RootEvent::SetScriptExecutionMode(const char *pScript)
{
    //--Flags.
    mIsMalformed = false;
    mIsScriptExecutor = true;
    mIsScriptRawInstruction = false;
    ResetString(mScriptPath, pScript);

    //--Constructor execution is immediate.
    if(mScriptPath)
        LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)SCRIPT_EVENT_CONSTRUCTOR);
}
void RootEvent::SetInstructionExecutionMode(const char *pInstructionData)
{
    mIsMalformed = false;
    mIsScriptExecutor = false;
    mIsScriptRawInstruction = true;
    ResetString(mScriptPath, pInstructionData);
}

//========================================= Core Methods ==========================================
bool RootEvent::IsComplete()
{
    //--Returns true if the event has completed and should be removed from the execution list. Events
    //  can have their own peculiar interpretation of what it means, and this can be used by derived classes.
    //--The base event has a few cases: Blockers return true if they are the 0th element on the list,
    //  script execution cases look for a static flag, and instruction executions always return true.
    //--Blockers use external logic since they don't know their position on the list, so that's not checked here.

    //--Malformed events always complete immediately. This occurs if an event did not have any properties
    //  set by whatever created it, which would normally make them impossible to remove. It is still possible
    //  to make an event that cannot be removed (with a malformed script) but that can't be evaluated in the C++ code.
    if(mIsMalformed) return true;

    //--Script execution mode: After running the update call, this flag should be true to indicate the
    //  script completed. It gets reset before the call.
    if(mIsScriptExecutor && xIsScriptComplete)
    {
        return true;
    }

    //--Script instruction mode: Always returns true, since the instructions are meant to only execute once.
    if(mIsScriptRawInstruction)
    {
        return true;
    }

    //--All checks failed, the event is not complete.
    return false;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void RootEvent::Update()
{
    //--Called immediately before the IsComplete() check, the event has a chance to execute whatever
    //  its internal logic is. Malformed events never execute anything.
    if(mIsMalformed) return;

    //--Script events: Execute the associated lua script.
    if(mIsScriptExecutor)
    {
        //--Set this flag to false before running anything.
        xIsScriptComplete = false;

        //--No script exists: This event should complete immediately.
        if(!mScriptPath)
        {
            xIsScriptComplete = true;
        }
        //--The script exists. Fire it with the EXECUTE firing code.
        else
        {
            LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)SCRIPT_EVENT_EXECUTE);
        }

        //--If the script path was null'd off during execution then this event removes itself. Note that
        //  this is considered the improper way to remove the event.
        if(!mScriptPath) xIsScriptComplete = true;
    }
    //--Instruction execution. Executes the mScriptPath as if it were a set of instructions.
    else if(mIsScriptRawInstruction)
    {
        //--If no script exists then we can't very well call it now can we?
        if(!mScriptPath)
        {
        }
        //--Assemble a temporary script and pass it to the LuaManager. Remember that the call stack cannot be accessed!
        else
        {
            DataLibrary::Fetch()->PushActiveEntity(this);
            xCurrentCheckpointState = mCheckpointState;
            LuaManager::Fetch()->LoadLuaString(mScriptPath);
            LuaManager::Fetch()->ExecuteLoadedFile();
            DataLibrary::Fetch()->PopActiveEntity();
        }
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
DataList *RootEvent::GetDataList()
{
    return mDataList;
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void RootEvent::HookToLuaState(lua_State *pLuaState)
{
    /* REvent_SetProperty("Script Complete") (Static)
       REvent_SetProperty("ScriptExec", sPath)
       REvent_SetProperty("InstructionExec", sInstructions)
       Sets the given property in the active RootEvent or derived type. */
    lua_register(pLuaState, "REvent_SetProperty", &Hook_REvent_SetProperty);

    /* REvent_GetProperty("Dummy") (1 integer)
       Returns the requested property from the active RootEvent or derived type. */
    lua_register(pLuaState, "REvent_GetProperty", &Hook_REvent_GetProperty);

    /* REvent_DefineData(sName, "N", fValue)
       REvent_DefineData(sName, "S", sString)
       Macro, pushes the RootEvent's DataList and then calls the DataList function of the same name.
       Auto-pops the DataList when done with it. */
    lua_register(pLuaState, "REvent_DefineData", &Hook_REvent_DefineData);

    /* REvent_GetData(sName, "N")
       REvent_GetData(sName, "S")
       Macro, pushes the RootEvent's DataList and then calls the DataList function of the same name.
       Auto-pops the DataList when done with it. */
    lua_register(pLuaState, "REvent_GetData", &Hook_REvent_GetData);

    /* REvent_SetData(sName, "N", fValue)
       REvent_SetData(sName, "S", sString[])
       Macro, pushes the RootEvent's DataList and then calls the DataList function of the same name.
       Auto-pops the DataList when done with it. */
    lua_register(pLuaState, "REvent_SetData", &Hook_REvent_SetData);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_REvent_SetProperty(lua_State *L)
{
    //REvent_SetProperty("Script Complete") (Static)
    //REvent_SetProperty("ScriptExec", sPath)
    //REvent_SetProperty("InstructionExec", sInstructions)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("REvent_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Static Cases]
    //--This flag marks a script event as having completed execution.
    if(!strcasecmp(rSwitchType, "Script Complete") && tArgs == 1)
    {
        RootEvent::xIsScriptComplete = true;
        return 0;
    }

    //--[Instantiated Cases]
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    RootEvent *rEvent = (RootEvent *)rDataLibrary->rActiveObject;
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_ROOT)) return LuaTypeError("REvent_SetProperty");

    //--Path of the execution script for this script.
    if(!strcasecmp(rSwitchType, "ScriptExec") && tArgs == 2)
    {
        rEvent->SetScriptExecutionMode(lua_tostring(L, 2));
    }
    //--String instructions to execute.
    else if(!strcasecmp(rSwitchType, "InstructionExec") && tArgs == 2)
    {
        rEvent->SetInstructionExecutionMode(lua_tostring(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("REvent_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_REvent_GetProperty(lua_State *L)
{
    //REvent_GetProperty("Dummy") (1 integer)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("REvent_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Active object.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    RootEvent *rEvent = (RootEvent *)rDataLibrary->rActiveObject;
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_ROOT)) return LuaTypeError("REvent_GetProperty");

    //--Dummy.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("REvent_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_REvent_DefineData(lua_State *L)
{
    //--See DataList's version
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    RootEvent *rEvent = (RootEvent *)rDataLibrary->rActiveObject;
    rDataLibrary->PushActiveEntity();

    //--Check the Entity
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_ROOT)) return LuaTypeError("REvent_DefineData");

    rDataLibrary->rActiveObject = rEvent->GetDataList();
        Hook_DataList_Define(L);
    rDataLibrary->PopActiveEntity();
    return 0;
}
int Hook_REvent_GetData(lua_State *L)
{
    //--See DataList's version
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    RootEvent *rEvent = (RootEvent *)rDataLibrary->rActiveObject;
    rDataLibrary->PushActiveEntity();

    //--Check the Entity
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_ROOT)) return LuaTypeError("REvent_GetData");

    rDataLibrary->rActiveObject = rEvent->GetDataList();
        Hook_DataList_GetData(L);
    rDataLibrary->PopActiveEntity();
    return 1;
}
int Hook_REvent_SetData(lua_State *L)
{
    //--See DataList's version
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    RootEvent *rEvent = (RootEvent *)rDataLibrary->rActiveObject;
    rDataLibrary->PushActiveEntity();

    //--Check the Entity
    if(!rEvent || !rEvent->IsOfType(POINTER_TYPE_EVENT_ROOT)) return LuaTypeError("REvent_SetData");

    rDataLibrary->rActiveObject = rEvent->GetDataList();
        Hook_DataList_SetData(L);
    rDataLibrary->PopActiveEntity();
    return 0;
}
