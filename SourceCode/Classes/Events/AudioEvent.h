//--[AudioEvent]
//--Event that plays sound or music, or changes the audio state in some way.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootEvent.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class AudioEvent : public RootEvent
{
    private:

    protected:
    //--System
    //--Storage
    bool mIsMusic;
    char *mSoundPath;
    int mDelay;

    public:
    //--System
    AudioEvent();
    virtual ~AudioEvent();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    void SetDelay(int pTicks);
    void SetMusic(const char *pMusicName);
    void SetSound(const char *pSoundName);

    //--Core Methods
    virtual bool IsComplete();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AudioEvent_SetProperty(lua_State *L);
