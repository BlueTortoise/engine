//--[ActorEvent]
//--Event that commands a TilemapActor to do something. Depending on the case, will complete instantly, or else monitor
//  the subject until they have completed their action.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootEvent.h"

//--[Local Structures]
//--[Local Definitions]
#define AE_TYPE_NONE 0
#define AE_TYPE_FACE 1
#define AE_TYPE_TELEPORTTO 2
#define AE_TYPE_MOVETO 3
#define AE_TYPE_MOVEFIXED 4
#define AE_TYPE_STOPMOVEMENT 5
#define AE_TYPE_CHANGESPECIALFRAME 6
#define AE_TYPE_FLASHWHITE 7
#define AE_TYPE_RESETMOVETIMER 8
#define AE_TYPE_AUTODESPAWN 9
#define AE_TYPE_MOVEWHILEFACING 10
#define AE_TYPE_PARTYMERGE 11
#define AE_TYPE_PARTYAUTOFOLD 12
#define AE_TYPE_DELAYMOVETIMER 13
#define AE_TYPE_CHANGEYOFFSET 14

//--[Classes]
class ActorEvent : public RootEvent
{
    private:

    protected:
    //--System
    bool mIsComplete;
    int mInstructionType;

    //--Target
    uint32_t mTargetID;

    //--Data Storage
    union
    {
        struct
        {
            float mAmountX;
            float mAmountY;
            char mFacingTarget[32];
        }Face;
        struct
        {
            int mTargetX;
            int mTargetY;
            int mTargetZ;
        }TeleportTo;
        struct
        {
            int mTargetX;
            int mTargetY;
            float mSpeedModifier;
        }MoveTo;
        struct
        {
            int mTargetX;
            int mTargetY;
            int mFaceX;
            int mFaceY;
            float mSpeedModifier;
        }MoveToFacing;
        struct
        {
            float mAmountX;
            float mAmountY;
        }MoveFixed;
        struct
        {
            char mFrameName[32];
        }SpecialFrame;
        struct
        {
            char mFlashwhiteName[32];
        }Flashwhite;
        struct
        {
            int mTimer;
        }PartyMerge;
        struct
        {
            int mTimer;
        }DelayMoveTimer;
        struct
        {
            float mOffset;
        }ChangeYOffset;
    }Data;

    public:
    //--System
    ActorEvent();
    virtual ~ActorEvent();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    uint32_t GetSubjectID();

    //--Manipulators
    void SetTarget(uint32_t pID);
    void SetTargetByName(const char *pName);
    void SetFaceTowards(int pX, int pY);
    void SetFaceTowards(const char *pTargetName);
    void SetTeleportTo(int pX, int pY);
    void SetTeleportTo(int pX, int pY, int pZ);
    void SetMoveTo(int pX, int pY);
    void SetMoveTo(int pX, int pY, float pSpeed);
    void SetMoveToFacing(int pX, int pY, int pFaceX, int pFaceY);
    void SetMoveToFacing(int pX, int pY, int pFaceX, int pFaceY, float pSpeed);
    void SetMoveAmount(int pX, int pY);
    void SetStopMove();
    void SetChangeSpecialFrame(const char *pFrameName);
    void SetFlashwhite(const char *pSpecialFrame);
    void SetResetMoveTimer();
    void SetAutoDespawn();
    void SetPartyMerge();
    void SetPartyAutofold();
    void SetDelayMoveTimer(int pTicks);
    void SetChangeYOffset(float pOffset);

    //--Core Methods
    virtual bool IsComplete();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_ActorEvent_SetProperty(lua_State *L);
int Hook_ActorEvent_GetProperty(lua_State *L);
