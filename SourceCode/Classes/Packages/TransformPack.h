//--[TransformPack]
//--Package containing script information pertaining to transformation. Used by the CorrupterMenu when
//  the player selects the Transform tab.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--[Local Definitions]
#define TFPACK_CONSTRUCTOR 0
#define TFPACK_EXECUTE 1

//--[Classes]
class TransformPack : public RootObject
{
    private:
    //--System
    char *mLocalName;
    char *mScriptPath;

    //--Display
    SugarBitmap *rDisplayImage;
    char *mDescription;

    protected:

    public:
    //--System
    TransformPack();
    virtual ~TransformPack();

    //--Public Variables
    static SugarLinkedList *xTransformationList;

    //--Property Queries
    const char *GetName();
    const char *GetScript();
    const char *GetDescription();
    SugarBitmap *GetDisplayImage();

    //--Manipulators
    void SetName(const char *pName);
    void SetScript(const char *pPath);
    void SetDescription(const char *pPath);
    void SetDisplayImageS(const char *pDLPath);

    //--Core Methods
    void Execute();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_TF_Register(lua_State *L);
int Hook_TF_ClearList(lua_State *L);
int Hook_TF_GetProperty(lua_State *L);
int Hook_TF_SetProperty(lua_State *L);
