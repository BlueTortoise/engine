//--[PerkPack]
//--Stores a perk used by the player in Corrupter Mode. Perks can be unlocked from the Corrupter Menu,
//  and most of their execution is done in Lua. Some static functions are available to parse what perks
//  have been unlocked.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class PerkPack : public RootObject
{
    private:
    //--System
    char *mLocalName;

    //--Storage
    int mUnlockCost;
    int mUnlockCount;
    int mUnlocksMax;

    //--Description
    char *mDescription;

    //--Execution
    char *mUnlockScript;

    protected:

    public:
    //--System
    PerkPack();
    virtual ~PerkPack();

    //--Public Variables
    static SugarLinkedList *xPerkList;

    //--Property Queries
    const char *GetName();
    const char *GetDescription();
    int GetCost();
    int GetUnlockCount();
    int GetUnlocksMax();

    //--Manipulators
    void SetName(const char *pName);
    void SetCost(int pCost);
    void SetMaxUnlocks(int pMax);
    void SetDescription(const char *pDescription);
    void SetUnlockScript(const char *pScript);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Unlock();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Perk_Register(lua_State *L);
int Hook_Perk_GetProperty(lua_State *L);
int Hook_Perk_SetProperty(lua_State *L);
int Hook_Perk_Unlock(lua_State *L);
int Hook_Perk_ClearList(lua_State *L);
