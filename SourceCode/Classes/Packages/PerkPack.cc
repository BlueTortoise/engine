//--Base
#include "PerkPack.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
PerkPack::PerkPack()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_PERKPACK;

    //--[PerkPack]
    //--System
    mLocalName = InitializeString("Unnamed Perk");

    //--Storage
    mUnlockCost = 0;
    mUnlockCount = 0;
    mUnlocksMax = 1;

    //--Description
    mDescription = NULL;

    //--Execution
    mUnlockScript = NULL;
}
PerkPack::~PerkPack()
{
    free(mLocalName);
    free(mDescription);
    free(mUnlockScript);
}

//--[Public Statics]
//--Static list, contains all perks. Can be wiped/modified from the Lua state.
SugarLinkedList *PerkPack::xPerkList = new SugarLinkedList(true);

//====================================== Property Queries =========================================
const char *PerkPack::GetName()
{
    return mLocalName;
}
const char *PerkPack::GetDescription()
{
    return mDescription;
}
int PerkPack::GetCost()
{
    return mUnlockCost;
}
int PerkPack::GetUnlockCount()
{
    return mUnlockCount;
}
int PerkPack::GetUnlocksMax()
{
    return mUnlocksMax;
}

//========================================= Manipulators ==========================================
void PerkPack::SetName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void PerkPack::SetCost(int pCost)
{
    mUnlockCost = pCost;
}
void PerkPack::SetMaxUnlocks(int pMax)
{
    mUnlocksMax = pMax;
    if(mUnlockCount > mUnlocksMax) mUnlockCount = mUnlocksMax;
}
void PerkPack::SetDescription(const char *pDescription)
{
    ResetString(mDescription, pDescription);
}
void PerkPack::SetUnlockScript(const char *pScript)
{
    ResetString(mUnlockScript, pScript);
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void PerkPack::Unlock()
{
    //--Unlocks the perk, if it can be unlocked, and runs the associated script. Not all perks have
    //  or need an associated script, if they don't need to execute any logic the script can legally
    //  be NULL.
    if(mUnlockCount >= mUnlocksMax) return;
    mUnlockCount ++;

    //--Exec script, if it exists.
    if(mUnlockScript) LuaManager::Fetch()->PushExecPop(this, mUnlockScript);
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void PerkPack::HookToLuaState(lua_State *pLuaState)
{
    /* Perk_Register(sPerkName)
       Creates and register a new PerkPack, and pushes it atop the activity stack. */
    lua_register(pLuaState, "Perk_Register", &Hook_Perk_Register);

    /* Perk_GetProperty("Name") (1 string)
       Perk_GetProperty("Max Unlocks) (1 integer)
       Returns the requested property in the active PerkPack. */
    lua_register(pLuaState, "Perk_GetProperty", &Hook_Perk_GetProperty);

    /* Perk_SetProperty("Cost", iMPCost)
       Perk_SetProperty("Max Unlocks", iMaxUnlocks)
       Perk_SetProperty("Unlock Script", sPath)
       Perk_SetProperty("Description", sDescription)
       Sets the requested property in the active PerkPack. */
    lua_register(pLuaState, "Perk_SetProperty", &Hook_Perk_SetProperty);

    /* Perk_Unlock(sPerkName)
       Runs the unlock routine on the named perk, assuming it exists. If multiple perks exist with
       the same name, the first match found is used. */
    lua_register(pLuaState, "Perk_Unlock", &Hook_Perk_Unlock);

    /* Perk_ClearList()
       Deletes all perks on the static list. */
    lua_register(pLuaState, "Perk_ClearList", &Hook_Perk_ClearList);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_Perk_Register(lua_State *L)
{
    //Perk_Register(sPerkName)
    DataLibrary::Fetch()->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Perk_Register");

    //--Create.
    PerkPack *nPack = new PerkPack();
    nPack->SetName(lua_tostring(L, 1));

    //--Register.
    DataLibrary::Fetch()->rActiveObject = nPack;
    PerkPack::xPerkList->AddElement(lua_tostring(L, 1), nPack, &RootObject::DeleteThis);
    return 0;
}
int Hook_Perk_GetProperty(lua_State *L)
{
    //Perk_GetProperty("Name") (1 string)
    //Perk_GetProperty("Max Unlocks") (1 integer)
    int tArgs = lua_gettop(L);

    //--Get the class.
    PerkPack *rPerkPack = (PerkPack *)DataLibrary::Fetch()->rActiveObject;
    if(!rPerkPack || !rPerkPack->IsOfType(POINTER_TYPE_PERKPACK)) return LuaTypeError("Perk_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Perk's name.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushstring(L, rPerkPack->GetName());
    }
    //--Maximum number of times it can be unlocked.
    else if(!strcasecmp(rSwitchType, "Max Unlocks") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushinteger(L, rPerkPack->GetUnlocksMax());
    }
    //--Unknown type.
    else
    {
        LuaPropertyNoMatchError("Perk_GetProperty", rSwitchType);
    }

    return tReturns;
}
int Hook_Perk_SetProperty(lua_State *L)
{
    //Perk_SetProperty("Cost", iMPCost)
    //Perk_SetProperty("Max Unlocks", iMaxUnlocks)
    //Perk_SetProperty("Unlock Script", sPath)
    //Perk_SetProperty("Description", sDescription)
    int tArgs = lua_gettop(L);
    const char *rSwitchType = lua_tostring(L, 1);

    //--Get the class.
    PerkPack *rPerkPack = (PerkPack *)DataLibrary::Fetch()->rActiveObject;
    if(!rPerkPack || !rPerkPack->IsOfType(POINTER_TYPE_PERKPACK)) return LuaTypeError("Perk_SetProperty");

    //--What it costs to unlock.
    if(!strcasecmp(rSwitchType, "Cost") && tArgs == 2)
    {
        rPerkPack->SetCost(lua_tointeger(L, 2));
    }
    //--How many times the perk can be unlocked.
    else if(!strcasecmp(rSwitchType, "Max Unlocks") && tArgs == 2)
    {
        rPerkPack->SetMaxUnlocks(lua_tointeger(L, 2));
    }
    //--Script that executes upon unlock.
    else if(!strcasecmp(rSwitchType, "Unlock Script") && tArgs == 2)
    {
        rPerkPack->SetUnlockScript(lua_tostring(L, 2));
    }
    //--Sets the description visible when the perk is being unlocked.
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 2)
    {
        rPerkPack->SetDescription(lua_tostring(L, 2));
    }
    //--Unknown type.
    else
    {
        LuaPropertyNoMatchError("Perk_SetProperty", rSwitchType);
    }

    return 0;
}
int Hook_Perk_Unlock(lua_State *L)
{
    //Perk_Unlock(sPerkName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Perk_Unlock");

    //--Find the perk.
    PerkPack *rPerkPack = (PerkPack *)PerkPack::xPerkList->GetElementByName(lua_tostring(L, 1));
    if(!rPerkPack)
    {
        DebugManager::ForcePrint("Perk_Unlock: Error, no perk found %s\n", lua_tostring(L, 1));
        return 0;
    }

    //--Run.
    rPerkPack->Unlock();
    return 0;
}
int Hook_Perk_ClearList(lua_State *L)
{
    //Perk_ClearList()
    PerkPack::xPerkList->ClearList();
    return 0;
}
