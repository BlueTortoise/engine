//--[PairanormalSettingsMenu]
//--Menu that can be opened at any time. Allows the player to change game settings. Statically
//  stored in the MapManager since many objects access it.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
#define PSM_BTN_TODEFAULTS 0
#define PSM_BTN_CANCEL 1
#define PSM_BTN_ACCEPT 2
#define PSM_SLIDER_MUSIC 3
#define PSM_SLIDER_SOUND 4
#define PSM_SLIDER_TEXT 5
#define PSM_BTN_SWAGGER 6
#define PSM_BTN_OXYGEN 7
#define PSM_BTN_YESFLASH 8
#define PSM_BTN_NOFLASH 9
#define PSM_BTN_YESTICKS 10
#define PSM_BTN_NOTICKS 11
#define PSM_BTN_YESHINTS 12
#define PSM_BTN_NOHINTS 13
#define PSM_BTN_TOTAL 14

#define PSM_FADE_TICKS 15

//--[Classes]
class PairanormalSettingsMenu : public RootObject
{
    private:
    //--System
    int mFadeTimer;
    bool mIsOpen;

    //--Buttons
    TwoDimensionReal mButtons[PSM_BTN_TOTAL];

    //--Sliders
    int mGrabbedSlider;
    float mMusicVolumeSlider;
    float mSoundVolumeSlider;
    float mTextSpeedSlider;

    //--Starting Values
    float mStartingMusicVolume;
    float mStartingSoundVolume;
    float mStartingTextSpeed;
    bool mFlashImages;
    bool mUseSwagger;
    bool mAllowTextTicks;
    bool mShowInvestigationHints;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Pieces
            SugarBitmap *rBacking;
            SugarBitmap *rBoxChecked;
            SugarBitmap *rBoxUnchecked;
            SugarBitmap *rHeaderBox;
            SugarBitmap *rLowerBtnAccept;
            SugarBitmap *rLowerBtnClose;
            SugarBitmap *rLowerBtnDefaults;
            SugarBitmap *rVolumeSliders;

            //--Fonts
            SugarFont *rMainFont;
            SugarFont *rSwagger40;
            SugarFont *rOxygen40;
        }Data;
    }Images;

    protected:

    public:
    //--System
    PairanormalSettingsMenu();
    virtual ~PairanormalSettingsMenu();
    void Construct();

    //--Public Variables
    bool mUpdatedThisTick;
    static PairanormalSettingsMenu *xStaticMenu;

    //--Property Queries
    bool IsOpen();

    //--Manipulators
    //--Core Methods
    void Open();
    void ResetToDefaults();
    void Cancel();
    void CloseAndSave();

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();
    void NotUpdate();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    static PairanormalSettingsMenu *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

