//--Base
#include "PairanormalMenu.h"

//--Classes
#include "PairanormalLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "VirtualFile.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "SaveManager.h"
#include "SugarLumpManager.h"

//--[Worker Function]
bool CheckHeader(VirtualFile *fInfile, const char *pComparison);

void PairanormalMenu::OpenLoadMenu()
{
    //--Attempt to scan the savefiles and store the needed display data from them. First, clear the loading packs.
    mLoadPackSelected = -1;
    memset(mLoadPacks, 0, sizeof(LoadPack) * LOAD_PACKS_MAX);

    //--Path base.
    char tBuffer[256];
    const char *rPairanormalPath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sPairanormalPath");
    sprintf(tBuffer, "%s/Datafiles/Backgrounds.slf", rPairanormalPath);

    //--Open the backgrounds datafile.
    SugarLumpManager::Fetch()->Open(tBuffer);

    //--For each load pack:
    for(int i = 0; i < LOAD_PACKS_MAX; i ++)
    {
        //--Build filename.
        char tFileBuf[80];
        sprintf(tFileBuf, "%s/../../Saves/Save%02i.slf", rPairanormalPath, i);

        //--[Basic File Checking]
        //--Open the file, check it. Do this is a VirtualFile to speed up reading.
        VirtualFile *fInfile = new VirtualFile(tFileBuf, true);
        if(!fInfile->IsReady())
        {
            delete fInfile;
            continue;
        }

        //--Make sure this is a "STARv200" savefile.
        char tInBuffer[32];
        memset(tInBuffer, 0, sizeof(char) * 32);
        fInfile->SetUseOneByteForStringLengths(false);
        fInfile->Read(tInBuffer, sizeof(char), (int)strlen("STARv200"));

        //--If this is a STARv200 header, everything is fine.
        if(!strcasecmp(tInBuffer, "STARv200"))
        {
        }
        //--Error, incorrect file type.
        else
        {
            delete fInfile;
            continue;
        }

        //--[Loading Info]
        //--This information tells us what checkpoint data to use. We don't need it, we need to skip over it.
        if(!CheckHeader(fInfile, "LOADINFO_"))
        {
            delete fInfile;
            continue;
        }

        //--Read the checkpoint data.
        char *tCheckpointName = fInfile->ReadLenString();
        char *tCheckpointScript = fInfile->ReadLenString();
        char *tCheckpointArg = fInfile->ReadLenString();
        free(tCheckpointName);
        free(tCheckpointScript);
        free(tCheckpointArg);

        //--[Script Variables]
        //--Check header.
        if(!CheckHeader(fInfile, "SCRIPTVARS_"))
        {
            delete fInfile;
            continue;
        }

        //--Run. Create a dummy DataLibrary for this.
        DataLibrary *tDataLibrary = new DataLibrary();
        tDataLibrary->ReadFromFile(fInfile);

        //--The name of the player:
        SysVar *rPlayerNameVar = (SysVar *)tDataLibrary->GetEntry("Root/Variables/System/Player/Playername");
        if(rPlayerNameVar)
        {
            strcpy(mLoadPacks[i].mCharacterName, rPlayerNameVar->mAlpha);
        }
        else
        {
            strcpy(mLoadPacks[i].mCharacterName, "Jay");
        }

        //--Play time.
        SysVar *rPlayTimeVar = (SysVar *)tDataLibrary->GetEntry("Root/Variables/System/Player/PlayTime");
        if(rPlayTimeVar)
        {
            int tIntVar = (int)rPlayTimeVar->mNumeric;
            int tHours = tIntVar / 60 / 60 / 60;
            int tMinutes = (tIntVar / 60 / 60) % 60;
            sprintf(mLoadPacks[i].mPlayTime, "%i:%02i", tHours, tMinutes);
        }
        else
        {
            strcpy(mLoadPacks[i].mPlayTime, "0:00");
        }

        //--[Last Used Background]
        //--Store the last used background.
        SysVar *rLastBackground = (SysVar *)tDataLibrary->GetEntry("Root/Variables/System/Player/LastBackground");
        if(rLastBackground)
        {
            //--If the image exists, just crossload it.
            DataLibrary *rLibrary = DataLibrary::Fetch();
            if(rLibrary->DoesEntryExist(rLastBackground->mAlpha))
            {
                mLoadPacks[i].rDisplayImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(rLastBackground->mAlpha);
            }
            //--Otherwise, we need to extract it.
            else
            {
                //--Get the last part of the name out.
                char tImageBuffer[256];
                int tLastSlash = -1;
                for(int p = (int)strlen(rLastBackground->mAlpha); p >= 0; p --)
                {
                    if(rLastBackground->mAlpha[p] == '/')
                    {
                        tLastSlash = p+1;
                        break;
                    }
                }

                //--No background.
                if(tLastSlash == -1)
                {

                }
                //--Copy across.
                else
                {
                    //--Copy the name.
                    strcpy(tImageBuffer, &rLastBackground->mAlpha[tLastSlash]);

                    //--Load the image, store it in the datalibrary.
                    SugarBitmap *nNewBitmap = NULL;
                    nNewBitmap = SugarLumpManager::Fetch()->GetImage(tImageBuffer);
                    if(nNewBitmap)
                    {
                        rLibrary->RegisterPointer(rLastBackground->mAlpha, nNewBitmap, &SugarBitmap::DeleteThis);
                        mLoadPacks[i].rDisplayImage = nNewBitmap;
                    }
                }
            }
        }

        //--[Finish up]
        //--Clean up, deallocate.
        delete tDataLibrary;
        delete fInfile;
    }

    //--Clean.
    SugarLumpManager::Fetch()->Close();
}
void PairanormalMenu::UpdateLoadMenu()
{
    //--Updates the gallery controls.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--[Fade to Black]
    //--When fading to black, update fails.
    if(mIsFadingToBlack)
    {
        //--Increment.
        mFadeToBlackTimer ++;

        //--Ending case.
        if(mFadeToBlackTimer >= PM_FADE_TO_BLACK_TICKS + PM_FADE_TO_BLACK_HOLD_TICKS)
        {
            //--We need to make sure we have a path to operate from.
            const char *rPairanormalPath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sPairanormalPath");

            //--Store which load pack was in use. The load pack becomes unstable as soon as the MapManager
            //  receives the new level.
            char tSaveNameBuf[80];
            sprintf(tSaveNameBuf, "%s/../../Saves/Save%02i.slf", rPairanormalPath, mLoadPackSelected);

            //--Start a new game. DESTABILIZES THIS OBJECT.
            PairanormalLevel *nLoadingLevel = new PairanormalLevel();
            MapManager::Fetch()->ReceiveLevel(nLoadingLevel);
            PairanormalLevel::xIsRunningToCheckpoint = true;
            fprintf(stderr, "Starting run to checkpoint.\n");

            //--Execute the launcher script.
            char tPathBuffer[256];
            sprintf(tPathBuffer, "%s/Chapter 1/ZLaunch.lua", rPairanormalPath);
            LuaManager::Fetch()->ExecuteLuaFile(tPathBuffer);
            SaveManager::Fetch()->LoadPairanormalFile(tSaveNameBuf);
            ResetString(PairanormalLevel::xActiveGamePath, tSaveNameBuf);

            //--Load handler.
            sprintf(tPathBuffer, "%s/Load Handler.lua", rPairanormalPath);
            LuaManager::Fetch()->ExecuteLuaFile(tPathBuffer);

            //--Order the PairanormalLevel to load instead of normal execution.
            nLoadingLevel->RunToCheckpointAfterLoading();

            //--This object is now unstable. Return out.
            return;
        }

        //--In all cases, stop the update.
        return;
    }

    //--[Timer]
    if(mLoadFadeTimer < PM_FADE_TICKS)
    {
        mLoadFadeTimer ++;
    }

    //--[Mouse Position]
    //--Get mouse location.
    float tMouseZ;
    rControlManager->GetMouseCoordsF(mMouseX, mMouseY, tMouseZ);

    //--[Mouse Clicking]
    //--Player clicks the mouse at a location on screen.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--[Normal Case]
        //--Go back to the main menu.
        if(mLoadingBtnDim[PMBTN_LOAD_BACK].IsPointWithin(mMouseX, mMouseY))
        {
            mCurrentMode = PM_MODE_MAINMENU;
            AudioManager::Fetch()->PlaySound("UI|Select");
            RecheckFileExistence();
        }
        //--Select file 0.
        else if(mLoadingBtnDim[PMBTN_LOAD_HIGHLIGHT_0].IsPointWithin(mMouseX, mMouseY))
        {
            if(mLoadingBtnDim[PMBTN_LOAD_MOD_0].IsPointWithin(mMouseX, mMouseY) && mLoadPackSelected == 0)
            {
                xImmediateRename = true;
            }
            mLoadPackSelected = 0;
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--Select file 1.
        else if(mLoadingBtnDim[PMBTN_LOAD_HIGHLIGHT_1].IsPointWithin(mMouseX, mMouseY))
        {
            if(mLoadingBtnDim[PMBTN_LOAD_MOD_1].IsPointWithin(mMouseX, mMouseY) && mLoadPackSelected == 1)
            {
                xImmediateRename = true;
            }
            mLoadPackSelected = 1;
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--Select file 2.
        else if(mLoadingBtnDim[PMBTN_LOAD_HIGHLIGHT_2].IsPointWithin(mMouseX, mMouseY))
        {
            if(mLoadingBtnDim[PMBTN_LOAD_MOD_2].IsPointWithin(mMouseX, mMouseY) && mLoadPackSelected == 2)
            {
                xImmediateRename = true;
            }
            mLoadPackSelected = 2;
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--Play the selected file.
        else if(mLoadingBtnDim[PMBTN_LOAD_PLAY].IsPointWithin(mMouseX, mMouseY))
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            //--Fail if no file is selected.
            if(mLoadPackSelected < 0 || mLoadPackSelected >= LOAD_PACKS_MAX) return;
            if(mLoadPacks[mLoadPackSelected].mCharacterName[0] == '\0') return;

            //--Fade to black.
            mIsFadingToBlack = true;
            mFadeToBlackTimer = 0;
        }
        //--Delete the selected file.
        else if(mLoadingBtnDim[PMBTN_LOAD_DELETE].IsPointWithin(mMouseX, mMouseY))
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            //--Fail if no file is selected.
            if(mLoadPackSelected < 0 || mLoadPackSelected >= LOAD_PACKS_MAX) return;
            if(mLoadPacks[mLoadPackSelected].mCharacterName[0] == '\0') return;

            //--Path Base.
            const char *rPairanormalPath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sPairanormalPath");

            //--Build a pathname.
            char tSaveNameBuf[80];
            sprintf(tSaveNameBuf, "%s/../../Saves/Save%02i.slf", rPairanormalPath, mLoadPackSelected);

            //--Delete it.
            int tRemoved = remove(tSaveNameBuf);

            //--Remove the loading pack's information so it can't be selected again.
            if(tRemoved == 0)
            {
                mLoadPacks[mLoadPackSelected].mCharacterName[0] = '\0';
                mLoadPackSelected = -1;
            }
        }
        //--Anywhere else, unselected load pack.
        else
        {
            if(mLoadPackSelected != -1)
            {
                AudioManager::Fetch()->PlaySound("UI|Select");
                mLoadPackSelected = -1;
            }
        }
    }
}
void PairanormalMenu::RenderLoadMenu()
{
    //--[Documentation and Setup]
    //--Renders the gallery.
    if(!Images.mIsReady) return;

    //--Alpha.
    float cAlpha = (float)mLoadFadeTimer / (float)PM_FADE_TICKS;
    if(cAlpha > 1.0f) cAlpha = 1.0f;
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Render the background.
    if(rGalleryBackground)
    {
        //--Determine scale and offset.
        float cUseScale = BG_SCALE;
        float cYOffset = BG_OFFSET_Y;
        if(PairanormalLevel::xIsLowResMode)
        {
            cUseScale = cUseScale * 2.0f;
            cYOffset = cYOffset * 0.5f;
        }

        //--Render.
        glScalef(cUseScale, cUseScale, 1.0f);
        rGalleryBackground->Draw(0, BG_OFFSET_Y);
        glScalef(1.0f / cUseScale, 1.0f / cUseScale, 1.0f);
    }

    //--Darken overlay.
    StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 0.50f * cAlpha);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(            0.0f,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
        glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--UI Buttons.
    Images.Data.rIconBack->Draw();

    //--Fixed UI Parts
    Images.Data.rLoadBacking->Draw();
    Images.Data.rLoadDeleteBtn->Draw();
    Images.Data.rLoadHeader->Draw();
    Images.Data.rLoadPlayBtn->Draw();

    //--[Loading Packs]
    //--Position setup.
    float cImgLft = 321.0f;
    float cImgTop = 193.0f;
    float cImgWid = 227.0f;
    float cImgHei = 170.0f;
    float cSpacing = 270.0f;

    //--Display the pack properties.
    for(int i = 0; i < LOAD_PACKS_MAX; i ++)
    {
        //--If the name is null, skip it.
        if(mLoadPacks[i].mCharacterName[0] == '\0') continue;

        //--If this pack is selected, display the backing.
        if(mLoadPackSelected == i) Images.Data.rLoadSelectedBacking->Draw(cImgLft + (cSpacing*i) - 35.0f, cImgTop - 35.0f);

        //--Show the background if it exists.
        if(mLoadPacks[i].rDisplayImage)
        {
            mLoadPacks[i].rDisplayImage->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(0.0f, 1.0f); glVertex2f(cImgLft + (cSpacing*i),           cImgTop);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(cImgLft + (cSpacing*i) + cImgWid, cImgTop);
                glTexCoord2f(1.0f, 0.0f); glVertex2f(cImgLft + (cSpacing*i) + cImgWid, cImgTop + cImgHei);
                glTexCoord2f(0.0f, 0.0f); glVertex2f(cImgLft + (cSpacing*i),           cImgTop + cImgHei);
            glEnd();
        }

        //--Character's name.
        if(mLoadPackSelected == i) StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f * cAlpha);
        Images.Data.rDisclaimerFont->DrawTextArgs(cImgLft + (cSpacing*i) + 112.0f, cImgTop + 185.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mLoadPacks[i].mCharacterName);

        //--Play time.
        Images.Data.rDisclaimerFont->DrawTextArgs(cImgLft + (cSpacing*i) + 112.0f, cImgTop + 185.0f + 41.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mLoadPacks[i].mPlayTime);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

        //--The pencil renders on the right side of the name.
        if(mLoadPackSelected == i && !xImmediateRename) Images.Data.rLoadPencil->Draw(cImgLft + (cSpacing*i) + 229.0f, cImgTop + 261.0f);
    }

    //--[Clean]
    StarlightColor::ClearMixer();

    //--[Special]
    //--Shows a special instruction for the player when mousing over the pencil.
    //--Get mouse location.
    float tMouseX, tMouseY, tMouseZ;
    ControlManager::Fetch()->GetMouseCoordsF(tMouseX, tMouseY, tMouseZ);

    //--Mouse is over.
    if(!xImmediateRename)
    {
        if(mLoadingBtnDim[PMBTN_LOAD_MOD_0].IsPointWithin(tMouseX, tMouseY) || mLoadingBtnDim[PMBTN_LOAD_MOD_1].IsPointWithin(tMouseX, tMouseY) ||
           mLoadingBtnDim[PMBTN_LOAD_MOD_2].IsPointWithin(tMouseX, tMouseY))
        {
            Images.Data.rDisclaimerFont->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y - 80.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Click the pencil to rename your character");
            Images.Data.rDisclaimerFont->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y - 50.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "after loading the game.");
        }
    }
}
