//--Base
#include "StringTyrantTitle.h"
#include "StringTyrantTitleStructures.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"
#include "MapManager.h"

//--[Local Definitions]
//--Menu Position
#define STT_MENU_X 65.0f
#define STT_MENU_RALIGN 630.0f
#define STT_MENU_Y 137.5f
#define STT_MENU_H 40.0f
#define STT_FONT_SCALE 1.0f

//=========================================== System ==============================================
StringTyrantTitle::StringTyrantTitle()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_STRINGTYRANTTITLE;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System
    mName = NULL;
    ResetString(mName, "Unnamed Level");

    //--[StringTyrantTitle]
    //--System
    mIsVisible = true;
    mVisibilityTimer = 0;
    mPostExecScript = NULL;

    //--Background Animation
    mBGAnimTimer = 0;

    //--Fading to Black
    mIsFadingToBlack = false;
    mFadeToBlackTimer = 0;
    rFadePostExec = NULL;

    //--Game Options
    mDifficulty = STT_DIFFICULTY_NORMAL;
    strcpy(mDifficultyStrings[STT_DIFFICULTY_NORMAL], "Normal");
    strcpy(mDifficultyStrings[STT_DIFFICULTY_HARD], "Hard");
    strcpy(mDifficultyStrings[STT_DIFFICULTY_EASY], "Easy");
    mCombatType = STT_COMBAT_WAIT;
    strcpy(mCombatStrings[STT_COMBAT_WAIT], "Turn Mode");
    strcpy(mCombatStrings[STT_COMBAT_ACTIVE], "Active Mode");

    //--Display Mode
    memset(mDisplayModeBuf, 0, sizeof(mDisplayModeBuf));

    //--Menu Handling
    mHighlight = -1;
    mActiveMenu = 0;
    mMenuList = new SugarLinkedList(true);

    //--Loading Storage
    mActiveLoadSlot = 0;
    mLoadOffset = 0;
    mLoadNamesTotal = 0;
    mLoadNames = NULL;

    //--Images
    memset(&Images, 0, sizeof(Images));

    //--[Construction]
    //--Fast Access Pointers
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Fonts.
    Images.Data.rFontMenuMain  = rDataLibrary->GetFont("String Tyrant Title Large");
    Images.Data.rFontMenuSmall = rDataLibrary->GetFont("String Tyrant Title Medium");

    //--Images.
    Images.Data.rBackgrounds[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Title/Layer0");
    Images.Data.rBackgrounds[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Title/Layer1");
    Images.Data.rBackgrounds[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Title/Layer2");
    Images.Data.rBackgrounds[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Title/Layer3");
    Images.Data.rBackgrounds[4] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Title/Layer4");
    Images.Data.rBackgrounds[5] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Title/Layer5");
    Images.Data.rBackgrounds[6] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Title/Layer6");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--[Menu Builder]
    //--Main Menu. Call each of the builder functions.
    BuildMenuMain();
    BuildMenuNewgame();
    BuildMenuLoadGame();
    BuildMenuOptions();
}
StringTyrantTitle::~StringTyrantTitle()
{
    for(int i = 0; i < mLoadNamesTotal; i ++) free(mLoadNames[i]);
    free(mLoadNames);
    free(mPostExecScript);
    delete mMenuList;
}

//====================================== Property Queries =========================================
bool StringTyrantTitle::IsOfType(int pType)
{
    if(pType == POINTER_TYPE_ROOTLEVEL) return true;
    if(pType == POINTER_TYPE_STRINGTYRANTTITLE) return true;
    return false;
}

//========================================= Manipulators ==========================================
void StringTyrantTitle::SetPostExecScript(const char *pPath)
{
    ResetString(mPostExecScript, pPath);
}
void StringTyrantTitle::BeginFadeout(STTPostFadeExec pFadePostExec)
{
    if(!pFadePostExec) return;
    mIsFadingToBlack = true;
    mFadeToBlackTimer = 0;
    rFadePostExec = pFadePostExec;
}

//========================================= Core Methods ==========================================
#include "SugarFileSystem.h"
void StringTyrantTitle::ScanSavegames()
{
    //--Scans all the savegames in the Saves/ folder and stores them in an array. First, deallocate.
    for(int i = 0; i < mLoadNamesTotal; i ++) free(mLoadNames[i]);
    free(mLoadNames);

    //--Reset.
    mLoadNamesTotal = 0;
    mLoadNames = NULL;

    //--Temporary linked list for storage, since we don't know how many files we'll get.
    SugarLinkedList *tStorageList = new SugarLinkedList(false);

    //--Scan the folder.
    SugarFileSystem *tFileSystem = new SugarFileSystem();
    tFileSystem->SetRecursiveFlag(false);
    tFileSystem->ScanDirectory("Saves/");
    tFileSystem->SortDirectoryByLastAccess();

    //--Scan across the entries.
    int tTotalEntries = tFileSystem->GetTotalEntries();
    //fprintf(stderr, "Found %i files.\n", tTotalEntries);
    for(int i = 0; i < tTotalEntries; i ++)
    {
        //--Get the file, check if it's a .tls file.
        FileInfo *rFileInfo = tFileSystem->GetEntry(i);
        int tNameLen = (int)strlen(rFileInfo->mPath);
        if(strcasecmp(&rFileInfo->mPath[tNameLen-4], ".tls"))
        {
            //fprintf(stderr, " File %s is not a savefile.\n", rFileInfo->mPath);
            continue;
        }

        //--Figure out where the last '/' is in the file name. Trim the file to make it easier to read.
        int tStartAt = 0;
        for(int p = tNameLen-1; p >= 0; p --)
        {
            if(rFileInfo->mPath[p] == '/' || rFileInfo->mPath[p] == '\\')
            {
                tStartAt = p+1;
                break;
            }
        }

        //--Savefile. Store it.
        char *nString = InitializeString(&rFileInfo->mPath[tStartAt]);
        tStorageList->AddElementAsTail("X", nString);
        //fprintf(stderr, " Stored %s\n", nString);

        //--Strip off the tls.
        tNameLen = (int)strlen(nString);
        for(int p = 0; p < 4; p ++)
        {
            nString[tNameLen-p-1] = '\0';
        }
    }

    //--No savefiles found!
    if(tStorageList->GetListSize() < 1)
    {
        delete tStorageList;
        delete tFileSystem;
        PopulateSavegames();
        return;
    }

    //--Allocate space.
    mLoadNamesTotal = tStorageList->GetListSize();
    mLoadNames = (char **)starmemoryalloc(sizeof(char *) * mLoadNamesTotal);

    //--Place the entries in the array.
    int i = 0;
    char *rPtr = (char *)tStorageList->SetToHeadAndReturn();
    while(rPtr)
    {
        //--Liberate, store in array.
        tStorageList->LiberateRandomPointerEntry();
        mLoadNames[i] = rPtr;

        //--Next.
        i ++;
        rPtr = (char *)tStorageList->SetToHeadAndReturn();
    }

    //--Clean up.
    delete tStorageList;
    delete tFileSystem;

    //--[Name Load]
    //--Place the names in the menu options.
    mLoadOffset = 0;
    PopulateSavegames();

    //--[Debug]
    //--Print.
    if(false)
    {
        fprintf(stderr, "Scanned save directory. Found %i savefiles.\n", mLoadNamesTotal);
        for(int i = 0; i < mLoadNamesTotal; i ++)
        {
            fprintf(stderr, " %02i: %s\n", i, mLoadNames[i]);
        }
    }
}
void StringTyrantTitle::PopulateSavegames()
{
    //--Places the savegame names from the stored list into the menu package names.
    STTMenuPack *rMenuPack = (STTMenuPack *)mMenuList->GetElementByName("Load Game Menu");
    if(!rMenuPack) return;

    //--If there are zero savegames:
    if(mLoadNamesTotal < 1)
    {
        //--Clear strings.
        rMenuPack->SetEntryText(STT_LOADGAME_MENU_SHOWING, "No savegames found.");
        rMenuPack->SetEntryText(STT_LOADGAME_MENU_NEXT, " ");
        rMenuPack->SetEntryText(STT_LOADGAME_MENU_PREV, " ");
        for(int i = STT_LOADGAME_MENU_GAME0; i < STT_LOADGAME_MENU_TOTAL; i ++)
        {
            rMenuPack->SetEntryText(i, " ");
        }
        return;
    }

    //--Determine ranges.
    int tMax = mLoadOffset+5;
    if(tMax > mLoadNamesTotal) tMax = mLoadNamesTotal;

    //--Set.
    rMenuPack->SetEntryText(STT_LOADGAME_MENU_NEXT, "Next 5");
    rMenuPack->SetEntryText(STT_LOADGAME_MENU_PREV, "Previous 5");

    //--Main text.
    char tBuffer[128];
    sprintf(tBuffer, "Showing Files %i-%i of %i", mLoadOffset+1, tMax, mLoadNamesTotal);
    rMenuPack->SetEntryText(STT_LOADGAME_MENU_SHOWING, tBuffer);

    //--Scan.
    for(int i = STT_LOADGAME_MENU_GAME0; i < STT_LOADGAME_MENU_TOTAL; i ++)
    {
        //--Figure out which save slot this is.
        int tSaveSlot = (i - STT_LOADGAME_MENU_GAME0) + mLoadOffset;

        //--In range:
        if(tSaveSlot < mLoadNamesTotal)
        {
            sprintf(tBuffer, "File %i: %s", tSaveSlot+1, mLoadNames[tSaveSlot]);
            rMenuPack->SetEntryText(i, tBuffer);
        }
        //--Out of range:
        else
        {
            rMenuPack->SetEntryText(i, " ");
        }
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void StringTyrantTitle::Update()
{
    //--[Visiblity Handler]
    //--If visible, increment the timer.
    if(mIsVisible)
    {
        if(mVisibilityTimer < STT_VIS_TICKS) mVisibilityTimer ++;
    }
    //--Not visible, run timer. Update stops later after other timers run.
    else
    {
        if(mVisibilityTimer > 0) mVisibilityTimer --;
    }

    //--[Menu Timers]
    //--All menus except the visible one decrement their timers. Visible menu increments.
    int i = 0;
    STTMenuPack *rMenu = (STTMenuPack *)mMenuList->PushIterator();
    while(rMenu)
    {
        //--Not visible:
        if(i != mActiveMenu)
        {
            if(rMenu->mVisibilityTimer > 0) rMenu->mVisibilityTimer --;
        }
        //--Visible.
        else
        {
            if(rMenu->mVisibilityTimer < STT_MENU_SLIDE_TICKS) rMenu->mVisibilityTimer ++;
        }

        //--Next.
        i ++;
        rMenu = (STTMenuPack *)mMenuList->AutoIterate();
    }

    //--[Animation Timer]
    //--This timer determines the background animation state.
    mBGAnimTimer ++;

    //--[Fading to Black]
    //--Fadeout to black, blocks control updates.
    if(mIsFadingToBlack)
    {
        //--Timer.
        mFadeToBlackTimer ++;

        //--Ending. Execute the handler. This object is assumed to be unstable afterwards.
        if(mFadeToBlackTimer >= STT_FADE_TO_BLACK_TICKS)
        {
            mIsFadingToBlack = false;
            mFadeToBlackTimer = 0;
            rFadePostExec(this);
        }

        //--Stop update here.
        return;
    }

    //--[Stop if Not Visible]
    //--If the object is not visible, it does not handle controls.
    if(!mIsVisible) return;

    //--[Mouse Click]
    //--Fast-access pointers.
    int tMouseX, tMouseY, tMouseZ;
    ControlManager *rControlManager = ControlManager::Fetch();
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Get the active menu cluster.
    rMenu = (STTMenuPack *)mMenuList->GetElementBySlot(mActiveMenu);
    if(!rMenu) return;

    //--Ignore clicks when the menu is mostly offscreen.
    if(rMenu->mVisibilityTimer < STT_MENU_SLIDE_TICKS / 2) return;

    //--Check for Ctrl-Click.
    bool tIsCtrlClick = rControlManager->IsDown("Ctrl");

    //--Store previous highlight.
    int tPrevHighlight = mHighlight;
    if(!rControlManager->IsFirstPress("MouseLft"))
    {
        mHighlight = -1;
    }

    //--Figure out which slot the mouse is over.
    float cLPos = STT_MENU_X;
    float cTPos = STT_MENU_Y + 3.0f;
    float cBPos = cTPos + STT_MENU_H - 1.0f;
    for(int i = 0; i < rMenu->mEntries; i ++)
    {
        //--Compute length.
        float cRPos = cLPos + Images.Data.rFontMenuMain->GetTextWidth(rMenu->mEntryText[i]);

        //--If this is an option, the right-pos is the right-align position.
        if(rMenu->mOptionPacks[i].mType != POINTER_TYPE_FAIL) cRPos = STT_MENU_RALIGN;

        //--Is the mouse on the option?
        if(IsPointWithin(tMouseX, tMouseY, cLPos, cTPos, cRPos, cBPos))
        {
            //--Left-click:
            if(rControlManager->IsFirstPress("MouseLft"))
            {
                mHighlight = i;
                if(rMenu->rMenuClickHandler)
                {
                    rMenu->rMenuClickHandler(this, i, false, tIsCtrlClick);
                }
            }
            //--Right-click:
            else if(rControlManager->IsFirstPress("MouseRgt"))
            {
                mHighlight = i;
                if(rMenu->rMenuClickHandler)
                {
                    rMenu->rMenuClickHandler(this, i, true, tIsCtrlClick);
                }
            }
            //--Mouse over.
            else
            {
                mHighlight = i;
                if(mHighlight != tPrevHighlight) AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            break;
        }

        //--Next.
        cTPos = cTPos + STT_MENU_H;
        cBPos = cBPos + STT_MENU_H;
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void StringTyrantTitle::AddToRenderList(SugarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void StringTyrantTitle::RenderBackground(float pMixAlpha)
{
    //--[Documentation and Setup]
    //--Renders the background under the title, multiplied by the mix alpha. The background is dynamic
    //  and changes transparency by layer periodically.
    if(pMixAlpha <= 0.0f) return;

    //--Render the black background layer. This never changes.
    Images.Data.rBackgrounds[0]->Draw();

    //--[Mist]
    //--The mist fades in slowly. Nothing else renders.
    if(mBGAnimTimer < 45)
    {
        float cMistAlpha = EasingFunction::QuadraticInOut(mBGAnimTimer, 45);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cMistAlpha * pMixAlpha);
        Images.Data.rBackgrounds[1]->Draw();
        return;
    }

    //--Render the full mist layer.
    Images.Data.rBackgrounds[1]->Draw();

    //--[Lettering]
    //--Lettering appears. This occurs at the same time as the doll body.
    if(mBGAnimTimer < 60)
    {
        return;
    }
    else if(mBGAnimTimer < 120)
    {
        //--Alpha.
        float cLetterAlpha = EasingFunction::QuadraticInOut(mBGAnimTimer-60, 60);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cLetterAlpha * pMixAlpha);

        //--Render.
        for(int i = 2; i < STT_BG_LAYERS_TOTAL; i ++)
        {
            Images.Data.rBackgrounds[i]->Draw();
        }
        return;
    }

    //--[Lighting]
    //--Figure out if the light is flickering.
    bool tDontRenderLight = false;
    int cFlickerTick = 1;
    int tUseTimer = (mBGAnimTimer - 120) % 450;
    if(tUseTimer >= 120 && tUseTimer < 120 + cFlickerTick) tDontRenderLight = true;
    if(tUseTimer >= 125 && tUseTimer < 125 + cFlickerTick) tDontRenderLight = true;
    if(tUseTimer >= 186 && tUseTimer < 186 + cFlickerTick) tDontRenderLight = true;
    if(tUseTimer >= 192 && tUseTimer < 192 + cFlickerTick) tDontRenderLight = true;
    if(tUseTimer >= 312 && tUseTimer < 312 + cFlickerTick) tDontRenderLight = true;
    if(tUseTimer >= 318 && tUseTimer < 318 + cFlickerTick) tDontRenderLight = true;
    if(tUseTimer >= 324 && tUseTimer < 324 + cFlickerTick) tDontRenderLight = true;

    //--The lighting flickers periodically. If it's not rendering, show the doll/title.
    if(tDontRenderLight)
    {
        Images.Data.rBackgrounds[3]->Draw();
        Images.Data.rBackgrounds[4]->Draw();
    }
    //--Render all layers.
    else
    {
        for(int i = 2; i < STT_BG_LAYERS_TOTAL; i ++)
        {
            Images.Data.rBackgrounds[i]->Draw();
        }
    }
}
void StringTyrantTitle::Render()
{
    //--[Documentation and Setup]
    //--Flip this flag.
    MapManager::xHasRenderedMenus = true;

    //--Error check.
    if(!Images.mIsReady) return;

    //--Compute alpha.
    if(mVisibilityTimer < 1) return;
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, STT_VIS_TICKS);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--[Background]
    //--Handled by a subroutine. Changes the color mixer.
    RenderBackground(cAlpha);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--[Menus]
    //--Render the menu groupings.
    STTMenuPack *rMenuPack = (STTMenuPack *)mMenuList->PushIterator();
    while(rMenuPack)
    {
        //--Pack is invisible.
        if(rMenuPack->mVisibilityTimer < 1)
        {
            rMenuPack = (STTMenuPack *)mMenuList->AutoIterate();
            continue;
        }

        //--Setup.
        float cPercent = 1.0f - EasingFunction::QuarticInOut(rMenuPack->mVisibilityTimer, STT_MENU_SLIDE_TICKS);
        float cXPos = STT_MENU_X + (STT_MENU_SLIDE_DISTANCE * cPercent * -1.0f);
        float cRPos = STT_MENU_RALIGN  + (STT_MENU_SLIDE_DISTANCE * cPercent * -1.0f);
        float cYPos = STT_MENU_Y;

        //--Colors.
        StarlightColor cGrey  = StarlightColor::MapRGBAF(0.80f, 0.80f, 0.80f, cAlpha * (1.0f-cPercent));
        StarlightColor cWhite = StarlightColor::MapRGBAF(1.00f, 1.00f, 1.00f, cAlpha * (1.0f-cPercent));

        for(int i = 0; i < rMenuPack->mEntries; i ++)
        {
            //--Resolve color.
            if(i == mHighlight)
                cWhite.SetAsMixer();
            else
                cGrey.SetAsMixer();

            //--Render the left side of the entry.
            Images.Data.rFontMenuMain->DrawText(cXPos, cYPos, 0, STT_FONT_SCALE, rMenuPack->mEntryText[i]);

            //--If this is an option, render the right side. First, if a string is set, use that.
            if(rMenuPack->mOptionPacks[i].rAssociatedString)
            {
                Images.Data.rFontMenuMain->DrawText(cRPos, cYPos, SUGARFONT_RIGHTALIGN_X, STT_FONT_SCALE, rMenuPack->mOptionPacks[i].rAssociatedString);
            }
            //--Otherwise, render the integer value:
            else if(rMenuPack->mOptionPacks[i].mType == POINTER_TYPE_INT)
            {
                Images.Data.rFontMenuMain->DrawTextArgs(cRPos, cYPos, SUGARFONT_RIGHTALIGN_X, STT_FONT_SCALE, "%i", rMenuPack->mOptionPacks[i].mValue.i);
            }

            //--Move the text down.
            cYPos = cYPos + STT_MENU_H;
        }

        //--Render text matching the highlighted option, if it exists.
        if(mHighlight >= 0 && mHighlight < rMenuPack->mEntries)
        {
            float cTxtHei = Images.Data.rFontMenuSmall->GetTextHeight();
            cYPos = cYPos + STT_MENU_H;
            for(int i = 0; i < STT_OPTION_MAX_LINES; i ++)
            {
                if(rMenuPack->mOptionPacks[mHighlight].mOptionalText[i][0] == '\0') continue;
                Images.Data.rFontMenuSmall->DrawText(cXPos, cYPos, 0, 1.0f, rMenuPack->mOptionPacks[mHighlight].mOptionalText[i]);
                cYPos = cYPos + cTxtHei;
            }
        }

        //--Next.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        rMenuPack = (STTMenuPack *)mMenuList->AutoIterate();
    }

    //--[Fadeout]
    //--If fading to black, cover the screen with a transparent black rectangle.
    if(mIsFadingToBlack)
    {
        float cAlpha = EasingFunction::QuadraticOut(mFadeToBlackTimer, STT_FADE_TO_BLACK_TICKS-10);
        SugarBitmap::DrawFullBlack(cAlpha);
    }

    //--[Clean]
    //--Finish up.
    StarlightColor::ClearMixer();
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void StringTyrantTitle::HookToLuaState(lua_State *pLuaState)
{
    /* STT_Create(sPostExecPath)
       Creates a StringTyrantTitle class and makes it the active map object. */
    lua_register(pLuaState, "STT_Create", &Hook_STT_Create);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_STT_Create(lua_State *L)
{
    //STT_Create()
    StringTyrantTitle *nNewLevel = new StringTyrantTitle();
    MapManager::Fetch()->ReceiveLevel(nNewLevel);

    //--[Execution]
    int tArgs = lua_gettop(L);
    if(tArgs >= 1)
    {
        nNewLevel->SetPostExecScript(lua_tostring(L, 1));
    }

    //--Finish up.
    return 0;
}
