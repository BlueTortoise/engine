//--Base
#include "StringTyrantTitle.h"
#include "StringTyrantTitleStructures.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"
#include "MapManager.h"

//--These handlers run when a menu is clicked and handle the changes caused by the click. That's it.

//--[Main Menu]
void StringTyrantTitle::BuildMenuMain()
{
    STTMenuPack *nMainMenu = (STTMenuPack *)starmemoryalloc(sizeof(STTMenuPack));
    nMainMenu->Initialize();
    nMainMenu->AllocateEntries(STT_MAIN_MENU_TOTAL);
    nMainMenu->SetEntryText(STT_MAIN_MENU_NEWGAME,  "New Game");
    nMainMenu->SetEntryText(STT_MAIN_MENU_LOADGAME, "Load Game");
    nMainMenu->SetEntryText(STT_MAIN_MENU_OPTIONS,  "Options");
    nMainMenu->SetEntryText(STT_MAIN_MENU_QUIT,     "Quit");
    nMainMenu->SetClickHandler(&HandleClick_Main);
    mMenuList->AddElementAsTail("Main Menu", nMainMenu, STTMenuPack::DeleteThis);
}
void StringTyrantTitle::HandleClick_Main(StringTyrantTitle *pCaller, int pIndex, bool pIsRightClick, bool pIsCtrlClick)
{
    //--Called on the main menu when the player clicks an option.
    if(!pCaller) return;

    //--Open the New Game menu.
    if(pIndex == STT_MAIN_MENU_NEWGAME)
    {
        AudioManager::Fetch()->PlaySound("Menu|Select");
        pCaller->mActiveMenu = STT_MENU_NEWGAME;
    }
    //--Open the Load Game menu.
    else if(pIndex == STT_MAIN_MENU_LOADGAME)
    {
        pCaller->ScanSavegames();
        pCaller->mActiveMenu = STT_MENU_LOADGAME;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Open the Options menu.
    else if(pIndex == STT_MAIN_MENU_OPTIONS)
    {
        pCaller->mActiveMenu = STT_MENU_OPTIONS;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Quit.
    else if(pIndex == STT_MAIN_MENU_QUIT)
    {
        AudioManager::Fetch()->PlaySound("Menu|Select");
        MapManager::Fetch()->mBackToTitle = true;
        if(MapManager::xOnlyOneGame) Global::Shared()->gQuit = true;
    }
}

//--[New Game Menu]
void StringTyrantTitle::BuildMenuNewgame()
{
    //--Menu.
    STTMenuPack *nNewGameMenu = (STTMenuPack *)starmemoryalloc(sizeof(STTMenuPack));
    nNewGameMenu->Initialize();
    nNewGameMenu->AllocateEntries(STT_NEWGAME_MENU_TOTAL);
    nNewGameMenu->SetEntryText(  STT_NEWGAME_MENU_TUTORIAL,         "Tutorial");
    nNewGameMenu->SetEntryText(  STT_NEWGAME_MENU_STARTGAME,        "Start Game");
    nNewGameMenu->SetEntryText(  STT_NEWGAME_MENU_STARTGAMENOINTRO, "Start Game (Skip Intro)");
    nNewGameMenu->SetEntryText(  STT_NEWGAME_MENU_DIFFICULTY,       "Game Difficulty");
    nNewGameMenu->SetEntryText(  STT_NEWGAME_MENU_COMBATTYPE,       "Combat Style");
    nNewGameMenu->SetEntryText(  STT_NEWGAME_MENU_BACK,             "Back");
    nNewGameMenu->SetClickHandler(&HandleClick_Newgame);
    mMenuList->AddElementAsTail("New Game Menu", nNewGameMenu, STTMenuPack::DeleteThis);

    //--Set up the difficulty option.
    nNewGameMenu->SetOptionType(STT_NEWGAME_MENU_DIFFICULTY,  POINTER_TYPE_INT);
    nNewGameMenu->SetOptionValsI(STT_NEWGAME_MENU_DIFFICULTY, 0, 0);
    nNewGameMenu->SetOptionStringOverride(STT_NEWGAME_MENU_DIFFICULTY, mDifficultyStrings[STT_DIFFICULTY_NORMAL]);
    nNewGameMenu->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 0, "Normal is the difficulty the game was intended to be played at");
    nNewGameMenu->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 1, "for first time players with a mouse and keyboard.");
    nNewGameMenu->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 2, "Click to change.");

    //--Set up the combat style.
    nNewGameMenu->SetOptionType(STT_NEWGAME_MENU_COMBATTYPE,  POINTER_TYPE_INT);
    nNewGameMenu->SetOptionValsI(STT_NEWGAME_MENU_COMBATTYPE, 0, 0);
    nNewGameMenu->SetOptionStringOverride(STT_NEWGAME_MENU_COMBATTYPE, mCombatStrings[STT_COMBAT_WAIT]);
    nNewGameMenu->SetOptionText(STT_NEWGAME_MENU_COMBATTYPE, 0, "In turn-based mode, you have as much time as you need to consider your choices.");
    nNewGameMenu->SetOptionText(STT_NEWGAME_MENU_COMBATTYPE, 1, "Enemies only attack when you make a certain number of moves.");
    nNewGameMenu->SetOptionText(STT_NEWGAME_MENU_COMBATTYPE, 2, "Click to change.");
}
void StringTyrantTitle::HandleClick_Newgame(StringTyrantTitle *pCaller, int pIndex, bool pIsRightClick, bool pIsCtrlClick)
{
    //--Called on the new game menu when the player clicks an option.
    if(!pCaller) return;

    //--Start the Tutorial.
    if(pIndex == STT_NEWGAME_MENU_TUTORIAL)
    {
        pCaller->BeginFadeout(&StringTyrantTitle::Fadeout_Tutorial);
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Start a new game.
    else if(pIndex == STT_NEWGAME_MENU_STARTGAME)
    {
        pCaller->BeginFadeout(&StringTyrantTitle::Fadeout_NewGame);
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Start a new game with no introduction.
    else if(pIndex == STT_NEWGAME_MENU_STARTGAMENOINTRO)
    {
        pCaller->BeginFadeout(&StringTyrantTitle::Fadeout_NewGameNoIntro);
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Cycle the difficulty.
    else if(pIndex == STT_NEWGAME_MENU_DIFFICULTY)
    {
        //--Cycle.
        if(!pIsRightClick)
        {
            pCaller->mDifficulty = (pCaller->mDifficulty + 1) % STT_DIFFICULTY_TOTAL;
        }
        else
        {
            pCaller->mDifficulty = (pCaller->mDifficulty - 1);
            if(pCaller->mDifficulty < 0) pCaller->mDifficulty = STT_DIFFICULTY_TOTAL - 1;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");

        //--Update the associated string.
        STTMenuPack *rActivePack = (STTMenuPack *)pCaller->mMenuList->GetElementByName("New Game Menu");
        if(rActivePack)
        {
            rActivePack->SetOptionStringOverride(STT_NEWGAME_MENU_DIFFICULTY, pCaller->mDifficultyStrings[pCaller->mDifficulty]);
        }

        //--Update the string.
        if(pCaller->mDifficulty == STT_DIFFICULTY_EASY)
        {
            rActivePack->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 0, "Easy is intended for players struggling with normal, or who are");
            rActivePack->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 1, "playing with a trackpad or other disability.");
            rActivePack->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 2, "Click to change.");
        }
        else if(pCaller->mDifficulty == STT_DIFFICULTY_NORMAL)
        {
            rActivePack->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 0, "Normal is the difficulty the game was intended to be played at");
            rActivePack->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 1, "for first time players with a mouse and keyboard.");
            rActivePack->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 2, "Click to change.");
        }
        else if(pCaller->mDifficulty == STT_DIFFICULTY_HARD)
        {
            rActivePack->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 0, "Hard is for players who want a challenge and have mastered the game's");
            rActivePack->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 1, "combat system and memorized its layout. Good luck.");
            rActivePack->SetOptionText(STT_NEWGAME_MENU_DIFFICULTY, 2, "Click to change.");
        }
    }
    //--Change combat type.
    else if(pIndex == STT_NEWGAME_MENU_COMBATTYPE)
    {
        //--Cycle.
        if(pCaller->mCombatType == STT_COMBAT_WAIT)
        {
            pCaller->mCombatType = STT_COMBAT_ACTIVE;
        }
        else
        {
            pCaller->mCombatType = STT_COMBAT_WAIT;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");

        //--Update the associated string.
        STTMenuPack *rActivePack = (STTMenuPack *)pCaller->mMenuList->GetElementByName("New Game Menu");
        if(rActivePack)
        {
            rActivePack->SetOptionStringOverride(STT_NEWGAME_MENU_COMBATTYPE, pCaller->mCombatStrings[pCaller->mCombatType]);
        }

        //--Update the string.
        if(pCaller->mCombatType == STT_COMBAT_WAIT)
        {
            rActivePack->SetOptionText(STT_NEWGAME_MENU_COMBATTYPE, 0, "In turn-based mode, you have as much time as you need to consider your choices.");
            rActivePack->SetOptionText(STT_NEWGAME_MENU_COMBATTYPE, 1, "Enemies only attack when you make a certain number of moves. Think carefully.");
            rActivePack->SetOptionText(STT_NEWGAME_MENU_COMBATTYPE, 2, "Click to change.");
        }
        else if(pCaller->mCombatType == STT_COMBAT_ACTIVE)
        {
            rActivePack->SetOptionText(STT_NEWGAME_MENU_COMBATTYPE, 0, "In active mode, enemies charge attacks constantly, and you can play cards");
            rActivePack->SetOptionText(STT_NEWGAME_MENU_COMBATTYPE, 1, "as fast as you can click them. Your cards automatically draw. Think quickly!");
            rActivePack->SetOptionText(STT_NEWGAME_MENU_COMBATTYPE, 2, "Click to change.");
        }
    }
    //--Previous menu.
    else if(pIndex == STT_NEWGAME_MENU_BACK)
    {
        pCaller->mActiveMenu = STT_MENU_MAIN;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}

//--[Load Game]
void StringTyrantTitle::BuildMenuLoadGame()
{
    //--Menu.
    STTMenuPack *nLoadGameMenu = (STTMenuPack *)starmemoryalloc(sizeof(STTMenuPack));
    nLoadGameMenu->Initialize();
    nLoadGameMenu->AllocateEntries(STT_LOADGAME_MENU_TOTAL);
    nLoadGameMenu->SetEntryText(STT_LOADGAME_MENU_SHOWING, "Showing Files 1-5");
    nLoadGameMenu->SetEntryText(STT_LOADGAME_MENU_NEXT, "Next 5");
    nLoadGameMenu->SetEntryText(STT_LOADGAME_MENU_PREV, "Previous 5");
    nLoadGameMenu->SetEntryText(STT_LOADGAME_MENU_BACK, "Back to Title");
    nLoadGameMenu->SetEntryText(STT_LOADGAME_MENU_SKIP, " ");
    for(int i = STT_LOADGAME_MENU_GAME0; i < STT_LOADGAME_MENU_TOTAL; i ++)
    {
        nLoadGameMenu->SetEntryText(i, " ");
    }
    nLoadGameMenu->SetClickHandler(&HandleClick_LoadGame);
    mMenuList->AddElementAsTail("Load Game Menu", nLoadGameMenu, STTMenuPack::DeleteThis);

    //--Text for Next 5
    nLoadGameMenu->SetOptionText(STT_LOADGAME_MENU_NEXT, 0, "Show the next 5 savefiles.");
    nLoadGameMenu->SetOptionText(STT_LOADGAME_MENU_NEXT, 1, " ");
    nLoadGameMenu->SetOptionText(STT_LOADGAME_MENU_NEXT, 2, " ");

    //--Text for Prev 5
    nLoadGameMenu->SetOptionText(STT_LOADGAME_MENU_PREV, 0, "Show the previous 5 savefiles.");
    nLoadGameMenu->SetOptionText(STT_LOADGAME_MENU_PREV, 1, " ");
    nLoadGameMenu->SetOptionText(STT_LOADGAME_MENU_PREV, 2, " ");
}
void StringTyrantTitle::HandleClick_LoadGame(StringTyrantTitle *pCaller, int pIndex, bool pIsRightClick, bool pIsCtrlClick)
{
    //--Called on the load game menu when the player clicks an option.
    if(!pCaller) return;

    //--Ignore clicks on these.
    if(pIndex == STT_LOADGAME_MENU_SHOWING || pIndex == STT_LOADGAME_MENU_SKIP)
    {
    }
    //--Previous menu.
    else if(pIndex == STT_LOADGAME_MENU_BACK)
    {
        pCaller->mActiveMenu = STT_MENU_MAIN;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Next 5.
    else if(pIndex == STT_LOADGAME_MENU_NEXT)
    {
        //--Check if we have enough files.
        int tLoadPagesMax = pCaller->mLoadNamesTotal / STT_FILES_PER_PAGE;
        int tLoadPageCur  = pCaller->mLoadOffset     / STT_FILES_PER_PAGE;
        //if(pCaller->mLoadNamesTotal % STT_FILES_PER_PAGE > 0) tLoadPagesMax ++;

        //--Can increment?
        if(tLoadPageCur < tLoadPagesMax)
        {
            pCaller->mLoadOffset += STT_FILES_PER_PAGE;
            pCaller->PopulateSavegames();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Failed.
        else
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
    }
    //--Previous 5.
    else if(pIndex == STT_LOADGAME_MENU_PREV)
    {
        //--Can decrement?
        if(pCaller->mLoadOffset > 0)
        {
            pCaller->mLoadOffset -= STT_FILES_PER_PAGE;
            if(pCaller->mLoadOffset < 0) pCaller->mLoadOffset = 0;
            pCaller->PopulateSavegames();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Failed.
        else
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
    }
    //--Click was on a save file!
    else
    {
        //--Was the savefile in range?
        int tSaveIndex = pIndex - STT_LOADGAME_MENU_GAME0 + pCaller->mLoadOffset;
        if(tSaveIndex < pCaller->mLoadNamesTotal)
        {
            pCaller->mActiveLoadSlot = tSaveIndex;
            pCaller->BeginFadeout(&StringTyrantTitle::Fadeout_LoadGame);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Out of range, do nothing.
        else
        {
        }
    }
}

//--[Options]
void StringTyrantTitle::BuildMenuOptions()
{
    //--[Setup]
    //--Base.
    STTMenuPack *nOptionsMenu = (STTMenuPack *)starmemoryalloc(sizeof(STTMenuPack));
    nOptionsMenu->Initialize();
    nOptionsMenu->AllocateEntries(STT_OPTIONS_MENU_TOTAL);
    nOptionsMenu->SetEntryText(STT_OPTIONS_MENU_MUSICVOLUME, "Music Volume");
    nOptionsMenu->SetEntryText(STT_OPTIONS_MENU_SOUNDVOLUME, "Sound Volume");
    nOptionsMenu->SetEntryText(STT_OPTIONS_MENU_RESOLUTION,  "Resolution");
    nOptionsMenu->SetEntryText(STT_OPTIONS_MENU_FULLSCREEN,  "Toggle Fullscreen");
    nOptionsMenu->SetEntryText(STT_OPTIONS_MENU_APPLY,       "Apply");
    nOptionsMenu->SetEntryText(STT_OPTIONS_MENU_CANCEL,      "Cancel");
    nOptionsMenu->SetClickHandler(&HandleClick_Options);
    mMenuList->AddElementAsTail("Options Menu", nOptionsMenu, STTMenuPack::DeleteThis);

    //--[Volumes]
    //--Get the current volumes.
    int tCurrentMusicVol = (int)(AudioManager::xMusicVolume * 100.0f);
    int tCurrentSoundVol = (int)(AudioManager::xSoundVolume * 100.0f);

    //--Music is an integer, ranges from 0 to 100.
    nOptionsMenu->SetOptionType(STT_OPTIONS_MENU_MUSICVOLUME,  POINTER_TYPE_INT);
    nOptionsMenu->SetOptionValsI(STT_OPTIONS_MENU_MUSICVOLUME, tCurrentMusicVol, (int)AM_DEFAULT_MUSIC_VOLUME);
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_MUSICVOLUME, 0, "Volume of ambient music.");
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_MUSICVOLUME, 1, "Left-click to increase by 5, right-click to decrease by 5.");
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_MUSICVOLUME, 2, "Hold Ctrl to increment/decrement by 1.");

    //--Sound is an integer, ranges from 0 to 100.
    nOptionsMenu->SetOptionType(STT_OPTIONS_MENU_SOUNDVOLUME,  POINTER_TYPE_INT);
    nOptionsMenu->SetOptionValsI(STT_OPTIONS_MENU_SOUNDVOLUME, tCurrentSoundVol, (int)AM_DEFAULT_SOUND_VOLUME);
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_SOUNDVOLUME, 0, "Volume of sound effects, including combat.");
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_SOUNDVOLUME, 1, "Left-click to increase by 5, right-click to decrease by 5.");
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_SOUNDVOLUME, 2, "Hold Ctrl to increment/decrement by 1.");

    //--[Resolution]
    //--Get the display mode information from the display manager.
    int tActiveDisplayIndex;
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    DisplayInfo tDisplayInfo = rDisplayManager->GetActiveDisplayMode(tActiveDisplayIndex);
    sprintf(mDisplayModeBuf, "%ix%i %ihz", tDisplayInfo.mWidth, tDisplayInfo.mHeight, tDisplayInfo.mRefreshRate);

    //--Option setting.
    nOptionsMenu->SetOptionType(STT_OPTIONS_MENU_RESOLUTION,  POINTER_TYPE_INT);
    nOptionsMenu->SetOptionValsI(STT_OPTIONS_MENU_RESOLUTION, tActiveDisplayIndex, tActiveDisplayIndex);
    nOptionsMenu->SetOptionStringOverride(STT_OPTIONS_MENU_RESOLUTION, mDisplayModeBuf);
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_RESOLUTION, 0, "Cycles the resolution. Resolution is changed when Apply is clicked.");
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_RESOLUTION, 1, "Default resolution is 1366x768.");
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_RESOLUTION, 2, "Some systems may not allow dynamic resolution changing.");

    //--Flip Fullscreen.
    nOptionsMenu->SetOptionType(STT_OPTIONS_MENU_FULLSCREEN,  POINTER_TYPE_FAIL);
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_FULLSCREEN, 0, "Toggles fullscreen mode on or off.");
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_FULLSCREEN, 1, "This can also be done with the F11 or F12 keys.");
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_FULLSCREEN, 2, " ");

    //--[System Buttons]
    //--Apply.
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_APPLY, 0, "Apply changes and return to the previous menu.");

    //--Cancel.
    nOptionsMenu->SetOptionText(STT_OPTIONS_MENU_CANCEL, 0, "Discard changes and return to the previous menu.");
}
void StringTyrantTitle::HandleClick_Options(StringTyrantTitle *pCaller, int pIndex, bool pIsRightClick, bool pIsCtrlClick)
{
    //--Called on the options when the player clicks an option.
    if(!pCaller) return;

    //--Music volume.
    if(pIndex == STT_OPTIONS_MENU_MUSICVOLUME)
    {
        //--Setup.
        int tChangeVal = 0;

        //--Left click, no ctrl.
        if(!pIsRightClick && !pIsCtrlClick)
        {
            tChangeVal = 5;
        }
        //--Left click, ctrl.
        else if(!pIsRightClick && pIsCtrlClick)
        {
            tChangeVal = 1;
        }
        //--Right click, ctrl.
        else if(pIsRightClick && pIsCtrlClick)
        {
            tChangeVal = -1;
        }
        //--Right click, no ctrl.
        else
        {
            tChangeVal = -5;
        }

        //--Access pointer.
        STTMenuPack *rOptionsPack = (STTMenuPack *)pCaller->mMenuList->GetElementByName("Options Menu");
        if(!rOptionsPack) return;

        //--Compute change.
        int tCurrent = rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_MUSICVOLUME].mValue.i;
        tCurrent = tCurrent + tChangeVal;
        if(tCurrent > 100) tCurrent = 100;
        if(tCurrent <   0) tCurrent =   0;

        //--Set.
        rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_MUSICVOLUME].mValue.i = tCurrent;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Sound volume.
    else if(pIndex == STT_OPTIONS_MENU_SOUNDVOLUME)
    {
        //--Setup.
        int tChangeVal = 0;

        //--Left click, no ctrl.
        if(!pIsRightClick && !pIsCtrlClick)
        {
            tChangeVal = 5;
        }
        //--Left click, ctrl.
        else if(!pIsRightClick && pIsCtrlClick)
        {
            tChangeVal = 1;
        }
        //--Right click, ctrl.
        else if(pIsRightClick && pIsCtrlClick)
        {
            tChangeVal = -1;
        }
        //--Right click, no ctrl.
        else
        {
            tChangeVal = -5;
        }

        //--Access pointer.
        STTMenuPack *rOptionsPack = (STTMenuPack *)pCaller->mMenuList->GetElementByName("Options Menu");
        if(!rOptionsPack) return;

        //--Compute change.
        int tCurrent = rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_SOUNDVOLUME].mValue.i;
        tCurrent = tCurrent + tChangeVal;
        if(tCurrent > 100) tCurrent = 100;
        if(tCurrent <   0) tCurrent =   0;

        //--Set.
        rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_SOUNDVOLUME].mValue.i = tCurrent;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Resolution cycler.
    else if(pIndex == STT_OPTIONS_MENU_RESOLUTION)
    {
        //--Access pointer.
        STTMenuPack *rOptionsPack = (STTMenuPack *)pCaller->mMenuList->GetElementByName("Options Menu");
        if(!rOptionsPack) return;

        //--Increment. Only goes by 1 or -1.
        int tIncrement = 1;
        if(pIsRightClick) tIncrement = -1;
        int tValue = rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_RESOLUTION].mValue.i + tIncrement;

        //--Clamp.
        DisplayManager *rDisplayManager = DisplayManager::Fetch();
        if(tValue < 0) tValue = rDisplayManager->GetTotalDisplayModes() - 1;
        if(tValue >= rDisplayManager->GetTotalDisplayModes()) tValue = 0;

        //--Update the buffer.
        DisplayInfo tDisplayInfo = rDisplayManager->GetDisplayMode(tValue);
        sprintf(pCaller->mDisplayModeBuf, "%ix%i %ihz", tDisplayInfo.mWidth, tDisplayInfo.mHeight, tDisplayInfo.mRefreshRate);

        //--Set.
        rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_RESOLUTION].mValue.i = tValue;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Toggle fullscreen.
    else if(pIndex == STT_OPTIONS_MENU_FULLSCREEN)
    {
        DisplayManager::Fetch()->FlipFullscreen();
        ControlManager::Fetch()->BlankControls();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Previous, apply changes.
    else if(pIndex == STT_OPTIONS_MENU_APPLY)
    {
        //--Return to previous menu.
        pCaller->mActiveMenu = STT_MENU_MAIN;
        AudioManager::Fetch()->PlaySound("Menu|Select");

        //--Access package.
        STTMenuPack *rOptionsPack = (STTMenuPack *)pCaller->mMenuList->GetElementByName("Options Menu");
        if(!rOptionsPack) return;

        //--Send changes to audio manager.
        float tCurrentMusic = rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_MUSICVOLUME].mValue.i / 100.0f;
        float tCurrentSound = rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_SOUNDVOLUME].mValue.i / 100.0f;
        AudioManager::Fetch()->ChangeMusicVolumeTo(tCurrentMusic);
        AudioManager::Fetch()->ChangeSoundVolumeTo(tCurrentSound);

        //--Change the resolution if possible.
        int tActiveDisplayIndex;
        DisplayManager *rDisplayManager = DisplayManager::Fetch();
        rDisplayManager->GetActiveDisplayMode(tActiveDisplayIndex);
        if(tActiveDisplayIndex != rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_RESOLUTION].mValue.i)
        {
            rDisplayManager->SwitchDisplayModes(rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_RESOLUTION].mValue.i);
        }

        //--Info for new package.
        DisplayInfo tDisplayInfo = rDisplayManager->GetActiveDisplayMode(tActiveDisplayIndex);

        //--Set the options manager version.
        OptionsManager *rOptionsManager = OptionsManager::Fetch();
        rOptionsManager->SetOptionI("WinSizeX", tDisplayInfo.mWidth);
        rOptionsManager->SetOptionI("WinSizeY", tDisplayInfo.mHeight);
        rOptionsManager->SetOptionI("MusicVolume", rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_MUSICVOLUME].mValue.i);
        rOptionsManager->SetOptionI("SoundVolume", rOptionsPack->mOptionPacks[STT_OPTIONS_MENU_SOUNDVOLUME].mValue.i);
        rOptionsManager->WriteConfigFiles();
    }
    //--Previous.
    else if(pIndex == STT_OPTIONS_MENU_CANCEL)
    {
        pCaller->mActiveMenu = STT_MENU_MAIN;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
