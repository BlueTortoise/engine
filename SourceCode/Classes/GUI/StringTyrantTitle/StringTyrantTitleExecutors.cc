//--Base
#include "StringTyrantTitle.h"
#include "StringTyrantTitleStructures.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "LuaManager.h"

//--When the title screen fades out, these functions get called as a callback. Typically they launch the game
//  with certain settings.

void StringTyrantTitle::Fadeout_Tutorial(StringTyrantTitle *pCaller)
{
    //--After fadeout is completed, launches the tutorial.
    if(!pCaller) return;
    LuaManager::Fetch()->ExecuteLuaFile(pCaller->mPostExecScript, 1, "S", "Tutorial");
}
void StringTyrantTitle::Fadeout_NewGame(StringTyrantTitle *pCaller)
{
    //--After fadeout is completed, starts a new game.
    if(!pCaller) return;

    //--Figure out the difficulty.
    char tBuffer[128];
    strcpy(tBuffer, "New Game|");
    if(pCaller->mDifficulty == STT_DIFFICULTY_NORMAL)
    {
        strcat(tBuffer, "Normal|");
    }
    else if(pCaller->mDifficulty == STT_DIFFICULTY_HARD)
    {
        strcat(tBuffer, "Hard|");
    }
    else
    {
        strcat(tBuffer, "Easy|");
    }

    //--Add on Active/Wait for combat.
    if(pCaller->mCombatType == STT_COMBAT_ACTIVE)
    {
        strcat(tBuffer, "Active");
    }
    else
    {
        strcat(tBuffer, "Wait");
    }

    //--Call.
    LuaManager::Fetch()->ExecuteLuaFile(pCaller->mPostExecScript, 1, "S", tBuffer);
}
void StringTyrantTitle::Fadeout_NewGameNoIntro(StringTyrantTitle *pCaller)
{
    //--After fadeout is completed, starts a new game, skips the intro.
    if(!pCaller) return;

    //--Figure out the difficulty.
    char tBuffer[128];
    strcpy(tBuffer, "New Game NoTut|");
    if(pCaller->mDifficulty == STT_DIFFICULTY_NORMAL)
    {
        strcat(tBuffer, "Normal|");
    }
    else if(pCaller->mDifficulty == STT_DIFFICULTY_HARD)
    {
        strcat(tBuffer, "Hard|");
    }
    else
    {
        strcat(tBuffer, "Easy|");
    }

    //--Add on Active/Wait for combat.
    if(pCaller->mCombatType == STT_COMBAT_ACTIVE)
    {
        strcat(tBuffer, "Active");
    }
    else
    {
        strcat(tBuffer, "Wait");
    }

    //--Call.
    LuaManager::Fetch()->ExecuteLuaFile(pCaller->mPostExecScript, 1, "S", tBuffer);
}
void StringTyrantTitle::Fadeout_LoadGame(StringTyrantTitle *pCaller)
{
    //--After fadeout is completed, loads the game.
    if(!pCaller) return;

    //--Make sure the savegame exists.
    if(pCaller->mActiveLoadSlot < 0 || pCaller->mActiveLoadSlot >= pCaller->mLoadNamesTotal) return;

    //--Call!
    char tBuffer[128];
    strcpy(tBuffer, "Load Game|");
    strcat(tBuffer, pCaller->mLoadNames[pCaller->mActiveLoadSlot]);
    LuaManager::Fetch()->ExecuteLuaFile(pCaller->mPostExecScript, 1, "S", tBuffer);
}
