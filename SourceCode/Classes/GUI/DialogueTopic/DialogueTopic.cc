//--Base
#include "DialogueTopic.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

//=========================================== System ==============================================
DialogueTopic::DialogueTopic()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_DIALOGUETOPIC;

    //--[DialogueTopic]
    //--System
    mLocalName = NULL;
    mDisplayName = NULL;

    //--Level
    mCurrentLevel = 0;

    //--NPC Registration
    mNPCListing = new SugarLinkedList(true);
}
DialogueTopic::~DialogueTopic()
{
    free(mLocalName);
    free(mDisplayName);
    delete mNPCListing;
}

//====================================== Property Queries =========================================
const char *DialogueTopic::GetInternalName()
{
    return (const char *)mLocalName;
}
const char *DialogueTopic::GetDisplayName()
{
    return (const char *)mDisplayName;
}
int DialogueTopic::GetLevel()
{
    return mCurrentLevel;
}
int DialogueTopic::GetLevelOfNPC(const char *pNPCName)
{
    DTNPCRegPack *rCheckPack = (DTNPCRegPack *)mNPCListing->GetElementByName(pNPCName);
    if(!rCheckPack) return -1;
    return rCheckPack->mLastSpokeLevel;
}
bool DialogueTopic::HasDiscussedBefore(const char *pNPCName)
{
    //--Get the pack. If it doesn't exist, return false.
    DTNPCRegPack *rCheckPack = (DTNPCRegPack *)mNPCListing->GetElementByName(pNPCName);
    if(!rCheckPack) return false;

    //--If the NPC's dialogue level is equal or less than the current level, they have not discussed this before.
    return (mCurrentLevel <= rCheckPack->mLastSpokeLevel);
}

//========================================= Manipulators ==========================================
void DialogueTopic::SetName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void DialogueTopic::SetDisplayName(const char *pName)
{
    ResetString(mDisplayName, pName);
}
void DialogueTopic::SetLevel(int pLevel)
{
    mCurrentLevel = pLevel;
}
void DialogueTopic::RegisterNPC(const char *pNPCName, int pLastLevel)
{
    //--NPC already exists: Fail.
    DTNPCRegPack *rCheckPack = (DTNPCRegPack *)mNPCListing->GetElementByName(pNPCName);
    if(rCheckPack) return;

    //--Create, add.
    SetMemoryData(__FILE__, __LINE__);
    DTNPCRegPack *nPack = (DTNPCRegPack *)starmemoryalloc(sizeof(DTNPCRegPack));
    nPack->mLastSpokeLevel = pLastLevel;
    mNPCListing->AddElement(pNPCName, nPack, &FreeThis);
}
void DialogueTopic::SetNPCLevel(const char *pNPCName, int pLevel)
{
    DTNPCRegPack *rCheckPack = (DTNPCRegPack *)mNPCListing->GetElementByName(pNPCName);
    if(!rCheckPack)
    {
        return;
    }
    rCheckPack->mLastSpokeLevel = pLevel;
}
void DialogueTopic::MarkAsUnread()
{
    //--Go through all NPCs and flag them as not having read the topic.
    DTNPCRegPack *rCheckPack = (DTNPCRegPack *)mNPCListing->PushIterator();
    while(rCheckPack)
    {
        if(rCheckPack->mLastSpokeLevel >= mCurrentLevel)
        {
            rCheckPack->mLastSpokeLevel = mCurrentLevel - 1;
        }

        rCheckPack = (DTNPCRegPack *)mNPCListing->AutoIterate();
    }
}
void DialogueTopic::RegisterOrOverrideNPC(const char *pNPCName, int pLevel)
{
    //--Used during loading. If the NPC does not exist, adds it. Otherwise, overrides it level with
    //  the provided level.
    if(!pNPCName) return;

    //--Get the pack. If it exists, override.
    DTNPCRegPack *rCheckPack = (DTNPCRegPack *)mNPCListing->GetElementByName(pNPCName);
    if(rCheckPack)
    {
        rCheckPack->mLastSpokeLevel = pLevel;
        return;
    }

    //--Register it if it doesn't exist.
    SetMemoryData(__FILE__, __LINE__);
    DTNPCRegPack *nPack = (DTNPCRegPack *)starmemoryalloc(sizeof(DTNPCRegPack));
    nPack->mLastSpokeLevel = pLevel;
    mNPCListing->AddElement(pNPCName, nPack, &FreeThis);
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
SugarLinkedList *DialogueTopic::GetNPCList()
{
    return mNPCListing;
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
