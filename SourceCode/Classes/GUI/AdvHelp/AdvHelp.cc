//--Base
#include "AdvHelp.h"

//--Classes
//--CoreClasses
#include "StarlightString.h"
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"

//=========================================== System ==============================================
AdvHelp::AdvHelp()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVHELP;

    //--[AdvHelp]
    //--System
    mStringsTotal = 0;
    mStrings = NULL;

    //--Render
    mIsReady = false;
    rHeadingFont = NULL;
    rMainlineFont = NULL;
    rBacking = NULL;
}
AdvHelp::~AdvHelp()
{
    for(int i = 0; i < mStringsTotal; i ++)
    {
        delete mStrings[i];
    }
    free(mStrings);
}
void AdvHelp::Construct()
{
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rHeadingFont = rDataLibrary->GetFont("Adventure Help Heading");
    rMainlineFont = rDataLibrary->GetFont("Adventure Help Mainline");
    rBacking = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/HelpBacking");
    mIsReady = (rHeadingFont && rMainlineFont && rBacking);
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void AdvHelp::AllocateStrings(int pTotal)
{
    //--Deallocate.
    for(int i = 0; i < mStringsTotal; i ++) delete mStrings[i];
    free(mStrings);

    //--Zero.
    mStringsTotal = 0;
    mStrings = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mStringsTotal = pTotal;
    mStrings = (StarlightString **)starmemoryalloc(sizeof(StarlightString *) * mStringsTotal);
    for(int i = 0; i < mStringsTotal; i ++) mStrings[i] = new StarlightString();
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void AdvHelp::Render(float pYOffset)
{
    //--[Documentation and Setup]
    //--Render the help window with a Y offset passed in by the parent class.
    if(!mIsReady) return;

    //--Position.
    glTranslatef(0.0f, pYOffset, 0.0f);

    //--[Fixed Components]
    //--Backing.
    rBacking->Draw();

    //--Heading.
    rHeadingFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 196.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Controls");

    //--[Render Lines]
    //--Position.
    float cTxtLft = 403.0f;
    float tTxtTop = 233.0f;
    float cTxtHei = rHeadingFont->GetTextHeight();

    //--Iterate.
    for(int i = 0; i < mStringsTotal; i ++)
    {
        mStrings[i]->DrawText(cTxtLft, tTxtTop, 0, 1.0f, rMainlineFont);
        tTxtTop = tTxtTop + cTxtHei;
    }

    //--[Clean Up]
    glTranslatef(0.0f, -pYOffset, 0.0f);
}

//======================================= Pointer Routing =========================================
StarlightString **AdvHelp::GetStrings()
{
    return mStrings;
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
