//--Base
#include "WorldDialogue.h"

//--Classes
#include "AdventureLevel.h"
#include "DialogueActor.h"
#include "DialogueTopic.h"
#include "StringEntry.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"

//--[Debug]
//#define WD_SAVE_DEBUG
#ifdef WD_SAVE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//=========================================== System ==============================================
WorldDialogue::WorldDialogue()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_WORLDDIALOGUE;

    //--[WorldDialogue]
    //--System
    mIsAnyKeyMode = false;
    mNeedsToReappend = false;
    mAutoHastenDialogue = false;
    mInstantTextMode = false;
    mVisibilityState = WD_INVISIBLE;
    mHidingTimer = -1;
    mAllKeysHasten = false;

    //--Visual Novel Mode
    mIsVisualNovel = false;
    VN.mBackgroundTimer = 0;
    VN.rCurBackground = NULL;
    VN.rPrevBackground = NULL;
    VN.mBackgroundRemaps = new SugarLinkedList(true);
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        VN.mActorCurrents[i] = 0.0f;
        VN.mActorStarts[i] = 0.0f;
        VN.mActorTargets[i] = 0.0f;
        VN.mActorTimers[i] = 0;
        VN.mActorTimersMax[i] = 0;
        VN.mActorMoveFlags[i] = WD_VN_MOVETPYE_NONE;
        VN.mActorPostExecs[i] = NULL;
    }

    //--Sound
    mIsSilenced = false;
    mTextTickTimer = 0.0f;

    //--Position
    mTextScale = 1.0f;
    mDimensions.SetWH(0.0f, 576.0f, VIRTUAL_CANVAS_X, 192.0f);
    mUpperDimensions.SetWH(0.0f, 0.0f, VIRTUAL_CANVAS_X, 221.0f);

    //--Colors
    mWhitePack.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);

    //--Dialogue Storage
    mHasAppendedNonSpace = false;
    mDialogueUsesAutoresolveTarget = true;
    mFadeTimer = 0;
    mIsPrintingComplete = false;
    mMajorSequenceSpeaker = NULL;
    mMajorSequenceSpeakerDisplay = NULL;
    mSpeakerNameRemaps = new SugarLinkedList(true);
    mDialogueLog = new SugarLinkedList(true);
    mDialogueList = new SugarLinkedList(true);

    //--Decisions
    mTicksSinceDecisionOpen = 0;
    mIsDecisionMode = false;
    mDecisionCursor = 0;
    mHighlightedDecision = -1;
    mDecisionScale = 2.0f;
    mDecisionList = new SugarLinkedList(true);
    mDecisionDimensions[0].Set(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.40f, 1.0f, 1.0f);
    mDecisionDimensions[1].Set(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.50f, 1.0f, 1.0f);
    mDecisionDimensions[2].Set(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.60f, 1.0f, 1.0f);

    //--Topics
    mRunTopicsAfterDialogue = false;
    memset(mRunTopicsAfterDialogueName, 0, sizeof(char) * STD_NPC_LETTERS);
    mIsTopicsMode = false;
    mTopicTimer = 0;
    mTopicCursor = 0;
    mTopicScroll = 0;
    mGoodbyeTopic = new DialogueTopic();
    mGoodbyeTopic->SetName("Goodbye");
    mGoodbyeTopic->SetDisplayName("Goodbye");
    mTopicListing = new SugarLinkedList(true);
    mrCurrentTopicListing = new SugarLinkedList(false);

    //--Continuation
    mContinuationScript = NULL;
    mContinuationString = NULL;

    //--Blocking
    mIsBlocking = false;
    mIsSoftBlocking = false;
    mSoftBlockTimer = 0;
    mPendingDialogue = NULL;

    //--Temporary
    mHideDialogueBoxes = false;
    mIsSpookyText = false;
    mLettersAppended = 0;
    rActiveAppendList = NULL;

    //--Complex Actor Properties
    mIsMajorSequenceMode = false;
    mMajorSequenceNeedsReset = true;
    mMajorSequenceTimer = 0;
    mActorBench = new SugarLinkedList(true);
    memset(mDialogueActors, 0, sizeof(DialogueActor *) * WD_DIALOGUE_ACTOR_SLOTS);
    memset(mPortraitLookupsX, 0, sizeof(float) * WD_DIALOGUE_ACTOR_SLOTS);

    //--Actor Storage
    memset(mPreviousActorNames,    0, WD_DIALOGUE_ACTOR_SLOTS * WD_ACTOR_NAME_MAX);
    memset(mPreviousActorEmotions, 0, WD_DIALOGUE_ACTOR_SLOTS * WD_ACTOR_NAME_MAX);

    //--Crossfades
    memset(rPreviousActors, 0, sizeof(DialogueActor *) * WD_DIALOGUE_ACTOR_SLOTS);
    memset(rPreviousImages, 0, sizeof(SugarBitmap *) * WD_DIALOGUE_ACTOR_SLOTS);
    memset(mPreviousFadeTimers, 0, sizeof(int) * WD_DIALOGUE_ACTOR_SLOTS);

    //--CG and TF Scene Handling
    mIsSceneMode = false;
    mIgnoreSceneOffsets = false;
    mSceneOffsetX = 0.0f;
    mSceneOffsetY = 0.0f;
    rSceneImg = NULL;
    mSceneTimer = 0;
    mSceneFadeTimeMax = WD_SCENE_CROSSFADE_TICKS;
    mSceneTicksPerFrame = 1;
    mSceneAnimFramesTotal = 0;
    mrSceneAnimFrames = NULL;
    mSceneTransitionTimer = 0;
    rOldSceneImg = NULL;
    mOldSceneOffsetX = 0.0f;
    mOldSceneOffsetY = 0.0f;

    //--Scene Flash
    mIsSceneFlash = false;
    mSceneFlashTimer = 0;

    //--Voice Controller
    mIsFreshClear = true;
    mLeaderVoice = InitializeString("NoLeader");
    mLastSetSpeaker = InitializeString("NoLeader");
    rCurrentVoice = mNormalTextTick;
    strcpy(mNormalTextTick, "Menu|TextTick");
    mVoiceListing = new SugarLinkedList(true);

    //--String Entry
    mIsStringEntryMode = false;
    mStringEntryCallOnComplete = NULL;
    mStringEntryForm = NULL;

    //--Credits Sequence
    mIsCreditsSequence = false;
    mCreditsTimerCur = 0;
    mCreditsTimerMax = 1;
    mCreditsPacks = NULL;
    rCreditsBackground = NULL;

    //--Images
    memset(&Images, 0, sizeof(Images));
}
WorldDialogue::~WorldDialogue()
{
    delete VN.mBackgroundRemaps;
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++) free(VN.mActorPostExecs[i]);
    delete mSpeakerNameRemaps;
    delete mDialogueLog;
    delete mDialogueList;
    delete mDecisionList;
    free(mPendingDialogue);
    delete mGoodbyeTopic;
    delete mTopicListing;
    delete mrCurrentTopicListing;
    free(mContinuationScript);
    free(mContinuationString);
    delete mActorBench;
    free(mMajorSequenceSpeaker);
    free(mMajorSequenceSpeakerDisplay);
    free(mrSceneAnimFrames);
    free(mLeaderVoice);
    free(mLastSetSpeaker);
    delete mVoiceListing;
    delete mStringEntryForm;
    delete mCreditsPacks;
}
void WorldDialogue::Construct()
{
    //--[Image Loading]
    //--Builds resources after setup has been completed.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(Images.mIsReady) return;

    //--Borders
    Images.Data.rBorderCard  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/BorderCard");
    Images.Data.rNameBox     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/NamelessBox");
    Images.Data.rNamelessBox = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/NamelessBox");
    Images.Data.rNamePanel   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Dialogue/NamePanel");

    //--Fonts.
    Images.Data.rHeadingFont  = rDataLibrary->GetFont("World Dialogue Heading");
    Images.Data.rDecisionFont = rDataLibrary->GetFont("World Dialogue Decision");
    Images.Data.rDialogueFont = rDataLibrary->GetFont("World Dialogue Main");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--[Rendering Constants]
    //--Note: These constants can change when the field becomes full. See MoveActorToSlot() for more.
    mPortraitLookupsX[0] = VIRTUAL_CANVAS_X * 0.20f;
    mPortraitLookupsX[1] = VIRTUAL_CANVAS_X * 0.30f;
    mPortraitLookupsX[2] = VIRTUAL_CANVAS_X * 0.40f;
    mPortraitLookupsX[3] = VIRTUAL_CANVAS_X * 0.85f;
    mPortraitLookupsX[4] = VIRTUAL_CANVAS_X * 0.70f;
    mPortraitLookupsX[5] = VIRTUAL_CANVAS_X * 0.60f;

    //--Heavy left version. Used for when there are four characters on the left side.
    mPortraitLookupsXHeavyLft[0] = VIRTUAL_CANVAS_X * 0.20f;
    mPortraitLookupsXHeavyLft[1] = VIRTUAL_CANVAS_X * 0.30f;
    mPortraitLookupsXHeavyLft[2] = VIRTUAL_CANVAS_X * 0.40f;
    mPortraitLookupsXHeavyLft[3] = VIRTUAL_CANVAS_X * 0.90f;
    mPortraitLookupsXHeavyLft[4] = VIRTUAL_CANVAS_X * 0.75f;
    mPortraitLookupsXHeavyLft[5] = VIRTUAL_CANVAS_X * 0.65f;
    mPortraitLookupsXHeavyLft[6] = VIRTUAL_CANVAS_X * 0.10f;
}

//--[Public Statics]
//--Variable that tracks how many appends are in use, since the Append() function is re-entrant. Special
//  behaviors may occur on the 0th stack.
int WorldDialogue::xAppendStack = 0;

//--Set at AdventureMode startup, determines where to look for topics when NPCs enter topics mode.
char *WorldDialogue::xTopicsDirectory = NULL;

//--Parsing flag. Indicates the last parsed letter is '::' which resolves to ':'.
int WorldDialogue::xIsColon = 0;

//--Y distance, in pixels, between the lower and upper dialogues.
float WorldDialogue::xUpperDialogueYOffset = -540.0f;

//====================================== Property Queries =========================================
bool WorldDialogue::IsVisible()
{
    if(mHidingTimer != -1) return false;
    if(mVisibilityState == WD_INVISIBLE) return false;
    return true;
}
bool WorldDialogue::IsMajorSequence()
{
    return mIsMajorSequenceMode;
}
bool WorldDialogue::IsBlocking()
{
    return mIsBlocking;
}
bool WorldDialogue::IsStoppingEvents()
{
    //--If not visible, can't stop events.
    if(!IsVisible()) return false;
    if(IsVisible()) return true;

    //--If the dialogue is still printing, events stop.
    if(!mIsPrintingComplete)
    {
        return true;
    }

    //--If currently waiting on a key press, events stop.
    if(mIsBlocking)
    {
        return true;
    }

    //--All checks passed, events not stopped.
    return false;
}
bool WorldDialogue::HasTextFlowing()
{
    return !mIsPrintingComplete;
}
bool WorldDialogue::IsAutoHastening()
{
    return mAutoHastenDialogue;
}

//========================================= Manipulators ==========================================
void WorldDialogue::Show()
{
    Wipe();
    mHideDialogueBoxes = false;
    mIsFreshClear = true;
    mIsSilenced = false;
    mVisibilityState = WD_VISIBLE;
}
void WorldDialogue::Hide()
{
    //--Can't hide if already hiding!
    if(mHidingTimer != -1) return;
    mHidingTimer = 0;
}
void WorldDialogue::SetAnyKeyMode(bool pFlag)
{
    mIsAnyKeyMode = pFlag;
}
void WorldDialogue::ActivateFlash(int pStartTicks)
{
    mIsSceneFlash = true;
    mSceneFlashTimer = pStartTicks;
}
void WorldDialogue::SetTargetAutoresolve(bool pFlag)
{
    mDialogueUsesAutoresolveTarget = pFlag;
}
void WorldDialogue::BypassFade()
{
    //--Use this to stop fading after calling Show().
    mMajorSequenceTimer = WD_MAJOR_SEQUENCE_FADE_TICKS;
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        mPreviousFadeTimers[i] = WD_CHARACTER_CROSSFADE_TICKS;
    }
}
void WorldDialogue::SetSilenceFlag(bool pFlag)
{
    mIsSilenced = pFlag;
}
void WorldDialogue::AddDialogueLockout()
{
    AdventureLevel *rFetchLevel = AdventureLevel::Fetch();
    if(rFetchLevel) rFetchLevel->AddLockout(WD_STANDARD_LOCKOUT);
}
void WorldDialogue::ClearDialogueLockout()
{
    AdventureLevel *rFetchLevel = AdventureLevel::Fetch();
    if(rFetchLevel) rFetchLevel->RemoveLockout(WD_STANDARD_LOCKOUT);
}
void WorldDialogue::SetSpeaker(const char *pSpeakerName)
{
    //--Reset the voice to the default text tick.
    rCurrentVoice = mNormalTextTick;

    //--Store this as the last set speaker. This happens even if the speaker is invalid for some reason.
    ResetString(mLastSetSpeaker, pSpeakerName);

    //--Portrait handling. Modifies the visiblity states of all actors. The bench is used here,
    //  since theoretically a benched actor could enter the scene and want their grey state set.
    //--If no speaker gets set, all actors are ungreyed.
    if(!pSpeakerName || !strcasecmp(pSpeakerName, "Null") || !strcasecmp(pSpeakerName, "Narrator"))
    {
        //--Iterate.
        DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
        while(rActor)
        {
            rActor->SetDarkenedState(true);
            rActor = (DialogueActor *)mActorBench->AutoIterate();
        }
    }
    //--If the speaker is "Thought" then the party leader is highlighted.
    else if(!strcasecmp(pSpeakerName, "Thought"))
    {
        //--Iterate.
        DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
        while(rActor)
        {
            //--Set.
            rActor->SetDarkeningFromName(mLeaderVoice);

            //--Next.
            rActor = (DialogueActor *)mActorBench->AutoIterate();
        }

        //--Override the voice to that of the leader.
        SetVoice(mLeaderVoice);
    }
    //--In all other cases, the Actors will set their grey state based on their aliases.
    else
    {
        //--Iterate.
        DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
        while(rActor)
        {
            //--Set.
            rActor->SetDarkeningFromName(pSpeakerName);

            //--If the Actor is not darkened, check their name against the voice list. If a match is found, set that as the active voice.
            //  The actor must be on the stage or they won't set their voice, since some actors share aliases.
            int tActorSlot = IsActorOnStage(rActor);
            if(!rActor->IsDarkened() && tActorSlot != -1)
            {
                //--Search voice list. Set that voice.
                char *rCheckVoice = (char *)mVoiceListing->GetElementByName(rActor->GetName());
                if(rCheckVoice) rCurrentVoice = rCheckVoice;
            }

            //--Next.
            rActor = (DialogueActor *)mActorBench->AutoIterate();
        }
    }
}
void WorldDialogue::AddSpeakerRemap(const char *pSpeakerName, const char *pRemapName)
{
    //--Adds a name that will be remapped. The pSpeakerName will be replaced with the pRemapName
    //  when it appears in a [NAME|pSpeakerName] tag, and also when setting the speaker with SetSpeaker().
    if(!pSpeakerName || !pRemapName) return;
    mSpeakerNameRemaps->AddElement(pSpeakerName, InitializeString(pRemapName), &FreeThis);
}
int WorldDialogue::IsActorOnStage(void *pCheckPtr)
{
    //--Returns which slot the actor is in, or -1 if it's not on the stage.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++) if(mDialogueActors[i] == pCheckPtr) return i;
    return -1;
}
void WorldDialogue::SetContinuationScript(const char *pPath, const char *pString)
{
    ResetString(mContinuationScript, pPath);
    ResetString(mContinuationString, pString);
}
void WorldDialogue::SetVoice(const char *pSpeaker)
{
    rCurrentVoice = (char *)mVoiceListing->GetElementByName(pSpeaker);
}
void WorldDialogue::SetLeaderVoice(const char *pLeaderVoice)
{
    if(!pLeaderVoice) return;
    ResetString(mLeaderVoice, pLeaderVoice);
}
void WorldDialogue::RegisterVoice(const char *pSpeaker, const char *pVoicePath)
{
    //--Note: Speaker will refer to the DialogueActor, *not* the alias! "Alraune" covers "Rochea", "Adina" and "Alraune", for example.
    if(!pSpeaker || !pVoicePath) return;

    //--If the entry already exists, replace it.
    mVoiceListing->RemoveElementS(pSpeaker);
    mVoiceListing->AddElement(pSpeaker, InitializeString(pVoicePath), &FreeThis);
}
void WorldDialogue::SetHideFlag(bool pHideDialogueBoxes)
{
    mHideDialogueBoxes = pHideDialogueBoxes;
}
void WorldDialogue::SetAutoHastenFlag(bool pAutoHastenDialogue)
{
    mAutoHastenDialogue = pAutoHastenDialogue;
}
void WorldDialogue::SetInstantTextFlag(bool pFlag)
{
    mInstantTextMode = pFlag;
}
void WorldDialogue::BypassBlock()
{
    //--Do nothing if not blocking.
    if(!mIsBlocking) return;

    //--Store the old pending dialogue.
    char *tOldPendingDialogue = InitializeString(mPendingDialogue);

    //--Clear the pointer to it in case AppendString() needs to use it again.
    mPendingDialogue = NULL;

    //--Re-append. Artificially increase the appendation stack so the emphasis does not change.
    //  Not that a [CLEAR] tag will override this.
    mIsBlocking = false;
    mIsSoftBlocking = false;
    xAppendStack += 2;
    AppendString(tOldPendingDialogue);
    xAppendStack -= 2;

    //--Clean up the old dialogue.
    free(tOldPendingDialogue);
}
void WorldDialogue::ActivateStringEntry(const char *pPostExec)
{
    //--Error check.
    if(!pPostExec || mIsStringEntryMode) return;

    //--Flags.
    mIsStringEntryMode = true;
    ResetString(mStringEntryCallOnComplete, pPostExec);

    //--If this is the first time using the string entry, construct it.
    if(!mStringEntryForm)
    {
        mStringEntryForm = new StringEntry();
        mStringEntryForm->SetString(NULL);
        mStringEntryForm->SetCompleteFlag(false);
        mStringEntryForm->SetPromptString("Enter password:");
        mStringEntryForm->SetEmptyString("Password");
    }
    //--Otherwise, just reset it.
    else
    {
        mStringEntryForm->SetString(NULL);
        mStringEntryForm->SetCompleteFlag(false);
        mStringEntryForm->SetPromptString("Enter password:");
        mStringEntryForm->SetEmptyString("Password");
    }
}
void WorldDialogue::SetAllKeysHasten(bool pFlag)
{
    mAllKeysHasten = pFlag;
}

//========================================= Core Methods ==========================================
void WorldDialogue::Clear()
{
    //--Clears all dialogue off the screen. This only affects whichever dialogue box is rendering.
    mDialogueLog->ClearList();
    mDialogueList->ClearList();

    //--Common.
    mIsSpookyText = false;
    rActiveAppendList = NULL;

    //--Reset flags.
    mHasAppendedNonSpace = false;
    mFadeTimer = 0;
    mTextTickTimer = 0.0f;
    mIsPrintingComplete = true;
    rCurrentVoice = NULL;

    //--When in scenes mode, the current voice auto-switches to the text tick.
    if(mIsSceneMode) rCurrentVoice = mNormalTextTick;
}
void WorldDialogue::Wipe()
{
    //--[Documentation]
    //--Wipes the dialogue back to factory zero. Used whenever this object is hidden.

    //--[WorldDialogue]
    //--System
    mNeedsToReappend = false;
    mVisibilityState = WD_INVISIBLE;
    mHidingTimer = -1;
    mAllKeysHasten = false;

    //--Sound
    mIsSilenced = false;
    mTextTickTimer = 0.0f;

    //--Position
    //--Colors
    //--Dialogue Storage
    mHasAppendedNonSpace = false;
    mFadeTimer = 0;
    mIsPrintingComplete = false;
    ResetString(mMajorSequenceSpeaker, NULL);
    ResetString(mMajorSequenceSpeakerDisplay, NULL);
    mDialogueLog->ClearList();
    mDialogueList->ClearList();

    //--Decisions
    mIsDecisionMode = false;
    mDecisionList->ClearList();

    //--Topics
    mRunTopicsAfterDialogue = false;
    mIsTopicsMode = false;

    //--Continuation
    ResetString(mContinuationScript, NULL);
    ResetString(mContinuationString, NULL);

    //--Blocking
    mIsBlocking = false;
    ResetString(mPendingDialogue, NULL);

    //--Temporary
    rActiveAppendList = NULL;

    //--Complex Actor Properties
    mIsMajorSequenceMode = false;
    mMajorSequenceNeedsReset = true;
    mMajorSequenceResetTimer = Global::Shared()->gTicksElapsed;
    memset(rPreviousImages, 0, sizeof(SugarBitmap *) * WD_DIALOGUE_ACTOR_SLOTS);
    memset(mPreviousFadeTimers, 0, sizeof(int) * WD_DIALOGUE_ACTOR_SLOTS);
    memset(mDialogueActors, 0, sizeof(DialogueActor *) * WD_DIALOGUE_ACTOR_SLOTS);

    //--Scene mode.
    mIsSceneMode = false;
    mIgnoreSceneOffsets = false;
    mSceneOffsetX = 0.0f;
    mSceneOffsetY = 0.0f;
    rSceneImg = NULL;
    mSceneTimer = 0;
    mSceneTicksPerFrame = 1;
    mSceneAnimFramesTotal = 0;
    free(mrSceneAnimFrames);
    mrSceneAnimFrames = NULL;
    rCurrentVoice = NULL;

    //--Reset these position flags.
    mPortraitLookupsX[0] = VIRTUAL_CANVAS_X * 0.20f;
    mPortraitLookupsX[1] = VIRTUAL_CANVAS_X * 0.30f;
    mPortraitLookupsX[2] = VIRTUAL_CANVAS_X * 0.40f;
}
SugarLinkedList *WorldDialogue::ResolveActiveDialogueList()
{
    return mDialogueList;
}
float WorldDialogue::ComputeLengthOf(SugarLinkedList *pListOfCharacters)
{
    //--Provided a list containing DialogueCharacter structures, computes the length of the resulting string
    //  and returns it. This takes scaling into account.
    //--Because these are individual characters, we can use the kerning lookup tables.
    if(!pListOfCharacters || !Images.mIsReady) return 0.0f;

    //--Setup.
    float tRunningWidth = 0.0f;

    //--Iterate.
    DialogueCharacter *rCharacter = (DialogueCharacter *)pListOfCharacters->PushIterator();
    while(rCharacter)
    {
        //--Get the next character.
        DialogueCharacter *rNextCharacter = (DialogueCharacter *)pListOfCharacters->AutoIterate();

        //--Next character exists:
        if(rNextCharacter)
        {
            tRunningWidth = tRunningWidth + Images.Data.rDialogueFont->GetKerningBetween(rCharacter->mLetter, rNextCharacter->mLetter);
        }
        //--Next character does not exist, pass a NULL for the next character.
        else
        {
            tRunningWidth = tRunningWidth + Images.Data.rDialogueFont->GetKerningBetween(rCharacter->mLetter, '\0');
        }

        //--Iterate.
        rCharacter = rNextCharacter;
    }

    //--All done, return.
    return tRunningWidth;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void WorldDialogue::Update()
{
    //--Debug.
    DebugPush(true, "WorldDialogue:Update - Begin.\n");

    //--Credits.
    if(mIsCreditsSequence)
    {
        UpdateCredits();
        return;
    }

    //--Run the update routine for all dialogue actors. All they do is modify internal timers.
    DialogueActor *rActor = (DialogueActor *)mActorBench->PushIterator();
    while(rActor)
    {
        if(!mIsMajorSequenceMode) rActor->SetDarkenedState(true);
        rActor->Update();
        rActor = (DialogueActor *)mActorBench->AutoIterate();
    }

    //--When hiding, do nothing except run the hide timer.
    if(mHidingTimer != -1)
    {
        mHidingTimer ++;
        if(mHidingTimer >= WD_HIDING_TICKS)
        {
            mHidingTimer = -1;
            mVisibilityState = WD_INVISIBLE;
            Wipe();
            DeactivateScenesMode();
        }
        return;
    }

    //--Scene flash.
    if(mIsSceneFlash)
    {
        mSceneFlashTimer ++;
    }

    //--Run timer.
    mSceneTimer ++;

    //--Crossfades.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        if(mPreviousFadeTimers[i] < WD_CHARACTER_CROSSFADE_TICKS)
        {
            mPreviousFadeTimers[i] ++;
        }

        //--All timers autocomplete when fading in.
        if(mIsMajorSequenceMode && mMajorSequenceTimer < WD_MAJOR_SEQUENCE_FADE_TICKS)
        {
            mPreviousFadeTimers[i] = WD_CHARACTER_CROSSFADE_TICKS;
        }
    }

    //--Scene transition timer.
    if(mIsSceneMode && mSceneTransitionTimer < mSceneFadeTimeMax)
    {
        mSceneTransitionTimer ++;
    }

    //--Major sequence timer.
    if(mMajorSequenceTimer < WD_MAJOR_SEQUENCE_FADE_TICKS)
    {
        mMajorSequenceTimer ++;
        for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
        {
            mPreviousFadeTimers[i] = WD_CHARACTER_CROSSFADE_TICKS;
        }
    }

    //--Visual Novel Update
    if(mIsVisualNovel)
    {
        UpdateVisualNovel();
    }

    //--In topics mode, pass the update off.
    if(mIsTopicsMode)
    {
        UpdateTopicsMode();
        DebugPop("WorldDialogue:Update - Ended from topics mode.\n");
        return;
    }

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Iterate across the lines, update the timers.
    if(!mIsPrintingComplete)
    {
        //--Debug.
        DebugPrint("Printing is incomplete.\n");

        //--Flags.
        bool tAnyLettersFading = false;
        int tIncrementBy = 1;
        float cUseTickInterval = WD_TEXT_TICK_INTERVAL;

        bool tIsHoldingActivate = rControlManager->IsDown("Activate");
        bool tIsHoldingCancel = rControlManager->IsDown("Cancel");
        if(mIsAnyKeyMode)
        {
            tIsHoldingActivate = rControlManager->IsAnyKeyDown();
        }

        //--If the player is holding down the Activate key, text scrolls faster.
        if(tIsHoldingActivate || mAutoHastenDialogue)
        {
            tIncrementBy = 3;
            cUseTickInterval = cUseTickInterval * WD_TEXT_TICK_FACTOR;
        }
        //--All keys hasten.
        else if(mAllKeysHasten && rControlManager->IsAnyKeyDown())
        {
            tIncrementBy = 3;
            cUseTickInterval = cUseTickInterval * WD_TEXT_TICK_FACTOR;
        }

        //--If the player is holding down the Cancel key, text scrolls super-fast!
        if(tIsHoldingCancel)
        {
            tIncrementBy = 10;
            cUseTickInterval = cUseTickInterval * WD_TEXT_TICK_FACTOR * 3.0f;
        }

        //--Instant text scroll.
        if(mInstantTextMode)
        {
            tIncrementBy = 10000000;
        }

        //--Resolve the active dialogue list.
        SugarLinkedList *rActiveList = ResolveActiveDialogueList();

        //--Begin iterating.
        DebugPrint("Iterating.\n");
        SugarLinkedList *rLetterList = (SugarLinkedList *)rActiveList->PushIterator();
        while(rLetterList)
        {
            //--Iterate.
            DialogueCharacter *rCharacterPack = (DialogueCharacter *)rLetterList->PushIterator();
            while(rCharacterPack)
            {
                //--Does this character need to be updated?
                if(rCharacterPack->mTimer < WD_LETTER_FADE_TICKS)
                {
                    //--Timer.
                    rCharacterPack->mTimer += tIncrementBy;

                    //--Recompute the alpha.
                    if(rCharacterPack->mTimer >= 0 && rCharacterPack->mTimer < WD_LETTER_FADE_TICKS)
                    {
                        //--Text tick handling.
                        if(!tAnyLettersFading)
                        {
                            //--Timer.
                            mTextTickTimer = mTextTickTimer + 1.0f;

                            //--Sound effect.
                            if(mTextTickTimer >= cUseTickInterval && !mIsSilenced)
                            {
                                if(cUseTickInterval > 0.0f) mTextTickTimer = fmodf(mTextTickTimer, cUseTickInterval);

                                //--If there's no voice, use the default tick.
                                if(!rCurrentVoice)
                                {
                                    if(!IsFlashSuppressingTicks()) AudioManager::Fetch()->PlaySound("Menu|TextTick");
                                }
                                //--Use the character's voice.
                                else
                                {
                                    if(!IsFlashSuppressingTicks()) AudioManager::Fetch()->PlaySound(rCurrentVoice);
                                }
                            }
                        }

                        //--Flags.
                        tAnyLettersFading = true;
                        rCharacterPack->mAlpha = rCharacterPack->mTimer / (float)WD_LETTER_FADE_TICKS;
                    }
                    //--Clamp alpha at 1.0f.
                    else if(rCharacterPack->mTimer >= WD_LETTER_FADE_TICKS)
                    {
                        rCharacterPack->mAlpha = 1.0f;
                    }
                }

                //--Next letter.
                rCharacterPack = (DialogueCharacter *)rLetterList->AutoIterate();
            }

            //--Next line.
            rLetterList = (SugarLinkedList *)rActiveList->AutoIterate();
        }

        //--If no letters were fading, then fading is complete.
        if(!tAnyLettersFading)
        {
            mIsPrintingComplete = true;
        }
    }
    //--Printing has been completed. Keypresses will advance the dialogue.
    else
    {
        //--Blocking state. The player must press the activate key to resume dialogue.
        if(mIsBlocking)
        {
            //--Debug.
            DebugPrint("In block state.\n");

            //--Player is pressing "Activate". In any-key mode, any key will do.
            bool tIsPressingActivate = rControlManager->IsFirstPress("Activate");
            bool tIsHoldingActivate = rControlManager->IsDown("Activate");
            bool tIsPressingCancel = rControlManager->IsFirstPress("Cancel");
            bool tIsHoldingCancel = rControlManager->IsDown("Cancel");
            if(mIsAnyKeyMode)
            {
                tIsPressingActivate = rControlManager->IsAnyKeyPressed();
                tIsPressingCancel = rControlManager->IsAnyKeyPressed();
            }

            //--Soft block timer. Waits half a second, then advances the dialogue automatically.
            if(mIsSoftBlocking)
            {
                mSoftBlockTimer ++;
                if(tIsHoldingCancel) mSoftBlockTimer = 25;
            }
            else
            {
                mSoftBlockTimer = 0;
            }

            //--Special: If the player is holding down cancel and activate at the same time, we never wait for keypresses.
            if(tIsHoldingActivate && tIsHoldingCancel)
            {
                mSoftBlockTimer = 25;
            }

            //--Must be a new keypress, or the soft-block timer activating.
            if(tIsPressingActivate || tIsPressingCancel || mSoftBlockTimer >= 25)
            {
                //--Store the old pending dialogue.
                char *tOldPendingDialogue = InitializeString(mPendingDialogue);

                //--Clear the pointer to it in case AppendString() needs to use it again.
                mPendingDialogue = NULL;

                //--Re-append. Artificially increase the appendation stack so the emphasis does not change.
                //  Not that a [CLEAR] tag will override this.
                mIsBlocking = false;
                mIsSoftBlocking = false;
                xAppendStack += 2;
                AppendString(tOldPendingDialogue);
                xAppendStack -= 2;

                //--Clean up the old dialogue.
                free(tOldPendingDialogue);
            }
        }
        //--Decision state.
        else if(mIsDecisionMode)
        {
            DebugPrint("In decision state.\n");
            UpdateDecisions();
        }
        //--String Entry
        else if(mIsStringEntryMode)
        {
            DebugPrint("In string entry state.\n");
            if(mStringEntryForm)
            {
                //--Run update.
                mStringEntryForm->Update();

                //--Completion case.
                if(mStringEntryForm->IsComplete())
                {
                    //--Get the string and make a copy.
                    const char *rExecString = mStringEntryForm->GetString();
                    char *tCallOnCompleteCopy = InitializeString(mStringEntryCallOnComplete);

                    //--Something illegal happened, like entering a string with no letters.
                    if(!rExecString)
                    {
                        //--Clean. We need to do this before the lua call, in case that creates a new string entry.
                        mIsStringEntryMode = false;
                        free(mStringEntryCallOnComplete);
                        mStringEntryCallOnComplete = NULL;

                        LuaManager::Fetch()->ExecuteLuaFile(tCallOnCompleteCopy, 1, "S", "Null");
                        free(tCallOnCompleteCopy);
                        return;
                    }

                    //--Make a copy of the passed string.
                    char *tCopyString = InitializeString(rExecString);

                    //--Clean. We need to do this before the lua call, in case that creates a new string entry.
                    mIsStringEntryMode = false;
                    free(mStringEntryCallOnComplete);
                    mStringEntryCallOnComplete = NULL;

                    //--Execute.
                    LuaManager::Fetch()->ExecuteLuaFile(tCallOnCompleteCopy, 1, "S", tCopyString);

                    //--Clean up.
                    free(tCallOnCompleteCopy);
                    free(tCopyString);
                }
                //--Cancel case. Always passes "Null".
                else if(mStringEntryForm->IsCancelled())
                {
                    //--Make a copy of the call-on-complete string.
                    char *tCallOnCompleteCopy = InitializeString(mStringEntryCallOnComplete);

                    //--Clean up before execution, in case the lua execution starts a new string entry.
                    mIsStringEntryMode = false;
                    free(mStringEntryCallOnComplete);
                    mStringEntryCallOnComplete = NULL;

                    //--This passes "Null" to the script.
                    LuaManager::Fetch()->ExecuteLuaFile(tCallOnCompleteCopy, 1, "S", "Null");

                    //--Clean.
                    free(tCallOnCompleteCopy);
                }
            }
        }
        //--Activate topics mode if flagged.
        else if(mRunTopicsAfterDialogue)
        {
            DebugPrint("Activating topics.\n");
            mRunTopicsAfterDialogue = false;
            ActivateTopicsMode(mRunTopicsAfterDialogueName);
        }
        //--Normal case.
        else
        {
            //--Player is pressing "Activate". In any-key mode, any key will do.
            bool tIsPressingActivate = rControlManager->IsFirstPress("Activate");
            bool tIsPressingCancel = rControlManager->IsFirstPress("Cancel");
            if(mIsAnyKeyMode)
            {
                tIsPressingActivate = rControlManager->IsAnyKeyPressed();
                tIsPressingCancel = rControlManager->IsAnyKeyPressed();
            }

            //--Pressing this closes the dialogue or fires the next part of the sequence.
            DebugPrint("Waiting for close.\n");
            if((tIsPressingActivate || tIsPressingCancel))
            {
                DebugPrint("Handling close.\n");
                HandleCloseOrContinue();
            }
            //--Special: Holding down both Activate and Cancel auto-proceeds.
            else if(rControlManager->IsDown("Activate") && rControlManager->IsDown("Cancel"))
            {
                DebugPrint("Handling close.\n");
                HandleCloseOrContinue();
            }
        }
    }

    //--Debug.
    DebugPop("WorldDialogue:Update - Ended normally.\n");
}
void WorldDialogue::HandleCloseOrContinue()
{
    //--When the dialogue runs out of... dialogue... this function is called. If there is no more
    //  dialogue pending (the continuation strings are NULL) then this object closes. Otherwise,
    //  the continuation strings are used to run another script and presumably either generate
    //  more dialogue or fire an event sequence.
    if(!mContinuationScript || !mContinuationString)
    {
        //--Clear any dialogue locks.
        AdventureLevel *rFetchLevel = AdventureLevel::Fetch();
        if(rFetchLevel) rFetchLevel->RemoveLockout(WD_STANDARD_LOCKOUT);

        //--Run the hiding sequence.
        Hide();
        return;
    }

    //--If we got this far, then there is both a continuation path and string. First, we need to
    //  copy the reference pointer since the new script will probably modify the continuation strings.
    char *rRefCopyScript = mContinuationScript;
    char *rRefCopyString = mContinuationString;
    mContinuationScript = NULL;
    mContinuationString = NULL;

    //--Now run the script in question.
    LuaManager::Fetch()->ExecuteLuaFile(rRefCopyScript, 1, "S", rRefCopyString);

    //--Clean up.
    free(rRefCopyScript);
    free(rRefCopyString);
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void WorldDialogue::Render()
{
    //--Rendering cycle. Does nothing if the object is not visible in any way, or the images did not resolve.
    if(!Images.mIsReady || mVisibilityState == WD_INVISIBLE) return;

    //--Debug.
    DebugPush(true, "WorldDialogue:Render - Begin.\n");

    //--Subsequences.
    if(mIsCreditsSequence)
    {
        RenderCredits();
        DebugPop("WorldDialogue:Render - Ended via credits.\n");
        return;
    }
    if(mIsMajorSequenceMode)
    {
        RenderMajorSequence();
        DebugPop("WorldDialogue:Render - Ended via major sequence.\n");
        return;
    }
    if(mIsSceneMode)
    {
        RenderScenesMode();
        DebugPop("WorldDialogue:Render - Ended via scenes.\n");
        return;
    }
    if(mIsTopicsMode)
    {
        RenderTopicsMode();
        DebugPop("WorldDialogue:Render - Ended via topics.\n");
        return;
    }

    //--Mixing.
    float tAlphaFactor = 1.0f;
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);

    //--Positioning.
    float tCursorX = 0.0f;
    float tCursorY = 0.0f;

    //--If there is a speaker:
    bool tIsSpeakerCase = (mMajorSequenceSpeaker && strcasecmp(mMajorSequenceSpeaker, "Null") && strcasecmp(mMajorSequenceSpeaker, "Narrator") && strcasecmp(mMajorSequenceSpeaker, "Thought"));
    if(tIsSpeakerCase)
    {
        //--Render.
        Images.Data.rNameBox->Draw();
        Images.Data.rNamePanel->Draw();
        Images.Data.rHeadingFont->DrawText(264.0f, 563.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, mMajorSequenceSpeaker);

        //--Cursor position.
        tCursorX =  42.0f;
        tCursorY = 600.0f;
    }
    //--No speaker.
    else
    {
        //--Render.
        Images.Data.rNamelessBox->Draw();

        //--Cursor position.
        tCursorX =  42.0f;
        tCursorY = 600.0f;
    }

    //--Subroutine renders the letters themselves.
    RenderOnlyText(tCursorX, tCursorY, tAlphaFactor);

    //--[Decisions]
    //--Renders over everything else, but only when active.
    if(mIsDecisionMode && !mIsBlocking && mIsPrintingComplete) RenderDecisions();

    //--[String Entry]
    if(mIsStringEntryMode && mStringEntryForm && mIsPrintingComplete) mStringEntryForm->Render();

    //--[Flash]
    RenderFlash();
    DebugPop("WorldDialogue:Render - Ended normally.\n");
}
void WorldDialogue::RenderOnlyText(float pX, float pY, float pAlphaFactor)
{
    //--Subroutine that only renders the text using the provided offsets. This is used both internally and for the
    //  TextLevel handlers since they use the same inputs.

    //--Reposition the rendering cursor for the dialogue.
    glTranslatef(pX, pY, 0.0f);

    //--Now start rendering.
    SugarLinkedList *rLetterList = (SugarLinkedList *)mDialogueList->PushIterator();
    while(rLetterList)
    {
        //--Offset value. Resets for each line.
        float tLineCursor = 0.0f;

        //--Iterate.
        DialogueCharacter *rCharacterPack = (DialogueCharacter *)rLetterList->PushIterator();
        while(rCharacterPack)
        {
            //--Spooky characters change color.
            if(rCharacterPack->mIsSpooky)
            {
                if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 2)
                {
                    StarlightColor::SetMixer(1.0f, 0.4f, 0.2f, rCharacterPack->mAlpha * pAlphaFactor);
                }
                else if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 4)
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 0.1f, rCharacterPack->mAlpha * pAlphaFactor);
                }
                else
                {
                    StarlightColor::SetMixer(1.0f, 0.2f, 0.5f, rCharacterPack->mAlpha * pAlphaFactor);
                }
            }
            //--Fixed color.
            else if(rCharacterPack->rColor)
            {
                StarlightColor::SetMixer(rCharacterPack->rColor->r, rCharacterPack->rColor->g, rCharacterPack->rColor->b, rCharacterPack->mAlpha * pAlphaFactor);
            }
            //--White.
            else
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, rCharacterPack->mAlpha * pAlphaFactor);
            }

            //--Next letter. It may be NULL. This also does the work of iterating.
            DialogueCharacter *rNextLetterPack = (DialogueCharacter *)rLetterList->AutoIterate();

            //--Render the letter.
            char tNextLetter = '\0';
            if(rNextLetterPack) tNextLetter = rNextLetterPack->mLetter;
            float tAdvance = Images.Data.rDialogueFont->DrawLetter(tLineCursor, 0.0f, 0, mTextScale, rCharacterPack->mLetter, tNextLetter);

            //--Move the cursor over.
            tLineCursor = tLineCursor + (tAdvance * mTextScale);
            rCharacterPack = rNextLetterPack;
        }

        //--Move the cursor down.
        pY = pY + (Images.Data.rDialogueFont->GetTextHeight() * mTextScale);
        glTranslatef(0.0f, (Images.Data.rDialogueFont->GetTextHeight() * mTextScale), 0.0f);

        //--Next line.
        rLetterList = (SugarLinkedList *)mDialogueList->AutoIterate();
    }

    //--Clean up.
    StarlightColor::ClearMixer();
    glTranslatef(pX * -1, pY * -1, 0.0f);
}
bool WorldDialogue::IsFlashSuppressingTicks()
{
    //--Not if we're not flashing!
    if(!mIsSceneFlash) return false;

    //--Constants
    int cSeq1Timer = 10;
    int cSeq2Timer = cSeq1Timer + 60;

    //--Checks.
    if(mSceneFlashTimer <= cSeq2Timer - 40) return false;
    if(mSceneFlashTimer <= cSeq2Timer - 40 + 240) return true;
    return false;
}
void WorldDialogue::RenderFlash()
{
    //--Flashes white. Used for rune animations.
    if(!mIsSceneFlash) return;

    //--Hasn't started yet.
    if(mSceneFlashTimer < 0) return;

    //--Constants
    int cSeq1Timer = 10;
    int cSeq2Timer = cSeq1Timer + 60;
    int cSeq3Timer = cSeq2Timer + 5;
    int cSeq4Timer = cSeq3Timer + 120;
    int cSeq5Timer = cSeq4Timer + 120;

    //--SFX.
    if(mSceneFlashTimer == cSeq2Timer - 40) AudioManager::Fetch()->PlaySound("World|Flash");

    //--Ramps.
    float cRamp0 = 1.0f;
    float cRamp1 = 3.0f;
    float cRamp2 = 5.0f;
    float cRamp3 = CANY - cRamp2 - cRamp1 - cRamp0;

    //--Position.
    float cYMid = CANY * 0.50f;

    //--Disable textures.
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);

    //--Initial.
    if(mSceneFlashTimer < cSeq1Timer)
    {
        //--Compute height.
        float cPercent = EasingFunction::QuadraticIn(mSceneFlashTimer, cSeq1Timer);
        float cHeight = cRamp0 + (cRamp1 * cPercent);

        //--Render top.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(0.0f, cYMid - cHeight);
        glVertex2f(CANX, cYMid - cHeight);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(CANX, cYMid + 0.0f);
        glVertex2f(0.0f, cYMid + 0.0f);

        //--Render bottom.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(0.0f, cYMid - 0.0f);
        glVertex2f(CANX, cYMid - 0.0f);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(CANX, cYMid + cHeight);
        glVertex2f(0.0f, cYMid + cHeight);
    }
    //--Minor ramp up.
    else if(mSceneFlashTimer < cSeq2Timer)
    {

        //--Compute height.
        float cPercent = EasingFunction::QuadraticOut(mSceneFlashTimer - cSeq1Timer, cSeq2Timer - cSeq1Timer);
        float cHeight = (cRamp0 + cRamp1) + (cRamp2 * cPercent);

        //--Render top.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(0.0f, cYMid - cHeight);
        glVertex2f(CANX, cYMid - cHeight);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(CANX, cYMid + 0.0f);
        glVertex2f(0.0f, cYMid + 0.0f);

        //--Render bottom.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(0.0f, cYMid - 0.0f);
        glVertex2f(CANX, cYMid - 0.0f);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(CANX, cYMid + cHeight);
        glVertex2f(0.0f, cYMid + cHeight);
    }
    //--Rapid flash up.
    else if(mSceneFlashTimer < cSeq3Timer)
    {

        //--Compute height.
        float cPercent = EasingFunction::QuadraticIn(mSceneFlashTimer - cSeq2Timer, cSeq3Timer - cSeq2Timer);
        float cHeight = (cRamp0 + cRamp1 + cRamp2) + (cRamp3* cPercent);

        //--Render top.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(0.0f, cYMid - cHeight);
        glVertex2f(CANX, cYMid - cHeight);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(CANX, cYMid + 0.0f);
        glVertex2f(0.0f, cYMid + 0.0f);

        //--Render bottom.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
        glVertex2f(0.0f, cYMid - 0.0f);
        glVertex2f(CANX, cYMid - 0.0f);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.0f);
        glVertex2f(CANX, cYMid + cHeight);
        glVertex2f(0.0f, cYMid + cHeight);
    }
    //--Fullwhite.
    else if(mSceneFlashTimer < cSeq4Timer)
    {
        glVertex2f(0.0f, 0.0f);
        glVertex2f(CANX, 0.0f);
        glVertex2f(CANX, CANY);
        glVertex2f(0.0f, CANY);
    }
    //--Fades down.
    else if(mSceneFlashTimer < cSeq5Timer)
    {
        float cPercent = 1.0f - EasingFunction::QuadraticIn(mSceneFlashTimer - cSeq4Timer, cSeq5Timer - cSeq4Timer);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPercent);
        glVertex2f(0.0f, 0.0f);
        glVertex2f(CANX, 0.0f);
        glVertex2f(CANX, CANY);
        glVertex2f(0.0f, CANY);
    }
    //--Cancel.
    else
    {
        mIsSceneFlash = false;
        AllocateSceneAnimFrames(0, 1, 0);
    }

    //--Clean.
    glEnd();
    glEnable(GL_TEXTURE_2D);
}

//======================================= Pointer Routing =========================================
DialogueActor *WorldDialogue::GetActor(const char *pName)
{
    return (DialogueActor *)mActorBench->GetElementByName(pName);
}
SugarLinkedList *WorldDialogue::GetTopicList()
{
    return mTopicListing;
}

//====================================== Static Functions =========================================
WorldDialogue *WorldDialogue::Fetch()
{
    //--Cannot legally return NULL.
    return MapManager::Fetch()->GetWorldDialogue();
}

