//--Base
#include "WorldDialogue.h"

//--Classes
#include "AdventureLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "OptionsManager.h"

//#define WD_CREDITS_DEBUG
#ifdef WD_CREDITS_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--[Structure Functions]
void CreditsPack::Initialize()
{
    //--System
    mDisplayType = WD_CREDITS_TYPE_NONE;
    mTimerOffset = 0;

    //--Fonts and Flags
    mX = 0.0f;
    mY = 0.0f;
    mScale = 1.0f;
    mFlags = 0x0000;
    rFont = NULL;
    mDisplayText[0] = '\0';

    //--Image Handler
    rDisplayImage = NULL;

    //--Fade-in/Fade-out
    mFadeTicksIn = 0;
    mFadeTicksHold = 0;
    mFadeTicksOut = 0;

    //--Scrolling
    mScrollPixPerTick = 0.0f;
}
void CreditsPack::Delete()
{
}
void CreditsPack::DeleteThis(void *pPtr)
{
    CreditsPack *rPack = (CreditsPack *)pPtr;
    rPack->Delete();
    free(rPack);
}
bool CreditsPack::IsComplete(int pWorldTicks)
{
    //--Common:
    int tUseTimer = pWorldTicks + mTimerOffset;

    //--Fade in: Fades in, holds, and out. Then, ends.
    if(mDisplayType == WD_CREDITS_TYPE_FADEIN)
    {
        if(tUseTimer > mFadeTicksIn + mFadeTicksHold + mFadeTicksOut) return true;
        return false;
    }

    //--Error.
    return true;
}
void CreditsPack::Update()
{

}
void CreditsPack::Render(int pWorldTicks)
{
    //--Common:
    int tUseTimer = pWorldTicks + mTimerOffset;
    if(tUseTimer < 1) return;

    //--Fade in: Fades in, holds, and out. Then, ends.
    if(mDisplayType == WD_CREDITS_TYPE_FADEIN)
    {
        //--Compute alpha.
        float tAlpha = 1.0f;
        if(tUseTimer < mFadeTicksIn)
        {
            tAlpha = EasingFunction::QuadraticIn((float)tUseTimer, (float)mFadeTicksIn);
        }
        else if(tUseTimer < mFadeTicksIn + mFadeTicksHold)
        {

        }
        else
        {
            int tMinusTimer = tUseTimer - (mFadeTicksIn + mFadeTicksHold);
            tAlpha = 1.0f - EasingFunction::QuadraticOut((float)tMinusTimer, (float)mFadeTicksIn);
        }

        //--Set.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tAlpha);

        //--Render text.
        if(rFont)
        {
            rFont->DrawText(mX, mY, mFlags, mScale, mDisplayText);
        }

        //--Render image.
        if(rDisplayImage)
        {
            rDisplayImage->Draw(mX, mY);
        }
    }

    //--Clean.
    if(mDisplayType == WD_CREDITS_TYPE_FADEIN)
    {
        StarlightColor::ClearMixer();
    }
}

//--[Property Queries]
bool WorldDialogue::IsCreditsMode()
{
    return mIsCreditsSequence;
}

//--[Manipulators]
void WorldDialogue::ActivateCreditsMode()
{
    mIsCreditsSequence = true;
    mCreditsTimerCur = 0;
    mCreditsPacks = new SugarLinkedList(true);
}
void WorldDialogue::DeactivateCreditsMode()
{
    mIsCreditsSequence = false;
    delete mCreditsPacks;
    mCreditsPacks = NULL;
}
void WorldDialogue::SetCreditsTimeMax(int pTicks)
{
    mCreditsTimerMax = pTicks;
}
void WorldDialogue::SetCreditsBackground(const char *pDLPath)
{
    rCreditsBackground = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}

//--[Core Methods]
void WorldDialogue::CreateCreditsPack()
{
    //--Creates and pushes a CreditsPack to the Activity Stack. Remember to pop it when done.
    //  Expect a lot of error messages if not in credits mode, ya dingus.
    if(!mIsCreditsSequence || !mCreditsPacks) return;

    //--Allocate and push.
    SetMemoryData(__FILE__, __LINE__);
    CreditsPack *nPack = (CreditsPack *)starmemoryalloc(sizeof(CreditsPack));
    nPack->Initialize();
    DataLibrary::Fetch()->PushActiveEntity(nPack);

    //--Register this to the credits list.
    mCreditsPacks->AddElementAsTail("X", nPack, &CreditsPack::DeleteThis);
}

//--[Update]
void WorldDialogue::UpdateCredits()
{
    //--Updates the credits packs, ends the credits sequence when the time runs out.
    DebugPush(true, "Beginning credits update.\n");
    if(!mIsCreditsSequence)
    {
        DebugPop("Done, credits not active.\n");
        return;
    }

    //--Player presses Escape, cancels credits.
    if(ControlManager::Fetch()->IsFirstPress("Escape"))
    {
        //--Flags.
        mVisibilityState = WD_INVISIBLE;
        DeactivateCreditsMode();
        Wipe();

        //--Audio.
        AudioManager::Fetch()->FadeMusic(60);

        //--Clear any dialogue locks.
        AdventureLevel *rFetchLevel = AdventureLevel::Fetch();
        if(rFetchLevel) rFetchLevel->RemoveLockout(WD_STANDARD_LOCKOUT);
        DebugPop("Done, credits time expired, exiting.\n");
        return;
    }

    //--Basic timer.
    DebugPrint("Running timer.\n");
    mCreditsTimerCur ++;
    if(mCreditsTimerCur >= mCreditsTimerMax)
    {
        //--Flags.
        mVisibilityState = WD_INVISIBLE;
        DeactivateCreditsMode();
        Wipe();

        //--Clear any dialogue locks.
        AdventureLevel *rFetchLevel = AdventureLevel::Fetch();
        if(rFetchLevel) rFetchLevel->RemoveLockout(WD_STANDARD_LOCKOUT);
        DebugPop("Done, credits time expired, exiting.\n");
        return;
    }

    //--Update credits packs.
    DebugPrint("Updating packs.\n");
    CreditsPack *rCreditsPack = (CreditsPack *)mCreditsPacks->SetToHeadAndReturn();
    while(rCreditsPack)
    {
        rCreditsPack->Update();
        if(rCreditsPack->IsComplete(mCreditsTimerCur))
        {
            DebugPrint("Pack expired, deleting - ");
            mCreditsPacks->RemoveRandomPointerEntry();
            DebugPrint("done.\n");
        }
        rCreditsPack = (CreditsPack *)mCreditsPacks->IncrementAndGetRandomPointerEntry();
    }

    //--Debug.
    DebugPop("Done, credits completed update normally.\n");
}

//--[Render]
void WorldDialogue::RenderCredits()
{
    //--Updates the credits packs, as well as the background and any other information.
    DebugPush(true, "Beginning credits render.\n");
    if(!mIsCreditsSequence)
    {
        DebugPop("Done, credits not active.\n");
        return;
    }

    //--Alpha.
    float tBackingAlpha = 1.0f;
    if(mCreditsTimerCur < 45)
    {
        tBackingAlpha = (float)(mCreditsTimerCur-15) / 30.0f;
    }
    else if(mCreditsTimerCur >= mCreditsTimerMax - 30)
    {
        tBackingAlpha = (float)(mCreditsTimerMax - mCreditsTimerCur) / 30.0f;
    }

    //--Clamp.
    if(tBackingAlpha > 1.0f) tBackingAlpha = 1.0f;
    if(tBackingAlpha < 0.0f) tBackingAlpha = 0.0f;

    //--Grey overlay.
    SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, tBackingAlpha * 0.75f));

    //--Background.
    if(rCreditsBackground)
    {
        bool tIsLowResMode = OptionsManager::Fetch()->GetOptionB("LowResAdventureMode");
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tBackingAlpha);
        if(!tIsLowResMode)
        {
            glScalef(0.5f, 0.5f, 1.0f);
            rCreditsBackground->Draw();
            glScalef(2.0f, 2.0f, 1.0f);
        }
        else
        {
            rCreditsBackground->DrawScaled(0, 0, LOWRES_SCALEINV * 0.50f, LOWRES_SCALEINV * 0.50f);
        }
    }

    //--Render credits packs.
    CreditsPack *rCreditsPack = (CreditsPack *)mCreditsPacks->PushIterator();
    while(rCreditsPack)
    {
        rCreditsPack->Render(mCreditsTimerCur);
        rCreditsPack = (CreditsPack *)mCreditsPacks->AutoIterate();
    }

    //--Debug.
    DebugPop("Done, credits finished rendering normally.\n");
}

//--[Lua]
//--Registration.
void WorldDialogue::HookToLuaStateCredits(lua_State *pLuaState)
{
    /* WD_SetCreditsProperty("Set Time to Start", iTicks)
       WD_SetCreditsProperty("Set Mode As Fades", iFadeInTicks, iHoldTicks, iFadeOutTicks)
       WD_SetCreditsProperty("Set Mode As Scroll", fPixelsPerTick)
       Sets the requested property in the active CreditsPack. */
    lua_register(pLuaState, "WD_SetCreditsProperty", &Hook_WD_SetCreditsProperty);
}

//--Functions.
int Hook_WD_SetCreditsProperty(lua_State *L)
{
    //WD_SetCreditsProperty("Set Time to Start", iTicks)
    //WD_SetCreditsProperty("Set Position", fX, fY)
    //WD_SetCreditsProperty("Set Text", sString, uiFlags, fScale, sFontPath)
    //WD_SetCreditsProperty("Set Mode As Fades", iFadeInTicks, iHoldTicks, iFadeOutTicks)
    //WD_SetCreditsProperty("Set Mode As Scroll", fPixelsPerTick)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("WD_SetCreditsProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Active object.
    CreditsPack *rCreditsPack = (CreditsPack *)DataLibrary::Fetch()->rActiveObject;
    if(!rCreditsPack) return LuaTypeError("WD_SetCreditsProperty");

    //--Sets how many ticks until this pack actually starts updating and rendering. This is passed
    //  as a positive, but is turned negative for internal reasons.
    if(!strcasecmp(rSwitchType, "Set Time to Start") && tArgs == 2)
    {
        rCreditsPack->mTimerOffset = lua_tointeger(L, 2) * -1;
    }
    //--Sets the position of the pack.
    else if(!strcasecmp(rSwitchType, "Set Position") && tArgs == 3)
    {
        rCreditsPack->mX = lua_tonumber(L, 2);
        rCreditsPack->mY = lua_tonumber(L, 3);
    }
    //--Sets the text that displays.
    else if(!strcasecmp(rSwitchType, "Set Text") && tArgs == 5)
    {
        strncpy(rCreditsPack->mDisplayText, lua_tostring(L, 2), 128);
        rCreditsPack->mFlags = lua_tointeger(L, 3);
        rCreditsPack->mScale = lua_tonumber(L, 4);
        rCreditsPack->rFont = DataLibrary::Fetch()->GetFont(lua_tostring(L, 5));
    }
    //--Causes the pack to fade in, hold, and fade out.
    else if(!strcasecmp(rSwitchType, "Set Mode As Fades") && tArgs == 4)
    {
        rCreditsPack->mDisplayType = WD_CREDITS_TYPE_FADEIN;
        rCreditsPack->mFadeTicksIn = lua_tointeger(L, 2);
        rCreditsPack->mFadeTicksHold = lua_tointeger(L, 3);
        rCreditsPack->mFadeTicksOut = lua_tointeger(L, 4);
    }
    //--Causes the pack to scroll in from the bottom of the screen. Y coordinate is ignored if provided.
    else if(!strcasecmp(rSwitchType, "Set Mode As Scroll") && tArgs == 2)
    {
        rCreditsPack->mDisplayType = WD_CREDITS_TYPE_SCROLLUP;
        rCreditsPack->mScrollPixPerTick = lua_tonumber(L, 2);
    }
    //--Error.
    else
    {
        LuaPropertyError("WD_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
