//--Base
#include "WorldDialogue.h"

//--Classes
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

//--[Debug]
//#define WD_SCENES_DEBUG
#ifdef WD_SCENES_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--[Manipulators]
void WorldDialogue::ActivateScenesMode()
{
    //--Activates the scene handler. Also clears everything to defaults.
    mIsSceneMode = true;
    mSceneOffsetX = 0.0f;
    mSceneOffsetY = 0.0f;
    rSceneImg = NULL;

    //--Clear animation if present.
    mSceneTimer = 0;
    mSceneTicksPerFrame = 1;
    mSceneAnimFramesTotal = 0;
    free(mrSceneAnimFrames);
    mIgnoreOldSceneOffsets = false;
    mrSceneAnimFrames = NULL;
    mSceneTransitionTimer = 0;
    rOldSceneImg = NULL;
    mOldSceneOffsetX = 0.0f;
    mOldSceneOffsetY = 0.0f;

    //--Timers.
    mMajorSequenceTimer = 0;

    //--Clear this flag. MajorSequence and CGs are exclusive.
    SetMajorSequence(false);
}
void WorldDialogue::DeactivateScenesMode()
{
    //--Clear flag.
    mIsSceneMode = false;

    //--Clear animation if present.
    mSceneAnimFramesTotal = 0;
    free(mrSceneAnimFrames);
    mrSceneAnimFrames = NULL;
    mIgnoreOldSceneOffsets = false;
    mSceneTransitionTimer = 0;
}
void WorldDialogue::SetIgnoreSceneOffsets(bool pFlag)
{
    mIgnoreSceneOffsets = pFlag;
}
void WorldDialogue::SetSceneOffsets(float pX, float pY)
{
    //--Modifies offsets of the scene image. Defualts are 0, 0. Offsets do *not* reset when the image changes.
    mIgnoreSceneOffsets = false;
    mSceneOffsetX = pX;
    mSceneOffsetY = pY;
}
void WorldDialogue::SetSceneFadeTimers(int pTicks)
{
    mSceneFadeTimeMax = pTicks;
    mSceneTimer = 0;
    if(mSceneFadeTimeMax < 1) mSceneFadeTimeMax = WD_SCENE_CROSSFADE_TICKS;
}
void WorldDialogue::ChangeSceneImage(const char *pImagePath)
{
    //--Store previous. Note that this can be NULL.
    mIgnoreOldSceneOffsets = mIgnoreSceneOffsets;
    mOldSceneOffsetX = mSceneOffsetX;
    mOldSceneOffsetY = mSceneOffsetY;
    mSceneTransitionTimer = 0;
    rOldSceneImg = rSceneImg;

    //--New.
    mIgnoreSceneOffsets = false;
    rSceneImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
}
void WorldDialogue::ChangeSceneImageInstantly(const char *pImagePath)
{
    //--Sets the scene image to fullbright instantly. Used to get crossfading to work correctly with scenes.
    mIgnoreOldSceneOffsets = mIgnoreSceneOffsets;
    mOldSceneOffsetX = mSceneOffsetX;
    mOldSceneOffsetY = mSceneOffsetY;
    mSceneTransitionTimer = mSceneFadeTimeMax;
    rOldSceneImg = rSceneImg;

    //--New.
    mIgnoreSceneOffsets = false;
    rSceneImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
}
void WorldDialogue::AllocateSceneAnimFrames(int pTotal, int pTicksPerFrame, int pStartingTick)
{
    //--Deallocate.
    mSceneAnimFramesTotal = 0;
    free(mrSceneAnimFrames);
    mrSceneAnimFrames = NULL;
    if(pTotal < 1) return;

    //--Set starting timer.
    mSceneTimer = pStartingTick;

    //--Flag.
    mSceneTicksPerFrame = pTicksPerFrame;
    if(mSceneTicksPerFrame < 1) mSceneTicksPerFrame = 1;

    //--Allocate.
    mSceneAnimFramesTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mrSceneAnimFrames = (SugarBitmap **)starmemoryalloc(sizeof(SugarBitmap *) * mSceneAnimFramesTotal);
    memset(mrSceneAnimFrames, 0, sizeof(SugarBitmap *) * mSceneAnimFramesTotal);
}
void WorldDialogue::SetSceneAnimFrame(int pSlot, const char *pImagePath)
{
    if(pSlot < 0 || pSlot >= mSceneAnimFramesTotal) return;
    mrSceneAnimFrames[pSlot] = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
}

//--[Rendering]
void WorldDialogue::RenderScenesMode()
{
    //--[Documentation]
    //--Scenes mode is different from the other modes in that it shows an entity in the middle of the screen,
    //  based on which one is currently showing. All the other rules of dialogue are the same.
    DebugPush(true, "WorldDialogue:Render Scenes - Begin.\n");
    bool tIsSmallPortraitMode = OptionsManager::Fetch()->GetOptionB("LowResAdventureMode");

    //--[Compute Alphas]
    float tAlphaFactor = EasingFunction::QuadraticInOut(mMajorSequenceTimer, WD_MAJOR_SEQUENCE_FADE_TICKS / 2);
    if(tAlphaFactor < 0.0f) tAlphaFactor = 0.0f;
    if(tAlphaFactor > 1.0f) tAlphaFactor = 1.0f;

    //--[Backing and Widescreen]
    //--Base borders.
    float cLft = 0.0f;
    float cTop = 0.0f;
    float cRgt = VIRTUAL_CANVAS_X;
    float cBot = VIRTUAL_CANVAS_Y;

    //--Render a black border under everything else. This makes portraits pop out of the screen better.
    glColor4f(0.1f, 0.1f, 0.1f, 0.65f * tAlphaFactor);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(cLft, cTop);
        glVertex2f(cRgt, cTop);
        glVertex2f(cRgt, cBot);
        glVertex2f(cLft, cBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor);

    //--Debug.
    DebugPrint("Finished computations.\n");

    //--[Entity]
    //--No crossfading.
    if(mSceneTransitionTimer >= mSceneFadeTimeMax)
    {
        //--Debug.
        DebugPrint("Begun no-crossfade mode.\n");

        //--If the image has been set, render it. Add the offsets. The image is expected to be centered.
        if(rSceneImg)
        {
            //--Image is larger than the canvas, so halve the scale.
            if(rSceneImg->GetTrueWidth() > VIRTUAL_CANVAS_X || rSceneImg->GetTrueHeight() > VIRTUAL_CANVAS_Y)
            {
                //--Normal sizing:
                if(!tIsSmallPortraitMode)
                {
                    glScalef(0.5f, 0.5f, 1.0f);
                    rSceneImg->Draw();
                    glScalef(2.0f, 2.0f, 1.0f);
                }
                //--Reduced scale:
                else
                {
                    rSceneImg->DrawScaled(0, 0, LOWRES_SCALEINV * 0.50f, LOWRES_SCALEINV * 0.50f);
                }
            }
            //--Exact match:
            else if(rSceneImg->GetTrueWidth() == VIRTUAL_CANVAS_X && rSceneImg->GetTrueHeight() == VIRTUAL_CANVAS_Y)
            {
                //--Normal sizing:
                if(!tIsSmallPortraitMode)
                {
                    rSceneImg->Draw();
                }
                //--Reduced scale:
                else
                {
                    rSceneImg->DrawScaled(0, 0, LOWRES_SCALEINV * 0.50f, LOWRES_SCALEINV * 0.50f);
                }
            }
            //--Normal case: Attempt to center the image.
            else
            {
                //--Small-portrait mode. 1/4s the size of the image.
                if(!tIsSmallPortraitMode)
                {
                    //--Centering info.
                    float tXPos = (VIRTUAL_CANVAS_X * 0.5f) - (rSceneImg->GetTrueWidth()  * 0.5f) + mSceneOffsetX;
                    float tYPos = (VIRTUAL_CANVAS_Y * 0.4f) - (rSceneImg->GetTrueHeight() * 0.5f) + mSceneOffsetY;

                    //--If ignoring the offsets, set them back to 0.
                    if(mIgnoreSceneOffsets)
                    {
                        tXPos = 0.0f;
                        tYPos = 0.0f;
                    }

                    //--Render.
                    rSceneImg->Draw(tXPos, tYPos);
                }
                //--In small mode, computations are different.
                else
                {
                    //--Centering info.
                    float tXPos = (VIRTUAL_CANVAS_X * 0.5f) - (rSceneImg->GetTrueWidth()  * 0.5f * LOWRES_SCALE) + mSceneOffsetX;
                    float tYPos = (VIRTUAL_CANVAS_Y * 0.4f) - (rSceneImg->GetTrueHeight() * 0.5f * LOWRES_SCALE) + mSceneOffsetY;

                    //--If ignoring the offsets, set them back to 0.
                    if(mIgnoreSceneOffsets)
                    {
                        tXPos = 0.0f;
                        tYPos = 0.0f;
                    }

                    //--Render.
                    rSceneImg->DrawScaled(tXPos, tYPos, LOWRES_SCALEINV, LOWRES_SCALEINV);
                }

            }
        }
    }
    //--Crossfading.
    else
    {
        //--Debug.
        DebugPrint("Begun crossfade mode %p\n", rOldSceneImg);

        //--Compute alphas.
        float cFadeOutAlpha = (1.0f - EasingFunction::QuadraticInOut(mSceneTransitionTimer, mSceneFadeTimeMax));
        float cFadeInAlpha = 1.6f - cFadeOutAlpha;

        //--Previous scene image. May not exist.
        if(rOldSceneImg)
        {
            //--Mixer.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cFadeOutAlpha);

            //--If the true size of the image is larger than the screen, this is a half-scale image. It also has no offset.
            if(rOldSceneImg->GetTrueWidth() > VIRTUAL_CANVAS_X || rOldSceneImg->GetTrueHeight() > VIRTUAL_CANVAS_Y)
            {
                if(!tIsSmallPortraitMode)
                {
                    glScalef(0.5f, 0.5f, 1.0f);
                    rOldSceneImg->Draw();
                    glScalef(2.0f, 2.0f, 1.0f);
                }
                else
                {
                    rOldSceneImg->DrawScaled(0, 0, LOWRES_SCALEINV * 0.50f, LOWRES_SCALEINV * 0.50f);
                }
            }
            //--Exact match:
            else if(rOldSceneImg->GetTrueWidth() == VIRTUAL_CANVAS_X && rOldSceneImg->GetTrueHeight() == VIRTUAL_CANVAS_Y)
            {
                //--Normal sizing:
                if(!tIsSmallPortraitMode)
                {
                    rOldSceneImg->Draw();
                }
                //--Reduced scale:
                else
                {
                    rOldSceneImg->DrawScaled(0, 0, LOWRES_SCALEINV * 0.50f, LOWRES_SCALEINV * 0.50f);
                }
            }
            //--Normal case: Attempt to center the image.
            else
            {
                //--Small-portrait mode. 1/4s the size of the image.
                if(!tIsSmallPortraitMode)
                {
                    //--Centering info.
                    float tXPos = (VIRTUAL_CANVAS_X * 0.5f) - (rOldSceneImg->GetTrueWidth()  * 0.5f) + mOldSceneOffsetX;
                    float tYPos = (VIRTUAL_CANVAS_Y * 0.4f) - (rOldSceneImg->GetTrueHeight() * 0.5f) + mOldSceneOffsetY;

                    //--If ignoring the offsets, set them back to 0.
                    if(mIgnoreSceneOffsets)
                    {
                        tXPos = 0.0f;
                        tYPos = 0.0f;
                    }

                    //--Render.
                    rOldSceneImg->Draw(tXPos, tYPos);
                }
                //--In small mode, computations are different.
                else
                {
                    //--Centering info.
                    float tXPos = (VIRTUAL_CANVAS_X * 0.5f) - (rOldSceneImg->GetTrueWidth()  * 0.5f * LOWRES_SCALE) + mOldSceneOffsetX;
                    float tYPos = (VIRTUAL_CANVAS_Y * 0.4f) - (rOldSceneImg->GetTrueHeight() * 0.5f * LOWRES_SCALE) + mOldSceneOffsetY;

                    //--If ignoring the offsets, set them back to 0.
                    if(mIgnoreSceneOffsets)
                    {
                        tXPos = 0.0f;
                        tYPos = 0.0f;
                    }

                    //--Render.
                    rOldSceneImg->DrawScaled(tXPos, tYPos, LOWRES_SCALEINV, LOWRES_SCALEINV);
                }
            }
        }

        //--Debug.
        DebugPrint("Rendering scene image %p\n", rSceneImg);

        //--If the image has been set, render it. Add the offsets. The image is expected to be centered.
        if(rSceneImg)
        {
            //--Mixer.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cFadeInAlpha);

            //--If the true size of the image is larger than the screen, this is a half-scale image. It also has no offset.
            if(rSceneImg->GetTrueWidth() > VIRTUAL_CANVAS_X || rSceneImg->GetTrueHeight() > VIRTUAL_CANVAS_Y)
            {
                //--Normal sizing:
                if(!tIsSmallPortraitMode)
                {
                    glScalef(0.5f, 0.5f, 1.0f);
                    rSceneImg->Draw(0, 0);
                    glScalef(2.0f, 2.0f, 1.0f);
                }
                //--Reduced scale:
                else
                {
                    rSceneImg->DrawScaled(0, 0, LOWRES_SCALEINV * 0.50f, LOWRES_SCALEINV * 0.50f);
                }
            }
            //--Exact match:
            else if(rSceneImg->GetTrueWidth() == VIRTUAL_CANVAS_X && rSceneImg->GetTrueHeight() == VIRTUAL_CANVAS_Y)
            {
                //--Normal sizing:
                if(!tIsSmallPortraitMode)
                {
                    rSceneImg->Draw(0, 0);
                }
                else
                {
                    rSceneImg->DrawScaled(0, 0, LOWRES_SCALEINV * 0.50f, LOWRES_SCALEINV * 0.50f);
                }
            }
            //--Normal case: Attempt to center the image.
            else
            {
                //--Normal mode.
                if(!tIsSmallPortraitMode)
                {
                    //--Centering info.
                    float tXPos = (VIRTUAL_CANVAS_X * 0.5f) - (rSceneImg->GetTrueWidth()  * 0.5f) + mSceneOffsetX;
                    float tYPos = (VIRTUAL_CANVAS_Y * 0.4f) - (rSceneImg->GetTrueHeight() * 0.5f) + mSceneOffsetY;

                    //--If ignoring the offsets, set them back to 0.
                    if(mIgnoreSceneOffsets)
                    {
                        tXPos = 0.0f;
                        tYPos = 0.0f;
                    }

                    //--Render.
                    rSceneImg->Draw(tXPos, tYPos);
                }
                //--In small mode, computations are different.
                else
                {
                    //--Centering info.
                    float tXPos = (VIRTUAL_CANVAS_X * 0.5f) - (rSceneImg->GetTrueWidth()  * 0.5f * LOWRES_SCALE) + mSceneOffsetX;
                    float tYPos = (VIRTUAL_CANVAS_Y * 0.4f) - (rSceneImg->GetTrueHeight() * 0.5f * LOWRES_SCALE) + mSceneOffsetY;

                    //--If ignoring the offsets, set them back to 0.
                    if(mIgnoreSceneOffsets)
                    {
                        tXPos = 0.0f;
                        tYPos = 0.0f;
                    }

                    //--Render.
                    rSceneImg->DrawScaled(tXPos, tYPos, LOWRES_SCALEINV, LOWRES_SCALEINV);
                }
            }
        }

        //--Clean.
        StarlightColor::ClearMixer();
    }

    //--Debug.
    DebugPrint("Render dialogue box.\n");

    //--Dialogue box.
    if(!mHideDialogueBoxes)
    {
        //--Positioning.
        float tCursorX = 42.0f;
        float tCursorY = 600.0f;

        //--Render.
        Images.Data.rNamelessBox->Draw();

        //--Reposition the rendering cursor for the dialogue.
        glTranslatef(tCursorX, tCursorY, 0.0f);

        //--Now start rendering.
        SugarLinkedList *rLetterList = (SugarLinkedList *)mDialogueList->PushIterator();
        while(rLetterList)
        {
            //--Offset value. Resets for each line.
            float tLineCursor = 0.0f;

            //--Iterate.
            DialogueCharacter *rCharacterPack = (DialogueCharacter *)rLetterList->PushIterator();
            while(rCharacterPack)
            {
                //--Spooky characters change color.
                if(rCharacterPack->mIsSpooky)
                {
                    if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 2)
                    {
                        StarlightColor::SetMixer(1.0f, 0.4f, 0.2f, rCharacterPack->mAlpha * tAlphaFactor);
                    }
                    else if((int)(tLineCursor + Global::Shared()->gTicksElapsed) % 6 < 4)
                    {
                        StarlightColor::SetMixer(1.0f, 1.0f, 0.1f, rCharacterPack->mAlpha * tAlphaFactor);
                    }
                    else
                    {
                        StarlightColor::SetMixer(1.0f, 0.2f, 0.5f, rCharacterPack->mAlpha * tAlphaFactor);
                    }
                }
                //--Coloring.
                else if(rCharacterPack->rColor)
                {
                    StarlightColor::SetMixer(rCharacterPack->rColor->r, rCharacterPack->rColor->g, rCharacterPack->rColor->b, rCharacterPack->mAlpha * tAlphaFactor);
                }
                else
                {
                    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, rCharacterPack->mAlpha * tAlphaFactor);
                }

                //--Next letter. It may be NULL. This also does the work of iterating.
                DialogueCharacter *rNextLetterPack = (DialogueCharacter *)rLetterList->AutoIterate();

                //--Render the letter.
                char tNextLetter = '\0';
                if(rNextLetterPack) tNextLetter = rNextLetterPack->mLetter;
                float tAdvance = Images.Data.rDialogueFont->DrawLetter(tLineCursor, 0.0f, 0, mTextScale, rCharacterPack->mLetter, tNextLetter);

                //--Move the cursor over.
                tLineCursor = tLineCursor + (tAdvance * mTextScale);
                rCharacterPack = rNextLetterPack;
            }

            //--Move the cursor down.
            tCursorY = tCursorY + (Images.Data.rDialogueFont->GetTextHeight() * mTextScale);
            glTranslatef(0.0f, (Images.Data.rDialogueFont->GetTextHeight() * mTextScale), 0.0f);

            //--Next line.
            rLetterList = (SugarLinkedList *)mDialogueList->AutoIterate();
        }

        //--Clean up.
        glColor3f(1.0f, 1.0f, 1.0f);
        glTranslatef(tCursorX * -1, tCursorY * -1, 0.0f);
    }

    //--[Flash]
    DebugPrint("Begun flash.\n");
    RenderFlash();

    //--[Animation]
    //--If there's an animation, render that over the entity. It's still centered and uses the same offset.
    if(mSceneAnimFramesTotal > 0 && mSceneTimer >= 0)
    {
        //--Debug.
        DebugPrint("Begun animations.\n");

        //--Compute the frame.
        int tFrame = mSceneTimer / mSceneTicksPerFrame;
        if(tFrame >= mSceneAnimFramesTotal) tFrame = mSceneAnimFramesTotal - 1;

        //--Get the image. It can be NULL, in which case don't render it.
        SugarBitmap *rRenderImage = mrSceneAnimFrames[tFrame];
        if(rRenderImage)
        {
            //--Centering info.
            float tXPos = (VIRTUAL_CANVAS_X * 0.5f) - (rRenderImage->GetWidth() * 0.5f)  + mSceneOffsetX;
            float tYPos = (VIRTUAL_CANVAS_Y * 0.4f) - (rRenderImage->GetHeight() * 0.5f) + mSceneOffsetY;

            //--Render.
            rRenderImage->Draw(tXPos, tYPos);
        }
    }

    //--Debug.
    DebugPop("WorldDialogue:Render Scenes - Ended normall.\n");
}
