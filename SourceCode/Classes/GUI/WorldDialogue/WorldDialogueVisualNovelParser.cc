//--Base
#include "WorldDialogue.h"

//--Classes
#include "DialogueActor.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--Worker Functions
int ResolveNextWord(const char *pString, char *pOutBuffer, int pStartLetter, char pDelimiter);
char *ResolveLastWord(SugarLinkedList *pDialogueCharacterList);

int WorldDialogue::HandleSpecialSequencesVN(const char *pString)
{
    //--An add-on to HandleSpecialSequences(), this does the same thing but only runs for Visual Novel cases, which have
    //  extra-different sequences.
    if(!pString) return 0;
    int tLength = (int)strlen(pString);

    //--Resolve the list to append to.
    //SugarLinkedList *rDialogueList = ResolveActiveDialogueList();

    //--[MOVECHAR|Slot|Location|Flag] Moves the character in the given slot to the given location using the flag specifying the movement type.
    //  The slot can also be a character name, in which case the slot auto-resolves.
    if(tLength >= 10 && !strncasecmp(pString, "[MOVECHAR|", 10))
    {
        //--[Base Properties]
        //--Variables.
        int tCursor = 10;
        int tCharacterSlot = -1;
        int tLocation = WD_VN_MOVEDEST_INVALID;
        int tMoveFlag = WD_VN_MOVETPYE_NONE;

        //--Split into strings.
        char tCharacterNameBuf[STD_MAX_LETTERS];
        char tLocationBuf[STD_MAX_LETTERS];
        char tFlagBuf[STD_MAX_LETTERS];

        //--Actor's Name/Slot
        int tParsed = ResolveNextWord(pString, tCharacterNameBuf, tCursor, '|');
        tCursor = tCursor + tParsed + 1; //Skip the '|' as well as the parsed letters.

        //--Destination.
        tParsed = ResolveNextWord(pString, tLocationBuf, tCursor, '|');
        tCursor = tCursor + tParsed + 1;

        //--Move type flag.
        tParsed = ResolveNextWord(pString, tFlagBuf, tCursor, ']');
        tCursor = tCursor + tParsed; //Don't skip the ']' for return-length reasons.

        //--[Resolving Character Slot]
        //--If this buffer's first letter is a number, assume this is a slot.
        if(tCharacterNameBuf[0] >= '0' && tCharacterNameBuf[0] <= '9')
        {
            tCharacterSlot = atoi(tCharacterNameBuf);
        }
        //--Alpha. Assume it's a character's name.
        else
        {
            tCharacterSlot = GetSlotOfActor(tCharacterNameBuf);
        }

        //--[Resolving Move Flag]
        //--If this buffer's first letter is a number, assume this is a numeric flag.
        if(tFlagBuf[0] >= '0' && tFlagBuf[0] <= '9')
        {
            tMoveFlag = atoi(tFlagBuf);
        }
        //--Alpha. Assume it's a flag's name, in which case resolve it.
        else
        {
            //--Teleport instantly.
            if(!strcasecmp(tFlagBuf, "Teleport"))
            {
                tMoveFlag = WD_VN_MOVETPYE_TELEPORT;
            }
            //--Move at a constant speed.
            else if(!strcasecmp(tFlagBuf, "Linear"))
            {
                tMoveFlag = WD_VN_MOVETPYE_TELEPORT;
            }
            //--Quadratic-In.
            else if(!strcasecmp(tFlagBuf, "QuadIn"))
            {
                tMoveFlag = WD_VN_MOVETPYE_QUADIN;
            }
            //--Quadratic-Out.
            else if(!strcasecmp(tFlagBuf, "QuadOut"))
            {
                tMoveFlag = WD_VN_MOVETPYE_QUADOUT;
            }
            //--Quadratic-In-Out.
            else if(!strcasecmp(tFlagBuf, "QuadInOut"))
            {
                tMoveFlag = WD_VN_MOVETPYE_QUADINOUT;
            }
            //--Error.
            else
            {
                fprintf(stderr, "Warning: Unable to resolve movement flag from %s, unknown type.\n", tFlagBuf);
                return tCursor;
            }
        }

        //--Resolve values and range check.
        tLocation = ComputeAdvancedFormula(tLocationBuf);
        if(tCharacterSlot < 0 || tCharacterSlot >= STD_MAX_LETTERS)
        {
            fprintf(stderr, "Warning: Unable to resolve slot %i from %s, out of range.\n", tCharacterSlot, tLocationBuf);
            return tCursor;
        }

        //--Now set the required function.
        VN.mActorStarts[tCharacterSlot] = VN.mActorCurrents[tCharacterSlot];
        VN.mActorTargets[tCharacterSlot] = (float)tLocation;
        VN.mActorTimers[tCharacterSlot] = 0;
        VN.mActorTimersMax[tCharacterSlot] = WD_VN_MOVE_DEFAULT_TICKS;
        VN.mActorMoveFlags[tCharacterSlot] = tMoveFlag;
        free(VN.mActorPostExecs[tCharacterSlot]);
        VN.mActorPostExecs[tCharacterSlot] = NULL;

        //--Return a negative indicating parsing needs to stop.
        return tCursor;
    }
    //--[MOVECHARTIME|Slot|Location|Flag|Ticks] Same as above, but expects the number of ticks passed in.
    else if(tLength >= 18 && !strncasecmp(pString, "[MOVECHARTIME|", 14))
    {
        //--[Base Properties]
        //--Variables.
        int tCursor = 14;
        int tCharacterSlot = -1;
        int tLocation = WD_VN_MOVEDEST_INVALID;
        int tMoveFlag = WD_VN_MOVETPYE_NONE;
        int tTicks = WD_VN_MOVE_DEFAULT_TICKS;

        //--Split into strings.
        char tCharacterNameBuf[STD_MAX_LETTERS];
        char tLocationBuf[STD_MAX_LETTERS];
        char tFlagBuf[STD_MAX_LETTERS];
        char tTicksBuf[STD_MAX_LETTERS];

        //--Actor's Name/Slot
        int tParsed = ResolveNextWord(pString, tCharacterNameBuf, tCursor, '|');
        tCursor = tCursor + tParsed + 1; //Skip the '|' as well as the parsed letters.

        //--Destination.
        tParsed = ResolveNextWord(pString, tLocationBuf, tCursor, '|');
        tCursor = tCursor + tParsed + 1;

        //--Move type flag.
        tParsed = ResolveNextWord(pString, tFlagBuf, tCursor, '|');
        tCursor = tCursor + tParsed + 1;

        //--Number of ticks.
        tParsed = ResolveNextWord(pString, tTicksBuf, tCursor, ']');
        tCursor = tCursor + tParsed; //Don't skip the ']' for return-length reasons.

        //--[Resolving Character Slot]
        //--If this buffer's first letter is a number, assume this is a slot.
        if(tCharacterNameBuf[0] >= '0' && tCharacterNameBuf[0] <= '9')
        {
            tCharacterSlot = atoi(tCharacterNameBuf);
        }
        //--Alpha. Assume it's a character's name.
        else
        {
            tCharacterSlot = GetSlotOfActor(tCharacterNameBuf);
        }

        //--[Resolving Move Flag]
        //--If this buffer's first letter is a number, assume this is a numeric flag.
        if(tFlagBuf[0] >= '0' && tFlagBuf[0] <= '9')
        {
            tMoveFlag = atoi(tFlagBuf);
        }
        //--Alpha. Assume it's a flag's name, in which case resolve it.
        else
        {
            //--Teleport instantly.
            if(!strcasecmp(tFlagBuf, "Teleport"))
            {
                tMoveFlag = WD_VN_MOVETPYE_TELEPORT;
            }
            //--Move at a constant speed.
            else if(!strcasecmp(tFlagBuf, "Linear"))
            {
                tMoveFlag = WD_VN_MOVETPYE_TELEPORT;
            }
            //--Quadratic-In.
            else if(!strcasecmp(tFlagBuf, "QuadIn"))
            {
                tMoveFlag = WD_VN_MOVETPYE_QUADIN;
            }
            //--Quadratic-Out.
            else if(!strcasecmp(tFlagBuf, "QuadOut"))
            {
                tMoveFlag = WD_VN_MOVETPYE_QUADOUT;
            }
            //--Quadratic-In-Out.
            else if(!strcasecmp(tFlagBuf, "QuadInOut"))
            {
                tMoveFlag = WD_VN_MOVETPYE_QUADINOUT;
            }
            //--Error.
            else
            {
                fprintf(stderr, "Warning: Unable to resolve movement flag from %s, unknown type.\n", tFlagBuf);
                return tCursor;
            }
        }

        //--Resolve values and range check.
        tLocation = ComputeAdvancedFormula(tLocationBuf);
        tTicks = atoi(tTicksBuf);
        if(tCharacterSlot < 0 || tCharacterSlot >= STD_MAX_LETTERS)
        {
            fprintf(stderr, "Warning: Unable to resolve slot %i from %s, out of range.\n", tCharacterSlot, tLocationBuf);
            return tCursor;
        }

        //--Tick range check.
        if(tTicks < 1) tTicks = 1;

        //--Now set the required function.
        VN.mActorStarts[tCharacterSlot] = VN.mActorCurrents[tCharacterSlot];
        VN.mActorTargets[tCharacterSlot] = (float)tLocation;
        VN.mActorTimers[tCharacterSlot] = 0;
        VN.mActorTimersMax[tCharacterSlot] = tTicks;
        VN.mActorMoveFlags[tCharacterSlot] = tMoveFlag;
        free(VN.mActorPostExecs[tCharacterSlot]);
        VN.mActorPostExecs[tCharacterSlot] = NULL;

        //--Return a negative indicating parsing needs to stop.
        return tCursor;
    }
    //--[ADDCHARPOS|Actor|Slot|Emotion|Formula] is the same as ADDCHAR, except the character will teleport to the location resolved
    //  by the given formula.
    else if(tLength >= 15 && !strncasecmp(pString, "[ADDCHARPOS|", 12))
    {
        //--Buffers.
        int tCursor = 12;
        char tCharacterNameBuf[STD_MAX_LETTERS];
        char tSlotNumberBuf[STD_MAX_LETTERS];
        char tEmotionNameBuf[STD_MAX_LETTERS];
        char tFormulaBuf[STD_MAX_LETTERS];

        //--Actor's Name
        int tParsed = ResolveNextWord(pString, tCharacterNameBuf, tCursor, '|');
        tCursor = tCursor + tParsed + 1; //Skip the '|' as well as the parsed letters.

        //--Slot Number
        tParsed = ResolveNextWord(pString, tSlotNumberBuf, tCursor, '|');
        tCursor = tCursor + tParsed + 1;

        //--Emotion Name
        tParsed = ResolveNextWord(pString, tEmotionNameBuf, tCursor, '|');
        tCursor = tCursor + tParsed + 1;

        //--Location Formula
        tParsed = ResolveNextWord(pString, tFormulaBuf, tCursor, ']');
        tCursor = tCursor + tParsed; //Don't skip the ']' for return-length reasons.

        //--Change slot to an integer.
        int tDestinationSlot = atoi(tSlotNumberBuf);
        //fprintf(stderr, "Adding character by position %s\n", tFormulaBuf);

        //--Check if the character is already on the field.
        int tPreviousSlot = -1;
        for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
        {
            if(!mDialogueActors[i]) continue;
            if(!strcasecmp(mDialogueActors[i]->GetName(), tCharacterNameBuf))
            {
                tPreviousSlot = i;
                break;
            }
        }

        //--If the character is already on the field, move them to the given slot.
        int tUsedDestination = tDestinationSlot;
        if(tPreviousSlot != -1 && tDestinationSlot != -1)
        {
            MoveActorToSlotI(tPreviousSlot, tDestinationSlot);
        }
        //--Put the character on the field.
        else if(tPreviousSlot == -1)
        {
            //--If the destination slot is -1, set the slot based on the first empty one.
            if(tDestinationSlot < 0 || tDestinationSlot >= WD_DIALOGUE_ACTOR_SLOTS)
            {
                for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
                {
                    if(mDialogueActors[i]) continue;
                    tUsedDestination = i;
                    AddActorToSlot(tCharacterNameBuf, i);
                    SetActorEmotionS(tCharacterNameBuf, tEmotionNameBuf);
                    break;
                }
            }
            //--No need for special code, toss them right in.
            else
            {
                AddActorToSlot(tCharacterNameBuf, tDestinationSlot);
                SetActorEmotionS(tCharacterNameBuf, tEmotionNameBuf);
            }

            //--Re-run the speaker code. This makes it easier if a character appears during a line they are speaking.
            SetSpeaker(mLastSetSpeaker);
        }

        //--Specify a location by formula, and move the character there.
        if(tUsedDestination >= 0 && tUsedDestination < WD_DIALOGUE_ACTOR_SLOTS)
        {
            VN.mActorMoveFlags[tUsedDestination] = WD_VN_MOVETPYE_NONE;
            VN.mActorCurrents[tUsedDestination] = ComputeAdvancedFormula(tFormulaBuf);
        }

        //--Pass back the letters parsed.
        return tCursor;
    }

    //--No special cases.
    return 0;
}
int WorldDialogue::ComputeAdvancedFormula(const char *pString)
{
    //--An 'advanced formula' is a string that incorporates variables into it. Only very specific variables can be used
    //  since this is just advanced scripting. Presently, these are the names of actors, a few constants, and numeric constants.
    //--There are no delimiters present. Components of an advanced formula are always split by an operation: +, - only, no
    //  more advanced stuff.
    //--Order of operations are not supported, nor are parenthesis.
    //--Returns the final value of the computation, as an integer.
    if(!pString) return 0;
    //fprintf(stderr, "Running advanced formula. %s\n", pString);

    //--Buffers.
    int tValueSoFar = 0;
    int tLenSoFar = 0;
    int tCurrentOperand = 0; //0 is add, 1 is subtract.
    char tInputBuffer[STD_MAX_LETTERS];

    //--If the first letter is an operand, fail. Those aren't allowed.
    if(pString[0] == '+' || pString[0] == '-') return 0;

    //--Begin iterating.
    int tLen = (int)strlen(pString);
    for(int i = 0; i < tLen; i ++)
    {
        //--Flag.
        bool tRunComputation = false;

        //--Check if this is a normal letter/number.
        if(pString[i] != '+' && pString[i] != '-')
        {
            //--If it's a space, skip over it and don't add it to the string.
            if(pString[i] == ' ')
            {

            }
            //--Add it.
            else
            {
                tInputBuffer[tLenSoFar+0] = pString[i];
                tInputBuffer[tLenSoFar+1] = '\0';
            }

            tLenSoFar ++;
        }
        //--It's an operator.
        else
        {
            tRunComputation = true;
        }

        //--If this is the last letter in the string, run the computation as well.
        if(i == tLen - 1) tRunComputation = true;

        //--Computation code:
        if(tRunComputation)
        {
            //--Flags.
            int tModValue = 0;
            //fprintf(stderr, " Operating on formula. %s\n", tInputBuffer);

            //--If the first letter is a numeric, this is a constant.
            if(tInputBuffer[0] >= '0' && tInputBuffer[0] <= '9')
            {
                tModValue = atoi(tInputBuffer);
                //fprintf(stderr, "  Numeric constant: %i\n", tModValue);
            }
            //--Otherwise, it's a alpha, which can be a named constant or a character's name.
            else
            {
                //--Check if it's a named constant. 'Body' is approximately the width of a character's
                //  body under normal circumstances.
                if(!strcasecmp(tInputBuffer, "Body"))
                {
                    tModValue = 210;
                }
                //--HfBody is half of a body width. Used for centering.
                else if(!strcasecmp(tInputBuffer, "HfBody"))
                {
                    tModValue = 105;
                }
                //--Middle is the X-middle of the screen.
                else if(!strcasecmp(tInputBuffer, "Middle"))
                {
                    tModValue = (int)(VIRTUAL_CANVAS_X * 0.50f);
                }
                //--LEdge is the left of the screen.
                else if(!strcasecmp(tInputBuffer, "LEdge"))
                {
                    tModValue = 0;
                }
                //--REdge is the right of the screen.
                else if(!strcasecmp(tInputBuffer, "REdge"))
                {
                    tModValue = VIRTUAL_CANVAS_X;
                }
                //--Otherwise, it's a character's name. Find the character and get their location.
                else
                {
                    //--Check which slot they're in.
                    int tCharacterSlot = GetSlotOfActor(tInputBuffer);
                    if(tCharacterSlot < 0 || tCharacterSlot >= WD_DIALOGUE_ACTOR_SLOTS)
                    {
                        fprintf(stderr, "Error, advanced formula cannot resolve %s.\n", tInputBuffer);
                        tModValue = 0;
                    }
                    else
                    {
                        tModValue = (int)VN.mActorCurrents[tCharacterSlot];
                    }
                }
            }

            //--Addition:
            if(tCurrentOperand == 0)
            {
                tValueSoFar = tValueSoFar + tModValue;
            }
            //--Subtraction:
            else
            {
                tValueSoFar = tValueSoFar - tModValue;
            }

            //--Switch operands.
            if(pString[i] == '+')
            {
                tCurrentOperand = 0;
            }
            else
            {
                tCurrentOperand = 1;
            }

            //--Reset everything.
            tLenSoFar = 0;
            tInputBuffer[0] = '\0';
        }
    }

    //--Once all is done, pass back the computed value.
    //fprintf(stderr, " After computations, value is %i\n", tValueSoFar);
    return tValueSoFar;
}
