//--Base
#include "WorldDialogue.h"

//--Classes
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--[Local Definitions]
#define HIGHLIGHT_SCALE 1.1f
#define HIGHLIGHT_SCALE_INV (1.0f / HIGHLIGHT_SCALE)

//--[Property Queries]
bool WorldDialogue::IsDecisionMode()
{
    return mIsDecisionMode;
}

//--[Manipulators]
void WorldDialogue::SetDecisionMode(bool pFlag)
{
    //--If freshly activating decision mode, clear the decisions implicitly. This also occurs when
    //  deactivating it, for similar reasons.
    if(mIsDecisionMode != pFlag) ClearDecisions();
    mIsDecisionMode = pFlag;
    mTicksSinceDecisionOpen = 0;
}
void WorldDialogue::AddDecision(const char *pTitle, const char *pFiringScript, const char *pFiringString)
{
    //--Creates a new decision pack and registers it. No string is allowed to be NULL.
    if(!pTitle || !pFiringScript || !pFiringString) return;

    //--Create and initialize.
    SetMemoryData(__FILE__, __LINE__);
    DecisionPack *nPack = (DecisionPack *)starmemoryalloc(sizeof(DecisionPack));
    nPack->Initialize();
    ResetString(nPack->mText, pTitle);
    ResetString(nPack->mContinuationScript, pFiringScript);
    ResetString(nPack->mFiringString, pFiringString);

    //--Register.
    mDecisionList->AddElementAsTail("X", nPack, &DecisionPack::DeleteThis);

    //--Whenever the decision count changes, the positions need to be refreshed.
    RefreshDecisionPositions();
}

//--[Core Methods]
void WorldDialogue::ClearDecisions()
{
    mDecisionCursor = 0;
    mHighlightedDecision = 0;
    mDecisionList->ClearList();
}
void WorldDialogue::RefreshDecisionPositions()
{
    //--Re-resolve the positions of the decisions on screen for the purposes of clicking on them.
    if(!Images.mIsReady) return;

    //--Deprecated. Might be used again at some point.
    return;

    //--Exactly one decision: Center it.
    if(mDecisionList->GetListSize() == 1)
    {
        //--Get the decision.
        DecisionPack *rPack = (DecisionPack *)mDecisionList->GetElementBySlot(0);

        //--Decision is centered.
        float tXPos = VIRTUAL_CANVAS_X * 0.5f;
        float tYPos = VIRTUAL_CANVAS_Y * 0.5f;

        //--Compute top-left edge.
        float tWidth  = Images.Data.rDialogueFont->GetTextWidth(rPack->mText) * mDecisionScale;
        float tHeight = Images.Data.rDialogueFont->GetTextHeight() * mDecisionScale;
        tXPos = tXPos + (tWidth * -0.5f);
        tYPos = tYPos + (tHeight * -0.5f);

        //--Set.
        mDecisionDimensions[0].SetWH(tXPos, tYPos, tWidth, tHeight);
    }
    //--If there are two decisions, they space differently.
    else if(mDecisionList->GetListSize() == 2)
    {
        //--Get the decisions.
        DecisionPack *rPackA = (DecisionPack *)mDecisionList->GetElementBySlot(0);
        DecisionPack *rPackB = (DecisionPack *)mDecisionList->GetElementBySlot(1);

        //--0th Element.
        float tXPos = VIRTUAL_CANVAS_X * 0.5f;
        float tYPos = VIRTUAL_CANVAS_Y * 0.45f;
        float tWidth  = Images.Data.rDialogueFont->GetTextWidth(rPackA->mText) * mDecisionScale;
        float tHeight = Images.Data.rDialogueFont->GetTextHeight() * mDecisionScale;
        tXPos = tXPos + (tWidth * -0.5f);
        tYPos = tYPos + (tHeight * -0.5f);
        mDecisionDimensions[0].SetWH(tXPos, tYPos, tWidth, tHeight);

        //--1th Element.
        tXPos = VIRTUAL_CANVAS_X * 0.5f;
        tYPos = VIRTUAL_CANVAS_Y * 0.55f;
        tWidth  = Images.Data.rDialogueFont->GetTextWidth(rPackB->mText) * mDecisionScale;
        tHeight = Images.Data.rDialogueFont->GetTextHeight() * mDecisionScale;
        tXPos = tXPos + (tWidth * -0.5f);
        tYPos = tYPos + (tHeight * -0.5f);
        mDecisionDimensions[1].SetWH(tXPos, tYPos, tWidth, tHeight);
    }
    //--Three part algorithm.
    else
    {
        //--Iterate.
        for(int i = 0; i < WD_DECISIONS_PER_PAGE; i ++)
        {
            //--Get the pack.
            DecisionPack *rPack = (DecisionPack *)mDecisionList->GetElementBySlot(mDecisionCursor + i);
            if(!rPack) continue;

            //--Position.
            float tXPos = VIRTUAL_CANVAS_X * 0.5f;
            float tYPos = VIRTUAL_CANVAS_Y * (0.40f + (0.10f * (float)i));
            float tWidth  = Images.Data.rDialogueFont->GetTextWidth(rPack->mText) * mDecisionScale;
            float tHeight = Images.Data.rDialogueFont->GetTextHeight() * mDecisionScale;
            tXPos = tXPos + (tWidth * -0.5f);
            tYPos = tYPos + (tHeight * -0.5f);

            //--Set.
            mDecisionDimensions[i].SetWH(tXPos, tYPos, tWidth, tHeight);
        }
    }
}

//--[Update]
void WorldDialogue::UpdateDecisions()
{
    //--First, if there are no decisions when an update cycle occurs, cancel decision mode.
    if(mDecisionList->GetListSize() < 1)
    {
        SetDecisionMode(false);
        return;
    }

    //--Timer.
    mTicksSinceDecisionOpen ++;

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Up, scrolls negatively.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Move the highlighted decision up by 1. It cannot go below 0.
        mHighlightedDecision --;
        if(mHighlightedDecision < 0) mHighlightedDecision = 0;
    }
    //--Down, scrolls positively.
    else if(rControlManager->IsFirstPress("Down"))
    {
        //--Move the highlighted decision up by 1.
        mHighlightedDecision ++;
        if(mHighlightedDecision >= mDecisionList->GetListSize()) mHighlightedDecision --;
    }
    //--Activate will execute the given decision.
    else if(rControlManager->IsFirstPress("Activate") && mTicksSinceDecisionOpen > 5)
    {
        //--Get the decision in question.
        DecisionPack *rPack = (DecisionPack *)mDecisionList->GetElementBySlot(mHighlightedDecision);
        if(!rPack) return;

        //--Store this decision's information. The script might delete it.
        char *rStoredScript = rPack->mContinuationScript;
        char *rStoredString = rPack->mFiringString;
        rPack->mContinuationScript = NULL;
        rPack->mFiringString = NULL;

        //--Deactivate decision mode. The script may re-activate it.
        SetDecisionMode(false);

        //--Run the script in question. It should be assumed that the DecisionPack is now unstable.
        LuaManager::Fetch()->ExecuteLuaFile(rStoredScript, 1, "S", rStoredString);

        //--Clean.
        free(rStoredScript);
        free(rStoredString);
    }
}

//--[Drawing]
void WorldDialogue::RenderDecisions()
{
    //--[Documentation]
    //--Renders the decisions available to the player in the bottom left near the dialogue pane.
    if(!Images.mIsReady) return;

    //--Compute alphas.
    float tAlphaFactor = EasingFunction::QuadraticInOut(mMajorSequenceTimer, WD_MAJOR_SEQUENCE_FADE_TICKS / 2);
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);
    if(tAlphaFactor < 0.0f) tAlphaFactor = 0.0f;
    if(tAlphaFactor > 1.0f) tAlphaFactor = 1.0f;

    //--Constants.
    float cTextSize  = 1.0f;
    float cTextHei   = Images.Data.rDialogueFont->GetTextHeight() * cTextSize;
    float cCursorWid = 8.0f;

    //--Iterate across the decisions and figure out how wide the pane should be.
    float tWidestDecision = 0.0f;
    DecisionPack *rPack = (DecisionPack *)mDecisionList->PushIterator();
    while(rPack)
    {
        //--Get and compare length.
        float tLength = (Images.Data.rDialogueFont->GetTextWidth(rPack->mText) * cTextSize);
        if(tLength > tWidestDecision) tWidestDecision = tLength;

        //--Next.
        rPack = (DecisionPack *)mDecisionList->AutoIterate();
    }

    //--Increase the width to accomodate the cursor.
    float cXIndent = 24.0f;
    float cXExdent = 30.0f;
    float cYIndent =  8.0f;
    float cYExdent = 17.0f;
    tWidestDecision = tWidestDecision + cCursorWid + cXIndent + cXExdent;

    //--Compute the lft/top position of the backing. Top position needs to move up as more decisions exist.
    float cTextBoxTop = 540.0f;
    float cLftPos =  17.0f;
    float cTopPos = cTextBoxTop - (mDecisionList->GetListSize() * cTextHei) - cYIndent - cYExdent;

    //--Figure out the sizing.
    TwoDimensionReal tDimensions;
    tDimensions.Set(cLftPos, cTopPos, cLftPos + (tWidestDecision), cTextBoxTop);

    //--Texture sizings.
    float cTxW = 0.333333f;
    float cTxH = 0.333333f;
    float cWid = 30.0f;
    float cHei = 30.0f;

    //--Border card.
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor);
    Images.Data.rBorderCard->Bind();
    glBegin(GL_QUADS);

        //--[Top Vertically]
        //--Top left.
        glTexCoord2f(cTxW * 0.0f, cTxH * 0.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 0.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 0.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mTop + (cHei * 1.0f));

        //--Top bar.
        glTexCoord2f(cTxW * 1.0f, cTxH * 0.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 0.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));

        //--Top right.
        glTexCoord2f(cTxW * 2.0f, cTxH * 0.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 0.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));

        //--[Middle Vertically]
        //--Middle left.
        glTexCoord2f(cTxW * 0.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 0.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mBot - (cHei * 1.0f));

        //--Middle bar.
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));

        //--Middle right.
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 1.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));

        //--[Bottom Vertically]
        //--Bottom left.
        glTexCoord2f(cTxW * 0.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 3.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 0.0f));
        glTexCoord2f(cTxW * 0.0f, cTxH * 3.0f); glVertex2f(tDimensions.mLft + (cWid * 0.0f), tDimensions.mBot - (cHei * 0.0f));

        //--Bottom bar.
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 3.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 0.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 3.0f); glVertex2f(tDimensions.mLft + (cWid * 1.0f), tDimensions.mBot - (cHei * 0.0f));

        //--Bottom right.
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 2.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 3.0f); glVertex2f(tDimensions.mRgt - (cWid * 0.0f), tDimensions.mBot - (cHei * 0.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 3.0f); glVertex2f(tDimensions.mRgt - (cWid * 1.0f), tDimensions.mBot - (cHei * 0.0f));

    glEnd();

    //--[Decision Rendering]
    //--Render the text options.
    int i = 0;
    rPack = (DecisionPack *)mDecisionList->PushIterator();
    while(rPack)
    {
        //--Render base text.
        Images.Data.rDialogueFont->DrawText(tDimensions.mLft + cCursorWid + cXIndent, tDimensions.mTop + (cTextHei * i) + cYIndent, 0, cTextSize, rPack->mText);

        //--Next.
        i ++;
        rPack = (DecisionPack *)mDecisionList->AutoIterate();
    }

    //--Render the cursor.
    cCursorWid = 5.0f;
    float cCursorHei = (cTextHei * 0.80f) - 2.0f;
    float tTopicCursorX = tDimensions.mLft + cXIndent;
    float tTopicCursorY = tDimensions.mTop + (cTextHei * mHighlightedDecision) + 7.0f + cYIndent;

    //--Render.
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_TRIANGLES);
        glVertex2f(tTopicCursorX,              tTopicCursorY);
        glVertex2f(tTopicCursorX + cCursorWid, tTopicCursorY + cCursorHei / 2.0f);
        glVertex2f(tTopicCursorX,              tTopicCursorY + cCursorHei);
    glEnd();
    glEnable(GL_TEXTURE_2D);
}
void WorldDialogue::SubrenderDecision(DecisionPack *pPack, float pXCenter, float pYCenter, bool pIsHighlighted)
{
    //--Subroutine, renders a decision package at the provided location. If highlighted, it becomes slightly larger
    //  to indicate the player is mousing over it.
    if(!pPack) return;

    //--Reposition.
    glTranslatef(pXCenter, pYCenter, 0.0f);

    //--If this decision is highlighted, slightly increase the scale.
    if(pIsHighlighted) glScalef(HIGHLIGHT_SCALE, HIGHLIGHT_SCALE, 1.0f);

    //--Centering.
    //float tWidth  = Images.Data.rDialogueFont->GetTextWidth(pPack->mText) * 0.5f;
    //float tHeight = Images.Data.rDialogueFont->GetTextHeight()            * 0.5f;
    Images.Data.rDialogueFont->DrawText(0, 0, 0, mDecisionScale, pPack->mText);

    //--Clean.
    if(pIsHighlighted) glScalef(HIGHLIGHT_SCALE_INV, HIGHLIGHT_SCALE_INV, 1.0f);
    glTranslatef(-pXCenter, -pYCenter, 0.0f);
}
void WorldDialogue::RenderDialogueBorderCard(SugarBitmap *pBorderCard, float pLft, float pTop, float pRgt, float pBot)
{
    //--Error check.
    if(!pBorderCard) return;

    //--Texture sizings.
    float cTxW = 0.333333f;
    float cTxH = 0.333333f;
    float cWid = 30.0f;
    float cHei = 30.0f;

    //--Border card.
    pBorderCard->Bind();
    glBegin(GL_QUADS);

        //--[Top Vertically]
        //--Top left.
        glTexCoord2f(cTxW * 0.0f, cTxH * 0.0f); glVertex2f(pLft + (cWid * 0.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 0.0f); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 0.0f, cTxH * 1.0f); glVertex2f(pLft + (cWid * 0.0f), pTop + (cHei * 1.0f));

        //--Top bar.
        glTexCoord2f(cTxW * 1.0f, cTxH * 0.0f); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 0.0f); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 1.0f));

        //--Top right.
        glTexCoord2f(cTxW * 2.0f, cTxH * 0.0f); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 0.0f); glVertex2f(pRgt - (cWid * 0.0f), pTop + (cHei * 0.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 1.0f); glVertex2f(pRgt - (cWid * 0.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 1.0f));

        //--[Middle Vertically]
        //--Middle left.
        glTexCoord2f(cTxW * 0.0f, cTxH * 1.0f); glVertex2f(pLft + (cWid * 0.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 0.0f, cTxH * 2.0f); glVertex2f(pLft + (cWid * 0.0f), pBot - (cHei * 1.0f));

        //--Middle bar.
        glTexCoord2f(cTxW * 1.0f, cTxH * 1.0f); glVertex2f(pLft + (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 1.0f));

        //--Middle right.
        glTexCoord2f(cTxW * 2.0f, cTxH * 1.0f); glVertex2f(pRgt - (cWid * 1.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 1.0f); glVertex2f(pRgt - (cWid * 0.0f), pTop + (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 2.0f); glVertex2f(pRgt - (cWid * 0.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 1.0f));

        //--[Bottom Vertically]
        //--Bottom left.
        glTexCoord2f(cTxW * 0.0f, cTxH * 2.0f); glVertex2f(pLft + (cWid * 0.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 3.0f); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 0.0f));
        glTexCoord2f(cTxW * 0.0f, cTxH * 3.0f); glVertex2f(pLft + (cWid * 0.0f), pBot - (cHei * 0.0f));

        //--Bottom bar.
        glTexCoord2f(cTxW * 1.0f, cTxH * 2.0f); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 3.0f); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 0.0f));
        glTexCoord2f(cTxW * 1.0f, cTxH * 3.0f); glVertex2f(pLft + (cWid * 1.0f), pBot - (cHei * 0.0f));

        //--Bottom right.
        glTexCoord2f(cTxW * 2.0f, cTxH * 2.0f); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 2.0f); glVertex2f(pRgt - (cWid * 0.0f), pBot - (cHei * 1.0f));
        glTexCoord2f(cTxW * 3.0f, cTxH * 3.0f); glVertex2f(pRgt - (cWid * 0.0f), pBot - (cHei * 0.0f));
        glTexCoord2f(cTxW * 2.0f, cTxH * 3.0f); glVertex2f(pRgt - (cWid * 1.0f), pBot - (cHei * 0.0f));

    glEnd();
}
