//--Base
#include "PairanormalDialogue.h"

//--Classes
#include "TiledLevel.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "SugarLumpManager.h"

void PairanormalDialogue::ParseSLFFile(const char *pPath)
{
    //--[Documentation and Setup]
    //--Given a path to a file, parses that file as a .slf file exported from Tiled. The file's entity data is used (tile data is ignored)
    //  to build the examinable positions used by the engine to click on things and display them.
    if(!pPath) return;

    //--Order the SugarLumpManager to open the file.
    SugarLumpManager *rSLM = SugarLumpManager::Fetch();
    rSLM->Open(pPath, STANDARD_HEADER);
    if(!rSLM->IsFileOpen()) return;

    //--Create a TiledLevel to parse the data for us.
    TiledLevel *tTiledLevel = new TiledLevel();
    tTiledLevel->ParseFile(rSLM);

    //--The data is now parsed out, we only care about the object data.
    SugarLinkedList *rObjectData = tTiledLevel->GetObjectData();

    //--[Data Parsing]
    //--For each object, get its properties and creates objects as needed for Pairanormal.
    ObjectInfoPack *rObjectPack = (ObjectInfoPack *)rObjectData->PushIterator();
    while(rObjectPack)
    {
        //--Subroutine does the heavy lifting.
        HandleObject(rObjectPack);

        //--Next.
        rObjectPack = (ObjectInfoPack *)rObjectData->AutoIterate();
    }

    //--[Clean]
    //--All objects have been handled. Clean up memory.
    delete tTiledLevel; //Destabilizes rObjectData!
    rSLM->Close();
}
void PairanormalDialogue::HandleObject(ObjectInfoPack *pObject)
{
    //--[Documentation and Setup]
    //--Given an ObjectInfoPack *, parses out its properties and creates objects for the level. This is a worker function
    //  designed to simplify the ParseSLFFile's big loop.
    if(!pObject) return;

    //--Fast-access pointers.
    const char *rType = pObject->mType;

    //--Examinable. A position the player can click on to fire the examination script.
    if(!strcasecmp(rType, "Examinable"))
    {
        //--Dimensions
        TwoDimensionReal tDimensions;
        tDimensions.SetWH(pObject->mX, pObject->mY, pObject->mW, pObject->mH);

        //--The name is always unique, but the script call can be duplicated. The '|' character indicates where to stop parsing
        //  for the examinable's call argument.
        char tBuffer[128];
        int tLen = (int)strlen(pObject->mName);
        for(int i = 0; i < tLen; i ++)
        {
            //--Stop on bars.
            if(pObject->mName[i] == '|')
            {
                break;
            }

            //--Otherwise, copy the letter.
            tBuffer[i+0] = pObject->mName[i];
            tBuffer[i+1] = '\0';
        }

        //--Create the investigation object.
        AddInvestigationObject(pObject->mName, tDimensions, PairanormalDialogue::xInvestigationStdImg, PairanormalDialogue::xInvestigationStdPath, tBuffer, pObject->mX, pObject->mY, pObject->mX+pObject->mW, pObject->mY+pObject->mH);

        //--Property Check:
        for(int i = 0; i < pObject->mProperties->mPropertiesTotal; i ++)
        {
            //--Sample property:
            //if(!strcasecmp(pObject->mProperties[i][0], "Sample"))
            //{
            //}
        }
    }
    //--Renderable, specifies vis coordinates.
    else if(!strcasecmp(rType, "Renderable"))
    {
        AddRenderableObject(pObject->mName, pObject->mX, pObject->mY, pObject->mX+pObject->mW, pObject->mY+pObject->mH);
    }
    //--Unhandled object type, print an error.
    else
    {
        fprintf(stderr, "Pairanormal Dialogue SLF Parser: Warning, unhandled object type %s.\n", rType);
    }
}
