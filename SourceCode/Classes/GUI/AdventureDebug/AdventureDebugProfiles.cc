//--Base
#include "AdventureDebug.h"

//--Classes
//--CoreClasses
#include "SugarFont.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

//====================================== System Constants =========================================
#define ADM_STATPROFILE_GRAPHMODE_BACK 0
#define ADM_STATPROFILE_GRAPHMODE_SETSPLITGRAPHS 1
#define ADM_STATPROFILE_GRAPHMODE_SETSINGLEGRAPH 2
#define ADM_STATPROFILE_GRAPHMODE_TOTAL 3

#define ADM_GRAPHMODE_SPLIT 0
#define ADM_GRAPHMODE_MERGED 1

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void AdventureDebug::AllocateProfiles(int pTotal)
{
    //--Deallocate.
    mStatProfilesTotal = 0;
    free(mStatProfiles);
    mStatProfiles = NULL;
    if(pTotal < 1) return;

    //--Allocate.
    mStatProfilesTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mStatProfiles = (CharStatProfile *)starmemoryalloc(sizeof(CharStatProfile) * mStatProfilesTotal);
    memset(mStatProfiles, 0, sizeof(CharStatProfile) * mStatProfilesTotal);
}
void AdventureDebug::CreateProfile(int pSlot, const char *pName, const char *pScript)
{
    if(pSlot < 0 || pSlot >= mStatProfilesTotal) return;
    strncpy(mStatProfiles[pSlot].mName, pName, ADM_CHAR_MAX_LETTERS);
    strncpy(mStatProfiles[pSlot].mScript, pScript, STD_PATH_LEN);
}
void AdventureDebug::SetProfileData(int pSlot, int pLevel, int pMaxHP, int pAttack, int pAccuracy, int pEvade, int pInitiative)
{
    if(pSlot  < 0 || pSlot  >= mStatProfilesTotal) return;
    if(pLevel < 0 || pLevel >= ADM_CHAR_MAX_LEVELS) return;
    mStatProfiles[pSlot].mMaxHP[pLevel] = pMaxHP;
    mStatProfiles[pSlot].mAttack[pLevel] = pAttack;
    mStatProfiles[pSlot].mAccuracy[pLevel] = pAccuracy;
    mStatProfiles[pSlot].mEvade[pLevel] = pEvade;
    mStatProfiles[pSlot].mInitiative[pLevel] = pInitiative;
}

//============================================ Update =============================================
void AdventureDebug::UpdateStatProfiles()
{
    //--When selecting which profile to examine:
    if(mActiveProfile == -1)
    {
        UpdateStatProfilesSelector();
    }
    //--Graph is active.
    else
    {
        UpdateStatProfilesGraphs();
    }
}
void AdventureDebug::UpdateStatProfilesSelector()
{
    //--[Documentation and Setup]
    //--Allow the user to select a profile for examination.
    ControlManager *rControlManager = ControlManager::Fetch();
    int tOldCursor = mMenuCursor;

    //--[Arrow Keys]
    //--Up!
    if(rControlManager->IsFirstPress("Up"))
    {
        mMenuCursor --;
        if(mMenuCursor < 0) mMenuCursor = 0;
    }

    //--Down!
    if(rControlManager->IsFirstPress("Down"))
    {
        mMenuCursor ++;
        if(mMenuCursor >= mMenuMax) mMenuCursor = mMenuMax - 1;
        if(mMenuCursor < 0) mMenuCursor = 0;
    }

    //--If the cursor changed, handle SFX.
    if(tOldCursor != mMenuCursor)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--[Activate]
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--On the zeroth element, cancel out.
        if(mMenuCursor == 0)
        {
            SetToMain();
            mMenuCursor = ADM_OPT_MAIN_OPEN_PROFILES;
        }
        //--Otherwise, activate the given profile.
        else
        {
            //--Range check.
            int tSlot = mMenuCursor - 1;
            if(tSlot < 0 || tSlot >= mStatProfilesTotal) return;

            //--Call the script.
            LuaManager::Fetch()->ExecuteLuaFile(mStatProfiles[tSlot].mScript, 1, "N", (float)tSlot);

            //--Switch modes.
            mMenuCursor = 0;
            mActiveProfile = tSlot;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--[Cancel]
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_PROFILES;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}
void AdventureDebug::UpdateStatProfilesGraphs()
{
    //--[Documentation and Setup]
    //--Allows the player to see the graph of stats.
    ControlManager *rControlManager = ControlManager::Fetch();
    int tOldCursor = mMenuCursor;

    //--[Arrow Keys]
    //--Up!
    if(rControlManager->IsFirstPress("Up"))
    {
        mMenuCursor --;
        if(mMenuCursor < 0) mMenuCursor = 0;
    }
    //--Down!
    if(rControlManager->IsFirstPress("Down"))
    {
        mMenuCursor ++;
        if(mMenuCursor >= ADM_STATPROFILE_GRAPHMODE_TOTAL) mMenuCursor = ADM_STATPROFILE_GRAPHMODE_TOTAL - 1;
        if(mMenuCursor < 0) mMenuCursor = 0;
    }

    //--If the cursor changed, handle SFX.
    if(tOldCursor != mMenuCursor)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--[Activate]
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Previous mode:
        if(mMenuCursor == ADM_STATPROFILE_GRAPHMODE_BACK)
        {
            mActiveProfile = -1;
        }
        //--Switch to split graphs mode.
        else if(mMenuCursor == ADM_STATPROFILE_GRAPHMODE_SETSPLITGRAPHS)
        {
            mGraphMode = ADM_GRAPHMODE_SPLIT;
        }
        //--Single graph.
        else if(mMenuCursor == ADM_STATPROFILE_GRAPHMODE_SETSINGLEGRAPH)
        {
            mGraphMode = ADM_GRAPHMODE_MERGED;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--[Cancel]
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mActiveProfile = -1;
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
}

//=========================================== Drawing =============================================
void AdventureDebug::RenderStatProfiles()
{
    //--Selecting.
    if(mActiveProfile == -1)
    {
        RenderStatProfilesSelector();
    }
    //--Graph is active.
    else
    {
        RenderStatProfilesGraphs();
    }
}
void AdventureDebug::RenderStatProfilesSelector()
{
    //--[Documentation and Setup]
    //--Get the font size.
    float cScale = 1.0f;
    float tFontSize = Images.Data.rMenuFont->GetTextHeight() * cScale;

    //--Header.
    Images.Data.rMenuFont->DrawText(8.0f, 8.0f + (tFontSize * 0.0f), 0, cScale, "Select a Profile");

    //--Zeroth entry is "Cancel".
    RenderText(0, 12.0f, 8.0f + (tFontSize * 1.0f), cScale, "Cancel");

    //--Iterate across the profiles.
    for(int i = 0; i < mStatProfilesTotal; i ++)
    {
        RenderText(i+1, 12.0f, 8.0f + (tFontSize * (2.0f + i)), cScale, mStatProfiles[i].mName);
    }
}
void AdventureDebug::RenderStatProfilesGraphs()
{
    //--[Documentation and Setup]
    //--Get the font size.
    float cScale = 1.0f;
    float tFontSize = Images.Data.rMenuFont->GetTextHeight() * cScale;

    //--Header.
    Images.Data.rMenuFont->DrawText(8.0f, 8.0f + (tFontSize * 0.0f), 0, cScale, "Displaying Graphs");

    //--Zeroth entry is "Cancel".
    RenderText(0, 12.0f, 8.0f + (tFontSize * 1.0f), cScale, "Return");

    //--Graph modifiers.
    RenderText(1, 12.0f, 8.0f + (tFontSize * 2.0f), cScale, "Split Graphs");
    RenderText(2, 12.0f, 8.0f + (tFontSize * 3.0f), cScale, "Merge Graphs");

    //--[Split Graphs]
    //--Each stat gets its own little graph.
    if(mGraphMode == ADM_GRAPHMODE_SPLIT)
    {
        //--Positions.
        float cHPGraphX  = 180.0f;
        float cHPGraphY  = 80.0f;

        float cAtkGraphX = 775.0f;
        float cAtkGraphY = 80.0f;

        float cAccGraphX = 180.0f;
        float cAccGraphY = 310.0f;

        float cEvdGraphX = 775.0f;
        float cEvdGraphY = 310.0f;

        float cIniGraphX = 180.0f;
        float cIniGraphY = 530.0f;

        //--Health Graph
        Images.Data.rMenuFont->DrawText(cHPGraphX, cHPGraphY, 0, 1.0f, "Health");
        RenderStatGraph(cHPGraphX, cHPGraphY+tFontSize, mStatProfiles[mActiveProfile].mMaxHP, 1000, StarlightColor::MapRGBAF(1.0f, 0.0f, 0.0f, 1.0f), mStatProfileGroups[ADM_PROFILE_GROUP_HPMAX]);

        //--Attack Graph
        Images.Data.rMenuFont->DrawText(cAtkGraphX, cAtkGraphY, 0, 1.0f, "Attack");
        RenderStatGraph(cAtkGraphX, cAtkGraphY+tFontSize, mStatProfiles[mActiveProfile].mAttack, 300, StarlightColor::MapRGBAF(1.0f, 0.5f, 0.0f, 1.0f), mStatProfileGroups[ADM_PROFILE_GROUP_ATTACK]);

        //--Accuracy Graph
        Images.Data.rMenuFont->DrawText(cAccGraphX, cAccGraphY, 0, 1.0f, "Accuracy");
        RenderStatGraph(cAccGraphX, cAccGraphY+tFontSize, mStatProfiles[mActiveProfile].mAccuracy, 200, StarlightColor::MapRGBAF(1.0f, 1.0f, 0.0f, 1.0f), mStatProfileGroups[ADM_PROFILE_GROUP_ACCURACY]);

        //--Evade Graph
        Images.Data.rMenuFont->DrawText(cEvdGraphX, cEvdGraphY, 0, 1.0f, "Evade");
        RenderStatGraph(cEvdGraphX, cEvdGraphY+tFontSize, mStatProfiles[mActiveProfile].mEvade, 200, StarlightColor::MapRGBAF(0.0f, 0.0f, 1.0f, 1.0f), mStatProfileGroups[ADM_PROFILE_GROUP_EVADE]);

        //--Evade Graph
        Images.Data.rMenuFont->DrawText(cIniGraphX, cIniGraphY, 0, 1.0f, "Initiative");
        RenderStatGraph(cIniGraphX, cIniGraphY+tFontSize, mStatProfiles[mActiveProfile].mInitiative, 100, StarlightColor::MapRGBAF(1.0f, 0.0f, 1.0f, 1.0f), mStatProfileGroups[ADM_PROFILE_GROUP_INITIATIVE]);
    }
    //--[Merged Graphs]
    //--Graphs all merged onto one big graph.
    else if(mGraphMode == ADM_GRAPHMODE_MERGED)
    {

    }
}
void AdventureDebug::RenderStatGraph(float pX, float pY, int *pStatArray, int pRange, StarlightColor pColor, StatProfileGroup *pGroupRanges)
{
    //--Renders a graph showing the given stat. The graph starts at the given position but has a fixed
    //  size otherwise.
    if(!pStatArray || !pGroupRanges || pRange < 1) return;

    //--Setup.
    float cWid = 500.0f;
    float cHei = 200.0f;

    //--Resolve width per level.
    float cWidPerLev = cWid / (float)ADM_CHAR_MAX_LEVELS;

    //--Position.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
    glDisable(GL_TEXTURE_2D);
    glTranslatef(pX, pY, 0.0f);

    //--Rendering:
    float cLin = 2.0f;
    glBegin(GL_QUADS);

        //--Top edge.
        glVertex2f(0.0f, 0.0f);
        glVertex2f(cWid, 0.0f);
        glVertex2f(cWid, cLin);
        glVertex2f(0.0f, cLin);

        //--Bottom edge.
        glVertex2f(0.0f, cHei);
        glVertex2f(cWid, cHei);
        glVertex2f(cWid, cHei-cLin);
        glVertex2f(0.0f, cHei-cLin);

        //--Left edge.
        glVertex2f(0.0f, 0.0f);
        glVertex2f(cLin, 0.0f);
        glVertex2f(cLin, cHei);
        glVertex2f(0.0f, cHei);

        //--Right edge.
        glVertex2f(cWid,      0.0f);
        glVertex2f(cWid-cLin, 0.0f);
        glVertex2f(cWid-cLin, cHei);
        glVertex2f(cWid,      cHei);

        //--Render values for each level.
        pColor.SetAsMixer();
        for(int i = 0; i < ADM_CHAR_MAX_LEVELS; i ++)
        {
            float cLft = i * cWidPerLev;
            float cRgt = cLft + cWidPerLev;
            float cBot = cHei;
            float cTop = cHei - (pStatArray[i] / (float)pRange * cHei);
            glVertex2f(cLft, cTop);
            glVertex2f(cRgt, cTop);
            glVertex2f(cRgt, cBot);
            glVertex2f(cLft, cBot);
        }
    glEnd();


    //--Clean.
    glTranslatef(-pX, -pY, 0.0f);
    glEnable(GL_TEXTURE_2D);
    StarlightColor::ClearMixer();

    //--Render text.
    float tFontSize = Images.Data.rMenuFont->GetTextHeight() * 1.0f;
    Images.Data.rMenuFont->DrawTextArgs(pX + cWid, pY,                0, 1.0f, "+%i", pRange);
    Images.Data.rMenuFont->DrawTextArgs(pX + cWid, pY+cHei-tFontSize, 0, 1.0f, "+%i", 0);

    //--Determine which group the highest falls into.
    const char *rLetters = NULL;
    int cHighest = pStatArray[ADM_CHAR_MAX_LEVELS-1];
    for(int i = 0; i < ADM_PROFILE_RANGE_TOTAL; i ++)
    {
        if(cHighest >= pGroupRanges[i].mLowerBound && cHighest <= pGroupRanges[i].mUpperBound)
        {
            rLetters = pGroupRanges[i].mString;
            break;
        }
    }
    if(!rLetters) rLetters = pGroupRanges[ADM_PROFILE_RANGE_TOTAL-1].mString;

    //--Render the highest value in the color of the chart.
    pColor.SetAsMixer();
    float cHighestPos = (cHighest/ (float)pRange * cHei);
    Images.Data.rMenuFont->DrawTextArgs(pX + cWid, pY+cHei-cHighestPos-(tFontSize*0.5f), 0, 1.0f, "->%i %s", cHighest, rLetters);
    StarlightColor::ClearMixer();
}
