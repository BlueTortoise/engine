//--Base
#include "StringEntry.h"

//--Classes
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"

//--[Forward Declarations]
void ConditionalAppend(char *sString, bool pConditional, const char *pFalseString, const char *pTrueString);

#define REFIRE_DEFAULT 15
#define REFIRE_MINIMUM 5

//=========================================== System ==============================================
StringEntry::StringEntry()
{
    //--[String Entry]
    //--System
    mIsComplete = false;
    mIsCancelled = false;
    mPromptString = NULL;

    //--String
    mTimer = 1;
    memset(mEmptyString, 0, sizeof(char) * SE_MAXLEN);
    memset(mCurrentString, 0, sizeof(char) * SE_MAXLEN);

    //--Inputs
    mHighlightedButton = -1;
    mIsShifted = false;
    mIsCapsLocked = false;
    memset(&mPressTimers, 0, sizeof(int) * SE_BTN_TOTAL);
    memset(&mButtons, 0, sizeof(TwoDimensionReal) * SE_BTN_TOTAL);
    memset(&mBtnStrings, 0, sizeof(char) * SE_BTN_TOTAL * 16);
    mRefireTimer = REFIRE_DEFAULT;

    //--Images
    memset(&Images, 0, sizeof(Images));

    //--Public Variables
    mIgnoreMouse = false;

    //--[Construction]
    //--Load Images.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    Images.Data.rHeaderFont    = rDataLibrary->GetFont("String Entry Header");
    Images.Data.rMainlineFont  = rDataLibrary->GetFont("String Entry Main");
    Images.Data.rRenderBase    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/String/Base");
    Images.Data.rRenderPressed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/String/Pressed");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--Rendering position constants.
    float cRenderX = 278.0f;
    float cRenderY =  61.0f;

    //--1234567890
    float cBtnSqr = 43.0f;
    float cBtnSpc = cBtnSqr + 5.0f;
    float cXPos = 108.0f + cRenderX;
    float tYPos = 287.0f;
    for(int i = 0; i < 10; i ++)
    {
        mButtons[SE_BTN_1 + i].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[SE_BTN_1 + i], "%i", (i+1) % 10);
    }

    //--QWERTYUIOP
    cXPos = 132.0f + cRenderX;
    tYPos = 335.0f;
    int cOrder[] = {SE_BTN_Q, SE_BTN_W, SE_BTN_E, SE_BTN_R, SE_BTN_T, SE_BTN_Y, SE_BTN_U, SE_BTN_I, SE_BTN_O, SE_BTN_P};
    for(int i = 0; i < 10; i ++)
    {
        mButtons[cOrder[i]].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[cOrder[i]], "%c", cOrder[i] + 'a');
    }

    //--ASDFGHJKL
    cXPos = 156.0f + cRenderX;
    tYPos = 383.0f;
    int cOrder2[] = {SE_BTN_A, SE_BTN_S, SE_BTN_D, SE_BTN_F, SE_BTN_G, SE_BTN_H, SE_BTN_J, SE_BTN_K, SE_BTN_L};
    for(int i = 0; i < 9; i ++)
    {
        mButtons[cOrder2[i]].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[cOrder2[i]], "%c", cOrder2[i] + 'a');
    }

    //--ZXCVBNM
    cXPos = 180.0f + cRenderX;
    tYPos = 431.0f;
    int cOrder3[] = {SE_BTN_Z, SE_BTN_X, SE_BTN_C, SE_BTN_V, SE_BTN_B, SE_BTN_N, SE_BTN_M};
    for(int i = 0; i < 7; i ++)
    {
        mButtons[cOrder3[i]].SetWH(cXPos + (cBtnSpc * i), tYPos, cBtnSqr, cBtnSqr);
        sprintf(mBtnStrings[cOrder3[i]], "%c", cOrder3[i] + 'a');
    }

    //--Other keys.
    mButtons[SE_BTN_CAPSLOCK].SetWH(14.0f + cRenderX, 383.0f, 132.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_CAPSLOCK], "Caps");

    mButtons[SE_BTN_SHIFT].SetWH(38.0f + cRenderX, 431.0f, 132.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_SHIFT], "Shift");

    mButtons[SE_BTN_SPACE].SetWH(230.0f + cRenderX, 479.0f, 253.0f, 43.0f);
    strcpy(mBtnStrings[SE_BTN_SPACE], "Space");

    mButtons[SE_BTN_BACKSPACE].SetWH(593.0f + cRenderX, 287.0f, 132.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_BACKSPACE], "Undo");

    //--Smaller special keys.
    mButtons[SE_BTN_COMMA].SetWH(516.0f + cRenderX, 370.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_COMMA], ",");

    mButtons[SE_BTN_PERIOD].SetWH(564.0f + cRenderX, 370.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_PERIOD], ".");

    mButtons[SE_BTN_SLASH].SetWH(612.0f + cRenderX, 370.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_SLASH], "/");

    mButtons[SE_BTN_MINUS].SetWH(516.0f + cRenderX, 178.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_MINUS], "-");

    mButtons[SE_BTN_EQUALS].SetWH(564.0f + cRenderX, 178.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_EQUALS], "=");

    mButtons[SE_BTN_BACKSLASH].SetWH(612.0f + cRenderX, 178.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_BACKSLASH], "\\");

    mButtons[SE_BTN_GRAVE].SetWH(60.0f + cRenderX, 226.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_GRAVE], "`");

    mButtons[SE_BTN_SEMICOLON].SetWH(588.0f + cRenderX, 322.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_SEMICOLON], ";");

    mButtons[SE_BTN_QUOTE].SetWH(636.0f + cRenderX, 322.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_QUOTE], "'");

    mButtons[SE_BTN_LBRACE].SetWH(612.0f + cRenderX, 274.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_LBRACE], "[");

    mButtons[SE_BTN_RBRACE].SetWH(660.0f + cRenderX, 274.0f + cRenderY, cBtnSqr, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_RBRACE], "]");

    //--Finishing button.
    mButtons[SE_BTN_DONE].SetWH(659.0f + cRenderX, 556.0f, 132.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_DONE], "Done");

    //--Cancel button.
    mButtons[SE_BTN_CANCEL].SetWH(659.0f + cRenderX, 604.0f, 132.0f, cBtnSqr);
    strcpy(mBtnStrings[SE_BTN_CANCEL], "Cancel");
}
StringEntry::~StringEntry()
{
    free(mPromptString);
}

//====================================== Property Queries =========================================
bool StringEntry::IsComplete()
{
    return mIsComplete;
}
bool StringEntry::IsCancelled()
{
    return mIsCancelled;
}
const char *StringEntry::GetString()
{
    return (const char *)mCurrentString;
}

//========================================= Manipulators ==========================================
void StringEntry::SetCompleteFlag(bool pFlag)
{
    mIsComplete = pFlag;
    mIsCancelled = false;
}
void StringEntry::SetPromptString(const char *pString)
{
    ResetString(mPromptString, pString);
}
void StringEntry::SetString(const char *pString)
{
    if(!pString)
    {
        memset(mCurrentString, 0, sizeof(char) * SE_MAXLEN);
    }
    else
    {
        strcpy(mCurrentString, pString);
    }
}
void StringEntry::SetEmptyString(const char *pString)
{
    if(!pString)
    {
        memset(mEmptyString, 0, sizeof(char) * SE_MAXLEN);
    }
    else
    {
        strcpy(mEmptyString, pString);
    }
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void StringEntry::Update()
{
    //--[Documentation and Setup]
    //--Updates the StringEntry object. This scans for keypresses and mouse clicks on buttons.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get mouse positions.
    float tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoordsF(tMouseX, tMouseY, tMouseZ);

    //--Timer. Always runs.
    mTimer ++;

    //--[Enter]
    //--Same as clicking Done.
    if(rControlManager->IsFirstPress("Enter"))
    {
        mIsComplete = true;
        return;
    }

    //--[Escape]
    //--Same as clicking Cancel.
    if(rControlManager->IsFirstPress("Escape"))
    {
        mIsCancelled = true;
        return;
    }

    //--[Button Scanner]
    //--Check which button the mouse is over.
    mHighlightedButton = -1;
    for(int i = 0; i < SE_BTN_TOTAL; i ++)
    {
        if(IsPointWithin2DReal(tMouseX, tMouseY, mButtons[i]))
        {
            mHighlightedButton = i;
            break;
        }
    }

    //--[Mouse Click]
    //--When the user clicks the mouse, what happens depends on the highlighted button.
    int cAddTicks = 15;
    if(rControlManager->IsFirstPress("MouseLft") && !mIgnoreMouse)
    {
        //--Finished button.
        if(mHighlightedButton == SE_BTN_DONE)
        {
            mIsComplete = true;
        }
        //--Cancel button.
        else if(mHighlightedButton == SE_BTN_CANCEL)
        {
            mIsCancelled = true;
        }
        //--Backspace.
        else if(mHighlightedButton == SE_BTN_BACKSPACE)
        {
            mPressTimers[SE_BTN_BACKSPACE] = mTimer + cAddTicks;
            int tLen = (int)strlen(mCurrentString);
            mCurrentString[tLen-1] = '\0';
        }
        //--Space.
        else if(mHighlightedButton == SE_BTN_SPACE)
        {
            mPressTimers[SE_BTN_SPACE] = mTimer + cAddTicks;
            strcat(mCurrentString, " ");
        }
        //--Shift.
        else if(mHighlightedButton == SE_BTN_SHIFT)
        {
            mIsShifted = !mIsShifted;
        }
        //--Caps Lock.
        else if(mHighlightedButton == SE_BTN_CAPSLOCK)
        {
            mIsCapsLocked = !mIsCapsLocked;
        }
        //--All other keys.
        else if(mHighlightedButton != -1)
        {
            //--Setup.
            char tBuf[2];
            tBuf[0] = mBtnStrings[mHighlightedButton][0];
            tBuf[1] = '\0';

            //--Mark timer.
            mPressTimers[mHighlightedButton] = mTimer + cAddTicks;

            //--If the key is shifted, move it to uppercase.
            if(mIsShifted)
            {
                //--Letters.
                if(tBuf[0] >= 'a' && tBuf[0] <= 'z')
                {
                    tBuf[0] = tBuf[0] + 'A' - 'a';
                }
                //--Numerics.
                else if(tBuf[0] >= '0' && tBuf[0] <= '9')
                {
                    if(tBuf[0] == '1') tBuf[0] = '!';
                    if(tBuf[0] == '2') tBuf[0] = '@';
                    if(tBuf[0] == '3') tBuf[0] = '#';
                    if(tBuf[0] == '4') tBuf[0] = '$';
                    if(tBuf[0] == '5') tBuf[0] = '%';
                    if(tBuf[0] == '6') tBuf[0] = '^';
                    if(tBuf[0] == '7') tBuf[0] = '&';
                    if(tBuf[0] == '8') tBuf[0] = '*';
                    if(tBuf[0] == '9') tBuf[0] = '(';
                    if(tBuf[0] == '0') tBuf[0] = ')';
                }
            }

            //--If Caps-Locked, switch case.
            if(mIsCapsLocked)
            {
                //--Letters lowercase.
                if(tBuf[0] >= 'a' && tBuf[0] <= 'z')
                {
                    tBuf[0] = tBuf[0] + 'A' - 'a';
                }
                //--Letters uppercase.
                else if(tBuf[0] >= 'A' && tBuf[0] <= 'Z')
                {
                    tBuf[0] = tBuf[0] + 'a' - 'A';
                }
            }

            //--Append.
            strcat(mCurrentString, tBuf);

            //--Cancel shift unless the key was down.
            if(!rControlManager->IsDown("Shift")) mIsShifted = false;
        }
    }

    //--[Keypresses]
    int tKeyboard, tMouse, tJoy;
    rControlManager->GetKeyPressCodes(tKeyboard, tMouse, tJoy);
    const char *rPressedKeyName = rControlManager->GetNameOfKeyIndex(tKeyboard);
    if(rPressedKeyName)
    {
        //--To be valid, the key must have one letter in its name, and it must be standard ASCII.
        if(strlen(rPressedKeyName) == 1 && rPressedKeyName[0] < 128)
        {
            //--Setup.
            char tBuf[2];
            tBuf[0] = rPressedKeyName[0];
            tBuf[1] = '\0';

            //--Downshift it if it's a letter.
            if(tBuf[0] >= 'A' && tBuf[0] <= 'Z')
            {
                tBuf[0] = tBuf[0] + 'a' - 'A';
            }

            //--Resolve which letter this matches on the key list.
            int tLetterSlot = tBuf[0] - 'a';
            if(tLetterSlot >= SE_BTN_A && tLetterSlot <= SE_BTN_0)
            {
                mPressTimers[tLetterSlot] = mTimer + cAddTicks;
            }

            //--If the key is shifted, move it to uppercase.
            if(mIsShifted)
            {
                //--Letters.
                if(tBuf[0] >= 'a' && tBuf[0] <= 'z')
                {
                    tBuf[0] = tBuf[0] + 'A' - 'a';
                }
                //--Numerics.
                else if(tBuf[0] >= '0' && tBuf[0] <= '9')
                {
                    if(tBuf[0] == '1') tBuf[0] = '!';
                    if(tBuf[0] == '2') tBuf[0] = '@';
                    if(tBuf[0] == '3') tBuf[0] = '#';
                    if(tBuf[0] == '4') tBuf[0] = '$';
                    if(tBuf[0] == '5') tBuf[0] = '%';
                    if(tBuf[0] == '6') tBuf[0] = '^';
                    if(tBuf[0] == '7') tBuf[0] = '&';
                    if(tBuf[0] == '8') tBuf[0] = '*';
                    if(tBuf[0] == '9') tBuf[0] = '(';
                    if(tBuf[0] == '0') tBuf[0] = ')';
                }
            }

            //--If Caps-Locked, switch case.
            if(mIsCapsLocked)
            {
                //--Letters lowercase.
                if(tBuf[0] >= 'a' && tBuf[0] <= 'z')
                {
                    tBuf[0] = tBuf[0] + 'A' - 'a';
                }
                //--Letters uppercase.
                else if(tBuf[0] >= 'A' && tBuf[0] <= 'Z')
                {
                    tBuf[0] = tBuf[0] + 'a' - 'A';
                }
            }

            //--Append it.
            strcat(mCurrentString, tBuf);

            //--Cancel shift unless the key was down.
            if(!rControlManager->IsDown("Shift")) mIsShifted = false;
        }
        //--Backspace.
        else if(!strcasecmp(rPressedKeyName, "Backspace"))
        {
            int tLen = (int)strlen(mCurrentString);
            mCurrentString[tLen-1] = '\0';
            mPressTimers[SE_BTN_BACKSPACE] = mTimer + cAddTicks;
        }
        //--Space.
        else if(!strcasecmp(rPressedKeyName, "Space"))
        {
            strcat(mCurrentString, " ");
            mPressTimers[SE_BTN_SPACE] = mTimer + cAddTicks;
        }
        //--Capslock.
        else if(!strcasecmp(rPressedKeyName, "CapsLock"))
        {
            mIsCapsLocked = !mIsCapsLocked;
        }
        //--Shift.
        else if(!strcasecmp(rPressedKeyName, "LShift") || !strcasecmp(rPressedKeyName, "RShift"))
        {
            mIsShifted = !mIsShifted;
        }
        //--Grave/Tilde.
        else if(!strcasecmp(rPressedKeyName, "Tilde"))
        {
            ConditionalAppend(mCurrentString, (mIsShifted == true), "`", "~");
            mPressTimers[SE_BTN_GRAVE] = mTimer + cAddTicks;
        }
        //--Minus. Can be from the pad.
        else if(!strcasecmp(rPressedKeyName, "Minus"))
        {
            ConditionalAppend(mCurrentString, (mIsShifted == true), "-", "_");
            mPressTimers[SE_BTN_MINUS] = mTimer + cAddTicks;
        }
        //--Equals. Can be from the pad.
        else if(!strcasecmp(rPressedKeyName, "Equals"))
        {
            ConditionalAppend(mCurrentString, (mIsShifted == true), "=", "+");
            mPressTimers[SE_BTN_EQUALS] = mTimer + cAddTicks;
        }
        //--Left or Open Brace.
        else if(!strcasecmp(rPressedKeyName, "LBrace"))
        {
            ConditionalAppend(mCurrentString, (mIsShifted == true), "[", "{");
            mPressTimers[SE_BTN_LBRACE] = mTimer + cAddTicks;
        }
        //--Right or Close Brace.
        else if(!strcasecmp(rPressedKeyName, "RBrace"))
        {
            ConditionalAppend(mCurrentString, (mIsShifted == true), "]", "}");
            mPressTimers[SE_BTN_RBRACE] = mTimer + cAddTicks;
        }
        //--Semicolon.
        else if(!strcasecmp(rPressedKeyName, "Semicolon"))
        {
            ConditionalAppend(mCurrentString, (mIsShifted == true), ";", ":");
            mPressTimers[SE_BTN_SEMICOLON] = mTimer + cAddTicks;
        }
        //--Quote.
        else if(!strcasecmp(rPressedKeyName, "Quote"))
        {
            ConditionalAppend(mCurrentString, (mIsShifted == true), "'", "\"");
            mPressTimers[SE_BTN_QUOTE] = mTimer + cAddTicks;
        }
        //--Backslash.
        else if(!strcasecmp(rPressedKeyName, "Backslash"))
        {
            ConditionalAppend(mCurrentString, (mIsShifted == true), "\\", "|");
            mPressTimers[SE_BTN_BACKSLASH] = mTimer + cAddTicks;
        }
        //--Comma.
        else if(!strcasecmp(rPressedKeyName, "Comma"))
        {
            ConditionalAppend(mCurrentString, (mIsShifted == true), ",", "<");
            mPressTimers[SE_BTN_COMMA] = mTimer + cAddTicks;
        }
        //--Period.
        else if(!strcasecmp(rPressedKeyName, "Period"))
        {
            ConditionalAppend(mCurrentString, (mIsShifted == true), ".", ">");
            mPressTimers[SE_BTN_PERIOD] = mTimer + cAddTicks;
        }
        //--Slash.
        else if(!strcasecmp(rPressedKeyName, "Slash"))
        {
            ConditionalAppend(mCurrentString, (mIsShifted == true), "/", "?");
            mPressTimers[SE_BTN_SLASH] = mTimer + cAddTicks;
        }
    }
    //--Backspace refire case.
    else if(rControlManager->IsDown("Backspace") && mTimer >= mPressTimers[SE_BTN_BACKSPACE] + mRefireTimer - cAddTicks)
    {
        int tLen = (int)strlen(mCurrentString);
        mCurrentString[tLen-1] = '\0';
        mPressTimers[SE_BTN_BACKSPACE] = mTimer + cAddTicks;
        mRefireTimer -= 2;
        if(mRefireTimer < REFIRE_MINIMUM) mRefireTimer = REFIRE_MINIMUM;
    }

    //--Reset the refire timer.
    if(!rControlManager->IsDown("Backspace"))
    {
        mRefireTimer = REFIRE_DEFAULT;
    }
    //fprintf(stderr, "Refire timer %i\n", mRefireTimer);

    //--[Special Timers]
    //--The buttons change timers when active.
    if(mIsShifted) mPressTimers[SE_BTN_SHIFT] = mTimer + cAddTicks;
    if(mIsCapsLocked) mPressTimers[SE_BTN_CAPSLOCK] = mTimer + cAddTicks;

    //--First release of the shift key disables shifting.
    if(rControlManager->IsFirstRelease("Shift") && mIsShifted) mIsShifted = false;
    if(rControlManager->IsFirstPress("Shift")   && !mIsShifted) mIsShifted = true;
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void StringEntry::Render()
{
    Render(1.0f);
}
void StringEntry::Render(float pOpacity)
{
    //--[Documentation and Setup]
    //--Renders the StringEntry object, including highlighting the selected button. This modifies the
    //  color mixer back to default when execution completes.
    if(!Images.mIsReady || pOpacity <= 0.0f) return;

    //--Set opacity.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pOpacity);

    //--Constants.
    float cRenderX = 278.0f;
    float cRenderY =  61.0f;
    float cTextX = 401.0f;
    float cTextY = 152.0f;

    //--Base.
    Images.Data.rRenderBase->Draw(cRenderX, cRenderY);

    //--[Text Parts]
    //--Current text, if not NULL and containing at least one letter:
    if(mCurrentString[0] != '\0')
    {
        Images.Data.rHeaderFont->DrawText(cTextX, cTextY, 0, 1.0f, mCurrentString);
    }
    else
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pOpacity);
        Images.Data.rHeaderFont->DrawText(cTextX, cTextY, 0, 1.0f, mEmptyString);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pOpacity);
    }

    //--Flashing indicator.
    if(mTimer % 30 < 15)
    {
        float cLen = Images.Data.rHeaderFont->GetTextWidth(mCurrentString) * 1.0f;
        Images.Data.rHeaderFont->DrawText(cTextX + cLen, cTextY, 0, 1.0f, "_");
    }

    //--Render the prompt.
    if(mPromptString)
    {
        Images.Data.rHeaderFont->DrawText(683.0f, 83.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mPromptString);
    }

    //--[Input Clicks]
    //--Constants
    float cRenderWid = (float)Images.Data.rRenderPressed->GetWidth();
    float cRenderHei = (float)Images.Data.rRenderPressed->GetHeight();

    //--Show the keys pressed as slightly red.
    Images.Data.rRenderPressed->Bind();
    glBegin(GL_QUADS);
    for(int i = 0; i < SE_BTN_TOTAL; i ++)
    {
        //--If the button's timer is less than the current timer, or higher than the fade value, don't render.
        if(mPressTimers[i] < mTimer) continue;

        //--Compute alpha.
        float cAlpha = (mPressTimers[i] - mTimer) / 15.0f;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha * pOpacity);

        //--Compute where this button is on the texture.
        float cTxL = (mButtons[i].mLft - cRenderX) / cRenderWid;
        float cTxT = (mButtons[i].mTop - cRenderY) / cRenderHei;
        float cTxR = (mButtons[i].mRgt - cRenderX) / cRenderWid;
        float cTxB = (mButtons[i].mBot - cRenderY) / cRenderHei;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex2f(mButtons[i].mLft, mButtons[i].mTop);
        glTexCoord2f(cTxR, cTxT); glVertex2f(mButtons[i].mRgt, mButtons[i].mTop);
        glTexCoord2f(cTxR, cTxB); glVertex2f(mButtons[i].mRgt, mButtons[i].mBot);
        glTexCoord2f(cTxL, cTxB); glVertex2f(mButtons[i].mLft, mButtons[i].mBot);
    }
    glEnd();
    StarlightColor::ClearMixer();

}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//--[Worker Function]
//--Appends one or the other string based on the conditional. Basically here to turn 6 lines into 1.
void ConditionalAppend(char *sString, bool pConditional, const char *pFalseString, const char *pTrueString)
{
    if(!pConditional)
    {
        strcat(sString, pFalseString);
    }
    else
    {
        strcat(sString, pTrueString);
    }
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
