//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdvHelp.h"
#include "AdventureInventory.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "Global.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "OptionsManager.h"

//--[Local Definitions]
void RenderLineBounded(float pLft, float pTop, float pWidth, SugarFont *pFont, const char *pString);
int ComputeAbilityCursorMax(SugarLinkedList *pAbilityList);

//--[Manipulators]
void AdventureMenu::SetToMainMenu()
{
    //--Flags.
    mBaseVars.mGridCurrent = 0;
    mCurrentMode = AM_MODE_BASE;

    //--Reset the help string, in case it changed.
    mShowHelpString->SetImageP(0, CM_IMG_OFFSET_Y, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();

    //--On the first build, setup the menu grid.
    if(!mBaseVars.mHasBuiltGrid)
    {
        //--Flag.
        mBaseVars.mHasBuiltGrid = true;

        //--Grid Auto-Builder. 7x7 grid that automatically positions the buttons relative to one another.
        int cGridVals[7][7];
        for(int x = 0; x < 7; x ++)
        {
            for(int y = 0; y < 7; y ++)
            {
                cGridVals[x][y] = -1;
            }
        }

        //--Set the positions. 3x3 is the middle position.
        cGridVals[3][3] = AM_MAIN_ITEMS;
        cGridVals[3][2] = AM_MAIN_EQUIPMENT;
        cGridVals[2][3] = AM_MAIN_HEAL;
        cGridVals[4][3] = AM_MAIN_SKILLS;
        cGridVals[3][4] = AM_MAIN_MAP;

        cGridVals[2][2] = AM_MAIN_STATUS;
        cGridVals[4][2] = AM_MAIN_OPTIONS;
        cGridVals[2][4] = AM_MAIN_QUIT;

        //--Now loop through the grid and set everything accordingly. Note that the very edges
        //  are skipped, because they are "buffer" entries.
        for(int x = 1; x < 6; x ++)
        {
            for(int y = 1; y < 6; y ++)
            {
                //--Skip -1 instances.
                if(cGridVals[x][y] == -1) continue;

                //--Get the entry.
                int i = cGridVals[x][y];

                //--Set which controls go where.
                mBaseVars.mGrid[i].mControlLookups[GRID_LFT] = cGridVals[x-1][y+0];
                mBaseVars.mGrid[i].mControlLookups[GRID_TOP] = cGridVals[x+0][y-1];
                mBaseVars.mGrid[i].mControlLookups[GRID_RGT] = cGridVals[x+1][y+0];
                mBaseVars.mGrid[i].mControlLookups[GRID_BOT] = cGridVals[x+0][y+1];

                //--Set the grid position.
                mBaseVars.mGrid[i].mXPos = AM_GRID_CENT_X + ((x-3) * AM_GRID_SPAC_X);
                mBaseVars.mGrid[i].mYPos = AM_GRID_CENT_Y + ((y-3) * AM_GRID_SPAC_Y);
            }
        }
    }
}
void AdventureMenu::RefreshMainMenuHelp()
{
    //--Resolve the images needed for the help menu.
    ControlManager *rControlManager = ControlManager::Fetch();
    mMainMenuHelp->Construct();

    //--Allocate strings.
    int cStrings = 5;
    mMainMenuHelp->AllocateStrings(cStrings);
    StarlightString **rStrings = mMainMenuHelp->GetStrings();

    //--Set.
    rStrings[0]->SetString("[IMG0] Select Option");
    rStrings[0]->AllocateImages(1);
    rStrings[0]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Activate"));

    rStrings[1]->SetString("[IMG0] Return to Game");
    rStrings[1]->AllocateImages(1);
    rStrings[1]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Cancel"));

    rStrings[2]->SetString(" ");

    rStrings[3]->SetString("[IMG0][IMG1][IMG2][IMG3] Change Selection");
    rStrings[3]->AllocateImages(4);
    rStrings[3]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Up"));
    rStrings[3]->SetImageP(1, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Right"));
    rStrings[3]->SetImageP(2, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Down"));
    rStrings[3]->SetImageP(3, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Left"));

    rStrings[4]->SetString("[IMG0] Close This Display");
    rStrings[4]->AllocateImages(1);
    rStrings[4]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("F1"));

    //--All strings cross-reference here.
    for(int i = 0; i < cStrings; i ++) rStrings[i]->CrossreferenceImages();
}

//--[Update]
bool AdventureMenu::UpdateMainMenu(bool pCannotHandleUpdate)
{
    ///--[Documentation and Setup]
    //--Standard update for the cursor. Right and Left controls are enabled. Wrapping is disabled.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mCurrentMode == AM_MODE_BASE && mIsVisible)
    {
        if(mBaseVars.mGridOpacityTimer < ADV_MENU_VIS_TICKS) mBaseVars.mGridOpacityTimer ++;
    }
    //--Decrement.
    else
    {
        if(mBaseVars.mGridOpacityTimer > 0) mBaseVars.mGridOpacityTimer --;
    }

    //--Help is visible.
    if(mIsMainHelpVisible)
    {
        if(mMainHelpTimer < ADV_HELP_STD_TICKS) mMainHelpTimer ++;
    }
    else
    {
        if(mMainHelpTimer > 0) mMainHelpTimer --;
    }

    ///--[Not Main Object]
    //--If this menu is not the main object, stop the update here.
    if(mCurrentMode != AM_MODE_BASE || !mIsVisible || pCannotHandleUpdate) return false;

    ///--[Help Handler]
    if(mIsMainHelpVisible)
    {
        if(rControlManager->IsFirstPress("F1") || rControlManager->IsFirstPress("Cancel"))
        {
            mIsMainHelpVisible = false;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        return true;
    }

    //--Help activation.
    if(rControlManager->IsFirstPress("F1"))
    {
        mIsMainHelpVisible = true;
        RefreshMainMenuHelp();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[Grid Handler]
    //--Handle keypresses in the grid interface. First, range-check the grid.
    if(mBaseVars.mGridCurrent < 0 || mBaseVars.mGridCurrent >= AM_MAIN_TOTAL) mBaseVars.mGridCurrent = 0;

    //--Directional controls are handled programmatically. If the instruction is -1, it means there is nothing
    //  at that grid position.
    int tGridPrevious = mBaseVars.mGridCurrent;
    if(rControlManager->IsFirstPress("Up"))
    {
        int tTargetEntry = mBaseVars.mGrid[mBaseVars.mGridCurrent].mControlLookups[GRID_TOP];
        if(tTargetEntry != -1) mBaseVars.mGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tTargetEntry = mBaseVars.mGrid[mBaseVars.mGridCurrent].mControlLookups[GRID_BOT];
        if(tTargetEntry != -1) mBaseVars.mGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        int tTargetEntry = mBaseVars.mGrid[mBaseVars.mGridCurrent].mControlLookups[GRID_LFT];
        if(tTargetEntry != -1) mBaseVars.mGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tTargetEntry = mBaseVars.mGrid[mBaseVars.mGridCurrent].mControlLookups[GRID_RGT];
        if(tTargetEntry != -1) mBaseVars.mGridCurrent = tTargetEntry;
    }

    //--If the grid position changed, play sounds and handle the size changer.
    if(tGridPrevious != mBaseVars.mGridCurrent)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    ///--[Grid Timers]
    //--Size setting. All not-selected grid entries decrease their timers. The selected entry increases.
    for(int i = 0; i < AM_MAIN_TOTAL; i ++)
    {
        if(i != mBaseVars.mGridCurrent)
        {
            mBaseVars.mGrid[i].mTimer --;
            if(mBaseVars.mGrid[i].mTimer < 1) mBaseVars.mGrid[i].mTimer = 0;
        }
        else
        {
            mBaseVars.mGrid[i].mTimer ++;
            if(mBaseVars.mGrid[i].mTimer >= ADV_MENU_SIZE_TICKS) mBaseVars.mGrid[i].mTimer = ADV_MENU_SIZE_TICKS;
        }
    }

    ///--[Activation Handler]
    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        if(mBaseVars.mGridCurrent == AM_MAIN_ITEMS)
        {
            SetToItemsMenu();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(mBaseVars.mGridCurrent == AM_MAIN_EQUIPMENT)
        {
            SetToEquipMenu();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(mBaseVars.mGridCurrent == AM_MAIN_SKILLS)
        {
            SetToSkillsMode();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(mBaseVars.mGridCurrent == AM_MAIN_STATUS)
        {
            if(AdvCombat::Fetch()->GetActivePartyCount() > 0)
            {
                SetStatusCharacter(0);
                mCurrentMode = AM_MODE_STATUS;
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
        else if(mBaseVars.mGridCurrent == AM_MAIN_HEAL)
        {
            SetToHealMode();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(mBaseVars.mGridCurrent == AM_MAIN_MAP)
        {
            //--No map? Fail.
            if(!rActiveMap && mMapLayerList->GetListSize() < 1)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--Activate map mode.
            else
            {
                InitializeMapMode();
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
        else if(mBaseVars.mGridCurrent == AM_MAIN_OPTIONS)
        {
            SetToOptionsMode();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(mBaseVars.mGridCurrent == AM_MAIN_QUIT)
        {
            SetToQuitMenu();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel") && !mStartedHidingThisTick)
    {
        Hide();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--We handled the update.
    return true;
}

//--[Drawing]
void AdventureMenu::RenderMainMenu()
{
    ///--[Documentation and Setup]
    //--This menu allows the player to access the submenus. It shows cash, crafting items, catalysts, and party status.
    if(!Images.mIsReady) return;

    ///--[Quit Menu]
    //--If quit mode is active, render that over the menu. This ignores opacity computations.
    RenderQuitMenu();

    ///--[Opacity Percent]
    //--The render function is always called, but fades in and out. If opacity is zero, stop rendering.
    float tOpacityPct = (float)mBaseVars.mGridOpacityTimer / (float)ADV_MENU_VIS_TICKS;
    float cMainOpacity = (float)mMainOpacityTimer / (float)AM_OPACITY_TICKS;
    tOpacityPct = tOpacityPct * cMainOpacity;
    if(tOpacityPct <= 0.0f) return;

    ///--[Constants and Variables]
    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Text sizes.
    float cMainlineTextHeight = Images.BaseMenu.rMainlineFont->GetTextHeight();

    //--Rendering flag sets.
    uint32_t cFlagsL = 0;
    uint32_t cFlagsR = SUGARFONT_RIGHTALIGN_X;

    ///--[Title]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    Images.BaseMenu.rHeader->Draw(427.0f, 45.0f);
    Images.BaseMenu.rHeadingFont->DrawText(683.0f, 57.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Pause Menu");

    ///--[Static Backing]
    //--Always render in the same position.
    Images.BaseMenu.rFooter->Draw();
    Images.BaseMenu.rInventoryBacks->Draw();
    Images.BaseMenu.rInventoryBanners->Draw();
    Images.BaseMenu.rPlatinaBanner->Draw();

    ///--[Platina Display]
    //--Render how much cash the player has.
    float cPlatinaPosX = Images.BaseMenu.rPlatinaText->GetXOffset();
    float cPlatinaPosY = Images.BaseMenu.rPlatinaText->GetYOffset();
    Images.BaseMenu.rHeadingFont->DrawTextArgs(cPlatinaPosX, cPlatinaPosY, 0, 1.0f, "Platina: %i", rInventory->GetPlatina());

    ///--[Adamantite Display]
    //--Show the adamantite counts.
    float cAdamantiteX = 1146.0f;
    float cAdamantiteT = 1166.0f;
    float cAdamantiteR = 1356.0f;
    float cAdamantiteY =  150.0f;
    float cAdamantiteSpacingY = cMainlineTextHeight + 2.0f;
    for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
    {
        Images.BaseMenu.rAdamantiteImg[i]->Draw(cAdamantiteX, cAdamantiteY + (cAdamantiteSpacingY * i) + 3.0f);
    }
    Images.BaseMenu.rHeadingFont->DrawTextArgs(1251.0f, 104.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Adamantite");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 0.00f), cFlagsL, 1.0f, "Powder");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 0.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_POWDER));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 1.00f), cFlagsL, 1.0f, "Flakes");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 1.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_FLAKES));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 2.00f), cFlagsL, 1.0f, "Shards");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 2.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_SHARD));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 3.00f), cFlagsL, 1.0f, "Pieces");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 3.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_PIECE));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 4.00f), cFlagsL, 1.0f, "Chunks");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 4.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_CHUNK));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 5.00f), cFlagsL, 1.0f, "Ore");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 5.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_ORE));

    ///--[Catalyst Display]
    //--Show the catalyst counts.
    float cCatalystX = 1146.0f;
    float cCatalystR = 1216.0f;
    float cCatalystY = 420.0f;
    float cCatalystSpacingY = cMainlineTextHeight + 2.0f;
    Images.BaseMenu.rHeadingFont->DrawText(1251.0f, 377.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Catalysts");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 0.00f), cFlagsL, 1.0f, "HP:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 0.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_HEALTH));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 1.00f), cFlagsL, 1.0f, "Atk:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 1.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_ATTACK));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 2.00f), cFlagsL, 1.0f, "Ini:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 2.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_INITIATIVE));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 3.00f), cFlagsL, 1.0f, "Acc:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 3.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_ACCURACY));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 4.00f), cFlagsL, 1.0f, "Evd:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 4.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_DODGE));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 5.00f), cFlagsL, 1.0f, "Skl:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 5.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_SKILL));

    //--Catalyst Collection Total:
    int tCatalystTotal = rInventory->GetCatalystCount(CATALYST_HEALTH) + rInventory->GetCatalystCount(CATALYST_ATTACK) + rInventory->GetCatalystCount(CATALYST_INITIATIVE) + rInventory->GetCatalystCount(CATALYST_ACCURACY) + rInventory->GetCatalystCount(CATALYST_DODGE) + rInventory->GetCatalystCount(CATALYST_SKILL);
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 7.00f), cFlagsL, 1.0f, "Collected: %i / %i", tCatalystTotal, xCatalystTotal);

    //--Catalyst bonuses.
    float cBonusOff = 64.0f;
    if(rInventory->ComputeCatalystBonus(CATALYST_HEALTH) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 0.00f), 0, 1.0f, "+%i HP", rInventory->ComputeCatalystBonus(CATALYST_HEALTH));
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_ATTACK) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 1.00f), 0, 1.0f, "+%i Atk", rInventory->ComputeCatalystBonus(CATALYST_ATTACK));
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_INITIATIVE) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 2.00f), 0, 1.0f, "+%i Ini", rInventory->ComputeCatalystBonus(CATALYST_INITIATIVE));
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_ACCURACY) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 3.00f), 0, 1.0f, "+%i Acc", rInventory->ComputeCatalystBonus(CATALYST_ACCURACY));
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_DODGE) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 4.00f), 0, 1.0f, "+%i Evd", rInventory->ComputeCatalystBonus(CATALYST_DODGE));
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_SKILL) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 5.00f), 0, 1.0f, "+%i Skl", rInventory->ComputeCatalystBonus(CATALYST_SKILL));
    }

    ///--[Icons]
    //--Sizes.
    float cIconSize = Images.BaseMenu.rIconFrame->GetTrueWidth();

    //--Position the objects slide away from.
    float tOpacitySlidePct = EasingFunction::QuadraticOut(mBaseVars.mGridOpacityTimer, ADV_MENU_VIS_TICKS);
    float tCenterX = AM_GRID_CENT_X;
    float tCenterY = AM_GRID_CENT_Y;

    //--Render icons at triple-size. Base size is 32.0f.
    for(int i = 0; i < AM_MAIN_TOTAL; i ++)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(mBaseVars.mGrid[i].mTimer, ADV_MENU_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * ADV_MENU_SIZE_BOOST);
        float cScaleInv = 1.0f / cScale;

        //--Figure out the render position.
        float tXPos = mBaseVars.mGrid[i].mXPos;
        float tYPos = mBaseVars.mGrid[i].mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        glTranslatef(tXPos, tYPos, 0.0f);
        glScalef(cScale, cScale, 1.0f);
        Images.BaseMenu.rIconFrame->Draw();
        Images.BaseMenu.rIcons[i]->Draw();
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-tXPos, -tYPos, 0.0f);
    }
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);

    ///--[Info Box]
    //--Backing.
    Images.BaseMenu.rDescriptionBox->Draw();

    //--Sizes.
    float cBotHeaderX = 683.0f;
    float cBotHeaderY = 532.0f;
    float cDescriptionLft = 316.0f;
    float cDescriptionTop = 575.0f;
    float cDescriptionWid = 746.0f;

    //--Inventory.
    if(mBaseVars.mGridCurrent == AM_MAIN_ITEMS)
    {
        Images.BaseMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Inventory");
        RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.BaseMenu.rMainlineFont, "Shows all the items you've collected on your adventure, including those equipped.");
    }
    //--Equipment.
    else if(mBaseVars.mGridCurrent == AM_MAIN_EQUIPMENT)
    {
        Images.BaseMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Equipment");
        RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.BaseMenu.rMainlineFont, "Change which party member is wearing what. Allows socketing of gems.");
    }
    //--Doctor Bag.
    else if(mBaseVars.mGridCurrent == AM_MAIN_HEAL)
    {
        Images.BaseMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Doctor Bag");
        RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.BaseMenu.rMainlineFont, "Use your doctor bag to heal party members. The doctor bag recharges after combat based on how quickly you defeat enemies.");
    }
    //--Status.
    else if(mBaseVars.mGridCurrent == AM_MAIN_STATUS)
    {
        Images.BaseMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Status");
        RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.BaseMenu.rMainlineFont, "Show current status, including equipment, resistances, and experience.");
    }
    //--Skills, jobs.
    else if(mBaseVars.mGridCurrent == AM_MAIN_SKILLS)
    {
        Images.BaseMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Skills and Jobs");
        RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.BaseMenu.rMainlineFont, "Change character jobs, purchase new skills, and equip skills to free slots.");
    }
    //--Map.
    else if(mBaseVars.mGridCurrent == AM_MAIN_MAP)
    {
        Images.BaseMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Map");
        RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.BaseMenu.rMainlineFont, "Review the area map.");
    }
    //--Options menu.
    else if(mBaseVars.mGridCurrent == AM_MAIN_OPTIONS)
    {
        Images.BaseMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Options");
        RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.BaseMenu.rMainlineFont, "Configure the game options.");
    }
    //--Quit menu.
    else if(mBaseVars.mGridCurrent == AM_MAIN_QUIT)
    {
        Images.BaseMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Quit or Reset");
        RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.BaseMenu.rMainlineFont, "Exit to the main menu, or quit the program.");
    }

    ///--[Party Portraits]
    //--Options.
    bool tIsSmallPortraitMode = OptionsManager::Fetch()->GetOptionB("LowResAdventureMode");

    //--Variables.
    float cXOffset = 192.0f * -3.0f;
    int cActivePartySize = rAdventureCombat->GetActivePartyCount();

    //--Iterate.
    for(int i = 0; i < cActivePartySize; i ++)
    {
        //--Get the character in question.
        AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rActiveEntity) continue;

        //--Constants
        float cYOffset = -45.0f;

        //--Backing.
        Images.BaseMenu.rPortraitBack->Draw(cXOffset, 0.0f);
        Images.BaseMenu.rComboBar->Draw(cXOffset, 0.0f);

        //--Activate stencils and render a mask.
        DisplayManager::ActivateMaskRender(AM_STENCIL_MAIN+i);
        Images.BaseMenu.rPortraitMask->Draw(cXOffset, 0.0f);

        //--Countermask, if it exists, stops rendering on these pixels. Not all entities have one.
        SugarBitmap *rCounterMask = rActiveEntity->GetCombatCounterMask();
        TwoDimensionRealPoint cMainRenderCoords = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_BASEMENU_MAIN);
        if(rCounterMask)
        {
            DisplayManager::ActivateCountermaskRender();
            rCounterMask->Draw(cMainRenderCoords.mXCenter + cXOffset, cMainRenderCoords.mYCenter + cYOffset);
        }

        //--GL Setup for rendering.
        DisplayManager::ActivateStencilRender(AM_STENCIL_MAIN+i);

        //--Current character, always exists.
        SugarBitmap *rFieldPortrait = rActiveEntity->GetCombatPortrait();
        if(rFieldPortrait)
        {
            //--Normal rendering:
            if(!tIsSmallPortraitMode)
            {
                rFieldPortrait->Draw(cMainRenderCoords.mXCenter + cXOffset, cMainRenderCoords.mYCenter + cYOffset);
            }
            //--Scaled rendering.
            else
            {
                rFieldPortrait->DrawScaled(cMainRenderCoords.mXCenter + cXOffset, cMainRenderCoords.mYCenter + cYOffset, LOWRES_SCALEINV, LOWRES_SCALEINV);
            }
        }

        //--Clean up.
        DisplayManager::DeactivateStencilling();

        //--Front of the portrait.
        Images.BaseMenu.rNameBar->Draw(cXOffset, 0.0f);
        Images.BaseMenu.rHeadingFont->DrawText(670.0f + cXOffset, 692.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, rActiveEntity->GetDisplayName());

        //--Health Bar
        Images.BaseMenu.rHealthBarBack->Draw(cXOffset, 0.0f);
        float tHPPercent = rActiveEntity->GetHealthPercent();
        if(tHPPercent > 0.0f)
        {
            //--Rendering setup.
            float cLft = Images.BaseMenu.rHealthBarFill->GetXOffset() + cXOffset;
            float cTop = Images.BaseMenu.rHealthBarFill->GetYOffset();
            float cRgt = Images.BaseMenu.rHealthBarFill->GetXOffset() + (Images.BaseMenu.rHealthBarFill->GetWidth() * tHPPercent) + cXOffset;
            float cBot = Images.BaseMenu.rHealthBarFill->GetYOffset() +  Images.BaseMenu.rHealthBarFill->GetHeight();
            float cTxL = 0.0f;
            float cTxT = 0.0f;
            float cTxR = tHPPercent;
            float cTxB = 1.0f;

            //--Render.
            Images.BaseMenu.rHealthBarFill->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cTop);
                glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cTop);
                glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cBot);
                glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cBot);
            glEnd();
        }
        Images.BaseMenu.rHealthBarFrame->Draw(cXOffset, 0.0f);

        //--Render the HP values.
        int tHP = rActiveEntity->GetHealth();
        int tHPMax = rActiveEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_HPMAX);
        Images.BaseMenu.rMainlineFont->DrawTextArgs(672.0f + cXOffset, 749.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i / %i", tHP, tHPMax);

        //--Character's level.
        Images.BaseMenu.rMainlineFont->DrawTextArgs(603.0f + cXOffset, 721.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i", rActiveEntity->GetLevel() + 1);

        //--Move the X offset over.
        cXOffset = cXOffset + 192.0f;
    }

    ///--[Debug]
    //--Render the version number in the top left.
    GLOBAL *rGlobal = Global::Shared();
    if(rGlobal->gBitmapFont)
    {
        rGlobal->gBitmapFont->DrawText(0.0f, 0.0f, 0, 1.0f, rGlobal->gVersionString);
    }

    ///--[Help Menu]
    //--Help string.
    mShowHelpString->DrawText(0.0f, 20.0f, 0, 1.0f, Images.BaseMenu.rMainlineFont);

    //--Menu, if visible.
    if(mMainHelpTimer > 0)
    {
        //--Compute opacity.
        float cPercentage = EasingFunction::QuadraticInOut(mMainHelpTimer, ADV_HELP_STD_TICKS);
        SugarBitmap::DrawFullBlack(cPercentage * 0.75f);

        //--Offset.
        float tYOffset = ADV_HELP_STD_OFFSET * (1.0f - cPercentage);
        mMainMenuHelp->Render(tYOffset);
    }
}

void PrintLetters(const char *pString)
{
    if(pString[0] == '\0') return;
    fprintf(stderr, "%c", pString[0]);
    PrintLetters(&pString[1]);
}
