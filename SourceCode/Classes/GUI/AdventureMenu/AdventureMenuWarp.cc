//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureLevel.h"
#include "TilemapActor.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

//--[Local Definitions]
#define WARPS_PER_PAGE 10
#define WARP_CROSSFADE_TICKS 15
#define WARP_REPOSITION_TICKS 15

//--[Forward Declarations]
void RenderLineBounded(float pLft, float pTop, float pWidth, SugarFont *pFont, const char *pString);
void RenderTextConditionalColor(SugarFont *pFont, float pX, float pY, int pFlags, float pSize, const char *pText, bool pConditional, StarlightColor pTrueColor, StarlightColor pFalseColor);

//--[System]
void AdventureMenu::SetToWarpMode()
{
    //--[Flags]
    mIsWarpMode = true;
    mWarpZoomFactor = 1.00f;
    mWarpGridCurrent = 0;
    mWarpGridSelectedRegion = -1;

    //--Clean old data.
    mReliveGrid->ClearList();

    //--[Positioning and Controls]
    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    int cXSize = 5;
    int cYSize = 3;
    int cXMiddle = 2;
    int cYMiddle = 1;
    int **tGridData = GridHandler::BuildGrid(cXSize, cYSize, cXMiddle, cYMiddle, mWarpList->GetListSize());

    //--Debug.
    if(false)
    {
        fprintf(stderr, "Warp grid %ix%i:\n", cXSize-2, cYSize-2);
        for(int y = 1; y < cYSize-1+2; y ++)
        {
            for(int x = 1; x < cXSize-1+2; x ++)
            {
                fprintf(stderr, "%2i ", tGridData[x][y]);
            }
            fprintf(stderr, "\n");
        }
    }

    //--Call the subroutine to build the control list for us.
    GridHandler::BuildGridList(mWarpGrid, tGridData, cXSize, cYSize, mWarpList->GetListSize(), AM_GRID_CENT_X, AM_GRID_CENT_Y, AM_GRID_SPAC_X, AM_GRID_SPAC_Y);

    //--Clean up the grid data. This argument expects the original grid size.
    GridHandler::DeallocateGrid(tGridData, cXSize);
}

//--[Manipulators]
void AdventureMenu::RegisterWarpRegionImage(const char *pName, const char *pImagePath)
{
    //--Registers a name/image pair. When the UI creates a new region, it is checked against the image list.
    //  If a match is found, that is the icon used by the region's grid entry.
    if(!pName || !pImagePath) return;
    mWarpRegionImages->AddElementAsTail(pName, DataLibrary::Fetch()->GetEntry(pImagePath));
}
void AdventureMenu::ClearWarpRegionImages()
{
    mWarpRegionImages->ClearList();
}
void AdventureMenu::RegisterWarpDestination(const char *pRegionName, const char *pDisplayName, const char *pActualName, const char *pImgName, const char *pOverlayName, float pImgX, float pImgY)
{
    //--Error check.
    if(!pRegionName || !pDisplayName || !pActualName || !pImgName) return;

    //--Check if an existing warp package is in the same region. If so, add to that list.
    WarpPackage *rPackage = (WarpPackage *)mWarpList->GetElementByName(pRegionName);
    if(rPackage)
    {
        //--Fast-access variables.
        int i = rPackage->mRoomsTotal;
        rPackage->mRoomsTotal ++;

        //--Set.
        strncpy(rPackage->mWarpNames[i], pDisplayName, STD_MAX_LETTERS);
        strncpy(rPackage->mWarpRooms[i], pActualName, STD_MAX_LETTERS);
        rPackage->mCoordX[i] = pImgX;
        rPackage->mCoordY[i] = pImgY;

        //--Finish up.
        return;
    }

    //--No match, we need to create a new region.
    SetMemoryData(__FILE__, __LINE__);
    WarpPackage *nPackage = (WarpPackage *)starmemoryalloc(sizeof(WarpPackage));
    nPackage->Initialize();
    mWarpList->AddElement(pRegionName, nPackage, &FreeThis);

    //--Call case: A script needs to be called to populate advanced map properties when warping.
    if(!strncmp(pImgName, "CALL|", 5))
    {
        strcpy(nPackage->mCallBuffer, &pImgName[5]);
        //fprintf(stderr, "Call buffer: %s\n", nPackage->mCallBuffer);
    }
    //--Normal case, single-image maps.
    else
    {
        //--Get the associated image.
        char tBuffer[256];
        sprintf(tBuffer, "Root/Images/AdvMaps/General/%s", pImgName);
        nPackage->rMapImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(tBuffer);

        //--Attempt to locate the overlay.
        sprintf(tBuffer, "Root/Images/AdvMaps/General/%s", pOverlayName);
        nPackage->rMapOverlayImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(tBuffer);
    }

    //--The zeroth entry is the passed in data.
    strncpy(nPackage->mWarpNames[0], pDisplayName, STD_MAX_LETTERS);
    strncpy(nPackage->mWarpRooms[0], pActualName, STD_MAX_LETTERS);
    nPackage->mCoordX[0] = pImgX;
    nPackage->mCoordY[0] = pImgY;
    nPackage->mRoomsTotal ++;

    //--Try to resolve a matching image. If it exists, add it to the linked list. Dummy values also get added to maintain
    //  parallel lists.
    SugarBitmap *rCheckImage = (SugarBitmap *)mWarpRegionImages->GetElementByName(pRegionName);
    if(!rCheckImage) rCheckImage = Images.CampfireMenu.rIconFrame;
    mWarpGridImg->AddElement("X", rCheckImage);
}

//--[Core Methods]
void AdventureMenu::ClampWarpFocusPoint(SugarBitmap *pImage, float &sXPos, float &sYPos)
{
    //--Clamps the warp focus point so it doesn't go offscreen.
    if(!pImage) return;

    //--Sizes.
    float cFactor = 1.0f / mWarpZoomFactor;
    int tLftClamp = 0.0f + (VIRTUAL_CANVAS_X * cFactor * 0.50f);
    int tTopClamp = 0.0f + (VIRTUAL_CANVAS_Y * cFactor * 0.50f);
    int tRgtClamp = pImage->GetTrueWidth()  - (VIRTUAL_CANVAS_X * cFactor * 0.50f);
    int tBotClamp = pImage->GetTrueHeight() - (VIRTUAL_CANVAS_Y * cFactor * 0.50f);

    //--Clamp!
    if(sXPos < tLftClamp) sXPos = tLftClamp;
    if(sYPos < tTopClamp) sYPos = tTopClamp;
    if(sXPos > tRgtClamp) sXPos = tRgtClamp;
    if(sYPos > tBotClamp) sYPos = tBotClamp;
}

//--[Update]
void AdventureMenu::UpdateWarpMode()
{
    //--[Region Selection]
    //--Run the region update.
    mWarpHandledInput = false;
    UpdateWarpRegionMode();
    if(mWarpHandledInput) return;

    //--[Timers]
    //--If in warp mode, increment this timer.
    if(mIsWarpMode)
    {
        //--In region mode, we stop the update.
        if(mWarpGridSelectedRegion != -1)
        {
            if(mWarpGridOpacityTimer > 0) mWarpGridOpacityTimer --;
            return;
        }
        //--Otherwise, increase the opacity timer.
        else
        {
            if(mWarpGridOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mWarpGridOpacityTimer ++;
        }
    }
    //--Decrement and stop if not.
    else
    {
        if(mWarpGridOpacityTimer > 0) mWarpGridOpacityTimer --;
        return;
    }

    //--[Setup]
    //--Handles controls for the warp menu. Handles up/down/activate/cancel.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Error check:
    if(mWarpGridCurrent < 0 || mWarpGridCurrent >= mWarpList->GetListSize()) mWarpGridCurrent = 0;
    GridPackage *rPackage = (GridPackage *)mWarpGrid->GetElementBySlot(mWarpGridCurrent);
    if(!rPackage)
    {
        mStartedHidingThisTick = true;
        mIsWarpMode = false;
        mCurrentCursor = AM_SAVE_WARP;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    //--[Directions]
    //--Directional controls are handled programmatically. If the instruction is -1, it means there is nothing
    //  at that grid position.
    int tWarpGridPrevious = mWarpGridCurrent;
    if(rControlManager->IsFirstPress("Up"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_TOP];
        if(tTargetEntry != -1) mWarpGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_BOT];
        if(tTargetEntry != -1) mWarpGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_LFT];
        if(tTargetEntry != -1) mWarpGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_RGT];
        if(tTargetEntry != -1) mWarpGridCurrent = tTargetEntry;
    }

    //--If the grid changed positions, play a sound.
    if(tWarpGridPrevious != mWarpGridCurrent)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Size setting. All not-selected grid entries decrease their timers. The selected entry increases.
    GridPackage *rTimerPackage = (GridPackage *)mWarpGrid->PushIterator();
    while(rTimerPackage)
    {
        //--Increment if selected.
        if(rTimerPackage == rPackage)
        {
            if(rTimerPackage->mTimer < AM_CAMPFIRE_SIZE_TICKS) rTimerPackage->mTimer ++;
        }
        //--Decrement if deselected.
        else
        {
            if(rTimerPackage->mTimer > 0) rTimerPackage->mTimer --;
        }
        rTimerPackage = (GridPackage *)mWarpGrid->AutoIterate();
    }

    //--Select a warp region.
    if(rControlManager->IsFirstPress("Activate") && xWarpExecuteScript)
    {
        //--Set this as the active region.
        mWarpGridSelectedRegion = mWarpGridCurrent;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--Error check.
        WarpPackage *rWarpPackage = (WarpPackage *)mWarpList->GetElementBySlot(mWarpGridSelectedRegion);
        if(!rWarpPackage)
        {
            mWarpGridSelectedRegion = -1;
            return;
        }

        //--Archive the map properties so they don't overwrite the current room's map.
        ArchiveAdvancedMap();

        //--If the call buffer has something in it, we need to call a script to set advanced map properties.
        if(rWarpPackage->mCallBuffer[0] != '\0')
        {

            //--Call.
            char tBuffer[STD_PATH_LEN];
            const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
            sprintf(tBuffer, "%s/%s", rAdventurePath, rWarpPackage->mCallBuffer);
            LuaManager::Fetch()->ExecuteLuaFile(tBuffer);
        }

        //--Set the cursor to the zeroth entry.
        mWarpSelectedRoom = 0;

        //--Take the zeroth entry for the region and move the map to it.
        mWarpReposTimer = WARP_REPOSITION_TICKS;
        mWarpReposXStart = rWarpPackage->mCoordX[0];
        mWarpReposYStart = rWarpPackage->mCoordY[0];
        mWarpReposXFinish = rWarpPackage->mCoordX[0];
        mWarpReposYFinish = rWarpPackage->mCoordY[0];
        ClampWarpFocusPoint(rWarpPackage->rMapImage, mWarpReposXStart, mWarpReposYStart);
        ClampWarpFocusPoint(rWarpPackage->rMapImage, mWarpReposXFinish, mWarpReposYFinish);
    }
    //--Back to the main menu.
    else if(rControlManager->IsFirstPress("Cancel") && !mStartedHidingThisTick)
    {
        mIsWarpMode = false;
        mStartedHidingThisTick = true;
        mCurrentCursor = AM_SAVE_WARP;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

//--[Drawing]
void AdventureMenu::RenderWarpMode()
{
    //--[Underlay]
    //--The region may be fading out. Render it here.
    RenderWarpRegionMode();

    //--[Setup]
    //--Don't render at all if the opacity is at zero.
    if(mWarpGridOpacityTimer < 1) return;
    if(!Images.mIsReady) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mWarpGridOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--[Title]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    Images.CampfireMenu.rHeader->Draw(427.0f, 45.0f);
    Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 57.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select a Region");
    StarlightColor::ClearMixer();

    //--[Icons]
    //--Sizes.
    float cIconSize = Images.CampfireMenu.rIconFrame->GetTrueWidth();

    //--Position the objects slide away from.
    float tOpacitySlidePct = EasingFunction::QuadraticOut(mWarpGridOpacityTimer, AM_CAMPFIRE_OPACITY_TICKS);
    float tCenterX = AM_GRID_CENT_X;
    float tCenterY = AM_GRID_CENT_Y;

    //--Iterate across the list.
    GridPackage *rGridPack = (GridPackage *)mWarpGrid->PushIterator();
    WarpPackage *rWarpPack = (WarpPackage *)mWarpList->PushIterator();
    SugarBitmap *rWarpImg  = (SugarBitmap *)mWarpGridImg->PushIterator();
    while(rGridPack && rWarpPack && rWarpImg)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
        float cScaleInv = 1.0f / cScale;

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        glTranslatef(tXPos, tYPos, 0.0f);
        glScalef(cScale, cScale, 1.0f);
        Images.CampfireMenu.rIconFrame->Draw();
        if(rWarpImg) rWarpImg->Draw(0, 0);
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-tXPos, -tYPos, 0.0f);

        //--Next.
        rGridPack = (GridPackage *)mWarpGrid->AutoIterate();
        rWarpPack = (WarpPackage *)mWarpList->AutoIterate();
        rWarpImg  = (SugarBitmap *)mWarpGridImg->AutoIterate();
    }

    //--After the main grid loop, re-run the loop and render the text. This is to make it appear over everything else.
    rGridPack = (GridPackage *)mWarpGrid->PushIterator();
    rWarpPack = (WarpPackage *)mWarpList->PushIterator();
    while(rGridPack && rWarpPack)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);
        float tTextX = tXPos;

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Text.
        float cTxtScale = 1.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
        Images.CampfireMenu.rMainlineFont->DrawText(tTextX, tYPos - 15.0f, SUGARFONT_AUTOCENTER_X, cTxtScale, mWarpList->GetIteratorName());

        //--Next.
        rGridPack = (GridPackage *)mWarpGrid->AutoIterate();
        rWarpPack = (WarpPackage *)mWarpList->AutoIterate();
    }
}
void AdventureMenu::UpdateWarpRegionMode()
{
    //--[Documentation and Setup]
    //--When the player selects a region to warp to, this update handles the rest. It always gets called
    //  in case it's fading out.
    if(mWarpGridSelectedRegion != -1)
    {
        if(mWarpRegionOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mWarpRegionOpacityTimer ++;
    }
    //--Decrement and stop if not.
    else
    {
        if(mWarpRegionOpacityTimer > 0) mWarpRegionOpacityTimer --;
        return;
    }

    //--[Advanced Map Reroll]
    //--Timers for advanced map mode. Does nothing in simplified map modes.
    if(mAdvancedMapReroll > 0)
    {
        mAdvancedMapReroll --;
    }
    else
    {
        mAdvancedMapReroll = 3 + (rand() % 3);
        mAdvancedMapLastRoll = rand() % 1000;
    }

    //--Run this timer.
    if(mWarpReposTimer < WARP_REPOSITION_TICKS) mWarpReposTimer ++;

    //--Get the selected warp region.
    WarpPackage *rPackage = (WarpPackage *)mWarpList->GetElementBySlot(mWarpGridSelectedRegion);
    if(!rPackage)
    {
        UnarchiveAdvancedMap();
        mWarpGridSelectedRegion = -1;
        mStartedHidingThisTick = true;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    //--[Control Handlers]
    //--Setup.
    int tOldRoom = mWarpSelectedRoom;
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Up.
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Decrement.
        mWarpSelectedRoom = mWarpSelectedRoom - 1;
        if(mWarpSelectedRoom < 0) mWarpSelectedRoom = rPackage->mRoomsTotal - 1;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Down.
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        //--Increment.
        mWarpSelectedRoom = mWarpSelectedRoom + 1;
        if(mWarpSelectedRoom >= rPackage->mRoomsTotal) mWarpSelectedRoom = 0;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--If the room target changed...
    if(tOldRoom != mWarpSelectedRoom)
    {
        //--Compute where the map's focus currently is.
        float tWarpReposPct = EasingFunction::QuadraticOut(mWarpReposTimer, WARP_REPOSITION_TICKS);
        float tFinalX = mWarpReposXStart + ((mWarpReposXFinish - mWarpReposXStart) * tWarpReposPct);
        float tFinalY = mWarpReposYStart + ((mWarpReposYFinish - mWarpReposYStart) * tWarpReposPct);

        //--If the room changed, reset these variables so we can move the map to focus on the selected location.
        mWarpReposTimer = 0;
        mWarpReposXStart = tFinalX;
        mWarpReposYStart = tFinalY;
        mWarpReposXFinish = rPackage->mCoordX[mWarpSelectedRoom];
        mWarpReposYFinish = rPackage->mCoordY[mWarpSelectedRoom];
        ClampWarpFocusPoint(rPackage->rMapImage, mWarpReposXStart, mWarpReposYStart);
        ClampWarpFocusPoint(rPackage->rMapImage, mWarpReposXFinish, mWarpReposYFinish);
    }

    //--Warp to a location!
    if(rControlManager->IsFirstPress("Activate") && xWarpExecuteScript)
    {
        //--Clear advanced map properties back to their archive variants, if any.
        UnarchiveAdvancedMap();

        //--Execute the standard file to warp there.
        LuaManager::Fetch()->ExecuteLuaFile(xWarpExecuteScript, 1, "S", rPackage->mWarpRooms[mWarpSelectedRoom]);

        //--Close this menu.
        mWarpHandledInput = true;
        UnarchiveAdvancedMap();
        mWarpGridSelectedRegion = -1;
        mStartedHidingThisTick = true;

        //--Close the region selection menu.
        mIsWarpMode = false;
        mStartedHidingThisTick = true;
        mCurrentCursor = AM_SAVE_WARP;

        //--Hide the object.
        Hide();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Back to the main menu.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        UnarchiveAdvancedMap();
        mWarpGridSelectedRegion = -1;
        mStartedHidingThisTick = true;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }
}
void AdventureMenu::RenderWarpRegionMode()
{
    //--[Documentation and Setup]
    //--Renders a map and list of locations the player can warp to. This is done after the player has selected
    //  a region to warp to. Always renders because it may be fading out.
    if(mWarpRegionOpacityTimer < 1) return;
    if(!Images.mIsReady) return;

    //--Make sure the package in question exists.
    WarpPackage *rPackage = (WarpPackage *)mWarpList->GetElementBySlot(mWarpGridSelectedRegion);
    if(!rPackage) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mWarpRegionOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);

    //--[Position]
    //--Figure out where on the map we should focus on.
    float tWarpReposPct = EasingFunction::QuadraticOut(mWarpReposTimer, WARP_REPOSITION_TICKS);
    float tFinalX = mWarpReposXStart + ((mWarpReposXFinish - mWarpReposXStart) * tWarpReposPct);
    float tFinalY = mWarpReposYStart + ((mWarpReposYFinish - mWarpReposYStart) * tWarpReposPct);

    //--[Render]
    //--Position.
    glTranslatef(-tFinalX, -tFinalY, 0.0f);
    //Scale

    //--[Layered Maps]
    if(mMapLayerList->GetListSize() > 0)
    {
        //--Iterate.
        AdvancedMapPack *rPack = (AdvancedMapPack *)mMapLayerList->PushIterator();
        while(rPack)
        {
            //--Render if the roll is within range.
            if(mAdvancedMapLastRoll >= rPack->mRenderChanceLo && mAdvancedMapLastRoll <= rPack->mRenderChanceHi && rPack->rImage)
            {
                rPack->rImage->Draw(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.50f);
            }

            //--Next.
            rPack = (AdvancedMapPack *)mMapLayerList->AutoIterate();
        }
    }
    //--[Simplified Maps]
    else if(rPackage->rMapImage)
    {
        rPackage->rMapImage->Draw(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.50f);
    }

    //--Render a pin for each warp destination.
    float cPinOffX = -9.0f;
    float cPinOffY = -30.0f;
    for(int i = 0; i < rPackage->mRoomsTotal; i ++)
    {
        //--Positions.
        float cX = rPackage->mCoordX[i] + cPinOffX + (VIRTUAL_CANVAS_X * 0.50f);
        float cY = rPackage->mCoordY[i] + cPinOffY + (VIRTUAL_CANVAS_Y * 0.50f);

        if(i == mWarpSelectedRoom)
        {
            Images.CampfireMenu.rMapPinSelected->Draw(cX, cY);
            Images.CampfireMenu.rMainlineFont->DrawText(cX, cY - 45.0f, SUGARFONT_AUTOCENTER_X, 2.0f, rPackage->mWarpNames[i]);
        }
        else
        {
            Images.CampfireMenu.rMapPin->Draw(cX, cY);
        }
    }

    //--Clean.
    //Scale
    glTranslatef(tFinalX, tFinalY, 0.0f);

    //--[Overlay]
    if(rPackage->rMapOverlayImage) rPackage->rMapOverlayImage->Draw();

    //--[Instructions]
    float cTxtHei = Images.CampfireMenu.rMainlineFont->GetTextHeight();
    Images.CampfireMenu.rMainlineFont->DrawText(0.0f, VIRTUAL_CANVAS_Y - ((cTxtHei*2.0f) + 15.0f), 0, 1.0f, "Press 'Activate' key to warp");
    Images.CampfireMenu.rMainlineFont->DrawText(0.0f, VIRTUAL_CANVAS_Y - ((cTxtHei*1.0f) + 15.0f), 0, 1.0f, "Arrow Up and Down to select location");

    //--[Clean]
    //--Color mixer.
    StarlightColor::ClearMixer();
}
