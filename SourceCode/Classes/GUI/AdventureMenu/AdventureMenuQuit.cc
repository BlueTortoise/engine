//--Base
#include "AdventureMenu.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
#include "EasingFunctions.h"
#include "Global.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "MapManager.h"

#define FADE_TICKS 30

void AdventureMenu::SetToQuitMenu()
{
    mQuitCursor = 0;
    mCurrentMode = AM_MODE_QUIT;
    mIsTitleQuitConfirmMode = false;
    mIsFullQuitConfirmMode = false;
    mIsFadingForQuit = false;
}
bool AdventureMenu::UpdateQuitMenu(bool pCannotHandleUpdate)
{
    //--[Documentation and Setup]
    //--Standard update for the cursor. Highly limited selection on the quit menu, as expected.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mCurrentMode == AM_MODE_QUIT)
    {
        if(mQuitVisTimer < ADVMENU_SKILLS_VIS_TICKS) mQuitVisTimer ++;
    }
    //--Decrement.
    else
    {
        if(mQuitVisTimer > 0) mQuitVisTimer --;
    }

    ///--[Error Checking]
    //--If this menu is not the main object, stop the update here.
    if(mCurrentMode != AM_MODE_QUIT || pCannotHandleUpdate) return false;

    //--Fading.
    if(mIsFadingForQuit)
    {
        //--Timer.
        mQuitFadeTimer ++;

        //--Ending case.
        if(mQuitFadeTimer >= FADE_TICKS)
        {
            //--Ending: Quit to the title screen.
            if(mIsTitleQuitConfirmMode)
            {
                MapManager::Fetch()->mBackToTitle = true;
            }
            //--Ending: Quit the program.
            else
            {
                Global::Shared()->gQuit = true;
            }
        }
    }
    //--Normal:
    else if(!mIsTitleQuitConfirmMode && !mIsFullQuitConfirmMode)
    {
        //--Up...
        if(rControlManager->IsFirstPress("Up"))
        {
            if(mQuitCursor > 0)
            {
                mQuitCursor --;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Down...
        else if(rControlManager->IsFirstPress("Down"))
        {
            if(mQuitCursor < AM_QUIT_TOTAL - 1)
            {
                mQuitCursor ++;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--Pressing Activate...
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--On the cancel...
            if(mQuitCursor == AM_QUIT_CANCEL)
            {
                mCurrentMode = AM_MODE_BASE;
            }
            //--Quit to Title: Confirm.
            else if(mQuitCursor == AM_QUIT_TO_TITLE)
            {
                mQuitCursor = 0;
                mIsTitleQuitConfirmMode = true;
            }
            //--Quit Program: Confirm.
            else if(mQuitCursor == AM_QUIT_PROGRAM)
            {
                mQuitCursor = 0;
                mIsFullQuitConfirmMode = true;
            }

            //--All cases trigger SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Cancel, back to the main menu.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mCurrentMode = AM_MODE_BASE;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    //--Confirm quit to title:
    else if(mIsTitleQuitConfirmMode)
    {
        //--Up
        if(rControlManager->IsFirstPress("Up"))
        {
            if(mQuitCursor > 0)
            {
                mQuitCursor = 0;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Down...
        else if(rControlManager->IsFirstPress("Down"))
        {
            if(mQuitCursor < 1)
            {
                mQuitCursor = 1;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--Activate...
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Left decision is "No".
            if(mQuitCursor == 0)
            {
                mIsTitleQuitConfirmMode = false;
                mQuitCursor = AM_QUIT_TO_TITLE;
            }
            //--Right decision is "Yes".
            else
            {
                mIsFadingForQuit = true;
                mQuitFadeTimer = 0;
            }

            //--All cases trigger SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Cancel, back to the quit menu.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsTitleQuitConfirmMode = false;
            mQuitCursor = AM_QUIT_TO_TITLE;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    //--Confirm quit program:
    else if(mIsFullQuitConfirmMode)
    {
        //--Up
        if(rControlManager->IsFirstPress("Up"))
        {
            if(mQuitCursor > 0)
            {
                mQuitCursor = 0;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Down...
        else if(rControlManager->IsFirstPress("Down"))
        {
            if(mQuitCursor < 1)
            {
                mQuitCursor = 1;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--Activate...
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Left decision is "No".
            if(mQuitCursor == 0)
            {
                mIsFullQuitConfirmMode = false;
                mQuitCursor = AM_QUIT_PROGRAM;
            }
            //--Right decision is "Yes".
            else
            {
                mIsFadingForQuit = true;
                mQuitFadeTimer = 0;
            }

            //--All cases trigger SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Cancel, back to the quit menu.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsFullQuitConfirmMode = false;
            mQuitCursor = AM_QUIT_PROGRAM;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }

    //--Handled update.
    return true;
}
void AdventureMenu::RenderQuitMenu()
{
    //--Renders the quitting submenu. This renders over the base menu.
    if(!Images.mIsReady) return;

    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mQuitVisTimer, ADVMENU_SKILLS_VIS_TICKS);
    if(cAlpha <= 0.0f) return;

    //--Rendering Constants
    float cCenterX = VIRTUAL_CANVAS_X * 0.50f;
    float cOptionX = 400.0f;
    float cHeaderY = 216.0f;
    float cOptionY = 280.0f;
    float cTextHeight = Images.BaseMenu.rMainlineFont->GetTextHeight();

    //--Color Setup
    StarlightColor cNormalCol = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, cAlpha);
    StarlightColor cSelectCol = StarlightColor::MapRGBAF(1.0f, 0.0f, 0.0f, cAlpha);

    //--Render a box in the middle of the screen.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
    Images.BaseMenu.rQuitBacking->Draw();

    //--Normal mode:
    if(!mIsTitleQuitConfirmMode && !mIsFullQuitConfirmMode)
    {
        //--Header.
        Images.BaseMenu.rHeadingFont->DrawText(cCenterX, cHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Select an option:");

        //--Body.
        const char *cOptions[] = {"Cancel", "Quit to Title", "Quit Program"};
        for(int i = 0; i < 3; i ++)
        {
            if(mQuitCursor == i) cSelectCol.SetAsMixerAlpha(cAlpha);
            Images.BaseMenu.rMainlineFont->DrawText(cOptionX, cOptionY + (cTextHeight * i), 0, 1.0f, cOptions[i]);
            if(mQuitCursor == i) cNormalCol.SetAsMixerAlpha(cAlpha);
        }
    }
    //--Quitting back to title.
    else if(mIsTitleQuitConfirmMode)
    {
        //--Header.
        Images.BaseMenu.rHeadingFont->DrawText(cCenterX, cHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Return to title screen?");

        //--Body.
        const char *cOptions[] = {"No", "Yes"};
        for(int i = 0; i < 2; i ++)
        {
            if(mQuitCursor == i) cSelectCol.SetAsMixerAlpha(cAlpha);
            Images.BaseMenu.rMainlineFont->DrawText(cOptionX, cOptionY + (cTextHeight * i), 0, 1.0f, cOptions[i]);
            if(mQuitCursor == i) cNormalCol.SetAsMixerAlpha(cAlpha);
        }
    }
    //--Quitting the program entirely.
    else if(mIsFullQuitConfirmMode)
    {
        //--Header.
        Images.BaseMenu.rHeadingFont->DrawText(cCenterX, cHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Quit the program?");

        //--Body.
        const char *cOptions[] = {"No", "Yes"};
        for(int i = 0; i < 2; i ++)
        {
            if(mQuitCursor == i) cSelectCol.SetAsMixerAlpha(cAlpha);
            Images.BaseMenu.rMainlineFont->DrawText(cOptionX, cOptionY + (cTextHeight * i), 0, 1.0f, cOptions[i]);
            if(mQuitCursor == i) cNormalCol.SetAsMixerAlpha(cAlpha);
        }
    }

    //--If fading out...
    if(mIsFadingForQuit)
    {
        //--Compute alpha.
        float tAlpha = mQuitFadeTimer / (float)FADE_TICKS;

        //--Setup.
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.0f, 0.0f, 0.0f, tAlpha);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
    }

    //--Clean.
    StarlightColor::ClearMixer();
}
