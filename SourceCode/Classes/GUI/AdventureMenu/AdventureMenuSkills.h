//--Base
#include "AdventureMenu.h"
#include "AdvCombat.h"
#include "StarlightString.h"

#pragma once

//======================================== Definitions ============================================
//--Timers
#define ADVMENU_SKILLS_VIS_TICKS 15
#define ADVMENU_STD_MOVE_TICKS 10

//--Sezes
#define ADVMENU_SKILLS_MAX_JOBS_PER_PAGE 14
#define ADVMENU_SKILLS_JOB_SCROLL_RANGE 3
#define ADVMENU_SKILLS_MAX_SKILLS_PER_PAGE 14
#define ADVMENU_SKILLS_SKILL_SCROLL_RANGE 3
#define ADVMENU_HIGHLIGHT_INDENT 4.0f

//--Modes
#define ADVMENU_SKILLS_MODE_JOB_SELECT 0
#define ADVMENU_SKILLS_MODE_SKILL_SELECT 1
#define ADVMENU_SKILLS_MODE_SKILL_QUERY 2
#define ADVMENU_SKILLS_MODE_SKILL_REPLACE 3
#define ADVMENU_SKILLS_MODE_SKILL_CONFIRM_PURCHASE 4

//========================================= Structures ============================================
typedef struct AdvMenuSkillImages
{
    //--Fonts
    SugarFont *rHeaderFont;
    SugarFont *rJobFont;
    SugarFont *rSkillFont;
    SugarFont *rCharacterNameFont;
    SugarFont *rDescriptionHeaderFont;
    SugarFont *rDescriptionFont;
    SugarFont *rConfirmationSmall;
    SugarFont *rConfirmationBig;

    //--Images for this UI
    SugarBitmap *rClassActive;
    SugarBitmap *rClassMaster;
    SugarBitmap *rHeader;
    SugarBitmap *rHighlight;
    SugarBitmap *rPaneJobs;
    SugarBitmap *rPaneSkills;
    SugarBitmap *rScrollbarFront;
    SugarBitmap *rScrollbarJobs;
    SugarBitmap *rScrollbarSkills;
    SugarBitmap *rSkillEquipped;

    //--Images from Combat UI
    SugarBitmap *rAbilityPane;
    SugarBitmap *rAbilityFrameCatalyst[ADVCOMBAT_CATALYST_MAX];
    SugarBitmap *rAbilityFrameCustom;
    SugarBitmap *rCharacterPaneBack;
    SugarBitmap *rCharacterPaneFrame;
    SugarBitmap *rCharacterPaneMask;
    SugarBitmap *rCharacterPaneBanner;
    SugarBitmap *rDescriptionPane;
    SugarBitmap *rAbilityHighlight;

    //--Functions
    void Construct()
    {
        //--Setup.
        DataLibrary *rDataLibrary = DataLibrary::Fetch();

        //--Fonts
        rHeaderFont            = rDataLibrary->GetFont("Adventure Menu Skills Header");
        rJobFont               = rDataLibrary->GetFont("Adventure Menu Skills Job");
        rSkillFont             = rDataLibrary->GetFont("Adventure Menu Skills Skills");
        rCharacterNameFont     = rDataLibrary->GetFont("Adventure Menu Skills Character");
        rDescriptionHeaderFont = rDataLibrary->GetFont("Adventure Combat Description Header");
        rDescriptionFont       = rDataLibrary->GetFont("Adventure Combat Description");
        rConfirmationSmall     = rDataLibrary->GetFont("Adventure Menu Skills Confirmation Small");
        rConfirmationBig       = rDataLibrary->GetFont("Adventure Menu Skills Confirmation Big");

        //--Images for this UI
        rClassActive     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SkillsMenu/ClassActive");
        rClassMaster     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SkillsMenu/ClassMaster");
        rHeader          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SkillsMenu/Header");
        rHighlight       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SkillsMenu/Highlight");
        rPaneJobs        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SkillsMenu/PaneJobs");
        rPaneSkills      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SkillsMenu/PaneSkills");
        rScrollbarFront  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SkillsMenu/ScrollbarFront");
        rScrollbarJobs   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SkillsMenu/ScrollbarJobs");
        rScrollbarSkills = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SkillsMenu/ScrollbarSkills");
        rSkillEquipped   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SkillsMenu/SkillEquipped");

        //--Images from Combat UI
        rAbilityFrameCatalyst[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|AbilityFrameCatalyst1");
        rAbilityFrameCatalyst[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|AbilityFrameCatalyst2");
        rAbilityFrameCatalyst[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|AbilityFrameCatalyst3");
        rAbilityFrameCatalyst[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|AbilityFrameCatalyst4");
        rAbilityFrameCatalyst[4] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|AbilityFrameCatalyst5");
        rAbilityFrameCatalyst[5] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|AbilityFrameCatalyst6");
        rAbilityFrameCustom      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|AbilityFrameCustom");
        rAbilityPane             = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|AbilityFrameMain");
        rCharacterPaneBack       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|MainPortraitRingBack");
        rCharacterPaneFrame      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|MainPortraitRing");
        rCharacterPaneMask       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|MainPortraitMask");
        rCharacterPaneBanner     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|MainNameBanner");
        rDescriptionPane         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|Descriptionwindow");
        rAbilityHighlight        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Combat/PlayerInterface|AbilityHighlight");
    }
}AdvMenuSkillImages;

typedef struct AdvMenuSkillVars
{
    //--System
    int mVisibilityTimer;
    int mActiveCharacter;
    int mMode;

    //--Cursors
    int mJobCursor;
    int mSkillCursor;
    int mCombatPaneCursorX;
    int mCombatPaneCursorY;

    //--Cursor Movement
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;

    //--Cursor Storage
    bool mGoBackToJob;
    bool mShowJobHighlight;
    bool mShowAbilityHighlight;
    int mStopShowingJobHighlightTimer;
    int mStopShowingAbilityHighlightTimer;
    TwoDimensionReal mJobHightlightStore;
    TwoDimensionReal mAbilityHightlightStore;

    //--Scrollbars
    int mScrollOffJob;
    int mScrollOffSkill;

    //--Strings
    StarlightString *mAcceptString;
    StarlightString *mCancelString;

    //--Functions
    void Initialize()
    {
        //--System
        mVisibilityTimer = 0;
        mActiveCharacter = 0;
        mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;

        //--Cursors
        mJobCursor = 0;
        mSkillCursor = -1;
        mCombatPaneCursorX = -1;
        mCombatPaneCursorY = -1;

        //--Cursor Movement
        mHighlightPos.Initialize();
        mHighlightSize.Initialize();

        //--Cursor Storage
        mGoBackToJob = false;
        mShowJobHighlight = false;
        mShowAbilityHighlight = false;
        mStopShowingJobHighlightTimer = 0;
        mStopShowingAbilityHighlightTimer = 0;
        mJobHightlightStore.SetWH(0.0f, 0.0f, 1.0f, 1.0f);
        mAbilityHightlightStore.SetWH(0.0f, 0.0f, 1.0f, 1.0f);

        //--Scrollbars
        mScrollOffJob = 0;
        mScrollOffSkill = 0;

        //--Strings.
        mAcceptString = new StarlightString();
        mCancelString = new StarlightString();
    }
    void Deallocate()
    {
        delete mAcceptString;
        delete mCancelString;
    }
}AdvMenuSkillVars;
