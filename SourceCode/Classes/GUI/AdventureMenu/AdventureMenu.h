//--[AdventureMenu]
//--Menu used in Adventure Mode. Has sub-menus for things like status, party arrangement, equipment, and inventory.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"
#include "GridHandler.h"
#include "AdvCombatDefStruct.h"
#include "AdventureMenuMain.h"
#include "AdventureMenuSkills.h"
#include "AdventureMenuFieldAbilities.h"
#include "AdventureInventory.h"

//--[Local Structures]
//--Forward Declaration
#ifndef _ADAM_TYPES_
#define _ADAM_TYPES_
#define CRAFT_ADAMANTITE_POWDER 0
#define CRAFT_ADAMANTITE_FLAKES 1
#define CRAFT_ADAMANTITE_SHARD 2
#define CRAFT_ADAMANTITE_PIECE 3
#define CRAFT_ADAMANTITE_CHUNK 4
#define CRAFT_ADAMANTITE_ORE 5
#define CRAFT_ADAMANTITE_TOTAL 6
#endif

//--Local Definitions
#define AM_OPACITY_TICKS 15

//--Structure Declarations
struct LoadingPack;

//--[Local Definitions]
//--Modes
#define AM_MODE_BASE 0
#define AM_MODE_ITEMS 1
#define AM_MODE_EQUIP 2
#define AM_MODE_STATUS 3
#define AM_MODE_SKILLS 4
#define AM_MODE_MAP 5
#define AM_MODE_OPTIONS 6
#define AM_MODE_QUIT 7
#define AM_MODE_HEAL 8
#define AM_MODE_TOTAL 9

//--Stencil Codes
#define AM_STENCIL_MAIN 1
#define AM_STENCIL_STATUS 10
#define AM_STENCIL_EQUIP 20
#define AM_STENCIL_SKILLS 30

//--Item Submenu codes
#define AM_ITEM_USE 0
#define AM_ITEM_DROP 1

//--Save Menu codes
#define AM_SAVE_REST 0
#define AM_SAVE_CHAT 1
#define AM_SAVE_SAVE 2
#define AM_SAVE_COSTUMES 3
#define AM_SAVE_SKILLBOOKS 4
#define AM_SAVE_FORM 5
#define AM_SAVE_WARP 6
#define AM_SAVE_RELIVE 7
#define AM_SAVE_PASSWORD 8
#define AM_SAVE_TOTAL 9

//--Options Menu codes
#define AM_OPTIONS_MUSICVOL 0
#define AM_OPTIONS_SOUNDVOL 1
#define AM_OPTIONS_AUTODOCTORBAG 2
#define AM_OPTIONS_AUTOHASTEN 3
#define AM_OPTIONS_TOURISTMODE 4
#define AM_OPTIONS_AMBIENTLIGHT 5
#define AM_OPTIONS_MEMORYCURSOR 6
#define AM_OPTIONS_CONTROLREBIND 7
#define AM_OPTIONS_OKAY 8
#define AM_OPTIONS_DEFAULTS 9
#define AM_OPTIONS_TOTAL 10

//--Quit Menu codes
#define AM_QUIT_CANCEL 0
#define AM_QUIT_TO_TITLE 1
#define AM_QUIT_PROGRAM 2
#define AM_QUIT_TOTAL 3

//--Sizing
#define AM_STD_INDENT 12.0f
#define AM_CHARACTER_CARD_X 192.0f
#define AM_CHARACTER_CARD_Y 192.0f
#define AM_ITEMS_PER_PAGE 16
#define AM_GEMS_PER_PAGE 24
#define AM_SHOP_ITEMS_PER_PAGE 17

//--Shop Types
#define AM_SELECT_SHOP_MODE 0
#define AM_BUY 1
#define AM_SELL 2
#define AM_BUYBACK 3
#define AM_GEMCUTTER 4

//--Cursor for Mode Selection in Shops
#define AM_SHOP_SELECT_MIN 0
#define AM_SHOP_SELECT_BUY 0
#define AM_SHOP_SELECT_SELL 1
#define AM_SHOP_SELECT_BUYBACK 2
#define AM_SHOP_SELECT_GEMCUTTER 3
#define AM_SHOP_SELECT_MAX 3

//--Save Selection
#define AM_SCAN_TOTAL 100
#define AM_FILES_PER_PAGE 5

//--Ability Inspector
#define AM_ABILITY_MODE_TICKS 10.0f

//--Map Zooming
#define AM_MAP_ZOOM_TICKS 15

//--Inventory Image Headers
#define AM_INV_HEADER_HP 0
#define AM_INV_HEADER_ATK 1
#define AM_INV_HEADER_INI 2
#define AM_INV_HEADER_ACC 3
#define AM_INV_HEADER_EVD 4
#define AM_INV_HEADER_PRT 5
#define AM_INV_HEADER_SLASHING 6
#define AM_INV_HEADER_STRIKING 7
#define AM_INV_HEADER_PIERCING 8
#define AM_INV_HEADER_FLAMING 9
#define AM_INV_HEADER_FREEZING 10
#define AM_INV_HEADER_SHOCKING 11
#define AM_INV_HEADER_CRUSADING 12
#define AM_INV_HEADER_OBSCURING 13
#define AM_INV_HEADER_BLEEDING 14
#define AM_INV_HEADER_POISONING 15
#define AM_INV_HEADER_CORRODING 16
#define AM_INV_HEADER_TERRIFYING 17
#define AM_INV_HEADER_TOTAL 18

#define AM_INV_NAME_REMAP_MAXLEN 4

//--[Advanced Map Pack]
//--Contains an image layer when using multi-layered images, as well as a chance for that
//  image to appear. This creates the flashing effect.
typedef struct AdvancedMapPack
{
    SugarBitmap *rImage;
    int mRenderChanceLo;
    int mRenderChanceHi;
}AdvancedMapPack;

//--[Costume Heading Pack]
//--Contains a character's name, image, form, and which sublist contains their costume packages.
typedef struct CostumeHeadingPack
{
    SugarBitmap *rImage;
    char mCharacterName[STD_MAX_LETTERS];
    char mFormName[STD_MAX_LETTERS];
    int mSublistIndex;
}CostumeHeadingPack;

//--[Costume Pack]
//--Contains a costume entry for a character in a given form.
typedef struct CostumePack
{
    bool mIsSelected;
    SugarBitmap *rImage;
    char mExecPath[STD_PATH_LEN];
}CostumePack;

//--[Stat Compare Pack]
//--An image and an index in the STATS_X series of stats to compare.
typedef struct StatComparePack
{
    SugarBitmap *rImage;
    int mIndex;
    void Set(int pIndex, SugarBitmap *pImage)
    {
        mIndex = pIndex;
        rImage = pImage;
    }
}StatComparePack;

//--[Shop Inventory Pack]
//--A package containing an item to be purchased in a shop.
#include "AdventureItem.h"
typedef struct ShopInventoryPack
{
    AdventureItem *mItem;
    int mPriceOverride;
    int mQuantity;
    void Initialize()
    {
        mItem = NULL;
        mPriceOverride = -1;
        mQuantity = 0;
    }
    static void DeleteThis(void *pPtr)
    {
        ShopInventoryPack *rPtr = (ShopInventoryPack *)pPtr;
        delete rPtr->mItem;
        free(rPtr);
    }
}ShopInventoryPack;

//--Values.
#define AM_STAT_COMPARE_PACK_HPMAX 0
#define AM_STAT_COMPARE_PACK_ATTACK 1
#define AM_STAT_COMPARE_PACK_ACCURACY 2
#define AM_STAT_COMPARE_PACK_EVADE 3
#define AM_STAT_COMPARE_PACK_INITIATIVE 4
#define AM_STAT_COMPARE_PACK_PROTECTION 5
#define AM_STAT_COMPARE_PACK_RES_SLASH 6
#define AM_STAT_COMPARE_PACK_RES_STRIKE 7
#define AM_STAT_COMPARE_PACK_RES_PIERCE 8
#define AM_STAT_COMPARE_PACK_RES_FLAME 9
#define AM_STAT_COMPARE_PACK_RES_FREEZE 10
#define AM_STAT_COMPARE_PACK_RES_SHOCK 11
#define AM_STAT_COMPARE_PACK_RES_CRUSADE 12
#define AM_STAT_COMPARE_PACK_RES_OBSCURE 13
#define AM_STAT_COMPARE_PACK_RES_BLEED 14
#define AM_STAT_COMPARE_PACK_RES_POISON 15
#define AM_STAT_COMPARE_PACK_RES_CORRODE 16
#define AM_STAT_COMPARE_PACK_RES_TERRIFY 17
#define AM_STAT_COMPARE_PACKS_TOTAL 18

//--[Grid Entry]
//--Grid Opacity Values
#define AM_CAMPFIRE_OPACITY_TICKS 10
#define AM_CAMPFIRE_SIZE_TICKS 10
#define AM_CAMPFIRE_SIZE_BOOST 1.50f

//--Grid Positioning Values
#define AM_GRID_CENT_X (VIRTUAL_CANVAS_X * 0.50f)
#define AM_GRID_CENT_Y (VIRTUAL_CANVAS_Y * 0.40f)
#define AM_GRID_SPAC_X (VIRTUAL_CANVAS_X * 0.10f)
#define AM_GRID_SPAC_Y (VIRTUAL_CANVAS_X * 0.10f)

//--[Relive Package]
//--Contains a path and an image, used for the Relive submenu.
typedef struct RelivePackage
{
    //--Members
    char mScriptPath[STD_PATH_LEN];
    SugarBitmap *rImage;

    //--Functions
    void Initialize()
    {
        memset(mScriptPath, 0, sizeof(char) * STD_PATH_LEN);
        rImage = NULL;
    }
}RelivePackage;

//--[Warp Package]
//--Constains a map destination, an image, and a set of coordinates. Used by the Warp submenu.
#define WARP_MAX_PER_REGION 32
typedef struct WarpPackage
{
    //--Members
    int mRoomsTotal;
    char mWarpNames[WARP_MAX_PER_REGION][STD_MAX_LETTERS];
    char mWarpRooms[WARP_MAX_PER_REGION][STD_MAX_LETTERS];
    float mCoordX[WARP_MAX_PER_REGION];
    float mCoordY[WARP_MAX_PER_REGION];
    SugarBitmap *rMapImage;
    SugarBitmap *rMapOverlayImage;
    char mCallBuffer[STD_MAX_LETTERS];

    //--Functions
    void Initialize()
    {
        mRoomsTotal = 0;
        memset(mWarpNames, 0, sizeof(char) * WARP_MAX_PER_REGION * STD_MAX_LETTERS);
        memset(mWarpRooms, 0, sizeof(char) * WARP_MAX_PER_REGION * STD_MAX_LETTERS);
        memset(mCoordX, 0, sizeof(float) * WARP_MAX_PER_REGION);
        memset(mCoordY, 0, sizeof(float) * WARP_MAX_PER_REGION);
        memset(mCallBuffer, 0, sizeof(char) * STD_MAX_LETTERS);
        rMapImage = NULL;
        rMapOverlayImage = NULL;
    }
}WarpPackage;

//--[Classes]
class AdventureMenu : public RootObject
{
    private:
    //--System
    bool mStartedHidingThisTick;
    bool mHandledUpdate;
    int mMainOpacityTimer;
    bool mIsVisible;
    bool mIsCampfireMode;
    uint8_t mCurrentMode;

    //--Common
    int mCurrentCursor;
    float mMenuWidest;
    float mMenuWidestSecond;

    //--Timers
    int mHealVisTimer;

    //--Main Menu
    TwoDimensionReal mMainDimensions;
    TwoDimensionReal mCraftingDimensions;
    TwoDimensionReal mPlatinaDimensions;
    TwoDimensionReal mCatalystDimensions;
    TwoDimensionReal mPartyBoardDimensions;

    //--Main Grid Menu
    bool mIsMainHelpVisible;
    int mMainHelpTimer;
    StarlightString *mShowHelpString;
    AdvMenuBaseVars mBaseVars;

    //--Equipment Menu
    int mEquipVisibilityTimer;
    bool mRebuildModifyList;
    int mCharacterIndex;
    bool mIsModifying;
    int mModifySlot;
    TwoDimensionReal mEquipDimensions;
    TwoDimensionReal mEquipListDimensions;
    SugarLinkedList *mValidEquipsList;
    int mEquipMinimumRender;
    int mGemMinimumRender;
    bool mIsSocketEditingMode;
    int mSocketEditTimer;
    int mSocketEditSlot;
    int mSocketQuerySlot;

    //--Items Menu
    int mItemVisTimer;
    int mInventorySkip;
    bool mIsItemSubMenuOpen;
    int mItemSubCursor;
    TwoDimensionReal mItemDimensions;
    TwoDimensionReal mItemPropertyDimensions;

    //--[Saving / File Selection]
    //--System
    bool mIsBenchMode;
    bool mIsEditingNoteForNewSave;
    bool mIsSelectFileMode;
    int mEmptySaveSlotsTotal;
    int mSaveOpacityTimer;

    //--Selection and Navigation
    int mSelectFileCursor;
    int mSelectFileLastColumn;
    int mSelectFileOffset;
    int mHighestCursor;
    int mEditingFileCursor;
    int mArrowBobTimer;
    int mArrowOpacityTimerLft;
    int mArrowOpacityTimerRgt;

    //--Page Moving
    bool mIsPageMoving;
    bool mIsPageMovingUp;
    int mPageMoveTimer;

    //--File Information Storage
    int mCurrentSortingType;
    int mFileListingIndex[AM_SCAN_TOTAL];
    char mFileListing[AM_SCAN_TOTAL][80];
    LoadingPack *mLoadingPacks[AM_SCAN_TOTAL];
    bool mExistingFileListing[AM_SCAN_TOTAL];

    //--Entering Notes
    bool mIsEnteringNotes;
    int mNoteSlot;
    StringEntry *mStringEntryForm;

    //--Deleting a Save
    int mSaveDeletionIndex;
    int mSaveDeletionIndexOld;
    int mSaveDeletionOpacityTimer;

    //--[Forms Submenu]
    //--System
    bool mIsFormsMode;
    int mFormsPartyMember;

    //--Character Selection.
    int mFormsOpacityTimer;
    int mFormsGridCurrent;
    SugarLinkedList *mFormsPartyList;    //dummy, ref, parallel with mFormsPartyGrid
    SugarLinkedList *mFormsPartyGrid;    //GridPackage *, master
    SugarLinkedList *mFormsPartyGridImg; //SugarBitmap *, ref, parallel with mFormsGrid

    //--Form Selection.
    int mFormSubOpacityTimer;
    int mFormSubGridCurrent;
    AdvCombatEntity *rFormCharacter;
    SugarLinkedList *mFormSubGrid;         //GridPackage *, master
    SugarLinkedList *mFormSubPaths;        //char *, master, parallel with mFormSubGrid
    SugarLinkedList *mFormSubJobScripts;   //char *, master, parallel with mFormSubGrid
    SugarLinkedList *mFormSubGridImg;      //SugarBitmap *, ref, parallel with mFormSubGrid
    SugarLinkedList *mFormSubComparisons;  //CombatStatistics *, master, parallel with mFormSubGrid
    StatComparePack mStatComparePacks[AM_STAT_COMPARE_PACKS_TOTAL];

    //--[Other Submenus]
    //--Gemcutter Menu
    bool mIsGemcutterMode;
    int mGemcutterCursor;
    int mSelectedGemCursor;
    bool mGemConfirmCutMode;

    //--Status Menu
    int mStatusTimer;
    int mStatusCursor;
    TwoDimensionReal mStatusBacking;

    //--Shop Menu
    bool mIsShopMode;
    bool mShopCanGemcut;
    char mShopName[STD_MAX_LETTERS];
    int mShopModeType;
    int mShopCursor;
    int mShopSkip;
    int mShopSkipStore;
    bool mIsExchanging;
    int mPurchaseQuantity;
    char *mShopTeardownPath;
    int mComparisonCharacter;
    int mComparisonSlot;
    int mCharacterWalkTimer;
    SugarLinkedList *mShopInventory; //ShopInventoryPack *, master
    SugarLinkedList *mDescriptionLines;
    StarlightString *mChangeCharString;
    StarlightString *mChangeItemString;

    //--Shop Fading
    bool mIsShopFading;
    int mShopFadeMode;
    int mShopFadeTimer;

    //--Options Menu
    int mOptionsVisibilityTimer;
    int mOptionsCursor;
    TwoDimensionReal mOptionsBacking;
    float mOptionOldMusic;
    float mOptionOldSound;
    bool mOptionOldDoctor;
    bool mOptionOldTourist;
    float mOptionOldAmbientLight;
    bool mOptionOldMemoryCursor;

    //--Localized FlexMenu for Rebinding
    bool mFlexMenuIsRebinding;
    FlexMenu *mLocalizedRebindMenu;

    //--Quit Menu
    int mQuitVisTimer;
    int mQuitCursor;
    bool mIsTitleQuitConfirmMode;
    bool mIsFullQuitConfirmMode;
    bool mIsFadingForQuit;
    int mQuitFadeTimer;

    //--Map Mode
    bool mIsEnteringMapMode;
    bool mHasRendered;
    int mMapTimer;
    int mMapFadeTimer;
    int mMapFocusPointX;
    int mMapFocusPointY;
    float mMapZoom;
    float mMapZoomStart;
    float mMapZoomDest;
    int mMapZoomTimer;
    SugarBitmap *rActiveMap;
    SugarBitmap *rActiveMapOverlay;
    int mPlayerMapPositionX;
    int mPlayerMapPositionY;
    float mMapScrollTimerX;
    float mMapScrollTimerY;

    //--Advanced Map Mode
    int mAdvancedMapLastRoll;
    int mAdvancedMapReroll;
    float mAdvancedMapRemapX;
    float mAdvancedMapRemapY;
    float mAdvancedMapScaleX;
    float mAdvancedMapScaleY;
    SugarLinkedList *mMapLayerList; //AdvancedMapPack *, master

    //--Advanced Map Archive
    int mArchiveAdvancedMapLastRoll;
    int mArchiveAdvancedMapReroll;
    float mArchiveAdvancedMapRemapX;
    float mArchiveAdvancedMapRemapY;
    float mArchiveAdvancedMapScaleX;
    float mArchiveAdvancedMapScaleY;
    SugarLinkedList *mArchiveMapLayerList; //AdvancedMapPack *, master

    //--Campfire Grid Menu
    int mCampfireGridOpacityTimer;
    int mCampfireGridCurrent;
    GridPackage mCampfireGrid[AM_SAVE_TOTAL];

    //--[Party Chatting]
    //--System
    bool mIsChatMode;
    int mChatGridCurrent;
    int mChatGridOpacityTimer;
    SugarLinkedList *mChatGrid;    //GridPackage *, master
    SugarLinkedList *mChatGridImg; //SugarBitmap *, ref,    parallel with mChatGrid
    SugarLinkedList *mChatListing; //char *,        master, parallel with mChatGrid

    //--[Costumes]
    //--System
    bool mIsCostumesMenu;
    int mCostumeGridCurrent;
    int mCostumeGridOpacityTimer;
    SugarLinkedList *mCostumeGrid;         //GridPackage *, master
    SugarLinkedList *mCostumeGridHeaders;  //CostumeHeadingPack *, master, parallel mCostumeGrid
    SugarLinkedList *mCostumeGridSublists; //SugarLinkedList *, master, parallel with mCostumeGridHeaders -> contains CostumePack *, master.

    //--Costume Selection
    bool mIsCostumesSubMenu;
    int mCostumeCharacterIndex;
    int mCostumeCharacterCurrent;
    int mCostumeCharacterOpacityTimer;
    SugarLinkedList *mCostumeCharacterGrid; //GridPackage *, master

    //--[Skillbook Menu]
    bool mIsSkillbookMode;
    int mSkillbookCursor;
    SugarLinkedList *mSkillbookList; //char *, master

    ///--[Skills Menu]
    AdvMenuSkillVars mSkillMenu;

    ///--[Field Abilities Menu]
    AdvMenuFieldAbilityVars mFieldAbilityMenu;

    //--[Relive Menu]
    bool mIsReliveMode;
    int mReliveGridOpacityTimer;
    int mReliveGridCurrent;
    SugarLinkedList *mReliveList; //RelivePackage *, master
    SugarLinkedList *mReliveGrid; //GridPackage *, master

    //--[Warp Menu]
    bool mIsWarpMode;
    float mWarpZoomFactor;
    bool mWarpHandledInput;
    int mWarpGridCurrent;
    int mWarpGridOpacityTimer;
    int mWarpGridSelectedRegion;
    int mWarpSelectedRoom;
    int mWarpRegionOpacityTimer;
    int mWarpReposTimer;
    float mWarpReposXStart;
    float mWarpReposYStart;
    float mWarpReposXFinish;
    float mWarpReposYFinish;
    SugarLinkedList *mWarpList; //WarpPackage *, master
    SugarLinkedList *mWarpGrid; //GridPackage *, master
    SugarLinkedList *mWarpGridImg; //SugarBitmap *, ref, parallel with mWarpGrid
    SugarLinkedList *mWarpRegionImages; //SugarBitmap *, ref

    ///--[Campfire Password Entry]
    //--System
    bool mIsPasswordEntry;
    int mPasswordOpacity;

    ///--[Help Menus]
    AdvHelp *mMainMenuHelp;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--General
            SugarBitmap *rBorderCard;
            SugarFont *rMenuFont;
        }Data;
        AdvMenuBaseImages BaseMenu;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rBaseBot;
            SugarBitmap *rBaseMid;
            SugarBitmap *rBaseTop;
            SugarBitmap *rButton;
            SugarBitmap *rDescriptionBox;
            SugarBitmap *rHeader;
            SugarBitmap *rComparison;

            //--Map Pin. Used by Warp menu.
            SugarBitmap *rMapPin;
            SugarBitmap *rMapPinSelected;

            //--Icons
            SugarBitmap *rIconFrame;
            SugarBitmap *rIconFrameLarge;
            SugarBitmap *rIcons[AM_SAVE_TOTAL];
        }CampfireMenu;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rVSmallFont;

            //--Images
            SugarBitmap *rFrameLower;
            SugarBitmap *rFrameLowerActive;
            SugarBitmap *rFrameUpper;
            SugarBitmap *rHeader;
            SugarBitmap *rSelectorLower;
            SugarBitmap *rSelectorUpper;
        }CostumeUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;
            SugarFont *rVerySmallFont;

            //--Images
            SugarBitmap *rBackButton;
            SugarBitmap *rBarFill;
            SugarBitmap *rBarFrame;
            SugarBitmap *rBarHeader;
            SugarBitmap *rHealAllBtn;
            SugarBitmap *rMenuBack;
            SugarBitmap *rMenuHeaderA;
            SugarBitmap *rMenuHeaderB;
            SugarBitmap *rMenuInset;
            SugarBitmap *rOptionsDeactivated;
            SugarBitmap *rPortraitBtn;
            SugarBitmap *rPortraitMask;
        }DoctorUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rFrames;
            SugarBitmap *rNameplate;
            SugarBitmap *rPortraitMaskA;
            SugarBitmap *rPortraitMaskB;
            SugarBitmap *rPortraitMaskC;
            SugarBitmap *rUnequipIcon;
            SugarBitmap *rSocketFrameA;
            SugarBitmap *rSocketFrameB;
            SugarBitmap *rSocketFrameC;
            SugarBitmap *rGemEmpty22px;
            SugarBitmap *rFrameScroll;
            SugarBitmap *rSocketScroll;
            SugarBitmap *rScrollbarFront;
        }EquipmentUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rArrowLft;
            SugarBitmap *rArrowRgt;
            SugarBitmap *rHeader;
            SugarBitmap *rNewFileButton;
            SugarBitmap *rGridBackingCh0;
        }FileSelectUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rBanner;
            SugarBitmap *rBase;
            SugarBitmap *rButtonBack;
            SugarBitmap *rGemSelectBack;
            SugarBitmap *rGemSelectFrame;
            SugarBitmap *rInfoBox;
            SugarBitmap *rPlatinaBanner;
        }GemcutterUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rFrames;
            SugarBitmap *rScrollbarBack;
            SugarBitmap *rScrollbarFront;
        }InventoryUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rBars;
            SugarBitmap *rConfirmButtons;
            SugarBitmap *rHeader;
            SugarBitmap *rMenuBack;
            SugarBitmap *rMenuInsert;
            SugarBitmap *rSliderButtons;
            SugarBitmap *rSliders;
            SugarBitmap *rTextBoxes;
        }OptionsUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;
            SugarFont *rVerySmallFont;
            SugarFont *rResistanceFont;
            SugarFont *rHealthFont;
            SugarFont *rStatisticFont;
            SugarFont *rEquipmentFont;

            //--Images
            SugarBitmap *rAbilityCursor;
            SugarBitmap *rAbilityInspector;
            SugarBitmap *rBackgroundFill;
            SugarBitmap *rBannerTop;
            SugarBitmap *rBtnBack;
            SugarBitmap *rCharInfo;
            SugarBitmap *rCharOverlayMask;
            SugarBitmap *rCharOverlayMaskLft;
            SugarBitmap *rCharOverlayMaskRgt;
            SugarBitmap *rEXPBarFill;
            SugarBitmap *rEXPBarFrame;
            SugarBitmap *rHealthBarFill;
            SugarBitmap *rHealthBarFrame;
            SugarBitmap *rHealthBarUnder;
            SugarBitmap *rInventory;
            SugarBitmap *rNavButtons;
            SugarBitmap *rStatIndicatorAtk;
            SugarBitmap *rStatIndicatorPrt;
            SugarBitmap *rStatIndicatorIni;
            SugarBitmap *rStatIndicatorAcc;
            SugarBitmap *rStatIndicatorEvd;
            SugarBitmap *rDamageTypeIcon[ADVC_DAMAGE_TOTAL];
        }StatusUI;
        AdvMenuSkillImages SkillsUI;
        AdvMenuFieldAbilityImages FieldAbilityUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rConfirmOverlay;
            SugarBitmap *rFramesBuy;
            SugarBitmap *rFramesSell;
            SugarBitmap *rFramesBuyback;
            SugarBitmap *rFramesGemcutter;
            SugarBitmap *rGemEmpty22px;
            SugarBitmap *rCompareCursor;
            SugarBitmap *rScrollbarBack;
            SugarBitmap *rScrollbarFront;
        }VendorUI;

        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;
        }FormUI;
    }Images;

    protected:

    public:
    //--System
    AdventureMenu();
    virtual ~AdventureMenu();

    //--Public Variables
    static bool xIsSkillbookMenu;
    static char *xFormResolveScript;
    static char *xFormPartyResolveScript;
    static char *xChatResolveScript;
    static char *xRestResolveScript;
    static char *xReliveResolveScript;
    static char *xCostumeResolveScript;
    static char *xFieldAbilityResolveScript;
    static char *xWarpResolveScript;
    static char *xWarpExecuteScript;
    static int xCatalystTotal;
    static int xLastSavefileSortingType;
    static bool xHandledPassword;

    //--Text Display Remaps
    static int xTextImgRemapsTotal;
    static char **xTextImgRemap; //Name that appears in the text
    static char **xTextImgPath;  //Path that the name maps to

    //--Property Queries
    bool StartedHidingThisTick();
    bool IsVisible();
    bool NeedsToBlockControls();
    bool NeedsToRender();

    //--Manipulators
    void Show();
    void Hide();
    void SetSaveMode(bool pFlag);
    void SetBenchMode(bool pFlag);

    //--Core Methods
    //--Chat Menu
    void ActivateChatMenu();
    void DeactivateChatMenu();
    void AddChatEntity(const char *pName, const char *pPath, const char *pImgPath);
    void UpdateChatMenu();
    void RenderChatMenu();

    //--Costumes
    void ActivateCostumesMenu();
    void DeactivateCostumesMenu();
    void AddCostumeHeading(const char *pInternalName, const char *pCharName, const char *pFormName, const char *pImagePath);
    void AddCostumeEntry(const char *pHeadingName, const char *pCostumeName, const char *pImagePath, const char *pScriptPath);
    void SetActiveCostumeEntry(const char *pHeadingName, const char *pCostumeName);
    void ActivateCostumeSubMenu(int pIndex);
    void UpdateCostumeMenu();
    void UpdateCostumeMenuSub();
    void RenderCostumeMenu();
    void RenderCostumeMenuSub();

    //--Equipment Menu
    void SetToEquipMenu();
    void RefreshEquipMenuHelp();
    void BuildSlotList(int pCharacterSlot, const char *pSlotType);
    bool UpdateEquipMenu(bool pCannotHandleUpdate);
    void RenderEquipMenu();
    void RenderEquipmentDescription(AdventureItem *pDescriptionItem);
    void RenderEquipmentComparison(AdventureItem *pDescriptionItem, AdventureItem *pComparisonItem);
    void RenderGemComparison(AdventureItem *pDescriptionGem, AdventureItem *pComparisonGem);

    //--Equipment Socketing
    void LoadEquipMenu();
    void ActivateSocketMode();
    void DeactivateSocketMode();
    void UpdateSocketMode();
    void RenderSocketMode();

    ///--Field Abilities
    void OpenFieldAbilities();
    void CloseFieldAbilities();
    void RegisterFieldAbility(const char *pDLPath);
    void UpdateFieldAbilities();
    void RenderFieldAbilities();

    //--File Selection
    void ScanExistingFiles();
    int GetArraySlotFromLookups(int pLookup);
    void SortFilesBySlot();
    void SortFilesBySlotRev();
    void SortFilesByDate();
    void SortFilesByDateRev();
    void UpdateFileSelection();
    void RenderFileSelection();
    void RenderFileBlock(int pIndex, float pLft, float pTop, bool pIsHighlighted, float pOpacityPct);

    //--Forms Menu
    void ActivateFormsMenu();
    void ActivateFormsSubMenu(int pPartyIndex);
    void DeactivateFormsMenu();
    void RegisterFormPartyMember(const char *pCharacterName, const char *pImgPath);
    void RegisterForm(const char *pFormName, const char *pFormPath, const char *pJobScriptPath, const char *pImgPath);
    void UpdateFormsMenu();
    void UpdateFormsSubMenu();
    void RenderFormsMenu();
    void RenderFormsSubMenu();

    //--Healing Menu
    void SetToHealMode();
    void RefreshHealMenuHelp();
    bool UpdateHealMode(bool pCannotHandleUpdate);
    void RenderHealMode();

    //--Items Menu
    void SetToItemsMenu();
    void RefreshItemMenuHelp();
    bool UpdateItemsMenu(bool pCannotHandleUpdate);
    void RenderItemsMenu();
    void RenderItemDescription(float pYPos, AdventureItem *pDescriptionItem);

    //--Gemcutter Mode
    void InitializeGemcutterMode();
    int ComputeGemMergeCost(int pRank, int pAdamantiteType);
    void UpdateGemcutterMode();
    void RenderGemcutterMode(bool pIsActiveMode, float pColorMix);
    void RenderGemInfo(float pX, float pY, bool pRenderRank, bool pCanBeUpgraded, bool pIsHighlighted, float pMixer, AdventureItem *pGem);
    void RenderMergeCost(AdventureItem *pSourceGem, AdventureItem *pMergeGem, float pColorMix);

    //--Status Menu
    void LoadStatusMenu();
    void SetStatusCharacter(int pIndex);
    void RefreshStatusMenuHelp();
    bool UpdateStatusMenu(bool pCannotHandleUpdate);
    void RenderStatusMenu();

    //--Main Menu
    void SetToMainMenu();
    void RefreshMainMenuHelp();
    bool UpdateMainMenu(bool pCannotHandleUpdate);
    void RenderMainMenu();

    //--Map Mode
    void InitializeMapMode();
    void RefreshMapMenuHelp();
    void SetMapInfo(const char *pDLPath, const char *pDLPathOverlay, int pPlayerX, int pPlayerY);
    void RepositionMap(int pX, int pY);
    void AddAdvancedLayer(const char *pDLPath, int pRenderLo, int pRenderHi);
    void SetAdvancedRemaps(float pX, float pY, float pScaleX, float pScaleY);
    void ArchiveAdvancedMap();
    void UnarchiveAdvancedMap();
    void UpdateMapMode();
    void RenderMapMode();

    //--Options Mode
    void SetToOptionsMode();
    void HandleOptionChange(int pCursor, bool pIsLeftPress);
    bool UpdateOptionsMode(bool pCannotHandleUpdate);
    void RenderOptionsMode();

    //--Password Mode
    void SetToPasswordMode();
    void UpdatePasswordMode();
    void RenderPasswordMode();

    //--Quit Menu
    void SetToQuitMenu();
    bool UpdateQuitMenu(bool pCannotHandleUpdate);
    void RenderQuitMenu();

    //--Relive Menu
    void SetToReliveMode();
    void ClearReliveList();
    void RegisterReliveScript(const char *pDisplayName, const char *pScriptPath, const char *pImagePath);
    void UpdateReliveMode();
    void RenderReliveMode();

    //--Campfire Mode
    void InitializeCampfireVariables();
    void BuildCampfirePositions();
    void ExecuteRest();
    void UpdateCampfireMenu();
    void RenderCampfireMenu();
    float RenderCampfireMenuBacking(int pMaxOptions, bool pRenderDescriptionBox);

    //--Shops Mode
    void InitializeShopMode(const char *pShopName, const char *pPath, const char *pTeardownPath);
    void RegisterItem(const char *pName, int pPriceOverride, int pQuantity);
    void AssembleDescriptionLines(const char *pDescriptionLine);
    void UpdateShopMenu();
    void RenderShopMenu();

    //--Shop Subfile: Buy
    void UpdateShopBuy();
    void RenderShopBuy(bool pIsActiveMode, float pColorMix);
    void RenderShopBuyProperties(AdventureItem *pItem, int pComparisonCharacter, float pColorMix);
    void RenderShopStatComparison(bool pDontRenderSecondColumn, CombatStatistics *pPackA, CombatStatistics *pPackB);
    void RenderShopBuyComparison(AdvCombatEntity *pCharacter, bool pIgnoreGems, AdventureItem *pOldItem, AdventureItem *pNewItem);
    void RenderShopGemProperties(bool pDontRenderModification, AdventureItem *pCurGem, AdventureItem *pNewGem);

    //--Shop Subfile: Sell
    void UpdateShopSell();
    void RenderShopSell(bool pIsActiveMode, float pColorMix);

    //--Shop Subfile: Buyback
    void UpdateShopBuyback();
    void RenderShopBuyback(bool pIsActiveMode, float pColorMix);

    //--Skillbooks Mode
    void SetToSkillbookMode();
    void RegisterSkillbookEntry(const char *pDisplayName, const char *pScriptPath);
    void OpenSkillbooksTo(const char *pCharacterName, const char *pScriptPath);
    void UpdateSkillbookMode();
    void RenderSkillbookMode();

    //--Skills UI
    void SetToSkillsMode();
    void RefreshSkillMenuHelp();
    bool IsCustomizableSlot(int pX, int pY);
    void RecomputeCursorPos(int pTicks);
    void RecomputeJobOffset();
    void RecomputeSkillOffset();
    bool UpdateSkillsMode(bool pCannotHandleUpdate);
    void RenderSkillsMode();

    //--Warp Menu
    void SetToWarpMode();
    void RegisterWarpRegionImage(const char *pName, const char *pImagePath);
    void ClearWarpRegionImages();
    void RegisterWarpDestination(const char *pRegionName, const char *pDisplayName, const char *pActualName, const char *pImgName, const char *pOverlayName, float pImgX, float pImgY);
    void ClampWarpFocusPoint(SugarBitmap *pImage, float &sXPos, float &sYPos);
    void UpdateWarpMode();
    void RenderWarpMode();
    void UpdateWarpRegionMode();
    void RenderWarpRegionMode();

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();
    void RenderCursor(float pLft, float pTop);
    static void RenderBox(float pLft, float pTop, float pRgt, float pBot);

    //--Pointer Routing
    SugarLinkedList *GetGemAdjectiveList();

    //--Static Functions
    static AdventureMenu *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
    static void HookToLuaStateShop(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AM_GetProperty(lua_State *L);
int Hook_AM_SetProperty(lua_State *L);
int Hook_AM_SetMapInfo(lua_State *L);
int Hook_AM_SetFormProperty(lua_State *L);

//--Shop Functions
int Hook_AM_SetShopProperty(lua_State *L);
