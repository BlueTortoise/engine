//====================================== AdventureMenuMain ========================================
//--Header file for variables and such local only to the base menu.
#include "DataLibrary.h"
#pragma once

//======================================== Definitions ============================================
//--Timers
#define ADV_MENU_VIS_TICKS 15
#define ADV_MENU_SIZE_TICKS 10
#define ADV_MENU_SIZE_BOOST 1.50f

//--Main Menu Codes
#define AM_MAIN_ITEMS 0
#define AM_MAIN_EQUIPMENT 1
#define AM_MAIN_HEAL 2
#define AM_MAIN_STATUS 3
#define AM_MAIN_SKILLS 4
#define AM_MAIN_MAP 5
#define AM_MAIN_OPTIONS 6
#define AM_MAIN_QUIT 7
#define AM_MAIN_TOTAL 8

//--Forward Declaration
#ifndef _ADAM_TYPES_
#define _ADAM_TYPES_
#define CRAFT_ADAMANTITE_POWDER 0
#define CRAFT_ADAMANTITE_FLAKES 1
#define CRAFT_ADAMANTITE_SHARD 2
#define CRAFT_ADAMANTITE_PIECE 3
#define CRAFT_ADAMANTITE_CHUNK 4
#define CRAFT_ADAMANTITE_ORE 5
#define CRAFT_ADAMANTITE_TOTAL 6
#endif

//========================================= Structures ============================================
typedef struct AdvMenuBaseImages
{
    //--Fonts
    SugarFont *rHeadingFont;
    SugarFont *rMainlineFont;
    SugarFont *rSmallFont;

    //--Images for this UI
    SugarBitmap *rComboBar;
    SugarBitmap *rFooter;
    SugarBitmap *rHeader;
    SugarBitmap *rHealthBarBack;
    SugarBitmap *rHealthBarFill;
    SugarBitmap *rHealthBarFrame;
    SugarBitmap *rInventoryBacks;
    SugarBitmap *rInventoryBanners;
    SugarBitmap *rNameBar;
    SugarBitmap *rPlatinaBanner;
    SugarBitmap *rPlatinaText;
    SugarBitmap *rPortraitBack;
    SugarBitmap *rPortraitFront;
    SugarBitmap *rPortraitMask;
    SugarBitmap *rQuitBacking;

    //--Icons
    SugarBitmap *rIconFrame;
    SugarBitmap *rIcons[AM_MAIN_TOTAL];

    //--Images shared with other UIs
    SugarBitmap *rDescriptionBox;

    //--Adamantite Images
    SugarBitmap *rAdamantiteImg[CRAFT_ADAMANTITE_TOTAL];

    //--Functions
    void Construct()
    {
        //--Setup.
        DataLibrary *rDataLibrary = DataLibrary::Fetch();

        //--Fonts
        rHeadingFont  = rDataLibrary->GetFont("Adventure Menu Header");
        rMainlineFont = rDataLibrary->GetFont("Adventure Menu Mainline");
        rSmallFont    = rDataLibrary->GetFont("Adventure Menu Small");

        //--Images for this UI
        rComboBar         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/ComboBar");
        rFooter           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/Footer");
        rHeader           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/Header");
        rHealthBarBack    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/HealthBarBack");
        rHealthBarFill    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/HealthBarFill");
        rHealthBarFrame   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/HealthBarFrame");
        rInventoryBacks   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/InventoryBacks");
        rInventoryBanners = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/InventoryBanners");
        rNameBar          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/NameBar");
        rPlatinaBanner    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/PlatinaBanner");
        rPlatinaText      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/PlatinaText");
        rPortraitBack     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/PortraitBack");
        rPortraitFront    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/PortraitFront");
        rPortraitMask     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/PortraitMask");
        rQuitBacking      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/QuitBacking");

        //--Icons
        rIconFrame = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/ICO|Frame");
        rIcons[AM_MAIN_ITEMS]     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/ICO|Inventory");
        rIcons[AM_MAIN_EQUIPMENT] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/ICO|Equipment");
        rIcons[AM_MAIN_HEAL]      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/ICO|DoctorBag");
        rIcons[AM_MAIN_STATUS]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/ICO|Status");
        rIcons[AM_MAIN_SKILLS]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/ICO|Skills");
        rIcons[AM_MAIN_MAP]       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/ICO|Map");
        rIcons[AM_MAIN_OPTIONS]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/ICO|Options");
        rIcons[AM_MAIN_QUIT]      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/BaseMenu/ICO|Quit");

        //--Images shared with other UIs
        rDescriptionBox = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/DescriptionBox");
    }
}AdvMenuBaseImages;

typedef struct AdvMenuBaseVars
{
    //--Members
    bool mHasBuiltGrid;
    int mGridOpacityTimer;
    int mGridCurrent;
    GridPackage mGrid[AM_MAIN_TOTAL];

    //--Functions
    void Initialize()
    {
        mHasBuiltGrid = false;
        mGridOpacityTimer = 0;
        mGridCurrent = AM_MAIN_ITEMS;
        for(int i = 0; i < AM_MAIN_TOTAL; i ++)
        {
            mGrid[i].Initialize();
        }
    }
}AdvMenuBaseVars;
