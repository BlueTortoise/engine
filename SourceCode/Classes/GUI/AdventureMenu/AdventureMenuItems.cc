//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdvCombat.h"
#include "AdvHelp.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

//--[Forward Declarations]
void RenderAndAdvance(float pLft, float &sYCursor, SugarFont *pFont, const char *pFormat, ...);
void RenderLineBounded(float pLft, float pTop, float pWidth, SugarFont *pFont, const char *pString);

//--[Manipulators]
void AdventureMenu::SetToItemsMenu()
{
    AdventureInventory::Fetch()->BuildExtendedItemList();
    mCurrentMode = AM_MODE_ITEMS;
    mCurrentCursor = 0;
}
void AdventureMenu::RefreshItemMenuHelp()
{
    //--Resolve the images needed for the help menu.
    ControlManager *rControlManager = ControlManager::Fetch();
    mMainMenuHelp->Construct();

    //--Allocate strings.
    int cStrings = 6;
    mMainMenuHelp->AllocateStrings(cStrings);
    StarlightString **rStrings = mMainMenuHelp->GetStrings();

    //--Set.
    rStrings[0]->SetString("[IMG0] Previous Menu");
    rStrings[0]->AllocateImages(1);
    rStrings[0]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Cancel"));

    rStrings[1]->SetString(" ");

    rStrings[2]->SetString("[IMG0][IMG1] Change Selection");
    rStrings[2]->AllocateImages(2);
    rStrings[2]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Up"));
    rStrings[2]->SetImageP(1, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Down"));

    rStrings[3]->SetString("[IMG0] (Hold) Move 10 Selections at once");
    rStrings[3]->AllocateImages(1);
    rStrings[3]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Ctrl"));

    rStrings[4]->SetString(" ");

    rStrings[5]->SetString("[IMG0] Close This Display");
    rStrings[5]->AllocateImages(1);
    rStrings[5]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("F1"));

    //--All strings cross-reference here.
    for(int i = 0; i < cStrings; i ++) rStrings[i]->CrossreferenceImages();
}

//--[Update]
bool AdventureMenu::UpdateItemsMenu(bool pCannotHandleUpdate)
{
    ///--[Documentation and Setup]
    //--Standard update for the cursor.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    SugarLinkedList *rExtendedItemList = rInventory->GetExtendedItemList();

    //--Storage.
    int tOldCursor = mCurrentCursor;

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mCurrentMode == AM_MODE_ITEMS)
    {
        if(mItemVisTimer < ADVMENU_SKILLS_VIS_TICKS) mItemVisTimer ++;
    }
    //--Decrement.
    else
    {
        if(mItemVisTimer > 0) mItemVisTimer --;
    }

    ///--[Error Checking]
    //--If this menu is not the main object, stop the update here.
    if(mCurrentMode != AM_MODE_ITEMS || pCannotHandleUpdate) return false;

    ///--[Help Handler]
    if(mIsMainHelpVisible)
    {
        if(rControlManager->IsFirstPress("F1") || rControlManager->IsFirstPress("Cancel"))
        {
            mIsMainHelpVisible = false;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        return true;
    }

    //--Help activation.
    if(rControlManager->IsFirstPress("F1"))
    {
        mIsMainHelpVisible = true;
        RefreshItemMenuHelp();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[Directional Buttons]
    //--Up. Decrement the cursor by one.
    if(rControlManager->IsFirstPress("Up"))
    {
        if(!rControlManager->IsDown("Ctrl"))
        {
            if(mCurrentCursor > 0) mCurrentCursor --;
        }
        else
        {
            if(mCurrentCursor > 0) mCurrentCursor -= 10;
            if(mCurrentCursor < 0) mCurrentCursor = 0;
        }

        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Down. Increments the cursor by one.
    if(rControlManager->IsFirstPress("Down"))
    {
        if(!rControlManager->IsDown("Ctrl"))
        {
            if(mCurrentCursor < rExtendedItemList->GetListSize() - 1) mCurrentCursor ++;
        }
        else
        {
            mCurrentCursor += 10;
            if(mCurrentCursor >= rExtendedItemList->GetListSize()) mCurrentCursor = rExtendedItemList->GetListSize() - 1;
        }
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--[Recentering Code]
    //--Attempt to make the 'skip' value such that the selection is in the middle of the page.
    if(mCurrentCursor != tOldCursor)
    {
        //--On a decrement:
        if(mCurrentCursor < tOldCursor)
        {
            //--Get where the cursor is on the page.
            int tPosition = mCurrentCursor - mInventorySkip;
            if(tPosition < 2) mInventorySkip = mCurrentCursor - 2;

            //--Clamp.
            if(mInventorySkip < 0) mInventorySkip = 0;
        }
        //--On an increment:
        else if(mCurrentCursor > tOldCursor)
        {
            //--Get where the cursor is on the page.
            int tPosition = mCurrentCursor - mInventorySkip;
            if(tPosition > AM_ITEMS_PER_PAGE - 3) mInventorySkip = mCurrentCursor - (AM_ITEMS_PER_PAGE - 3);

            //--Clamp.
            if(mInventorySkip > rExtendedItemList->GetListSize() - AM_ITEMS_PER_PAGE) mInventorySkip = rExtendedItemList->GetListSize() - AM_ITEMS_PER_PAGE;
        }
    }

    //--[Cancel]
    //--Cancel, return to the main menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mInventorySkip = 0;
        SetToMainMenu();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Handled update.
    return true;
}

//--[Drawing]
void AdventureMenu::RenderItemsMenu()
{
    //--[Documentation and Setup]
    //--Renders weapons, equipment, items, keys, you know. All that stuff. Adamantite and catalysts
    //  are rendered on the base menu.
    if(!Images.mIsReady) return;

    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mItemVisTimer, ADVMENU_SKILLS_VIS_TICKS);
    if(cAlpha <= 0.0f) return;
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    SugarLinkedList *rItemList = rInventory->GetExtendedItemList();

    //--[Static Parts]
    //--Static backing.
    Images.InventoryUI.rFrames->Draw();

    //--Static text.
    float cCenterX = VIRTUAL_CANVAS_X * 0.50f;
    Images.InventoryUI.rHeadingFont->DrawText(cCenterX, 24.0f, SUGARFONT_AUTOCENTER_X, 2.0f, "Inventory");

    //--[Headers]
    //--Variables.
    float cHeaderY = 98.0f;
    float cIconX  = 57.0f;
    float cIconW  = 22.0f;
    float cFaceX = cIconX + cIconW;
    float cFaceW = 32.0f;
    float cNameX  = cFaceX + cFaceW;
    float cValueX = 470.0f;
    float cValueXRight = 565.0f;
    float cGemsX = 580.0f;
    float cPropertiesX = cGemsX + (22.0f * 6.0f) + 50.0f;
    float cPropertiesW = 32.0f;

    //--Text headers.
    Images.EquipmentUI.rHeadingFont->DrawText(cIconX,  cHeaderY, 0, 1.0f, "Name");
    Images.EquipmentUI.rHeadingFont->DrawText(cValueX, cHeaderY, 0, 1.0f, "Value");
    Images.EquipmentUI.rHeadingFont->DrawText(cGemsX,  cHeaderY, 0, 1.0f, "Gems");

    //--Image headers.
    float cImgHeadersX[AM_STAT_COMPARE_PACKS_TOTAL];
    for(int i = 0; i < AM_STAT_COMPARE_PACKS_TOTAL; i ++)
    {
        cImgHeadersX[i] = cPropertiesX + (i * cPropertiesW);
        if(!mStatComparePacks[i].rImage) continue;
        mStatComparePacks[i].rImage->Draw(cImgHeadersX[i], cHeaderY + 18.0f);
    }

    //--At the midpoint of the image headers, render "Properties".
    int cMiddleHeader = AM_STAT_COMPARE_PACKS_TOTAL / 2;
    Images.StatusUI.rVerySmallFont->DrawText(cImgHeadersX[cMiddleHeader], cHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Properties");

    //--Constants.
    float cSpcY = 23.0f;
    float cTop = cHeaderY + 48.0f;

    //--Variables.
    float tCurrentY = cTop;
    int tSkipsLeft = mInventorySkip;

    //--Start rendering those items, baby!
    int i = 0;
    int tRenders = AM_ITEMS_PER_PAGE;
    AdventureItem *rItem = (AdventureItem *)rItemList->PushIterator();
    while(rItem)
    {
        //--[Skip Entries]
        //--Skip entries until we reach the requested page.
        if(tSkipsLeft > 0)
        {
            rItem = (AdventureItem *)rItemList->AutoIterate();
            tSkipsLeft --;
            i ++;
            continue;
        }

        //--[Backing]
        if(tRenders % 2 == 1)
        {
            SugarBitmap::DrawRectFill(22, tCurrentY, 1349, tCurrentY + cSpcY, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cAlpha));
        }

        //--[Item Icon]
        //--Render the item's icon here. It's a 22px entry.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        SugarBitmap *rItemIcon = rItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(cIconX, tCurrentY);

        //--[Owner's Face Indicator]
        //--Get the item's name and its owner's name. It's possible for the owner's name to be NULL
        //  if the item isn't equipped.
        const char *rOwnerName = rItemList->GetIteratorName();

        //--Item has no owner:
        if(!rOwnerName || !strcasecmp(rOwnerName, "Null"))
        {
        }
        //--Item has an owner:
        else
        {
            //--Fast-access pointers.
            AdvCombat *rAdventureCombat = AdvCombat::Fetch();

            //--Variables.
            SugarBitmap *rFaceImg = NULL;
            const char *rRenderOwnerName = rOwnerName;
            TwoDimensionReal tFaceDim;

            //--Resolve the owner's display name. It's usually the same, but not always (Ex: Chris, Christine).
            AdvCombatEntity *rOwningEntity = rAdventureCombat->GetRosterMemberS(rRenderOwnerName);
            if(rOwningEntity)
            {
                rFaceImg = rOwningEntity->GetFaceProperties(tFaceDim);
            }

            //--Render the character's face.
            if(rFaceImg)
            {
                tFaceDim.mLft = tFaceDim.mLft;
                tFaceDim.mTop = 1.0f - (tFaceDim.mTop);
                tFaceDim.mRgt = tFaceDim.mRgt;
                tFaceDim.mBot = 1.0f - (tFaceDim.mBot);
                rFaceImg->Bind();
                glBegin(GL_QUADS);
                    glTexCoord2f(tFaceDim.mLft, tFaceDim.mTop); glVertex2f(cFaceX,          tCurrentY);
                    glTexCoord2f(tFaceDim.mRgt, tFaceDim.mTop); glVertex2f(cFaceX + cFaceW, tCurrentY);
                    glTexCoord2f(tFaceDim.mRgt, tFaceDim.mBot); glVertex2f(cFaceX + cFaceW, tCurrentY + cFaceW);
                    glTexCoord2f(tFaceDim.mLft, tFaceDim.mBot); glVertex2f(cFaceX,          tCurrentY + cFaceW);
                glEnd();
            }
        }

        //--[Cursor Handling]
        //--If this is the selected item, render the cursor over it.
        if(i == mCurrentCursor)
        {
            //--Render the item description.
            RenderItemDescription(541.0f, rItem);

            //--Tint the color.
            StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, cAlpha);
        }

        //--[Item Name]
        //--Name.
        float cNameW = Images.EquipmentUI.rMainlineFont->GetTextWidth(rItem->GetName());
        Images.EquipmentUI.rMainlineFont->DrawText(cNameX, tCurrentY, 0, 1.0f, rItem->GetName());

        //--Render stock if the item is stackable. This goes to the right of the name.
        if(rItem->IsStackable() && rItem->GetStackSize() > 1)
        {
            //--Small black backing box.
            char tString[32];
            sprintf(tString, "x%i", rItem->GetStackSize());
            float cLft = cNameX + cNameW;
            float cTop = tCurrentY + 5.0f;
            //SugarBitmap::DrawRectFill(cLft, cTop, cRgt, cBot, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cAlpha));

            //--Color, render, clean.
            StarlightColor::SetMixer(0.0f, 1.0f, 0.0f, cAlpha);
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cLft, cTop, 0, 1.0f, "x%i", rItem->GetStackSize());
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        }

        //--[Item Value]
        //--Right-align.
        char tBuffer[32];

        //--Value is -1? Print N/A for value.
        int tValue = rItem->GetValue();
        if(tValue == -1)
        {
            strcpy(tBuffer, "N/A");
        }
        else
        {
            sprintf(tBuffer, "%i", rItem->GetValue());
        }

        //--Resolve position and render.
        float cXPos = cValueXRight - Images.EquipmentUI.rMainlineFont->GetTextWidth(tBuffer);
        Images.EquipmentUI.rMainlineFont->DrawTextArgs(cXPos, tCurrentY, 0, 1.0f, tBuffer);

        //--[Gems]
        //--Render indicators for gems. Presently, we render empty/filled, but not gem color.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        int tGemSlots = rItem->GetGemSlots();
        if(tGemSlots > 0)
        {
            //--Render each gem indicator from the same sheet.
            for(int p = 0; p < tGemSlots; p ++)
            {
                //--Render the base.
                Images.VendorUI.rGemEmpty22px->Draw(cGemsX + (22.0f * p), tCurrentY);

                //--Render the gem, i fit exists.
                AdventureItem *rGemInSlot = rItem->GetGemInSlot(p);
                if(rGemInSlot)
                {
                    SugarBitmap *rGemIcon = rGemInSlot->GetIconImage();
                    if(rGemIcon) rGemIcon->Draw(cGemsX + (22.0f * p), tCurrentY);
                }
            }
        }

        //--[Properties]
        //--Displays all properties in the order of the headers. If a property is not specified for an item,
        //  an underscore '_' is displayed.
        for(int p = 0; p < AM_STAT_COMPARE_PACKS_TOTAL; p ++)
        {
            //--Render the bonus.
            int tIndex = mStatComparePacks[p].mIndex;
            int cBonus = rItem->GetStatistic(tIndex);
            if(cBonus != 0)
            {
                Images.StatusUI.rVerySmallFont->DrawTextArgs(cImgHeadersX[p] + 9.0f, tCurrentY + 4.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonus);
            }
            //--Zero. Render an underscore.
            else
            {
                //Images.StatusUI.rVerySmallFont->DrawText(cImgHeadersX[p] + 9.0f, tCurrentY + 4.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "_");
            }

            //--Render a white line between properties.
            if(p < AM_STAT_COMPARE_PACKS_TOTAL - 1)
            {
                SugarBitmap::DrawRectFill(cImgHeadersX[p+1] - 9.0f, tCurrentY, cImgHeadersX[p+1] - 7.0f, tCurrentY + cSpcY, StarlightColor::MapRGBAF(0.3f, 0.3f, 0.3f, 1.0f));
            }
        }

        //--[Next]
        //--Increment the item render counter.
        i ++;
        tRenders --;
        tCurrentY = tCurrentY + cSpcY;

        //--Ending case.
        if(tRenders < 1)
        {
            rItemList->PopIterator();
            break;
        }

        //--Next.
        rItem = (AdventureItem *)rItemList->AutoIterate();
    }
    StarlightColor::ClearMixer();

    //--[Scrollbar]
    //--Render a scrollbar to indicate how much of the inventory is represented.
    if(rItemList->GetListSize() > AM_ITEMS_PER_PAGE)
    {
        //--Binding.
        Images.InventoryUI.rScrollbarFront->Bind();

        //--Scrollbar front. Stretches dynamically.
        float cScrollTop = 138.0f;
        float cScrollHei = 366.0f;

        //--Determine what percentage of the inventory is presently represented.
        int tMaxOffset = rItemList->GetListSize();
        float cPctTop = (mInventorySkip                    ) / (float)tMaxOffset;
        float cPctBot = (mInventorySkip + AM_ITEMS_PER_PAGE) / (float)tMaxOffset;

        //--Positions and Constants.
        float cLft = 1321.0f;
        float cRgt = cLft + Images.InventoryUI.rScrollbarFront->GetWidth();
        float cEdg = 6.0f / (float)Images.InventoryUI.rScrollbarFront->GetHeight();

        //--Compute where the bar should be.
        float cTopTop = cScrollTop + (cPctTop * cScrollHei);
        float cTopBot = cTopTop + 6.0f;
        float cBotBot = cScrollTop + (cPctBot * cScrollHei);
        float cBotTop = cBotBot - 6.0f;

        glBegin(GL_QUADS);
            //--Render the top of the bar.
            glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTopTop);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTopTop);
            glTexCoord2f(1.0f, cEdg); glVertex2f(cRgt, cTopBot);
            glTexCoord2f(0.0f, cEdg); glVertex2f(cLft, cTopBot);

            //--Render the bottom of the bar.
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(1.0f, 1.0f);        glVertex2f(cRgt, cBotBot);
            glTexCoord2f(0.0f, 1.0f);        glVertex2f(cLft, cBotBot);

            //--Render the middle of the bar.
            glTexCoord2f(0.0f, cEdg);        glVertex2f(cLft, cTopBot);
            glTexCoord2f(1.0f, cEdg);        glVertex2f(cRgt, cTopBot);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
        glEnd();


        //--Backing. Already positioned.
        Images.InventoryUI.rScrollbarBack->Draw();
    }

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Help Menu]
    //--Help string.
    mShowHelpString->DrawText(0.0f, 0.0f, 0, 1.0f, Images.BaseMenu.rMainlineFont);

    //--Menu, if visible.
    if(mMainHelpTimer > 0)
    {
        //--Compute opacity.
        float cPercentage = EasingFunction::QuadraticInOut(mMainHelpTimer, ADV_HELP_STD_TICKS);
        SugarBitmap::DrawFullBlack(cPercentage * 0.75f);

        //--Offset.
        float tYOffset = ADV_HELP_STD_OFFSET * (1.0f - cPercentage);
        mMainMenuHelp->Render(tYOffset);
    }
}
void AdventureMenu::RenderItemDescription(float pYPos, AdventureItem *pDescriptionItem)
{
    //--Renders information about the item in question. Almost identical to the equipment version, but uses
    //  different positioning and a different caller flag.
    if(!Images.mIsReady) return;
    if(!pDescriptionItem) return;

    //--First, render the name of the item.
    float cCenterX = VIRTUAL_CANVAS_X * 0.50f;
    Images.EquipmentUI.rHeadingFont->DrawText(cCenterX, pYPos, SUGARFONT_AUTOCENTER_X, 1.0f, pDescriptionItem->GetName());

    //--[Description]
    //--Constants.
    float cXPosition = 22.0f;
    float cWidth = 1318.0f;

    //--Variables.
    float tYPosition = pYPos + 72.0f;

    //--Lines auto-parse out to meet the length of the description box.
    int tCharsParsed = 0;
    int tCharsParsedTotal = 0;
    const char *rDescription = pDescriptionItem->GetDescription();
    if(rDescription)
    {
        //--Setup.
        int tStringLen = (int)strlen(rDescription);

        //--Loop until the whole description has been parsed out.
        while(tCharsParsedTotal < tStringLen)
        {
            //--Run the subdivide.
            char *tDescriptionLine = Subdivide::SubdivideString(tCharsParsed, &rDescription[tCharsParsedTotal], -1, cWidth, Images.EquipmentUI.rMainlineFont, 1.0f);
            RenderAndAdvance(cXPosition, tYPosition, Images.EquipmentUI.rMainlineFont, tDescriptionLine);

            //--Move to the next line.
            tCharsParsedTotal += tCharsParsed;

            //--Clean up.
            free(tDescriptionLine);
        }
    }
}

//--[Worker Functions]
//--Renders the given line and advances the cursor to the next line.
void RenderAndAdvance(float pLft, float &sYCursor, SugarFont *pFont, const char *pFormat, ...)
{
    //--Renders the provided string and advances the Y cursor by the font height.
    if(!pFont || !pFormat) return;

    //--Get the variable arg list.
    va_list tArgList;
    va_start(tArgList, pFormat);

    //--Print the args into a buffer.
    char tBuffer[256];
    vsprintf(tBuffer, pFormat, tArgList);
    va_end(tArgList);

    //--Render.
    pFont->DrawTextImg(pLft, sYCursor, 0, 1.0f, tBuffer, AdventureMenu::xTextImgRemapsTotal, AdventureMenu::xTextImgRemap, AdventureMenu::xTextImgPath);
    sYCursor = sYCursor + pFont->GetTextHeight();
}

//--Renders an entire series of lines until the characters run out, automatically subdividing when the edge is found.
void RenderLineBounded(float pLft, float pTop, float pWidth, SugarFont *pFont, const char *pString)
{
    //--Arg check.
    if(!pString || !pFont) return;
    if(pWidth < 10.0f) pWidth = 10.0f;

    //--Setup.
    int tStringLen = (int)strlen(pString);
    float tYPosition = pTop;
    int tCharsParsed = 0;
    int tCharsParsedTotal = 0;

    //--Loop until the whole description has been parsed out.
    while(tCharsParsedTotal < tStringLen)
    {
        //--Run the subdivide.
        char *tDescriptionLine = Subdivide::SubdivideString(tCharsParsed, &pString[tCharsParsedTotal], -1, pWidth, pFont, 1.0f);
        RenderAndAdvance(pLft, tYPosition, pFont, tDescriptionLine);

        //--Move to the next line.
        tCharsParsedTotal += tCharsParsed;

        //--Clean up.
        free(tDescriptionLine);
    }
}
