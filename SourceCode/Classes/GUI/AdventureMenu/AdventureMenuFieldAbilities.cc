//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "FieldAbility.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
void AdventureMenu::OpenFieldAbilities()
{
    //--Setup.
    float cRenderWid = 150.0f;
    float cRenderLft = 245.0f;
    float cRenderTop = 185.0f;

    //--System
    mFieldAbilityMenu.mIsFieldAbilityMode = true;
    mFieldAbilityMenu.mIsSwitching = false;
    mFieldAbilityMenu.mHiCursor = 0;
    mFieldAbilityMenu.mLoCursor = 0;

    //--Cursor Movement
    mFieldAbilityMenu.mHighlightPos.MoveTo(cRenderLft, cRenderTop, 0);
    mFieldAbilityMenu.mHighlightSize.MoveTo(cRenderWid, cRenderWid, 0);
    mFieldAbilityMenu.mIsFieldAbilityMode = true;

    //--Refresh field abilities.
    mFieldAbilityMenu.mrFieldAbilityList->ClearList();
    LuaManager::Fetch()->ExecuteLuaFile(AdventureMenu::xFieldAbilityResolveScript);
}
void AdventureMenu::CloseFieldAbilities()
{
    mFieldAbilityMenu.mIsFieldAbilityMode = false;
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void AdventureMenu::RegisterFieldAbility(const char *pDLPath)
{
    FieldAbility *rFieldAbility = (FieldAbility *)DataLibrary::Fetch()->GetEntry(pDLPath);
    if(!rFieldAbility) return;
    mFieldAbilityMenu.mrFieldAbilityList->AddElement("X", rFieldAbility);
}

//============================================ Update =============================================
void AdventureMenu::UpdateFieldAbilities()
{
    ///--[Timers]
    //--Cursor.
    mFieldAbilityMenu.mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mFieldAbilityMenu.mHighlightSize.Increment(EASING_CODE_QUADOUT);
    mFieldAbilityMenu.mLolightPos.Increment(EASING_CODE_QUADOUT);
    mFieldAbilityMenu.mLolightSize.Increment(EASING_CODE_QUADOUT);

    //--Visiblity timer.
    if(mFieldAbilityMenu.mIsFieldAbilityMode)
    {
        if(mFieldAbilityMenu.mVisibilityTimer < ADVMENU_FIELDABILITY_VIS_TICKS) mFieldAbilityMenu.mVisibilityTimer ++;
    }
    //--Hiding.
    else
    {
        if(mFieldAbilityMenu.mVisibilityTimer > 0) mFieldAbilityMenu.mVisibilityTimer --;
        return;
    }

    ///--[Controls]
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Setup for cursor.
    float cRenderWid = 150.0f;
    float cRenderSpc = 175.0f;
    float cRenderLft = 245.0f;
    float cRenderTop = 185.0f;

    //--Lower Render.
    float cAbiLft = 40.0f;
    float cAbiTop = 457.0f;
    float cAbiWid = 50.0f;
    float cAbiSpc = 55.0f;

    ///--[Slot Select]
    if(!mFieldAbilityMenu.mIsSwitching)
    {
        //--Left, decrement cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            if(mFieldAbilityMenu.mHiCursor > 0)
            {
                mFieldAbilityMenu.mHiCursor --;
                mFieldAbilityMenu.mHighlightPos.MoveTo(cRenderLft + (cRenderSpc * mFieldAbilityMenu.mHiCursor), cRenderTop, ADVMENU_FIELDABILITY_CURSOR_TICKS);
                mFieldAbilityMenu.mHighlightSize.MoveTo(cRenderWid, cRenderWid, ADVMENU_FIELDABILITY_CURSOR_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Right, increment cursor.
        else if(rControlManager->IsFirstPress("Right"))
        {
            if(mFieldAbilityMenu.mHiCursor < ADVCOMBAT_FIELD_ABILITY_SLOTS-1)
            {
                mFieldAbilityMenu.mHiCursor ++;
                mFieldAbilityMenu.mHighlightPos.MoveTo(cRenderLft + (cRenderSpc * mFieldAbilityMenu.mHiCursor), cRenderTop, ADVMENU_FIELDABILITY_CURSOR_TICKS);
                mFieldAbilityMenu.mHighlightSize.MoveTo(cRenderWid, cRenderWid, ADVMENU_FIELDABILITY_CURSOR_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Accept, begin switching.
        else if(rControlManager->IsFirstPress("Activate"))
        {
            //--Flag.
            mFieldAbilityMenu.mIsSwitching = true;

            //--Specify the cursor position as where the top cursor was.
            mFieldAbilityMenu.mLolightPos.MoveTo(cRenderLft + (cRenderSpc * mFieldAbilityMenu.mHiCursor), cRenderTop, 0);
            mFieldAbilityMenu.mLolightSize.MoveTo(cRenderWid, cRenderWid, 0);

            mFieldAbilityMenu.mLolightPos.MoveTo(cAbiLft + (cAbiSpc * mFieldAbilityMenu.mLoCursor), cAbiTop, ADVMENU_FIELDABILITY_CURSOR_TICKS);
            mFieldAbilityMenu.mLolightSize.MoveTo(cAbiWid, cAbiWid, ADVMENU_FIELDABILITY_CURSOR_TICKS);

            //--Audio.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Cancel, exit menu.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            CloseFieldAbilities();
            mStartedHidingThisTick = true;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    ///--[Replacement Select]
    else
    {
        //--Left, decrement cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            if(mFieldAbilityMenu.mLoCursor > 0)
            {
                mFieldAbilityMenu.mLoCursor --;
                mFieldAbilityMenu.mLolightPos.MoveTo(cAbiLft + (cAbiSpc * mFieldAbilityMenu.mLoCursor), cAbiTop, ADVMENU_FIELDABILITY_CURSOR_TICKS);
                mFieldAbilityMenu.mLolightSize.MoveTo(cAbiWid, cAbiWid, ADVMENU_FIELDABILITY_CURSOR_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Right, increment cursor.
        else if(rControlManager->IsFirstPress("Right"))
        {
            if(mFieldAbilityMenu.mLoCursor < mFieldAbilityMenu.mrFieldAbilityList->GetListSize()-1)
            {
                mFieldAbilityMenu.mLoCursor ++;
                mFieldAbilityMenu.mLolightPos.MoveTo(cAbiLft + (cAbiSpc * mFieldAbilityMenu.mLoCursor), cAbiTop, ADVMENU_FIELDABILITY_CURSOR_TICKS);
                mFieldAbilityMenu.mLolightSize.MoveTo(cAbiWid, cAbiWid, ADVMENU_FIELDABILITY_CURSOR_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Accept, begin switching.
        else if(rControlManager->IsFirstPress("Activate"))
        {
            //--Flag.
            mFieldAbilityMenu.mIsSwitching = false;

            //--Get the ability in the slot.
            FieldAbility *rFieldAbility = (FieldAbility *)mFieldAbilityMenu.mrFieldAbilityList->GetElementBySlot(mFieldAbilityMenu.mLoCursor);
            if(rFieldAbility)
            {
                //--Get the ability formerly in the slot.
                AdvCombat *rAdventureCombat = AdvCombat::Fetch();
                FieldAbility *rOldAbility = rAdventureCombat->GetFieldAbility(mFieldAbilityMenu.mHiCursor);

                //--Set this as the active ability.
                rAdventureCombat->SetFieldAbility(mFieldAbilityMenu.mHiCursor, rFieldAbility);

                //--If the ability is in another slot, clear it.
                for(int i = 0; i < ADVCOMBAT_FIELD_ABILITY_SLOTS; i ++)
                {
                    //--This is the slot we just replaced.
                    if(i == mFieldAbilityMenu.mHiCursor) continue;

                    //--Get the ability in the slot.
                    FieldAbility *rCheckAbility = rAdventureCombat->GetFieldAbility(i);

                    //--If it's the same as the one we just placed, replace it. This removes dupes and swaps slots.
                    if(rCheckAbility == rFieldAbility) rAdventureCombat->SetFieldAbility(i, rOldAbility);
                }
            }

            //--Audio.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Cancel, exit menu.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            mFieldAbilityMenu.mIsSwitching = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
}

//=========================================== Drawing =============================================
void AdventureMenu::RenderFieldAbilities()
{
    ///--[Documentation and Setup]
    if(!Images.mIsReady) return;

    ///--[Common]
    //--Compute visibility percentage.
    float cVisPercent = EasingFunction::QuadraticInOut(mFieldAbilityMenu.mVisibilityTimer, ADVMENU_FIELDABILITY_VIS_TICKS);
    if(cVisPercent == 0.0) return;

    //--Darken the background.
    SugarBitmap::DrawFullBlack(0.75f * cVisPercent);

    //--Images.
    SugarBitmap *rEmptyFrame = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/AdventureUI/Abilities/FrameRed");

    //--Render positions for the abilities.
    float cRenderWid = 175.0f;
    float cRenderLft = 245.0f;
    float cRenderTop = 185.0f;

    ///--[Header]
    //--Compute offsets.
    float cTopOffset = -500.0f * (1.0f - cVisPercent);
    glTranslatef(0.0f, cTopOffset, 0.0f);

    //--Backing.
    Images.FieldAbilityUI.rHeader->Draw();

    //--Render the five abilities.
    AdvCombat *rCombat = AdvCombat::Fetch();
    for(int i = 0; i < ADVCOMBAT_FIELD_ABILITY_SLOTS; i ++)
    {
        //--Check.
        FieldAbility *rFieldAbility = rCombat->GetFieldAbility(i);
        if(rFieldAbility)
        {
            //--Get the display variables.
            SugarBitmap *rBackingImg = rFieldAbility->GetBackingImage();
            SugarBitmap *rFrameImg   = rFieldAbility->GetFrameImage();
            SugarBitmap *rMainImg    = rFieldAbility->GetMainImage();

            //--Render.
            if(rBackingImg) rBackingImg->DrawScaled(cRenderLft + (cRenderWid * i), cRenderTop, 3.0f, 3.0f);
            if(rFrameImg)   rFrameImg->  DrawScaled(cRenderLft + (cRenderWid * i), cRenderTop, 3.0f, 3.0f);
            if(rMainImg)    rMainImg->   DrawScaled(cRenderLft + (cRenderWid * i), cRenderTop, 3.0f, 3.0f);

            //--Name.
            Images.FieldAbilityUI.rMainFont->DrawText(cRenderLft + (cRenderWid * i) + (cRenderWid * 0.50f) - 12.0f, cRenderTop - 30.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rFieldAbility->GetDisplayName());
        }
        //--Render an empty frame if not occupied.
        else
        {
            if(rEmptyFrame) rEmptyFrame->DrawScaled(cRenderLft + (cRenderWid * i), cRenderTop, 3.0f, 3.0f);
        }
    }

    //--Clean.
    glTranslatef(0.0f, -cTopOffset, 0.0f);

    ///--[Footer]
    //--Compute offsets.
    float cBotOffset = 500.0f * (1.0f - cVisPercent);
    glTranslatef(0.0f, cBotOffset, 0.0f);

    //--Backing.
    Images.FieldAbilityUI.rFooter->Draw();

    //--Render ability list.
    int tAbiCursorX = 0;
    int tAbiCursorY = 0;
    float cAbiLft = 40.0f;
    float cAbiTop = 457.0f;
    float cAbiWid = 55.0f;
    FieldAbility *rAbility = (FieldAbility *)mFieldAbilityMenu.mrFieldAbilityList->PushIterator();
    while(rAbility)
    {
        //--Compute.
        float tX = cAbiLft + (cAbiWid * tAbiCursorX);
        float tY = cAbiTop + (cAbiWid * tAbiCursorY);

        //--Get the display variables.
        SugarBitmap *rBackingImg = rAbility->GetBackingImage();
        SugarBitmap *rFrameImg   = rAbility->GetFrameImage();
        SugarBitmap *rMainImg    = rAbility->GetMainImage();

        //--Render.
        if(rBackingImg) rBackingImg->Draw(tX, tY);
        if(rFrameImg)   rFrameImg->  Draw(tX, tY);
        if(rMainImg)    rMainImg->   Draw(tX, tY);

        //--Next.
        tAbiCursorX ++;
        rAbility = (FieldAbility *)mFieldAbilityMenu.mrFieldAbilityList->AutoIterate();
    }

    //--Description.
    rAbility = rCombat->GetFieldAbility(mFieldAbilityMenu.mHiCursor);
    if(mFieldAbilityMenu.mIsSwitching) rAbility = (FieldAbility *)mFieldAbilityMenu.mrFieldAbilityList->GetElementBySlot(mFieldAbilityMenu.mLoCursor);
    if(rAbility)
    {
        //--Variables.
        float cTxtLft = 25.0f;
        float cTxtTop = 642.0f;
        float cTxtHei = 22.0f;

        //--Title.
        const char *rName = rAbility->GetDisplayName();
        Images.FieldAbilityUI.rMainFont->DrawText(cTxtLft, cTxtTop, 0, 1.0f, rName);

        //--Lines.
        int tLines = rAbility->GetDisplayStringsTotal();
        for(int i = 0; i < tLines; i ++)
        {
            const char *rDisplayString = rAbility->GetDisplayString(i);
            Images.FieldAbilityUI.rMainFont->DrawText(cTxtLft, cTxtTop + (cTxtHei * (i+1)), 0, 1.0f, rDisplayString);
        }
    }

    //--Clean.
    glTranslatef(0.0f, -cBotOffset, 0.0f);

    ///--[Highlight]
    glTranslatef(0.0f, cTopOffset, 0.0f);
    RenderExpandableHighlight(mFieldAbilityMenu.mHighlightPos.mXCur, mFieldAbilityMenu.mHighlightPos.mYCur, mFieldAbilityMenu.mHighlightPos.mXCur+mFieldAbilityMenu.mHighlightSize.mXCur, mFieldAbilityMenu.mHighlightPos.mYCur+mFieldAbilityMenu.mHighlightSize.mYCur, 4.0f, Images.SkillsUI.rHighlight);
    glTranslatef(0.0f, -cTopOffset, 0.0f);
    if(mFieldAbilityMenu.mIsSwitching)
    {
        glTranslatef(0.0f, cBotOffset, 0.0f);
        RenderExpandableHighlight(mFieldAbilityMenu.mLolightPos.mXCur, mFieldAbilityMenu.mLolightPos.mYCur, mFieldAbilityMenu.mLolightPos.mXCur+mFieldAbilityMenu.mLolightSize.mXCur, mFieldAbilityMenu.mLolightPos.mYCur+mFieldAbilityMenu.mLolightSize.mYCur, 4.0f, Images.SkillsUI.rHighlight);
        glTranslatef(0.0f, -cBotOffset, 0.0f);
    }
}
