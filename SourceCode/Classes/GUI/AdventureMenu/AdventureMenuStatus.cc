//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdvHelp.h"
#include "AdventureItem.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

//--Constants
#define AM_ABILITIES_PER_PAGE 11

//--Forward Declarations
void RenderTextToFit(SugarFont *pFont, float pX, float pY, float pMaxWid, const char *pText, ...);
void RenderAndAdvance(float pLft, float &sYCursor, SugarFont *pFont, const char *pFormat, ...);

//--[Worker Functions]
int ComputeAbilityCursorMax(SugarLinkedList *pAbilityList)
{
    //--Iterate across the given ability list and return the number of abilities on it.
    if(!pAbilityList) return 0;
    return pAbilityList->GetListSize();
}

//--[System]
void AdventureMenu::LoadStatusMenu()
{
    //--Called during initial loading.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    Images.StatusUI.rHeadingFont    = rDataLibrary->GetFont("Adventure Menu Status Header");
    Images.StatusUI.rMainlineFont   = rDataLibrary->GetFont("Adventure Menu Status Main");
    Images.StatusUI.rVerySmallFont  = rDataLibrary->GetFont("Adventure Menu Status Small");
    Images.StatusUI.rResistanceFont = rDataLibrary->GetFont("Adventure Menu Status Resistance");
    Images.StatusUI.rHealthFont     = rDataLibrary->GetFont("Adventure Menu Status Health");
    Images.StatusUI.rStatisticFont  = rDataLibrary->GetFont("Adventure Menu Status Statistic");
    Images.StatusUI.rEquipmentFont  = rDataLibrary->GetFont("Adventure Menu Status Equipment");

    //--Images.
    Images.StatusUI.rAbilityCursor      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Abilities/Select");
    Images.StatusUI.rAbilityInspector   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/AbilityInspector");
    Images.StatusUI.rBackgroundFill     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/BackgroundFill");
    Images.StatusUI.rBannerTop          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/BannerTop");
    Images.StatusUI.rBtnBack            = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/BtnBack");
    Images.StatusUI.rCharInfo           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/CharInfo");
    Images.StatusUI.rCharOverlayMask    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/CharOverlayMask");
    Images.StatusUI.rCharOverlayMaskLft = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/CharOverlayMaskLft");
    Images.StatusUI.rCharOverlayMaskRgt = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/CharOverlayMaskRgt");
    Images.StatusUI.rEXPBarFill         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/EXPBarFill");
    Images.StatusUI.rEXPBarFrame        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/EXPBarFrame");
    Images.StatusUI.rHealthBarFill      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/HealthBarFill");
    Images.StatusUI.rHealthBarFrame     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/HealthBarFrame");
    Images.StatusUI.rHealthBarUnder     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/HealthBarUnder");
    Images.StatusUI.rInventory          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/Inventory");
    Images.StatusUI.rNavButtons         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Status/NavButtons");

    //--Stat Icons
    Images.StatusUI.rStatIndicatorAtk = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Attack");
    Images.StatusUI.rStatIndicatorPrt = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Protection");
    Images.StatusUI.rStatIndicatorIni = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Initiative");
    Images.StatusUI.rStatIndicatorAcc = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Accuracy");
    Images.StatusUI.rStatIndicatorEvd = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Evade");

    //--Damage types.
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_PROTECTION] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Protection");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_SLASHING]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Slashing");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_STRIKING]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Striking");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_PIERCING]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Piercing");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_FLAMING]    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Flaming");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_FREEZING]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Freezing");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_SHOCKING]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Shocking");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_CRUSADING]  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Crusading");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_OBSCURING]  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Obscuring");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_BLEEDING]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Bleeding");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_POISONING]  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Poisoning");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_CORRODING]  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Corroding");
    Images.StatusUI.rDamageTypeIcon[ADVC_DAMAGE_TERRIFYING] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DamageTypeIcons/Terrifying");
}

//--[Main Functions]
void AdventureMenu::SetStatusCharacter(int pIndex)
{
    //--Set.
    mStatusCursor = pIndex;

    //--Range-check.
    AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(mStatusCursor);
    if(!rActiveEntity)
    {
        mCurrentMode = AM_MODE_BASE;
        mCurrentCursor = AM_MAIN_STATUS;
        return;
    }

    //--Order the entity to update their stats.
    rActiveEntity->RefreshStatsForUI();
}
void AdventureMenu::RefreshStatusMenuHelp()
{
    //--Resolve the images needed for the help menu.
    ControlManager *rControlManager = ControlManager::Fetch();
    mMainMenuHelp->Construct();

    //--Allocate strings.
    int cStrings = 3;
    mMainMenuHelp->AllocateStrings(cStrings);
    StarlightString **rStrings = mMainMenuHelp->GetStrings();

    //--Set.
    rStrings[0]->SetString("[IMG0][IMG1] Change Character");
    rStrings[0]->AllocateImages(2);
    rStrings[0]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Right"));
    rStrings[0]->SetImageP(1, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Left"));

    rStrings[1]->SetString(" ");

    rStrings[2]->SetString("[IMG0] Return to previous menu");
    rStrings[2]->AllocateImages(1);
    rStrings[2]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Cancel"));

    //--All strings cross-reference here.
    for(int i = 0; i < cStrings; i ++) rStrings[i]->CrossreferenceImages();
}
bool AdventureMenu::UpdateStatusMenu(bool pCannotHandleUpdate)
{
    ///--[Documentation and Setup]
    //--Handles controls for the status menu. Basically just press cancel to go back, left and right to change characters.
    ControlManager *rControlManager = ControlManager::Fetch();

    ///--[Timers]
    //--Visibility.
    if(mCurrentMode == AM_MODE_STATUS)
    {
        if(mStatusTimer < ADVMENU_SKILLS_VIS_TICKS) mStatusTimer ++;
    }
    //--Decrement.
    else
    {
        if(mStatusTimer > 0) mStatusTimer --;
    }

    ///--[Error Checking]
    //--Stop update if not active object.
    if(mCurrentMode != AM_MODE_STATUS || pCannotHandleUpdate) return false;

    ///--[Help Handler]
    if(mIsMainHelpVisible)
    {
        if(rControlManager->IsFirstPress("F1") || rControlManager->IsFirstPress("Cancel"))
        {
            mIsMainHelpVisible = false;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        return true;
    }

    //--Help activation.
    if(rControlManager->IsFirstPress("F1"))
    {
        mIsMainHelpVisible = true;
        RefreshStatusMenuHelp();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[Directions]
    //--Left!
    if(rControlManager->IsFirstPress("Left"))
    {
        //--Setup.
        int tCharactersMax = AdvCombat::Fetch()->GetActivePartyCount();
        if(tCharactersMax == 1) return true;

        //--Check the cursor.
        int tTempCursor = mStatusCursor - 1;
        if(tTempCursor < 0) tTempCursor = tCharactersMax - 1;
        SetStatusCharacter(tTempCursor);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--RIGHT!
    if(rControlManager->IsFirstPress("Right"))
    {
        //--Setup.
        int tCharactersMax = AdvCombat::Fetch()->GetActivePartyCount();
        if(tCharactersMax == 1) return true;

        //--Check the cursor.
        int tTempCursor = mStatusCursor - 1;
        if(tTempCursor < 0) tTempCursor = tCharactersMax - 1;
        SetStatusCharacter(tTempCursor);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Pressing down activates ability mode if it's not active.
    if(rControlManager->IsFirstPress("Down"))
    {
    }

    //--Pressing up deactivates ability mode if it's active.
    if(rControlManager->IsFirstPress("Up"))
    {
    }

    //--Back to the main menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Flags.
        mCurrentMode = AM_MODE_BASE;
        mCurrentCursor = AM_MAIN_STATUS;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Handled update.
    return true;
}

//--[Worker Function: RenderEquipmentImage]
//--Renders the image associated with an equipment slot for a character.
void RenderEquipmentImage(float pX, float pY, float pWidth, AdvCombatEntity *pEntity, SugarBitmap *pGemIndicator, const char *pEquipmentSlot, SugarFont *pRenderFont)
{
    //--Arg check.
    if(!pEntity || !pRenderFont || !pEquipmentSlot) return;

    //--Check if the entity has an item in this slot.
    AdventureItem *rCheckItem = pEntity->GetEquipmentBySlotS(pEquipmentSlot);
    if(!rCheckItem) return;

    //--Pass to subroutine.
    //RenderItemImage(pX,pY, pWidth, rCheckItem, pGemIndicator, pRenderFont);
}

//--[Drawing Function]
void AdventureMenu::RenderStatusMenu()
{
    ///--[Documentation and Setup]
    //--Verify images.
    if(!Images.mIsReady) return;

    //--Get the character in question.
    AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(mStatusCursor);
    if(!rActiveEntity) return;

    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mStatusTimer, ADVMENU_SKILLS_VIS_TICKS);
    if(cAlpha <= 0.0f) return;

    ///--[Static Backing]
    //--Background. Mostly transparent.
    SugarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 64));
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Render static pieces.
    Images.StatusUI.rBackgroundFill->Draw();
    Images.StatusUI.rBannerTop->Draw();

    ///--[Player Character Render]
    //--Render the player character in question. First, activate stenciling and render a mask.
    DisplayManager::ActivateMaskRender(AM_STENCIL_STATUS);
    Images.StatusUI.rCharOverlayMask->Draw();

    //--These overlays render on the 2, to prevent accidental overlay with the main image.
    glStencilFunc(GL_ALWAYS, 2, 0xFF);
    Images.StatusUI.rCharOverlayMaskLft->Draw();
    Images.StatusUI.rCharOverlayMaskRgt->Draw();

    //--GL Setup for rendering.
    DisplayManager::ActivateStencilRender(AM_STENCIL_STATUS);

    //--Current character, always exists.
    SugarBitmap *rFieldPortrait = rActiveEntity->GetCombatPortrait();
    TwoDimensionRealPoint cMainRenderCoords = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_STATUS_MAIN);
    bool tIsSmallPortraitMode = OptionsManager::Fetch()->GetOptionB("LowResAdventureMode");
    if(rFieldPortrait)
    {
        //--Normal rendering:
        if(!tIsSmallPortraitMode)
        {
            rFieldPortrait->Draw(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter);
        }
        //--Scaled rendering.
        else
        {
            rFieldPortrait->DrawScaled(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter, LOWRES_SCALEINV, LOWRES_SCALEINV);
        }
    }

    //--Draw the left character, if it exists. If the cursor is at 0, no character renders.
    AdvCombatEntity *rLftEntity = AdvCombat::Fetch()->GetActiveMemberI(mStatusCursor-1);
    if(rLftEntity)
    {
        //--Setup.
        glStencilFunc(GL_EQUAL, AM_STENCIL_STATUS+1, 0xFF);
        rFieldPortrait = rLftEntity->GetCombatPortrait();
        cMainRenderCoords = rLftEntity->GetUIRenderPosition(ACE_UI_INDEX_STATUS_LEFT);

        //--Normal rendering:
        if(!tIsSmallPortraitMode)
        {
            if(rFieldPortrait) rFieldPortrait->Draw(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter);
        }
        //--Scaled rendering.
        else
        {
            if(rFieldPortrait) rFieldPortrait->DrawScaled(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter, LOWRES_SCALEINV, LOWRES_SCALEINV);
        }
    }

    //--Draw the right character, if it exists.
    AdvCombatEntity *rRgtEntity = AdvCombat::Fetch()->GetActiveMemberI(mStatusCursor+1);
    if(rRgtEntity)
    {
        //--Setup.
        glStencilFunc(GL_EQUAL, AM_STENCIL_STATUS+2, 0xFF);
        rFieldPortrait = rRgtEntity->GetCombatPortrait();
        cMainRenderCoords = rRgtEntity->GetUIRenderPosition(ACE_UI_INDEX_STATUS_RIGHT);

        //--Normal rendering:
        if(!tIsSmallPortraitMode)
        {
            if(rFieldPortrait) rFieldPortrait->Draw(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter);
        }
        //--Scaled rendering.
        else
        {
            if(rFieldPortrait) rFieldPortrait->DrawScaled(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter, LOWRES_SCALEINV, LOWRES_SCALEINV);
        }
    }

    //--Clean up.
    DisplayManager::DeactivateStencilling();

    ///--[Equipment Icons]
    //--Render equipment icons, based on the player's inventory.
    float cEquipmentLft = 540.0f;
    float cEquipmentTop = 268.0f;
    float cEquipmentHei =  45.0f;
    int cEquipmentSlotsTotal = rActiveEntity->GetEquipmentSlotsTotal();
    for(int i = 0; i < cEquipmentSlotsTotal; i ++)
    {
        //--Compute Y position.
        float cYPos = cEquipmentTop + (cEquipmentHei * i);

        //--Render the name of the slot.
        const char *rSlotName = rActiveEntity->GetNameOfEquipmentSlot(i);
        Images.StatusUI.rEquipmentFont->DrawText(cEquipmentLft, cYPos, 0, 1.0f, rSlotName);
        cYPos = cYPos + 16.0f;

        //--Get the item in the slot. If it's null, render an empty space.
        AdventureItem *rItem = rActiveEntity->GetEquipmentBySlotS(rSlotName);
        if(!rItem)
        {
            StarlightColor::cxGrey.SetAsMixerAlpha(cAlpha);
            Images.StatusUI.rEquipmentFont->DrawText(cEquipmentLft + 23.0f, cYPos + 2.0f, 0, 1.0f, "(Empty)");
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        }
        //--Item in slot. Render its icon and name.
        else
        {
            SugarBitmap *rImage = rItem->GetIconImage();
            if(rImage) rImage->Draw(cEquipmentLft, cYPos);
            Images.StatusUI.rEquipmentFont->DrawText(cEquipmentLft + 23.0f, cYPos + 2.0f, 0, 1.0f, rItem->GetName());
        }
    }

    ///--[Static Fronting]
    //--Parts that go over dynamic UI pieces.
    Images.StatusUI.rCharInfo->Draw();
    Images.StatusUI.rNavButtons->Draw();
    Images.StatusUI.rEXPBarFrame->Draw();
    Images.StatusUI.rHealthBarFrame->Draw();

    ///--[Text Rendering]
    //--Indicate there are abilities to be had by pressing down.
    float cOff = -4.0f;
    Images.StatusUI.rHeadingFont->DrawText(387.0f, 115.0f + cOff, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "Status");
    Images.StatusUI.rHeadingFont->DrawText(677.0f, 243.0f + cOff, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "Equipment");
    Images.StatusUI.rHeadingFont->DrawText(979.0f, 159.0f + cOff, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "Resistances");

    //--Character name.
    Images.StatusUI.rHeadingFont->DrawText(384.0f, 437.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, rActiveEntity->GetDisplayName());

    //--Health Bar.
    Images.StatusUI.rHealthBarUnder->Draw();
    float tHPPercent = rActiveEntity->GetHealthPercent();
    if(tHPPercent > 0.0f)
    {
        //--Rendering setup.
        float cLft = Images.StatusUI.rHealthBarFill->GetXOffset();
        float cTop = Images.StatusUI.rHealthBarFill->GetYOffset();
        float cRgt = Images.StatusUI.rHealthBarFill->GetXOffset() + (Images.StatusUI.rHealthBarFill->GetWidth() * tHPPercent);
        float cBot = Images.StatusUI.rHealthBarFill->GetYOffset() + Images.StatusUI.rHealthBarFill->GetHeight();
        float cTxL = 0.0f;
        float cTxT = 0.0f;
        float cTxR = tHPPercent;
        float cTxB = 1.0f;

        //--Flip since OpenGL is upside-down.
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;

        //--Render.
        Images.StatusUI.rHealthBarFill->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
            glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
            glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
            glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
        glEnd();
    }
    Images.StatusUI.rHealthBarFrame->Draw();

    //--Health Value.
    Images.StatusUI.rHealthFont->DrawTextArgs(384.0f, 490.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i/%i", rActiveEntity->GetHealth(), rActiveEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_HPMAX));

    //--Statistics. Render icons, then values.
    float cLftPos = 297.0f;
    float cRgtPos = 454.0f;
    Images.StatusUI.rStatIndicatorAtk->Draw(457.0f, 512.0f);
    Images.StatusUI.rStatIndicatorPrt->Draw(457.0f, 538.0f);
    Images.StatusUI.rStatIndicatorIni->Draw(457.0f, 564.0f);
    Images.StatusUI.rStatIndicatorAcc->Draw(457.0f, 590.0f);
    Images.StatusUI.rStatIndicatorEvd->Draw(457.0f, 616.0f);
    Images.StatusUI.rStatisticFont->DrawText(    cLftPos, 512.0f - 4.0f,                      0, 1.0f, "Attack");
    Images.StatusUI.rStatisticFont->DrawTextArgs(cRgtPos, 512.0f - 4.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i",         rActiveEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_ATTACK));
    Images.StatusUI.rStatisticFont->DrawText(    cLftPos, 538.0f - 4.0f,                      0, 1.0f, "Protection");
    Images.StatusUI.rStatisticFont->DrawTextArgs(cRgtPos, 538.0f - 4.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i",         rActiveEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_RESIST_PROTECTION));
    Images.StatusUI.rStatisticFont->DrawText(    cLftPos, 564.0f - 4.0f,                      0, 1.0f, "Initiative");
    Images.StatusUI.rStatisticFont->DrawTextArgs(cRgtPos, 564.0f - 4.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i",         rActiveEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_INITIATIVE));
    Images.StatusUI.rStatisticFont->DrawText(    cLftPos, 590.0f - 4.0f,                      0, 1.0f, "Accuracy");
    Images.StatusUI.rStatisticFont->DrawTextArgs(cRgtPos, 590.0f - 4.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i",         rActiveEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_ACCURACY));
    Images.StatusUI.rStatisticFont->DrawText(    cLftPos, 616.0f - 4.0f,                      0, 1.0f, "Evade");
    Images.StatusUI.rStatisticFont->DrawTextArgs(cRgtPos, 616.0f - 4.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i",         rActiveEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_EVADE));

    //--Next level. Can change to "MAX!" if the character is at the max level.
    int tCurrLevel = rActiveEntity->GetXPOfLevel();
    int tNextLevel = rActiveEntity->GetXPToNextLevel();
    int tNextLevelMax = rActiveEntity->GetXPToNextLevelMax();
    if(tNextLevel > 0 && tNextLevelMax > 0)
    {
        //--Text.
        Images.StatusUI.rMainlineFont->DrawTextArgs(586.0f, 176.0f, 0, 1.0f, "Exp: %i / %i", rActiveEntity->GetXP()-tCurrLevel, tNextLevelMax);

        //--We need to use manual rendering to indicate the bar's fill percentage.
        float tFillPercent = (float)(rActiveEntity->GetXP()-tCurrLevel) / (float)tNextLevelMax;
        float cLft = Images.StatusUI.rEXPBarFill->GetXOffset();
        float cTop = Images.StatusUI.rEXPBarFill->GetYOffset();
        float cRgt = cLft + (Images.StatusUI.rEXPBarFill->GetWidth() * tFillPercent);
        float cBot = Images.StatusUI.rEXPBarFill->GetYOffset() + (1.0f * Images.StatusUI.rEXPBarFill->GetHeight());
        float cTxL = 0.0f;
        float cTxT = 0.0f;
        float cTxR = tFillPercent;
        float cTxB = 1.0f;

        //--Flip the Y values of the textures since OpenGL is upside-down.
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;

        //--Render.
        Images.StatusUI.rEXPBarFill->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
            glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
            glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
            glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
        glEnd();

        //--Render the overlay.
        Images.StatusUI.rEXPBarFrame->Draw();

        //--Current level, XP needed to next.
        Images.StatusUI.rHeadingFont-> DrawTextArgs(545.0f, 148.0f, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "%i", rActiveEntity->GetLevel()+1);
    }
    else
    {
        //--Text.
        Images.StatusUI.rMainlineFont->DrawTextArgs(586.0f, 176.0f, 0, 1.0f, "Exp: Maxed");

        //--Bars. Render at 100% fill.
        Images.StatusUI.rEXPBarFill->Draw();
        Images.StatusUI.rEXPBarFrame->Draw();

        //--Just show the current level.
        Images.StatusUI.rHeadingFont-> DrawTextArgs(545.0f, 148.0f, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "%i", rActiveEntity->GetLevel()+1);
    }

    ///--[Resistances]
    //--Constants.
    float cTxtLft = 850.0f;
    float cTxtTop = 186.0f;
    float cIndentRgt = cTxtLft + 60.0f;
    float cRgtAlign = 1100.0f;
    float cSpacing = 23.0f;

    //--Colors
    StarlightColor cBonusColor = StarlightColor::MapRGBAF(0.8f, 0.1f, 0.8f, cAlpha);
    StarlightColor cMalusColor = StarlightColor::MapRGBAF(1.0f, 0.0f, 0.0f, cAlpha);

    //--Render the left column.
    for(int i = 0; i < ADVC_DAMAGE_TOTAL; i ++)
    {
        //--Icon.
        float cYPos = cTxtTop + (cSpacing * i);
        Images.StatusUI.rDamageTypeIcon[i]->Draw(cTxtLft, cYPos + 3.0f);

        //--If the resistance value is 1000, the value is "Immune".
        int tResistValue = rActiveEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_RESIST_START + i);
        if(tResistValue >= 1000)
        {
            Images.StatusUI.rResistanceFont->DrawText(cRgtAlign, cYPos, SUGARFONT_RIGHTALIGN_X, 1.0f, "Immune");
        }
        //--Otherwise, render computed percentage.
        else
        {
            //--If a bonus is applied, render the statistic in purple:
            int tBonusEqp = rActiveEntity->GetStatistic(ADVCE_STATS_EQUIPMENT, STATS_RESIST_START + i);
            int tBonusAbi = rActiveEntity->GetStatistic(ADVCE_STATS_TEMPEFFECT, STATS_RESIST_START + i);
            int tTotalBonus = tBonusEqp + tBonusAbi;
            if(tTotalBonus > 0)
            {
                cBonusColor.SetAsMixer();
            }
            //--If a malus is applied, render in red.
            else if(tTotalBonus < 0)
            {
                cMalusColor.SetAsMixer();
            }

            //--Resistance amount.
            Images.StatusUI.rResistanceFont->DrawTextArgs(cIndentRgt, cYPos, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tResistValue);

            //--Computed reduction.
            float cReduction = 1.0f - AdvCombatEntity::ComputeResistance(tResistValue);
            Images.StatusUI.rResistanceFont->DrawTextArgs(cRgtAlign, cYPos, SUGARFONT_RIGHTALIGN_X, 1.0f, "%0.0f%%", cReduction * 100.0f);
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        }
    }

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Help Menu]
    //--Help string.
    mShowHelpString->DrawText(0.0f, 0.0f, 0, 1.0f, Images.BaseMenu.rMainlineFont);

    //--Menu, if visible.
    if(mMainHelpTimer > 0)
    {
        //--Compute opacity.
        float cPercentage = EasingFunction::QuadraticInOut(mMainHelpTimer, ADV_HELP_STD_TICKS);
        SugarBitmap::DrawFullBlack(cPercentage * 0.75f);

        //--Offset.
        float tYOffset = ADV_HELP_STD_OFFSET * (1.0f - cPercentage);
        mMainMenuHelp->Render(tYOffset);
    }
}
