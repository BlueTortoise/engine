//--Base
#include "AdventureMenu.h"
#include "StarlightString.h"
#include "SugarLinkedList.h"
#include "DataLibrary.h"

#pragma once

//======================================== Definitions ============================================
//--Timers
#define ADVMENU_FIELDABILITY_VIS_TICKS 15
#define ADVMENU_FIELDABILITY_CURSOR_TICKS 15


//========================================= Structures ============================================
typedef struct AdvMenuFieldAbilityImages
{
    //--Fonts
    SugarFont *rHeaderFont;
    SugarFont *rMainFont;

    //--Images for this UI
    SugarBitmap *rHeader;
    SugarBitmap *rFooter;

    //--Functions
    void Construct()
    {
        //--Setup.
        DataLibrary *rDataLibrary = DataLibrary::Fetch();

        //--Fonts
        rHeaderFont = rDataLibrary->GetFont("Adventure Menu Field Abilities Header");
        rMainFont = rDataLibrary->GetFont("Adventure Menu Field Abilities Main");

        //--Images for this UI
        rHeader = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/FieldMenu/Header");
        rFooter = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/FieldMenu/Footer");
    }
}AdvMenuFieldAbilityImages;

typedef struct AdvMenuFieldAbilityVars
{
    //--System
    bool mIsFieldAbilityMode;
    int mVisibilityTimer;
    bool mIsSwitching;
    int mHiCursor;
    int mLoCursor;

    //--Field Ability List
    SugarLinkedList *mrFieldAbilityList; //FieldAbility *, reference

    //--Cursor Movement
    EasingPack2D mHighlightPos;
    EasingPack2D mHighlightSize;
    EasingPack2D mLolightPos;
    EasingPack2D mLolightSize;

    //--Functions
    void Initialize()
    {
        //--System
        mIsFieldAbilityMode = false;
        mVisibilityTimer = 0;
        mIsSwitching = false;
        mHiCursor = 0;
        mLoCursor = 0;

        //--Field Ability List
        mrFieldAbilityList = new SugarLinkedList(false);

        //--Cursor Movement
        mHighlightPos.Initialize();
        mHighlightSize.Initialize();
        mLolightPos.Initialize();
        mLolightSize.Initialize();
    }
    void Deallocate()
    {
        delete mrFieldAbilityList;
    }
}AdvMenuFieldAbilityVars;
