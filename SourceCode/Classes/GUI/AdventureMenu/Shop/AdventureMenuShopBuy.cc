//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--[Shop Fading]
#define FADE_MODE_NONE 0
#define FADE_INITIAL_OUT 1
#define FADE_INITIAL_HOLD 2
#define FADE_INITIAL_IN 3
#define FADE_FINAL_OUT 4
#define FADE_FINAL_HOLD 5
#define FADE_FINAL_IN 6

#define FADE_TICKS_OUT 15
#define FADE_TICKS_HOLD 30
#define FADE_TICKS_IN 15

//=========================================== System ==============================================
//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void AdventureMenu::UpdateShopBuy()
{
    ///--[Documentation and Setup]
    //--Quick-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Storage.
    int tOldCursor = mShopCursor;

    //--Always run this timer.
    mCharacterWalkTimer ++;

    ///--[Exchange Mode]
    //--In exchanging mode, prompt the player.
    if(mIsExchanging)
    {
        //--Setup.
        ShopInventoryPack *rItemPackage = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mShopCursor);
        if(!rItemPackage) return;

        //--Values.
        int tMaxBuy = 99;
        int tCurrentPlatina = rInventory->GetPlatina();
        int tPricePerItem = rItemPackage->mPriceOverride;
        if(tPricePerItem == -1) tPricePerItem = rItemPackage->mItem->GetValue();
        if(tPricePerItem > 0) tMaxBuy = tCurrentPlatina / tPricePerItem;

        //--If the item quantity in the shop inventory is limited, you can't buy more
        //  than that number.
        if(rItemPackage->mQuantity != -1 && tMaxBuy >= rItemPackage->mQuantity)
        {
            tMaxBuy = rItemPackage->mQuantity;
        }

        //--Left, decrement quantity.
        if(rControlManager->IsFirstPress("Left"))
        {
            mPurchaseQuantity --;
            if(mPurchaseQuantity < 1) mPurchaseQuantity = 1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Right, increment quantity.
        if(rControlManager->IsFirstPress("Right"))
        {
            mPurchaseQuantity ++;
            if(mPurchaseQuantity >= tMaxBuy) mPurchaseQuantity = tMaxBuy;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Up, increment quantity by 10.
        if(rControlManager->IsFirstPress("Up"))
        {
            mPurchaseQuantity += 10;
            if(mPurchaseQuantity >= tMaxBuy) mPurchaseQuantity = tMaxBuy;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Down, decrement quantity by 10.
        if(rControlManager->IsFirstPress("Down"))
        {
            mPurchaseQuantity -= 10;
            if(mPurchaseQuantity < 1) mPurchaseQuantity = 1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--If the player pushes "Activate", they will buy the item.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Get the quantity and decrement it. If it's -1, it is unlimited.
            if(rItemPackage->mQuantity > 0)
            {
                rItemPackage->mQuantity -= mPurchaseQuantity;
            }

            //--For each time the item was purchased:
            for(int i = 0; i < mPurchaseQuantity; i ++)
            {
                //--Put the item in the player's inventory if it was not adamantite.
                if(!rItemPackage->mItem->IsAdamantite())
                {
                    //--Simply run the construction script again.
                    const char *rItemName = rItemPackage->mItem->GetName();
                    LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 2, "S", rItemName, "N", 1.0f);
                }
                //--Adamantite. Has a special registering algorithm. If the item was flagged as "UNLIMITED" then we won't deallocate it.
                else
                {
                    rInventory->RegisterAdamantite(rItemPackage->mItem, false);
                }

                //--Decrement the player's cash. If the value is not -1, use the price override.
                if(rItemPackage->mPriceOverride > -1)
                {
                    rInventory->SetPlatina(rInventory->GetPlatina() - rItemPackage->mPriceOverride);
                }
                //--Use the value of the item.
                else
                {
                    rInventory->SetPlatina(rInventory->GetPlatina() - rItemPackage->mItem->GetValue());
                }
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");

            //--All cases end the exchange.
            mIsExchanging = false;
        }
    }
    ///--[Selecting Item]
    else
    {
        //--Setup.
        int tOldCursor = mShopCursor;

        //--Down increases the cursor.
        if(rControlManager->IsFirstPress("Down"))
        {
            //--Increment if in range.
            mShopCursor ++;
            if(mShopCursor >= mShopInventory->GetListSize()) mShopCursor = mShopInventory->GetListSize() - 1;
            if(mShopCursor < 0) mShopCursor = 0;
        }
        //--Up decreases the cursor.
        else if(rControlManager->IsFirstPress("Up"))
        {
            //--Increment if in range.
            mShopCursor --;
            if(mShopCursor < 0) mShopCursor = 0;
        }

        //--Get the item currently highlighted.
        ShopInventoryPack *rOldDescriptionPack = (ShopInventoryPack *)mShopInventory->GetElementBySlot(tOldCursor);
        ShopInventoryPack *rDescriptionItemPack = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mShopCursor);

        //--If the old and new packs are not the same, we may need to reset variables.
        if(rOldDescriptionPack != rDescriptionItemPack)
        {
            //--Make sure the item in both slots exists.
            if(rOldDescriptionPack->mItem && rDescriptionItemPack->mItem)
            {
                if(!rOldDescriptionPack->mItem->MatchesEquippable(rDescriptionItemPack->mItem))
                {
                    mComparisonSlot = 0;
                }
            }
            //--Clear.
            else
            {
                mComparisonSlot = 0;
            }
        }

        //--Left moves the comparison cursor down one.
        if(rControlManager->IsFirstPress("Left"))
        {
            //--Get the item under consideration.
            if(!rDescriptionItemPack || !rDescriptionItemPack->mItem) return;

            //--Attempt to decrement by one until a valid character is found.
            for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE - 1; i ++)
            {
                //--Compute slot.
                int tSlot = (mComparisonCharacter - i - 1);
                if(tSlot < 0) tSlot = ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE + tSlot;

                //--Get the character.
                AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(tSlot);
                if(!rCharacter) continue;

                //--If the character exists and can use the item, stop.
                if(rDescriptionItemPack->mItem->IsEquippableBy(rCharacter->GetName()))
                {
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    mComparisonCharacter = tSlot;
                    return;
                }
            }
        }
        //--Right moves the comparison cursor up one.
        else if(rControlManager->IsFirstPress("Right"))
        {
            //--Get the item under consideration.
            if(!rDescriptionItemPack || !rDescriptionItemPack->mItem) return;

            //--Attempt to decrement by one until a valid character is found.
            for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE - 1; i ++)
            {
                //--Compute slot.
                int tSlot = (mComparisonCharacter + i + 1) % ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE;

                //--Get the character.
                AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(tSlot);
                if(!rCharacter) continue;

                //--If the character exists and can use the item, stop.
                if(rDescriptionItemPack->mItem->IsEquippableBy(rCharacter->GetName()))
                {
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    mComparisonCharacter = tSlot;
                    return;
                }
            }
        }

        //--UpLevel moves the equipment comparison cursor up one.
        bool tIsUpLevelPressed = rControlManager->IsFirstPress("UpLevel");
        bool tIsDnLevelPressed = rControlManager->IsFirstPress("DnLevel");
        if(tIsDnLevelPressed || tIsUpLevelPressed)
        {
            //--Get the item under consideration.
            if(!rDescriptionItemPack || !rDescriptionItemPack->mItem) return;

            //--Get the character.
            AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(mComparisonCharacter);
            if(rCharacter)
            {
                //--Figure out how many slots match the item's equipment slot.
                int tSlotsTotal = 0;
                int tEquipSlotsTotal = rCharacter->GetEquipmentSlotsTotal();
                for(int i = 0; i < tEquipSlotsTotal; i ++)
                {
                    const char *rPackageName = rCharacter->GetNameOfEquipmentSlot(i);
                    if(rPackageName && rDescriptionItemPack->mItem->IsEquippableIn(rPackageName))
                    {
                        tSlotsTotal ++;
                    }
                }

                //--Decrement:
                if(tIsUpLevelPressed)
                {
                    mComparisonSlot --;
                    if(mComparisonSlot < 0) mComparisonSlot = tSlotsTotal - 1;
                    if(mComparisonSlot < 0) mComparisonSlot = 0;
                }
                //--Increment:
                else
                {
                    mComparisonSlot ++;
                    if(mComparisonSlot >= tSlotsTotal) mComparisonSlot = 0;
                }

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--Activate will purchase the item.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Check the item.
            ShopInventoryPack *rBuyingItemPack = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mShopCursor);
            if(!rBuyingItemPack) return;

            //--Check the price.
            int tPrice = rBuyingItemPack->mPriceOverride;
            if(tPrice == -1) tPrice = rBuyingItemPack->mItem->GetValue();

            //--If the player can afford the item, add it to their inventory.
            if(tPrice <= rInventory->GetPlatina())
            {
                mIsExchanging = true;
                mPurchaseQuantity = 1;
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Otherwise, play a sound effect to indicate failure.
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
        }
    }

    ///--[Item Change Handler]
    //--If the item changes, recheck the comparison character case. Also play sound effects.
    if(tOldCursor != mShopCursor)
    {
        //--[SFX]
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--[Recentering]
        //--On a decrement:
        if(mShopCursor < tOldCursor)
        {
            //--Get where the cursor is on the page.
            int tPosition = mShopCursor - mShopSkip;
            if(tPosition < 2) mShopSkip = mShopCursor - 2;

            //--Clamp.
            if(mShopSkip < 0) mShopSkip = 0;
        }
        //--On an increment:
        else if(mShopCursor > tOldCursor)
        {
            //--Get where the cursor is on the page.
            int tPosition = mShopCursor - mShopSkip;
            if(tPosition > AM_SHOP_ITEMS_PER_PAGE - 3) mShopSkip = mShopCursor - (AM_SHOP_ITEMS_PER_PAGE - 3);

            //--Clamp.
            if(mShopSkip > mShopInventory->GetListSize() - AM_SHOP_ITEMS_PER_PAGE) mShopSkip = mShopInventory->GetListSize() - AM_SHOP_ITEMS_PER_PAGE;
        }

        //--[Rechecking Comparison Character]
        //--Flag.
        int tPreviousComparison = mComparisonCharacter;

        //--Get the item under consideration.
        ShopInventoryPack *rDescriptionItemPack = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mShopCursor);
        if(!rDescriptionItemPack) return;

        //--If the item is not a piece of equipment, we can't compare it to anything.
        if(!rDescriptionItemPack->mItem->IsEquipment()) return;

        //--Check if the currently selected character can use the item.
        AdvCombatEntity *rComparisonCharacter = AdvCombat::Fetch()->GetActiveMemberI(mComparisonCharacter);
        if(rComparisonCharacter && rDescriptionItemPack->mItem->IsEquippableBy(rComparisonCharacter->GetName()))
        {

        }
        //--Otherwise, start at character zero and run through the list.
        else
        {
            //--Iterate.
            int tTotalCharacters = 0;
            for(int i = 0; i < ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE; i ++)
            {
                //--Get the character.
                AdvCombatEntity *rCharacter = AdvCombat::Fetch()->GetActiveMemberI(i);
                if(!rCharacter) continue;

                //--If it's usable by this character, stop here, flag.
                if(rDescriptionItemPack->mItem->IsEquippableBy(rCharacter->GetName()))
                {
                    //--Flag. The first character found becomes the comparison character.
                    if(tTotalCharacters == 0) mComparisonCharacter = i;

                    //--The previous character, even if not the first, becomes the comparison character. This way
                    //  the UI does not switch comparison characters if still valid.
                    if(tPreviousComparison == i) mComparisonCharacter = i;

                    //--Track the total number of comparable characters.
                    tTotalCharacters ++;
                }
            }
        }

        //--If the comparison character changed, reset this flag.
        if(tPreviousComparison != mComparisonCharacter)
        {
            mComparisonSlot = 0;
        }
    }
}

//=========================================== Drawing =============================================
void AdventureMenu::RenderShopBuy(bool pIsActiveMode, float pColorMix)
{
    ///--[Documentation and Setup]
    //--Render the buying portion of the submenu. The static backing and text are already rendered, we just need to
    //  render the item icons and descriptions of the highlighted item if applicable.
    if(!Images.mIsReady) return;

    ///--[Shop Inventory Listing]
    //--Similar to how the inventory renders information as a table. Item properties are not listed, though.
    float cHeaderY = 124.0f;
    float cIconX  = 30.0f;
    float cIconW  = 22.0f;
    float cFaceX = cIconX + cIconW;
    float cFaceW = 32.0f;
    float cNameX  = cFaceX + cFaceW;
    float cValueX = 360.0f;
    float cValueXRight = 432.0f;
    float cGemsX = 445.0f;
    float cUnderlayLft =  15.0f;
    float cUnderlayRgt = 579.0f;
    float cDescriptionLft = 557.0f;

    //--Text headers.
    StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
    Images.EquipmentUI.rHeadingFont->DrawText(cIconX,  cHeaderY, 0, 1.0f, "Name");
    Images.EquipmentUI.rHeadingFont->DrawText(cValueX, cHeaderY, 0, 1.0f, "Cost");
    Images.EquipmentUI.rHeadingFont->DrawText(cGemsX,  cHeaderY, 0, 1.0f, "Gems");

    //--Y positioning
    float cTop = cHeaderY + 44.0f;
    float tCurrentY = cTop;
    float cSpcY = 23.0f;

    //--Variables.
    int tRendersLeft = AM_SHOP_ITEMS_PER_PAGE;
    int tSkipsLeft = mShopSkip;

    //--[Rendering Loop]
    int tCurrentItem = 0;
    ShopInventoryPack *rItemPack = (ShopInventoryPack *)mShopInventory->PushIterator();
    while(rItemPack)
    {
        //--If this item should not render, skip it.
        if(tSkipsLeft > 0)
        {
            tCurrentItem ++;
            tSkipsLeft --;
            rItemPack = (ShopInventoryPack *)mShopInventory->AutoIterate();
            continue;
        }

        //--[Backing]
        if(tCurrentItem % 2 == 1)
        {
            SugarBitmap::DrawRectFill(cUnderlayLft, tCurrentY, cUnderlayRgt, tCurrentY + cSpcY, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
            StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
        }

        //--[Item Icon]
        //--Render the item's icon here. It's a 22px entry.
        SugarBitmap *rItemIcon = rItemPack->mItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(cIconX, tCurrentY);

        //--Compute value.
        int tValue = rItemPack->mPriceOverride;
        if(tValue == -1) tValue = rItemPack->mItem->GetValue();

        //--[Cursor Handling]
        //--If this is the selected item, render the cursor over it. If the mode is inactive, don't render this.
        //  Inactive mode is used when the player is hovering on Buy mode, but hasn't entered it yet.
        if(tCurrentItem == mShopCursor && pIsActiveMode)
        {
            //--Render the item description.
            RenderItemDescription(cDescriptionLft, rItemPack->mItem);

            //--Render the properties and comparisons.
            RenderShopBuyProperties(rItemPack->mItem, mComparisonCharacter, pColorMix);

            //--Player cannot afford the item, so grey it out.
            if(tValue > AdventureInventory::Fetch()->GetPlatina())
            {
                StarlightColor::SetMixer(1.0f * pColorMix, 0.0f * pColorMix, 0.0f * pColorMix, 1.0f);
            }
            //--Regular mixing.
            else
            {
                StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            }

            //--Tint the color.
            StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
        }
        //--Not selected or is inactive mode.
        else
        {

            //--Player cannot afford the item, so grey it out.
            if(tValue > AdventureInventory::Fetch()->GetPlatina())
            {
                StarlightColor::SetMixer(0.5f * pColorMix, 0.5f * pColorMix, 0.5f * pColorMix, 1.0f);
            }
            //--Regular mixing.
            else
            {
                StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            }
        }

        //--[Item Name]
        //--Name.
        Images.EquipmentUI.rMainlineFont->DrawText(cNameX, tCurrentY, 0, 1.0f, rItemPack->mItem->GetName());

        //--[Item Cost]
        //--Right-align.
        char tBuffer[32];
        sprintf(tBuffer, "%i", tValue);

        //--Resolve position and render.
        float cXPos = cValueXRight - Images.EquipmentUI.rMainlineFont->GetTextWidth(tBuffer);
        Images.EquipmentUI.rMainlineFont->DrawTextArgs(cXPos, tCurrentY, 0, 1.0f, tBuffer);
        StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);

        //--[Gem Slots]
        //--Items which have gem slots render them here. They are always empty in the shop UI.
        float cGemW = 23.0f;
        int tGemSlots = rItemPack->mItem->GetGemSlots();
        for(int i = 0; i < tGemSlots; i ++)
        {
            Images.VendorUI.rGemEmpty22px->Draw(cGemsX + (i * cGemW), tCurrentY);
        }

        //--If the renders have run out, stop here.
        tRendersLeft --;
        if(tRendersLeft < 1)
        {
            mShopInventory->PopIterator();
            break;
        }

        //--Next.
        tCurrentItem ++;
        tCurrentY = tCurrentY + cSpcY;
        rItemPack = (ShopInventoryPack *)mShopInventory->AutoIterate();
    }

    ///--[Scrollbar]
    //--Render a scrollbar to indicate how much of the inventory is represented.
    if(mShopInventory->GetListSize() > AM_SHOP_ITEMS_PER_PAGE)
    {
        //--Binding.
        Images.VendorUI.rScrollbarFront->Bind();

        //--Scrollbar front. Stretches dynamically.
        float cScrollTop = 158.0f;
        float cScrollHei = 366.0f;

        //--Determine what percentage of the inventory is presently represented.
        int tMaxOffset = mShopInventory->GetListSize();
        float cPctTop = (mShopSkip                         ) / (float)tMaxOffset;
        float cPctBot = (mShopSkip + AM_SHOP_ITEMS_PER_PAGE) / (float)tMaxOffset;

        //--Positions and Constants.
        float cLft = 548.0f;
        float cRgt = cLft + Images.InventoryUI.rScrollbarFront->GetWidth();
        float cEdg = 6.0f / (float)Images.InventoryUI.rScrollbarFront->GetHeight();

        //--Compute where the bar should be.
        float cTopTop = cScrollTop + (cPctTop * cScrollHei);
        float cTopBot = cTopTop + 6.0f;
        float cBotBot = cScrollTop + (cPctBot * cScrollHei);
        float cBotTop = cBotBot - 6.0f;

        glBegin(GL_QUADS);
            //--Render the top of the bar.
            glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTopTop);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTopTop);
            glTexCoord2f(1.0f, cEdg); glVertex2f(cRgt, cTopBot);
            glTexCoord2f(0.0f, cEdg); glVertex2f(cLft, cTopBot);

            //--Render the bottom of the bar.
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(1.0f, 1.0f);        glVertex2f(cRgt, cBotBot);
            glTexCoord2f(0.0f, 1.0f);        glVertex2f(cLft, cBotBot);

            //--Render the middle of the bar.
            glTexCoord2f(0.0f, cEdg);        glVertex2f(cLft, cTopBot);
            glTexCoord2f(1.0f, cEdg);        glVertex2f(cRgt, cTopBot);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
        glEnd();


        //--Backing. Already positioned.
        Images.VendorUI.rScrollbarBack->Draw();
    }

    ///--[Exchange Dialogue]
    //--In exchange mode, render the confirmation dialogue.
    if(mIsExchanging)
    {
        //--Get the item and verify it.
        ShopInventoryPack *rPurchaseItem = (ShopInventoryPack *)mShopInventory->GetElementBySlot(mShopCursor);
        if(!rPurchaseItem) return;

        //--Black out.
        SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.75f));

        //--Overlay.
        Images.VendorUI.rConfirmOverlay->Draw();

        //--Compute value.
        int tValue = rPurchaseItem->mPriceOverride;
        if(tValue == -1) tValue = rPurchaseItem->mItem->GetValue();

        //--Buffer text.
        char tBuffer[128];
        sprintf(tBuffer, "Purchase %sx%i for %i platina?", rPurchaseItem->mItem->GetName(), mPurchaseQuantity, tValue);

        //--Check the length.
        float tTextLength = Images.VendorUI.rHeadingFont->GetTextWidth(tBuffer);

        //--Width is not too long. Render normally.
        if(tTextLength < 500.0f)
        {
            Images.VendorUI.rHeadingFont->DrawText(683.0f, 210.0f, SUGARFONT_AUTOCENTER_X, 1.0f, tBuffer);
        }
        //--Too long, split it into two lines.
        else
        {
            Images.VendorUI.rHeadingFont->DrawTextArgs(683.0f, 210.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Purchase %sx%i", rPurchaseItem->mItem->GetName(), mPurchaseQuantity);
            Images.VendorUI.rHeadingFont->DrawTextArgs(683.0f, 245.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "for %i Platina?", tValue);
        }

        //--Other text.
        Images.VendorUI.rMainlineFont->DrawTextArgs(683.0f, 345.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Z to confirm");
        Images.VendorUI.rMainlineFont->DrawTextArgs(683.0f, 365.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "X to cancel");
        Images.VendorUI.rMainlineFont->DrawTextArgs(683.0f, 385.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Arrow keys to change amount");
    }
}
void AdventureMenu::RenderShopBuyProperties(AdventureItem *pItem, int pComparisonCharacter, float pColorMix)
{
    ///--[Documentation and Setup]
    //--Renders the properties of the item on the right side of the screen. For items that can be equipped, we also
    //  show which of the characters in the party can equip that item and comparisons with what they have equipped.
    //--Not all items can be easily compared, but we do our best!
    //--It is assumed that the Buy menu is active when this is taking place. If it is not active, no item should be selected.
    if(!pItem) return;

    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Positions.
    float cLftAlign = 600.0f;
    float tTopAlign = 130.0f;
    float cYSpc = 20.0f;

    //--Timers.
    int cTicksPerFrame = 15;

    //--Colors.
    float cHalfMix = pColorMix * 0.50f;

    ///--[Basic Information]
    //--Heading.
    Images.EquipmentUI.rHeadingFont->DrawTextArgs(cLftAlign, tTopAlign, 0, 1.0f, "Information");
    tTopAlign = tTopAlign + 33.0f;

    //--Render how many of the item we already have, if applicable.
    int tCurrentStock = AdventureInventory::Fetch()->GetCountOf(pItem->GetName());
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cLftAlign, tTopAlign + (cYSpc * 0.0f), 0, 1.0f, "You have: %i", tCurrentStock);

    //--What type the item is:
    const char *rTypeBuffer = pItem->GetItemTypeString();
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cLftAlign, tTopAlign + (cYSpc * 1.0f), 0, 1.0f, "Type: %s", rTypeBuffer);

    ///--[Equipment Comparison]
    //--If this is a piece of equipment, we need to show who can equip it and what properties it has.
    if(pItem->IsEquipment())
    {
        //--Heading.
        tTopAlign = tTopAlign + (cYSpc * 2.0f);
        Images.EquipmentUI.rHeadingFont->DrawTextArgs(cLftAlign, tTopAlign, 0, 1.0f, "Comparison");
        tTopAlign = tTopAlign + 43.0f;

        //--Character string.
        mChangeCharString->DrawText(cLftAlign, tTopAlign, 0, 1.0f, Images.EquipmentUI.rMainlineFont);
        tTopAlign = tTopAlign + 20.0f;

        //--Character sizing.
        float cCharSpaceX = 68.0f;

        //--Render the party.
        int tPartySize = rAdventureCombat->GetActivePartyCount();
        for(int i = 0; i < tPartySize; i ++)
        {
            //--Get the member.
            AdvCombatEntity *rEntity = rAdventureCombat->GetActiveMemberI(i);
            if(!rEntity) continue;

            //--Determine if this entity can use the item.
            bool tCanUseItem = pItem->IsEquippableBy(rEntity->GetName());

            //--Resolve frame.
            int tUseFrame = 0;
            if(tCanUseItem) tUseFrame = (mCharacterWalkTimer / cTicksPerFrame) % ADVCE_VENDOR_SPRITES_TOTAL;
            SugarBitmap *rImage = rEntity->GetVendorImage(tUseFrame);

            //--Render.
            if(rImage)
            {
                if(!tCanUseItem) StarlightColor::SetMixer(cHalfMix, cHalfMix, cHalfMix, 1.0f);
                rImage->DrawScaled(cLftAlign + (cCharSpaceX * i), tTopAlign, 3.0f, 3.0f);
                if(!tCanUseItem) StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            }

            //--If the character is the comparison character, render an arrow under them.
            if(i == mComparisonCharacter)
            {
                Images.VendorUI.rCompareCursor->Draw(cLftAlign + 33.0f + (cCharSpaceX * i), tTopAlign + 98.0f);
            }
        }
        tTopAlign = tTopAlign + 120.0f;

        //--Get the selected comparison character. Show their equipment that matches the slot.
        AdvCombatEntity *rCompareCharacter = rAdventureCombat->GetActiveMemberI(mComparisonCharacter);
        if(rCompareCharacter && pItem->IsEquippableBy(rCompareCharacter->GetName()))
        {
            //--Header.
            mChangeItemString->DrawText(cLftAlign, tTopAlign, 0, 1.0f, Images.EquipmentUI.rMainlineFont);
            tTopAlign = tTopAlign + 25.0f;

            //--Position setup.
            int tCurrentSlot = 0;
            float tXPos = cLftAlign;
            float cGemW = 23.0f;
            float cGemL = 900.0f;

            //--Storage.
            AdventureItem *rCompareItem = NULL;

            //--Iterate across the slots.
            int tSlotsTotal = rCompareCharacter->GetEquipmentSlotsTotal();
            for(int i = 0; i < tSlotsTotal; i ++)
            {
                //--Get package.
                const char *rPackageName = rCompareCharacter->GetNameOfEquipmentSlot(i);
                EquipmentSlotPack *rEquipmentPackage = rCompareCharacter->GetEquipmentSlotPackageI(i);
                if(!rPackageName || !rEquipmentPackage) continue;

                //--If the item cannot be equipped in this slot, ignore it.
                if(!pItem->IsEquippableIn(rPackageName)) continue;

                //--Item can be equipped. Show what is in the slot. If nothing, render empty.
                if(!rEquipmentPackage->mEquippedItem)
                {
                    //--Render slot name, greyed out.
                    StarlightColor::SetMixer(cHalfMix, cHalfMix, cHalfMix, 1.0f);
                    if(mComparisonSlot == tCurrentSlot) StarlightColor::SetMixer(cHalfMix, 0.0f, 0.0f, 1.0f);
                    Images.EquipmentUI.rMainlineFont->DrawText(tXPos+23.0f, tTopAlign, 0, 1.0f, rPackageName);
                    StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
                }
                //--Render the item's information.
                else
                {
                    //--Store.
                    if(mComparisonSlot == tCurrentSlot) rCompareItem = rEquipmentPackage->mEquippedItem;

                    //--Get description pieces.
                    const char *rRendername = rEquipmentPackage->mEquippedItem->GetName();
                    SugarBitmap *rIcon = rEquipmentPackage->mEquippedItem->GetIconImage();

                    //--Render icon.
                    if(rIcon) rIcon->Draw(tXPos, tTopAlign);

                    //--Render name.
                    if(mComparisonSlot == tCurrentSlot) StarlightColor::SetMixer(pColorMix, 0.0f, 0.0f, 1.0f);
                    Images.EquipmentUI.rMainlineFont->DrawText(tXPos+23.0f, tTopAlign, 0, 1.0f, rRendername);
                    if(mComparisonSlot == tCurrentSlot) StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);

                    //--Gem slots.
                    int tGemSlots = rEquipmentPackage->mEquippedItem->GetGemSlots();
                    for(int p = 0; p < tGemSlots; p ++)
                    {
                        AdventureItem *rGem = rEquipmentPackage->mEquippedItem->GetGemInSlot(p);
                        Images.VendorUI.rGemEmpty22px->Draw(cGemL + (p * cGemW), tTopAlign);
                        if(rGem)
                        {
                            SugarBitmap *rGemImg = rGem->GetIconImage();
                            if(rGemImg) rGemImg->Draw(cGemL + (p * cGemW), tTopAlign);
                        }
                    }
                }

                //--Modify the slot counter.
                tCurrentSlot ++;

                //--Increment.
                tTopAlign = tTopAlign + 20.0f;
            }

            //--Render comparison. rCompareItem can legally be NULL.
            RenderShopBuyComparison(rCompareCharacter, false, rCompareItem, pItem);
        }
    }
    ///--[Gem Properties]
    //--If this item is a gem, show its properties.
    else if(pItem->IsGem())
    {
        RenderShopGemProperties(true, pItem, NULL);
    }
}
void AdventureMenu::RenderShopStatComparison(bool pDontRenderSecondColumn, CombatStatistics *pPackA, CombatStatistics *pPackB)
{
    ///--[Documentation and Setup]
    //--Renders the statistical difference between the two packages provided.
    if(!pPackA || !pPackB) return;

    //--Positions.
    float cHeaderXPos = 1235.0f;
    float tLXPos      = 1150.0f;
    float tAXPos      = 1237.0f; //Right-align
    float tBXPos      = 1240.0f;
    float tCXPos      = 1318.0f; //Right-align
    float tStatY      = 115.0f;
    float tResistY    = 306.0f;
    float tYSpc       =  21.0f;

    //--Headers.
    Images.FormUI.rHeadingFont->DrawText(cHeaderXPos,  64.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Stats");
    Images.FormUI.rHeadingFont->DrawText(cHeaderXPos, 255.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Resistances");

    //--Other.
    StarlightColor cColorVL = StarlightColor::MapRGBAF(1.0f, 0.0f, 0.0f, 1.0f);
    StarlightColor cColorLo = StarlightColor::MapRGBAF(1.0f, 0.5f, 0.0f, 1.0f);
    StarlightColor cColorNe = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    StarlightColor cColorHi = StarlightColor::MapRGBAF(0.5f, 0.5f, 1.0f, 1.0f);
    StarlightColor cColorVH = StarlightColor::MapRGBAF(0.2f, 0.9f, 0.3f, 1.0f);

    //--Render the icons.
    for(int i = 0; i < AM_STAT_COMPARE_PACKS_TOTAL; i ++)
    {
        //--Verify the image.
        if(!mStatComparePacks[i].rImage) continue;

        //--Compute Y position. It jumps when resistances get computed.
        float cYPos = tStatY + (tYSpc * i);
        if(i >= AM_STAT_COMPARE_PACK_PROTECTION) cYPos = tResistY + (tYSpc * (i-AM_STAT_COMPARE_PACK_PROTECTION));

        //--Render the image.
        mStatComparePacks[i].rImage->Draw(tLXPos, cYPos);

        //--Index.
        int p = mStatComparePacks[i].mIndex;

        //--Get comparisons and resolve color.
        int tValueL = pPackA->mValueList[p];
        int tValueR = pPackB->mValueList[p];

        //--Special: If both values are 1000 or higher, print "Immune" across the stat line.
        if(tValueL >= 1000 && tValueR >= 1000)
        {
            Images.FormUI.rMainlineFont->DrawTextArgs(tCXPos, cYPos-4, SUGARFONT_RIGHTALIGN_X, 1.0f, "Immune");
            continue;
        }

        //--Render the stat of the base.
        if(tValueL >= 1000)
            Images.FormUI.rMainlineFont->DrawTextArgs(tAXPos, cYPos-4, SUGARFONT_RIGHTALIGN_X, 1.0f, "IMN");
        else
            Images.FormUI.rMainlineFont->DrawTextArgs(tAXPos, cYPos-4, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pPackA->mValueList[p]);

        //--Stop rendering here if flagged.
        if(pDontRenderSecondColumn)
        {
            StarlightColor::ClearMixer();
            continue;
        }

        //--Render an arrow.
        Images.FormUI.rMainlineFont->DrawTextArgs(tBXPos, cYPos-6, 0, 1.0f, "->");

        //--Resolve comparison color.
        if(tValueR < tValueL * 0.75f)
            cColorVL.SetAsMixer();
        else if(tValueR < tValueL * 1.00f)
            cColorLo.SetAsMixer();
        else if(tValueR == tValueL * 1.00f)
            cColorNe.SetAsMixer();
        else if(tValueR < tValueL * 1.25f)
            cColorHi.SetAsMixer();
        else
            cColorVH.SetAsMixer();

        //--Render the comparison value.
        if(tValueR >= 1000)
            Images.FormUI.rMainlineFont->DrawTextArgs(tCXPos, cYPos-4, SUGARFONT_RIGHTALIGN_X, 1.0f, "IMN");
        else
            Images.FormUI.rMainlineFont->DrawTextArgs(tCXPos, cYPos-4, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", pPackB->mValueList[p]);
        StarlightColor::ClearMixer();
    }
}
void AdventureMenu::RenderShopBuyComparison(AdvCombatEntity *pCharacter, bool pIgnoreGems, AdventureItem *pOldItem, AdventureItem *pNewItem)
{
    ///--[Documentation and Setup]
    //--Renders a comparison between an existing item and a new item. The existing item is optional, if pOldItem is NULL then
    //  zeroes will be used for its values.
    //--Optionally, gems may be ignored. This can be toggled by the player to allow comparison of an item's base statistics.
    if(!pCharacter || !pNewItem) return;

    //--Storage structures.
    CombatStatistics tOldItemStats;
    CombatStatistics tNewItemStats;
    CombatStatistics tCharStatistics;
    tOldItemStats.Zero();
    tNewItemStats.Zero();
    tCharStatistics.Zero();

    //--Get the character's base statistics.
    CombatStatistics *rCharStatistics = pCharacter->GetStatisticsGroup(ADVCE_STATS_FINAL);
    memcpy(&tCharStatistics, rCharStatistics, sizeof(CombatStatistics));

    //--Get stats for the new item. Copy them into the temp structure.
    CombatStatistics *rNewItemStats = pNewItem->GetFinalStatistics();
    if(pIgnoreGems) rNewItemStats = pNewItem->GetEquipStatistics();
    memcpy(&tNewItemStats, rNewItemStats, sizeof(CombatStatistics));

    //--Old item stats are zero by default. If an item is provided, they get overwritten.
    //  The item also removes itself from the character's statistics.
    if(pOldItem)
    {
        CombatStatistics *rCopyStats = pOldItem->GetFinalStatistics();
        memcpy(&tOldItemStats, rCopyStats, sizeof(CombatStatistics));
        for(int i = 0; i < STATS_TOTAL; i ++)
        {
            tCharStatistics.SetStatByIndex(i, tCharStatistics.GetStatByIndex(i) - tOldItemStats.GetStatByIndex(i));
        }
    }

    //--Now add the character stats to both comparisons.
    for(int i = 0; i < STATS_TOTAL; i ++)
    {
        int tCharStat = tCharStatistics.GetStatByIndex(i);
        tOldItemStats.SetStatByIndex(i, tOldItemStats.GetStatByIndex(i) + tCharStat);
        tNewItemStats.SetStatByIndex(i, tNewItemStats.GetStatByIndex(i) + tCharStat);
    }

    //--Rendering is handled by a subroutine.
    RenderShopStatComparison(false, &tOldItemStats, &tNewItemStats);
}
void AdventureMenu::RenderShopGemProperties(bool pDontRenderModification, AdventureItem *pCurGem, AdventureItem *pNewGem)
{
    ///--[Documentation and Setup]
    //--Renders a gem's properties. Can optionally be provided with a second gem which adds to the first gem. This
    //  functionality is used in gemcutter mode.
    if(!pCurGem) return;

    //--Get the stats out of the first gem.
    CombatStatistics *rFirstGemStats = pCurGem->GetFinalStatistics();
    CombatStatistics *rNextGemStats = rFirstGemStats;

    //--If a second gem was passed in, use its stats.
    if(pNewGem) rNextGemStats = pNewGem->GetFinalStatistics();

    //--Run the renderer.
    RenderShopStatComparison(pDontRenderModification, rFirstGemStats, rNextGemStats);
}
