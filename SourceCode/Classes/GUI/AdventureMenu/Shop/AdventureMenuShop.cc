//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--[Shop Fading]
#define FADE_MODE_NONE 0
#define FADE_INITIAL_OUT 1
#define FADE_INITIAL_HOLD 2
#define FADE_INITIAL_IN 3
#define FADE_FINAL_OUT 4
#define FADE_FINAL_HOLD 5
#define FADE_FINAL_IN 6

#define FADE_TICKS_OUT 15
#define FADE_TICKS_HOLD 30
#define FADE_TICKS_IN 15

//=========================================== System ==============================================
void AdventureMenu::InitializeShopMode(const char *pShopName, const char *pPath, const char *pTeardownPath)
{
    //--[Documentation and Setup]
    //--Boots shopping mode, using the provided path to get information about the items. Note that the path
    //  is executed like any other script, so it could fire a cutscene instead of specifying shop information.
    //--If the script is NULL, the shop information is cleared and shop mode is deactivated.
    mShopCanGemcut = false;
    mShopName[0] = '\0';
    mShopModeType = AM_SELECT_SHOP_MODE;
    mShopCursor = 0;
    mIsExchanging = false;
    ResetString(mShopTeardownPath, NULL);
    mShopInventory->ClearList();
    mCharacterWalkTimer = 0;
    if(!pShopName || !pPath || !pTeardownPath) return;

    //--Set the shop name.
    strncpy(mShopName, pShopName, STD_MAX_LETTERS - 1);
    ResetString(mShopTeardownPath, pTeardownPath);

    //--Run the provided script.
    LuaManager::Fetch()->ExecuteLuaFile(pPath);

    //--Set initial variables.
    mIsShopMode = true;
    mShopFadeMode = FADE_INITIAL_OUT;
    mShopFadeTimer = 0;

    //--Initialize these strings. They must be re-initted every time this opens since the
    //  control key in question can change.
    mChangeCharString->SetString("[IMG0] Change Characters [IMG1]");
    mChangeCharString->AllocateImages(2);
    mChangeCharString->SetImageP(0, 4.0f, ControlManager::Fetch()->ResolveControlImage("Left"));
    mChangeCharString->SetImageP(1, 4.0f, ControlManager::Fetch()->ResolveControlImage("Right"));
    mChangeCharString->CrossreferenceImages();

    mChangeItemString->SetString("[IMG0] Change Items [IMG1]");
    mChangeItemString->AllocateImages(2);
    mChangeItemString->SetImageP(0, 4.0f, ControlManager::Fetch()->ResolveControlImage("UpLevel"));
    mChangeItemString->SetImageP(1, 4.0f, ControlManager::Fetch()->ResolveControlImage("DnLevel"));
    mChangeItemString->CrossreferenceImages();
}
void AdventureMenu::InitializeGemcutterMode()
{
    //--When fired, allows the shop to cut gems.
    mShopCanGemcut = true;

    //--Rebuild the gems list.
    AdventureInventory::Fetch()->BuildGemList();
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void AdventureMenu::RegisterItem(const char *pName, int pPriceOverride, int pQuantity)
{
    //--Registers an item for sale. Items are cloned when purchased. If the price override value is
    //  -1, then the price is the default value for the item. If the quantity is -1, then the item
    //  may be purchased an unlimited number of times.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    if(!pName || !AdventureLevel::xItemListPath) return;

    //--Run the item list code to create the item in question.
    rInventory->rLastReggedItem = NULL;
    rInventory->mBlockStackingOnce = true;
    LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 2, "S", pName, "N", 1.0f);
    rInventory->mBlockStackingOnce = false;

    //--Unregister the item that was last created and add it to our local list instead.
    AdventureItem *rLastItem = (AdventureItem *)rInventory->rLastReggedItem;
    if(!rLastItem) return;
    rInventory->LiberateItemP(rLastItem);

    //--Create a package to hold the item.
    SetMemoryData(__FILE__, __LINE__);
    ShopInventoryPack *nPackage = (ShopInventoryPack *)starmemoryalloc(sizeof(ShopInventoryPack));
    nPackage->Initialize();
    nPackage->mItem = rLastItem;
    nPackage->mPriceOverride = pPriceOverride;
    nPackage->mQuantity = pQuantity;

    //--All checks passed, register.
    mShopInventory->AddElementAsTail("X", nPackage, &ShopInventoryPack::DeleteThis);
}

//========================================= Core Methods ==========================================
void AdventureMenu::AssembleDescriptionLines(const char *pDescriptionLine)
{
    //--Splits the rendering strings into smaller strings so they don't run over the edge of the description box.
    mDescriptionLines->ClearList();
    if(!pDescriptionLine) return;

    //--Setup.
    int tCharsParsedTotal = 0;
    int tStringLen = (int)strlen(pDescriptionLine);

    //--Loop until the whole description has been parsed out.
    while(tCharsParsedTotal < tStringLen)
    {
        //--Run the subdivide.
        int tCharsParsed = 0;
        char *nDescriptionLine = Subdivide::SubdivideString(tCharsParsed, &pDescriptionLine[tCharsParsedTotal], -1, VIRTUAL_CANVAS_X * 0.90f, Images.Data.rMenuFont, 1.0f);
        mDescriptionLines->AddElementAsTail("X", nDescriptionLine, &FreeThis);

        //--Move to the next line.
        tCharsParsedTotal += tCharsParsed;
    }
}

//============================================ Update =============================================
void AdventureMenu::UpdateShopMenu()
{
    //--[Fade Controllers]
    if(mShopFadeMode != FADE_MODE_NONE)
    {
        //--Initial fade out.
        if(mShopFadeMode == FADE_INITIAL_OUT || mShopFadeMode == FADE_FINAL_OUT)
        {
            mShopFadeTimer ++;
            if(mShopFadeTimer > FADE_TICKS_OUT)
            {
                mShopFadeMode ++;
                mShopFadeTimer = 0;
            }
        }
        //--Holding.
        else if(mShopFadeMode == FADE_INITIAL_HOLD || mShopFadeMode == FADE_FINAL_HOLD)
        {
            mShopFadeTimer ++;
            if(mShopFadeTimer > FADE_TICKS_HOLD)
            {
                mShopFadeMode ++;
                mShopFadeTimer = 0;
            }
        }
        //--Fade back in.
        else if(mShopFadeMode == FADE_INITIAL_IN || mShopFadeMode == FADE_FINAL_IN)
        {
            mShopFadeTimer ++;
            if(mShopFadeTimer > FADE_TICKS_IN)
            {
                //--For exiting the menu, disable shop mode now.
                if(mShopFadeMode == FADE_FINAL_IN)
                {
                    //--Teardown script.
                    if(mShopTeardownPath && strcasecmp(mShopTeardownPath, "Null")) LuaManager::Fetch()->ExecuteLuaFile(mShopTeardownPath);

                    //--Stock.
                    mIsShopMode = false;
                    Hide();
                }

                //--Stock.
                mShopFadeMode = FADE_MODE_NONE;
                mShopFadeTimer = 0;
            }
        }
        return;
    }

    //--[Setup]
    //--Quick-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Storage.
    bool tChangedCursor = false;
    //int tOldCursor = mShopCursor;

    //--[Selecting Buy/Sell]
    //--Selecting of buy and sell.
    if(mShopModeType == AM_SELECT_SHOP_MODE)
    {
        //--Pressing left decrements the cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            if(mShopCursor > AM_SHOP_SELECT_MIN)
            {
                tChangedCursor = true;
                mShopSkip = 0;
                mShopCursor --;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            //--Needed for rendering.
            if(mShopCursor == AM_SHOP_SELECT_GEMCUTTER) AdventureInventory::Fetch()->BuildExtendedItemList();
        }
        //--Pressing right increments the cursor.
        else if(rControlManager->IsFirstPress("Right"))
        {
            if(mShopCursor < AM_SHOP_SELECT_MAX )
            {
                tChangedCursor = true;
                mShopSkip = 0;
                mShopCursor ++;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            //--Needed for rendering.
            if(mShopCursor == AM_SHOP_SELECT_GEMCUTTER) AdventureInventory::Fetch()->BuildExtendedItemList();
        }

        //--If the cursor changed, and ended on the gemcutter tab, rebuild the gem list.
        if(tChangedCursor && mShopCursor == AM_SHOP_SELECT_GEMCUTTER)
        {
            AdventureInventory::Fetch()->BuildGemList();
        }

        //--Pressing Activate will enter whatever mode is currently selected.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Buying mode.
            if(mShopCursor == AM_SHOP_SELECT_BUY)
            {
                //--Fail if there are zero items.
                if(mShopInventory->GetListSize() < 1)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                    return;
                }

                //--Flags.
                mShopModeType = AM_BUY;
                AudioManager::Fetch()->PlaySound("Menu|Select");

                //--Get the 0th element. Assemble the description for it.
                ShopInventoryPack *rZerothItem = (ShopInventoryPack *)mShopInventory->GetElementBySlot(0);
                AssembleDescriptionLines(rZerothItem->mItem->GetDescription());
            }
            //--Selling mode.
            else if(mShopCursor == AM_SHOP_SELECT_SELL)
            {
                //--Fail if there are zero items.
                SugarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();
                if(rItemList->GetListSize() < 1)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                    return;
                }

                //--Flags.
                mShopCursor = 0;
                mShopModeType = AM_SELL;
                AudioManager::Fetch()->PlaySound("Menu|Select");

                //--Assemble the description for the 0th item.
                AdventureItem *rZerothItem = (AdventureItem *)rItemList->GetElementBySlot(0);
                AssembleDescriptionLines(rZerothItem->GetDescription());
            }
            //--Buyback. Allows repurchase of the last 20 items sold to any shop.
            else if(mShopCursor == AM_SHOP_SELECT_BUYBACK)
            {
                //--Fail if there are zero items.
                SugarLinkedList *rItemList = AdventureInventory::Fetch()->GetBuybackList();
                if(rItemList->GetListSize() < 1)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                    return;
                }

                //--Flags.
                mShopCursor = 0;
                mShopModeType = AM_BUYBACK;
                AudioManager::Fetch()->PlaySound("Menu|Select");

                //--Assemble the description for the 0th item.
                ShopInventoryPack *rZerothItem = (ShopInventoryPack *)rItemList->GetElementBySlot(0);
                AssembleDescriptionLines(rZerothItem->mItem->GetDescription());
            }
            //--Gemcutter mode. Not in this prototype.
            else if(mShopCursor == AM_SHOP_SELECT_GEMCUTTER)
            {
                //--Play a fail sound if the shop can't gemcut.
                if(!mShopCanGemcut)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
                //--Normal case:
                else
                {
                    AdventureInventory::Fetch()->BuildGemList();
                    mShopCursor = 0;
                    mGemcutterCursor = 0;
                    mShopModeType = AM_GEMCUTTER;
                    AudioManager::Fetch()->PlaySound("Menu|Select");
                }
            }
            return;
        }
        //--Pressing Cancel will back out of the shop mode.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            //--Activate fade mode.
            mShopFadeMode = FADE_FINAL_OUT;
            mShopFadeTimer = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return;
        }
    }
    //--[Sub Handlers]
    else
    {
        //--Press cancel either stops exchanges or returns to mode selection.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            //--Cancel is ignored if the gemcutter UI is currently in an advanced selection:
            if(mShopModeType == AM_GEMCUTTER && mSelectedGemCursor != -1)
            {

            }
            //--Normal case:
            else
            {
                //--Cancel the exchange.
                if(mIsExchanging)
                {
                    mIsExchanging = false;
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    return;
                }
                //--Return to mode selection.
                else
                {
                    //--Buy handler.
                    if(mShopModeType == AM_BUY)
                    {
                        mShopCursor = AM_SHOP_SELECT_BUY;
                    }
                    //--Sell Handler.
                    else if(mShopModeType == AM_SELL)
                    {
                        mShopCursor = AM_SHOP_SELECT_SELL;
                    }
                    //--Purify Handler.
                    else if(mShopModeType == AM_BUYBACK)
                    {
                        mShopCursor = AM_SHOP_SELECT_BUYBACK;
                    }
                    //--Gemcutter Handler.
                    else if(mShopModeType == AM_GEMCUTTER)
                    {
                        mShopCursor = AM_SHOP_SELECT_GEMCUTTER;
                    }

                    //--Clear outstanding description lines.
                    mShopModeType = AM_SELECT_SHOP_MODE;
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    return;
                }
            }
        }

        //--Run the buy update if in that mode.
        if(mShopModeType == AM_BUY)
        {
            UpdateShopBuy();
        }
        //--Sell Handler.
        else if(mShopModeType == AM_SELL)
        {
            UpdateShopSell();
        }
        //--Buyback Handler.
        else if(mShopModeType == AM_BUYBACK)
        {
            UpdateShopBuyback();
        }
        //--Gemcutter Handler.
        else if(mShopModeType == AM_GEMCUTTER)
        {
            UpdateGemcutterMode();
        }
    }
}

//=========================================== Drawing =============================================
void AdventureMenu::RenderShopMenu()
{
    //--[Documentation]
    //--Render the shop menu.
    if(!Images.mIsReady) return;

    //--[Fade Handling]
    //--These fades stop rendering of the menu entirely.
    if(mShopFadeMode == FADE_INITIAL_HOLD || mShopFadeMode == FADE_FINAL_HOLD)
    {
        SugarBitmap::DrawFullBlack(1.0f);
        return;
    }
    //--This is a fade out from the normal map, menu does not render.
    else if(mShopFadeMode == FADE_INITIAL_OUT)
    {
        SugarBitmap::DrawFullBlack(mShopFadeTimer / (float)FADE_TICKS_OUT);
        return;
    }
    //--This is a fade from the shop to the normal map.
    else if(mShopFadeMode == FADE_FINAL_IN)
    {
        SugarBitmap::DrawFullBlack(1.0f - (mShopFadeTimer / (float)FADE_TICKS_OUT));
        return;
    }

    //--[Setup]
    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--[Static Parts]
    //--Shop has a background behind it.
    SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.35f, 1.0f));

    //--[Render Splitting]
    //--Figure out which subsection should be rendering, and whether or not it is selected.
    int tBuySellCursor = mShopCursor;
    if(mShopModeType == AM_BUY)       tBuySellCursor = AM_SHOP_SELECT_BUY;
    if(mShopModeType == AM_SELL)      tBuySellCursor = AM_SHOP_SELECT_SELL;
    if(mShopModeType == AM_BUYBACK)   tBuySellCursor = AM_SHOP_SELECT_BUYBACK;
    if(mShopModeType == AM_GEMCUTTER) tBuySellCursor = AM_SHOP_SELECT_GEMCUTTER;

    //--Positions.
    float cBuyX     =  80.0f;
    float cSellX    = 208.0f;
    float cBuybackX = 356.0f;
    float cGemsX    = 503.0f;

    //--Buy is currently highlighted.
    if(tBuySellCursor == AM_SHOP_SELECT_BUY)
    {
        //--Frame set.
        Images.VendorUI.rFramesBuy->Draw();

        //--Render the headers as whited/greyed based on state.
        Images.VendorUI.rHeadingFont->DrawText(cBuyX,     81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText(cSellX,    81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");
        Images.VendorUI.rHeadingFont->DrawText(cBuybackX, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buyback");
        Images.VendorUI.rHeadingFont->DrawText(cGemsX,    81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Gems");
        StarlightColor::ClearMixer();

        //--Render the buy mode, but grey it out because we haven't actually activated the mode.
        if(mShopModeType == AM_SELECT_SHOP_MODE)
        {
            RenderShopBuy(false, 0.5f);
        }
        //--Render everything at full brightness.
        else
        {
            RenderShopBuy(true, 1.0f);
        }
        StarlightColor::ClearMixer();
    }
    //--Sell is currently highlighted.
    else if(tBuySellCursor == AM_SHOP_SELECT_SELL)
    {
        //--Frame set.
        Images.VendorUI.rFramesSell->Draw();

        //--Render the headers as whited/greyed based on state.
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText(cBuyX,     81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
        StarlightColor::ClearMixer();
        Images.VendorUI.rHeadingFont->DrawText(cSellX,    81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText(cBuybackX, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buyback");
        Images.VendorUI.rHeadingFont->DrawText(cGemsX,    81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Gems");
        StarlightColor::ClearMixer();

        //--Render the sell mode, but grey it out because we haven't actually activated the mode.
        if(mShopModeType == AM_SELECT_SHOP_MODE)
        {
            RenderShopSell(false, 0.5f);
        }
        //--Render everything at full brightness.
        else
        {
            RenderShopSell(true, 1.0f);
        }
        StarlightColor::ClearMixer();
    }
    //--Purify is currently highlighted.
    else if(tBuySellCursor == AM_SHOP_SELECT_BUYBACK)
    {
        //--Frame set.
        Images.VendorUI.rFramesBuyback->Draw();

        //--Render the headers as whited/greyed based on state.
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText(cBuyX,     81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
        Images.VendorUI.rHeadingFont->DrawText(cSellX,    81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");
        StarlightColor::ClearMixer();
        Images.VendorUI.rHeadingFont->DrawText(cBuybackX, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buyback");
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText(cGemsX,    81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Gems");
        StarlightColor::ClearMixer();

        //--Render buyback mode, grey out all elements as it is not selected yet.
        if(mShopModeType == AM_SELECT_SHOP_MODE)
        {
            RenderShopBuyback(false, 0.5f);
        }
        //--Render everything at full brightness.
        else
        {
            RenderShopBuyback(true, 1.0f);
        }
        StarlightColor::ClearMixer();
    }
    //--Gemcutter is currently highlighted.
    else if(tBuySellCursor == AM_SHOP_SELECT_GEMCUTTER)
    {
        //--Frame set.
        Images.VendorUI.rFramesGemcutter->Draw();

        //--Render the headers as whited/greyed based on state.
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText(cBuyX,     81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
        Images.VendorUI.rHeadingFont->DrawText(cSellX,    81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");
        Images.VendorUI.rHeadingFont->DrawText(cBuybackX, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buyback");
        StarlightColor::ClearMixer();
        Images.VendorUI.rHeadingFont->DrawText(cGemsX,    81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Gems");

        //--Render the gemcutter mode, but grey it out because we haven't actually activated the mode.
        if(mShopModeType == AM_SELECT_SHOP_MODE)
        {
            RenderGemcutterMode(false, 0.5f);
        }
        //--Render everything at full brightness.
        else
        {
            RenderGemcutterMode(true, 1.0f);
        }
        StarlightColor::ClearMixer();
    }

    //--[Common]
    //--Shows the name of the shop.
    Images.VendorUI.rHeadingFont->DrawText(29.0f, 19.0f, 0, 1.0f, mShopName);

    //--Platina.
    Images.VendorUI.rHeadingFont->DrawTextArgs(902.0f, 19.0f, 0, 1.0f, "Platina: %i", rInventory->GetPlatina());

    //--[Fade Handling]
    //--Fading in to the shop menu.
    if(mShopFadeMode == FADE_INITIAL_IN)
    {
        SugarBitmap::DrawFullBlack(1.0f - (mShopFadeTimer / (float)FADE_TICKS_OUT));
    }
    //--This is a fade from the shop to the normal map.
    else if(mShopFadeMode == FADE_FINAL_OUT)
    {
        SugarBitmap::DrawFullBlack(mShopFadeTimer / (float)FADE_TICKS_OUT);
    }
}

//========================================= Lua Hooking ===========================================
void AdventureMenu::HookToLuaStateShop(lua_State *pLuaState)
{
    /* AM_SetShopProperty("Show", sShopName, sBootPath)
       AM_SetShopProperty("Add Item", sName, iPriceOverride, iQuantity)
       AM_SetShopProperty("Register Upgrade", sName, iPlatinaCost, iPowderCost, iFlakeCost, iShardCost, iPieceCost, iChunkCost, iOreCost)
       Sets properties related to shops. */
    lua_register(pLuaState, "AM_SetShopProperty", &Hook_AM_SetShopProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AM_SetShopProperty(lua_State *L)
{
    ///--[Argument Listing]
    //AM_SetShopProperty("Show", sShopName, sBootPath, sTeardownPath)
    //AM_SetShopProperty("Add Item", sName, iPriceOverride, iQuantity)
    //AM_SetShopProperty("Register Upgrade", sName, iPlatinaCost, iPowderCost, iFlakeCost, iShardCost, iPieceCost, iChunkCost, iOreCost)
    //AM_SetShopProperty("Allow Gemcutting")

    ///--[Argument Checking]
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AM_SetShopProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Object.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("AM_SetShopProperty");

    ///--[General]
    //--Activates the shop menu and calls the provided subroutine.
    if(!strcasecmp(rSwitchType, "Show") && tArgs == 4)
    {
        rMenu->Show();
        rMenu->InitializeShopMode(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Adds a new item to the shop. Pass -1 for price to use default value. Pass -1 for quantity to have unlimited.
    else if(!strcasecmp(rSwitchType, "Add Item") && tArgs == 4)
    {
        rMenu->RegisterItem(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Allows the shop to cut gems.
    else if(!strcasecmp(rSwitchType, "Allow Gemcutting") && tArgs == 1)
    {
        rMenu->InitializeGemcutterMode();
    }
    ///--[Error]
    else
    {
        LuaPropertyError("AM_SetShopProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
