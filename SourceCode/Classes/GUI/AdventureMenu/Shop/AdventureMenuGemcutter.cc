//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--[Shop Fading]
//--Exactly the same as the regular shop.
#define FADE_MODE_NONE 0
#define FADE_INITIAL_OUT 1
#define FADE_INITIAL_HOLD 2
#define FADE_INITIAL_IN 3
#define FADE_FINAL_OUT 4
#define FADE_FINAL_HOLD 5
#define FADE_FINAL_IN 6

#define FADE_TICKS_OUT 15
#define FADE_TICKS_HOLD 30
#define FADE_TICKS_IN 15

#define ADVMENU_GEMCUT_DEBUG
#ifdef ADVMENU_GEMCUT_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//=========================================== System ==============================================
//--[Rendering Statics]
//--[Forward Declarations]
void RenderAndAdvance(float pLft, float &sYCursor, SugarFont *pFont, const char *pFormat, ...);

//====================================== Property Queries =========================================
int AdventureMenu::ComputeGemMergeCost(int pRank, int pAdamantiteType)
{
    //--Returns how many of a given type of adamantite a given rank of gem needs to upgrade.
    if(pRank < 0) return 0;
    if(pRank >= 5) return 0;

    //--Rank 0 gems:
    if(pRank == 0)
    {
        if(pAdamantiteType == CRAFT_ADAMANTITE_POWDER) return 3;
        if(pAdamantiteType == CRAFT_ADAMANTITE_FLAKES) return 1;
        return 0;
    }
    //--Rank 1 gems:
    else if(pRank == 1)
    {
        if(pAdamantiteType == CRAFT_ADAMANTITE_POWDER) return 3;
        if(pAdamantiteType == CRAFT_ADAMANTITE_FLAKES) return 1;
        if(pAdamantiteType == CRAFT_ADAMANTITE_SHARD)  return 1;
        return 0;
    }
    //--Rank 2 gems:
    else if(pRank == 2)
    {
        if(pAdamantiteType == CRAFT_ADAMANTITE_POWDER) return 5;
        if(pAdamantiteType == CRAFT_ADAMANTITE_FLAKES) return 2;
        if(pAdamantiteType == CRAFT_ADAMANTITE_SHARD)  return 1;
        if(pAdamantiteType == CRAFT_ADAMANTITE_PIECE)  return 1;
        return 0;
    }
    //--Rank 3 gems:
    else if(pRank == 3)
    {
        if(pAdamantiteType == CRAFT_ADAMANTITE_POWDER) return 5;
        if(pAdamantiteType == CRAFT_ADAMANTITE_FLAKES) return 1;
        if(pAdamantiteType == CRAFT_ADAMANTITE_SHARD)  return 0;
        if(pAdamantiteType == CRAFT_ADAMANTITE_PIECE)  return 0;
        if(pAdamantiteType == CRAFT_ADAMANTITE_CHUNK)  return 1;
        return 0;
    }
    //--Rank 4 gems:
    else if(pRank == 4)
    {
        if(pAdamantiteType == CRAFT_ADAMANTITE_POWDER) return 5;
        if(pAdamantiteType == CRAFT_ADAMANTITE_FLAKES) return 1;
        if(pAdamantiteType == CRAFT_ADAMANTITE_SHARD)  return 0;
        if(pAdamantiteType == CRAFT_ADAMANTITE_PIECE)  return 0;
        if(pAdamantiteType == CRAFT_ADAMANTITE_CHUNK)  return 0;
        if(pAdamantiteType == CRAFT_ADAMANTITE_ORE)    return 1;
        return 0;
    }

    //--Out of range.
    return 0;
}

//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void AdventureMenu::UpdateGemcutterMode()
{
    ///--[Setup]
    //--Quick-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    SugarLinkedList *rGemList = rInventory->GetGemList();

    //--Storage.
    int tOldCursor = mGemcutterCursor;

    //--Debug.
    DebugPush(true, "Updating Gemcutter Mode.\n");

    ///--[Exchange Mode]
    //--Confirms player purchase.
    if(mIsExchanging)
    {
        //--Confirm.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Gems in question.
            SugarLinkedList *rGemMergeList = rInventory->GetGemMergeList();
            AdventureItem *rBaseGem  = (AdventureItem *)rGemList->GetElementBySlot(mSelectedGemCursor);
            AdventureItem *rMergeGem = (AdventureItem *)rGemMergeList->GetElementBySlot(mGemcutterCursor);

            //--Compute the adamantite cost.
            int tRank = rBaseGem->GetRank() + rMergeGem->GetRank();

            //--Subtract from inventory.
            for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
            {
                //--If the cost is zero, ignore it.
                int tCost = ComputeGemMergeCost(tRank, i);
                if(tCost == 0) continue;

                //--If the cost is too high, stop.
                int tCraftingCount = rInventory->GetCraftingCount(i);
                rInventory->SetCraftingMaterial(i, tCraftingCount - tCost);
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");

            //--Order the gems to merge.
            rBaseGem->MergeWithGem(rMergeGem);

            //--Remove the merged gem from the inventory. Liberate it so as not to deallocate it.
            rInventory->LiberateItemP(rMergeGem);

            //--Order the gem list to rebuild.
            rInventory->BuildGemList();
            rInventory->BuildGemMergeList(rBaseGem);

            //--Make sure the selected gem cursor still points at the base gem. This allows multiple
            //  merges without resetting the UI.
            mSelectedGemCursor = rGemList->GetSlotOfElementByPtr(rBaseGem);

            //--Decrement the gemcutter cursor.
            mGemcutterCursor --;
            if(mGemcutterCursor < 1) mGemcutterCursor = 0;

            //--Exit out of exchange mode.
            mIsExchanging = false;
            mShopSkip = mShopSkipStore;
            mShopSkip --;

            //--Check if any further gems can be merged. If not, exit merging mode.
            //AdventureItem *rCheckItem = (AdventureItem *)rGemList->GetElementBySlot(mSelectedGemCursor);
            if(rGemMergeList->GetListSize() < 1) mSelectedGemCursor = -1;
        }
        //--Cancel transaction.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsExchanging = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    ///--[Selecting a Gem]
    //--When selecting a gem to improve, these controls are used.
    else if(mSelectedGemCursor == -1)
    {
        //--[Directional Navigation Keys]
        //--Up, decrement by one row.
        if(rControlManager->IsFirstPress("Up"))
        {
            //--Decrement.
            mGemcutterCursor --;
            if(mGemcutterCursor < 1) mGemcutterCursor = 0;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Down, increment by one row.
        else if(rControlManager->IsFirstPress("Down"))
        {
            //--Increment.
            mGemcutterCursor ++;

            //--Range check the cursor on the last page, if needed.
            if(mGemcutterCursor >= rGemList->GetListSize()) mGemcutterCursor --;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Left, decrement by one. Moves page if applicable.
        else if(rControlManager->IsFirstPress("Left"))
        {
        }
        //--Right, increment by one. Moves page if applicable.
        else if(rControlManager->IsFirstPress("Right"))
        {
        }

        //--[Action Keys]
        //--Activate, enters upgrade mode for the gem in question.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Debug.
            DebugPrint(" Gem Slot: %i\n", mGemcutterCursor);

            //--Get the item in question.
            AdventureItem *rCheckItem = (AdventureItem *)rGemList->GetElementBySlot(mGemcutterCursor);
            if(!rCheckItem)
            {
                DebugPop("No item in slot, exiting.\n");
                return;
            }

            //--Check if we have enough adamantite of the types needed.
            bool tHasEnough = true;
            int tRank = rCheckItem->GetRank();

            //--Render how many of each adamantite type are needed to upgrade the gem. If an adamantite type
            //  is missing, render it in red.
            DebugPrint(" Scanning adamantite.\n");
            for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
            {
                //--If the cost is zero, ignore it.
                int tCost = ComputeGemMergeCost(tRank, i);
                if(tCost == 0) continue;

                //--If the cost is too high, fail.
                int tCraftingCount = AdventureInventory::Fetch()->GetCraftingCount(i);
                if(tCost > tCraftingCount)
                {
                    tHasEnough = false;
                    break;
                }
            }

            //--Build a merge list.
            DebugPrint(" Building merge list.\n");
            SugarLinkedList *rGemMergeList = rInventory->BuildGemMergeList(rCheckItem);
            DebugPrint(" Done with merge list.\n");

            //--Not enough adamantite. Play a sound.
            if(!tHasEnough)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--No gems can be merged in.
            else if(rGemMergeList->GetListSize() < 1)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--Has enough adamantite, switch modes.
            else
            {
                mShopSkipStore = mShopSkip;
                mShopSkip = 0;
                mSelectedGemCursor = mGemcutterCursor;
                mGemcutterCursor = 0;
                AudioManager::Fetch()->PlaySound("Menu|Select");
                DebugPop("Had enough adamantite, exiting.\n");
                return;
            }
            DebugPop("Exiting as failure.\n");
        }

        //--SFX, and check the comparison cursor if the item changed.
        if(tOldCursor != mGemcutterCursor)
        {
            //--[Recentering]
            //--On a decrement:
            if(mGemcutterCursor < tOldCursor)
            {
                //--Get where the cursor is on the page.
                int tPosition = mGemcutterCursor - mShopSkip;
                if(tPosition < 2) mShopSkip = mGemcutterCursor - 2;

                //--Clamp.
                if(mShopSkip < 0) mShopSkip = 0;
            }
            //--On an increment:
            else if(mGemcutterCursor > tOldCursor)
            {
                //--Get where the cursor is on the page.
                int tPosition = mGemcutterCursor - mShopSkip;
                if(tPosition > AM_SHOP_ITEMS_PER_PAGE - 3) mShopSkip = mGemcutterCursor - (AM_SHOP_ITEMS_PER_PAGE - 3);

                //--Clamp.
                if(mShopSkip > rGemList->GetListSize() - AM_SHOP_ITEMS_PER_PAGE) mShopSkip = rGemList->GetListSize() - AM_SHOP_ITEMS_PER_PAGE;
            }
        }
    }
    ///--[Merge Selection]
    //--Allows merging of gems. Select a gem and merge!
    else
    {
        //--[List]
        SugarLinkedList *rGemMergeList = rInventory->GetGemMergeList();
        AdventureItem *rSelectedGem = (AdventureItem *)rGemList->GetElementBySlot(mSelectedGemCursor);
        AdventureItem *rMergeGem = (AdventureItem *)rGemMergeList->GetElementBySlot(mGemcutterCursor);

        //--[Directional Navigation Keys]
        //--Up, decrement by one row.
        if(rControlManager->IsFirstPress("Up"))
        {
            //--Clamp.
            if(mGemcutterCursor > 0)
            {
                mGemcutterCursor --;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Down, increment by one row.
        else if(rControlManager->IsFirstPress("Down"))
        {
            //--Clamp.
            if(mGemcutterCursor < rGemMergeList->GetListSize() - 1)
            {
                mGemcutterCursor ++;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--[Action Keys]
        //--Activate, shows the confirm cut prompt.
        if(rControlManager->IsFirstPress("Activate") && rSelectedGem && rMergeGem)
        {
            //--Compute the adamantite cost.
            int tRank = rSelectedGem->GetRank() + rMergeGem->GetRank();

            //--Verify we can afford it.
            bool tHasEnough = true;
            for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
            {
                //--If the cost is zero, ignore it.
                int tCost = ComputeGemMergeCost(tRank, i);
                if(tCost == 0) continue;

                //--If the cost is too high, stop.
                int tCraftingCount = rInventory->GetCraftingCount(i);
                if(tCost > tCraftingCount)
                {
                    tHasEnough = false;
                    break;
                }
            }

            //--Not enough.
            if(!tHasEnough)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                DebugPop("Not enough adamantite to cut gem. Exiting.\n");
                return;
            }

            //--Activate confirmation mode.
            mIsExchanging = true;
        }
        //--Cancel, returns to gem selection.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            mGemcutterCursor = mSelectedGemCursor;
            mSelectedGemCursor = -1;
            mShopSkip = mShopSkipStore;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Debug.
    DebugPop("Update ended normally.\n");
}

//=========================================== Drawing =============================================
void AdventureMenu::RenderGemcutterMode(bool pIsActiveMode, float pColorMix)
{
    ///--[Documentation and Setup]
    //--Renders the gemcutting menu. The frames are already rendered by the calling function, this just renders the text and icons.
    if(!Images.mIsReady) return;

    //--Debug.
    DebugPush(true, "Rendering Gemcutter Mode.\n");

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    SugarLinkedList *rItemList = rInventory->GetGemList();

    ///--[No Gemcut Mode]
    //--If this flag is not set, then this vendor cannot cut gems.
    if(!mShopCanGemcut)
    {
        Images.EquipmentUI.rHeadingFont->DrawText(19.0f, 124.0f, 0, 1.0f, "This vendor cannot modify gems.");
        DebugPop("Exiting because shop cannot gemcut.\n");
        return;
    }

    //--Constants.
    float cHeaderY = 124.0f;
    float cIconX  = 30.0f;
    float cRankX = 370.0f;
    float cUnderlayLft =  15.0f;
    float cUnderlayRgt = 579.0f;
    DebugPrint("Rendering headers.\n");

    ///--[No Gem Selected]
    if(mSelectedGemCursor == -1)
    {
        ///--[Gem Inventory]
        //--Text headers.
        DebugPrint("No gem selected.\n");
        StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
        Images.EquipmentUI.rHeadingFont->DrawText(cIconX, cHeaderY, 0, 1.0f, "Name");
        Images.EquipmentUI.rHeadingFont->DrawText(cRankX, cHeaderY, 0, 1.0f, "Rank");

        //--Y positioning
        float cTop = cHeaderY + 44.0f;
        float tCurrentY = cTop;
        float cSpcY = 23.0f;

        //--Variables.
        int tRendersLeft = AM_SHOP_ITEMS_PER_PAGE;
        int tSkipsLeft = mShopSkip;

        //--Cursor.
        int tCursor = mGemcutterCursor;

        //--[Rendering Loop]
        int tCurrentItem = 0;
        AdventureItem *rHighlightedGem = NULL;
        DebugPrint("Iterating across gems.\n");
        AdventureItem *rItem = (AdventureItem *)rItemList->PushIterator();
        while(rItem)
        {
            //--If this item should not render, skip it.
            if(tSkipsLeft > 0)
            {
                tCurrentItem ++;
                tSkipsLeft --;
                rItem = (AdventureItem *)rItemList->AutoIterate();
                continue;
            }

            //--[Backing]
            if(tCurrentItem % 2 == 1)
            {
                SugarBitmap::DrawRectFill(cUnderlayLft, tCurrentY, cUnderlayRgt, tCurrentY + cSpcY, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
                StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            }

            //--[Upgrade Determination]
            //--Set a flag if the gem can't be upgraded.
            bool tCanBeUpgraded = (rItem->GetRank() < ADITEM_MAX_GEM_RANK);

            //--[Render]
            //--Basic information.
            RenderGemInfo(cIconX, tCurrentY, true, tCanBeUpgraded, (tCurrentItem == tCursor && pIsActiveMode), pColorMix, rItem);

            //--If selected, render the description.
            if(tCurrentItem == tCursor && pIsActiveMode)
            {
                rHighlightedGem = rItem;
                RenderItemDescription(557.0f, rItem);
            }

            //--[Ending Case]
            //--If the renders have run out, stop here.
            tRendersLeft --;
            if(tRendersLeft < 1)
            {
                rItemList->PopIterator();
                break;
            }

            //--Next.
            StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            tCurrentItem ++;
            tCurrentY = tCurrentY + cSpcY;
            rItem = (AdventureItem *)rItemList->AutoIterate();
        }

        //--Clean.
        DebugPrint("Finished iteration.\n");
        StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);

        ///--[Scrollbar]
        //--Render a scrollbar to indicate how much of the inventory is represented.
        if(rItemList->GetListSize() > AM_SHOP_ITEMS_PER_PAGE)
        {
            //--Binding.
            DebugPrint("Rendering scrollbar.\n");
            Images.VendorUI.rScrollbarFront->Bind();

            //--Scrollbar front. Stretches dynamically.
            float cScrollTop = 158.0f;
            float cScrollHei = 366.0f;

            //--Determine what percentage of the inventory is presently represented.
            int tMaxOffset = rItemList->GetListSize();
            float cPctTop = (mShopSkip                         ) / (float)tMaxOffset;
            float cPctBot = (mShopSkip + AM_SHOP_ITEMS_PER_PAGE) / (float)tMaxOffset;

            //--Positions and Constants.
            float cLft = 548.0f;
            float cRgt = cLft + Images.InventoryUI.rScrollbarFront->GetWidth();
            float cEdg = 6.0f / (float)Images.InventoryUI.rScrollbarFront->GetHeight();

            //--Compute where the bar should be.
            float cTopTop = cScrollTop + (cPctTop * cScrollHei);
            float cTopBot = cTopTop + 6.0f;
            float cBotBot = cScrollTop + (cPctBot * cScrollHei);
            float cBotTop = cBotBot - 6.0f;

            glBegin(GL_QUADS);
                //--Render the top of the bar.
                glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTopTop);
                glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTopTop);
                glTexCoord2f(1.0f, cEdg); glVertex2f(cRgt, cTopBot);
                glTexCoord2f(0.0f, cEdg); glVertex2f(cLft, cTopBot);

                //--Render the bottom of the bar.
                glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
                glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
                glTexCoord2f(1.0f, 1.0f);        glVertex2f(cRgt, cBotBot);
                glTexCoord2f(0.0f, 1.0f);        glVertex2f(cLft, cBotBot);

                //--Render the middle of the bar.
                glTexCoord2f(0.0f, cEdg);        glVertex2f(cLft, cTopBot);
                glTexCoord2f(1.0f, cEdg);        glVertex2f(cRgt, cTopBot);
                glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
                glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
            glEnd();

            //--Backing. Already positioned.
            Images.VendorUI.rScrollbarBack->Draw();
            DebugPrint("Done rendering scrollbar.\n");
        }

        //--[Ingredients Listing]
        DebugPrint("Rendering ingredients required.\n");
        RenderMergeCost(rHighlightedGem, NULL, pColorMix);
        StarlightColor::ClearMixer();

        //--Statistics.
        DebugPrint("Rendering gem properties.\n");
        RenderShopGemProperties(true, rHighlightedGem, NULL);
        StarlightColor::ClearMixer();
        DebugPrint("Done.\n");
    }
    ///--[Gem Selected for Merging]
    //--A gem has been selected for merging. Render the available gems, as well as the gem in question.
    else
    {
        ///--[Selected Gem Information]
        //--Get the selected gem.
        DebugPrint("Rendering selected gem information: %i\n", mSelectedGemCursor);
        AdventureItem *rSelectedGem = (AdventureItem *)rItemList->GetElementBySlot(mSelectedGemCursor);

        //--Header.
        float cSelectedGemY = cHeaderY + 44.0f;
        float cSpcY         = 23.0f;

        //--Text headers.
        StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
        Images.EquipmentUI.rHeadingFont->DrawText(cIconX, cHeaderY, 0, 1.0f, "Selected Gem");

        //--Selected gem's info.
        DebugPrint("Gem info render.\n");
        RenderGemInfo(cIconX, cSelectedGemY, false, true, false, pColorMix, rSelectedGem);

        //--[Merge List]
        //--Get the list.
        SugarLinkedList *rGemMergeList = rInventory->GetGemMergeList();

        //--Variables.
        int tRendersLeft = AM_SHOP_ITEMS_PER_PAGE - 5;
        int tSkipsLeft = mShopSkip;
        int tCursor = mGemcutterCursor;

        //--Header.
        cHeaderY = cSelectedGemY + 20.0f;
        Images.EquipmentUI.rHeadingFont->DrawText(cIconX, cHeaderY, 0, 1.0f, "Candidates");

        //--Rendering Vars
        float tCurrentY = cHeaderY + 44.0f;

        //--List of mergeable gems.
        int tCurrentItem = 0;
        AdventureItem *rHighlightedGem = NULL;
        DebugPrint("Rendering list of mergeable gems.\n");
        AdventureItem *rMergeGem = (AdventureItem *)rGemMergeList->PushIterator();
        while(rMergeGem)
        {
            //--If this item should not render, skip it.
            if(tSkipsLeft > 0)
            {
                tCurrentItem ++;
                tSkipsLeft --;
                rMergeGem = (AdventureItem *)rGemMergeList->AutoIterate();
                continue;
            }

            //--[Backing]
            if(tCurrentItem % 2 == 1)
            {
                SugarBitmap::DrawRectFill(cUnderlayLft, tCurrentY, cUnderlayRgt, tCurrentY + cSpcY, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
                StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            }

            //--[Render]
            //--Basic information.
            RenderGemInfo(cIconX, tCurrentY, false, true, (tCurrentItem == tCursor), pColorMix, rMergeGem);

            //--If selected, render the description.
            if(tCurrentItem == tCursor)
            {
                DebugPrint("Selected highlight gem in slot %i %p.\n", tCurrentItem, rMergeGem);
                rHighlightedGem = rMergeGem;
                RenderItemDescription(557.0f, rMergeGem);
            }

            //--[Ending Case]
            //--If the renders have run out, stop here.
            tRendersLeft --;
            if(tRendersLeft < 1)
            {
                rItemList->PopIterator();
                break;
            }

            //--Next.
            StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            tCurrentItem ++;
            tCurrentY = tCurrentY + cSpcY;
            rMergeGem = (AdventureItem *)rGemMergeList->AutoIterate();
        }

        //--[Render Costs]
        DebugPrint("Resolved gems: %p %p\n", rSelectedGem, rHighlightedGem);
        DebugPrint("Rendering merge costs.\n");
        RenderMergeCost(rSelectedGem, rHighlightedGem, pColorMix);
        StarlightColor::ClearMixer();

        //--[Render Properties]
        //--Because the gems in question are being merged, we render their added stats.
        DebugPrint("Computing stat comparisons.\n");
        CombatStatistics *rBaseGemStats = rSelectedGem->GetFinalStatistics();
        CombatStatistics tSumStats;

        //--If there is a comparison gem:
        if(rHighlightedGem)
        {
            CombatStatistics *rHighlightStats = rHighlightedGem->GetFinalStatistics();
            DebugPrint("Pointers: %p %p\n", rBaseGemStats, rHighlightStats);
            for(int i = 0; i < STATS_TOTAL; i ++)
            {
                tSumStats.SetStatByIndex(i, rBaseGemStats->GetStatByIndex(i) + rHighlightStats->GetStatByIndex(i));
            }
        }
        //--No comparison gem.
        else
        {
            DebugPrint("No comparison gem for %p\n", rBaseGemStats);
            for(int i = 0; i < STATS_TOTAL; i ++)
            {
                tSumStats.SetStatByIndex(i, rBaseGemStats->GetStatByIndex(i));
            }
        }

        //--Rendering is handled by a subroutine.
        DebugPrint("Rendering stat comparisons.\n");
        RenderShopStatComparison(false, rBaseGemStats, &tSumStats);
        StarlightColor::ClearMixer();
        DebugPrint("Done.\n");
    }

    //--[Clean]
    StarlightColor::ClearMixer();

    ///--[Exchange Dialogue]
    //--In exchange mode, render the confirmation dialogue.
    if(mIsExchanging)
    {
        //--Gems in question.
        DebugPrint("Rendering exchange dialogue.\n");
        SugarLinkedList *rGemMergeList = rInventory->GetGemMergeList();
        AdventureItem *rBaseGem  = (AdventureItem *)rItemList->GetElementBySlot(mSelectedGemCursor);
        AdventureItem *rMergeGem = (AdventureItem *)rGemMergeList->GetElementBySlot(mGemcutterCursor);

        //--Get the item and verify it.
        if(!rBaseGem || !rMergeGem)
        {
            DebugPop("Exiting, invalid gem pointers during exchange.\n");
            return;
        }

        //--Black out.
        SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.75f));

        //--Overlay.
        Images.VendorUI.rConfirmOverlay->Draw();

        //--Heading.
        Images.VendorUI.rHeadingFont->DrawText(683.0f, 210.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Merge these gems?");

        //--First gem, icon and name.
        SugarBitmap *rBaseGemImg = rBaseGem->GetIconImage();
        if(rBaseGemImg) rBaseGemImg->Draw(532.0f, 253.0f);
        Images.VendorUI.rMainlineFont->DrawTextArgs(532.0f+22.0f, 253.0f, 0, 1.0f, "%s", rBaseGem->GetName());

        //--Second gem, icon and name.
        SugarBitmap *rMergeGemImg = rMergeGem->GetIconImage();
        if(rMergeGemImg) rMergeGemImg->Draw(532.0f, 275.0f);
        Images.VendorUI.rMainlineFont->DrawTextArgs(532.0f+22.0f, 275.0f, 0, 1.0f, "%s", rMergeGem->GetName());

        //--Other text.
        Images.VendorUI.rMainlineFont->DrawTextArgs(683.0f, 345.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Z to confirm");
        Images.VendorUI.rMainlineFont->DrawTextArgs(683.0f, 365.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "X to cancel");
        DebugPrint("Done exchange dialogue.\n");
    }
    StarlightColor::ClearMixer();

    //--Debug.
    DebugPop("Render ended normally.\n");
}
void AdventureMenu::RenderGemInfo(float pX, float pY, bool pRenderRank, bool pCanBeUpgraded, bool pIsHighlighted, float pMixer, AdventureItem *pGem)
{
    //--[Documentation and Setup]
    //--Renders the gem's name, owner, and icon. Used for the listing of the gemcutter screen.
    if(!pGem) return;

    //--Positions
    float cIconX = pX;
    float cIconW = 22.0f;
    float cFaceX = pX + cIconW;
    float cFaceW = 32.0f;
    float cNameX = pX + cIconW + cFaceW + 2.0f;
    float cRankX = 370.0f;

    //--Lists.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    SugarLinkedList *rItemList = rInventory->GetExtendedItemList();

    //--[Item Icon]
    //--Render the item's icon here. It's a 22px entry.
    SugarBitmap *rItemIcon = pGem->GetIconImage();
    if(rItemIcon) rItemIcon->Draw(cIconX, pY);

    //--[Owner's Face Indicator]
    //--Figure out which entity has this gem equipped, if any.
    const char *rOwnerName = NULL;
    AdventureItem *rOwnerItem = pGem->rGemParent;
    if(rOwnerItem)
    {
        int tSlot = rItemList->GetSlotOfElementByPtr(rOwnerItem);
        rOwnerName = rItemList->GetNameOfElementBySlot(tSlot);
    }

    //--Item has no owner:
    if(!rOwnerName || !strcasecmp(rOwnerName, "Null"))
    {
    }
    //--Item has an owner:
    else
    {
        //--Setup.
        AdvCombat *rAdventureCombat = AdvCombat::Fetch();
        const char *rRenderOwnerName = rOwnerName;
        TwoDimensionReal tFaceDim;

        //--Resolve the owner's display name. It's usually the same, but not always (Ex: Chris, Christine).
        AdvCombatEntity *rEntity = rAdventureCombat->GetRosterMemberS(rRenderOwnerName);
        if(!rEntity) return;
        SugarBitmap *rFaceImg = rEntity->GetFaceProperties(tFaceDim);

        //--Render the character's face.
        if(rFaceImg)
        {
            tFaceDim.mLft = tFaceDim.mLft;
            tFaceDim.mTop = 1.0f - (tFaceDim.mTop);
            tFaceDim.mRgt = tFaceDim.mRgt;
            tFaceDim.mBot = 1.0f - (tFaceDim.mBot);
            rFaceImg->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(tFaceDim.mLft, tFaceDim.mTop); glVertex2f(cFaceX,          pY);
                glTexCoord2f(tFaceDim.mRgt, tFaceDim.mTop); glVertex2f(cFaceX + cFaceW, pY);
                glTexCoord2f(tFaceDim.mRgt, tFaceDim.mBot); glVertex2f(cFaceX + cFaceW, pY + cFaceW);
                glTexCoord2f(tFaceDim.mLft, tFaceDim.mBot); glVertex2f(cFaceX,          pY + cFaceW);
            glEnd();
        }
    }

    //--[Cursor Handling]
    //--If this is the selected item, render the cursor over it. If the mode is inactive, don't render this.
    //  Inactive mode is used when the player is hovering on the mode, but hasn't entered it yet.
    if(pIsHighlighted)
    {
        if(pCanBeUpgraded)
            StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
        else
            StarlightColor::SetMixer(0.5f, 0.0f, 0.0f, 1.0f);
    }
    //--Not selected or is inactive mode.
    else
    {
        if(pCanBeUpgraded)
            StarlightColor::SetMixer(pMixer, pMixer, pMixer, 1.0f);
        else
            StarlightColor::SetMixer(pMixer * 0.50f, pMixer * 0.50f, pMixer * 0.50f, 1.0f);
    }

    //--[Item Name]
    //--Name.
    Images.EquipmentUI.rMainlineFont->DrawText(cNameX, pY, 0, 1.0f, pGem->GetName());

    //--[Rank]
    //--Rank. This is how many other gems are inside this gem.
    if(pRenderRank)
    {
        int tRank = pGem->GetRank();
        Images.EquipmentUI.rMainlineFont->DrawTextArgs(cRankX, pY, 0, 1.0f, "%i", tRank);
    }
    StarlightColor::SetMixer(pMixer, pMixer, pMixer, 1.0f);
}
void AdventureMenu::RenderMergeCost(AdventureItem *pSourceGem, AdventureItem *pMergeGem, float pColorMix)
{
    //--[Documentation and Setup]
    //--On the right side of the screen, renders the merging cost for the gem in question. The source gem must exist,
    //  but the merge gem does not, in which case the cost is equal to merging a rank 0 gem.
    if(!pSourceGem) return;

    //--Fast-Access Pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Setup.
    bool tHasEnough = true;
    float cCostX = 840.0f;
    float cCostR = 913.0f;

    //--Constants.
    float cAdmIconX  = 602.0f;
    float cAdmX      = cAdmIconX + 23.0f;
    float cAdmR      = cAdmX + 150.0f;
    float cAdmY      = 160.0f;
    float cAdmSpc    =  23.0f;
    uint32_t cFlagsL = 0;
    uint32_t cFlagsR = SUGARFONT_RIGHTALIGN_X;

    //--Heading.
    Images.EquipmentUI.rHeadingFont->DrawTextArgs(cAdmIconX, 125.0f, 0, 1.0f, "Adamantite");
    Images.EquipmentUI.rHeadingFont->DrawTextArgs(cCostX, 125.0f, 0, 1.0f, "Cost");

    //--Icons, values.
    for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
    {
        Images.BaseMenu.rAdamantiteImg[i]->Draw(cAdmIconX, cAdmY + (cAdmSpc * i) + 3.0f);
    }
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 0.00f), cFlagsL, 1.0f, "Powder");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 0.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_POWDER));
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 1.00f), cFlagsL, 1.0f, "Flakes");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 1.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_FLAKES));
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 2.00f), cFlagsL, 1.0f, "Shards");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 2.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_SHARD));
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 3.00f), cFlagsL, 1.0f, "Pieces");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 3.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_PIECE));
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 4.00f), cFlagsL, 1.0f, "Chunks");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 4.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_CHUNK));
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 5.00f), cFlagsL, 1.0f, "Ore");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 5.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_ORE));

    //--Compute rank.
    int tRank = pSourceGem->GetRank();

    //--If there is a merge gem, add its rank.
    if(pMergeGem) tRank += pMergeGem->GetRank();

    //--Render how many of each adamantite type are needed to upgrade the gem. If an adamantite type
    //  is missing, render it in red.
    for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
    {
        //--If the cost is zero, ignore it.
        int tCost = ComputeGemMergeCost(tRank, i);
        if(tCost == 0) continue;

        //--If the cost is too high, switch to red.
        int tCraftingCount = rInventory->GetCraftingCount(i);
        if(tCost > tCraftingCount)
        {
            tHasEnough = false;
            StarlightColor::SetMixer(pColorMix, 0.0f, 0.0f, 1.0f);
        }
        else
        {
            StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
        }

        //--Render text.
        Images.EquipmentUI.rMainlineFont->DrawTextArgs(cCostR, cAdmY + (cAdmSpc * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", tCost);
    }

    //--Clean.
    StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);

    //--If there is not enough adamantite available, render a red warning below the adamantite counts.
    if(!tHasEnough)
    {
        StarlightColor::SetMixer(pColorMix, 0.0f, 0.0f, 1.0f);
        Images.EquipmentUI.rMainlineFont->DrawText(cAdmIconX, 297.0f, 0, 1.0f, "Not enough adamantite");
    }
}
