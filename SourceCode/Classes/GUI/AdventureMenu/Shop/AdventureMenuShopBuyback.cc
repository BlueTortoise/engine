//--Base
#include "AdventureMenu.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"

//--[Shop Fading]
#define FADE_MODE_NONE 0
#define FADE_INITIAL_OUT 1
#define FADE_INITIAL_HOLD 2
#define FADE_INITIAL_IN 3
#define FADE_FINAL_OUT 4
#define FADE_FINAL_HOLD 5
#define FADE_FINAL_IN 6

#define FADE_TICKS_OUT 15
#define FADE_TICKS_HOLD 30
#define FADE_TICKS_IN 15

//#define ADINV_BUYBACK_DEBUG
#ifdef ADINV_BUYBACK_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//=========================================== System ==============================================
//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void AdventureMenu::UpdateShopBuyback()
{
    //--[Setup]
    //--Quick-access pointers.
    DebugPush(true, "AdvMenu Buyback - Begin Update.\n");
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    SugarLinkedList *rBuybackList = rInventory->GetBuybackList();

    //--Storage.
    int tOldCursor = mShopCursor;

    //--Always run this timer.
    mCharacterWalkTimer ++;

    //--[Exchange Mode]
    //--In exchanging mode, prompt the player.
    if(mIsExchanging)
    {
        //--Debug.
        DebugPrint("Exchange mode.\n");

        //--If the player pushes "Activate", they will buy the item.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Get the package in question.
            ShopInventoryPack *rItemPackage = (ShopInventoryPack *)rBuybackList->GetElementBySlot(mShopCursor);
            if(!rItemPackage)
            {
            }
            else
            {
                //--Decrement the player's cash.
                rInventory->SetPlatina(rInventory->GetPlatina() - rItemPackage->mPriceOverride);

                //--Move the item into the inventory.
                rInventory->RegisterItem(rItemPackage->mItem);
                rItemPackage->mItem = NULL;

                //--Delete the package from the buyback list.
                rBuybackList->RemoveElementP(rItemPackage);

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");

                //--All cases end the exchange.
                mIsExchanging = false;
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");

            //--All cases end the exchange.
            mIsExchanging = false;

            //--If there are no more items on the buyback list, exit this mode.
            if(rBuybackList->GetListSize() < 1)
            {
                mShopModeType = AM_SELECT_SHOP_MODE;
                mShopCursor = AM_SHOP_SELECT_BUYBACK;
            }
            //--Make sure the cursor doesn't run over the end of the list.
            else
            {
                mShopCursor --;
                if(mShopCursor < 1) mShopCursor = 0;
            }

        }
    }
    //--Selecting which item to buyback.
    else
    {
        //--Debug.
        DebugPrint("Selecting buyback item.\n");

        //--Down increases the cursor.
        if(rControlManager->IsFirstPress("Down"))
        {
            //--Increment if in range.
            mShopCursor ++;
            if(mShopCursor >= rBuybackList->GetListSize()) mShopCursor = rBuybackList->GetListSize() - 1;
            if(mShopCursor < 0) mShopCursor = 0;
        }
        //--Up decreases the cursor.
        else if(rControlManager->IsFirstPress("Up"))
        {
            //--Increment if in range.
            mShopCursor --;
            if(mShopCursor < 0) mShopCursor = 0;
        }

        //--Get the item currently highlighted.
        ShopInventoryPack *rDescriptionItemPack = (ShopInventoryPack *)rBuybackList->GetElementBySlot(mShopCursor);

        //--Left moves the comparison cursor down one.
        if(rControlManager->IsFirstPress("Left"))
        {
            //--Get the item under consideration.
            if(!rDescriptionItemPack)
            {
                DebugPop("Advmenu Buyback - Pressed left, no item under consideration.\n");
                return;
            }

            //--Attempt to decrement by one until a valid character is found.
            /*
            for(int i = 0; i < AC_PARTY_MAX - 1; i ++)
            {
                //--Compute slot.
                int tSlot = (mComparisonCharacter - i - 1) % AC_PARTY_MAX;

                //--Get the character.
                AdventureCombatEntity *rCharacter = AdventureCombatUI::Fetch()->GetPartyMember(tSlot);
                if(!rCharacter) continue;

                //--If it's usable by this character, stop here.
                if(rDescriptionItem->IsEquippableBy(rCharacter->GetName()))
                {
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    mComparisonCharacter = tSlot;
                    DebugPop("Advmenu Buyback - Finished description compare iteration left.\n");
                    return;
                }
            }*/
        }
        //--Right moves the comparison cursor up one.
        else if(rControlManager->IsFirstPress("Right"))
        {
            //--Get the item under consideration.
            if(!rDescriptionItemPack)
            {
                DebugPop("Advmenu Buyback - Pressed right, no item under consideration.\n");
                return;
            }

            //--Attempt to increment the cursor until a valid character is found.
            /*
            for(int i = 0; i < AC_PARTY_MAX - 1; i ++)
            {
                //--Compute slot.
                int tSlot = (mComparisonCharacter + i + 1) % AC_PARTY_MAX;

                //--Get the character.
                AdventureCombatEntity *rCharacter = AdventureCombatUI::Fetch()->GetPartyMember(tSlot);
                if(!rCharacter) continue;

                //--If it's usable by this character, stop here.
                if(rDescriptionItem->IsEquippableBy(rCharacter->GetName()))
                {
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    mComparisonCharacter = tSlot;
                    DebugPop("Advmenu Buyback - Finished description compare iteration right.\n");
                    return;
                }
            }*/
        }

        //--Activate will purchase the item.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Check the item.
            ShopInventoryPack *rBuyingItemPack = (ShopInventoryPack *)rBuybackList->GetElementBySlot(mShopCursor);
            if(!rBuyingItemPack)
            {
                DebugPop("Advmenu Buyback - Pressed activate, no item in slot %i.\n", mShopCursor);
                return;
            }

            //--Check the price.
            int tPrice = rBuyingItemPack->mPriceOverride;
            if(tPrice == -1) tPrice = rBuyingItemPack->mItem->GetValue();

            //--If the player can afford the item, add it to their inventory.
            if(tPrice <= rInventory->GetPlatina())
            {
                mIsExchanging = true;
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Otherwise, play a sound effect to indicate failure.
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
        }
    }

    //--If the item changes, recheck the comparison character case. Also play sound effects.
    if(tOldCursor != mShopCursor)
    {
        //--Debug.
        DebugPrint("Cursor changed.\n");

        //--[SFX]
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--[Recentering]
        //--On a decrement:
        if(mShopCursor < tOldCursor)
        {
            //--Get where the cursor is on the page.
            int tPosition = mShopCursor - mShopSkip;
            if(tPosition < 2) mShopSkip = mShopCursor - 2;

            //--Clamp.
            if(mShopSkip < 0) mShopSkip = 0;
        }
        //--On an increment:
        else if(mShopCursor > tOldCursor)
        {
            //--Get where the cursor is on the page.
            int tPosition = mShopCursor - mShopSkip;
            if(tPosition > AM_SHOP_ITEMS_PER_PAGE - 3) mShopSkip = mShopCursor - (AM_SHOP_ITEMS_PER_PAGE - 3);

            //--Clamp.
            if(mShopSkip > rBuybackList->GetListSize() - AM_SHOP_ITEMS_PER_PAGE) mShopSkip = rBuybackList->GetListSize() - AM_SHOP_ITEMS_PER_PAGE;
        }

        //--[Rechecking Comparison Character]
        //--Flag.
        //int tPreviousComparison = mComparisonCharacter;

        //--Get the item under consideration.
        ShopInventoryPack *rDescriptionItemPack = (ShopInventoryPack *)rBuybackList->GetElementBySlot(mShopCursor);
        if(!rDescriptionItemPack)
        {
            DebugPop("Advmenu Buyback - Finished, no comparison item.\n");
            return;
        }

        //--If the item is not a piece of equipment, we can't compare it to anything.
        if(!rDescriptionItemPack->mItem->IsEquipment())
        {
            DebugPop("Advmenu Buyback - Finished, comparison item is not equipment.\n");
            return;
        }
    }
    DebugPop("Advmenu Buyback - Finished update normally.\n");
}

//=========================================== Drawing =============================================
void AdventureMenu::RenderShopBuyback(bool pIsActiveMode, float pColorMix)
{
    ///--[Documentation and Setup]
    //--Render the buying portion of the submenu. The static backing and text are already rendered, we just need to
    //  render the item icons and descriptions of the highlighted item if applicable.
    if(!Images.mIsReady) return;
    DebugPush(true, "AdvMenu Buyback - Begin Render.\n");
    SugarLinkedList *rBuybackList = AdventureInventory::Fetch()->GetBuybackList();

    ///--[Shop Inventory Listing]
    //--Similar to how the inventory renders information as a table. Item properties are not listed, though.
    float cHeaderY = 124.0f;
    float cIconX  = 30.0f;
    float cIconW  = 22.0f;
    float cFaceX = cIconX + cIconW;
    float cFaceW = 32.0f;
    float cNameX  = cFaceX + cFaceW;
    float cValueX = 360.0f;
    float cValueXRight = 432.0f;
    float cGemsX = 445.0f;
    float cUnderlayLft =  15.0f;
    float cUnderlayRgt = 579.0f;
    float cDescriptionLft = 557.0f;

    //--Text headers.
    StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
    Images.EquipmentUI.rHeadingFont->DrawText(cIconX,  cHeaderY, 0, 1.0f, "Name");
    Images.EquipmentUI.rHeadingFont->DrawText(cValueX, cHeaderY, 0, 1.0f, "Cost");
    Images.EquipmentUI.rHeadingFont->DrawText(cGemsX,  cHeaderY, 0, 1.0f, "Gems");

    //--Y positioning
    float cTop = cHeaderY + 44.0f;
    float tCurrentY = cTop;
    float cSpcY = 23.0f;

    //--Variables.
    int tRendersLeft = AM_SHOP_ITEMS_PER_PAGE;
    int tSkipsLeft = mShopSkip;

    //--[Rendering Loop]
    int tCurrentItem = 0;
    DebugPrint("Rendering %i items.\n", rBuybackList->GetListSize());
    ShopInventoryPack *rItemPack = (ShopInventoryPack *)rBuybackList->PushIterator();
    while(rItemPack)
    {
        //--If this item should not render, skip it.
        if(tSkipsLeft > 0)
        {
            tCurrentItem ++;
            tSkipsLeft --;
            rItemPack = (ShopInventoryPack *)rBuybackList->AutoIterate();
            continue;
        }

        //--[Backing]
        if(tCurrentItem % 2 == 1)
        {
            SugarBitmap::DrawRectFill(cUnderlayLft, tCurrentY, cUnderlayRgt, tCurrentY + cSpcY, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
            StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
        }

        //--[Item Icon]
        //--Render the item's icon here. It's a 22px entry.
        SugarBitmap *rItemIcon = rItemPack->mItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(cIconX, tCurrentY);

        //--Compute value.
        int tValue = rItemPack->mPriceOverride;
        if(tValue == -1) tValue = rItemPack->mItem->GetValue();

        //--[Cursor Handling]
        //--If this is the selected item, render the cursor over it. If the mode is inactive, don't render this.
        //  Inactive mode is used when the player is hovering on Buy mode, but hasn't entered it yet.
        if(tCurrentItem == mShopCursor && pIsActiveMode)
        {
            //--Render the item description.
            RenderItemDescription(cDescriptionLft, rItemPack->mItem);

            //--Render the properties and comparisons.
            RenderShopBuyProperties(rItemPack->mItem, mComparisonCharacter, pColorMix);

            //--Player cannot afford the item, so grey it out.
            if(tValue > AdventureInventory::Fetch()->GetPlatina())
            {
                StarlightColor::SetMixer(1.0f * pColorMix, 0.0f * pColorMix, 0.0f * pColorMix, 1.0f);
            }
            //--Regular mixing.
            else
            {
                StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            }

            //--Tint the color.
            StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
        }
        //--Not selected or is inactive mode.
        else
        {

            //--Player cannot afford the item, so grey it out.
            if(tValue > AdventureInventory::Fetch()->GetPlatina())
            {
                StarlightColor::SetMixer(0.5f * pColorMix, 0.5f * pColorMix, 0.5f * pColorMix, 1.0f);
            }
            //--Regular mixing.
            else
            {
                StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            }
        }

        //--[Item Name]
        //--Name.
        Images.EquipmentUI.rMainlineFont->DrawText(cNameX, tCurrentY, 0, 1.0f, rItemPack->mItem->GetName());

        //--[Item Cost]
        //--Right-align.
        char tBuffer[32];
        sprintf(tBuffer, "%i", tValue);

        //--Resolve position and render.
        float cXPos = cValueXRight - Images.EquipmentUI.rMainlineFont->GetTextWidth(tBuffer);
        Images.EquipmentUI.rMainlineFont->DrawTextArgs(cXPos, tCurrentY, 0, 1.0f, tBuffer);
        StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);

        //--[Gem Slots]
        //--Items which have gem slots render them here. They are always empty in the shop UI.
        float cGemW = 23.0f;
        int tGemSlots = rItemPack->mItem->GetGemSlots();
        for(int i = 0; i < tGemSlots; i ++)
        {
            Images.VendorUI.rGemEmpty22px->Draw(cGemsX + (i * cGemW), tCurrentY);
        }

        //--If the renders have run out, stop here.
        tRendersLeft --;
        if(tRendersLeft < 1)
        {
            rBuybackList->PopIterator();
            break;
        }

        //--Next.
        tCurrentItem ++;
        tCurrentY = tCurrentY + cSpcY;
        rItemPack = (ShopInventoryPack *)rBuybackList->AutoIterate();
    }

    ///--[Scrollbar]
    //--Render a scrollbar to indicate how much of the inventory is represented.
    if(rBuybackList->GetListSize() > AM_SHOP_ITEMS_PER_PAGE)
    {
        //--Debug.
        DebugPrint("Rendering scrollbar.\n");

        //--Binding.
        Images.VendorUI.rScrollbarFront->Bind();

        //--Scrollbar front. Stretches dynamically.
        float cScrollTop = 158.0f;
        float cScrollHei = 366.0f;

        //--Determine what percentage of the inventory is presently represented.
        int tMaxOffset = rBuybackList->GetListSize();
        float cPctTop = (mShopSkip                         ) / (float)tMaxOffset;
        float cPctBot = (mShopSkip + AM_SHOP_ITEMS_PER_PAGE) / (float)tMaxOffset;

        //--Positions and Constants.
        float cLft = 548.0f;
        float cRgt = cLft + Images.InventoryUI.rScrollbarFront->GetWidth();
        float cEdg = 6.0f / (float)Images.InventoryUI.rScrollbarFront->GetHeight();

        //--Compute where the bar should be.
        float cTopTop = cScrollTop + (cPctTop * cScrollHei);
        float cTopBot = cTopTop + 6.0f;
        float cBotBot = cScrollTop + (cPctBot * cScrollHei);
        float cBotTop = cBotBot - 6.0f;

        glBegin(GL_QUADS);
            //--Render the top of the bar.
            glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTopTop);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTopTop);
            glTexCoord2f(1.0f, cEdg); glVertex2f(cRgt, cTopBot);
            glTexCoord2f(0.0f, cEdg); glVertex2f(cLft, cTopBot);

            //--Render the bottom of the bar.
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(1.0f, 1.0f);        glVertex2f(cRgt, cBotBot);
            glTexCoord2f(0.0f, 1.0f);        glVertex2f(cLft, cBotBot);

            //--Render the middle of the bar.
            glTexCoord2f(0.0f, cEdg);        glVertex2f(cLft, cTopBot);
            glTexCoord2f(1.0f, cEdg);        glVertex2f(cRgt, cTopBot);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
        glEnd();


        //--Backing. Already positioned.
        Images.VendorUI.rScrollbarBack->Draw();
    }

    ///--[Exchange Dialogue]
    //--In exchange mode, render the confirmation dialogue.
    if(mIsExchanging)
    {
        //--Debug.
        DebugPrint("Exchange mode.\n");

        //--Get the item and verify it.
        ShopInventoryPack *rPurchaseItem = (ShopInventoryPack *)rBuybackList->GetElementBySlot(mShopCursor);
        if(!rPurchaseItem)
        {
            DebugPop("Advmenu Buyback - Failed during exchange, no item found.\n");
            return;
        }

        //--Black out.
        SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.75f));

        //--Overlay.
        Images.VendorUI.rConfirmOverlay->Draw();

        //--Compute value.
        int tValue = rPurchaseItem->mPriceOverride;
        if(tValue == -1) tValue = rPurchaseItem->mItem->GetValue();

        //--Buffer text.
        char tBuffer[128];
        sprintf(tBuffer, "Purchase %s for %i platina?", rPurchaseItem->mItem->GetName(), tValue);

        //--Check the length.
        float tTextLength = Images.VendorUI.rHeadingFont->GetTextWidth(tBuffer);

        //--Width is not too long. Render normally.
        if(tTextLength < 500.0f)
        {
            Images.VendorUI.rHeadingFont->DrawText(683.0f, 210.0f, SUGARFONT_AUTOCENTER_X, 1.0f, tBuffer);
        }
        //--Too long, split it into two lines.
        else
        {
            Images.VendorUI.rHeadingFont->DrawTextArgs(683.0f, 210.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Purchase %s", rPurchaseItem->mItem->GetName());
            Images.VendorUI.rHeadingFont->DrawTextArgs(683.0f, 245.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "for %i Platina?", tValue);
        }

        //--Other text.
        Images.VendorUI.rMainlineFont->DrawTextArgs(683.0f, 345.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Z to confirm");
        Images.VendorUI.rMainlineFont->DrawTextArgs(683.0f, 365.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "X to cancel");
    }
    DebugPop("Advmenu Buyback - Finished render normally.\n");
}
