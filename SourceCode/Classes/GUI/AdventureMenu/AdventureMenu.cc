//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "AdvHelp.h"
#include "AdvCombatEntity.h"
#include "FlexMenu.h"
#include "StringEntry.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
AdventureMenu::AdventureMenu()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVENTUREMENU;

    //--[AdventureMenu]
    //--System
    mStartedHidingThisTick = false;
    mHandledUpdate = false;
    mMainOpacityTimer = 0;
    mIsVisible = false;
    mIsCampfireMode = false;
    mCurrentMode = AM_MODE_BASE;

    //--Common
    mCurrentCursor = 0;
    mMenuWidest = 0.0f;
    mMenuWidestSecond = 0.0f;

    //--Timers
    mHealVisTimer = 0;

    //--Main Menu
    float cTextHei = 20.0f;
    float cIndent = AM_STD_INDENT;
    mMainDimensions.SetWH(0.0f, 32.0f, 288.0f, 150.0f);
    mCraftingDimensions.SetWH(mMainDimensions.mLft, mMainDimensions.mBot + 4.0f, mMainDimensions.GetWidth(), (cTextHei * 6.0f) + (cIndent * 2.0f));
    mPlatinaDimensions.SetWH(mMainDimensions.mLft, mCraftingDimensions.mBot + 4.0f, mMainDimensions.GetWidth(), (cTextHei * 1.0f) + (cIndent * 2.0f));
    mCatalystDimensions.SetWH(mMainDimensions.mLft, mPlatinaDimensions.mBot + 4.0f, mMainDimensions.GetWidth(), (cTextHei * (CATALYST_TOTAL + 3)) + (cIndent * 2.0f));
    mPartyBoardDimensions.SetWH(mMainDimensions.mRgt + 8.0f, mMainDimensions.mTop, 150.0f, 150.0f);

    //--Main Grid Menu
    mIsMainHelpVisible = false;
    mMainHelpTimer = 0;
    mShowHelpString = new StarlightString();
    mBaseVars.Initialize();

    //--Equipment Menu
    mEquipVisibilityTimer = 0;
    mRebuildModifyList = false;
    mEquipMinimumRender = 0;
    mCharacterIndex = 0;
    mIsModifying = false;
    mModifySlot = 0;
    mEquipDimensions.Set(mMainDimensions.mRgt + 8.0f, mMainDimensions.mTop, VIRTUAL_CANVAS_X - 32.0f, 200.0f);
    mEquipListDimensions.Set(0.0f, 0.0f, 1.0f, 1.0f);
    mValidEquipsList = new SugarLinkedList(false);
    mIsSocketEditingMode = false;
    mSocketEditTimer = 0;
    mSocketEditSlot = 0;
    mSocketQuerySlot = 0;
    mGemMinimumRender = 0;

    //--Items Menu
    mItemVisTimer = 0;
    mInventorySkip = 0;
    mItemDimensions.Set(mMainDimensions.mRgt + 8.0f, mMainDimensions.mTop + 0.0f, VIRTUAL_CANVAS_X - 32.0f, mCraftingDimensions.mBot);
    mItemPropertyDimensions.Set(mMainDimensions.mRgt + 8.0f, mPlatinaDimensions.mTop, VIRTUAL_CANVAS_X - 32.0f, VIRTUAL_CANVAS_Y - AM_CHARACTER_CARD_Y - 8.0f);

    //--[Saving / File Selection]
    //--System
    mIsBenchMode = false;
    mIsEditingNoteForNewSave = false;
    mIsSelectFileMode = false;
    mEmptySaveSlotsTotal = AM_SCAN_TOTAL;
    mSaveOpacityTimer = 0;

    //--Selection and Navigation
    mSelectFileCursor = 0;
    mSelectFileLastColumn = 2;
    mSelectFileOffset = 0;
    mHighestCursor = 0;
    mEditingFileCursor = -1;
    mArrowBobTimer = 0;
    mArrowOpacityTimerLft = 0;
    mArrowOpacityTimerRgt = 0;

    //--Page Moving
    mIsPageMoving = false;
    mIsPageMovingUp = false;
    mPageMoveTimer = 0;

    //--File Information Storage
    mCurrentSortingType = xLastSavefileSortingType;
    for(int i = 0; i < AM_SCAN_TOTAL; i ++)
    {
        mFileListingIndex[i] = i;
    }
    memset(mFileListing, 0, sizeof(char) * AM_SCAN_TOTAL * 80);
    memset(mLoadingPacks, 0, sizeof(LoadingPack *) * AM_SCAN_TOTAL);
    memset(mExistingFileListing, 0, sizeof(bool) * AM_SCAN_TOTAL);

    //--Entering Notes
    mIsEnteringNotes = false;
    mNoteSlot = 0;
    mStringEntryForm = new StringEntry();

    //--Deleting a Save
    mSaveDeletionIndex = -1;
    mSaveDeletionIndexOld = -1;
    mSaveDeletionOpacityTimer = 0;

    //--[Forms Submenu]
    //--System
    mIsFormsMode = false;
    mFormsPartyMember = -1;

    //--Character Selection.
    mFormsOpacityTimer = 0;
    mFormsGridCurrent = 0;
    mFormsPartyList = new SugarLinkedList(false);
    mFormsPartyGrid = new SugarLinkedList(true);
    mFormsPartyGridImg = new SugarLinkedList(false);

    //--Form Selection.
    mFormSubOpacityTimer = 0;
    mFormSubGridCurrent = 0;
    rFormCharacter = NULL;
    mFormSubGrid = new SugarLinkedList(true);
    mFormSubPaths = new SugarLinkedList(true);
    mFormSubJobScripts = new SugarLinkedList(true);
    mFormSubGridImg = new SugarLinkedList(false);
    mFormSubComparisons = new SugarLinkedList(true);
    memset(mStatComparePacks, 0, sizeof(StatComparePack) * AM_STAT_COMPARE_PACKS_TOTAL);

    //--[Other Submenus]
    //--Gemcutter Menu
    mIsGemcutterMode = false;
    mGemcutterCursor = 0;
    mSelectedGemCursor = -1;
    mGemConfirmCutMode = false;

    //--Status Menu
    mStatusTimer = 0;
    mStatusCursor = 0;
    mStatusBacking.Set(-1.0f, -1.0f, 0.0f, 0.0f);

    //--Shop Menu
    mIsShopMode = false;
    mShopCanGemcut = false;
    mShopName[0] = '\0';
    mShopModeType = AM_SELECT_SHOP_MODE;
    mShopCursor = 0;
    mShopSkip = 0;
    mShopSkipStore = 0;
    mIsExchanging = false;
    mPurchaseQuantity = 1;
    mShopTeardownPath = NULL;
    mComparisonCharacter = 0;
    mComparisonSlot = 0;
    mCharacterWalkTimer = 0;
    mShopInventory = new SugarLinkedList(true);
    mDescriptionLines = new SugarLinkedList(true);
    mChangeCharString = new StarlightString();
    mChangeItemString = new StarlightString();

    //--Shop Fading
    mIsShopFading = false;
    mShopFadeMode = 0;
    mShopFadeTimer = 0;

    //--Options Menu
    mOptionsVisibilityTimer = 0;
    mOptionsCursor = 0;
    mOptionsBacking.Set(-1.0f, -1.0f, 0.0f, 0.0f);
    mOptionOldMusic = 0.0f;
    mOptionOldSound = 0.0f;
    mOptionOldDoctor = false;
    mOptionOldTourist = false;
    mOptionOldAmbientLight = 0.0f;

    //--Localized FlexMenu for Rebinding
    mFlexMenuIsRebinding = false;
    mLocalizedRebindMenu = new FlexMenu();

    //--Quit Menu
    mQuitVisTimer = 0;
    mQuitCursor = 0;
    mIsTitleQuitConfirmMode = false;
    mIsFullQuitConfirmMode = false;
    mIsFadingForQuit = false;
    mQuitFadeTimer = 0;

    //--Map Mode
    mMapFadeTimer = -1;
    mMapFocusPointX = 0;
    mMapFocusPointY = 0;
    mMapZoom = 1.0f;
    mMapZoomStart = 1.0f;
    mMapZoomDest = 1.0f;
    mMapZoomTimer = AM_MAP_ZOOM_TICKS;
    rActiveMap = NULL;
    rActiveMapOverlay = NULL;
    mPlayerMapPositionX = -100;
    mPlayerMapPositionY = -100;
    mMapScrollTimerX = 0.0f;
    mMapScrollTimerY = 0.0f;

    //--Advanced Map Mode
    mAdvancedMapLastRoll = rand() % 1000;
    mAdvancedMapReroll = 15;
    mAdvancedMapRemapX = 0.0f;
    mAdvancedMapRemapY = 0.0f;
    mAdvancedMapScaleX = 1.0f;
    mAdvancedMapScaleY = 1.0f;
    mMapLayerList = new SugarLinkedList(true);

    //--Advanced Map Archive
    mArchiveAdvancedMapLastRoll = rand() % 1000;
    mArchiveAdvancedMapReroll = 15;
    mArchiveAdvancedMapRemapX = 0.0f;
    mArchiveAdvancedMapRemapY = 0.0f;
    mArchiveAdvancedMapScaleX = 1.0f;
    mArchiveAdvancedMapScaleY = 1.0f;
    mArchiveMapLayerList = NULL;

    //--Campfire Grid Menu
    mCampfireGridOpacityTimer = 0;
    mCampfireGridCurrent = AM_SAVE_REST;
    for(int i = 0; i < AM_SAVE_TOTAL; i ++)
    {
        mCampfireGrid[i].Initialize();
    }

    //--[Party Chatting]
    //--System
    mIsChatMode = false;
    mChatGridCurrent = 0;
    mChatGridOpacityTimer = 0;
    mChatGrid = new SugarLinkedList(true);
    mChatGridImg = NULL;
    mChatListing = NULL;

    //--[Costumes Menu]
    //--System
    mIsCostumesMenu = false;
    mCostumeGridCurrent = 0;
    mCostumeGridOpacityTimer = 0;
    mCostumeGrid = new SugarLinkedList(true);
    mCostumeGridHeaders = new SugarLinkedList(true);
    mCostumeGridSublists = new SugarLinkedList(true);

    //--Costume Selection
    mIsCostumesSubMenu = false;
    mCostumeCharacterIndex = -1;
    mCostumeCharacterCurrent = 0;
    mCostumeCharacterOpacityTimer = 0;
    mCostumeCharacterGrid = new SugarLinkedList(true);

    //--[Skillbook Menu]
    mIsSkillbookMode = false;
    mSkillbookCursor = -1;
    mSkillbookList = new SugarLinkedList(true);

    ///--[Skills Menu]
    mSkillMenu.Initialize();

    ///--[Field Abilities Menu]
    mFieldAbilityMenu.Initialize();

    //--[Relive Menu]
    mIsReliveMode = false;
    mReliveGridOpacityTimer = 0;
    mReliveGridCurrent = 0;
    mReliveList = new SugarLinkedList(true);
    mReliveGrid = new SugarLinkedList(true);

    //--[Warp Menu]
    mIsWarpMode = false;
    mWarpHandledInput = false;
    mWarpZoomFactor = 1.0f;
    mWarpGridCurrent = 0;
    mWarpGridOpacityTimer = 0;
    mWarpGridSelectedRegion = -1;
    mWarpSelectedRoom = 0;
    mWarpRegionOpacityTimer = 0;
    mWarpReposTimer = 0;
    mWarpReposXStart = 0.0f;
    mWarpReposYStart = 0.0f;
    mWarpReposXFinish = 0.0f;
    mWarpReposYFinish = 0.0f;
    mWarpList = new SugarLinkedList(true);
    mWarpGrid = new SugarLinkedList(true);
    mWarpGridImg = new SugarLinkedList(false);
    mWarpRegionImages = new SugarLinkedList(false);

    ///--[Campfire Password Entry]
    //--System
    mIsPasswordEntry = false;
    mPasswordOpacity = 0;

    ///--[Help Menus]
    mMainMenuHelp = new AdvHelp();

    //--Images
    memset(&Images, 0, sizeof(Images));

    //--[Construction]
    //--Resolve graphics.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Common parts.
    Images.Data.rBorderCard = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/System/CardBorder");
    Images.Data.rMenuFont   = rDataLibrary->GetFont("Adventure Menu Main");

    //--Base Menu
    Images.BaseMenu.Construct();

    //--Adamantite. Can be bypassed if one doesn't load.
    SugarBitmap *rWhitePixel = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/System/System/WhitePixel");
    Images.BaseMenu.rAdamantiteImg[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Symbols22/AdmPowder");
    Images.BaseMenu.rAdamantiteImg[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Symbols22/AdmFlakes");
    Images.BaseMenu.rAdamantiteImg[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Symbols22/AdmShards");
    Images.BaseMenu.rAdamantiteImg[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Symbols22/AdmPieces");
    Images.BaseMenu.rAdamantiteImg[4] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Symbols22/AdmChunks");
    Images.BaseMenu.rAdamantiteImg[5] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Symbols22/AdmOre");
    for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
    {
        if(!Images.BaseMenu.rAdamantiteImg[i]) Images.BaseMenu.rAdamantiteImg[i] = rWhitePixel;
    }

    //--Campfire Menu
    Images.CampfireMenu.rHeadingFont    = rDataLibrary->GetFont("Adventure Menu Campfire Header");
    Images.CampfireMenu.rMainlineFont   = rDataLibrary->GetFont("Adventure Menu Campfire Main");
    Images.CampfireMenu.rBaseBot        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/BaseBot");
    Images.CampfireMenu.rBaseMid        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/BaseMid");
    Images.CampfireMenu.rBaseTop        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/BaseTop");
    Images.CampfireMenu.rButton         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/Button");
    Images.CampfireMenu.rDescriptionBox = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/DescriptionBox");
    Images.CampfireMenu.rHeader         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/Header");
    Images.CampfireMenu.rComparison     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/Comparison");
    Images.CampfireMenu.rMapPin         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/MapPin");
    Images.CampfireMenu.rMapPinSelected = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/MapPinSelected");
    Images.CampfireMenu.rIconFrame      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/ICO|Frame");
    Images.CampfireMenu.rIconFrameLarge = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/CHATICO|Frame");

    Images.CampfireMenu.rIcons[AM_SAVE_REST]       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/ICO|Rest");
    Images.CampfireMenu.rIcons[AM_SAVE_CHAT]       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/ICO|Chat");
    Images.CampfireMenu.rIcons[AM_SAVE_SAVE]       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/ICO|Save");
    Images.CampfireMenu.rIcons[AM_SAVE_COSTUMES]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/ICO|Costume");
    Images.CampfireMenu.rIcons[AM_SAVE_SKILLBOOKS] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/ICO|Skills");
    Images.CampfireMenu.rIcons[AM_SAVE_FORM]       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/ICO|Transform");
    Images.CampfireMenu.rIcons[AM_SAVE_WARP]       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/ICO|Warp");
    Images.CampfireMenu.rIcons[AM_SAVE_RELIVE]     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/ICO|Relive");
    Images.CampfireMenu.rIcons[AM_SAVE_PASSWORD]   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/CampfireMenu/ICO|Password");

    //--Costumes Menu
    Images.CostumeUI.rHeadingFont      = rDataLibrary->GetFont("Adventure Menu Costume Header");
    Images.CostumeUI.rVSmallFont       = rDataLibrary->GetFont("Adventure Menu Costume Small");
    Images.CostumeUI.rFrameLower       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Costume/FrameLower");
    Images.CostumeUI.rFrameLowerActive = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Costume/FrameLowerActive");
    Images.CostumeUI.rFrameUpper       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Costume/FrameUpper");
    Images.CostumeUI.rHeader           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Costume/Header");
    Images.CostumeUI.rSelectorLower    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Costume/SelectorLower");
    Images.CostumeUI.rSelectorUpper    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Costume/SelectorUpper");

    //--Equipment Menu
    LoadEquipMenu();

    //--File Select Menu
    Images.FileSelectUI.rHeadingFont    = rDataLibrary->GetFont("Adventure Menu File Select Header");
    Images.FileSelectUI.rMainlineFont   = rDataLibrary->GetFont("Adventure Menu File Select Main");
    Images.FileSelectUI.rArrowLft       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SaveMenu/ArrowLft");
    Images.FileSelectUI.rArrowRgt       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SaveMenu/ArrowRgt");
    Images.FileSelectUI.rHeader         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SaveMenu/Header");
    Images.FileSelectUI.rNewFileButton  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SaveMenu/NewFileButton");
    Images.FileSelectUI.rGridBackingCh0 = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/SaveMenu/GridBackingCh0");

    //--Gemcutter UI
    Images.GemcutterUI.rHeadingFont    = rDataLibrary->GetFont("Adventure Menu Gemcutter Header");
    Images.GemcutterUI.rMainlineFont   = rDataLibrary->GetFont("Adventure Menu Gemcutter Main");
    Images.GemcutterUI.rBanner         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/GemCutter/Banner");
    Images.GemcutterUI.rBase           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/GemCutter/Base");
    Images.GemcutterUI.rButtonBack     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/GemCutter/ButtonBack");
    Images.GemcutterUI.rGemSelectBack  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/GemCutter/GemSelectBack");
    Images.GemcutterUI.rGemSelectFrame = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/GemCutter/GemSelectFrame");
    Images.GemcutterUI.rInfoBox        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/GemCutter/InfoBox");
    Images.GemcutterUI.rPlatinaBanner  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/GemCutter/PlatinaBanner");

    //--Inventory Menu
    Images.InventoryUI.rHeadingFont    = rDataLibrary->GetFont("Adventure Menu Inventory Header");
    Images.InventoryUI.rMainlineFont   = rDataLibrary->GetFont("Adventure Menu Inventory Main");
    Images.InventoryUI.rFrames         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Inventory/Frames");
    Images.InventoryUI.rScrollbarBack  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Inventory/ScrollbarBack");
    Images.InventoryUI.rScrollbarFront = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Inventory/ScrollbarFront");

    //--Options Menu
    Images.OptionsUI.rHeadingFont    = rDataLibrary->GetFont("Adventure Menu Options Header");
    Images.OptionsUI.rMainlineFont   = rDataLibrary->GetFont("Adventure Menu Options Main");
    Images.OptionsUI.rBars           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Options/Bars");
    Images.OptionsUI.rConfirmButtons = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Options/ConfirmButtons");
    Images.OptionsUI.rHeader         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Options/Header");
    Images.OptionsUI.rMenuBack       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Options/MenuBack");
    Images.OptionsUI.rMenuInsert     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Options/MenuInsert");
    Images.OptionsUI.rSliderButtons  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Options/SliderButtons");
    Images.OptionsUI.rSliders        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Options/Sliders");
    Images.OptionsUI.rTextBoxes      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Options/TextBoxes");

    //--Status Screen
    LoadStatusMenu();

    //--Skills UI
    Images.SkillsUI.Construct();

    //--Field Ability UI
    Images.FieldAbilityUI.Construct();

    //--Doctor Bag
    Images.DoctorUI.rHeadingFont        = rDataLibrary->GetFont("Adventure Menu Doctor Bag Header");
    Images.DoctorUI.rMainlineFont       = rDataLibrary->GetFont("Adventure Menu Doctor Bag Main");
    Images.DoctorUI.rVerySmallFont      = rDataLibrary->GetFont("Adventure Menu Doctor Bag Small");
    Images.DoctorUI.rBackButton         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/BackButton");
    Images.DoctorUI.rBarFill            = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/BarFill");
    Images.DoctorUI.rBarFrame           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/BarFrame");
    Images.DoctorUI.rBarHeader          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/BarHeader");
    Images.DoctorUI.rHealAllBtn         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/HealAllBtn");
    Images.DoctorUI.rMenuBack           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/MenuBack");
    Images.DoctorUI.rMenuHeaderA        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/MenuHeaderA");
    Images.DoctorUI.rMenuHeaderB        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/MenuHeaderB");
    Images.DoctorUI.rMenuInset          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/MenuInset");
    Images.DoctorUI.rOptionsDeactivated = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/OptionsDeactivated");
    Images.DoctorUI.rPortraitBtn        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/PortraitBtn");
    Images.DoctorUI.rPortraitMask       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DoctorBag/PortraitMask");

    //--Vendor UI
    Images.VendorUI.rHeadingFont     = rDataLibrary->GetFont("Adventure Menu Vendor Header");
    Images.VendorUI.rMainlineFont    = rDataLibrary->GetFont("Adventure Menu Vendor Main");
    Images.VendorUI.rConfirmOverlay  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Vendor/ConfirmOverlay");
    Images.VendorUI.rFramesBuy       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Vendor/FramesBuy");
    Images.VendorUI.rFramesSell      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Vendor/FramesSell");
    Images.VendorUI.rFramesBuyback   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Vendor/FramesBuyback");
    Images.VendorUI.rFramesGemcutter = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Vendor/FramesGemcutter");
    Images.VendorUI.rGemEmpty22px    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Symbols22/GemSlot");
    Images.VendorUI.rCompareCursor   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Vendor/CompareCursor");
    Images.VendorUI.rScrollbarBack   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Vendor/ScrollbarBack");
    Images.VendorUI.rScrollbarFront  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/Vendor/ScrollbarFront");

    //--Form UI
    Images.FormUI.rHeadingFont     = rDataLibrary->GetFont("Adventure Menu Form Header");
    Images.FormUI.rMainlineFont    = rDataLibrary->GetFont("Adventure Menu Form Main");

    //--Make sure everything resolved.
    bool tVerifyData        = VerifyStructure(&Images.Data,         sizeof(Images.Data),          sizeof(void *), false);
    bool tVerifyBaseMenu    = VerifyStructure(&Images.BaseMenu,     sizeof(Images.BaseMenu),      sizeof(void *), false);
    bool tVerifyStatusUI    = VerifyStructure(&Images.StatusUI,     sizeof(Images.StatusUI),      sizeof(void *), false);
    bool tVerifySkillsUI    = VerifyStructure(&Images.SkillsUI,     sizeof(Images.SkillsUI),      sizeof(void *), false);
    bool tVerifyFieldAbUI   = VerifyStructure(&Images.SkillsUI,     sizeof(Images.FieldAbilityUI),sizeof(void *), false);
    bool tVerifyDoctorUI    = VerifyStructure(&Images.DoctorUI,     sizeof(Images.DoctorUI),      sizeof(void *), false);
    bool tVerifyVendorUI    = VerifyStructure(&Images.VendorUI,     sizeof(Images.VendorUI),      sizeof(void *), false);
    bool tVerifyEquipUI     = VerifyStructure(&Images.EquipmentUI,  sizeof(Images.EquipmentUI),   sizeof(void *), false);
    bool tVerifyInventoryUI = VerifyStructure(&Images.InventoryUI,  sizeof(Images.InventoryUI),   sizeof(void *), false);
    bool tVerifyOptionsUI   = VerifyStructure(&Images.OptionsUI,    sizeof(Images.OptionsUI),     sizeof(void *), false);
    bool tVerifyCampfireUI  = VerifyStructure(&Images.CampfireMenu, sizeof(Images.CampfireMenu),  sizeof(void *), false);
    bool tVerifyFileUI      = VerifyStructure(&Images.FileSelectUI, sizeof(Images.FileSelectUI),  sizeof(void *), false);
    bool tVerifyGemcutterUI = VerifyStructure(&Images.GemcutterUI,  sizeof(Images.GemcutterUI),   sizeof(void *), false);
    bool tVerifyCostumeUI   = VerifyStructure(&Images.CostumeUI,    sizeof(Images.CostumeUI),     sizeof(void *), false);
    bool tVerifyFormUI      = VerifyStructure(&Images.FormUI,       sizeof(Images.FormUI),        sizeof(void *), false);
    //fprintf(stderr, "%i %i %i %i - %i %i %i %i - %i %i %i %i\n", tVerifyData, tVerifyBaseMenu, tVerifyStatusUI, tVerifyDoctorUI, tVerifyVendorUI, tVerifyEquipUI, tVerifyInventoryUI, tVerifyOptionsUI, tVerifyCampfireUI, tVerifyFileUI, tVerifyGemcutterUI, tVerifyCostumeUI);

    Images.mIsReady = (tVerifyData        && tVerifyBaseMenu  && tVerifyStatusUI   && tVerifyDoctorUI && tVerifyVendorUI    && tVerifyEquipUI   &&
                       tVerifyInventoryUI && tVerifyOptionsUI && tVerifyCampfireUI && tVerifyFileUI   && tVerifyGemcutterUI && tVerifyCostumeUI &&
                       tVerifySkillsUI    && tVerifyFieldAbUI && tVerifyFormUI);

    //--[Stat Compare Packages]
    mStatComparePacks[AM_STAT_COMPARE_PACK_HPMAX]      .Set(STATS_HPMAX,             (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Health"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_ATTACK]     .Set(STATS_ATTACK,            (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Attack"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_ACCURACY]   .Set(STATS_ACCURACY,          (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Accuracy"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_EVADE]      .Set(STATS_EVADE,             (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Evade"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_INITIATIVE] .Set(STATS_INITIATIVE,        (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Initiative"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_PROTECTION] .Set(STATS_RESIST_PROTECTION, (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Protection"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_SLASH]  .Set(STATS_RESIST_SLASH,      (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Slash"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_STRIKE] .Set(STATS_RESIST_STRIKE,     (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Strike"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_PIERCE] .Set(STATS_RESIST_PIERCE,     (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Pierce"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_FLAME]  .Set(STATS_RESIST_FLAME,      (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Flaming"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_FREEZE] .Set(STATS_RESIST_FREEZE,     (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Freezing"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_SHOCK]  .Set(STATS_RESIST_SHOCK,      (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Shocking"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_CRUSADE].Set(STATS_RESIST_CRUSADE,    (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Crusading"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_OBSCURE].Set(STATS_RESIST_OBSCURE,    (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Obscuring"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_BLEED]  .Set(STATS_RESIST_BLEED,      (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Bleed"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_POISON] .Set(STATS_RESIST_POISON,     (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Poison"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_CORRODE].Set(STATS_RESIST_CORRODE,    (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Corrode"));
    mStatComparePacks[AM_STAT_COMPARE_PACK_RES_TERRIFY].Set(STATS_RESIST_TERRIFY,    (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/StatisticIcons/Terrify"));

    //--[Help String]
    mShowHelpString->SetString("[IMG0] Show Help");
    mShowHelpString->AllocateImages(1);
    mShowHelpString->SetImageP(0, CM_IMG_OFFSET_Y, ControlManager::Fetch()->ResolveControlImage("F1"));
    mShowHelpString->CrossreferenceImages();

    //--[Finalization]
    //--Objects which depend on these for sizing build themselves here.
    if(!Images.mIsReady) return;

    //--Call campfire positions builder.
    BuildCampfirePositions();
}
AdventureMenu::~AdventureMenu()
{
    delete mValidEquipsList;
    delete mFormsPartyList;
    delete mFormsPartyGrid;
    delete mFormsPartyGridImg;
    delete mFormSubGrid;
    delete mFormSubPaths;
    delete mFormSubJobScripts;
    delete mFormSubGridImg;
    delete mFormSubComparisons;
    delete mShopInventory;
    delete mDescriptionLines;
    delete mChangeCharString;
    delete mChangeItemString;
    free(mShopTeardownPath);
    for(int i = 0; i < AM_SCAN_TOTAL; i ++) free(mLoadingPacks[i]);
    delete mMapLayerList;
    delete mArchiveMapLayerList;
    delete mChatGrid;
    delete mChatGridImg;
    delete mChatListing;
    delete mCostumeGrid;
    delete mCostumeGridHeaders;
    delete mCostumeGridSublists;
    delete mCostumeCharacterGrid;
    delete mSkillbookList;
    delete mReliveList;
    delete mReliveGrid;
    delete mWarpList;
    delete mWarpGrid;
    delete mWarpGridImg;
    delete mWarpRegionImages;
    delete mStringEntryForm;
    mSkillMenu.Deallocate();
    mFieldAbilityMenu.Deallocate();
}

//--[Public Statics]
//--Variable used when reading skillbooks. If the player accesses a skillbook from the menu and they already know
//  the ability, a different text blurb plays, and the menu re-activates afterwards.
bool AdventureMenu::xIsSkillbookMenu = false;

//--Script used by the form selection menu. When a character goes to transform, this script will be
//  called to provide a list of transformations.
char *AdventureMenu::xFormResolveScript = NULL;

//--Script used by the form selection menu. Builds a list of party members who appear on the transformation
//  grid. This may include characters who can't transform.
char *AdventureMenu::xFormPartyResolveScript = NULL;

//--Script used to resolve who can be talked to when at a campfire.
char *AdventureMenu::xChatResolveScript = NULL;

//--Script that fires when the rest executes. Often used to trigger dialogues.
char *AdventureMenu::xRestResolveScript = NULL;

//--Script that fires when the menu initializes. Allows the player to relive certain cutscenes.
char *AdventureMenu::xReliveResolveScript = NULL;

//--Script that fires when the costumes menu initializes, building a list of available costumes.
char *AdventureMenu::xCostumeResolveScript = NULL;

//--Script that fires when figuring out which field skills are available.
char *AdventureMenu::xFieldAbilityResolveScript = NULL;

//--Script that fires when the menu initializes. Allows the player to warp.
char *AdventureMenu::xWarpResolveScript = NULL;

//--Script that fires when the player elects to warp.
char *AdventureMenu::xWarpExecuteScript = NULL;

//--Tracks the total number of Catalysts the player can have collected so far.
int AdventureMenu::xCatalystTotal = 0;

//--Stores the last means of sorting files so the same type is used when opening a new menu instance.
//  This gets saved to the configuration file whenever the player changes the sorting type and closes
//  the menu.
int AdventureMenu::xLastSavefileSortingType = 0;

//--When using passwords, indicates that the last called file handled the password somehow.
bool AdventureMenu::xHandledPassword = false;

//--[Text Display Remaps]
//--These are two paralell arrays which allow remapping between images and letters when in a text box
//  display, such as inventory or equipment descriptions.
//--The typical format is [IMG|Name] which remaps to a DataLibrary path.
//--The names are stored in xTextImgRemap, and the DL paths are in xTextImgPath.
int AdventureMenu::xTextImgRemapsTotal = 0;
char **AdventureMenu::xTextImgRemap = NULL;
char **AdventureMenu::xTextImgPath = NULL;

//====================================== Property Queries =========================================
bool AdventureMenu::StartedHidingThisTick()
{
    //--Used to prevent the menu from closing and opening in the same tick since the control is the
    //  same for both.
    return mStartedHidingThisTick;
}
bool AdventureMenu::IsVisible()
{
    return mIsVisible;
}
bool AdventureMenu::NeedsToBlockControls()
{
    //--The menu blocks controls if it is the active object. It can still render if it is not the active
    //  object but the players controls are unlocked.
    if(mIsVisible) return true;

    //--If currently in Field Ability mode, block.
    if(mFieldAbilityMenu.mIsFieldAbilityMode) return true;

    //--All checks failed, controls return to the caller.
    return false;
}
bool AdventureMenu::NeedsToRender()
{
    //--If the menu is hiding itself, it may be rendering even if it is not the active object.
    if(mIsVisible) return true;
    return true;

    //--Hiding.
    if(mMainOpacityTimer > 0) return true;
    if(mCampfireGridOpacityTimer > 0) return true;

    //--All checks failed, don't render.
    return false;
}

//========================================= Manipulators ==========================================
void AdventureMenu::Show()
{
    mIsVisible = true;
    mIsShopMode = false;
    mIsCampfireMode = false;
    SetToMainMenu();
}
void AdventureMenu::Hide()
{
    //--Set flags.
    mIsVisible = false;
    mStartedHidingThisTick = true;

    //--Clean up outstanding variables.
    DeactivateFormsMenu();
}
void AdventureMenu::SetSaveMode(bool pFlag)
{
    //--Flags.
    mIsCampfireMode = pFlag;
    if(mIsCampfireMode) mCurrentCursor = AM_SAVE_REST;

    //--Clear these.
    mIsWarpMode = false;
    mIsReliveMode = false;
    mIsFormsMode = false;
    mIsSkillbookMode = false;

    //--If the flag is true, build the chat listing/warp listing/relive listing. These are rebuilt each time!
    if(mIsCampfireMode)
    {
        //--Reset variables.
        InitializeCampfireVariables();

        //--Chat listing:
        if(xChatResolveScript)
        {
            if(mChatListing) mChatListing->ClearList();
            LuaManager::Fetch()->ExecuteLuaFile(xChatResolveScript);
        }

        //--Warp listing:
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(xWarpResolveScript && rActiveLevel)
        {
            //--Clear.
            mWarpList->ClearList();

            //--We need the name of the current level to be passed in as well.
            LuaManager::Fetch()->ExecuteLuaFile(xWarpResolveScript, 1, "S", rActiveLevel->GetName());
        }

        //--Relive listing:
        if(xReliveResolveScript)
        {
            mReliveList->ClearList();
            LuaManager::Fetch()->ExecuteLuaFile(xReliveResolveScript);
        }
    }
}
void AdventureMenu::SetBenchMode(bool pFlag)
{
    mIsBenchMode = pFlag;
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void AdventureMenu::Update()
{
    //--Reset this flag.
    mStartedHidingThisTick = false;
    mHandledUpdate = false;

    //--Shop mode.
    if(mIsShopMode)
    {
        UpdateShopMenu();
        return;
    }
    else if(mIsGemcutterMode)
    {
        UpdateGemcutterMode();
    }

    //--If in CampfireMode or one of its subtypes, that handles the updates.
    if(mIsCampfireMode)
    {
        UpdateCampfireMenu();
        return;
    }

    //--If in Field Ability mode, that handles the update.
    UpdateFieldAbilities();
    if(mFieldAbilityMenu.mIsFieldAbilityMode)
    {
        return;
    }

    //--Always run these updates. These have internal timers and will ignore controls if they are
    //  not the main object.
    int tOldMode = mCurrentMode;
    mHandledUpdate |= UpdateMainMenu(mHandledUpdate);
    mHandledUpdate |= UpdateSkillsMode(mHandledUpdate);
    mHandledUpdate |= UpdateEquipMenu(mHandledUpdate);
    mHandledUpdate |= UpdateStatusMenu(mHandledUpdate);
    mHandledUpdate |= UpdateOptionsMode(mHandledUpdate);
    mHandledUpdate |= UpdateItemsMenu(mHandledUpdate);
    mHandledUpdate |= UpdateHealMode(mHandledUpdate);
    mHandledUpdate |= UpdateQuitMenu(mHandledUpdate);

    //--If this object is visible, opacity increases.
    if(mIsVisible)
    {
        if(mMainOpacityTimer < AM_OPACITY_TICKS) mMainOpacityTimer ++;
    }
    //--Object is not visible. Stop update, decrease opacity timer. For safety reasons also decrement
    //  the campfire version.
    else
    {
        if(mCampfireGridOpacityTimer > 0) mCampfireGridOpacityTimer --;
        if(mMainOpacityTimer > 0) mMainOpacityTimer --;
        return;
    }

    //--If the mode changed between the mandatory updates, block the update here.
    if(tOldMode != mCurrentMode) return;

    //--Route the update to the submode.
    if(mCurrentMode == AM_MODE_MAP)
    {
        UpdateMapMode();
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void AdventureMenu::Render()
{
    //--Routing render.
    if(mIsCampfireMode)
    {
        RenderCampfireMenu();
        return;
    }

    //--Render these component in all cases. They auto-handle fading.
    RenderMainMenu();
    RenderSkillsMode();
    RenderFieldAbilities();
    RenderEquipMenu();
    RenderStatusMenu();
    RenderOptionsMode();
    RenderItemsMenu();
    RenderHealMode();

    //--Past this point, only render if explicitly visible.
    if(!mIsVisible) return;

    if(mIsShopMode)
    {
        RenderShopMenu();
    }
    else if(mCurrentMode == AM_MODE_BASE || mCurrentMode == AM_MODE_QUIT)
    {
    }
    else if(mCurrentMode == AM_MODE_ITEMS)
    {
    }
    else if(mCurrentMode == AM_MODE_EQUIP)
    {
    }
    else if(mCurrentMode == AM_MODE_STATUS)
    {
    }
    else if(mCurrentMode == AM_MODE_MAP)
    {
        RenderMapMode();
    }
    else if(mCurrentMode == AM_MODE_OPTIONS)
    {
    }
    else if(mCurrentMode == AM_MODE_HEAL)
    {
    }
}
void AdventureMenu::RenderCursor(float pLft, float pYCenter)
{
    //--Renders a cursor at the given location. Used by many of the submenus.
    float cTextHeight = 14.0f;
    float cCursorWid = 7.0f;
    float cCursorHei = cTextHeight * 0.90f;
    float cCursorHeiHf = cCursorHei * 0.50f;
    pYCenter = pYCenter + 2.0f;

    //--Render.
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_TRIANGLES);

        //--Black Underlay.
        glColor3f(0.0f, 0.0f, 0.0f);
        glVertex2f(pLft - 1.0f,              pYCenter - cCursorHeiHf - 1.0f);
        glVertex2f(pLft + cCursorWid + 1.0f, pYCenter                + 0.0f);
        glVertex2f(pLft - 1.0f,              pYCenter + cCursorHeiHf + 1.0f);

        //--White overlay.
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex2f(pLft,              pYCenter - cCursorHeiHf);
        glVertex2f(pLft + cCursorWid, pYCenter);
        glVertex2f(pLft,              pYCenter + cCursorHeiHf);
    glEnd();
    glEnable(GL_TEXTURE_2D);
}
void AdventureMenu::RenderBox(float pLft, float pTop, float pWid, float pHei)
{
    //--Renders a box, black borders with grey backing. Used here temporarily until we have a proper UI.
    //  For speed, assumes that textures have already been turned off.
    float pRgt = pLft + pWid;
    float pBot = pTop + pHei;
    glBegin(GL_QUADS);
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex2f(pLft     , pTop);
        glVertex2f(pLft+3.0f, pTop);
        glVertex2f(pLft+3.0f, pBot);
        glVertex2f(pLft     , pBot);

        glVertex2f(pRgt     , pTop);
        glVertex2f(pRgt-3.0f, pTop);
        glVertex2f(pRgt-3.0f, pBot);
        glVertex2f(pRgt     , pBot);

        glVertex2f(pLft, pTop);
        glVertex2f(pRgt, pTop);
        glVertex2f(pRgt, pTop+3.0f);
        glVertex2f(pLft, pTop+3.0f);

        glVertex2f(pLft, pBot);
        glVertex2f(pRgt, pBot);
        glVertex2f(pRgt, pBot-3.0f);
        glVertex2f(pLft, pBot-3.0f);

        glColor3f(0.1f, 0.1f, 0.1f);
        glVertex2f(pLft+3.0f, pTop+3.0f);
        glVertex2f(pRgt-3.0f, pTop+3.0f);
        glVertex2f(pRgt-3.0f, pBot-3.0f);
        glVertex2f(pLft+3.0f, pBot-3.0f);
    glEnd();
    glColor3f(1.0f, 1.0f, 1.0f);
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
AdventureMenu *AdventureMenu::Fetch()
{
    //--Menu is held by an AdventureLevel. Get that.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return NULL;

    //--Return the menu held by the level.
    return rActiveLevel->GetMenu();
}

//========================================= Lua Hooking ===========================================
void AdventureMenu::HookToLuaState(lua_State *pLuaState)
{
    /* AM_GetProperty("Is Skillbook Menu") (Static) (1 boolean)
       Returns the requested property in the AdventureMenu. */
    lua_register(pLuaState, "AM_GetProperty", &Hook_AM_GetProperty);

    /* AM_SetProperty("Party Resolve Script", sPath) (Static)
       AM_SetProperty("Rest Resolve Script", sPath) (Static)
       AM_SetProperty("Warp Resolve Script", sPath) (Static)
       AM_SetProperty("Warp Execute Script", sPath) (Static)
       AM_SetProperty("Relive Resolve Script", sPath) (Static)
       AM_SetProperty("Costume Resolve Script", sPath) (Static)
       AM_SetProperty("Field Ability Resolve Script", sPath) (Static)
       AM_SetProperty("Total Catalysts", iNumber) (Static)
       AM_SetProperty("Text Image Remaps Total", iNumber) (Static)
       AM_SetProperty("Text Image Remaps", iSlot, sBaseName, sDLPath) (Static)
       AM_SetProperty("Flag Handled Password") (Static)
       AM_SetProperty("Chat Member", sName, sPath, sImagePath)
       AM_SetProperty("Register Warp Region Image", sRegionName, sImagePath)
       AM_SetProperty("Clear Region Images")
       AM_SetProperty("Register Warp Destination", sRegionName, sDisplayName, sMapName, sImgPath, fImgX, fImgY)
       AM_SetProperty("Register Relive Script", sDisplayName, sScriptPath, sImagePath)
       AM_SetProperty("Register Skillbook Script", sDisplayName, sScriptPath)
       AM_SetProperty("Open To Skillbook Menu", sCharacterName, sSkillbookScript)
       AM_SetProperty("Advanced Map", sLayerPath)
       AM_SetProperty("Advanced Map", sLayerPath, iLayerChanceLo, iLayerChanceHi)
       AM_SetProperty("Advanced Map Properties", fRemapX, fRemapY, fScaleX, fScaleY)
       AM_SetProperty("Execute Rest")
       AM_SetProperty("Register Costume Heading", sInternalName, sCharacterName, sFormName, sImageDLPath)
       AM_SetProperty("Register Costume Entry", sHeadingName, sCostumeName, sImageDLPath, sScriptPath)
       AM_SetProperty("Register Field Ability", sDLPath)
       Sets the requested property in the AdventureMenu. */
    lua_register(pLuaState, "AM_SetProperty", &Hook_AM_SetProperty);

    /* AM_SetMapInfo(sMapPath, sMapPathOverlay, iPlayerX, iPlayerY)
       Sets the current map image and where the player is on the map. Pass "Null" to clear the map. */
    lua_register(pLuaState, "AM_SetMapInfo", &Hook_AM_SetMapInfo);

    /* AM_SetFormProperty("Form Resolve Script", sScriptPath) (Static)
       AM_SetFormProperty("Form Party Resolve Script", sScriptPath) (Static)
       AM_SetFormProperty("Add Party Member", sMemberName, sImgPath)
       AM_SetFormProperty("Add Form", sFormName, sFormPath, sJobScriptPath, sImgPath)
       Sets the requested property in the AdventureMenu. Note that the menu is purged when the level
       reloads, so properties are always temporary unless static. */
    lua_register(pLuaState, "AM_SetFormProperty", &Hook_AM_SetFormProperty);

    //--[Sub Handlers]
    HookToLuaStateShop(pLuaState);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AM_GetProperty(lua_State *L)
{
    //AM_GetProperty("Is Skillbook Menu") (Static) (1 boolean)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AM_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Static: Returns true if this query was fired from the skillbook menu.
    if(!strcasecmp(rSwitchType, "Is Skillbook Menu") && tArgs == 1)
    {
        lua_pushboolean(L, AdventureMenu::xIsSkillbookMenu);
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("AM_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_AM_SetProperty(lua_State *L)
{
    //AM_SetProperty("Party Resolve Script", sPath) (Static)
    //AM_SetProperty("Rest Resolve Script", sPath) (Static)
    //AM_SetProperty("Warp Resolve Script", sPath) (Static)
    //AM_SetProperty("Warp Execute Script", sPath) (Static)
    //AM_SetProperty("Relive Resolve Script", sPath) (Static)
    //AM_SetProperty("Costume Resolve Script", sPath) (Static)
    //AM_SetProperty("Field Ability Resolve Script", sPath) (Static)
    //AM_SetProperty("Clear Skillbook Flag") (Static)
    //AM_SetProperty("Total Catalysts", iNumber) (Static)
    //AM_SetProperty("Text Image Remaps Total", iNumber) (Static)
    //AM_SetProperty("Text Image Remaps", iSlot, sBaseName, sDLPath) (Static)
    //AM_SetProperty("Flag Handled Password")
    //AM_SetProperty("Chat Member", sName, sPath, sImagePath)
    //AM_SetProperty("Register Warp Region Image", sRegionName, sImagePath)
    //AM_SetProperty("Clear Region Images")
    //AM_SetProperty("Register Warp Destination", sRegionName, sDisplayName, sMapName, sImgPath, fImgX, fImgY)
    //AM_SetProperty("Register Relive Script", sDisplayName, sScriptPath, sImagePath)
    //AM_SetProperty("Register Skillbook Script", sDisplayName, sScriptPath)
    //AM_SetProperty("Open To Skillbook Menu", sCharacterName, sSkillbookScript)
    //AM_SetProperty("Advanced Map", sLayerPath)
    //AM_SetProperty("Advanced Map", sLayerPath, iLayerChanceLo, iLayerChanceHi)
    //AM_SetProperty("Advanced Map Properties", fRemapX, fRemapY, fScaleX, fScaleY)
    //AM_SetProperty("Execute Rest")
    //AM_SetProperty("Register Costume Heading", sInternalName, sCharacterName, sFormName, sImageDLPath)
    //AM_SetProperty("Register Costume Entry", sHeadingName, sCostumeName, sImageDLPath, sScriptPath)
    //AM_SetProperty("Register Field Ability", sDLPath)

    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AM_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Statics.
    if(!strcasecmp(rSwitchType, "Party Resolve Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xChatResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired when the player rests.
    else if(!strcasecmp(rSwitchType, "Rest Resolve Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xRestResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to determine where the player can warp to.
    else if(!strcasecmp(rSwitchType, "Warp Resolve Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xWarpResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to warp the player somewhere.
    else if(!strcasecmp(rSwitchType, "Warp Execute Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xWarpExecuteScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to determine reliving of scenarios.
    else if(!strcasecmp(rSwitchType, "Relive Resolve Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xReliveResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to determine which costumes are available.
    else if(!strcasecmp(rSwitchType, "Costume Resolve Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xCostumeResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Script fired to resolve which field abilities are available.
    else if(!strcasecmp(rSwitchType, "Field Ability Resolve Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xFieldAbilityResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Clears the skillbook flag. This prevents oddities when examining skillbooks.
    else if(!strcasecmp(rSwitchType, "Clear Skillbook Flag") && tArgs == 1)
    {
        AdventureMenu::xIsSkillbookMenu = false;
        return 0;
    }
    //--Total number of Catalysts available in this chapter.
    else if(!strcasecmp(rSwitchType, "Total Catalysts") && tArgs == 2)
    {
        AdventureMenu::xCatalystTotal = lua_tointeger(L, 2);
        return 0;
    }
    //--How many text-image remaps there are. Allocates space.
    else if(!strcasecmp(rSwitchType, "Text Image Remaps Total") && tArgs == 2)
    {
        //--Get how many are expected.
        int tExpected = lua_tointeger(L, 2);

        //--Deallocate existing amount.
        for(int i = 0; i < AdventureMenu::xTextImgRemapsTotal; i ++)
        {
            free(AdventureMenu::xTextImgPath[i]);
            free(AdventureMenu::xTextImgRemap[i]);
        }
        AdventureMenu::xTextImgRemapsTotal = 0;
        free(AdventureMenu::xTextImgPath);
        free(AdventureMenu::xTextImgRemap);

        //--Allocate space and clear.
        AdventureMenu::xTextImgRemapsTotal = tExpected;
        SetMemoryData(__FILE__, __LINE__);
        AdventureMenu::xTextImgPath  = (char **)starmemoryalloc(sizeof(char *) * tExpected);
        SetMemoryData(__FILE__, __LINE__);
        AdventureMenu::xTextImgRemap = (char **)starmemoryalloc(sizeof(char *) * tExpected);
        for(int i = 0; i < AdventureMenu::xTextImgRemapsTotal; i ++)
        {
            AdventureMenu::xTextImgPath[i]  = NULL;
            AdventureMenu::xTextImgRemap[i] = NULL;
        }
        //fprintf(stderr, "Set image remaps total to %i\n", tExpected);
        return 0;
    }
    //--Sets a text remap.
    else if(!strcasecmp(rSwitchType, "Text Image Remaps") && tArgs == 4)
    {
        //--Check the slot.
        int tSlot = lua_tointeger(L, 2);
        if(tSlot < 0 || tSlot >= AdventureMenu::xTextImgRemapsTotal) return 0;

        //--Set.
        ResetString(AdventureMenu::xTextImgRemap[tSlot], lua_tostring(L, 3));
        ResetString(AdventureMenu::xTextImgPath[tSlot],  lua_tostring(L, 4));
        //fprintf(stderr, "Remapped %i to %s - %s\n", tSlot, AdventureMenu::xTextImgRemap[tSlot], AdventureMenu::xTextImgPath[tSlot]);
        //void *rCheckPtr = DataLibrary::Fetch()->GetEntry(AdventureMenu::xTextImgPath[tSlot]);
        //fprintf(stderr, " Check ptr %p\n", rCheckPtr);
        return 0;
    }
    //--Indicates a subscript handled a password.
    else if(!strcasecmp(rSwitchType, "Flag Handled Password") && tArgs == 1)
    {
        AdventureMenu::xHandledPassword = true;
        return 0;
    }

    //--Dynamic. Get the object in question.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("AM_SetProperty");

    //--Adds a character to the chat listing.
    if(!strcasecmp(rSwitchType, "Chat Member") && tArgs == 4)
    {
        rMenu->AddChatEntity(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Adds an image remap for a given region name.
    else if(!strcasecmp(rSwitchType, "Register Warp Region Image") && tArgs == 3)
    {
        rMenu->RegisterWarpRegionImage(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Clear region remaps.
    else if(!strcasecmp(rSwitchType, "Clear Region Images") && tArgs == 1)
    {
        rMenu->ClearWarpRegionImages();
    }
    //--Adds a warp destination.
    else if(!strcasecmp(rSwitchType, "Register Warp Destination") && tArgs == 8)
    {
        rMenu->RegisterWarpDestination(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5), lua_tostring(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
    }
    //--Adds a script we can relive.
    else if(!strcasecmp(rSwitchType, "Register Relive Script") && tArgs == 4)
    {
        rMenu->RegisterReliveScript(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4));
    }
    //--Registers a path for skillbook examination.
    else if(!strcasecmp(rSwitchType, "Register Skillbook Script") && tArgs == 3)
    {
        rMenu->RegisterSkillbookEntry(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Opens the menu to the given character/skillbook script. This is used after reading a skillbook at a campfire.
    else if(!strcasecmp(rSwitchType, "Open To Skillbook Menu") && tArgs == 3)
    {
        rMenu->Show();
        rMenu->SetSaveMode(true);
        rMenu->OpenSkillbooksTo(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Adds a layer to advanced map mode.
    else if(!strcasecmp(rSwitchType, "Advanced Map") && tArgs == 2)
    {
        rMenu->AddAdvancedLayer(lua_tostring(L, 2), 0, 1000);
    }
    //--Adds a layer to advanced map mode with definite render chances.
    else if(!strcasecmp(rSwitchType, "Advanced Map") && tArgs == 4)
    {
        rMenu->AddAdvancedLayer(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Remapping properties for positioning the player cursor on the map.
    else if(!strcasecmp(rSwitchType, "Advanced Map Properties") && tArgs == 5)
    {
        rMenu->SetAdvancedRemaps(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--Executes a rest action, doesn't handle the fading in and out part.
    else if(!strcasecmp(rSwitchType, "Execute Rest") && tArgs == 1)
    {
        rMenu->ExecuteRest();
    }
    //--When building the costume menu, registers a new "Heading", which is a playable form (like Christine Golem or Mei Alraune)
    else if(!strcasecmp(rSwitchType, "Register Costume Heading") && tArgs == 5)
    {
        rMenu->AddCostumeHeading(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5));
    }
    //--When building the costume menu, registers a costume entry to an existing heading. These are costumes like Christine's Gala dress.
    else if(!strcasecmp(rSwitchType, "Register Costume Entry") && tArgs == 5)
    {
        rMenu->AddCostumeEntry(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5));
    }
    ///--[Field Abilities]
    //--Registers a field ability when rebuilding the ability list.
    else if(!strcasecmp(rSwitchType, "Register Field Ability") && tArgs == 2)
    {
        rMenu->RegisterFieldAbility(lua_tostring(L, 2));
    }
    ///--[Error]
    //--Error.
    else
    {
        LuaPropertyError("AM_SetProperty", rSwitchType, tArgs);
    }
    return 0;
}
int Hook_AM_SetMapInfo(lua_State *L)
{
    //AM_SetMapInfo(sMapPath, sMapPathOverlay, iPlayerX, iPlayerY)
    int tArgs = lua_gettop(L);
    if(tArgs < 3) return LuaArgError("AM_SetMapInfo");

    //--Set.
    AdventureMenu::Fetch()->SetMapInfo(lua_tostring(L, 1), lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    return 0;
}
int Hook_AM_SetFormProperty(lua_State *L)
{
    //AM_SetFormProperty("Form Resolve Script", sScriptPath) (Static)
    //AM_SetFormProperty("Form Party Resolve Script", sScriptPath) (Static)
    //AM_SetFormProperty("Add Party Member", sMemberName, sImgPath)
    //AM_SetFormProperty("Add Form", sFormName, sFormPath, sJobScriptPath, sImgPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AM_SetFormProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Static]
    //--Which script to use when resolving forms for each party member.
    if(!strcasecmp(rSwitchType, "Form Resolve Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xFormResolveScript, lua_tostring(L, 2));
        return 0;
    }
    //--Which script to use to figure out which party members are in the party and can transform.
    if(!strcasecmp(rSwitchType, "Form Party Resolve Script") && tArgs == 2)
    {
        ResetString(AdventureMenu::xFormPartyResolveScript, lua_tostring(L, 2));
        return 0;
    }

    //--[Dynamic]
    //--Menu is required to exist.
    AdventureMenu *rMenu = AdventureMenu::Fetch();
    if(!rMenu) return LuaTypeError("AM_SetFormProperty");

    //--Adds a party member to the listing.
    if(!strcasecmp(rSwitchType, "Add Party Member") && tArgs == 3)
    {
        rMenu->RegisterFormPartyMember(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Adds a form to the listing.
    else if(!strcasecmp(rSwitchType, "Add Form") && tArgs == 5)
    {
        rMenu->RegisterForm(lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4), lua_tostring(L, 5));
    }
    //--Error case.
    else
    {
        LuaPropertyError("AM_SetFormProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
