//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdvHelp.h"
#include "AdventureLevel.h"
#include "AdventureInventory.h"
#include "TilemapActor.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"

//--[System]
void AdventureMenu::SetToHealMode()
{
    //--Flags.
    mCurrentMode = AM_MODE_HEAL;
    mCurrentCursor = 0;
}

//--[Manipulators]
void AdventureMenu::RefreshHealMenuHelp()
{
    //--Resolve the images needed for the help menu.
    ControlManager *rControlManager = ControlManager::Fetch();
    mMainMenuHelp->Construct();

    //--Allocate strings.
    int cStrings = 5;
    mMainMenuHelp->AllocateStrings(cStrings);
    StarlightString **rStrings = mMainMenuHelp->GetStrings();

    //--Set.
    rStrings[0]->SetString("[IMG0] Use doctor bag on selected character");
    rStrings[0]->AllocateImages(1);
    rStrings[0]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Activate"));

    rStrings[1]->SetString("[IMG0] Previous menu");
    rStrings[1]->AllocateImages(1);
    rStrings[1]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Cancel"));

    rStrings[2]->SetString(" ");

    rStrings[3]->SetString("[IMG0][IMG1][IMG2][IMG3] Change Selection");
    rStrings[3]->AllocateImages(4);
    rStrings[3]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Up"));
    rStrings[3]->SetImageP(1, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Right"));
    rStrings[3]->SetImageP(2, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Down"));
    rStrings[3]->SetImageP(3, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Left"));

    rStrings[4]->SetString("[IMG0] Close This Display");
    rStrings[4]->AllocateImages(1);
    rStrings[4]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("F1"));

    //--All strings cross-reference here.
    for(int i = 0; i < cStrings; i ++) rStrings[i]->CrossreferenceImages();
}

//--[Core Methods]
//--[Update]
bool AdventureMenu::UpdateHealMode(bool pCannotHandleUpdate)
{
    ///--[Documentation and Setup]
    //--Handles controls for the warp menu. Handles up/down/activate/cancel.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mCurrentMode == AM_MODE_HEAL)
    {
        if(mHealVisTimer < ADVMENU_SKILLS_VIS_TICKS) mHealVisTimer ++;
    }
    //--Decrement.
    else
    {
        if(mHealVisTimer > 0) mHealVisTimer --;
    }

    ///--[Error Checking]
    //--If this menu is not the main object, stop the update here.
    if(mCurrentMode != AM_MODE_HEAL || pCannotHandleUpdate) return false;

    ///--[Help Handler]
    if(mIsMainHelpVisible)
    {
        if(rControlManager->IsFirstPress("F1") || rControlManager->IsFirstPress("Cancel"))
        {
            mIsMainHelpVisible = false;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        return true;
    }

    //--Help activation.
    if(rControlManager->IsFirstPress("F1"))
    {
        mIsMainHelpVisible = true;
        RefreshHealMenuHelp();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[Directional Controls]
    //--Up!
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Decrement.
        if(mCurrentCursor > 0)
        {
            mCurrentCursor = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Down!
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        if(mCurrentCursor == 0)
        {
            mCurrentCursor = 1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Left.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Does nothing if the cursor is at 0.
        if(mCurrentCursor <= 1) return true;

        //--Decrement.
        mCurrentCursor --;
        if(mCurrentCursor < 1) mCurrentCursor = 1;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Right.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Does nothing if the cursor is at max.
        if(mCurrentCursor >= rAdventureCombat->GetActivePartyCount()) return true;

        //--Increment.
        mCurrentCursor ++;
        if(mCurrentCursor >= rAdventureCombat->GetActivePartyCount()+1) mCurrentCursor --;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Heal.
    if(rControlManager->IsFirstPress("Activate") && xWarpExecuteScript)
    {
        //--Send the instruction.
        rAdventureCombat->HealFromDoctorBag(mCurrentCursor - 1);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Combat|DoctorBag");
    }
    //--Back to the main menu.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mCurrentMode = AM_MODE_BASE;
        mCurrentCursor = AM_MAIN_HEAL;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Handled update.
    return true;
}

//--[Drawing]
void RenderTextToFit(SugarFont *pFont, float pX, float pY, float pMaxWid, const char *pText, ...);
void AdventureMenu::RenderHealMode()
{
    ///--[Documentation and Setup]
    //--Rendering. Renders the list of warp destinations and the cursor.
    if(!Images.mIsReady) return;
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mHealVisTimer, ADVMENU_SKILLS_VIS_TICKS);
    if(cAlpha <= 0.0f) return;
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Main Menu]
    int tOldCursor = mCurrentCursor;
    mCurrentCursor = AM_MAIN_HEAL;
    RenderMainMenu();
    mCurrentCursor = tOldCursor;

    //--Darken the main menu.
    SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.80f * cAlpha));
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--[Static Parts]
    //Images.DoctorUI.rOptionsDeactivated->Draw();
    Images.DoctorUI.rMenuBack->Draw();
    Images.DoctorUI.rMenuHeaderA->Draw();
    Images.DoctorUI.rMenuHeaderB->Draw();
    Images.DoctorUI.rHeadingFont->DrawText(685.0f, 146.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Heal");

    //--Button text.
    //RenderMainMenuButton(Images.BaseMenu.rBtnBase, Images.BaseMenu.rHeadingFont, 2, true,  false, "Doctor Bag");

    //--[Current Doctor Bag Charges]
    //--Bar fill constants.
    int tChargesCur = AdventureInventory::Fetch()->GetDoctorBagCharges();
    int tChargesMax = AdventureInventory::Fetch()->GetDoctorBagChargesMax();
    float tPercent = (float)tChargesCur / (float)tChargesMax;

    //--Render the bar, the frames over it.
    Images.DoctorUI.rBarHeader->Draw();
    SugarBitmap::DrawHorizontalPercent(Images.DoctorUI.rBarFill, tPercent);
    Images.DoctorUI.rBarFrame->Draw();

    //--Render the text values.
    Images.DoctorUI.rMainlineFont->DrawTextArgs(685.0f, 373.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Doctor Bag: %i / %i", tChargesCur, tChargesMax);

    //--[Buttons]
    //--Heal All.
    if(mCurrentCursor != 0) StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, cAlpha);
    Images.DoctorUI.rHealAllBtn->Draw();
    Images.DoctorUI.rHeadingFont->DrawText(685.0f, 203.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Heal All");
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Constants
    float cXOffset   =   0.0f;
    float cYOffset   =   0.0f;
    float cIncrement = 116.0f;

    //--For each character:
    for(int i = 0; i < rAdventureCombat->GetActivePartyCount(); i ++)
    {
        //--Check character.
        AdvCombatEntity *rSlotEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rSlotEntity) return;

        //--Darken.
        if(mCurrentCursor != i+1 && mCurrentCursor != 0) StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, cAlpha);

        //--Portrait backing.
        Images.DoctorUI.rPortraitBtn->Draw(cXOffset, cYOffset);

        //--Turn on stencils to render the mask.
        glEnable(GL_STENCIL_TEST);
        glColorMask(false, false, false, false);
        glDepthMask(false);
        glStencilMask(0xFF);
        glStencilFunc(GL_ALWAYS, i+1+5, 0xFF);
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        Images.DoctorUI.rPortraitMask->Draw(cXOffset, cYOffset);

        //--Turn on stencil-controlled rendering and render the combat portrait. It renders
        //  at 50% size.
        SugarBitmap *rFieldPortrait = rSlotEntity->GetCombatPortrait();
        if(rFieldPortrait)
        {
            //--Positioning.
            TwoDimensionRealPoint tPoint = rSlotEntity->GetUIRenderPosition(ACE_UI_INDEX_DOCTOR_MAIN);
            glTranslatef(tPoint.mXCenter + cXOffset, tPoint.mYCenter + cYOffset, 0.0f);
            glScalef(0.5f, 0.5f, 1.0f);

            //--If there's a countermask, render it.
            SugarBitmap *rCounterMask = rSlotEntity->GetCombatCounterMask();
            if(rCounterMask)
            {
                glStencilFunc(GL_ALWAYS, 0, 0xFF);
                rCounterMask->Draw();
            }

            //--Render main portrait.
            glColorMask(true, true, true, true);
            glDepthMask(true);
            glStencilMask(0xFF);
            glStencilFunc(GL_EQUAL, i+1+5, 0xFF);
            glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

            //--Variables.
            bool tIsSmallPortraitMode = OptionsManager::Fetch()->GetOptionB("LowResAdventureMode");

            //--Normal rendering.
            if(!tIsSmallPortraitMode)
            {
                rFieldPortrait->Draw();
            }
            //--Small portrait.
            else
            {
                rFieldPortrait->DrawScaled(0.0f, 0.0f, LOWRES_SCALEINV, LOWRES_SCALEINV);
            }

            //--Clean.
            glScalef(2.0f, 2.0f, 1.0f);
            glTranslatef((tPoint.mXCenter + cXOffset) * -1.0f, (tPoint.mYCenter + cYOffset) * -1.0f, 0.0f);
        }

        //--Turn stencils off.
        glDisable(GL_STENCIL_TEST);

        //--Render the HP value for this character.
        int tHealthCur = rSlotEntity->GetHealth();
        int tHealthMax = rSlotEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_HPMAX);
        RenderTextToFit(Images.DoctorUI.rMainlineFont, 515.0f + cXOffset, 332.0f + cYOffset, 77.0f, "%i / %i", tHealthCur, tHealthMax);

        //--Next.
        cXOffset = cXOffset + cIncrement;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
    }

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Help Menu]
    //--Help string.
    mShowHelpString->DrawText(0.0f, 0.0f, 0, 1.0f, Images.BaseMenu.rMainlineFont);

    //--Menu, if visible.
    if(mMainHelpTimer > 0)
    {
        //--Compute opacity.
        float cPercentage = EasingFunction::QuadraticInOut(mMainHelpTimer, ADV_HELP_STD_TICKS);
        SugarBitmap::DrawFullBlack(cPercentage * 0.75f);

        //--Offset.
        float tYOffset = ADV_HELP_STD_OFFSET * (1.0f - cPercentage);
        mMainMenuHelp->Render(tYOffset);
    }
}
