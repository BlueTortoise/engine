//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "GridHandler.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

//--[Forward Declarations]
void RenderLineBounded(float pLft, float pTop, float pWidth, SugarFont *pFont, const char *pString);
void RenderTextConditionalColor(SugarFont *pFont, float pX, float pY, int pFlags, float pSize, const char *pText, bool pConditional, StarlightColor pTrueColor, StarlightColor pFalseColor);

//--[System]
void AdventureMenu::ActivateChatMenu()
{
    //--[Documentation and Setup]
    //--Called from the Save Menu. Activates chat mode. When the menu comes up, the list is already built,
    //  and we convert it into a grid set.
    if(!mChatListing || mChatListing->GetListSize() < 1) return;

    //--Flags.
    mIsChatMode = true;

    //--Clean old data.
    mChatGrid->ClearList();

    //--[Positioning and Controls]
    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    int cXSize = 7;
    int cYSize = 1;
    int cXMiddle = 3;
    int cYMiddle = 1;
    int **tGridData = GridHandler::BuildGrid(cXSize, cYSize, cXMiddle, cYMiddle, mChatListing->GetListSize());

    //--Call the subroutine to build the control list for us.
    GridHandler::BuildGridList(mChatGrid, tGridData, cXSize, cYSize, mChatListing->GetListSize(), AM_GRID_CENT_X, AM_GRID_CENT_Y, AM_GRID_SPAC_X, AM_GRID_SPAC_Y);

    //--Clean up the grid data. This argument expects the original grid size.
    GridHandler::DeallocateGrid(tGridData, cXSize);
}
void AdventureMenu::DeactivateChatMenu()
{
    //--Deactivates this menu.
    mIsChatMode = false;
}

//--[Manipulators]
void AdventureMenu::AddChatEntity(const char *pName, const char *pPath, const char *pImgPath)
{
    //--Adds a chat entity. Also boots the list if it wasn't booted yet.
    if(!pName || !pPath) return;

    //--Check if the list needs booting.
    if(!mChatListing) mChatListing = new SugarLinkedList(true);
    if(!mChatGridImg) mChatGridImg = new SugarLinkedList(false);

    //--Add.
    mChatListing->AddElementAsTail(pName, InitializeString(pPath), &FreeThis);

    //--Parallel image entry.
    void *rCheckPtr = DataLibrary::Fetch()->GetEntry(pImgPath);
    if(!rCheckPtr) rCheckPtr = Images.CampfireMenu.rIconFrame;
    mChatGridImg->AddElementAsTail("X", rCheckPtr);
}

//--[Update]
void AdventureMenu::UpdateChatMenu()
{
    //--[Timers]
    //--If in chat mode, increment this timer. Chat listing must also have been created.
    if(mIsChatMode && mChatListing)
    {
        if(mChatGridOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mChatGridOpacityTimer ++;
    }
    //--Decrement and stop if not.
    else
    {
        if(!mChatListing) DeactivateChatMenu();
        if(mChatGridOpacityTimer > 0) mChatGridOpacityTimer --;
        return;
    }

    //--[Setup]
    //--Handles controls for the chat menu. Handles up/down/activate/cancel.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Error check:
    if(mChatGridCurrent < 0 || mChatGridCurrent >= mChatGrid->GetListSize()) mChatGridCurrent = 0;
    GridPackage *rPackage = (GridPackage *)mChatGrid->GetElementBySlot(mChatGridCurrent);
    if(!rPackage)
    {
        mStartedHidingThisTick = true;
        mIsChatMode = false;
        mCurrentCursor = AM_SAVE_CHAT;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    //--[Directions]
    //--Directional controls are handled programmatically. If the instruction is -1, it means there is nothing
    //  at that grid position.
    int tChatGridPrevious = mChatGridCurrent;
    if(rControlManager->IsFirstPress("Up"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_TOP];
        if(tTargetEntry != -1) mChatGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_BOT];
        if(tTargetEntry != -1) mChatGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_LFT];
        if(tTargetEntry != -1) mChatGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_RGT];
        if(tTargetEntry != -1) mChatGridCurrent = tTargetEntry;
    }

    //--If the grid changed positions, play a sound.
    if(tChatGridPrevious != mChatGridCurrent)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Size setting. All not-selected grid entries decrease their timers. The selected entry increases.
    GridPackage *rTimerPackage = (GridPackage *)mChatGrid->PushIterator();
    while(rTimerPackage)
    {
        //--Increment if selected.
        if(rTimerPackage == rPackage)
        {
            if(rTimerPackage->mTimer < AM_CAMPFIRE_SIZE_TICKS) rTimerPackage->mTimer ++;
        }
        //--Decrement if deselected.
        else
        {
            if(rTimerPackage->mTimer > 0) rTimerPackage->mTimer --;
        }
        rTimerPackage = (GridPackage *)mChatGrid->AutoIterate();
    }

    //--Activate, talk to the specified character.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Run the requested script.
        const char *rScriptPath = (const char *)mChatListing->GetElementBySlot(mChatGridCurrent);
        if(rScriptPath)
        {
            //--Deactivate this menu, and hide everything.
            DeactivateChatMenu();
            Hide();

            //--Run.
            LuaManager::Fetch()->ExecuteLuaFile(rScriptPath, 2, "S", "Hello", "S", "IsFireside");
        }
        //--Error.
        else
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Cancel, returns to the main menu.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mStartedHidingThisTick = true;
        DeactivateChatMenu();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureMenu::RenderChatMenu()
{
    //--[Setup]
    //--Don't render at all if the opacity is at zero.
    if(mChatGridOpacityTimer < 1) return;
    if(!Images.mIsReady || !mChatListing) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mChatGridOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--[Title]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    Images.CampfireMenu.rHeader->Draw(427.0f, 45.0f);
    Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 57.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Chat Menu");
    StarlightColor::ClearMixer();

    //--[Icons]
    //--Sizes.
    float cIconSize = Images.CampfireMenu.rIconFrameLarge->GetTrueWidth();

    //--Position the objects slide away from.
    float tOpacitySlidePct = EasingFunction::QuadraticOut(mChatGridOpacityTimer, AM_CAMPFIRE_OPACITY_TICKS);
    float tCenterX = AM_GRID_CENT_X;
    float tCenterY = AM_GRID_CENT_Y;

    //--Iterate across the list.
    GridPackage *rGridPack = (GridPackage *)mChatGrid->PushIterator();
    SugarBitmap *rImage    = (SugarBitmap *)mChatGridImg->PushIterator();
    while(rGridPack && rImage)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 1.0f + (cBoostPct * (AM_CAMPFIRE_SIZE_BOOST * 0.33f));
        float cScaleInv = 1.0f / cScale;

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        glTranslatef(tXPos, tYPos, 0.0f);
        glScalef(cScale, cScale, 1.0f);
        Images.CampfireMenu.rIconFrameLarge->Draw();
        rImage->Draw();
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-tXPos, -tYPos, 0.0f);

        //--Next.
        rGridPack = (GridPackage *)mChatGrid->AutoIterate();
        rImage    = (SugarBitmap *)mChatGridImg->AutoIterate();
    }

    //--Iterate across the list again, this time just rendering the names.
    rGridPack = (GridPackage *)mChatGrid->PushIterator();
    void *rListingPtr = mChatListing->PushIterator();
    while(rGridPack && rListingPtr)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 1.0f + (cBoostPct * (AM_CAMPFIRE_SIZE_BOOST * 0.33f));

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);
        float tTextX = tXPos;

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        float cTxtScale = 1.0f + (cBoostPct * (AM_CAMPFIRE_SIZE_BOOST * 0.33f));
        Images.CampfireMenu.rMainlineFont->DrawText(tTextX, tYPos - 15.0f, SUGARFONT_AUTOCENTER_X, cTxtScale, mChatListing->GetIteratorName());

        //--Next.
        rGridPack = (GridPackage *)mChatGrid->AutoIterate();
        rListingPtr = mChatListing->AutoIterate();
    }

        /*
    //--After the main grid loop, re-run the loop and render the text. This is to make it appear over everything else.
    rGridPack = (GridPackage *)mReliveGrid->PushIterator();
    rRelivePack = (RelivePackage *)mReliveList->PushIterator();
    while(rGridPack && rRelivePack)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);
        float tTextX = tXPos;

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Text.
        float cTxtScale = 1.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
        Images.CampfireMenu.rMainlineFont->DrawText(tTextX, tYPos - 15.0f, SUGARFONT_AUTOCENTER_X, cTxtScale, mReliveList->GetIteratorName());

        //--Next.
        rGridPack = (GridPackage *)mReliveGrid->AutoIterate();
        rRelivePack = (RelivePackage *)mReliveList->AutoIterate();
    }
    */


        /*
    //--[Party Listing]
    //--Constants.
    float cHeightPerButton = 52.0f;

    //--Variables.
    float tYPosition = cRenderY;

    //--Colors.
    StarlightColor cHighlight = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    StarlightColor cGreyOut   = StarlightColor::MapRGBAF(0.5f, 0.5f, 0.5f, 1.0f);

    //--Render the names of the chat options.
    for(int i = 0; i < mChatListing->GetListSize(); i ++)
    {
        const char *rName = mChatListing->GetNameOfElementBySlot(i);
        RenderTextConditionalColor(Images.CampfireMenu.rHeadingFont, 683.0f, tYPosition, SUGARFONT_AUTOCENTER_X, 1.0f, rName, mChatCursor == i, cHighlight, cGreyOut);
        tYPosition = tYPosition + cHeightPerButton;
    }

    //--Clean.
    StarlightColor::ClearMixer();
    glTranslatef(0.0f, 32.0f, 0.0f);*/

    //--[Clean]
    StarlightColor::ClearMixer();
}
