//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

//--[Forward Declarations]
void RenderLineBounded(float pLft, float pTop, float pWidth, SugarFont *pFont, const char *pString);
void RenderTextConditionalColor(SugarFont *pFont, float pX, float pY, int pFlags, float pSize, const char *pText, bool pConditional, StarlightColor pTrueColor, StarlightColor pFalseColor);

//--[System]
void AdventureMenu::ActivateFormsMenu()
{
    //--[Documentation and Setup]
    //--Called from the Save Menu. Wipes forms information. This won't do anything if the static
    //  forms path was never set!
    if(!xFormResolveScript || !xFormPartyResolveScript) return;

    //--System
    mIsFormsMode = true;
    mFormsPartyMember = -1;

    //--Character Selection.
    mFormsGridCurrent = 0;
    mFormsPartyList->ClearList();
    mFormsPartyGrid->ClearList();
    mFormsPartyGridImg->ClearList();

    //--Form Selection.
    rFormCharacter = NULL;
    mFormSubGridCurrent = 0;
    mFormSubGrid->ClearList();
    mFormSubPaths->ClearList();
    mFormSubJobScripts->ClearList();
    mFormSubGridImg->ClearList();
    mFormSubComparisons->ClearList();

    //--Now call the party resolver script.
    LuaManager::Fetch()->ExecuteLuaFile(xFormPartyResolveScript);

    //--[Error Check]
    //--No forms resolved.
    if(mFormsPartyList->GetListSize() < 1)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        mIsFormsMode = false;
    }

    //--[Positioning and Controls]
    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    int cXSize = 7;
    int cYSize = 1;
    int cXMiddle = 3;
    int cYMiddle = 1;
    int **tGridData = GridHandler::BuildGrid(cXSize, cYSize, cXMiddle, cYMiddle, mFormsPartyList->GetListSize());

    //--Call the subroutine to build the control list for us.
    GridHandler::BuildGridList(mFormsPartyGrid, tGridData, cXSize, cYSize, mFormsPartyList->GetListSize(), AM_GRID_CENT_X, AM_GRID_CENT_Y, AM_GRID_SPAC_X, AM_GRID_SPAC_Y);

    //--Clean up the grid data. This argument expects the original grid size.
    GridHandler::DeallocateGrid(tGridData, cXSize);
}
void AdventureMenu::ActivateFormsSubMenu(int pPartyIndex)
{
    //--When a party member is selected, builds a list of forms for that member. Can fail. The party index refers to
    //  the name of the character in the follower group, which may or may not be a party member in the combat listing.
    void *rCheckPtr = mFormsPartyList->GetElementBySlot(pPartyIndex);
    if(!rCheckPtr)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--Get the name associated with the slot.
    const char *rName = mFormsPartyList->GetNameOfElementBySlot(pPartyIndex);

    //--Clear lists off.
    rFormCharacter = NULL;
    mFormSubGrid->ClearList();
    mFormSubPaths->ClearList();
    mFormSubJobScripts->ClearList();
    mFormSubGridImg->ClearList();
    mFormSubComparisons->ClearList();

    //--Locate the character in question.
    rFormCharacter = AdvCombat::Fetch()->GetRosterMemberS(rName);
    if(!rFormCharacter)
    {
        fprintf(stderr, "Error, no character %s on roster.\n", rName);
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--Run the script. This will populate the forms that character can change to.
    LuaManager::Fetch()->ExecuteLuaFile(xFormResolveScript, 1, "S", rName);

    //--If the character cannot change to any forms, fail here.
    if(mFormSubPaths->GetListSize() < 1)
    {
        //fprintf(stderr, "Error, character %s has no forms.\n", rName);
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--If we got this far, at least one form is available to transform to. Set this as the active character.
    mFormsPartyMember = pPartyIndex;

    //--Construct a TF grid.
    int cXSize = 7;
    int cYSize = 3;
    int cXMiddle = 3;
    int cYMiddle = 1;
    int **tGridData = GridHandler::BuildGrid(cXSize, cYSize, cXMiddle, cYMiddle, mFormSubPaths->GetListSize());
    GridHandler::BuildGridList(mFormSubGrid, tGridData, cXSize, cYSize, mFormSubPaths->GetListSize(), AM_GRID_CENT_X, AM_GRID_CENT_Y, AM_GRID_SPAC_X, AM_GRID_SPAC_Y);
    GridHandler::DeallocateGrid(tGridData, cXSize);

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|Select");

    //--Character statistics.
    CombatStatistics *rCurJobStats    = rFormCharacter->GetStatisticsGroup(ADVCE_STATS_JOB);
    CombatStatistics *rCharacterStats = rFormCharacter->GetStatisticsGroup(ADVCE_STATS_FINAL);

    //--For each job script chunk, build a comparison package.
    char *rJobScriptPath = (char *)mFormSubJobScripts->PushIterator();
    while(rJobScriptPath)
    {
        //--Construct a comparison package. This is a set of statistics compared against
        //  the base set of statistics.
        CombatStatistics *nPackage = (CombatStatistics *)starmemoryalloc(sizeof(CombatStatistics));
        nPackage->Zero();

        //--Clone the combat statistics from the character, then subtract the current job bonus.
        memcpy(nPackage, rCharacterStats, sizeof(CombatStatistics));
        for(int i = 0; i < STATS_TOTAL; i ++)
        {
            nPackage->mValueList[i] = nPackage->mValueList[i] - rCurJobStats->mValueList[i];
        }

        //--Push the entity and run their job script level up. This will compute the job change bonus,
        //  and store it in the adventure combat class.
        LuaManager::Fetch()->ExecuteLuaFile(rJobScriptPath, 2, "N", (float)ADVCJOB_CODE_LEVEL, "N", (float)rFormCharacter->GetLevel());

        //--Apply the stats we got out to the stats package.
        CombatStatistics *rNewStats = AdvCombat::Fetch()->GetJobLevelUpStorage();
        for(int i = 0; i < STATS_TOTAL; i ++)
        {
            nPackage->mValueList[i] = nPackage->mValueList[i] + rNewStats->mValueList[i];
        }

        //--Register the pack.
        mFormSubComparisons->AddElementAsTail("X", nPackage, &FreeThis);

        //--Next.
        rJobScriptPath = (char *)mFormSubJobScripts->AutoIterate();
    }
}
void AdventureMenu::DeactivateFormsMenu()
{
    //--Clears the forms list and returns to the main menu. Note that activating a form does not trigger this.
    mIsFormsMode = false;
}

//--[Property Queries]
//--[Manipulators]
void AdventureMenu::RegisterFormPartyMember(const char *pCharacterName, const char *pImgPath)
{
    //--Adds a character to the listing. Some characters may be listed but unable to transform, others may not be listed
    //  because their transformation is based on someone else's.
    if(!pCharacterName || !pImgPath) return;

    //--Dummy value. Never actually used, don't worry about it going out of scope.
    int tDummyValue = 100;
    mFormsPartyList->AddElementAsTail(pCharacterName, &tDummyValue);
    mFormsPartyGridImg->AddElementAsTail("X", DataLibrary::Fetch()->GetEntry(pImgPath));
}
void AdventureMenu::RegisterForm(const char *pFormName, const char *pFormPath, const char *pJobScriptPath, const char *pImgPath)
{
    //--Adds a form to the listing. Called from the Lua.
    if(!pFormName || !pFormPath || !pJobScriptPath || !pImgPath) return;
    mFormSubPaths->AddElementAsTail(pFormName, InitializeString(pFormPath), &FreeThis);
    mFormSubJobScripts->AddElementAsTail(pFormName, InitializeString(pJobScriptPath), &FreeThis);
    mFormSubGridImg->AddElementAsTail("X", DataLibrary::Fetch()->GetEntry(pImgPath));
}

//--[Update]
void AdventureMenu::UpdateFormsMenu()
{
    //--[Timers]
    //--If in chat mode, increment this timer. Chat listing must also have been created.
    if(mIsFormsMode)
    {
        if(mFormsPartyMember == -1)
        {
            if(mFormsOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mFormsOpacityTimer ++;
            if(mFormSubOpacityTimer > 0) mFormSubOpacityTimer --;
        }
        else
        {
            if(mFormsOpacityTimer > 0) mFormsOpacityTimer --;
            if(mFormSubOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mFormSubOpacityTimer ++;
            return;
        }
    }
    //--Decrement and stop if not.
    else
    {
        if(mFormsOpacityTimer > 0) mFormsOpacityTimer --;
        if(mFormSubOpacityTimer > 0) mFormSubOpacityTimer --;
        return;
    }

    //--[Setup]
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Error check:
    if(mFormsGridCurrent < 0 || mFormsGridCurrent >= mFormsPartyGrid->GetListSize()) mFormsGridCurrent = 0;
    GridPackage *rPackage = (GridPackage *)mFormsPartyGrid->GetElementBySlot(mFormsGridCurrent);
    if(!rPackage)
    {
        DeactivateFormsMenu();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    //--[Directions]
    //--Directional controls are handled programmatically. If the instruction is -1, it means there is nothing
    //  at that grid position.
    int tGridPrevious = mFormsGridCurrent;
    if(rControlManager->IsFirstPress("Up"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_TOP];
        if(tTargetEntry != -1) mFormsGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_BOT];
        if(tTargetEntry != -1) mFormsGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_LFT];
        if(tTargetEntry != -1) mFormsGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_RGT];
        if(tTargetEntry != -1) mFormsGridCurrent = tTargetEntry;
    }

    //--If the grid changed positions, play a sound.
    if(tGridPrevious != mFormsGridCurrent)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Size setting. All not-selected grid entries decrease their timers. The selected entry increases.
    GridPackage *rTimerPackage = (GridPackage *)mFormsPartyGrid->PushIterator();
    while(rTimerPackage)
    {
        //--Increment if selected.
        if(rTimerPackage == rPackage)
        {
            if(rTimerPackage->mTimer < AM_CAMPFIRE_SIZE_TICKS) rTimerPackage->mTimer ++;
        }
        //--Decrement if deselected.
        else
        {
            if(rTimerPackage->mTimer > 0) rTimerPackage->mTimer --;
        }
        rTimerPackage = (GridPackage *)mFormsPartyGrid->AutoIterate();
    }

    //--Activate, talk to the specified character. Subroutine handles SFX.
    if(rControlManager->IsFirstPress("Activate"))
    {
        ActivateFormsSubMenu(mFormsGridCurrent);
        mStartedHidingThisTick = true;
    }
    //--Cancel, previous menu.
    else if(rControlManager->IsFirstPress("Cancel") && !mStartedHidingThisTick)
    {
        DeactivateFormsMenu();
        mStartedHidingThisTick = true;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureMenu::UpdateFormsSubMenu()
{
    //--[Documentation]
    //--The submenu for Forms mode is the menu where the player selects which form the character in question
    //  should transform to. The party member should be -1 if this mode is not active.
    //--UpdateFormsMenu() is always called right before this routine, and will handle the timers for us.
    if(!mIsFormsMode || mFormsPartyMember == -1) return;

    //--[Setup]
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Error check:
    if(mFormSubGridCurrent < 0 || mFormSubGridCurrent >= mFormSubGrid->GetListSize()) mFormSubGridCurrent = 0;
    GridPackage *rPackage = (GridPackage *)mFormSubGrid->GetElementBySlot(mFormSubGridCurrent);
    if(!rPackage)
    {
        DeactivateFormsMenu();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    //--[Directions]
    //--Directional controls are handled programmatically. If the instruction is -1, it means there is nothing
    //  at that grid position.
    int tGridPrevious = mFormSubGridCurrent;
    if(rControlManager->IsFirstPress("Up"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_TOP];
        if(tTargetEntry != -1) mFormSubGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_BOT];
        if(tTargetEntry != -1) mFormSubGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_LFT];
        if(tTargetEntry != -1) mFormSubGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_RGT];
        if(tTargetEntry != -1) mFormSubGridCurrent = tTargetEntry;
    }

    //--If the grid changed positions, play a sound.
    if(tGridPrevious != mFormSubGridCurrent)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Size setting. All not-selected grid entries decrease their timers. The selected entry increases.
    GridPackage *rTimerPackage = (GridPackage *)mFormSubGrid->PushIterator();
    while(rTimerPackage)
    {
        //--Increment if selected.
        if(rTimerPackage == rPackage)
        {
            if(rTimerPackage->mTimer < AM_CAMPFIRE_SIZE_TICKS) rTimerPackage->mTimer ++;
        }
        //--Decrement if deselected.
        else
        {
            if(rTimerPackage->mTimer > 0) rTimerPackage->mTimer --;
        }
        rTimerPackage = (GridPackage *)mFormSubGrid->AutoIterate();
    }

    //--Activate, talk to the specified character.
    if(rControlManager->IsFirstPress("Activate") && !mStartedHidingThisTick)
    {
        //--Get the script path.
        const char *rScriptPath = (const char *)mFormSubPaths->GetElementBySlot(mFormSubGridCurrent);

        //--Run the script.
        LuaManager::Fetch()->ExecuteLuaFile(rScriptPath);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");

        //--Hide this menu. Must be called after everything else since it clears the form list.
        DeactivateFormsMenu();
        Hide();
        mFormsPartyMember = -1;
        mStartedHidingThisTick = true;
    }
    //--Cancel, previous menu.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mFormsPartyMember = -1;
        mStartedHidingThisTick = true;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureMenu::RenderFormsMenu()
{
    //--[Documentation]
    //--Renders the forms menu. This will always consist of at least the party listing, and may also consist of
    //  a list of forms for that character. Characters who cannot change forms are greyed out but still listed.
    if(!Images.mIsReady) return;

    //--[Setup]
    //--Don't render at all if the opacity is at zero.
    if(mFormsOpacityTimer < 1) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mFormsOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--[Title]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    Images.CampfireMenu.rHeader->Draw(427.0f, 45.0f);
    Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 57.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select a Character");
    StarlightColor::ClearMixer();

    //--[Icons]
    //--Sizes.
    float cIconSize = Images.CampfireMenu.rIconFrame->GetTrueWidth();

    //--Position the objects slide away from.
    float tOpacitySlidePct = EasingFunction::QuadraticOut(mFormsOpacityTimer, AM_CAMPFIRE_OPACITY_TICKS);
    float tCenterX = AM_GRID_CENT_X;
    float tCenterY = AM_GRID_CENT_Y;

    //--Iterate across the list.
    GridPackage *rGridPack = (GridPackage *)mFormsPartyGrid->PushIterator();
    SugarBitmap *rImage    = (SugarBitmap *)mFormsPartyGridImg->PushIterator();
    while(rGridPack && rImage)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
        float cScaleInv = 1.0f / cScale;

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        glTranslatef(tXPos, tYPos, 0.0f);
        glScalef(cScale, cScale, 1.0f);
        Images.CampfireMenu.rIconFrame->Draw();
        rImage->Draw(0, -3);
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-tXPos, -tYPos, 0.0f);

        //--Next.
        rGridPack = (GridPackage *)mFormsPartyGrid->AutoIterate();
        rImage    = (SugarBitmap *)mFormsPartyGridImg->AutoIterate();
    }

    //--Iterate across the list again, this time just rendering the names.
    rGridPack = (GridPackage *)mFormsPartyGrid->PushIterator();
    void *rListingPtr = mFormsPartyList->PushIterator();
    while(rGridPack && rListingPtr)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);
        float tTextX = tXPos;

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        float cTxtScale = 1.0f + (cBoostPct * (AM_CAMPFIRE_SIZE_BOOST * 0.33f));
        Images.CampfireMenu.rMainlineFont->DrawText(tTextX, tYPos - 15.0f, SUGARFONT_AUTOCENTER_X, cTxtScale, mFormsPartyList->GetIteratorName());

        //--Next.
        rGridPack = (GridPackage *)mFormsPartyGrid->AutoIterate();
        rListingPtr = mFormsPartyList->AutoIterate();
    }
}
void AdventureMenu::RenderFormsSubMenu()
{
    ///--[Documentation and Setup]
    //--Renders the transformation selection for a given character. Appears when the player is selecting which form to switch to.
    if(!Images.mIsReady || !rFormCharacter) return;

    //--[Setup]
    //--Don't render at all if the opacity is at zero.
    if(mFormSubOpacityTimer < 1) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mFormSubOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--Get the character in question.
    CombatStatistics *rCharacterStats = rFormCharacter->GetStatisticsGroup(ADVCE_STATS_FINAL);

    ///--[Title]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    Images.CampfireMenu.rHeader->Draw(427.0f, 45.0f);
    Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 57.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select a Form");
    StarlightColor::ClearMixer();

    ///--[Icons]
    //--Sizes.
    float cIconSize = Images.CampfireMenu.rIconFrame->GetTrueWidth();

    //--Position the objects slide away from.
    float tOpacitySlidePct = EasingFunction::QuadraticOut(mFormSubOpacityTimer, AM_CAMPFIRE_OPACITY_TICKS);
    float tCenterX = AM_GRID_CENT_X;
    float tCenterY = AM_GRID_CENT_Y;

    //--Iterate across the list.
    GridPackage *rGridPack = (GridPackage *)mFormSubGrid->PushIterator();
    SugarBitmap *rImage    = (SugarBitmap *)mFormSubGridImg->PushIterator();
    while(rGridPack && rImage)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
        float cScaleInv = 1.0f / cScale;

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        glTranslatef(tXPos, tYPos, 0.0f);
        glScalef(cScale, cScale, 1.0f);
        Images.CampfireMenu.rIconFrame->Draw();
        rImage->Draw(0, -3);
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-tXPos, -tYPos, 0.0f);

        //--Next.
        rGridPack = (GridPackage *)mFormSubGrid->AutoIterate();
        rImage    = (SugarBitmap *)mFormSubGridImg->AutoIterate();
    }

    //--Iterate across the list again, this time just rendering the names.
    rGridPack = (GridPackage *)mFormSubGrid->PushIterator();
    void *rListingPtr = mFormSubPaths->PushIterator();
    while(rGridPack && rListingPtr)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);
        float tTextX = tXPos;

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        float cTxtScale = 1.0f + (cBoostPct * (AM_CAMPFIRE_SIZE_BOOST * 0.33f));
        Images.CampfireMenu.rMainlineFont->DrawText(tTextX, tYPos - 15.0f, SUGARFONT_AUTOCENTER_X, cTxtScale, mFormSubPaths->GetIteratorName());

        //--Next.
        rGridPack = (GridPackage *)mFormSubGrid->AutoIterate();
        rListingPtr = mFormSubPaths->AutoIterate();
    }

    ///--[Comparison Handler]
    //--Backing.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    float cOffset = ((1.0f - tOpacitySlidePct) * 300.0f);
    float cPosX = 0.0f + cOffset;
    Images.CampfireMenu.rComparison->Draw(cPosX, 0.0f);

    //--Get the comparison package currently in use.
    CombatStatistics *rComparisonPack = (CombatStatistics *)mFormSubComparisons->GetElementBySlot(mFormSubGridCurrent);
    if(!rComparisonPack) return;

    //--Positions.
    float cHeaderXPos = 1153.0f + cOffset;
    float tLXPos = 1070.0f + cOffset;
    float tAXPos = 1157.0f + cOffset; //Right-align
    float tBXPos = 1160.0f + cOffset;
    float tCXPos = 1238.0f + cOffset; //Right-align
    float tStatY   = 138.0f;
    float tResistY = 329.0f;
    float tYSpc    =  21.0f;

    //--Headers.
    Images.FormUI.rHeadingFont->DrawText(cHeaderXPos,  87.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Stats");
    Images.FormUI.rHeadingFont->DrawText(cHeaderXPos, 278.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Resistances");

    //--Other.
    StarlightColor cColorVL = StarlightColor::MapRGBAF(1.0f, 0.0f, 0.0f, 1.0f);
    StarlightColor cColorLo = StarlightColor::MapRGBAF(0.5f, 0.0f, 0.0f, 1.0f);
    StarlightColor cColorNe = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    StarlightColor cColorHi = StarlightColor::MapRGBAF(0.0f, 0.5f, 0.0f, 1.0f);
    StarlightColor cColorVH = StarlightColor::MapRGBAF(0.0f, 1.0f, 0.0f, 1.0f);

    //--Render the icons.
    for(int i = 0; i < AM_STAT_COMPARE_PACKS_TOTAL; i ++)
    {
        //--Verify the image.
        if(!mStatComparePacks[i].rImage) continue;

        //--Compute Y position. It jumps when resistances get computed.
        float cYPos = tStatY + (tYSpc * i);
        if(i >= AM_STAT_COMPARE_PACK_PROTECTION) cYPos = tResistY + (tYSpc * (i-AM_STAT_COMPARE_PACK_PROTECTION));

        //--Render the image.
        mStatComparePacks[i].rImage->Draw(tLXPos, cYPos);

        //--Index.
        int p = mStatComparePacks[i].mIndex;

        //--Get comparisons and resolve color.
        int tValueL = rCharacterStats->mValueList[p];
        int tValueR = rComparisonPack->mValueList[p];

        //--Special: If both values are 1000 or higher, print "Immune" across the stat line.
        if(tValueL >= 1000 && tValueR >= 1000)
        {
            Images.FormUI.rMainlineFont->DrawTextArgs(tCXPos, cYPos-4, SUGARFONT_RIGHTALIGN_X, 1.0f, "Immune");
            continue;
        }

        //--Render the stat of the base.
        if(tValueL >= 1000)
            Images.FormUI.rMainlineFont->DrawTextArgs(tAXPos, cYPos-4, SUGARFONT_RIGHTALIGN_X, 1.0f, "IMN");
        else
            Images.FormUI.rMainlineFont->DrawTextArgs(tAXPos, cYPos-4, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rCharacterStats->mValueList[p]);

        //--Render an arrow.
        Images.FormUI.rMainlineFont->DrawTextArgs(tBXPos, cYPos-6, 0, 1.0f, "->");

        //--Resolve comparison color.
        if(tValueR < tValueL * 0.75f)
            cColorVL.SetAsMixer();
        else if(tValueR < tValueL * 0.95f)
            cColorLo.SetAsMixer();
        else if(tValueR < tValueL * 1.05f)
            cColorNe.SetAsMixer();
        else if(tValueR < tValueL * 1.25f)
            cColorHi.SetAsMixer();
        else
            cColorVH.SetAsMixer();

        //--Render the comparison value.
        if(tValueR >= 1000)
            Images.FormUI.rMainlineFont->DrawTextArgs(tCXPos, cYPos-4, SUGARFONT_RIGHTALIGN_X, 1.0f, "IMN");
        else
            Images.FormUI.rMainlineFont->DrawTextArgs(tCXPos, cYPos-4, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rComparisonPack->mValueList[p]);
        StarlightColor::ClearMixer();
    }
}
