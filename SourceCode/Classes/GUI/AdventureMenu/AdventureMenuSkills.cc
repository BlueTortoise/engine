//--Base
#include "AdventureMenu.h"
#include "AdventureMenuSkills.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "AdvHelp.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

//=========================================== System ==============================================
void AdventureMenu::SetToSkillsMode()
{
    //--Variables.
    mCurrentMode = AM_MODE_SKILLS;
    mSkillMenu.mActiveCharacter = 0;
    mSkillMenu.mJobCursor = 0;
    mSkillMenu.mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;

    //--Refresh these in case the controls changed.
    mSkillMenu.mAcceptString->SetString("[IMG0] Accept");
    mSkillMenu.mAcceptString->AllocateImages(1);
    mSkillMenu.mAcceptString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mSkillMenu.mAcceptString->CrossreferenceImages();

    mSkillMenu.mCancelString->SetString("[IMG0] Cancel");
    mSkillMenu.mCancelString->AllocateImages(1);
    mSkillMenu.mCancelString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mSkillMenu.mCancelString->CrossreferenceImages();

    //--Recompute positions with 0th entry.
    RecomputeCursorPos(0);
}
void AdventureMenu::RefreshSkillMenuHelp()
{
    //--Resolve the images needed for the help menu.
    ControlManager *rControlManager = ControlManager::Fetch();
    mMainMenuHelp->Construct();

    //--Allocate strings.
    int cStrings = 8;
    mMainMenuHelp->AllocateStrings(cStrings);
    StarlightString **rStrings = mMainMenuHelp->GetStrings();

    //--Set.
    rStrings[0]->SetString("[IMG0] Select Job/Skill for purchase, Confirm purchase");
    rStrings[0]->AllocateImages(1);
    rStrings[0]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Activate"));

    rStrings[1]->SetString("[IMG0] Previous Menu");
    rStrings[1]->AllocateImages(1);
    rStrings[1]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Cancel"));

    rStrings[2]->SetString(" ");

    rStrings[3]->SetString("[IMG0][IMG1] Change Job/Skill Selection");
    rStrings[3]->AllocateImages(2);
    rStrings[3]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Up"));
    rStrings[3]->SetImageP(1, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Down"));

    rStrings[4]->SetString(" ");

    rStrings[5]->SetString("[IMG0] Examine Equipped Skills");
    rStrings[5]->AllocateImages(1);
    rStrings[5]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Jump"));

    rStrings[6]->SetString("[IMG0][IMG1][IMG2][IMG3] Change Skill Selection");
    rStrings[6]->AllocateImages(4);
    rStrings[6]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Up"));
    rStrings[6]->SetImageP(1, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Right"));
    rStrings[6]->SetImageP(2, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Down"));
    rStrings[6]->SetImageP(3, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("Left"));

    rStrings[7]->SetString("[IMG0] Close This Display");
    rStrings[7]->AllocateImages(1);
    rStrings[7]->SetImageP(0, CM_IMG_OFFSET_Y, rControlManager->ResolveControlImage("F1"));

    //--All strings cross-reference here.
    for(int i = 0; i < cStrings; i ++) rStrings[i]->CrossreferenceImages();
}

//====================================== Property Queries =========================================
bool AdventureMenu::IsCustomizableSlot(int pX, int pY)
{
    //--Returns whether or not the given slot can be edited. Takes into account catalysts.
    if(pX < 0 || pY < 0) return false;
    if(pX >= ACE_ABILITY_GRID_SIZE_X || pY >= ACE_ABILITY_GRID_SIZE_Y) return false;

    //--Top row. Cannot be edited.
    if(pY == 0) return false;

    //--Middle row, left side. This is the class bar.
    if(pY == 1 && pX <= 5) return false;

    //--Middle right, right side. Can always be edited.
    if(pY == 1 && pX > 5) return true;

    //--Bottom row, left side. Based on catalysts.
    int tCatalystSlots = AdvCombat::Fetch()->GetSkillCatalystSlots() - 1;
    if(pY == 2 && pX <= 5 && pX <= tCatalystSlots) return true;

    //--Bottom row, right side. Can always be edited.
    if(pY == 2 && pX > 5) return true;
    return false;
}

//========================================= Core Methods ==========================================
void AdventureMenu::RecomputeCursorPos(int pTicks)
{
    //--[Documentation and Setup]
    //--Figure out where the cursor needs to be *after* scroll offsets have occurred.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Active entity:
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mSkillMenu.mActiveCharacter);
    if(!rActiveEntity) return;

    //--Position when in jobs window.
    if(mSkillMenu.mMode == ADVMENU_SKILLS_MODE_JOB_SELECT)
    {
        //--Job listing. We need the name of the selected job.
        SugarLinkedList *rJobList = rActiveEntity->GetJobList();
        AdvCombatJob *rSelectedJob = (AdvCombatJob *)rJobList->GetElementBySlot(mSkillMenu.mJobCursor);
        if(!rSelectedJob) return;

        //--Get X/Y render position.
        float cTxtLft =  31.0f;
        float cTxtTop = 120.0f;
        float cTxtHei = Images.SkillsUI.rJobFont->GetTextHeight() + ADVMENU_HIGHLIGHT_INDENT;

        //--Compute.
        int tIndex = mSkillMenu.mJobCursor - mSkillMenu.mScrollOffJob;
        mSkillMenu.mHighlightPos.MoveTo(cTxtLft, cTxtTop + (tIndex * cTxtHei), pTicks);

        //--Get the width of that entry.
        const char *rName = rSelectedJob->GetDisplayName();
        if(!rName) return;
        float cWid = Images.SkillsUI.rJobFont->GetTextWidth(rName) + (ADVMENU_HIGHLIGHT_INDENT*2);
        float cHei = Images.SkillsUI.rJobFont->GetTextHeight() + ADVMENU_HIGHLIGHT_INDENT;
        mSkillMenu.mHighlightSize.MoveTo(cWid, cHei, pTicks);
    }
    //--Skills window:
    else if(mSkillMenu.mMode == ADVMENU_SKILLS_MODE_SKILL_SELECT)
    {
        //--We need the selected job so we can get which ability set is being displayed.
        SugarLinkedList *rJobList = rActiveEntity->GetJobList();
        AdvCombatJob *rSelectedJob = (AdvCombatJob *)rJobList->GetElementBySlot(mSkillMenu.mJobCursor);
        if(!rSelectedJob) return;

        //--Get the ability list.
        SugarLinkedList *rAbilityList = rSelectedJob->GetAbilityList();
        AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillMenu.mSkillCursor);
        if(!rAbility) return;

        //--Sizes.
        float cTxtLft = 390.0f;
        float cTxtTop = 120.0f;
        float cTxtHei = Images.SkillsUI.rSkillFont->GetTextHeight() + ADVMENU_HIGHLIGHT_INDENT;

        //--Compute.
        int tIndex = mSkillMenu.mSkillCursor - mSkillMenu.mScrollOffSkill;
        mSkillMenu.mHighlightPos.MoveTo(cTxtLft, cTxtTop + (tIndex * cTxtHei), pTicks);

        //--Get the width of that entry.
        const char *rName = rAbility->GetDisplayName();
        if(!rName) return;
        float cWid = Images.SkillsUI.rSkillFont->GetTextWidth(rName) + (ADVMENU_HIGHLIGHT_INDENT*2) + 25.0f;
        float cHei = Images.SkillsUI.rSkillFont->GetTextHeight() + ADVMENU_HIGHLIGHT_INDENT;
        mSkillMenu.mHighlightSize.MoveTo(cWid, cHei, pTicks);
    }
    //--Ability query/Equipping:
    else if(mSkillMenu.mMode == ADVMENU_SKILLS_MODE_SKILL_QUERY || mSkillMenu.mMode == ADVMENU_SKILLS_MODE_SKILL_REPLACE)
    {
        //--Ability Grid
        float cAbltyLft = ADVCOMBAT_POSITION_ABILITY_X;
        float cAbltyTop = ADVCOMBAT_POSITION_ABILITY_Y;
        float cAbltyWid = ADVCOMBAT_POSITION_ABILITY_W;
        float cAbltyHei = ADVCOMBAT_POSITION_ABILITY_H;
        float cXPos = cAbltyLft + (mSkillMenu.mCombatPaneCursorX * cAbltyWid);
        float cYPos = cAbltyTop + (mSkillMenu.mCombatPaneCursorY * cAbltyHei);

        //--X position increases on the right half.
        if(mSkillMenu.mCombatPaneCursorX >= 6) cXPos = cXPos + 5.0f;

        //--Set.
        mSkillMenu.mHighlightPos.MoveTo(cXPos, cYPos, pTicks);
        mSkillMenu.mHighlightSize.MoveTo(50.0f, 50.0f, pTicks);
    }
}
void AdventureMenu::RecomputeJobOffset()
{
    //--Scroll offset computations.
    int cJobsPerPage = ADVMENU_SKILLS_MAX_JOBS_PER_PAGE;
    int cJobScrollRange = ADVMENU_SKILLS_JOB_SCROLL_RANGE;

    //--Error check the entity.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mSkillMenu.mActiveCharacter);
    if(!rActiveEntity) return;

    //--Get how many jobs this entity has.
    SugarLinkedList *rJobList = rActiveEntity->GetJobList();
    int cTotalJobs = rJobList->GetListSize();

    //--Decrement.
    if(mSkillMenu.mJobCursor < mSkillMenu.mScrollOffJob + cJobScrollRange)
    {
        mSkillMenu.mScrollOffJob = mSkillMenu.mJobCursor - cJobScrollRange;
    }

    //--Increment.
    if(mSkillMenu.mJobCursor > mSkillMenu.mScrollOffJob + cJobsPerPage - (cJobScrollRange + 1))
    {
        mSkillMenu.mScrollOffJob = mSkillMenu.mJobCursor + (cJobScrollRange + 1) - cJobsPerPage;
    }

    //--Clamp.
    if(mSkillMenu.mScrollOffJob > cTotalJobs - cJobsPerPage) mSkillMenu.mScrollOffJob = cTotalJobs - cJobsPerPage;
    if(mSkillMenu.mScrollOffJob <                         0) mSkillMenu.mScrollOffJob = 0;
}
void AdventureMenu::RecomputeSkillOffset()
{
    //--Scroll offset computations.
    int cSkillsPerPage = ADVMENU_SKILLS_MAX_SKILLS_PER_PAGE;
    int cSkillsScrollRange = ADVMENU_SKILLS_SKILL_SCROLL_RANGE;

    //--Error check the entity.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(mSkillMenu.mActiveCharacter);
    if(!rActiveEntity) return;

    //--Get the list of jobs, find the highlighted job.
    SugarLinkedList *rJobList = rActiveEntity->GetJobList();
    AdvCombatJob *rSelectedJob = (AdvCombatJob *)rJobList->GetElementBySlot(mSkillMenu.mJobCursor);
    if(!rSelectedJob) return;

    //--Get how many abilities the highlighted job has.
    SugarLinkedList *rAbilityList = rSelectedJob->GetAbilityList();
    int cAbilitiesTotal = rAbilityList->GetListSize();

    //--Decrement.
    if(mSkillMenu.mSkillCursor < mSkillMenu.mScrollOffSkill + cSkillsScrollRange)
    {
        mSkillMenu.mScrollOffSkill = mSkillMenu.mSkillCursor - cSkillsScrollRange;
    }

    //--Increment.
    if(mSkillMenu.mSkillCursor > mSkillMenu.mScrollOffSkill + cSkillsPerPage - (cSkillsScrollRange + 1))
    {
        mSkillMenu.mScrollOffSkill = mSkillMenu.mSkillCursor + (cSkillsScrollRange + 1) - cSkillsPerPage;
    }

    //--Clamp.
    if(mSkillMenu.mScrollOffSkill > cAbilitiesTotal - cSkillsPerPage) mSkillMenu.mScrollOffSkill = cAbilitiesTotal - cSkillsPerPage;
    if(mSkillMenu.mScrollOffSkill <                                0) mSkillMenu.mScrollOffSkill = 0;
}

//============================================ Update =============================================
bool AdventureMenu::UpdateSkillsMode(bool pCannotHandleUpdate)
{
    ///--[Documentation and Setup]
    //--Update handler. Has three modes: Selecting job, selecting skill, and applying skill to menu.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Setup all variables.
    AdvCombatEntity *rActiveEntity = NULL;
    SugarLinkedList *rJobList = NULL;
    int cTotalJobs = 0;
    AdvCombatJob *rSelectedJob = NULL;
    SugarLinkedList *rAbilityList = NULL;
    int cAbilitiesTotal = 0;

    ///--[Timers]
    //--If this is the active object, increment the vis timer.
    if(mCurrentMode == AM_MODE_SKILLS)
    {
        if(mSkillMenu.mVisibilityTimer < ADVMENU_SKILLS_VIS_TICKS) mSkillMenu.mVisibilityTimer ++;
    }
    //--Decrement.
    else
    {
        if(mSkillMenu.mVisibilityTimer > 0) mSkillMenu.mVisibilityTimer --;
    }

    //--Easing packages.
    mSkillMenu.mHighlightPos.Increment(EASING_CODE_QUADOUT);
    mSkillMenu.mHighlightSize.Increment(EASING_CODE_QUADOUT);

    //--Timers that trigger flags.
    if(mSkillMenu.mStopShowingJobHighlightTimer > 0)
    {
        mSkillMenu.mStopShowingJobHighlightTimer --;
        if(mSkillMenu.mStopShowingJobHighlightTimer < 1)
        {
            mSkillMenu.mShowJobHighlight = false;
        }
    }
    if(mSkillMenu.mStopShowingAbilityHighlightTimer > 0)
    {
        mSkillMenu.mStopShowingAbilityHighlightTimer --;
        if(mSkillMenu.mStopShowingAbilityHighlightTimer < 1)
        {
            mSkillMenu.mShowAbilityHighlight = false;
        }
    }

    ///--[Error Checking]
    //--If this menu is not the main object, stop the update here.
    if(mCurrentMode != AM_MODE_SKILLS || pCannotHandleUpdate) return false;

    //--Error check the entity.
    rActiveEntity = rAdventureCombat->GetActiveMemberI(mSkillMenu.mActiveCharacter);
    if(!rActiveEntity)
    {
        SetToMainMenu();
        return true;
    }

    //--Get how many jobs this entity has.
    rJobList = rActiveEntity->GetJobList();
    cTotalJobs = rJobList->GetListSize();

    //--Get the list of jobs, find the highlighted job.
    rSelectedJob = (AdvCombatJob *)rJobList->GetElementBySlot(mSkillMenu.mJobCursor);
    if(!rSelectedJob)
    {
        SetToMainMenu();
        return true;
    }

    //--Get how many abilities the highlighted job has.
    rAbilityList = rSelectedJob->GetAbilityList();
    cAbilitiesTotal = rAbilityList->GetListSize();

    ///--[Help Handler]
    if(mIsMainHelpVisible)
    {
        if(rControlManager->IsFirstPress("F1") || rControlManager->IsFirstPress("Cancel"))
        {
            mIsMainHelpVisible = false;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        return true;
    }

    //--Help activation.
    if(rControlManager->IsFirstPress("F1"))
    {
        mIsMainHelpVisible = true;
        RefreshSkillMenuHelp();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return true;
    }

    ///--[Job Highlight Mode]
    //--Player is selecting which job they want to select skills from.
    if(mSkillMenu.mMode == ADVMENU_SKILLS_MODE_JOB_SELECT)
    {
        //--Jump, switches to ability query mode.
        if(rControlManager->IsFirstPress("Jump"))
        {
            mSkillMenu.mGoBackToJob = true;
            mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_QUERY;
            mSkillMenu.mCombatPaneCursorX = 0;
            mSkillMenu.mCombatPaneCursorY = 0;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Up, moves selection up. Can change modes when at zero.
        if(rControlManager->IsFirstPress("Up"))
        {
            //--When on the zeroth option, do nothing.
            if(mSkillMenu.mJobCursor == 0)
            {
                mSkillMenu.mJobCursor = cTotalJobs - 1;
                mSkillMenu.mSkillCursor = 0;
                RecomputeJobOffset();
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            }
            //--Decrement.
            else
            {
                mSkillMenu.mJobCursor --;
                mSkillMenu.mSkillCursor = 0;
                RecomputeJobOffset();
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            }
        }
        //--Down, moves selection down. Can change modes when at maximum.
        if(rControlManager->IsFirstPress("Down"))
        {
            //--When on the last option, do nothing.
            if(mSkillMenu.mJobCursor == cTotalJobs-1)
            {
                mSkillMenu.mJobCursor = 0;
                mSkillMenu.mSkillCursor = 0;
                RecomputeJobOffset();
                RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
            //--Increment.
            else
            {
                mSkillMenu.mJobCursor ++;
                mSkillMenu.mSkillCursor = 0;
                RecomputeJobOffset();
                RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Left. Decrements character.
        if(rControlManager->IsFirstPress("Left"))
        {
            //--Increment until a valid party member is found.
            int tStartChar = mSkillMenu.mActiveCharacter;
            while(true)
            {
                mSkillMenu.mActiveCharacter = (mSkillMenu.mActiveCharacter - 1);
                if(mSkillMenu.mActiveCharacter < 0) mSkillMenu.mActiveCharacter = ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE-1;
                AdvCombatEntity *rCheckEntity = rAdventureCombat->GetActiveMemberI(mSkillMenu.mActiveCharacter);
                if(rCheckEntity) break;
            }
            if(tStartChar != mSkillMenu.mActiveCharacter)
            {
                mSkillMenu.mJobCursor = 0;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            //--In all cases, the update ends here since the entity may have changed.
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            return true;
        }
        //--Right. Increments character.
        if(rControlManager->IsFirstPress("Right"))
        {
            //--Increment until a valid party member is found.
            int tStartChar = mSkillMenu.mActiveCharacter;
            while(true)
            {
                mSkillMenu.mActiveCharacter = (mSkillMenu.mActiveCharacter + 1);
                if(mSkillMenu.mActiveCharacter >= ADVCOMBAT_MAX_ACTIVE_PARTY_SIZE) mSkillMenu.mActiveCharacter = 0;
                AdvCombatEntity *rCheckEntity = rAdventureCombat->GetActiveMemberI(mSkillMenu.mActiveCharacter);
                if(rCheckEntity) break;
            }
            if(tStartChar != mSkillMenu.mActiveCharacter)
            {
                mSkillMenu.mJobCursor = 0;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }

            //--In all cases, the update ends here since the entity may have changed.
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            return true;
        }

        //--Activate, goes into the selected job's ability list.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Store position.
            mSkillMenu.mShowJobHighlight = true;
            mSkillMenu.mStopShowingJobHighlightTimer = 0;
            mSkillMenu.mJobHightlightStore.SetWH(mSkillMenu.mHighlightPos.mXCur, mSkillMenu.mHighlightPos.mYCur, mSkillMenu.mHighlightSize.mXCur, mSkillMenu.mHighlightSize.mYCur);

            //--New mode.
            mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
            mSkillMenu.mSkillCursor = 0;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Cancel, return to main menu.
        else if(rControlManager->IsFirstPress("Cancel") && !mStartedHidingThisTick)
        {
            SetToMainMenu();
            mBaseVars.mGridCurrent = AM_MAIN_SKILLS;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    ///--[Skill Highlight Mode]
    //--Player is selecting which skill they want to place, or purchase.
    else if(mSkillMenu.mMode == ADVMENU_SKILLS_MODE_SKILL_SELECT)
    {
        //--Jump, switches to ability query mode.
        if(rControlManager->IsFirstPress("Jump"))
        {
            mSkillMenu.mGoBackToJob = false;
            mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_QUERY;
            mSkillMenu.mCombatPaneCursorX = 0;
            mSkillMenu.mCombatPaneCursorY = 0;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Up, moves selection up. Can change modes when at zero.
        if(rControlManager->IsFirstPress("Up"))
        {
            //--When on the zeroth option, do nothing.
            if(mSkillMenu.mSkillCursor == 0)
            {
                mSkillMenu.mSkillCursor = cAbilitiesTotal - 1;
                RecomputeSkillOffset();
                RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
            //--Decrement.
            else
            {
                mSkillMenu.mSkillCursor --;
                RecomputeSkillOffset();
                RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Down, moves selection down. Can change modes when at maximum.
        if(rControlManager->IsFirstPress("Down"))
        {
            //--When on the last option, do nothing.
            if(mSkillMenu.mSkillCursor == cAbilitiesTotal-1)
            {
                mSkillMenu.mSkillCursor = 0;
                if(mSkillMenu.mSkillCursor >= cAbilitiesTotal) mSkillMenu.mSkillCursor = cAbilitiesTotal - 1;
                RecomputeSkillOffset();
                RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
            //--Increment.
            else
            {
                mSkillMenu.mSkillCursor ++;
                if(mSkillMenu.mSkillCursor >= cAbilitiesTotal) mSkillMenu.mSkillCursor = cAbilitiesTotal - 1;
                RecomputeSkillOffset();
                RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Left. Does nothing.
        if(rControlManager->IsFirstPress("Left"))
        {
        }
        //--Right. Does nothing.
        if(rControlManager->IsFirstPress("Right"))
        {
        }

        //--Activate, either purchase the ability, or if already purchased, go to equip it.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Get the ability in question.
            AdvCombatAbility *rHighlightedAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillMenu.mSkillCursor);
            if(!rHighlightedAbility)
            {
                mSkillMenu.mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            }
            //--Ability is not unlocked.
            else if(!rHighlightedAbility->IsUnlocked())
            {
                //--Get the JP cost.
                int tJPCost = rHighlightedAbility->GetJPCost();

                //--Player has enough JP?
                if(tJPCost <= rActiveEntity->GetGlobalJP() + rSelectedJob->GetCurrentJP())
                {
                    mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_CONFIRM_PURCHASE;
                }
                //--Not enough JP.
                else
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
            }
            //--Ability is unlocked. Start equipping it.
            else
            {
                //--Ability is already equipped somewhere:
                if(rActiveEntity->IsAbilityEquipped(rHighlightedAbility))
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                    return true;
                }

                //--Ability is not equippable.
                if(!rHighlightedAbility->CanBeEquipped())
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                    return true;
                }

                //--Locate the first empty position. Priority is top to bottom, left to right.
                int tUseX = 6;
                int tUseY = 1;
                int tSlotsTotal = 14;
                int cSlotsX[] = {6, 6, 7, 7, 8, 8, 9, 9, 0, 1, 2, 3, 4, 5};
                int cSlotsY[] = {1, 2, 1, 2, 1, 2, 1, 2, 2, 2, 2, 2, 2, 2};
                for(int i = 0; i < tSlotsTotal; i ++)
                {
                    if(!rActiveEntity->GetAbilityBySlot(cSlotsX[i], cSlotsY[i]))
                    {
                        tUseX = cSlotsX[i];
                        tUseY = cSlotsY[i];
                        break;
                    }
                }

                //--Move to the ability query area.
                mSkillMenu.mGoBackToJob = false;
                mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_REPLACE;
                mSkillMenu.mCombatPaneCursorX = tUseX;
                mSkillMenu.mCombatPaneCursorY = tUseY;
                RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
        //--Cancel, return to main menu.
        else if(rControlManager->IsFirstPress("Cancel") && !mStartedHidingThisTick)
        {
            mSkillMenu.mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            mSkillMenu.mStopShowingJobHighlightTimer = ADVMENU_STD_MOVE_TICKS;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Debug key: F2 insta-buys all abilities on this job.
        if(rControlManager->IsFirstPress("F2"))
        {
            //--Get variables.
            AdvCombatAbility *rHighlightedAbility = (AdvCombatAbility *)rAbilityList->PushIterator();
            while(rHighlightedAbility)
            {
                rHighlightedAbility->SetUnlocked(true);
                rHighlightedAbility = (AdvCombatAbility *)rAbilityList->AutoIterate();
            }

            //--Play the purchase sound.
            AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");
        }
    }
    ///--[Skill Query Mode]
    //--Player is querying skills already placed.
    else if(mSkillMenu.mMode == ADVMENU_SKILLS_MODE_SKILL_QUERY)
    {
        //--Jump, switches to whichever mode we got here from.
        if(rControlManager->IsFirstPress("Jump"))
        {
            if(mSkillMenu.mGoBackToJob)
            {
                mSkillMenu.mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            }
            else
            {
                mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
            }
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Up, moves selection up. Can change modes when at zero.
        if(rControlManager->IsFirstPress("Up"))
        {
            mSkillMenu.mCombatPaneCursorY --;
            if(mSkillMenu.mCombatPaneCursorY < 0) mSkillMenu.mCombatPaneCursorY = ACE_ABILITY_GRID_SIZE_Y - 1;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Down, moves selection down. Can change modes when at maximum.
        if(rControlManager->IsFirstPress("Down"))
        {
            mSkillMenu.mCombatPaneCursorY ++;
            if(mSkillMenu.mCombatPaneCursorY >= ACE_ABILITY_GRID_SIZE_Y) mSkillMenu.mCombatPaneCursorY = 0;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Left. Moves cursor. Wraps.
        if(rControlManager->IsFirstPress("Left"))
        {
            mSkillMenu.mCombatPaneCursorX --;
            if(mSkillMenu.mCombatPaneCursorX < 0) mSkillMenu.mCombatPaneCursorX = ACE_ABILITY_GRID_SIZE_X - 1;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Right. Does nothing.
        if(rControlManager->IsFirstPress("Right"))
        {
            mSkillMenu.mCombatPaneCursorX ++;
            if(mSkillMenu.mCombatPaneCursorX >= ACE_ABILITY_GRID_SIZE_X) mSkillMenu.mCombatPaneCursorX = 0;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Activate, clears editable slots.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--If the slot was illegal, fail.
            if(!IsCustomizableSlot(mSkillMenu.mCombatPaneCursorX, mSkillMenu.mCombatPaneCursorY))
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return true;
            }

            //--Slot was not illegal. Clear it.
            rActiveEntity->SetAbilitySlot(mSkillMenu.mCombatPaneCursorX, mSkillMenu.mCombatPaneCursorY, "Null");
        }
        //--Cancel, returns to whichever mode got us here.
        else if(rControlManager->IsFirstPress("Cancel") && !mStartedHidingThisTick)
        {
            if(mSkillMenu.mGoBackToJob)
            {
                mSkillMenu.mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            }
            else
            {
                mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
            }
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    ///--[Skill Placement Mode]
    //--Player is placing a skill they have purchased.
    else if(mSkillMenu.mMode == ADVMENU_SKILLS_MODE_SKILL_REPLACE)
    {
        //--Jump, switches to whichever mode we got here from.
        if(rControlManager->IsFirstPress("Jump"))
        {
            if(mSkillMenu.mGoBackToJob)
            {
                mSkillMenu.mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            }
            else
            {
                mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
            }
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Up, moves selection up. Can change modes when at zero.
        if(rControlManager->IsFirstPress("Up"))
        {
            mSkillMenu.mCombatPaneCursorY --;
            if(mSkillMenu.mCombatPaneCursorY < 0) mSkillMenu.mCombatPaneCursorY = ACE_ABILITY_GRID_SIZE_Y - 1;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Down, moves selection down. Can change modes when at maximum.
        if(rControlManager->IsFirstPress("Down"))
        {
            mSkillMenu.mCombatPaneCursorY ++;
            if(mSkillMenu.mCombatPaneCursorY >= ACE_ABILITY_GRID_SIZE_Y) mSkillMenu.mCombatPaneCursorY = 0;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Left. Moves cursor. Wraps.
        if(rControlManager->IsFirstPress("Left"))
        {
            mSkillMenu.mCombatPaneCursorX --;
            if(mSkillMenu.mCombatPaneCursorX < 0) mSkillMenu.mCombatPaneCursorX = ACE_ABILITY_GRID_SIZE_X - 1;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Right. Does nothing.
        if(rControlManager->IsFirstPress("Right"))
        {
            mSkillMenu.mCombatPaneCursorX ++;
            if(mSkillMenu.mCombatPaneCursorX >= ACE_ABILITY_GRID_SIZE_X) mSkillMenu.mCombatPaneCursorX = 0;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Activate, does nothing.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Make sure the ability exists.
            AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillMenu.mSkillCursor);
            if(!rAbility)
            {
                mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
                RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return true;
            }

            //--If the slot was illegal, fail.
            if(!IsCustomizableSlot(mSkillMenu.mCombatPaneCursorX, mSkillMenu.mCombatPaneCursorY))
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return true;
            }

            //--Get internal name.
            const char *rInternalName = rAbility->GetInternalName();
            rActiveEntity->SetAbilitySlot(mSkillMenu.mCombatPaneCursorX, mSkillMenu.mCombatPaneCursorY, rInternalName);

            //--Switch back to skill select mode.
            mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Cancel, returns to whichever mode got us here.
        else if(rControlManager->IsFirstPress("Cancel") && !mStartedHidingThisTick)
        {
            if(mSkillMenu.mGoBackToJob)
            {
                mSkillMenu.mMode = ADVMENU_SKILLS_MODE_JOB_SELECT;
            }
            else
            {
                mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
            }
            RecomputeCursorPos(ADVMENU_STD_MOVE_TICKS);
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    ///--[Confirm Purchase Mode]
    //--Player is confirming buying a skill with JP.
    else if(mSkillMenu.mMode == ADVMENU_SKILLS_MODE_SKILL_CONFIRM_PURCHASE)
    {
        //--Activate, either purchase the ability, or if already purchased, go to equip it.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Get variables.
            AdvCombatAbility *rHighlightedAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillMenu.mSkillCursor);
            int tJPCost = rHighlightedAbility->GetJPCost();
            int tJobJP = rSelectedJob->GetCurrentJP();
            int tGlobalJP = rActiveEntity->GetGlobalJP();

            //--Spend JP out of the job's JP. If we have enough just for that, we're done.
            if(tJobJP >= tJPCost)
            {
                rSelectedJob->SetCurrentJP(tJobJP - tJPCost);
            }
            //--Otherwise, spend the rest out of the global.
            else
            {
                int tDifference = tJPCost - tJobJP;
                rSelectedJob->SetCurrentJP(0);
                rActiveEntity->SetGlobalJP(tGlobalJP - tDifference);
            }

            //--Mark the ability as purchased.
            rHighlightedAbility->SetUnlocked(true);

            //--Reset.
            mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;

            //--Play the purchase sound.
            AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");
        }
        //--Cancel, return to main menu.
        else if(rControlManager->IsFirstPress("Cancel") && !mStartedHidingThisTick)
        {
            mSkillMenu.mMode = ADVMENU_SKILLS_MODE_SKILL_SELECT;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }

    //--We handled the update.
    return true;
}

//=========================================== Drawing =============================================
void AdventureMenu::RenderSkillsMode()
{
    ///--[Documentation and Setup]
    if(!Images.mIsReady) return;

    //--Compute alpha.
    float cAlpha = EasingFunction::QuadraticInOut(mSkillMenu.mVisibilityTimer, ADVMENU_SKILLS_VIS_TICKS);
    if(cAlpha <= 0.0f) return;

    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Setup all variables.
    AdvCombatEntity *rActiveEntity = NULL;
    SugarLinkedList *rJobList = NULL;
    int cTotalJobs = 0;
    AdvCombatJob *rSelectedJob = NULL;
    SugarLinkedList *rAbilityList = NULL;

    //--Storage
    AdvCombatAbility *rDescriptionAbility = NULL;

    //--Error check the entity.
    rActiveEntity = rAdventureCombat->GetActiveMemberI(mSkillMenu.mActiveCharacter);
    if(!rActiveEntity)
    {
        SetToMainMenu();
        return;
    }

    //--Get how many jobs this entity has.
    rJobList = rActiveEntity->GetJobList();
    cTotalJobs = rJobList->GetListSize();

    //--Get the list of jobs, find the highlighted job.
    rSelectedJob = (AdvCombatJob *)rJobList->GetElementBySlot(mSkillMenu.mJobCursor);
    if(!rSelectedJob)
    {
        SetToMainMenu();
        return;
    }

    //--Get how many abilities the highlighted job has.
    rAbilityList = rSelectedJob->GetAbilityList();

    ///--[Position Setup]
    //--Header Setup
    float cJobListingLft = 17.0f;
    float cJobListingTop = 95.0f;

    //--Skill Listing
    float cSkillListingLft = 357.0f;
    float cSkillListingTop = 95.0f;

    //--Ability Grid
    float cAbltyLft = ADVCOMBAT_POSITION_ABILITY_X;
    float cAbltyTop = ADVCOMBAT_POSITION_ABILITY_Y;
    float cAbltyWid = ADVCOMBAT_POSITION_ABILITY_W;
    float cAbltyHei = ADVCOMBAT_POSITION_ABILITY_H;

    ///--[Static Parts]
    //--Images.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
    Images.SkillsUI.rHeader->Draw();

    //--Jobs pane is greyed if not in jobs mode:
    if(mSkillMenu.mMode != ADVMENU_SKILLS_MODE_JOB_SELECT) StarlightColor::cxGrey.SetAsMixerAlpha(cAlpha);
    Images.SkillsUI.rPaneJobs->Draw();
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Skills pane is greyed if not in skills mode:
    if(mSkillMenu.mMode != ADVMENU_SKILLS_MODE_SKILL_SELECT) StarlightColor::cxGrey.SetAsMixerAlpha(cAlpha);
    Images.SkillsUI.rPaneSkills->Draw();
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Ability pane. The main pane is greyed out if not in query mode.
    if(mSkillMenu.mMode != ADVMENU_SKILLS_MODE_SKILL_QUERY) StarlightColor::cxGrey.SetAsMixerAlpha(cAlpha);
    Images.SkillsUI.rAbilityPane->Draw();
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--The secondary panes are greyed if not in query or equip mode.
    if(mSkillMenu.mMode != ADVMENU_SKILLS_MODE_SKILL_QUERY && mSkillMenu.mMode != ADVMENU_SKILLS_MODE_SKILL_REPLACE) StarlightColor::cxGrey.SetAsMixerAlpha(cAlpha);

    //--Resolve catalyst.
    int tCatalyst = rAdventureCombat->GetSkillCatalystSlots();
    if(tCatalyst > 0)
    {
        int tFrameIndex = tCatalyst - 1;
        if(tFrameIndex >= ADVCOMBAT_CATALYST_MAX) tFrameIndex = ADVCOMBAT_CATALYST_MAX - 1;
        Images.SkillsUI.rAbilityFrameCatalyst[tFrameIndex]->Draw();
    }

    //--Other.
    Images.SkillsUI.rAbilityFrameCustom->Draw();
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--Always renders fullbright.
    Images.SkillsUI.rCharacterPaneBack->Draw();
    Images.SkillsUI.rDescriptionPane->Draw();

    //--Text.
    Images.SkillsUI.rHeaderFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 40.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Skills");
    Images.SkillsUI.rJobFont->DrawText(cJobListingLft, cJobListingTop, 0, 1.0f, "Jobs");

    ///--[Character Rendering]
    //--Portrait. Uses stencils.
    SugarBitmap *rCombatPortrait = rActiveEntity->GetCombatPortrait();
    if(rCombatPortrait)
    {
        //--Mask.
        DisplayManager::ActivateMaskRender(AM_STENCIL_SKILLS+0);
        Images.SkillsUI.rCharacterPaneMask->Draw();

        //--Portrait.
        TwoDimensionRealPoint tRenderPos = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_MAIN);
        DisplayManager::ActivateStencilRender(AM_STENCIL_SKILLS+0);
        rCombatPortrait->Draw(tRenderPos.mXCenter, tRenderPos.mYCenter);

        //--Clean.
        DisplayManager::DeactivateStencilling();
    }

    //--Frame, banner.
    Images.SkillsUI.rCharacterPaneFrame->Draw();
    Images.SkillsUI.rCharacterPaneBanner->Draw();

    //--Name.
    const char *rDisplayName = rActiveEntity->GetDisplayName();
    if(rDisplayName)
    {
        Images.SkillsUI.rCharacterNameFont->DrawText(129.0f, 666.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, rDisplayName);
    }

    //--Render all the abilities they currently have equipped.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            //--Skip empty slots.
            AdvCombatAbility *rAbility = rActiveEntity->GetAbilityBySlot(x, y);
            if(!rAbility) continue;

            //--If this ability is the highlighted one, and we're in skill-check mode, it becomes the description.
            if(mSkillMenu.mCombatPaneCursorX == x && mSkillMenu.mCombatPaneCursorY == y) rDescriptionAbility = rAbility;

            //--Resolve position.
            float cRenderX = cAbltyLft + (cAbltyWid * x);
            float cRenderY = cAbltyTop + (cAbltyHei * y);

            //--X position increases on the right half.
            if(x >= 6) cRenderX = cRenderX + 5.0f;

            //--Get rendering images.
            SugarBitmap *rImageBack  = rAbility->GetIconBack();
            SugarBitmap *rImageFrame = rAbility->GetIconFrame();
            SugarBitmap *rImage      = rAbility->GetIcon();
            SugarBitmap *rCPImage    = rAbility->GetCPIcon();

            //--Render.
            if(rImageBack)  rImageBack->Draw(cRenderX, cRenderY);
            if(rImageFrame) rImageFrame->Draw(cRenderX, cRenderY);
            if(rImage)      rImage    ->Draw(cRenderX, cRenderY);
            if(rCPImage)    rCPImage  ->Draw(cRenderX, cRenderY);
        }
    }

    ///--[Job Rendering]
    //--Fast-access pointers.
    AdvCombatJob *rActiveJob = rActiveEntity->GetActiveJob();
    AdvCombatJob *rHighlightJob = NULL;

    //--Positions
    int cJobsPerPage = ADVMENU_SKILLS_MAX_JOBS_PER_PAGE;
    float cTxtLft =  31.0f + ADVMENU_HIGHLIGHT_INDENT;
    float cTxtTop = 120.0f;
    float cTxtHei = Images.SkillsUI.rJobFont->GetTextHeight() + ADVMENU_HIGHLIGHT_INDENT;
    float cTxtRgt = 334.0f;
    float tCurY = cTxtTop;

    //--Renders a list of jobs.
    int i = 0;
    AdvCombatJob *rJob = (AdvCombatJob *)rJobList->PushIterator();
    while(rJob)
    {
        //--Skip jobs under the scroll cursor.
        if(i < mSkillMenu.mScrollOffJob)
        {
            i ++;
            rJob = (AdvCombatJob *)rJobList->AutoIterate();
            continue;
        }

        //--If the rendering position exceeds this point, stop.
        if(tCurY >= cTxtTop + (cJobsPerPage * cTxtHei))
        {
            rJobList->PopIterator();
            break;
        }

        //--If the job is highlighted, store it for later.
        if(i == mSkillMenu.mJobCursor) rHighlightJob = rJob;

        //--Render the job's name.
        const char *rJobName = rJob->GetDisplayName();
        if(rJobName) Images.SkillsUI.rJobFont->DrawText(cTxtLft, tCurY, 0, 1.0f, rJobName);

        //--If this is the active job, render an icon next to it.
        if(rJob == rActiveJob)
        {
            Images.SkillsUI.rClassActive->Draw(cTxtLft-24.0, tCurY+3);
        }

        //--If this job is mastered, render an icon next to it.
        if(false)
        {
            Images.SkillsUI.rClassMaster->Draw(cTxtLft + Images.SkillsUI.rJobFont->GetTextWidth(rJobName), tCurY+3);
        }

        //--Right-aligned JP.
        int tJPTotal = rJob->GetCurrentJP();
        Images.SkillsUI.rJobFont->DrawTextArgs(cTxtRgt, tCurY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%iJP", tJPTotal);

        //--Next.
        i ++;
        tCurY = tCurY + cTxtHei;
        rJob = (AdvCombatJob *)rJobList->AutoIterate();
    }

    //--Render a scrollbar if there is more than one "page" worth of entries.
    if(cTotalJobs > cJobsPerPage && cTotalJobs > 0)
    {
        //--Backing.
        Images.SkillsUI.rScrollbarJobs->Draw();
        RenderScrollbarFront(303.0f, 120.0f, 436.0f, mSkillMenu.mScrollOffJob, mSkillMenu.mScrollOffJob+cJobsPerPage, cTotalJobs, 6.0f, Images.SkillsUI.rScrollbarFront);
    }

    ///--[Ability Rendering]
    //--Setup.
    int cSkillsPerPage = ADVMENU_SKILLS_MAX_JOBS_PER_PAGE;
    cTxtLft = 390.0f;
    cTxtTop = 120.0f;
    cTxtHei = Images.SkillsUI.rSkillFont->GetTextHeight() + ADVMENU_HIGHLIGHT_INDENT;
    tCurY = cTxtTop;
    float cIconWid = 25.0f;

    //--Headers.
    float cJPColumnX = 840.0f;
    Images.SkillsUI.rJobFont->DrawTextArgs(cSkillListingLft, cSkillListingTop, 0, 1.0f, "Abilities for %s", rSelectedJob->GetDisplayName());
    Images.SkillsUI.rJobFont->DrawTextArgs(cJPColumnX,       cSkillListingTop, SUGARFONT_RIGHTALIGN_X, 1.0f, "JP Cost");

    //--Render a list of abilities associated with this job.
    if(rHighlightJob)
    {
        //--Get the list.
        SugarLinkedList *rAbilityList = rHighlightJob->GetAbilityList();

        //--Iterate.
        int i = 0;
        AdvCombatAbility *rAbility = (AdvCombatAbility *)rAbilityList->PushIterator();
        while(rAbility)
        {
            //--If below the offset, show nothing.
            if(i < mSkillMenu.mScrollOffSkill)
            {
                i ++;
                rAbility = (AdvCombatAbility *)rAbilityList->AutoIterate();
                continue;
            }

            //--If the rendering position exceeds this point, stop.
            if(tCurY >= cTxtTop + (cSkillsPerPage * cTxtHei))
            {
                rJobList->PopIterator();
                break;
            }

            //--Render the icon. These are rendered at half size.
            SugarBitmap *rBacking = rAbility->GetIconBack();
            SugarBitmap *rFrame   = rAbility->GetIconFrame();
            SugarBitmap *rIcon    = rAbility->GetIcon();
            SugarBitmap *rCPImage = rAbility->GetCPIcon();
            if(rBacking || rIcon || rFrame)
            {
                glTranslatef( cTxtLft,  tCurY, 0.0f);
                glScalef(0.5f, 0.5f, 1.0f);
                if(rBacking) rBacking->Draw();
                if(rFrame)   rFrame->Draw();
                if(rIcon)    rIcon->Draw();
                if(rCPImage) rCPImage->Draw();
                glScalef(2.0f, 2.0f, 1.0f);
                glTranslatef(-cTxtLft, -tCurY, 0.0f);
            }

            //--If this ability is equipped, render the equipped symbol.
            if(rAbility->IsEquipped()) Images.SkillsUI.rSkillEquipped->Draw(cTxtLft - 32, tCurY+4);

            //--If this ability has an internal version, render another symbol.
            if(rHighlightJob->HasInternalVersionOfAbility(rAbility)) Images.SkillsUI.rClassActive->Draw(cTxtLft - 16, tCurY+4);

            //--Render the name of the ability.
            const char *rName = rAbility->GetDisplayName();
            if(rName) Images.SkillsUI.rSkillFont->DrawText(cTxtLft+cIconWid, tCurY, 0, 1.0f, rName);

            //--If this ability is the highlighted one, and we're in skill-check mode, it becomes the description.
            if(mSkillMenu.mSkillCursor == i && mSkillMenu.mMode == ADVMENU_SKILLS_MODE_SKILL_SELECT) rDescriptionAbility = rAbility;

            //--Render JP cost, or "Unlocked" if already purchased.
            if(rAbility->IsUnlocked())
            {
                Images.SkillsUI.rClassMaster->Draw(cJPColumnX-15, tCurY-1);
            }
            else
            {
                Images.SkillsUI.rSkillFont->DrawTextArgs(cJPColumnX, tCurY, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", rAbility->GetJPCost());
            }

            //--Next.
            i ++;
            tCurY = tCurY + cTxtHei;
            rAbility = (AdvCombatAbility *)rAbilityList->AutoIterate();
        }

        //--Render a scrollbar if there is more than one "page" worth of entries.
        int cTotalSkills = rAbilityList->GetListSize();
        if(cTotalSkills > cSkillsPerPage && cTotalSkills > 0)
        {
            //--Backing.
            Images.SkillsUI.rScrollbarSkills->Draw();
            RenderScrollbarFront(1323.0f, 120.0f, 436.0f, mSkillMenu.mScrollOffSkill, mSkillMenu.mScrollOffSkill+cSkillsPerPage, cTotalSkills, 6.0f, Images.SkillsUI.rScrollbarFront);
        }
    }

    ///--[Highlight]
    //--Highlight is able to move between interfaces to indicate player control.
    RenderExpandableHighlight(mSkillMenu.mHighlightPos.mXCur, mSkillMenu.mHighlightPos.mYCur, mSkillMenu.mHighlightPos.mXCur+mSkillMenu.mHighlightSize.mXCur, mSkillMenu.mHighlightPos.mYCur+mSkillMenu.mHighlightSize.mYCur, 4.0f, Images.SkillsUI.rHighlight);

    //--If a job highlight is stored, render that.
    if(mSkillMenu.mShowJobHighlight)
    {
        RenderExpandableHighlight(mSkillMenu.mJobHightlightStore.mLft, mSkillMenu.mJobHightlightStore.mTop, mSkillMenu.mJobHightlightStore.mRgt, mSkillMenu.mJobHightlightStore.mBot, 4.0f, Images.SkillsUI.rHighlight);
    }

    //--If an ability highlight is stored, render that.
    if(mSkillMenu.mShowAbilityHighlight)
    {
        RenderExpandableHighlight(mSkillMenu.mAbilityHightlightStore.mLft, mSkillMenu.mAbilityHightlightStore.mTop, mSkillMenu.mAbilityHightlightStore.mRgt, mSkillMenu.mAbilityHightlightStore.mBot, 4.0f, Images.SkillsUI.rHighlight);
    }

    ///--[Description Rendering]
    //--Renders a description for whatever job or skill is currently highlighted. Note that this code is identical to the AdvCombat
    //  version, since they borrow the same UI.
    if(rDescriptionAbility)
    {
        //--Position.
        float cTxtLft = 839.0f;
        float cTxtTop = 538.0f;
        float cTxtHei =  20.0f;

        //--Name.
        float cNameX = 859.0f;
        float cNameY = 505.0f;
        Images.SkillsUI.rDescriptionHeaderFont->DrawText(cNameX, cNameY, 0, 1.0f, rDescriptionAbility->GetDisplayName());

        //--Render.
        int tTotalLines = rDescriptionAbility->GetDescriptionLinesTotal();
        for(int i = 0; i < tTotalLines; i ++)
        {
            StarlightString *rDescription = rDescriptionAbility->GetDescriptionLine(i);
            if(rDescription) rDescription->DrawText(cTxtLft, cTxtTop + (cTxtHei * i), 0, 1.0f, Images.SkillsUI.rDescriptionFont);
        }
    }

    ///--[Clean]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    ///--[Purchase Overlay]
    if(mSkillMenu.mMode == ADVMENU_SKILLS_MODE_SKILL_CONFIRM_PURCHASE)
    {
        //--Fade the background.
        SugarBitmap::DrawFullBlack(0.75f * cAlpha);

        //--Get JP cost.
        AdvCombatAbility *rHighlightedAbility = (AdvCombatAbility *)rAbilityList->GetElementBySlot(mSkillMenu.mSkillCursor);
        int tJPCost = 0;
        if(rHighlightedAbility) tJPCost = rHighlightedAbility->GetJPCost();

        //--Render question.
        float cXPosition = VIRTUAL_CANVAS_X * 0.50f;
        float cYPosition = VIRTUAL_CANVAS_Y * 0.40f;
        Images.SkillsUI.rConfirmationBig->DrawTextArgs(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY, 1.0f, "Unlock this ability for %i JP?", tJPCost);

        //--Compute render position.
        const char *rAbilityName = rHighlightedAbility->GetDisplayName();
        float cSkillX = cXPosition - (Images.SkillsUI.rConfirmationSmall->GetTextWidth(rAbilityName) * 0.50f);
        float cSkillY = cYPosition + 50.0f;

        //--Render the ability information.
        SugarBitmap *rBacking = rHighlightedAbility->GetIconBack();
        SugarBitmap *rFrame   = rHighlightedAbility->GetIconFrame();
        SugarBitmap *rIcon    = rHighlightedAbility->GetIcon();
        SugarBitmap *rCPImage = rHighlightedAbility->GetCPIcon();
        if(rBacking) rBacking->Draw(cSkillX - 50.0f, cSkillY);
        if(rFrame)   rFrame->Draw  (cSkillX - 50.0f, cSkillY);
        if(rIcon)    rIcon->Draw   (cSkillX - 50.0f, cSkillY);
        if(rCPImage) rCPImage->Draw(cSkillX - 50.0f, cSkillY);
        Images.SkillsUI.rConfirmationSmall->DrawText(cSkillX + 5.0f, cSkillY + 6.0f, 0, 1.0f, rAbilityName);

        //--Render the accept string.
        cXPosition = VIRTUAL_CANVAS_X * 0.40f;
        cYPosition = VIRTUAL_CANVAS_Y * 0.60f;
        mSkillMenu.mAcceptString->DrawText(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY, 1.0f, Images.SkillsUI.rConfirmationSmall);

        //--Render the cancel string.
        cXPosition = VIRTUAL_CANVAS_X * 0.60f;
        cYPosition = VIRTUAL_CANVAS_Y * 0.60f;
        mSkillMenu.mCancelString->DrawText(cXPosition, cYPosition, SUGARFONT_AUTOCENTER_XY, 1.0f, Images.SkillsUI.rConfirmationSmall);
    }

    //--Clean.
    StarlightColor::ClearMixer();

    ///--[Help Menu]
    //--Help string.
    mShowHelpString->DrawText(0.0f, 0.0f, 0, 1.0f, Images.BaseMenu.rMainlineFont);

    //--Menu, if visible.
    if(mMainHelpTimer > 0)
    {
        //--Compute opacity.
        float cPercentage = EasingFunction::QuadraticInOut(mMainHelpTimer, ADV_HELP_STD_TICKS);
        SugarBitmap::DrawFullBlack(cPercentage * 0.75f);

        //--Offset.
        float tYOffset = ADV_HELP_STD_OFFSET * (1.0f - cPercentage);
        mMainMenuHelp->Render(tYOffset);
    }
}
