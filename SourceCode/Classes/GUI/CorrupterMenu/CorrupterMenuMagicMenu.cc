//--Base
#include "CorrupterMenu.h"

//--Classes
#include "ContextMenu.h"
#include "MagicPack.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "HitDetection.h"
#include "Subdivide.h"

//--GUI
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "EntityManager.h"

//=========================================== System ==============================================
//====================================== Property Queries =========================================
bool CorrupterMenu::IsMagicMode()
{
    return (mCurrentMode == CM_MODE_MAGIC);
}

//========================================= Manipulators ==========================================
void CorrupterMenu::SetToMagicMode()
{
    //--Flags.
    mHighlightedMagicIndex = -1;
    mCurrentMode = CM_MODE_MAGIC;
    FillSpellDescription(NULL);

    //--If this flag is true, magic spells need to check if they can be cast from this menu. They also
    //  check things like MP, player location, and disguise status.
    if(mMustRefreshMagicLegality)
    {
        mMustRefreshMagicLegality = false;
        MagicPack *rMagicPack = (MagicPack *)MagicPack::xMagicList->PushIterator();
        while(rMagicPack)
        {
            rMagicPack->RecheckMenuCast();
            rMagicPack = (MagicPack *)MagicPack::xMagicList->AutoIterate();
        }
    }
}

//========================================= Core Methods ==========================================
void CorrupterMenu::FillSpellDescription(MagicPack *pMagicPack)
{
    //--Sets the description lines on the perk confirmation window. Clears the lines if NULL is passed.
    //  Borrows the lines used for the perk descriptions since they will never be used at the same time.
    memset(mPerkUnlockLines, 0, sizeof(char) * CM_PERK_UNLOCK_LINES * CM_PERK_UNLOCK_CHARS);
    if(!pMagicPack) return;

    //--Parse along the status description lines.
    const char *rDescription = pMagicPack->GetDescription();
    if(!Images.Data.rUIFont || !rDescription) return;

    //--Specify how wide the window is.
    float cLength = cDescriptionListingWid;

    //--Setup.
    int tDescriptionLen = (int)strlen(rDescription);
    int tCursor = 0;
    int tRunningCursor = 0;
    int tCurrentLine = 0;

    //--Cut up.
    while(tRunningCursor < tDescriptionLen)
    {
        //--Get the string.
        char *tString = Subdivide::SubdivideString(tCursor, &rDescription[tRunningCursor], CM_PERK_UNLOCK_CHARS-1, cLength, Images.Data.rUIFont, 1.0f);

        //--Copy the line over.
        strcpy(mPerkUnlockLines[tCurrentLine], tString);

        //--Advance the cursor so the next line gets put up.
        tRunningCursor += tCursor;
        if(tCurrentLine < CM_PERK_UNLOCK_LINES - 1) tCurrentLine ++;

        //--Clean.
        free(tString);
    }
}
void CorrupterMenu::PopulateContextAroundActor(Actor *pActor, ContextMenu *pMenu)
{
    //--Given an Actor and a ContextMenu, runs the magic spells to see if they can target that Actor.
    //  If they can, adds them to the ContextMenu.
    if(!pActor || !pMenu) return;

    //--Iterate across the magic list.
    MagicPack *rMagicPack = (MagicPack *)MagicPack::xMagicList->PushIterator();
    while(rMagicPack)
    {
        //--Run the subroutine. If it comes back true, add it to the list.
        if(rMagicPack->AppearsOnContextActor(pActor))
        {
            //--Storage pack.
            SetMemoryData(__FILE__, __LINE__);
            MagicContextPack *nDataPack = (MagicContextPack *)starmemoryalloc(sizeof(MagicContextPack));
            nDataPack->rCallingPtr = rMagicPack;
            nDataPack->rTargetPtr = pActor;

            //--Buffer for the name. Shows MP Cost.
            char tBuffer[64];

            //--Spell is free, don't show an MP cost.
            if(rMagicPack->GetMPCost() < 1)
                sprintf(tBuffer, "%s", rMagicPack->GetName());
            //--Spell is not free, show an MP cost.
            else
                sprintf(tBuffer, "%s (%i)", rMagicPack->GetName(), rMagicPack->GetMPCost());

            //--Allocate.
            SetMemoryData(__FILE__, __LINE__);
            ContextOption *nOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
            nOption->Setup(tBuffer, MagicPack::ExecuteMagicCommand, true, nDataPack);

            //--Store it.
            pMenu->RegisterCommand(nOption);
        }

        //--Next.
        rMagicPack = (MagicPack *)MagicPack::xMagicList->AutoIterate();
    }
}
void CorrupterMenu::PopulateContextAroundItem(InventoryItem *pItem, ContextMenu *pMenu)
{
    //--Given an InventoryItem and a ContextMenu, runs the magic spells to see if they can target that item.
    //  If they can, adds them to the ContextMenu.
    if(!pItem || !pMenu) return;

    //--Iterate across the magic list.
    MagicPack *rMagicPack = (MagicPack *)MagicPack::xMagicList->PushIterator();
    while(rMagicPack)
    {
        //--Run the subroutine. If it comes back true, add it to the list.
        if(rMagicPack->AppearsOnContextItem(pItem))
        {
            //--Storage pack.
            SetMemoryData(__FILE__, __LINE__);
            MagicContextPack *nDataPack = (MagicContextPack *)starmemoryalloc(sizeof(MagicContextPack));
            nDataPack->rCallingPtr = rMagicPack;
            nDataPack->rTargetPtr = pItem;

            //--Buffer for the name. Shows MP Cost.
            char tBuffer[64];

            //--Spell is free, don't show an MP cost.
            if(rMagicPack->GetMPCost() < 1)
                sprintf(tBuffer, "%s", rMagicPack->GetName());
            //--Spell is not free, show an MP cost.
            else
                sprintf(tBuffer, "%s (%i)", rMagicPack->GetName(), rMagicPack->GetMPCost());

            //--Allocate.
            SetMemoryData(__FILE__, __LINE__);
            ContextOption *nOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
            nOption->Setup(tBuffer, MagicPack::ExecuteMagicCommand, true, nDataPack);

            //--Store it.
            pMenu->RegisterCommand(nOption);
        }

        //--Next.
        rMagicPack = (MagicPack *)MagicPack::xMagicList->AutoIterate();
    }
}
void CorrupterMenu::PopulateContextAroundContainer(WorldContainer *pContainer, ContextMenu *pMenu)
{
    //--Given a WorldContainer and a ContextMenu, runs the magic spells to see if they can target that container.
    //  If they can, adds them to the ContextMenu.
    if(!pContainer || !pMenu) return;

    //--Iterate across the magic list.
    MagicPack *rMagicPack = (MagicPack *)MagicPack::xMagicList->PushIterator();
    while(rMagicPack)
    {
        //--Run the subroutine. If it comes back true, add it to the list.
        if(rMagicPack->AppearsOnContextContainer(pContainer))
        {
            //--Storage pack.
            SetMemoryData(__FILE__, __LINE__);
            MagicContextPack *nDataPack = (MagicContextPack *)starmemoryalloc(sizeof(MagicContextPack));
            nDataPack->rCallingPtr = rMagicPack;
            nDataPack->rTargetPtr = pContainer;

            //--Buffer for the name. Shows MP Cost.
            char tBuffer[64];

            //--Spell is free, don't show an MP cost.
            if(rMagicPack->GetMPCost() < 1)
                sprintf(tBuffer, "%s", rMagicPack->GetName());
            //--Spell is not free, show an MP cost.
            else
                sprintf(tBuffer, "%s (%i)", rMagicPack->GetName(), rMagicPack->GetMPCost());

            //--Allocate.
            SetMemoryData(__FILE__, __LINE__);
            ContextOption *nOption = (ContextOption *)starmemoryalloc(sizeof(ContextOption));
            nOption->Setup(tBuffer, MagicPack::ExecuteMagicCommand, true, nDataPack);

            //--Store it.
            pMenu->RegisterCommand(nOption);
        }

        //--Next.
        rMagicPack = (MagicPack *)MagicPack::xMagicList->AutoIterate();
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void CorrupterMenu::UpdateMagicMode()
{
    //--Update for Magic Mode. Cast spells!
    ControlManager *rControlManager = ControlManager::Fetch();
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Store the previous highlight.
    int tPreviousHighlight = mHighlightedMagicIndex;

    //--Left click.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Highlight the magic spell that matches. The spell only changes if the mouse's X position
        //  is within range, as the player may want to move to the description area.
        if(tMouseX >= cMagicListingLft && tMouseX <= cMagicListingLft + cMagicListingWid)
        {
            mHighlightedMagicIndex = (tMouseY - (cMagicListingTop + CM_BTN_TEXT_SIZE)) / CM_BTN_TEXT_SIZE;
            if(tMouseY < cMagicListingTop + CM_BTN_TEXT_SIZE) mHighlightedMagicIndex = -1;
        }
        //--Alternately, check if we hit the cast button. Attempt to cast the spell if so. Casting spells via this
        //  method implicitly targets the player.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mMagicCastBtn))
        {
            MagicPack *rMagicSpell = (MagicPack *)MagicPack::xMagicList->GetElementBySlot(mHighlightedMagicIndex);
            if(rMagicSpell && rMagicSpell->CanCastNow())
            {
                SetVisibility(false);
                rMagicSpell->Cast(EntityManager::Fetch()->GetLastPlayerEntity());
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
    }

    //--Right-click will attempt to cast the spell quickly. It won't work if the spell couldn't be cast normally.
    if(rControlManager->IsFirstPress("MouseRgt") && tMouseX >= cMagicListingLft && tMouseX <= cMagicListingLft + cMagicListingWid)
    {
        //--Set the slot.
        mHighlightedMagicIndex = (tMouseY - (cMagicListingTop + CM_BTN_TEXT_SIZE)) / CM_BTN_TEXT_SIZE;
        if(tMouseY < cMagicListingTop + CM_BTN_TEXT_SIZE) mHighlightedMagicIndex = -1;

        //--Get the spell. Cast it if possible. Spells cast via this menu implicitly target the player.
        MagicPack *rMagicSpell = (MagicPack *)MagicPack::xMagicList->GetElementBySlot(mHighlightedMagicIndex);
        if(rMagicSpell && rMagicSpell->CanCastNow())
        {
            SetVisibility(false);
            rMagicSpell->Cast(EntityManager::Fetch()->GetLastPlayerEntity());
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }

    //--If the highlight changed, reset the description.
    if(tPreviousHighlight != mHighlightedMagicIndex && mHighlightedMagicIndex >= 0 && mHighlightedMagicIndex < MagicPack::xMagicList->GetListSize())
    {
        //--Description.
        MagicPack *rMagicSpell = (MagicPack *)MagicPack::xMagicList->GetElementBySlot(mHighlightedMagicIndex);
        FillSpellDescription(rMagicSpell);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--Place the Cast button.
        if(rMagicSpell && rMagicSpell->CanCastNow())
        {
            //--Figure out where the description ends.
            float tYCursor = cDescriptionListingTop + (CM_BTN_TEXT_SIZE * 2.5f);
            for(int i = 0; i < CM_PERK_UNLOCK_LINES; i ++)
            {
                if(mPerkUnlockLines[i][0] == '\0') break;
                tYCursor = tYCursor + CM_BTN_TEXT_SIZE;
            }

            //--Place the cast button slightly below that.
            tYCursor = tYCursor + (CM_BTN_TEXT_SIZE * 0.5f);
            mMagicCastBtn.SetWH(cDescriptionListingLft, tYCursor, Images.Data.rUIFont->GetTextWidth("Cast") + (CM_BTN_INDENT * 2.0f), CM_BTN_TEXT_SIZE + (CM_BTN_INDENT * 2.0f));
        }
        //--Hide the cast button.
        else
        {
            mMagicCastBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
        }
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void CorrupterMenu::RenderMagicMode()
{
    //--[Documentation]
    //--Renders the player's current spells and shows the highlighted spell's description, if applicable.
    if(!Images.mIsReady) return;

    //--Heading.
    float tYCursor = cMagicListingTop;
    Images.Data.rUIFont->DrawText(cMagicListingLft, tYCursor, 0, 1.0f, "Available Spells");
    tYCursor = tYCursor + CM_BTN_TEXT_SIZE;

    //--List of spells.
    int i = 0;
    MagicPack *rMagicPack = (MagicPack *)MagicPack::xMagicList->PushIterator();
    while(rMagicPack)
    {
        //--If this spell is selected, render a highlight.
        if(mHighlightedMagicIndex == i)
            mHighlightCol.SetAsMixer();
        //--If the player cannot afford the spell, red it out.
        else if(rMagicPack->GetMPCost() > xPlayerMP)
            mUnavailableCol.SetAsMixer();
        //--Normal case.
        else
            mNormalTextCol.SetAsMixer();

        //--Render the name.
        Images.Data.rUIFont->DrawText(cMagicListingLft + CM_BTN_INDENT, tYCursor, 0, 1.0f, rMagicPack->GetName());

        //--Next.
        i ++;
        tYCursor = tYCursor + CM_BTN_TEXT_SIZE;
        rMagicPack = (MagicPack *)MagicPack::xMagicList->AutoIterate();
    }

    //--Clean.
    StarlightColor::ClearMixer();

    //--If a spell is active, render its description.
    rMagicPack = (MagicPack *)MagicPack::xMagicList->GetElementBySlot(mHighlightedMagicIndex);
    if(rMagicPack)
    {
        //--Setup.
        float tYCursor = cDescriptionListingTop;

        //--Spell Information. Never changes formats.
        Images.Data.rUIFont->DrawText    (cDescriptionListingLft, tYCursor + (CM_BTN_TEXT_SIZE * 0.0f), 0, 1.0f, rMagicPack->GetName());

        //--Cost (Normal case)
        if(rMagicPack->GetMPCost() > 0)
        {
            Images.Data.rUIFont->DrawTextArgs(cDescriptionListingLft, tYCursor + (CM_BTN_TEXT_SIZE * 1.0f), 0, 1.0f, "Cost: %i (%i Available)", rMagicPack->GetMPCost(), xPlayerMP);
        }
        //--Special case: Indicate the spell is free.
        else
        {
            Images.Data.rUIFont->DrawTextArgs(cDescriptionListingLft, tYCursor + (CM_BTN_TEXT_SIZE * 1.0f), 0, 1.0f, "Cost: Free");
        }

        //--Cursor.
        tYCursor = tYCursor + (CM_BTN_TEXT_SIZE * 2.5f);

        //--Render each description line. These should be set by FillSpellDescription().
        for(int i = 0; i < CM_PERK_UNLOCK_LINES; i ++)
        {
            Images.Data.rUIFont->DrawText(cDescriptionListingLft, tYCursor, 0, 1.0f, mPerkUnlockLines[i]);
            tYCursor = tYCursor + CM_BTN_TEXT_SIZE;
        }

        //--If the spell can be cast now, render this button.
        if(rMagicPack->CanCastNow())
        {
            VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mMagicCastBtn, CM_BTN_INDENT, "Cast");
        }
    }
    //--Render a string instructing the player.
    else
    {
        Images.Data.rUIFont->DrawText(cDescriptionListingLft, cDescriptionListingTop + (CM_BTN_TEXT_SIZE * 0.0f), 0, 1.0f, "Click a spell to see its description.");
        Images.Data.rUIFont->DrawText(cDescriptionListingLft, cDescriptionListingTop + (CM_BTN_TEXT_SIZE * 1.0f), 0, 1.0f, "Right-click a spell to quick-cast it.");
    }
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
