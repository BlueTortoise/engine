//--Base
#include "CorrupterMenu.h"

//--Classes
#include "Actor.h"
#include "PandemoniumRoom.h"
#include "VisualLevel.h"

//--CoreClasses
#include "DataList.h"
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"

//=========================================== System ==============================================
//====================================== Property Queries =========================================
bool CorrupterMenu::IsMinionsMode()
{
    return (mCurrentMode == CM_MODE_MINIONS);
}

//========================================= Manipulators ==========================================
void CorrupterMenu::SetToMinionsMode()
{
    //--Flags.
    mCurrentMode = CM_MODE_MINIONS;
    mMinionActiveSlot = -1;

    //--Refresh the monster listing before anything displays.
    RefreshMonsterListing();
}

//========================================= Core Methods ==========================================
void CorrupterMenu::RefreshMonsterListing()
{
    //--Wipe the existing monster listing and find all monsters. Create command packs for each one.
    mMonsterListing->ClearList();

    //--Iterate across the EntityManager's list.
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Entity must be an Actor.
        if(rCheckEntity->IsOfType(POINTER_TYPE_ACTOR))
        {
            //--Cast.
            Actor *rActor = (Actor *)rCheckEntity;

            //--Check its data list for the command string. If it's "Not a Monster" or doesn't exist, ignore it.
            const char *rCheckString = (const char *)rActor->GetDataList()->FetchDataEntry("sMonsterCommand");
            if(rCheckString && strcasecmp(rCheckString, "Not a Monster"))
            {
                //--Add to the list.
                MonsterPack *nPack = MonsterPack::Create();
                nPack->rMonster = rActor;
                mMonsterListing->AddElement(rActor->GetName(), nPack, MonsterPack::DeleteThis);

                //--If an icon is present, retrieve it.
                const char *rIconPath = (const char *)rActor->GetDataList()->FetchDataEntry("sIconPath");
                if(rIconPath) nPack->rIcon = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(rIconPath);
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void CorrupterMenu::UpdateMinionsMode()
{
    //--Update for Minions mode, excluding the header. Mostly allows modification of the orders.
    ControlManager *rControlManager = ControlManager::Fetch();
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Figure out if the orders button is highlighted.
    bool tOldHighlight = mIsOrdersButtonHighlighted;
    mIsOrdersButtonHighlighted = IsPointWithin2DReal(tMouseX, tMouseY, mOrdersBtn);

    //--SFX.
    if(!tOldHighlight && mIsOrdersButtonHighlighted) AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

    //--Figure out the active slot.
    int tOldMinionSlot = mMinionActiveSlot;
    float cStartPoint = mHeaderMinionsBtn.mBot + CM_HEADER_INDENT + (CM_Y_SPACING_PER_MONSTER * 1.5f) - 8.0f;
    mMinionActiveSlot = (tMouseY - cStartPoint) / CM_Y_SPACING_PER_MONSTER;
    if(tMouseY < cStartPoint) mMinionActiveSlot = -1;

    //--SFX.
    if(tOldMinionSlot != mMinionActiveSlot && mMinionActiveSlot > -1 && mMinionActiveSlot < mMonsterListing->GetListSize()) AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

    //--Clicking on the active slot rotates the standing orders up by 1.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--[Orders Button]
        //--If highlighted, rotate the standing orders.
        if(mIsOrdersButtonHighlighted)
        {
            //--Follow->Guard
            if(!strcasecmp(xStandingOrders, "Follow"))
            {
                ResetString(xStandingOrders, "Guard");
            }
            //--Guard->Hunt
            else if(!strcasecmp(xStandingOrders, "Guard"))
            {
                ResetString(xStandingOrders, "Hunt");
            }
            //--Hunt->Destroy
            else if(!strcasecmp(xStandingOrders, "Hunt"))
            {
                ResetString(xStandingOrders, "Destroy");
            }
            //--Destroy->Follow
            else if(!strcasecmp(xStandingOrders, "Destroy"))
            {
                ResetString(xStandingOrders, "Follow");
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--[Monster Orders Changes]
        //--If a monster is highlighted, modify their orders.
        else
        {
            //--Get the monster in the slot.
            MonsterPack *rMonsterPack = (MonsterPack *)mMonsterListing->GetElementBySlot(mMinionActiveSlot);
            if(!rMonsterPack || !rMonsterPack->rMonster) return;

            //--Get the string responsible for their current command, and modify it. It must exist!
            DataList *rMonsterDataList = rMonsterPack->rMonster->GetDataList();
            char *rCheckString = (char *)rMonsterDataList->FetchDataEntry("sMonsterCommand");
            if(!rCheckString) return;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");

            //--Depending on what the string is, change it.
            if(!strcasecmp(rCheckString, "Standing Orders"))
            {
                rMonsterDataList->AddDataEntryS("sMonsterCommand", "Follow");
            }
            //--Order to defend their current position.
            else if(!strcasecmp(rCheckString, "Follow"))
            {
                rMonsterDataList->AddDataEntryS("sMonsterCommand", "Guard");
            }
            //--Find the humans!
            else if(!strcasecmp(rCheckString, "Guard"))
            {
                rMonsterDataList->AddDataEntryS("sMonsterCommand", "Hunt");
            }
            //--Destroy items.
            else if(!strcasecmp(rCheckString, "Hunt"))
            {
                rMonsterDataList->AddDataEntryS("sMonsterCommand", "Destroy");
            }
            //--Cycle back to standing orders.
            else if(!strcasecmp(rCheckString, "Destroy"))
            {
                rMonsterDataList->AddDataEntryS("sMonsterCommand", "Standing Orders");
            }
        }
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void CorrupterMenu::RenderMinionsMode()
{
    //--[Documentation]
    //--Renders minions information, consisting of their names, HP/WP, location, and current orders.
    if(!Images.mIsReady) return;

    //--Setup.
    float tYCursor = mHeaderMinionsBtn.mBot + CM_HEADER_INDENT;

    //--[Heading Information]
    //--Labels above everything.
    Images.Data.rUIFont->DrawText(cXPosName, tYCursor, 0, 1.0f, "Name");

    //--HP.
    Images.Data.rUIFont->DrawText(cXPosHP, tYCursor, 0, 1.0f, "HP");

    //--WP.
    Images.Data.rUIFont->DrawText(cXPosWP, tYCursor, 0, 1.0f, "WP");

    //--Current location.
    Images.Data.rUIFont->DrawText(cXPosLocation, tYCursor, 0, 1.0f, "Location");

    //--Current orders.
    Images.Data.rUIFont->DrawText(cXPosOrders, tYCursor, 0, 1.0f, "Orders");

    //--[Standing Orders Button]
    //--Render a description and the standing orders.
    if(xStandingOrders)
    {
        //--If highlighted, render an underlay.
        if(mIsOrdersButtonHighlighted)
        {
            glColor3f(0.75f, 0.75f, 0.75f);
            glDisable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
                glVertex2f(mOrdersBtn.mLft, mOrdersBtn.mTop);
                glVertex2f(mOrdersBtn.mRgt, mOrdersBtn.mTop);
                glVertex2f(mOrdersBtn.mRgt, mOrdersBtn.mBot);
                glVertex2f(mOrdersBtn.mLft, mOrdersBtn.mBot);
            glEnd();
            glEnable(GL_TEXTURE_2D);
            glColor3f(1.0f, 1.0f, 1.0f);
        }

        //--Render the string.
        Images.Data.rUIFont->DrawTextArgs(mOrdersBtn.mLft + CM_BTN_INDENT, mOrdersBtn.mTop + CM_BTN_INDENT, 0, 1.0f, "Current Standing Orders: %s", xStandingOrders);
    }

    //--[Monster Information]
    //--Move the cursor down 1.5 entries.
    tYCursor = tYCursor + (CM_Y_SPACING_PER_MONSTER * 1.5f);

    //--For each minion...
    int i = 0;
    MonsterPack *rMonsterPack = (MonsterPack *)mMonsterListing->PushIterator();
    while(rMonsterPack)
    {

        //--Render the icon, if it exists.
        if(rMonsterPack->rIcon)
        {
            rMonsterPack->rIcon->Draw(cXPosIcon, tYCursor - 8.0f);
        }

        //--Render the information.
        if(rMonsterPack->rMonster)
        {
            //--Highlight color.
            if(mMinionActiveSlot == i)
                mHighlightCol.SetAsMixer();
            else
                mNormalTextCol.SetAsMixer();

            //--Name.
            SugarFont::xScaleAffectsY = false;
            Images.Data.rUIFont->DrawText(cXPosName, tYCursor, 0, 0.8f, rMonsterPack->rMonster->GetName());
            SugarFont::xScaleAffectsY = true;

            //--HP.
            Images.Data.rUIFont->DrawTextArgs(cXPosHP, tYCursor, 0, 1.0f, "%i/%i", rMonsterPack->rMonster->GetCombatStatistics().mHP, rMonsterPack->rMonster->GetCombatStatistics().mHPMax);

            //--WP.
            Images.Data.rUIFont->DrawTextArgs(cXPosWP, tYCursor, 0, 1.0f, "%i/%i", rMonsterPack->rMonster->GetCombatStatistics().mWillPower, rMonsterPack->rMonster->GetCombatStatistics().mWillPowerMax);

            //--Current location.
            PandemoniumRoom *rCurrentRoom = rMonsterPack->rMonster->GetCurrentRoom();
            if(rCurrentRoom)
            {
                SugarFont::xScaleAffectsY = false;
                Images.Data.rUIFont->DrawTextArgs(cXPosLocation, tYCursor, 0, 0.75f, "%s", rCurrentRoom->GetName());
                SugarFont::xScaleAffectsY = true;
            }

            //--Current orders.
            const char *rCheckString = (const char *)rMonsterPack->rMonster->GetDataList()->FetchDataEntry("sMonsterCommand");
            if(rCheckString)
            {
                Images.Data.rUIFont->DrawTextArgs(cXPosOrders, tYCursor, 0, 1.0f, "%s", rCheckString);
            }

            //--Clean.
            StarlightColor::ClearMixer();
        }

        //--Next.
        i ++;
        tYCursor = tYCursor + CM_Y_SPACING_PER_MONSTER;
        rMonsterPack = (MonsterPack *)mMonsterListing->AutoIterate();
    }

    //--Clean.
    StarlightColor::ClearMixer();
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
