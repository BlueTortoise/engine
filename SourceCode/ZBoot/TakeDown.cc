//--Base
#include "TakeDown.h"

//--Classes
//--CoreClases
#include "LoadInterrupt.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "CameraManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"
#include "MapManager.h"
#include "NetworkManager.h"
#include "SaveManager.h"
#include "SugarLumpManager.h"
#include "SteamManager.h"
#include "TransitionManager.h"

void Free_All_Memory()
{
    //--Deletes everything and deallocates all memory in use by the program.
    float tStartTime, tEndTime;

    //--[Debug]
    GLOBAL *rGlobal = Global::Shared();
    DebugManager::PushPrint(true, "[Cleaning Memory] Begin\n");

    //--Mark the error log that the execution is completing normally.
    time_t tRawTime;
    struct tm *tTimeInfo;
    time (&tRawTime);
    tTimeInfo = localtime(&tRawTime);
    FILE *fOutfile = fopen("ErrorLog.txt", "a");
    fprintf(fOutfile, "%s - Program execution completing, deallocating memory.\n", asctime(tTimeInfo));
    fclose(fOutfile);

    //--Purge any outstanding events.
    #if defined _ALLEGRO_PROJECT_
    while(al_drop_next_event(rGlobal->gEventQueue))
    {
    }
    #endif

    //--Store the starting time.
    tStartTime = GetGameTime();

    //--[Statics]
    //--Note: The fonts are not deleted, they are in the DataLibrary and should not be double-dealloc'd.

    //--[Managers]
    //--External
    DebugManager::Print("Managers:\n");
    DebugManager::Print(" Lua Manager\n");
    delete rGlobal->gLuaManager;
    DebugManager::Print(" Network Manager\n");
    delete rGlobal->gNetworkManager;
    DebugManager::Print(" Save Manager\n");
    delete rGlobal->gSaveManager;
    DebugManager::Print(" SugarLump Manager\n");
    delete rGlobal->gSugarLumpManager;

    //--Game
    DebugManager::Print(" Entity Manager\n");
    delete rGlobal->gEntityManager;
    DebugManager::Print(" Map Manager\n");
    delete rGlobal->gMapManager;

    //--System
    DebugManager::Print(" Audio Manager\n");
    delete rGlobal->gAudioManager;
    DebugManager::Print(" Camera Manager\n");
    delete rGlobal->gCameraManager;
    DebugManager::Print(" Control Manager\n");
    delete rGlobal->gControlManager;
    DebugManager::Print(" Display Manager\n");
    delete rGlobal->gDisplayManager;
    DebugManager::Print(" Options Manager\n");
    delete rGlobal->gOptionsManager;
    DebugManager::Print(" Transition Manager\n");
    delete rGlobal->gTransitionManager;
    DebugManager::Print(" Steam Manager\n");
    delete rGlobal->gSteamManager;

    //--Libraries
    DebugManager::Print("Data Library\n");
    delete rGlobal->gDataLibrary;

    //--[Allegro Destruction]
    #if defined _ALLEGRO_PROJECT_
        DebugManager::Print("Allegro Globals\n");
        DebugManager::Print(" Speed Timer\n");
        al_destroy_timer(rGlobal->gSpeedTimer);
        DebugManager::Print(" Event Queue\n");
        al_destroy_event_queue(rGlobal->gEventQueue);
        DebugManager::Print(" Display Hook\n");
        al_destroy_display(rGlobal->gDisplay);
    #endif


    //--Compute time spent.
    tEndTime = GetGameTime();
    DebugManager::PopPrint("Destruction complete, took %f seconds.\n", tEndTime - tStartTime);
}
