//--Base
#include "Startup.h"

//--Classes
#include "RootEvent.h"
#include "RootObject.h"
#include "Actor.h"
#include "ActorEvent.h"
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatAnimation.h"
#include "AdvCombatEffect.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "AdventureDebug.h"
#include "AdventureLevel.h"
#include "AdventureLevelGenerator.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "AdventureMenu.h"
#include "AliasStorage.h"
#include "AudioEvent.h"
#include "AudioPackage.h"
#include "BlueSphereLevel.h"
#include "BuildPackage.h"
#include "CameraEvent.h"
#include "CorrupterMenu.h"
#include "DialogueActor.h"
#include "FieldAbility.h"
#include "LuaContextOption.h"
#include "InventoryItem.h"
#include "GalleryMenu.h"
#include "KPopDanceGame.h"
#include "MagicPack.h"
#include "SugarCamera2D.h"
#include "PerkPack.h"
#include "TilemapActor.h"
#include "TransformPack.h"
#include "PairanormalMenu.h"
#include "PairanormalLevel.h"
#include "PairanormalDialogue.h"
#include "PairanormalDialogueCharacter.h"
#include "PlayerPony.h"
#include "PandemoniumLevel.h"
#include "PandemoniumRoom.h"
#include "RootEffect.h"
#include "StringTyrantTitle.h"
#include "FlexMenu.h"
#include "FlexButton.h"
#include "WorldContainer.h"
#include "WorldDialogue.h"
#include "VisualLevel.h"
#include "VisualRoom.h"
#include "TimerEvent.h"
#include "TextLevel.h"

//--CoreClases
#include "SugarAnimation.h"
#include "SugarBitmap.h"
#include "SugarButton.h"
#include "SugarFont.h"
#include "DataList.h"
#include "LoadInterrupt.h"
#include "SugarFileSystem.h"
#include "SugarLinkedList.h"
#include "SugarTags.h"

//--Definitions
#include "DebugDefinitions.h"
#include "GameGear.h"
#include "Global.h"
#include "GlDfn.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "CameraManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"
#include "MapManager.h"
#include "NetworkManager.h"
#include "ResetManager.h"
#include "SaveManager.h"
#include "SteamManager.h"
#include "SugarLumpManager.h"
#include "TransitionManager.h"

void RegisterLuaFunctions(lua_State *pLuaState)
{
    //--Called after the LuaManager is created, registers every function in the program to its
    //  Lua state.  Lua scripts should not execute before this.

    //--[Classes]
    //--System
    RootEffect::HookToLuaState(pLuaState);
    RootEntity::HookToLuaState(pLuaState);
    RootEvent::HookToLuaState(pLuaState);
    RootObject::HookToLuaState(pLuaState);

    //--Everything Else
    Actor::HookToLuaState(pLuaState);
    ActorEvent::HookToLuaState(pLuaState);
    AdvCombat::HookToLuaState(pLuaState);
    AdvCombatAbility::HookToLuaState(pLuaState);
    AdvCombatAnimation::HookToLuaState(pLuaState);
    AdvCombatEffect::HookToLuaState(pLuaState);
    AdvCombatEntity::HookToLuaState(pLuaState);
    AdvCombatJob::HookToLuaState(pLuaState);
    AdventureDebug::HookToLuaState(pLuaState);
    AdventureInventory::HookToLuaState(pLuaState);
    AdventureItem::HookToLuaState(pLuaState);
    AdventureLevel::HookToLuaState(pLuaState);
    AdventureLevelGenerator::HookToLuaState(pLuaState);
    AdventureMenu::HookToLuaState(pLuaState);
    AliasStorage::HookToLuaState(pLuaState);
    AudioEvent::HookToLuaState(pLuaState);
    BlueSphereLevel::HookToLuaState(pLuaState);
    BuildPackage::HookToLuaState(pLuaState);
    CameraEvent::HookToLuaState(pLuaState);
    CorrupterMenu::HookToLuaState(pLuaState);
    DialogueActor::HookToLuaState(pLuaState);
    FieldAbility::HookToLuaState(pLuaState);
    FlexMenu::HookToLuaState(pLuaState);
    FlexButton::HookToLuaState(pLuaState);
    GalleryMenu::HookToLuaState(pLuaState);
    InventoryItem::HookToLuaState(pLuaState);
    KPopDanceGame::HookToLuaState(pLuaState);
    LuaContextOption::HookToLuaState(pLuaState);
    MagicPack::HookToLuaState(pLuaState);
    PairanormalDialogue::HookToLuaState(pLuaState);
    PairanormalDialogueCharacter::HookToLuaState(pLuaState);
    PairanormalLevel::HookToLuaState(pLuaState);
    PairanormalMenu::HookToLuaState(pLuaState);
    PandemoniumLevel::HookToLuaState(pLuaState);
    PandemoniumRoom::HookToLuaState(pLuaState);
    PerkPack::HookToLuaState(pLuaState);
    StringTyrantTitle::HookToLuaState(pLuaState);
    SugarTag::HookToLuaState(pLuaState);
    TextLevel::HookToLuaState(pLuaState);
    TimerEvent::HookToLuaState(pLuaState);
    TransformPack::HookToLuaState(pLuaState);
    TilemapActor::HookToLuaState(pLuaState);
    VisualLevel::HookToLuaState(pLuaState);
    VisualRoom::HookToLuaState(pLuaState);
    WorldContainer::HookToLuaState(pLuaState);
    WorldDialogue::HookToLuaState(pLuaState);

    //--[Core Classes]
    SugarAnimation::HookToLuaState(pLuaState);
    SugarButton::HookToLuaState(pLuaState);
    SugarBitmap::HookToLuaState(pLuaState);
    SugarCamera2D::HookToLuaState(pLuaState);
    SugarFont::HookToLuaState(pLuaState);
    SugarFileSystem::HookToLuaState(pLuaState);
    LoadInterrupt::HookToLuaState(pLuaState);

    //--[Libraries]
    DataLibrary::HookToLuaState(pLuaState);

    //--[Managers]
    AudioManager::HookToLuaState(pLuaState);
    AudioPackage::HookToLuaState(pLuaState);
    CameraManager::HookToLuaState(pLuaState);
    ControlManager::HookToLuaState(pLuaState);
    CutsceneManager::HookToLuaState(pLuaState);
    DebugManager::HookToLuaState(pLuaState);
    DisplayManager::HookToLuaState(pLuaState);
    EntityManager::HookToLuaState(pLuaState);
    MapManager::HookToLuaState(pLuaState);
    OptionsManager::HookToLuaState(pLuaState);
    ResetManager::HookToLuaState(pLuaState);
    SaveManager::HookToLuaState(pLuaState);
    SteamManager::HookToLuaState(pLuaState);
    SugarLumpManager::HookToLuaState(pLuaState);
    TransitionManager::HookToLuaState(pLuaState);

    //--[Player]
    PlayerPony::HookToLuaState(pLuaState);
}
void RegisterConsoleFunctions()
{
    //--Much like registering Lua functions, this registers functions which can be called through
    //  the console.  It does not require the OM to be setup, the list is static.
    LuaManager::RegisterCommands();
    SugarCamera2D::RegisterCommands();
}
