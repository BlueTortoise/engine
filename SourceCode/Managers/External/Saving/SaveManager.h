//--[SaveManager]
//--Class which handles saving and loading in this project. Note that we cannot store the objects
//  in a linked-list implementation unless the objects preserve the ISaveable state. This is due
//  to a bug in multi-inheritance which causes casts from void* to crash.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--[Forward Declarations]
struct LoadingPack;

//--[Local Definitions]
#define SAVELIST_SIZE 2

#define SAVE_HEADER "Sugar Savefile 1.0"

class SaveManager
{
    private:
    //--System
    char *mSavegameName;
    char *mSavegamePath;

    //--Storage
    ISaveable *mSaveableObjects[SAVELIST_SIZE];

    protected:

    public:
    //--System
    SaveManager();
    ~SaveManager();

    //--Public Variables
    static char *xSaveAssemblerPath;
    static bool xRunBackupSave;

    //--Property Queries
    const char *GetSavegameName();
    const char *GetSavegamePath();

    //--Manipulators
    void SetSavegameName(const char *pName);
    void SetSavegamePath(const char *pPath);
    void RegisterObject(ISaveable *pObject);

    //--Core Methods
    void AssembleSaveList();

    //--Adventure Mode
    LoadingPack *GetSaveInfo(const char *pFilePath);
    void SaveAdventureFile(const char *pFilePath);
    void SaveControlsFile();
    void LoadFrom(const char *pPath);

    //--Pairanormal
    void SavePairanormalFile(const char *pPath);
    void LoadPairanormalFile(const char *pPath);
    void SavePairanormalGallery(const char *pPath);
    void LoadPairanormalGallery(const char *pPath);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    void SaveTo(const char *pPath);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static SaveManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Save_SetSaveHandlerPath(lua_State *L);
int Hook_Save_ExecuteAdventureLoad(lua_State *L);
int Hook_Save_WritePairanormalGalleryData(lua_State *L);
int Hook_Save_ReadPairanormalGalleryData(lua_State *L);
