//--Base
#include "SaveManager.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
#include "ISaveable.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "MapManager.h"
#include "SugarLumpManager.h"

//=========================================== System ==============================================
SaveManager::SaveManager()
{
    //--[SaveManager]
    //--System
    mSavegameName = InitializeString("Sample Game");
    mSavegamePath = InitializeString("Saves/Sample.slf");

    //--Storage
    for(int i = 0; i < SAVELIST_SIZE; i ++) mSaveableObjects[i] = NULL;
}
SaveManager::~SaveManager()
{
    free(mSavegameName);
    free(mSavegamePath);
}

//--[Public Statics]
//--Specifies which script to call before assembling variables for export.
char *SaveManager::xSaveAssemblerPath = NULL;

//--Create save backups when playing Pairanormal. Gets set to false and then true when writing a backup
//  save, but if booted to false, backups are never created.
bool SaveManager::xRunBackupSave = true;

//====================================== Property Queries =========================================
const char *SaveManager::GetSavegameName()
{
    return (const char *)mSavegameName;
}
const char *SaveManager::GetSavegamePath()
{
    return (const char *)mSavegamePath;
}

//========================================= Manipulators ==========================================
void SaveManager::SetSavegameName(const char *pName)
{
    if(!pName) return;
    ResetString(mSavegameName, pName);
}
void SaveManager::SetSavegamePath(const char *pPath)
{
    if(!pPath) return;
    ResetString(mSavegamePath, pPath);
}

//========================================= Core Methods ==========================================
void SaveManager::AssembleSaveList()
{
    //--Called at program startup, the SaveManager assembles a list of saveable objects and stores
    //  them. When it's time to save, the list is run against a file, and the results are output.
    //--Because Pandemonium has a more complex saving process, this is not used.

    //--EXAMPLE
    //mSaveableObjects[0] = MapManager::Fetch();
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
void SaveManager::SaveTo(const char *pPath)
{
    //--Saves to the given file. This is not used in this project, it is here for demonstration.
    SugarLumpManager *rSLM = SugarLumpManager::Fetch();
    rSLM->Clear();

    //--Run through the list. They add their lumps as they go.
    for(int i = 0; i < SAVELIST_SIZE; i ++)
    {
        if(mSaveableObjects[i]) mSaveableObjects[i]->SaveToFile();
    }

    //--Merge the lumps.
    rSLM->MergeLumps();

    //--With the lump list built, write it.
    rSLM->Write(pPath);
}

//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
SaveManager *SaveManager::Fetch()
{
    return Global::Shared()->gSaveManager;
}

//========================================= Lua Hooking ===========================================
void SaveManager::HookToLuaState(lua_State *pLuaState)
{
    /* Save_SetSaveHandlerPath(sLuaPath)
       Sets which script file is run to assemble variables before a save operation. */
    lua_register(pLuaState, "Save_SetSaveHandlerPath", &Hook_Save_SetSaveHandlerPath);

    /* Save_ExecuteAdventureLoad(sSavePath)
       Runs the loading sequence on the given file. It should be a STARv200 file. */
    lua_register(pLuaState, "Save_ExecuteAdventureLoad", &Hook_Save_ExecuteAdventureLoad);

    /* Save_WritePairanormalGalleryData(sSavePath)
       Writes gallery information for Pairanormal, with gallery variables being stored in the DataLibrary
       in the heading "Root/Variables/Gallery/All/". */
    lua_register(pLuaState, "Save_WritePairanormalGalleryData", &Hook_Save_WritePairanormalGalleryData);

    /* Save_ReadPairanormalGalleryData(sSavePath)
       Deliberately reads the galary data for Pairanormal. This is also implicitly used on the main menu
       when the gallery is loaded. */
    lua_register(pLuaState, "Save_ReadPairanormalGalleryData", &Hook_Save_ReadPairanormalGalleryData);

}
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_Save_SetSaveHandlerPath(lua_State *L)
{
    //Save_SetSaveHandlerPath(sLuaPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Save_SetSaveHandlerPath");

    //--Run it.
    ResetString(SaveManager::xSaveAssemblerPath, lua_tostring(L, 1));
    return 0;
}
int Hook_Save_ExecuteAdventureLoad(lua_State *L)
{
    //Save_ExecuteAdventureLoad(sSavePath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Save_ExecuteAdventureLoad");

    //--Run it.
    SaveManager::Fetch()->LoadFrom(lua_tostring(L, 1));
    return 0;
}
int Hook_Save_WritePairanormalGalleryData(lua_State *L)
{
    //Save_WritePairanormalGalleryData(sSavePath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Save_WritePairanormalGalleryData");

    //--Run it.
    SaveManager::Fetch()->SavePairanormalGallery(lua_tostring(L, 1));
    return 0;
}
int Hook_Save_ReadPairanormalGalleryData(lua_State *L)
{
    //Save_ReadPairanormalGalleryData(sSavePath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Save_ReadPairanormalGalleryData");

    //--Run it.
    SaveManager::Fetch()->LoadPairanormalGallery(lua_tostring(L, 1));
    return 0;
}
