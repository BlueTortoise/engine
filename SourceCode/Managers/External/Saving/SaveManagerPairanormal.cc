//--Base
#include "SaveManager.h"

//--Classes
#include "PairanormalDialogue.h"
#include "PairanormalLevel.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "DebugManager.h"

//--[Debug]
//#define PSAVE_DEBUG
#ifdef PSAVE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--[Worker Functions]
bool CheckHeader(VirtualFile *fInfile, const char *pComparison);

void SaveManager::SavePairanormalFile(const char *pPath)
{
    //--Given a path, writes the Pairanormal data to the file in question.
    if(!pPath) return;

    //--Debug.
    DebugPush(true, "SaveManager:SavePairanormalFile - Begin, path is %s\n", pPath);

    //--[Player's Name]
    //--Put the player's name in the DataLibrary.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rPlayerNameVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/System/Player/Playername");
    if(!rPlayerNameVar)
    {
        //--Create the variable.
        rPlayerNameVar = (SysVar *)malloc(sizeof(SysVar));
        rPlayerNameVar->mIsSaved = true;
        rPlayerNameVar->mNumeric = 0.0f;
        rPlayerNameVar->mAlpha = InitializeString("%s", PairanormalDialogue::Fetch()->GetPlayerName());

        //--Add the path.
        rDataLibrary->AddPath("Root/Variables/System/Player/");
        rDataLibrary->RegisterPointer("Root/Variables/System/Player/Playername", rPlayerNameVar, &DataLibrary::DeleteSysVar);
    }
    //--Update.
    else
    {
        ResetString(rPlayerNameVar->mAlpha, PairanormalDialogue::Fetch()->GetPlayerName());
    }

    //--[Play Time]
    //--Store play time, in ticks.
    SysVar *rPlayTimeVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/System/Player/PlayTime");
    if(!rPlayTimeVar)
    {
        //--Create the variable.
        rPlayTimeVar = (SysVar *)malloc(sizeof(SysVar));
        rPlayTimeVar->mIsSaved = true;
        rPlayTimeVar->mNumeric = PairanormalLevel::xPlayTime;
        rPlayTimeVar->mAlpha = NULL;

        //--Add the path.
        rDataLibrary->AddPath("Root/Variables/System/Player/");
        rDataLibrary->RegisterPointer("Root/Variables/System/Player/PlayTime", rPlayTimeVar, &DataLibrary::DeleteSysVar);
    }
    //--Update.
    else
    {
        rPlayTimeVar->mNumeric = PairanormalLevel::xPlayTime;
    }

    //--[Last Used Background]
    //--Store the last used background.
    SysVar *rLastBackground = (SysVar *)rDataLibrary->GetEntry("Root/Variables/System/Player/LastBackground");
    if(!rLastBackground)
    {
        //--Create the variable.
        rLastBackground = (SysVar *)malloc(sizeof(SysVar));
        rLastBackground->mIsSaved = true;
        rLastBackground->mNumeric = 0.0f;
        rLastBackground->mAlpha = InitializeString(PairanormalLevel::Fetch()->GetLastBackgroundPath());

        //--Add the path.
        rDataLibrary->AddPath("Root/Variables/System/Player/");
        rDataLibrary->RegisterPointer("Root/Variables/System/Player/LastBackground", rLastBackground, &DataLibrary::DeleteSysVar);
    }
    //--Update.
    else
    {
        ResetString(rLastBackground->mAlpha, PairanormalLevel::Fetch()->GetLastBackgroundPath());
    }

    //--[File Handling]
    //--Open the file, check it.
    FILE *fOutfile = fopen(pPath, "wb");
    if(!fOutfile)
    {
        DebugPop("Failed, file could not be opened.\n");
        return;
    }

    //--Create an auto-buffer to store the writes. It is faster to write the data to the hard drive
    //  in one stream rather than piecemeal. This gets the m-master letter since it's so vital.
    SugarAutoBuffer *mOutBuffer = new SugarAutoBuffer();

    //--[Header]
    //--Write the four-byte header.
    mOutBuffer->AppendStringWithoutNull("STAR");

    //--Write the saving version.
    mOutBuffer->AppendStringWithoutNull("v200");

    //--[Loading Info]
    //--Header.
    mOutBuffer->AppendStringWithoutNull("LOADINFO_");

    //--Current Checkpoint.
    PairanormalLevel *rActiveLevel = PairanormalLevel::Fetch();
    const char *rCurrentCheckpointName = rActiveLevel->GetCheckpointName();
    const char *rCurrentCheckpointScript = rActiveLevel->GetCheckpointScript();
    const char *rCurrentCheckpointArg = rActiveLevel->GetCheckpointArg();
    mOutBuffer->AppendStringWithLen(rCurrentCheckpointName);
    mOutBuffer->AppendStringWithLen(rCurrentCheckpointScript);
    mOutBuffer->AppendStringWithLen(rCurrentCheckpointArg);

    //--Before writing out the datalibrary, we should composite which characters are in the field.
    char *tCharsInField = PairanormalDialogue::Fetch()->ComposeCharacterList();
    SysVar *rCharsOnField = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Player/VisibleChars");
    if(rCharsOnField)
    {
        ResetString(rCharsOnField->mAlpha, tCharsInField);
    }
    free(tCharsInField);

    //--[Data Library]
    //--Header for the script variables.
    mOutBuffer->AppendStringWithoutNull("SCRIPTVARS_");
    rDataLibrary->WriteToBuffer(mOutBuffer);

    //--[Finish Up]
    //--Liberate the data.
    int mDataLen = mOutBuffer->GetCursor();
    uint8_t *mDataPtr = mOutBuffer->LiberateData();

    //--Write the buffer to the file.
    fwrite(mDataPtr, mDataLen, sizeof(uint8_t), fOutfile);

    //--Close the file, debug write.
    fclose(fOutfile);
    delete mOutBuffer;
    DebugPop("SaveManager:SavePairanormalFile - Completed normally.\n");

    //--[Backup Save]
    //--Make backup saves.
    if(xRunBackupSave)
    {
        //--Toggle the flag off to prevent infinite loop.
        xRunBackupSave = false;

        //--We need to make sure we have a path to operate from.
        const char *rPairanormalPath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sPairanormalPath");

        //--Modify the path to be in the backups folder.
        char tBackupPath[128];
        sprintf(tBackupPath, "%s/../../SaveBackups/", rPairanormalPath);

        //--Get the file name out of the original save path.
        int tEnd = (int)strlen(pPath) - 1;
        int tCursor = (int)strlen(pPath) - 1;
        while(tCursor > 1)
        {
            if(pPath[tCursor] == '.')
            {
                tEnd = tCursor-1;
            }
            if(pPath[tCursor] == '/' || pPath[tCursor] == '\\')
            {
                break;
            }

            tCursor --;
        }

        //--Append.
        strncat(tBackupPath, &pPath[tCursor+1], tEnd-tCursor);
        //fprintf(stderr, "File name is %s\n", tBackupPath);
        strcat(tBackupPath, "Backup");

        //--Iterate and locate the first non-existing backup, up to 99. Delete the next backup
        //  in the list if it exists. If we hit 99, go back to 0 and delete the 1th backup.
        char tCheckBuf[128];
        int tUseSlot = 0;
        while(tUseSlot < 10)
        {
            //--Build.
            sprintf(tCheckBuf, "%s%02i.slf", tBackupPath, tUseSlot);

            //--Check for existence.
            //fprintf(stderr, "Checking for existence: %s\n", tCheckBuf);
            FILE *fCheckFile = fopen(tCheckBuf, "r");
            if(fCheckFile)
            {
                fclose(fCheckFile);
                tUseSlot ++;
            }
            //--Doesn't exist.
            else
            {
                break;
            }
        }

        //--If the slot reached 100, roll back to 0.
        if(tUseSlot >= 10) tUseSlot = 0;
        int tNextSlot = (tUseSlot + 1) % 10;

        //--Save to this position.
        sprintf(tCheckBuf, "%s%02i.slf", tBackupPath, tUseSlot);
        //fprintf(stderr, "Autosaving to %s\n", tCheckBuf);
        SavePairanormalFile(tCheckBuf);

        //--Delete the next position up, wrapping to zero.
        sprintf(tCheckBuf, "%s%02i.slf", tBackupPath, tNextSlot);
        //fprintf(stderr, "Deleting %s\n", tCheckBuf);
        remove(tCheckBuf);

        //--Flip the flag back on.
        xRunBackupSave = true;
    }
}
void SaveManager::LoadPairanormalFile(const char *pPath)
{
    //--[Documentation and Setup]
    //--Saves the current game state to the given file. Stores checkpoint data and variable data.
    if(!pPath) return;

    //--Debug.
    DebugPush(true, "SaveManager:LoadPairanormalFile - Begin, path is %s\n", pPath);

    //--[Basic File Checking]
    //--Open the file, check it. Do this is a StarlightVirtualFile to speed up reading.
    VirtualFile *fInfile = new VirtualFile(pPath, false);
    if(!fInfile->IsReady())
    {
        DebugPop("Failed, file could not be opened.\n");
        return;
    }

    //--Starv200 files use 2 bytes for string lengths.
    fInfile->SetUseOneByteForStringLengths(false);

    //--Make sure this is a "STARv200" file.
    char tInBuffer[32];
    memset(tInBuffer, 0, sizeof(char) * 32);
    fInfile->Read(tInBuffer, sizeof(char), (int)strlen("STARv200"));

    //--If this is a STARv200 header, everything is fine.
    if(!strcasecmp(tInBuffer, "STARv200"))
    {
    }
    //--Error, incorrect file type.
    else
    {
        DebugPop("Failed, file is not a STARv200 file.\n");
        return;
    }

    //--[Loading Info]
    //--This information tells us what checkpoint data to use.
    if(!CheckHeader(fInfile, "LOADINFO_"))
    {
        DebugPop("Failed, expected LOADINFO_ lump.\n");
        return;
    }

    //--Read the checkpoint data.
    char *tCheckpointName = fInfile->ReadLenString();
    char *tCheckpointScript = fInfile->ReadLenString();
    char *tCheckpointArg = fInfile->ReadLenString();

    //--Send the checkpoint data to the PairanormalLevel.
    PairanormalLevel *rLevel = PairanormalLevel::Fetch();
    rLevel->SetCheckpoint(tCheckpointName, tCheckpointScript, tCheckpointArg);
    free(tCheckpointName);
    free(tCheckpointScript);
    free(tCheckpointArg);

    //--[Script Variables]
    //--Check header.
    if(!CheckHeader(fInfile, "SCRIPTVARS_"))
    {
        DebugPop("Failed, script variable info not found.\n");
        return;
    }

    //--Run.
    DataLibrary::Fetch()->ReadFromFile(fInfile);

    //--Copy across the player's name.
    SysVar *rPlayerNameVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Player/Playername");
    if(rPlayerNameVar)
    {
        PairanormalDialogue::Fetch()->SetPlayerName(rPlayerNameVar->mAlpha);
    }
    else
    {
        PairanormalDialogue::Fetch()->SetPlayerName("Jay");
    }

    //--Music.
    SysVar *rLastMusicVar = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Player/LastMusic");
    if(rLastMusicVar && rLastMusicVar->mAlpha && strcasecmp(rLastMusicVar->mAlpha, "No Music"))
    {
        AudioManager::Fetch()->PlayMusic(rLastMusicVar->mAlpha);
    }

    //--Last background.
    SysVar *rLastBackground = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Player/LastBackground");
    if(rLastBackground)
    {
        PairanormalLevel::Fetch()->SetBackground(rLastBackground->mAlpha);
    }

    //--Visible character listing.
    SysVar *rCharsOnField = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Player/VisibleChars");
    if(rCharsOnField)
    {
        PairanormalDialogue::Fetch()->ShowCharacterBySet(rCharsOnField->mAlpha);
    }

    //--[Finish up]
    delete fInfile;
    DebugPop("SaveManager:LoadPairanormalFile - Completed normally.\n");
}

//--[Gallery Handlers]
void SaveManager::SavePairanormalGallery(const char *pPath)
{
    //--Writes the gallery info block to the named path.
    if(!pPath) return;

    //--Go into the DataLibrary and find the list for "Root/Variables/Gallery/All/". Write all of these to the savefile.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SugarLinkedList *rGalleryVarList = rDataLibrary->GetHeading("Root/Variables/Gallery/All/");
    if(!rGalleryVarList) return;

    //--Open the output file.
    FILE *fOutfile = fopen(pPath, "wb");
    if(!fOutfile)
    {
        return;
    }

    //--Write a header.
    fprintf(fOutfile, "Gallery Data v100");

    //--Write how many entries are expected.
    uint32_t tEntries = rGalleryVarList->GetListSize();
    fwrite(&tEntries, sizeof(uint32_t), 1, fOutfile);

    //--Now write length strings for each entry in the data library heading.
    SysVar *rSysVar = (SysVar *)rGalleryVarList->PushIterator();
    while(rSysVar)
    {
        //--Write the name's length.
        const char *rName = rGalleryVarList->GetIteratorName();
        uint16_t tLen = (int16_t)strlen(rName);
        fwrite(&tLen, sizeof(int16_t), 1, fOutfile);

        //--Write the name itself.
        fwrite(rName, sizeof(char), tLen, fOutfile);

        //--Next.
        rSysVar = (SysVar *)rGalleryVarList->AutoIterate();
    }

    //--Clean up.
    fclose(fOutfile);
}
void SaveManager::LoadPairanormalGallery(const char *pPath)
{
    //--Loads gallery variables and stores them in the DataLibrary.
    if(!pPath) return;

    //--Get the DataLibrary.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->AddPath("Root/Variables/Gallery/All/");

    //--Open the output file.
    FILE *fInfile = fopen(pPath, "rb");
    if(!fInfile)
    {
        return;
    }

    //--Check the header.
    char tBuffer[32];
    fread(tBuffer, sizeof(char), 17, fInfile);
    tBuffer[17] = '\0';
    if(strcasecmp(tBuffer, "Gallery Data v100")) return;

    //--Read how many entries are expected.
    uint32_t tEntries = 0;
    fread(&tEntries, sizeof(uint32_t), 1, fInfile);
    if(tEntries > 100) return;

    //--Begin reading.
    char tVariableName[256];
    for(int i = 0; i < (int)tEntries; i ++)
    {
        //--Read the name's length.
        int16_t tLen = 0;
        fread(&tLen, sizeof(int16_t), 1, fInfile);

        //--Read the string in.
        fread(tVariableName, sizeof(char), tLen, fInfile);
        tVariableName[tLen] = '\0';

        //--Get the variable's path.
        char tVariableBuf[256];
        sprintf(tVariableBuf, "Root/Variables/Gallery/All/%s", tVariableName);
        fprintf(stderr, "Variable: %s\n", tVariableBuf);

        //--Fetch the var.
        bool tNewReg = false;
        rDataLibrary->mFailSilently = true;
        SysVar *rVar = (SysVar *)rDataLibrary->GetEntry(tVariableBuf);
        rDataLibrary->mFailSilently = false;

        //--Doesn't exist?  Create one.
        if(!rVar)
        {
            SetMemoryData(__FILE__, __LINE__);
            rVar = (SysVar *)starmemoryalloc(sizeof(SysVar));
            rVar->mIsSaved = true;
            rVar->mNumeric = 0.0f;
            SetMemoryData(__FILE__, __LINE__);
            rVar->mAlpha = (char *)starmemoryalloc(sizeof(char) * (strlen("NULL") + 1));
            strcpy(rVar->mAlpha, "NULL");
            tNewReg = true;
        }

        //--Set the variable to 1.0f implicitly. If we found it, it always is 1.
        rVar->mNumeric = 1.0f;

        //--If this is a new var, reg it.
        if(tNewReg)
        {
            rDataLibrary->AddPath(tVariableBuf);
            rDataLibrary->RegisterPointer(tVariableBuf, rVar, &DataLibrary::DeleteSysVar);
        }
    }

    //--Clean up.
    fclose(fInfile);
}
