//--[SugarLumpManager]
//--The manager responsible for controlling SLF files and extracting data from them on-demand.
//  Replaces the ExternalImageManager, by and large. Data is now of a generic lump type, which
//  can have subtypes to handle it (or be handled raw).

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--Local Structures
typedef struct
{
    uint64_t mPosition;
    char *mName;
}LumpLookup;
typedef struct
{
    //--Members
    bool mOwnsData;
    LumpLookup mLookup;
    uint32_t mDataSize;
    uint8_t *mData;

    //--Initialization Function
    void Init()
    {
        mOwnsData = false;
        mLookup.mPosition = 0;
        mLookup.mName = InitializeString("Unnamed Lump");
        mDataSize = 0;
        mData = NULL;
    }
}RootLump;

struct SMLocationPack;

//--Local Definitions
#define MAP_INVALID -1
#define MAP_2DLINEART 0
#define MAP_2DTWODEPTH 1
#define MAP_3DVOXEL 2
#define MAP_3DVECTOR 3

#define STANDARD_HEADER "SugarLumpFile100"
#define HEADER_MAX_LEN 80

#define DEFAULT_TILE_SIZE 16

class SugarLumpManager
{
    private:
    //--System
    bool mIsOpen;
    char *mOpenPath;
    int mFileVersion;
    char mHeader[HEADER_MAX_LEN];

    //--File
    VirtualFile *mVFile;

    //--Lumps
    uint32_t mTotalLumps;
    RootLump *mLumpList;

    //--New Lumps
    SugarLinkedList *mNewLumpList;

    protected:

    public:
    //--System
    SugarLumpManager();
    ~SugarLumpManager();
    static void DeleteRootLump(void *pPtr);

    //--Public Variables
    bool mSuppressErrorOnce;
    static bool xIsCaseInsensitive;
    static bool xDebugFlag;
    static int xTileSizeX;
    static int xTileSizeY;

    //--Property Queries
    bool IsFileOpen();
    char *GetOpenPath();
    int GetTotalLumps();
    bool DoesLumpExist(const char *pName);
    char *GetHeaderOf(const char *pName);

    //--Manipulators
    //--Core Methods
    int BinaryGetLookup(const char *pName, int pMin, int pMax);
    bool StandardSeek(const char *pLumpName, const char *pHeaderType);
    bool SlotwiseSeek(int pSlot);
    void Clear();

    //--Image Extraction
    SugarBitmapPrecache *GetImageData(const char *pLumpName);
    SugarBitmap *GetImage(const char *pLumpName);
    SugarBitmap *GetPaddedTileImage(const char *pLumpName);

    //--Lua Tarball
    LuaTarballEntry *GetTarball(int pSlot);

    //--Map Extraction
    int GetMapType(const char *pLumpName);

    //--Output
    void RegisterLump(RootLump *pLump);
    void MergeLumps();
    void Write(const char *pPath);

    //--Palette Extraction
    SugarPalette *GetPalette(const char *pLumpName);

    //--Update
    //--File I/O
    void Open(const char *pPath);
    void Open(const char *pPath, const char *pHeader);
    void Close();

    //--Drawing
    //--Pointer Routing
    VirtualFile *GetVirtualFile();

    //--Static Functions
    static SugarLumpManager *Fetch();
    static RootLump *CreateLump(const char *pName, const char *pHeader, uint32_t pExpectedSize);
    static RootLump *CreateLump(const char *pName);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_SLF_SetDebugFlag(lua_State *L);
int Hook_SLF_Open(lua_State *L);
int Hook_SLF_Close(lua_State *L);
int Hook_SLF_IsFileOpen(lua_State *L);
int Hook_SLF_GetOpenPath(lua_State *L);

