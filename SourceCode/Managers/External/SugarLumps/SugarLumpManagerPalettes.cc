//--Base
#include "SugarLumpManager.h"

//--Classes
//--CoreClasses
#include "SugarPalette.h"
#include "VirtualFile.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

SugarPalette *SugarLumpManager::GetPalette(const char *pLumpName)
{
    //--Extracts all the PaletteLump data and passes it back in a compiled SugarPalette.
    if(!mIsOpen || !pLumpName) return NULL;
    DebugManager::PushPrint(false, "Loading Palette Data %s\n", pLumpName);

    //--Locate it and seek to it.
    if(!StandardSeek(pLumpName, "PALETTE"))
    {
        DebugManager::ForcePrint("Error in SLF file\n");
        DebugManager::Pop();
        return NULL;
    }

    //--Create a blank SugarPalette.
    SugarPalette *nSugarPalette = new SugarPalette();

    //--Total colors.
    uint32_t tTotalColors = 0;
    mVFile->Read(&tTotalColors, sizeof(uint32_t), 1);
    nSugarPalette->SetTotalColors(tTotalColors);
    DebugManager::Print("Read out %i colors\n", tTotalColors);

    //--Read out those colors.
    StarlightColor tColor;
    for(uint32_t i = 0; i < tTotalColors; i ++)
    {
        uint8_t tRed, tGreen, tBlue;
        mVFile->Read(&tRed, sizeof(uint8_t), 1);
        mVFile->Read(&tGreen, sizeof(uint8_t), 1);
        mVFile->Read(&tBlue, sizeof(uint8_t), 1);
        tColor.SetRGBI(tRed, tGreen, tBlue);
        nSugarPalette->SetSourceColor(i, tColor);
    }

    //--Read out the number of palettes.
    uint32_t tTotalPalettes = 0;
    mVFile->Read(&tTotalPalettes, sizeof(uint32_t), 1);
    for(uint32_t i = 0; i < tTotalPalettes; i ++)
    {
        //--Read the name.
        char tString[64];
        uint8_t tLen = 0;
        mVFile->Read(&tLen, sizeof(uint8_t), 1);
        mVFile->Read(&tString, sizeof(char), tLen);
        tString[tLen] = '\0';

        //--Add it.
        nSugarPalette->AddPalette(tString);

        //--Read out its colors.
        for(uint32_t p = 0; p < tTotalColors; p ++)
        {
            uint8_t tRed, tGreen, tBlue;
            mVFile->Read(&tRed, sizeof(uint8_t), 1);
            mVFile->Read(&tGreen, sizeof(uint8_t), 1);
            mVFile->Read(&tBlue, sizeof(uint8_t), 1);
            nSugarPalette->SetPaletteColor(tString, p, StarlightColor::MapRGBI(tRed, tGreen, tBlue));
        }
    }

    //--Pass it back.
    DebugManager::Pop();
    return nSugarPalette;
}
