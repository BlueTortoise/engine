//--Base
#include "SugarLumpManager.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "VirtualFile.h"
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

SugarBitmapPrecache *SugarLumpManager::GetImageData(const char *pLumpName)
{
    //--Extracts the Image data as provided by the pLumpName. Returns NULL on failure.
    if(!mIsOpen || !pLumpName) return NULL;
    DebugManager::PushPrint(false, "Loading Image Data %s\n", pLumpName);

    //--Structure to hold the data.
    SetMemoryData(__FILE__, __LINE__);
    SugarBitmapPrecache *nCache = (SugarBitmapPrecache *)starmemoryalloc(sizeof(SugarBitmapPrecache));
    memset(nCache, 0, sizeof(SugarBitmapPrecache));
    SugarBitmap::SetPrecacheFlags(*nCache);

    //--Locate it and seek to it.
    if(!StandardSeek(pLumpName, "IMAGE"))
    {
        //--Block errors on this call if this flag is true. Sometimes, an image not existing is
        //  itself valid, so no need to bark.
        if(!mSuppressErrorOnce) DebugManager::ForcePrint("Error in SLF file\n");
        mSuppressErrorOnce = false;
        DebugManager::Pop();
        free(nCache);
        return NULL;
    }

    //--Get the compression type.
    mVFile->Read(&nCache->mCompressionType, sizeof(uint8_t), 1);
    DebugManager::Print("Compress type %i\n", nCache->mCompressionType);
    if(nCache->mCompressionType == 0)
    {
        DebugManager::ForcePrint("Compression error in %s\n", pLumpName);
        return nCache;
    }

    //--Read the offset data.
    mVFile->Read(&nCache->mTrueWidth, sizeof(int32_t), 1);
    mVFile->Read(&nCache->mTrueHeight, sizeof(int32_t), 1);
    mVFile->Read(&nCache->mXOffset, sizeof(int32_t), 1);
    mVFile->Read(&nCache->mYOffset, sizeof(int32_t), 1);
    mVFile->Read(&nCache->mWidth, sizeof(int32_t), 1);
    mVFile->Read(&nCache->mHeight, sizeof(int32_t), 1);
    DebugManager::Print("Statistics read %i %i\n", nCache->mTrueWidth, nCache->mTrueHeight);

    //--Length of the bitmap's data for upload.
    mVFile->Read(&nCache->mDataSize, sizeof(uint32_t), 1);
    DebugManager::Print("Raw data to read: %s %i bytes\n", pLumpName, nCache->mDataSize);

    //--Allocate an array for this job. The SugarBitmap will deallocate it for us.
    SetMemoryData(__FILE__, __LINE__);
    nCache->mArray = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * nCache->mDataSize);
    mVFile->Read(nCache->mArray, sizeof(uint8_t), nCache->mDataSize);
    DebugManager::Print("Data read successfully\n");

    //--Hitboxes
    mVFile->Read(&nCache->mTotalHitboxes, sizeof(uint8_t), 1);
    SetMemoryData(__FILE__, __LINE__);
    nCache->mHitboxes = (SgHitbox *)starmemoryalloc(sizeof(SgHitbox) * nCache->mTotalHitboxes);
    for(int i = 0; i < nCache->mTotalHitboxes; i ++)
    {
        mVFile->Read(&nCache->mHitboxes[i], sizeof(SgHitbox), 1);
        //fprintf(stderr, "Hitbox of %i %i - %i %i\n", nCache->mHitboxes[i].mLft, nCache->mHitboxes[i].mTop, nCache->mHitboxes[i].mRgt, nCache->mHitboxes[i].mBot);
    }

    //--Pass it back.
    DebugManager::PopPrint("Image data loaded\n");
    return nCache;
}
SugarBitmap *SugarLumpManager::GetImage(const char *pLumpName)
{
    //--[Documentation and Setup]
    //--Overload, gets the ImageData and immediately uploads it to a SugarBitmap.
    if(!mIsOpen || !pLumpName) return NULL;
    DebugManager::PushPrint(false, "Loading Image %s\n", pLumpName);

    //--[Raw Data]
    //--Get the raw data
    SugarBitmapPrecache *tDataCache = GetImageData(pLumpName);
    if(!tDataCache)
    {
        DebugManager::ForcePrint("Failed, can't find data\n");
        DebugManager::Pop();
        return NULL;
    }

    //--[Common]
    //--Create a bitmap for return.
    SugarBitmap *nBitmap = new SugarBitmap();

    //--[Standard, No Atlas]
    //--Just upload the bitmap.
    bool tWasAtlas = false;
    if(SugarBitmap::xDisallowAtlasing || !SugarBitmap::xIsAtlasModeActive || tDataCache->mTrueWidth >= SugarBitmap::xMaxTextureSize * 0.50f || tDataCache->mTrueHeight >= SugarBitmap::xMaxTextureSize * 0.50f)
    {
        //--[Upload]
        uint32_t tHandle = SugarBitmap::UploadCompressed(tDataCache);
        if(!tHandle)
        {
            DebugManager::ForcePrint("Failed, error uploading the data\n");
            DebugManager::Pop();
            return NULL;
        }

        //--Assign the handle.
        nBitmap->AssignHandle(tHandle, true, false);
    }
    //--[Atlas Mode]
    //--When atlasing is enabled, we try to upload this image to an atlas or create one if it doesn't
    //  already exist.
    else
    {
        //--Debug.
        tWasAtlas = true;
        //fprintf(stderr, "Beginning atlas work.\n");

        //--Check if there's no atlas currently active. If not, set this as the active atlas.
        if(!SugarBitmap::xrActiveAtlas)
        {
            //--Debug.
            //fprintf(stderr, " Initializing new atlas.\n");

            //--Set.
            SugarBitmap::xrActiveAtlas = nBitmap;
            nBitmap->mDependencyList = new SugarLinkedList(false);

            //--Setup the length. For now we use fixed values.
            SugarBitmap::xAtlasDataLen = (sizeof(uint32_t) * (int)SugarBitmap::xAtlasMaxSize * (int)SugarBitmap::xAtlasMaxSize);
            SugarBitmap::xAtlasStride = sizeof(uint32_t) * (int)SugarBitmap::xAtlasMaxSize;
            SetMemoryData(__FILE__, __LINE__);
            SugarBitmap::xAtlasData = (uint8_t *)starmemoryalloc(SugarBitmap::xAtlasDataLen);

            //--Reset the counting variables.
            SugarBitmap::xAtlasCurrentX = 0;
            SugarBitmap::xAtlasCurrentY = 0;
            SugarBitmap::xAtlasCurrentTallest = 0;

            //--Debug.
            //fprintf(stderr, " Done initializing new atlas.\n");
        }

        //--Get the raw bitmap data. This will uncompress it if it was compressed.
        int32_t tWidth = tDataCache->mWidth;
        int32_t tHeight = tDataCache->mHeight;
        int32_t tDataSize = tDataCache->mDataSize;

        //--Range check. See if there's enough space left for this bitmap.
        if(SugarBitmap::xAtlasCurrentX + tWidth >= (int)SugarBitmap::xAtlasMaxSize)
        {
            //--Next line set.
            //fprintf(stderr, " Handling Atlas new line case.\n");
            SugarBitmap::xAtlasCurrentX = 0;
            SugarBitmap::xAtlasCurrentY += SugarBitmap::xAtlasCurrentTallest;

            //--Check if there's enough vertical space left. If not, finish the atlas, and start
            //  a new one with this object as the atlas object.
            if(SugarBitmap::xAtlasCurrentY + tHeight >= (int)SugarBitmap::xAtlasMaxSize)
            {
                //--Finish the old atlas.
                //fprintf(stderr, " Handling Atlas-end case.\n");
                SugarBitmap::xrActiveAtlas->FinishAtlas();

                //--Boot a new atlas.
                SugarBitmap::xrActiveAtlas = nBitmap;
                nBitmap->mDependencyList = new SugarLinkedList(false);

                //--Setup the length. For now we use fixed values.
                SugarBitmap::xAtlasDataLen = (sizeof(uint32_t) * (int)SugarBitmap::xAtlasMaxSize * (int)SugarBitmap::xAtlasMaxSize);
                SugarBitmap::xAtlasStride = sizeof(uint32_t) * (int)SugarBitmap::xAtlasMaxSize;
                SetMemoryData(__FILE__, __LINE__);
                SugarBitmap::xAtlasData = (uint8_t *)starmemoryalloc(SugarBitmap::xAtlasDataLen);

                //--Reset the counting variables.
                SugarBitmap::xAtlasCurrentX = 0;
                SugarBitmap::xAtlasCurrentY = 0;
                SugarBitmap::xAtlasCurrentTallest = 0;
                //fprintf(stderr, " Handled Atlas-end case.\n");
            }

            //--Otherwise, set the values as necessary.
            SugarBitmap::xAtlasCurrentTallest = 0;
            //fprintf(stderr, " Handled new line case.\n");
        }

        //fprintf(stderr, " Getting raw data.\n");
        SugarBitmap::PutDataIntoAtlas(tDataCache->mArray, tWidth, tHeight, tDataCache->mCompressionType, tDataSize);
        if(SugarBitmap::xInterruptCall && !SugarBitmap::xSuppressInterrupt) SugarBitmap::xInterruptCall();

        //--Store our upload positions in the uv coordinates.
        SugarBitmap::xLastAtlasLft = (SugarBitmap::xAtlasCurrentX)           / SugarBitmap::xAtlasMaxSize;
        SugarBitmap::xLastAtlasTop = (SugarBitmap::xAtlasCurrentY)           / SugarBitmap::xAtlasMaxSize;
        SugarBitmap::xLastAtlasRgt = (SugarBitmap::xAtlasCurrentX + tWidth)  / SugarBitmap::xAtlasMaxSize;
        SugarBitmap::xLastAtlasBot = (SugarBitmap::xAtlasCurrentY + tHeight) / SugarBitmap::xAtlasMaxSize;
        nBitmap->SetAtlasCoordinates(SugarBitmap::xLastAtlasLft, SugarBitmap::xLastAtlasTop, SugarBitmap::xLastAtlasRgt, SugarBitmap::xLastAtlasBot);
        //fprintf(stderr, "Coords %f %f\n", SugarBitmap::xLastAtlasLft, SugarBitmap::xLastAtlasTop);

        //--Move the counting variables.
        SugarBitmap::xAtlasCurrentX += tWidth;
        if(SugarBitmap::xAtlasCurrentTallest < tHeight) SugarBitmap::xAtlasCurrentTallest = tHeight;

        //--If this bitmap is not the atlas bitmap, it's a dependent bitmap.
        if(nBitmap != SugarBitmap::xrActiveAtlas)
        {
            SugarBitmap::xrActiveAtlas->AddToAtlasList(nBitmap);
        }
        //fprintf(stderr, "Completed.\n");
    }

    //--Common properties.
    nBitmap->SetOffset(tDataCache->mXOffset, tDataCache->mYOffset);
    nBitmap->SetTrueSizes(tDataCache->mTrueWidth, tDataCache->mTrueHeight);
    nBitmap->SetSizes(tDataCache->mWidth, tDataCache->mHeight);
    if(SugarBitmap::xPadSpriteForEdging && !tWasAtlas)
    {
        nBitmap->SetOffset(tDataCache->mXOffset - 2, tDataCache->mYOffset - 2);
        nBitmap->SetSizes(tDataCache->mWidth + 4, tDataCache->mHeight + 4);
    }

    //--Hitboxes, if they exist.
    nBitmap->AllocateHitboxes(tDataCache->mTotalHitboxes);
    for(int i = 0; i < tDataCache->mTotalHitboxes; i ++)
    {
        nBitmap->SetHitbox(i, tDataCache->mHitboxes[i]);
    }

    //--Any image that was loaded out of the SLM is considered to be flipped. Note that this is not
    //  the case for Allegro-stripped bitmaps.
    nBitmap->SetInternalFlip(true);

    //--[Clean]
    DebugManager::PopPrint("Lump uploaded successfully\n");
    free(tDataCache->mArray);
    free(tDataCache->mHitboxes);
    free(tDataCache);
    return nBitmap;
}
SugarBitmap *SugarLumpManager::GetPaddedTileImage(const char *pLumpName)
{
    //--Special instance of GetImage(), this version is used for tile mappings. When tiles are upscaled or downscaled,
    //  they will often bleed into one another (or transparent space) because they are in atlas format. To prevent this,
    //  this algorithm will place buffers around the edges of all tiles. The buffers are the same color as the adjacent pixel.
    //--When handling this case, a 16x16 tile becomes 18x18 (pad on all four edges). Adjust your texel coordinates accordingly.
    //--Overload, gets the ImageData and immediately uploads it to an SugarBitmap.
    if(!mIsOpen || !pLumpName) return NULL;
    DebugManager::PushPrint(false, "Loading Image %s\n", pLumpName);

    //--Get the raw data
    SugarBitmapPrecache *tDataCache = GetImageData(pLumpName);
    if(!tDataCache)
    {
        DebugManager::ForcePrint("Failed, can't find data\n");
        DebugManager::Pop();
        return NULL;
    }

    //--Constants
    int cBytesPerPixel = sizeof(uint8_t) * 4;
    int cSizeX = xTileSizeX;
    int cSizeY = xTileSizeY;

    //--Expect number of discrete tiles.
    int tExpectedX = tDataCache->mTrueWidth / cSizeX;
    int tExpectedY = tDataCache->mTrueHeight / cSizeY;

    //--Stride values.
    int cOldStride = tDataCache->mTrueWidth * cBytesPerPixel;
    int cNewStride = (tExpectedX * (cSizeX+2)) * cBytesPerPixel;
    DebugManager::Print("A.\n");

    //--Figure out how many tiles we expect. Error check if there are somehow zero tiles.
    if(tExpectedX > 0 && tExpectedY > 0)
    {
        //--Allocate space for the padded array.
        SetMemoryData(__FILE__, __LINE__);
        uint8_t *nNewArray = (uint8_t *)starmemoryalloc((tExpectedX * (cSizeX+2)) * (tExpectedY * (cSizeY+2)) * cBytesPerPixel);

        //--Begin crossloading. Pad as we go.
        for(int x = 0; x < tExpectedX; x ++)
        {
            for(int y = 0; y < tExpectedY; y ++)
            {
                //--Iterate across the tile itself.
                for(int i = 0; i < cSizeX; i ++)
                {
                    for(int p = 0; p < cSizeY; p ++)
                    {
                        //--Compute the position of this pixel. Remember that the stride already includes the bytes-per-pixel value.
                        int tOrigPosition = (((y *  cSizeY)    + p + 0) * cOldStride) + (((x *  cSizeX)    + i + 0) * cBytesPerPixel);
                        int tNewPosition  = (((y * (cSizeY+2)) + p + 1) * cNewStride) + (((x * (cSizeX+2)) + i + 1) * cBytesPerPixel);

                        //--Crossload.
                        nNewArray[tNewPosition+0] = tDataCache->mArray[tOrigPosition+0];
                        nNewArray[tNewPosition+1] = tDataCache->mArray[tOrigPosition+1];
                        nNewArray[tNewPosition+2] = tDataCache->mArray[tOrigPosition+2];
                        nNewArray[tNewPosition+3] = tDataCache->mArray[tOrigPosition+3];

                        //--Special case: Left edge.
                        if(i == 0)
                        {
                            nNewArray[tNewPosition+0-cBytesPerPixel] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1-cBytesPerPixel] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2-cBytesPerPixel] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3-cBytesPerPixel] = tDataCache->mArray[tOrigPosition+3];
                        }
                        //--Special case: Right edge.
                        else if(i == cSizeX-1)
                        {
                            nNewArray[tNewPosition+0+cBytesPerPixel] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1+cBytesPerPixel] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2+cBytesPerPixel] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3+cBytesPerPixel] = tDataCache->mArray[tOrigPosition+3];
                        }

                        //--Special case: Top edge.
                        if(p == 0)
                        {
                            nNewArray[tNewPosition+0-cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1-cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2-cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3-cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }
                        else if(p == cSizeY-1)
                        {
                            nNewArray[tNewPosition+0+cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1+cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2+cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3+cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }

                        //--Top-left edge.
                        if(i == 0 && p == 0)
                        {
                            nNewArray[tNewPosition+0-cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1-cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2-cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3-cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }
                        //--Top-right edge.
                        else if(i == cSizeX-1 && p == 0)
                        {
                            nNewArray[tNewPosition+0+cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1+cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2+cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3+cBytesPerPixel-cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }
                        //--Bottom-left edge.
                        else if(i == 0 && p == cSizeY-1)
                        {
                            nNewArray[tNewPosition+0-cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1-cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2-cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3-cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }
                        //--Bottom-right edge.
                        else if(i == cSizeX-1 && p == cSizeY-1)
                        {
                            nNewArray[tNewPosition+0+cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+0];
                            nNewArray[tNewPosition+1+cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+1];
                            nNewArray[tNewPosition+2+cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+2];
                            nNewArray[tNewPosition+3+cBytesPerPixel+cNewStride] = tDataCache->mArray[tOrigPosition+3];
                        }
                    }
                }
            }
        }

        //--Replace the data in the original cache.
        free(tDataCache->mArray);
        tDataCache->mArray = nNewArray;
        tDataCache->mWidth = (tExpectedX * (cSizeX+2));
        tDataCache->mHeight = (tExpectedY * (cSizeY+2));
        tDataCache->mTrueWidth = (tExpectedX * (cSizeX+2));
        tDataCache->mTrueHeight = (tExpectedY * (cSizeY+2));
        tDataCache->mDataSize = (tExpectedX * (cSizeX+2)) * (tExpectedY * (cSizeY+2)) * cBytesPerPixel;
    }
    DebugManager::Print("B.\n");

    //--Upload it
    SugarBitmap *nBitmap = new SugarBitmap();
    uint32_t tHandle = SugarBitmap::UploadCompressed(tDataCache);
    if(!tHandle)
    {
        DebugManager::ForcePrint("Failed, error uploading the data\n");
        DebugManager::Pop();
        return NULL;
    }
    DebugManager::Print("C.\n");
    nBitmap->AssignHandle(tHandle, true, false);
    nBitmap->SetOffset(tDataCache->mXOffset, tDataCache->mYOffset);
    nBitmap->SetTrueSizes(tDataCache->mTrueWidth, tDataCache->mTrueHeight);
    nBitmap->SetSizes(tDataCache->mWidth, tDataCache->mHeight);
    if(SugarBitmap::xPadSpriteForEdging)
    {
        nBitmap->SetOffset(tDataCache->mXOffset - 2, tDataCache->mYOffset - 2);
        nBitmap->SetSizes(tDataCache->mWidth + 4, tDataCache->mHeight + 4);
    }

    //--Hitboxes, if they exist.
    nBitmap->AllocateHitboxes(tDataCache->mTotalHitboxes);
    for(int i = 0; i < tDataCache->mTotalHitboxes; i ++)
    {
        nBitmap->SetHitbox(i, tDataCache->mHitboxes[i]);
    }
    DebugManager::Print("D.\n");

    //--Any image that was loaded out of the SLM is considered to be flipped. Note that this is not
    //  the case for Allegro-stripped bitmaps.
    nBitmap->SetInternalFlip(true);
    DebugManager::Print("E.\n");

    //--Clean
    DebugManager::PopPrint("Lump uploaded successfully\n");
    free(tDataCache->mArray);
    free(tDataCache->mHitboxes);
    free(tDataCache);
    return nBitmap;
}
