//--Base
#include "SteamManager.h"

//--Classes
#include "CSteamStats.h"

//--CoreClasses
//--Definitions
#include "Global.h"

//--Libraries
//--Managers

//=========================================== System ==============================================
SteamManager::SteamManager()
{
    //--[Steam Manager]
    //--By default, the Steam Manager has no variables and none of its functions do anything. However,
    //  the define _STEAM_API_ causes the headers to be included. The manager also requires activation
    //  by scripts or it is disabled.
    #if defined _STEAM_API_

    //--System
    mIsSteamRunning = false;
    mTickOfLastStatRequest = -1000;
    mHasReceivedCurrentStats = false;
    mSteamStats = NULL;

    //--Steam Achievement Storage
    mAchievementsTotal = 0;
    mAchievements = NULL;
    #endif
}
SteamManager::~SteamManager()
{
    #if defined _STEAM_API_
    for(int i = 0; i < mAchievementsTotal; i ++)
    {
        mAchievements[i].FreeMemory();
    }
    free(mAchievements);
    SteamAPI_Shutdown();
    #endif
}
void SteamManager::Boot(int pGameID)
{
    //--Boot sequence. Does not occur automatically. If not booted, all of the manager's function calls
    //  never do anything.
    //--The Game ID is the internal index, not the steam ID itself.
    #if defined _STEAM_API_
    if(mIsSteamRunning) return;
    //fprintf(stderr, "Booting steam manager.\n");

    //--Setting app ID.
    if(pGameID == STEAM_GAME_INDEX_STRING_TYRANT)
    {
        xSteamAppID = STRING_TYRANT_APPID;
    }
    else if(pGameID == STEAM_GAME_INDEX_STRING_TYRANT_DEMO)
    {
        xSteamAppID = STRING_TYRANT_DEMO_APPID;
    }

    //--Activate.
    mIsSteamRunning = SteamAPI_Init();

    //--Steam failed to run!
    if(!mIsSteamRunning)
    {
        fprintf(stderr, "Steam is not running, Steam functionality disabled.\n");
        return;
    }

    //--Start manual callback handling.
    SteamAPI_ManualDispatch_Init();

    //--Request player statistics.
    RequestStats();
    //fprintf(stderr, "Finished booting steam manager.\n");
    #endif
}

//--[Public Variables]
int SteamManager::xSteamAppID = STRING_TYRANT_APPID;

//====================================== Property Queries =========================================
bool SteamManager::IsSteamRunning()
{
    #if defined _STEAM_API_
    return mIsSteamRunning;
    #endif
    return false;
}

//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
void SteamManager::AllocateAchievements(int pTotal)
{
    #if defined _STEAM_API_
    //--Deallocate.
    //fprintf(stderr, "Allocating achievements.\n");
    for(int i = 0; i < mAchievementsTotal; i ++)
    {
        mAchievements[i].FreeMemory();
    }
    free(mAchievements);

    //--Reset.
    mAchievementsTotal = 0;
    mAchievements = NULL;
    if(pTotal < 1)
    {
        fprintf(stderr, "Achievements count was zero.\n");
        return;
    }

    //--Allocate.
    mAchievementsTotal = pTotal;
    mAchievements = (SteamAchievement *)starmemoryalloc(sizeof(SteamAchievement) * pTotal);
    for(int i = 0; i < mAchievementsTotal; i ++)
    {
        mAchievements[i].Initialize();
    }
    //fprintf(stderr, "Finished allocating achievements.\n");
    #endif
}
void SteamManager::SetAchievement(int pIndex, const char *pName)
{
    #if defined _STEAM_API_
    //fprintf(stderr, "Setting achievement %i %s\n", pIndex, pName);
    if(pIndex < 0 || pIndex >= mAchievementsTotal) return;
    ResetString(mAchievements[pIndex].mName, pName);
    #endif
}
void SteamManager::UnlockAchievement(int pIndex)
{
    #if defined _STEAM_API_
    //--Range check.
    //fprintf(stderr, "Unlocking achievement %i\n", pIndex);
    if(pIndex < 0 || pIndex >= mAchievementsTotal) return;
	if(SteamUserStats() == NULL || SteamUser() == NULL) return;

    //--Achievement already unlocked.
    if(mAchievements[pIndex].mIsAchieved) return;

    //--Unlock it!
    mAchievements[pIndex].mIsAchieved = true;
    SteamUserStats()->SetAchievement(mAchievements[pIndex].mName);
    SteamUserStats()->StoreStats();
    #endif
}
bool SteamManager::RequestStats()
{
    //--Requests player stats and achievements. Should be called after Steam boots or if the stats
    //  need to be refreshed, such as after unlocking something.
    //--Returns false if the request was not sent for any reason.
    #if defined _STEAM_API_
    //fprintf(stderr, "Requesting stats.\n");
    if(mHasReceivedCurrentStats || !mIsSteamRunning) return false;

    //--If it has been less than 10 seconds since the last request was sent, don't send one.
    int tCurrentTick = (int)Global::Shared()->gTicksElapsed;
    if(mTickOfLastStatRequest + 600 >= tCurrentTick) return false;

    //--Store when the request was sent.
    mTickOfLastStatRequest = tCurrentTick;

    //--Debug.
	//fprintf(stderr, "==> Called request stats on tick %i.\n", tCurrentTick);

	//--Is Steam loaded? If not we can't get stats.
	if(SteamUserStats() == NULL || SteamUser() == NULL)
	{
		return false;
	}
	//--Is the user logged on? If not we can't get stats.
	if(!SteamUser()->BLoggedOn())
	{
		return false;
	}

	//--Request user stats. This will fire a callback when they arrive.
	//fprintf(stderr, "    Issued request.\n");
	return SteamUserStats()->RequestCurrentStats();
    #endif
    return false;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void SteamManager::PumpCallbackStack()
{
    #if defined _STEAM_API_
    //--At the end of a logic tick, checks for callbacks and runs them in the order they were received.
    if(!mIsSteamRunning) return;
	if(SteamUserStats() == NULL || SteamUser() == NULL) return;
    //fprintf(stderr, "Pumping callback stack.\n");

    //--Get Steam Pipe.
	HSteamPipe tSteamPipe = SteamAPI_GetHSteamPipe();

	//--Run dispatch routine.
	SteamAPI_ManualDispatch_RunFrame(tSteamPipe);

	//--Iterate across all callbacks.
	CallbackMsg_t tCallback;
	while(SteamAPI_ManualDispatch_GetNextCallback(tSteamPipe, &tCallback))
	{
	    //--This is a dispatching API callback that is completed.
	    if(tCallback.m_iCallback == SteamAPICallCompleted_t::k_iCallback)
        {
            //--Debug.
            //fprintf(stderr, "API callback.\n");

            //--Allocate space for the callback result.
			SteamAPICallCompleted_t *rCallCompleted = (SteamAPICallCompleted_t *)tCallback.m_pubParam;
			void *tTmpCallResult = malloc(tCallback.m_cubParam);

            //--Populate with data, find out if the API result failed.
            bool tFailed = true;
            SteamAPI_ManualDispatch_GetAPICallResult(tSteamPipe, rCallCompleted->m_hAsyncCall, tTmpCallResult, tCallback.m_cubParam, tCallback.m_iCallback, &tFailed);
        }
        //--Received user stats.
        else if(tCallback.m_iCallback == UserStatsReceived_t::k_iCallback)
        {
            mHasReceivedCurrentStats = true;
            //fprintf(stderr, "Received user stats on tick %i.\n", Global::Shared()->gTicksElapsed);
            //fprintf(stderr, "There are %i achievements.\n", (int)SteamUserStats()->GetNumAchievements());

            //--Iterate across our achievement list and toggle them.
            for(int i = 0; i < mAchievementsTotal; i ++)
            {
                bool tGotAchievement = SteamUserStats()->GetAchievement(mAchievements[i].mName, &mAchievements[i].mIsAchieved);
                if(tGotAchievement)
                {
                    //fprintf(stderr, " Achievement %s found. Status %i.\n", mAchievements[i].mName, mAchievements[i].mIsAchieved);
                }
                else
                {
                    //fprintf(stderr, " Achievement %s not found in backend.\n", mAchievements[i].mName);
                }
            }
        }
        //--Friends status changed.
        else if(tCallback.m_iCallback == PersonaStateChange_t::k_iCallback)
        {
            //fprintf(stderr, "Changed friends status.\n");
        }
        //--Favourites status changed.
        else if(tCallback.m_iCallback == FavoritesListChanged_t::k_iCallback)
        {
            //fprintf(stderr, "Changed favourites status.\n");
        }
        //--Other type of callback.
        else
        {
            //--Ignore callback 715. It has to do with the focus changing.
            if(tCallback.m_iCallback == 715)
            {

            }
            //--Print.
            else
            {
                //fprintf(stderr, "Received callback of type %i\n", tCallback.m_iCallback);
            }
        }

		//--Clean up. This *must* be called before the next iteration since SteamAPI_ManualDispatch_GetNextCallback() procures memory.
		SteamAPI_ManualDispatch_FreeLastCallback(tSteamPipe);
	}
    //fprintf(stderr, "Finished pumping callback stack.\n");
    #endif
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
SteamManager *SteamManager::Fetch()
{
    return Global::Shared()->gSteamManager;
}

//========================================= Lua Hooking ===========================================
void SteamManager::HookToLuaState(lua_State *pLuaState)
{
    /* Steam_Activate(iGameIndex)
       Activates the steam overlay for the given game index. The indices are internal, presently all
       numbers just activate String Tyrant. */
    lua_register(pLuaState, "Steam_Activate", &Hook_Steam_Activate);

    /* Steam_AllocateAchievements(iTotal)
       Allocates the given number of achievements. These are client-sided. */
    lua_register(pLuaState, "Steam_AllocateAchievements", &Hook_Steam_AllocateAchievements);

    /* Steam_SetAchievement(iIndex, sName)
       Sets the name of the achievement. This is the same name as the API name on the Steampowered page. */
    lua_register(pLuaState, "Steam_SetAchievement", &Hook_Steam_SetAchievement);

    /* Steam_UnlockAchievement(iIndex)
       Marks the achievement in the index as unlocked. */
    lua_register(pLuaState, "Steam_UnlockAchievement", &Hook_Steam_UnlockAchievement);

}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_Steam_Activate(lua_State *L)
{
    //Steam_Activate(iGameIndex)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Steam_Activate");

    SteamManager::Fetch()->Boot(lua_tointeger(L, 1));
    return 0;
}
int Hook_Steam_AllocateAchievements(lua_State *L)
{
    //Steam_AllocateAchievements(iTotal)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Steam_AllocateAchievements");

    SteamManager::Fetch()->AllocateAchievements(lua_tointeger(L, 1));
    return 0;
}
int Hook_Steam_SetAchievement(lua_State *L)
{
    //Steam_SetAchievement(iIndex, sName)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("Steam_SetAchievement");

    SteamManager::Fetch()->SetAchievement(lua_tointeger(L, 1), lua_tostring(L, 2));
    return 0;
}
int Hook_Steam_UnlockAchievement(lua_State *L)
{
    //Steam_UnlockAchievement(iIndex)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Steam_UnlockAchievement");

    SteamManager::Fetch()->UnlockAchievement(lua_tointeger(L, 1));
    return 0;
}
