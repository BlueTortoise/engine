//--[CSteamStats]
//--Class prescribed by Steam to interact with its stats UI.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "SteamManager.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
#if defined _STEAM_API_
class CSteamStats
{
    private:
	int64_t m_iAppID; // Our current AppID
	Stat_t *m_pStats; // Stats data
	int32 m_iNumStats; // The number of Stats
	bool m_bInitialized; // Have we called Request stats and received the callback?

    public:
	CSteamStats(Stat_t *Stats, int NumStats);
	~CSteamStats();

	bool RequestStats();
	bool StoreStats();

	STEAM_CALLBACK( CSteamStats, OnUserStatsReceived, UserStatsReceived_t, m_CallbackUserStatsReceived );
	STEAM_CALLBACK( CSteamStats, OnUserStatsStored,   UserStatsStored_t,   m_CallbackUserStatsStored );
};
#endif

//--Hooking Functions

