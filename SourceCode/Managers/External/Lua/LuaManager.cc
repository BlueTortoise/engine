//--Base
#include "LuaManager.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"

//=========================================== System ==============================================
LuaManager::LuaManager()
{
    //--[LuaManager]
    //--System
    //--Lua State
    mLuaState = luaL_newstate();
    luaL_openlibs(mLuaState);

    //--Passing arguments
    mClearArgs = true;
    mTotalArgs = 0;
    mArgSlot = 0;
    mScriptArguments = NULL;

    //--Call Stack
    mCallStack = new SugarLinkedList(true);

    //--Precompilation
    mUsePrecompiling = false;

    //--Tarball mode.
    mIsTarballMode = false;
    mTotalTarballScripts = 0;
    mTarballScripts = NULL;

    //--Auto-exec Storage.
    mMacroArguments = new SugarLinkedList(true);
    mMacroList = new SugarLinkedList(true);
    mMacroList->SetCaseSensitivity(false);

    //--Alt-Directory Mode
    mCheckDirectoryPath = NULL;
    mAltDirectoryPath = NULL;

    //--Timer List
    mTimerList = new SugarLinkedList(true);

    //--Public Variables
    mWasLastActionInError = false;
    mAlwaysBarkResolveErrors = true;
    mFiringCode = -1;
    mFiringString = NULL;
    mBypassTarballOnce = false;

    //--[Hook the defaults]
    /* LM_GetSystemLibrary() (1 String)
       Returns either "Allegro" or "SDL" depending on which library is in use. */
    lua_register(mLuaState, "LM_GetSystemLibrary", &Hook_LM_GetSystemLibrary);

    /* LM_GetSystemOS() (1 String)
       Returns either "Windows", "Linux", or "OSX" depending on the executable. */
    lua_register(mLuaState, "LM_GetSystemOS", &Hook_LM_GetSystemOS);

    /* LM_SetAlternateDirectory(sFind, sReplace)
       Activates alternate-directory mode. In this mode, if a lua file fails to load, the program will check again
       but replace sFind with sReplace. This allows you to stack two directories paralell and modify certain files
       to keep functionality without having to maintain two different directory systems. */
    lua_register(mLuaState, "LM_SetAlternateDirectory", &Hook_LM_SetAlternateDirectory);

    /* LM_GetRandomNumber(iLowest, iHighest)
       Returns a random number within the specified range, inclusive. */
    lua_register(mLuaState, "LM_GetRandomNumber", &Hook_LM_GetRandomNumber);

    /* LM_SetSilenceFlag(bFlag)
       Sets whether or not standard lua functions will print anything to the console to indicate
       failure. */
    lua_register(mLuaState, "LM_SetSilenceFlag", &Hook_LM_SetSilenceFlag);

    /* LM_GetCallStack(iSlot)
       Returns the path of the called script from the stack.  Can pass if using stdin! */
    lua_register(mLuaState, "LM_GetCallStack", &Hook_LM_GetCallStack);

    /* LM_BootTarballMode(sTarballFilePath[])
       Causes the LuaManager to switch to Tarball mode, executing scripts from a precached datafile
       instead of manually loading them off the hard drive all the time. */
    lua_register(mLuaState, "LM_BootTarballMode", &Hook_LM_BootTarballMode);

    /* LM_StartTimer(sName)
       Starts a timer with the given name. If the name already exists, resets that timer to now. */
    lua_register(mLuaState, "LM_StartTimer", &Hook_LM_StartTimer);

    /* LM_CheckTimer(sName)
       Returns how long the named timer has been running, or 0 if the timer doesn't exist. */
    lua_register(mLuaState, "LM_CheckTimer", &Hook_LM_CheckTimer);

    /* LM_FinishTimer(sName)
       Returns how long the named timer has been running, or 0 if the timer doesn't exist. Then, deletes the timer. */
    lua_register(mLuaState, "LM_FinishTimer", &Hook_LM_FinishTimer);

    //--[Executor Functions]
    /* LM_ExecuteScript(sPathname[], ...)
       Executes the provided script with the provided args.  Regardless of what type they are passed
       in as, the argument is always stored as a string.  The number of passed arguments can be
       checked with LM_GetNumOfArgs(). */
    lua_register(mLuaState, "LM_ExecuteScript", &Hook_LM_ExecuteScript);

    /* LM_GetNumOfArgs()
       Returns how many arguments are currently on the LM's stack. */
    lua_register(mLuaState, "LM_GetNumOfArgs", &Hook_LM_GetNumOfArgs);

    /* LM_GetScriptArgument(iSlot)
       LM_GetScriptArgument(iSlot, "N")
       Returns the specified argument as a string.  Slots count from 0.  You can check how many args
       were passed with LM_GetNumOfArgs().  If an arg was not passed, the string will be "NULL".
       The second overload forces the arg to be coerced to a number first. */
    lua_register(mLuaState, "LM_GetScriptArgument", &Hook_LM_GetScriptArgument);

    //--[Auto-Exec]
    /* LM_NewMacro(sName[], sString[])
       Creates a new macro script with the given name, defaulting to the provided string.  The macro
       can be executed again at any time by using LM_Call() */
    lua_register(mLuaState, "LM_NewMacro", &Hook_LM_NewMacro);

    /* LM_AppendMacro(sName[], sAppend[])
       Appends the sAppend onto the end of the existing macro, if it exists.  If not, nothing happens. */
    lua_register(mLuaState, "LM_AppendMacro", &Hook_LM_AppendMacro);

    /* LM_Call(sName[])
       Calls the already-registered macro of the same name, if it exists.  The macro, it ought to be
       noted, will not have access to the Lua state's path controls, and will not respond correctly
       to fnResolvePath or related functions. */
    lua_register(mLuaState, "LM_Call", &Hook_LM_Call);
}
LuaManager::~LuaManager()
{
    lua_close(mLuaState);
    delete mMacroArguments;
    delete mMacroList;
    free(mCheckDirectoryPath);
    free(mAltDirectoryPath);
}
bool LuaManager::xFailSilently = false;
int LuaManager::xLastDumpedSize = 0;

//====================================== Property Queries =========================================
lua_State *LuaManager::GetLuaState()
{
    return mLuaState;
}
int LuaManager::GetTotalScriptArguments()
{
    return mTotalArgs;
}
char *LuaManager::GetScriptArgument(int pSlot)
{
    if(pSlot < 0 || pSlot >= mTotalArgs) return NULL;
    return (char *)mScriptArguments[pSlot];
}
bool LuaManager::DoesScriptExist(const char *pPath)
{
    if(!pPath) return false;

    FILE *fCheckFile = fopen(pPath, "r");
    if(!fCheckFile) return false;

    fclose(fCheckFile);
    return true;
}
char *LuaManager::GetCallStack(int pSlot)
{
    return (char *)mCallStack->GetElementBySlot(pSlot);
}

//--[WARNING]
//--These always return valid values, but the variable may have not been found. If that happens then
//  the error flag will be tripped.
//--Alternately, if mAlwaysBarkResolveErrors is true, the error flag is not flipped but a message
//  is dumped to the console.
float LuaManager::GetLuaGlobalAsFloat(const char *pName)
{
    float tReturn = 0.0f;
    lua_getglobal(mLuaState, pName);
    if(lua_isnil(mLuaState, -1) || !lua_isnumber(mLuaState, -1))
    {
        if(mAlwaysBarkResolveErrors)
        {
            DebugManager::ForcePrint("LuaManager: Error, could not resolve %s as a number.\n", pName);
        }
        else
        {
            mWasLastActionInError = true;
        }
    }
    else
    {
        tReturn = lua_tonumber(mLuaState, -1);
    }
    lua_pop(mLuaState, 1);
    return tReturn;
}
char *LuaManager::GetLuaGlobalAsString(const char *pName)
{
    //--You *are* responsible for deallocating the returned string. The result is copied as it is
    //  unknown when the Lua state will change it.
    //--Null can be returned on error.
    char *nReturn = NULL;
    lua_getglobal(mLuaState, pName);
    if(lua_isnil(mLuaState, -1) || !lua_isnumber(mLuaState, -1))
    {
        if(mAlwaysBarkResolveErrors)
        {
            DebugManager::ForcePrint("LuaManager: Error, could not resolve %s as a string.\n", pName);
        }
        else
        {
            mWasLastActionInError = true;
        }
    }
    else
    {
        ResetString(nReturn, lua_tostring(mLuaState, -1));
    }
    lua_pop(mLuaState, 1);
    return nReturn;
}

//========================================= Manipulators ==========================================
void LuaManager::SetAltDirectory(const char *pSearch, const char *pReplace)
{
    //--Note: NULL is valid for either. Alt-directory mode is disabled if either value is NULL.
    ResetString(mCheckDirectoryPath, pSearch);
    ResetString(mAltDirectoryPath, pReplace);
}
void LuaManager::SetArgumentListSize(int pSize)
{
    mTotalArgs = 0;
    ClearArgumentList();
    if(pSize < 1) return;

    mTotalArgs = pSize;
    SetMemoryData(__FILE__, __LINE__);
    mScriptArguments = (char **)starmemoryalloc(sizeof(char *) * mTotalArgs);
    memset(mScriptArguments, 0, sizeof(char *) * mTotalArgs);
}
void LuaManager::AddArgument(float pNumber)
{
    //--Overload, uses a floating-point number.  Integers passed in will convert correctly because
    //  we allocate 32 characters.
    if(mArgSlot < 0 || mArgSlot >= mTotalArgs) return;

    SetMemoryData(__FILE__, __LINE__);
    char *nBuffer = (char *)starmemoryalloc(sizeof(char) * 32);
    sprintf(nBuffer, "%f", pNumber);

    mScriptArguments[mArgSlot] = nBuffer;
    mArgSlot ++;
}
void LuaManager::AddArgument(const char *pString)
{
    //--String version, the string is COPIED.
    if(mArgSlot < 0 || mArgSlot >= mTotalArgs || !pString) return;

    SetMemoryData(__FILE__, __LINE__);
    char *nBuffer = (char *)starmemoryalloc(sizeof(char) * (strlen(pString) + 1));
    strcpy(nBuffer, pString);

    mScriptArguments[mArgSlot] = nBuffer;
    mArgSlot ++;
}
#include "DeletionFunctions.h"
void LuaManager::PushCallStack(const char *pPath)
{
    char *nString = NULL;
    ResetString(nString, pPath);
    mCallStack->AddElementAsHead("X", nString, &FreeThis);
    //fprintf(stderr, "Pushing %s\n", pPath);
}
void LuaManager::PopCallStack()
{
    mCallStack->RemoveElementI(0);
}
void LuaManager::AddTimer(const char *pIdentifier)
{
    //--Adds a new running timer. If the timer already exists, modifies it.
    time_t *rCheckTimer = (time_t *)mTimerList->GetElementByName(pIdentifier);
    if(!rCheckTimer)
    {
        SetMemoryData(__FILE__, __LINE__);
        time_t *nTimer = (time_t *)starmemoryalloc(sizeof(time_t));
        *nTimer = GetGameTime();
        mTimerList->AddElement(pIdentifier, nTimer, &FreeThis);
    }
    else
    {
        *rCheckTimer = GetGameTime();
    }
}
float LuaManager::CheckTimer(const char *pIdentifier)
{
    //--Returns the value of a running timer relative to the current time. Returns 0 if the timer doesn't exist.
    time_t *rCheckTimer = (time_t *)mTimerList->GetElementByName(pIdentifier);
    if(!rCheckTimer)
    {
        return 0.0f;
    }
    else
    {
        return GetGameTime() - (*rCheckTimer);
    }
}
float LuaManager::FinishTimer(const char *pIdentifier)
{
    //--Returns the value of a running timer relative to the current time. Returns 0 if the timer doesn't exist.
    //  If the timer does exist, removes it.
    time_t *rCheckTimer = (time_t *)mTimerList->GetElementByName(pIdentifier);
    if(!rCheckTimer)
    {
        return 0.0f;
    }
    else
    {
        float tDifference = GetGameTime() - (*rCheckTimer);
        mTimerList->RemoveElementS(pIdentifier);
        return tDifference;
    }
}

//========================================= Core Methods ==========================================
int LuaManager::BinaryGetLookup(const char *pName, int pMin, int pMax)
{
    //--Does a recursive binary search on the Tarball list, searching by pathname.  Does less NULL
    //  checking than most functions, since it's recursive.

    //--Array is empty
    if(pMax < pMin)
    {
        return -1;
    }

    //--Find the halfway point, cut the set in half
    int tMid = (pMin + pMax) / 2;

    //--Exceeds final position.  Error, not found.
    if(tMid >= pMax || tMid < 0) return -1;

    //--Check the given position.
    const char *rCheckName = mTarballScripts[tMid]->mName;

    int tRet = strcmp(pName, rCheckName);

    //--After Midpoint
    if(tRet > 0)
    {
        return BinaryGetLookup(pName, tMid+1, pMax);
    }
    //--Before midpoint
    else if(tRet < 0)
    {
        return BinaryGetLookup(pName, pMin, tMid);
    }
    //--Match
    else
    {
        return tMid;
    }
    return -1;
}
void LuaManager::ClearArgumentList()
{
    for(int i = 0; i < mTotalArgs; i ++)
    {
        free(mScriptArguments[i]);
    }
    mArgSlot = 0;
    mTotalArgs = 0;
    free(mScriptArguments);
    mScriptArguments = NULL;
}
void LuaManager::CleanPath(const char *pInBuffer, char *pOutBuffer)
{
    //--Cleans the path at pInBuffer to pOutBuffer, removing things like /../Stuff/ to make the
    //  path compatible with a tarball.  Make sure pOutBuffer has enough space!
    if(!pInBuffer || !pOutBuffer) return;

    //--Setup
    int tCurrentOutSlot = 0;

    //--For each letter...
    for(int i = 0; i < (int)strlen(pInBuffer); i ++)
    {
        //--Check for ".."
        if(pInBuffer[i] == '.' && pInBuffer[i-1] == '.')
        {
            //--Remove letters from the out buffer.
            int tSlashesFound = 0;
            for(int p = i; p >= 0; p --)
            {
                if(pInBuffer[p] == '/')
                {
                    if(tSlashesFound < 1)
                    {
                        tCurrentOutSlot --;
                        tSlashesFound ++;
                    }
                    else
                    {
                        p = -1;
                    }
                }
                else
                {
                    tCurrentOutSlot --;
                }
            }
        }
        else
        {
            pOutBuffer[tCurrentOutSlot+0] = pInBuffer[i];
            pOutBuffer[tCurrentOutSlot+1] = '\0';
            tCurrentOutSlot ++;
        }
    }

    //fprintf(stderr, "Compare %s %s\n", pInBuffer, pOutBuffer);
}
bool LuaManager::CompileLuaFile(const char *pString)
{
    //--Compiles the provided string as a Lua script, returning true on success.
    if(!pString) return false;

    //--Set these flags for the reader function.  They must be reset each pass.
    xIsFirstReaderCall = true;
    xLastDumpedSize = strlen(pString);

    //--Instance found, load it.
    lua_load(mLuaState, &LuaManager::ReaderFunction, (void *)pString, "Passed Chunk", "t");

    PushCallStack("");
    return true;
}
bool LuaManager::LoadLuaFile(const char *pPath)
{
    //--Attempts to load the requested file into Lua, returning true if it worked, and false if it
    //  did not.
    //--Note that the error during LOADING can be different than an error during EXECUTION.
    if(!pPath) return false;

    //--If we're in tarball mode, ignore all the other flags and just load it right from RAM.  This
    //  also bypasses the luaL_loadfile below, because lua_load is used instead.
    if(mIsTarballMode && !mBypassTarballOnce)
    {
        //--Clean the path up.
        char tCleanedPath[512];
        CleanPath(pPath, tCleanedPath);

        //--Do a binary search to locate the name within the array.
        //fprintf(stderr, "Looking for %s\n", tCleanedPath);
        int tTarballSlot = BinaryGetLookup(tCleanedPath, 0, mTotalTarballScripts);

        //--Normal Behavior
        if(tTarballSlot != -1)
        {
            //--Debug.
            //fprintf(stderr, "Exec slot is %i, %p\n", tTarballSlot, mTarballScripts[tTarballSlot]->mCompiledData);

            //--Instance is not compiled yet.  Run it, and dump it.
            if(!mTarballScripts[tTarballSlot]->mCompiledData)
            {
                //--Set these flags for the reader function.  They must be reset each pass.
                xIsFirstReaderCall = true;
                xLastDumpedSize = mTarballScripts[tTarballSlot]->mBinarySize;

                //--Instance found, load it.
                lua_load(mLuaState, &LuaManager::ReaderFunction, mTarballScripts[tTarballSlot]->mBinaryData, "Tarball Chunk", "b");

                //--Dump the data.
                xIsFirstWriterCall = true;
                xDumpBuffer = NULL;
                #ifdef _LUA_SHORT_
                    lua_dump(mLuaState, &LuaManager::WriterFunction, NULL);
                #else
                    lua_dump(mLuaState, &LuaManager::WriterFunction, NULL, 0);
                #endif
                mTarballScripts[tTarballSlot]->mCompiledData = xDumpBuffer;
                mTarballScripts[tTarballSlot]->mCompiledSize = xLastDumpedSize;
                //fprintf(stderr, "Compiled tarball %s to %i bytes\n", pPath, xLastDumpedSize);

                //--Report a successful load, and push the call stack.
                lua_pop(mLuaState, 1);
                xIsFirstReaderCall = true;
                xLastDumpedSize = mTarballScripts[tTarballSlot]->mCompiledSize;
                lua_load(mLuaState, &LuaManager::ReaderFunction, mTarballScripts[tTarballSlot]->mCompiledData, "Compiled Chunk", "b");

                PushCallStack(pPath);
                return true;
            }
            //--Instance is compiled!  Run it.
            else
            {
                //--Set these flags for the reader function.  They must be reset each pass.
                xIsFirstReaderCall = true;
                xLastDumpedSize = mTarballScripts[tTarballSlot]->mCompiledSize;

                //--Instance found, load it.
                int tErrorCode = lua_load(mLuaState, &LuaManager::ReaderFunction, mTarballScripts[tTarballSlot]->mCompiledData, "Tarball Chunk", "b");
                if(tErrorCode != 0)
                {
                    if(!xFailSilently) fprintf(stderr, "Lua Error %i during Loading: %s\n", tErrorCode, lua_tostring(mLuaState, -1));
                    lua_pop(mLuaState, 1);
                    return false;
                }

                //--Report a successful load, and push the call stack.
                PushCallStack(pPath);
                return true;
            }
        }
        //--If the file wasn't found in the tarball, try booting it off the hard drive.
        else
        {
            //fprintf(stderr, "Could not load file in tarball %s\n", pPath);
            int tLuaErrorCode = luaL_loadfile(mLuaState, pPath);
            if(tLuaErrorCode)
            {
                if(!xFailSilently) fprintf(stderr, "Lua Error during Loading: %s\n", lua_tostring(mLuaState, -1));
                lua_pop(mLuaState, 1);
                return false;
            }
            PushCallStack(pPath);
            return true;
        }
    }
    //--Attempt to use precompilation. This is in LuaManagerPrecompile.cc
    else if(mUsePrecompiling)
    {
        if(LoadPrecompiledScript(pPath)) return true;
    }
    //--Use neither Tarball nor Precompile. Run as normal.
    else
    {
        //fprintf(stderr, "Tarball bypassed for %s\n", pPath);
    }

    //--Note: The above ifs use returns, so anything that reaches this point requires the Tarball
    //  to be inactive/bypassed and not using precompilation mode.

    //--Flip this flag off each pass.
    mBypassTarballOnce = false;

    int tLuaErrorCode = luaL_loadfile(mLuaState, pPath);
    if(tLuaErrorCode)
    {
        //--Alt-directory case. If the original file failed to load, check if it's an alt-directory case
        //  and replace that part of the path.
        bool tResult = false;
        char tPathBuffer[512];
        static bool xIsReplaceCase = false;
        if(mCheckDirectoryPath && mAltDirectoryPath && !xIsReplaceCase)
        {
            //--Check if the paths start the same.
            if(!strncasecmp(mCheckDirectoryPath, pPath, strlen(mCheckDirectoryPath)))
            {
                //--Replace the path with that.
                int tCatPoint = (int)strlen(mCheckDirectoryPath);
                strcpy(tPathBuffer, mAltDirectoryPath);
                strcat(tPathBuffer, &pPath[tCatPoint]);

                //--Set this flag and try to load that lua file instead. Unset the flag when done. This prevents
                //  multiple attempts to use the alt-directory, which just slows things down.
                xIsReplaceCase = true;
                tResult = LoadLuaFile(tPathBuffer);
                xIsReplaceCase = false;
            }

            //--If we managed to handle it, stop here.
            if(tResult)
            {
                return true;
            }
        }

        //--If the result value is false, then either the regular load failed and there's no alt-directory,
        //  or both regular and alt-directory failed. Print an error for the alt-directory case.
        if(xIsReplaceCase)
        {
            if(!xFailSilently)
            {
                fprintf(stderr, "Lua Error: %s\n", lua_tostring(mLuaState, -1));
                fprintf(stderr, " Alt Path: %s\n", tPathBuffer);
            }
            lua_pop(mLuaState, 1);
            return false;
        }
        //--This is the case when there's no alt-directory. Most programs use this.
        else
        {
            if(!xFailSilently)
            {
                fprintf(stderr, "Lua Error during (B) Loading: %s\n", lua_tostring(mLuaState, -1));
                fprintf(stderr, " Reg Path: %s\n", pPath);
            }
            lua_pop(mLuaState, 1);
            return false;
        }
    }

    //--Everything went well normally.
    PushCallStack(pPath);
    return true;
}
bool LuaManager::LoadLuaString(const char *pString)
{
    //--Given a string which represents a lua instruction or set of instructions, executes that. Note that
    //  this cannot legally affect the call stack or other universal functions since it's not on the hard drive.
    //  Instead, the path "INTERNAL" will be used.
    if(!pString) return false;

    //--Pass it to the lua state.
    int tLuaErrorCode = luaL_loadstring(mLuaState, pString);
    if(tLuaErrorCode)
    {
        fprintf(stderr, "Lua Error during (C) string-loading: %s\n", lua_tostring(mLuaState, -1));
        fprintf(stderr, " String: %s\n", pString);
        lua_pop(mLuaState, 1);
        return false;
    }

    //--Everything went well normally.
    PushCallStack("INTERNAL");
    return true;
}
bool LuaManager::ExecuteLoadedFile()
{
    //--Attempts to execute the loaded file.  This is *deliberately* split from the loading of the
    //  file since we may want to load a file and then not execute it, for example, to save its
    //  bytecode somewhere.
    //--Returns true on success, false on error.
    int tLuaErrorCode = lua_pcall(mLuaState, 0, LUA_MULTRET, 0);
    PopCallStack();
    if(tLuaErrorCode)
    {
        if(!xFailSilently) fprintf(stderr, "Lua Error during Execution A: %s\n", lua_tostring(mLuaState, -1));
        lua_pop(mLuaState, 1);
        return false;
    }
    return true;
}
void *LuaManager::DumpLoadedFile()
{
    //--Dumps the loaded file out as compiled binary data.  Do NOT deallocate the pointer returned,
    //  just let it sit (it's not very big) as it will be cleared by the next call.
    //--Returns NULL on error.
    xLastDumpedSize = 0;
    free(xDumpBuffer);
    xDumpBuffer = NULL;

    //--Dump the data
    xIsFirstWriterCall = true;
    #ifdef _LUA_SHORT_
        lua_dump(mLuaState, &LuaManager::WriterFunction, NULL);
    #else
        lua_dump(mLuaState, &LuaManager::WriterFunction, NULL, 0);
    #endif

    //--Debug.
    //xIsFirstReaderCall = true;
    //lua_load(mLuaState, &LuaManager::ReaderFunction, xDumpBuffer, "Chunk", "bt");
    //lua_call(mLuaState, 0, 0);

    return xDumpBuffer;
}
void LuaManager::ExecuteLuaFile(const char *pPath)
{
    //--Loads a Lua file and then executes it. Clears the argument list implicitly.
    if(!pPath) return;
    ClearArgumentList();
    if(LoadLuaFile(pPath)) ExecuteLoadedFile();
}
void LuaManager::ExecuteLuaFileBypass(const char *pPath)
{
    //--Loads a Lua file and then executes it, but does not pay attention to argument allocation.
    if(!pPath) return;
    if(LoadLuaFile(pPath)) ExecuteLoadedFile();
}
void LuaManager::ExecuteLuaFile(const char *pPath, int pArgs, ...)
{
    //--Variable argument list auto-executor.  This should only be used from the C-code.
    //  Args should be "paired" with their types.  The currently accepted types are:
    //  "N" - Floating point number
    //  "S" - String
    //  Ex:  ExecuteLuaFile("Somepath.lua", 2, "N", 5.5f, "S", "This is a string")
    //--NOTE: Make sure the N values are actually floats, or cast any non-float value
    //  as a float, otherwise the result is 0.
    va_list tArgList;
    va_start(tArgList, pArgs);

    //--Get each arg and push it on the argument stack.
    const char *rType;

    SetArgumentListSize(pArgs);
    for(int i = 0; i < pArgs; i ++)
    {
        rType = va_arg(tArgList, const char *);
        if(rType[0] == 'N')
        {
            float tValue = va_arg(tArgList, double);
            AddArgument(tValue);
        }
        else if(rType[0] == 'S')
        {
            const char *rString = va_arg(tArgList, const char *);
            AddArgument(rString);
        }
        else
        {
            fprintf(stderr, "LuaManager::ExecuteLuaFile:  Error, cannot resolve type %s\n", rType);
        }
    }

    va_end(tArgList);

    //--Execute, use the bypass case since we handled the arguments.
    ExecuteLuaFileBypass(pPath);
}
void LuaManager::PushExecPop(void *pEntity, const char *pPath)
{
    //--Push the provided entity onto the DataLibrary's activity stack, execute the provided
    //  script, then pop it. This is a common feature for AI scripts.
    //--This implicitly clears the argument stack. If you want arguments, use the Bypass function below.
    //--The entity can be null, but the script can't.
    if(!pPath) return;
    ClearArgumentList();
    DataLibrary::Fetch()->PushActiveEntity(pEntity);
    ExecuteLuaFile(pPath);
    DataLibrary::Fetch()->PopActiveEntity();
}
void LuaManager::PushExecPopBypass(void *pEntity, const char *pPath)
{
    //--Same as PushExecPop, except assumes arguments were handled elsewhere.
    if(!pPath) return;
    DataLibrary::Fetch()->PushActiveEntity(pEntity);
    ExecuteLuaFileBypass(pPath);
    DataLibrary::Fetch()->PopActiveEntity();
}
void LuaManager::PushExecPop(void *pEntity, const char *pPath, int pArgs, ...)
{
    //--Variable argument list version of PushExecPop.
    if(!pPath) return;

    //--Get each arg and push it on the argument stack.
    va_list tArgList;
    va_start(tArgList, pArgs);
    const char *rType;

    //--For each argument...
    SetArgumentListSize(pArgs);
    for(int i = 0; i < pArgs; i ++)
    {
        rType = va_arg(tArgList, const char *);
        if(rType[0] == 'N')
        {
            float tValue = va_arg(tArgList, double);
            AddArgument(tValue);
        }
        else if(rType[0] == 'S')
        {
            const char *rString = va_arg(tArgList, const char *);
            AddArgument(rString);
        }
        else
        {
            fprintf(stderr, "LuaManager:PushExecPop - Error, cannot resolve type %s\n", rType);
        }
    }

    //--Finish up.
    va_end(tArgList);

    //--Execute.
    PushExecPopBypass(pEntity, pPath);
}

//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
LuaManager *LuaManager::Fetch()
{
    return Global::Shared()->gLuaManager;
}
bool LuaManager::xIsFirstWriterCall = false;
void *LuaManager::xDumpBuffer = NULL;
int LuaManager::WriterFunction(lua_State *pLuaState, const void *pSource, size_t pExpectedSize, void *pData)
{
    //--Worker function that Lua uses to dump data.  The pOutBuffer is the source of all data coming
    //  through to pDataBuffer.  The size should represent how many bytes we are adding.
    //--It is not known how many times this function is going to get called, so a concatenation
    //  is used here.

    //--Allocate space and copy the data.
    if(xIsFirstWriterCall)
    {
        xIsFirstWriterCall = false;
        free(xDumpBuffer);
        SetMemoryData(__FILE__, __LINE__);
        xDumpBuffer = starmemoryalloc(pExpectedSize);
        memcpy(xDumpBuffer, pSource, pExpectedSize);
        xLastDumpedSize = pExpectedSize;
    }
    else
    {
        //--Extend the length of the destination.
        uint32_t mOldSize = xLastDumpedSize;
        xLastDumpedSize = xLastDumpedSize + pExpectedSize;
        xDumpBuffer = realloc(xDumpBuffer, xLastDumpedSize);

        //--Buffer decoy, prevents complaints about void pointer arithmetic.
        uint8_t *rBufferDecoy = (uint8_t *)xDumpBuffer;
        memcpy(&rBufferDecoy[mOldSize], pSource, pExpectedSize);
    }

    //--Debug
    //fprintf(stderr, "Writer was called!  Passed %i bytes to %i total.\n", pExpectedSize, xLastDumpedSize);

    return 0;
}
bool LuaManager::xIsFirstReaderCall = false;
const char *LuaManager::ReaderFunction(lua_State *pLuaState, void *pData, size_t *pExpectedSize)
{
    //--Worker function, tells Lua how to read data out.  For now, just tell it the whole size of
    //  the buffer and give it the 0 address.
    if(xIsFirstReaderCall)
    {
        //fprintf(stderr, "Called reader, told about %i bytes\n", xLastDumpedSize);
        xIsFirstReaderCall = false;
        *pExpectedSize = xLastDumpedSize;
        return (const char *)pData;
    }
    //fprintf(stderr, "Called reader, NULLing off\n");
    return NULL;
}

//======================================= Command Hooking =========================================
#include "OptionsManager.h"
void LuaManager::RegisterCommands()
{
    /*| LM_Execute sString[]
      | LM sString[]
      Executes the provided lua script commands as if they had been part of a Lua script.  Effectively
      generates a lua script, compiles it, and executes it immediately.  Has the synonym LM to
      speed up typing. */
    OptionsManager::RegisterCommand("LM_Execute", &Com_LM_Execute);
    OptionsManager::RegisterCommand("LM", &Com_LM_Execute);

    /*| LM_Call sMacroName[]
      Executes the provided macro by name.  If it does not exist, nothing happens. */
    OptionsManager::RegisterCommand("LM_Call", &Com_LM_Call);
}

//=================================================================================================
//                                       Command Functions                                       ==
//=================================================================================================
#include "VirtualConsole.h"
void Com_LM_Execute(const char *pCommandLine)
{
    //LM_Execute sString[]
    //LM sString[]

    //--Echo the passed command.
    VirtualConsole::xStaticConsole->AddLine(pCommandLine);

    //--Argument check.
    int tArgs = GetTotalArguments(pCommandLine);
    if(tArgs < 1) return;

    //--First argument should be the Lua commands.
    char *tString = GetArgumentS(pCommandLine, 0);
    if(!tString) return;

    //--Pass and compile it, then execute it.
    LuaManager::Fetch()->CompileLuaFile(tString);
    LuaManager::Fetch()->ExecuteLoadedFile();

    //--Clean up.
    free(tString);
}
void Com_LM_Call(const char *pCommandLine)
{
    //LM_Call sMacroName[]

    //--Echo the passed command.
    VirtualConsole::xStaticConsole->AddLine(pCommandLine);

    //--Argument check.
    int tArgs = GetTotalArguments(pCommandLine);
    if(tArgs < 1) return;

    //--First argument should be the macro's name.
    char *tString = GetArgumentS(pCommandLine, 0);
    if(!tString) return;

    //--Argument storage.
    LuaManager *rLuaManager = LuaManager::Fetch();
    rLuaManager->ClearMacroArguments();
    for(int i = 1; i < tArgs; i ++)
    {
        char *tArg = GetArgumentS(pCommandLine, i);
        rLuaManager->AppendMacroArgument(tArg);
        free(tArg);
    }

    //--Pass to LuaManager.
    LuaManager::Fetch()->CallMacro(tString);

    //--Clean up.
    free(tString);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
//--[System Functions]
int Hook_LM_GetSystemLibrary(lua_State *L)
{
    //LM_GetSystemLibrary() (1 String)

    #if defined _ALLEGRO_PROJECT_
        lua_pushstring(L, "Allegro");
    #else
        lua_pushstring(L, "SDL");
    #endif
    return 1;
}
int Hook_LM_GetSystemOS(lua_State *L)
{
    //LM_GetSystemOS() (1 String)

    #if defined _TARGET_OS_MAC_
        lua_pushstring(L, "OSX");
    #elif defined _TARGET_OS_WINDOWS_
        lua_pushstring(L, "Windows");
    #elif defined _TARGET_OS_LINUX_
        lua_pushstring(L, "Linux");
    #endif
    return 1;
}
int Hook_LM_SetAlternateDirectory(lua_State *L)
{
    //LM_SetAlternateDirectory(sFind, sReplace)
    if(lua_gettop(L) < 2) return LuaArgError("LM_SetAlternateDirectory");
    LuaManager::Fetch()->SetAltDirectory(lua_tostring(L, 1), lua_tostring(L, 2));
    return 0;
}
int Hook_LM_GetRandomNumber(lua_State *L)
{
    //LM_GetRandomNumber(Lowest, Highest)
    if(lua_gettop(L) != 2)
    {
        LuaArgError("LM_GetRandomNumber");
        lua_pushnumber(L, 0);
        return 1;
    }

    //--Resolve.
    int tLowest  = lua_tointeger(L, 1);
    int tHighest = lua_tointeger(L, 2);

    //--Values are identical.
    if(tLowest == tHighest)
    {
        lua_pushnumber(L, tLowest);
        return 1;
    }

    //--Highest is lower than lowest. Flip them.
    if(tLowest > tHighest)
    {
        int tSwap = tHighest;
        tHighest = tLowest;
        tLowest = tSwap;
    }

    //--Roll.
    int tRange = tHighest-tLowest+1;
    int tRetval = (rand() % tRange) + tLowest;
    lua_pushnumber(L, tRetval);
    return 1;
}
int Hook_LM_SetSilenceFlag(lua_State *L)
{
    //LM_SetSilenceFlag(bFlag)
    LuaManager::xFailSilently = lua_toboolean(L, 1);
    return 0;
}
int Hook_LM_GetCallStack(lua_State *L)
{
    //LM_GetCallStack(iSlot)
    lua_pushstring(L, LuaManager::Fetch()->GetCallStack(lua_tointeger(L, 1)));
    return 1;
}
int Hook_LM_BootTarballMode(lua_State *L)
{
    //LM_BootTarballMode(sPathToTarballFile[])
    if(lua_gettop(L) != 1) return LuaArgError("LM_BootTarballMode");

    LuaManager::Fetch()->SetTarballMode(lua_tostring(L, 1));
    return 0;
}
int Hook_LM_StartTimer(lua_State *L)
{
    //LM_StartTimer(sName)
    if(lua_gettop(L) != 1) return LuaArgError("LM_StartTimer");
    LuaManager::Fetch()->AddTimer(lua_tostring(L, 1));
    return 0;
}
int Hook_LM_CheckTimer(lua_State *L)
{
    //LM_CheckTimer(sName)
    if(lua_gettop(L) != 1) return LuaArgError("LM_CheckTimer");
    lua_pushnumber(L, LuaManager::Fetch()->CheckTimer(lua_tostring(L, 1)));
    return 1;
}
int Hook_LM_FinishTimer(lua_State *L)
{
    //LM_FinishTimer(sName)
    if(lua_gettop(L) != 1) return LuaArgError("LM_FinishTimer");
    lua_pushnumber(L, LuaManager::Fetch()->FinishTimer(lua_tostring(L, 1)));
    return 1;
}

//--[Executors]
int Hook_LM_ExecuteScript(lua_State *L)
{
    //LM_ExecuteScript(Path[], ...)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("LM_ExecuteScript");

    //--Setup
    LuaManager *rLuaManager = LuaManager::Fetch();
    const char *rCallPath = lua_tostring(L, 1);
    int tNumOfArgs = tArgs - 1;

    //--Argument handling
    rLuaManager->SetArgumentListSize(tNumOfArgs);
    for(int i = 0; i < tNumOfArgs; i ++)
    {
        rLuaManager->AddArgument(lua_tostring(L, i+2));
    }

    //--Run the script
    rLuaManager->ExecuteLuaFileBypass(rCallPath);
    return 0;
}
int Hook_LM_GetNumOfArgs(lua_State *L)
{
    //LM_GetNumOfArgs()
    lua_pushnumber(L, LuaManager::Fetch()->GetTotalScriptArguments());
    return 1;
}
int Hook_LM_GetScriptArgument(lua_State *L)
{
    //LM_GetScriptArgument(iSlot)
    //LM_GetScriptArgument(iSlot, "N")
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        LuaArgError("LM_GetScriptArgument");
        lua_pushstring(L, "NULL");
        return 1;
    }

    //--Normal case
    if(tArgs == 1)
    {
        char *rString = LuaManager::Fetch()->GetScriptArgument(lua_tointeger(L, 1));
        lua_pushstring(L, rString);
    }
    //--Attempt to turn it into a number.
    else if(tArgs == 2)
    {
        float tValue = 0.0f;
        char *rString = LuaManager::Fetch()->GetScriptArgument(lua_tointeger(L, 1));
        if(rString) tValue = atof(rString);
        lua_pushnumber(L, tValue);
    }

    return 1;
}

//--[Auto-Exec]
int Hook_LM_NewMacro(lua_State *L)
{
    //LM_NewMacro(sName[], sString[])
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("LM_NewMacro");

    LuaManager::Fetch()->CreateMacro(lua_tostring(L, 1), lua_tostring(L, 2));

    return 0;
}
int Hook_LM_AppendMacro(lua_State *L)
{
    //LM_AppendMacro(sName[], sString[])
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("LM_AppendMacro");

    LuaManager::Fetch()->AppendMacro(lua_tostring(L, 1), lua_tostring(L, 2));

    return 0;
}
int Hook_LM_Call(lua_State *L)
{
    //LM_Call(sName[], ...)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("LM_Call");

    //--Argument storage.
    LuaManager *rLuaManager = LuaManager::Fetch();
    rLuaManager->ClearMacroArguments();
    for(int i = 1; i < tArgs; i ++)
    {
        rLuaManager->AppendMacroArgument(lua_tostring(L, i));
        fprintf(stderr, "Lua arg %i = %s\n", i-1, lua_tostring(L, i));
    }

    LuaManager::Fetch()->CallMacro(lua_tostring(L, 1));

    return 0;
}
