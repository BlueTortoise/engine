//--[LuaManager]
//--Stores the Lua state and provides an interface for using it, both in Lua scripts and in the
//  C++ program. Also manages the calling stack, arguments, and tarball construction.

#pragma once

#include "Definitions.h"
#include "Structures.h"

class LuaManager
{
    private:
    //--System
    static bool xIsFirstWriterCall;
    static bool xIsFirstReaderCall;
    static void *xDumpBuffer;

    //--Lua State
    lua_State *mLuaState;

    //--Passing arguments
    bool mClearArgs;
    int mTotalArgs;
    int mArgSlot;
    char **mScriptArguments;

    //--Call Stack
    SugarLinkedList *mCallStack;

    //--Precompilation
    bool mUsePrecompiling;

    //--Tarball mode
    bool mIsTarballMode;
    int mTotalTarballScripts;
    LuaTarballEntry **mTarballScripts;

    //--Auto-exec Storage.
    SugarLinkedList *mMacroArguments;
    SugarLinkedList *mMacroList;

    //--Alt-Directory Mode
    char *mCheckDirectoryPath;
    char *mAltDirectoryPath;

    //--Timer List
    SugarLinkedList *mTimerList;

    public:
    //--Public Variables
    bool mWasLastActionInError;
    bool mAlwaysBarkResolveErrors;
    int mFiringCode;
    char *mFiringString;
    bool mBypassTarballOnce;
    static bool xFailSilently;
    static int xLastDumpedSize;

    //--System
    LuaManager();
    ~LuaManager();

    //--Property Queries
    lua_State *GetLuaState();
    int GetTotalScriptArguments();
    char *GetScriptArgument(int pSlot);
    static bool DoesScriptExist(const char *pPath);
    char *GetCallStack(int pSlot);
    float GetLuaGlobalAsFloat(const char *pName);
    char *GetLuaGlobalAsString(const char *pName);

    //--Manipulators
    void SetAltDirectory(const char *pSearch, const char *pReplace);
    void SetArgumentListSize(int pSize);
    void AddArgument(float pNumber);
    void AddArgument(const char *pString);
    void SetRelativePath(const char *pPath);
    void PushCallStack(const char *pPath);
    void PopCallStack();
    void AddTimer(const char *pIdentifier);
    float CheckTimer(const char *pIdentifier);
    float FinishTimer(const char *pIdentifier);

    //--Core Methods
    int BinaryGetLookup(const char *pName, int pMin, int pMax);
    void CleanPath(const char *pInBuffer, char *pOutBuffer);
    void ClearArgumentList();
    bool CompileLuaFile(const char *pString);
    bool LoadLuaFile(const char *pPath);
    bool LoadLuaString(const char *pString);
    bool ExecuteLoadedFile();
    void *DumpLoadedFile();
    void ExecuteLuaFile(const char *pPath);
    void ExecuteLuaFileBypass(const char *pPath);
    void ExecuteLuaFile(const char *pPath, int pArgs, ...);
    void PushExecPop(void *pEntity, const char *pPath);
    void PushExecPopBypass(void *pEntity, const char *pPath);
    void PushExecPop(void *pEntity, const char *pPath, int pArgs, ...);

    //--Macro Handling
    void CreateMacro(const char *pName, const char *pStartString);
    void AppendMacro(const char *pName, const char *pAppendString);
    void CallMacro(const char *pName);
    void ClearMacroArguments();
    void AppendMacroArgument(const char *pArgument);
    char *GetArgumentAddedMacro(const char *pOriginalMacro);

    //--Precompilation
    bool LoadPrecompiledScript(const char *pPath);

    //--Tarball Mode
    void SetTarballMode(const char *pPathToTarball);

    //--Saving/Loading

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static LuaManager *Fetch();
    static int WriterFunction(lua_State *pLuaState, const void *pSource, size_t pExpectedSize, void *pDest);
    static const char *ReaderFunction(lua_State *pLuaState, void *pData, size_t *pExpectedSize);

    //--Command Hooking
    static void RegisterCommands();
};

//--[System Functions]
int Hook_LM_GetSystemLibrary(lua_State *L);
int Hook_LM_GetSystemOS(lua_State *L);
int Hook_LM_SetAlternateDirectory(lua_State *L);
int Hook_LM_GetRandomNumber(lua_State *L);
int Hook_LM_SetSilenceFlag(lua_State *L);
int Hook_LM_GetCallStack(lua_State *L);
int Hook_LM_BootTarballMode(lua_State *L);
int Hook_LM_StartTimer(lua_State *L);
int Hook_LM_CheckTimer(lua_State *L);
int Hook_LM_FinishTimer(lua_State *L);

//--[Executors]
int Hook_LM_ExecuteScript(lua_State *L);
int Hook_LM_GetNumOfArgs(lua_State *L);
int Hook_LM_GetScriptArgument(lua_State *L);

//--[Auto-Exec]
int Hook_LM_NewMacro(lua_State *L);
int Hook_LM_AppendMacro(lua_State *L);
int Hook_LM_Call(lua_State *L);

//--[Commands]
void Com_LM_Execute(const char *);
void Com_LM_Call(const char *);

