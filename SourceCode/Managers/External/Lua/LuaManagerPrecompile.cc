//--Base
#include "LuaManager.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

bool LuaManager::LoadPrecompiledScript(const char *pPath)
{
    //--[Salty's Note]
    //--This code is preserved for posterity. Unfortunately it doesn't work due to a scoping problem
    //  in Lua. If a function has another function in it, that function becomes invisible to the global
    //  stack and has problems double-compiling. The nominal solution is to run a script once as
    //  a non-function so the functions are globally compiled, or to not allow precompilation of
    //  scripts containing functions. Both of these face huge problems, not the least of which is
    //  slowdown. Until/If these problems can be fixed, this code may be usable. Until then, it's
    //  here and pending removal.

    //--Takes in the pathname for a script, and searches through the list of scripts that have
    //  been assembled. If the script has never been executed before, precompiles it as a function
    //  and then loads it atop the stack for execution.
    if(!pPath) return false;

    //--Fails if this is the SetupFuncs.lua
    //if(!strcmp(pPath, "Data/Scripts/SetupFuncs.lua")) return false;

    //--Parse the pathname and replace '/' with '_'.
    char tBuffer[256];
    strcpy(tBuffer, pPath);
    for(uint32_t i = 0; i < strlen(tBuffer); i ++)
    {
        if(tBuffer[i] == '/') tBuffer[i] = '_';
        if(tBuffer[i] == '.') tBuffer[i] = '_';
        if(tBuffer[i] == ' ') tBuffer[i] = '_';
    }

    //--First, check if the function has already been created. If not, we need to create it.
    PushCallStack(pPath);
    lua_getglobal(mLuaState, tBuffer);
    if(!lua_isfunction(mLuaState, 0))
    {
        fprintf(stderr, "Error, %s not compiled yet. Compiling...\n", tBuffer);

        //--Temporary. Offload this to the hard drive so I can look at it.
        FILE *fInfile = fopen(pPath, "r");
        if(!fInfile)
        {
            fprintf(stderr, "Error, couldn't open the file for reading!\n");
            return false;
        }

        //--Output file.
        char tFileBuffer[256];
        sprintf(tFileBuffer, "Delete/%s.txt", tBuffer);
        FILE *fOutfile = fopen(tFileBuffer, "w");
        if(!fOutfile)
        {
            fprintf(stderr, "Error, couldn't open the outfile for reading!\n");
            return false;
        }

        //--Write in the header for the outfile.
        fprintf(fOutfile, "%s = function()\n", tBuffer);

        //--Write the whole contents of the infile to the outfile after the header. No indent.
        char tLetter = fgetc(fInfile);
        while(!feof(fInfile))
        {
            fprintf(fOutfile, "%c", tLetter);
            tLetter = fgetc(fInfile);
        }

        //--Write the end of the function footer.
        fprintf(fOutfile, "\nend\n");

        //--Clean up.
        fclose(fInfile);
        fclose(fOutfile);

        //--Load the file into the execution buffer.
        int tLuaErrorCode = luaL_loadfile(mLuaState, tFileBuffer);
        if(tLuaErrorCode)
        {
            fprintf(stderr, "Lua Error during Loading: %s\n", lua_tostring(mLuaState, -1));
            lua_pop(mLuaState, 1);//Pop the error message off Lua's internal stack
            return false;
        }

        //--Execute the file. This will precompile the function, but not actually execute anything
        //  within said function.
        tLuaErrorCode = lua_pcall(mLuaState, 0, 0, 0);
        if(tLuaErrorCode)
        {
            fprintf(stderr, "Lua Error during Execution: %s\n", lua_tostring(mLuaState, -1));
            lua_pop(mLuaState, 1);
            return false;
        }

        //--Now that the function was precompiled, load it into the execution buffer.
        lua_getglobal(mLuaState, tBuffer);
    }
    else
    {
        fprintf(stderr, "Script already compiled!\n");
    }
    return true;
}
