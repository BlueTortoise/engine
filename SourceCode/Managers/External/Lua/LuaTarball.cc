//--Base
#include "LuaManager.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "SugarLumpManager.h"

void LuaManager::SetTarballMode(const char *pPathToTarball)
{
    //--Begins Tarball mode in the LuaManager, or, if NULL is passed, disables it.  If a name is
    //  passed that doesn't exist, tarball mode is turned off and a warning is barked.
    mIsTarballMode = false;
    for(int i = 0; i < mTotalTarballScripts; i ++)
    {
        free(mTarballScripts[i]);
    }
    free(mTarballScripts);
    mTarballScripts = NULL;
    mTotalTarballScripts = 0;
    if(!pPathToTarball) return;

    //--Check if the file exists. Fail silently if it doesn't.
    FILE *fCheckFile = fopen(pPathToTarball, "r");
    if(!fCheckFile) return;
    fclose(fCheckFile);

    //--Open up the tarball file using the SugarLumpManager.
    SugarLumpManager *rSLM = SugarLumpManager::Fetch();
    rSLM->Open(pPathToTarball);
    if(!rSLM->IsFileOpen())
    {
        DebugManager::ForcePrint("LuaManager:  File exists but tarball was invalid: %s\n", pPathToTarball);
        return;
    }

    //--File is open.  For now, we assume all entries within are tarballed scripts.  Get space.
    mTotalTarballScripts = rSLM->GetTotalLumps();
    SetMemoryData(__FILE__, __LINE__);
    mTarballScripts = (LuaTarballEntry **)starmemoryalloc(sizeof(LuaTarballEntry) * mTotalTarballScripts);
    for(int i = 0; i < mTotalTarballScripts; i ++)
    {
        //--Get the lump.  We can assume they are already alphabetically sorted.
        mTarballScripts[i] = rSLM->GetTarball(i);
        mTarballScripts[i]->mCompiledData = NULL;
        mTarballScripts[i]->mCompiledSize = 0;

        //--Debug.
        //fprintf(stderr, "Name of tarball %i is %s\n", i, mTarballScripts[i]->mName);
    }

    //--Compile the tarballs.
    for(int i = 0; i < mTotalTarballScripts; i ++)
    {
        //--Set these flags for the reader function.  They must be reset each pass.
        xIsFirstReaderCall = true;
        xLastDumpedSize = mTarballScripts[i]->mBinarySize;

        //--Instance found, load it.
        lua_load(mLuaState, &LuaManager::ReaderFunction, mTarballScripts[i]->mBinaryData, "Tarball Chunk", "t");

        //--Dump the data.
        xIsFirstWriterCall = true;
        xDumpBuffer = NULL;
        #ifdef _LUA_SHORT_
            lua_dump(mLuaState, &LuaManager::WriterFunction, NULL);
        #else
            //int tCatchValue = lua_dump(mLuaState, &LuaManager::WriterFunction, NULL, 1);
            lua_dump(mLuaState, &LuaManager::WriterFunction, NULL, 0);
        #endif
        mTarballScripts[i]->mCompiledData = xDumpBuffer;
        mTarballScripts[i]->mCompiledSize = xLastDumpedSize;
        if(!xDumpBuffer)
        {
            fprintf(stderr, "Warning:  There was an error in compilation of %i %s\n", i, mTarballScripts[i]->mName);
        }
        //fprintf(stderr, "%i Compiled %i tarball %s to %i bytes, %p\n", tCatchValue, i, mTarballScripts[i]->mName, xLastDumpedSize, mTarballScripts[i]->mCompiledData);

        //--Dump successful, remove the loaded data from the lua state.
        lua_pop(mLuaState, 1);
    }

    //--If we got this far, then Tarball mode was activated. Deactivate precompilation to save time.
    mUsePrecompiling = false;

    //--Flip this flag on.
    mIsTarballMode = true;
    DebugManager::ForcePrint("LuaManager:  Tarball is open!  %s\n", pPathToTarball);
}
