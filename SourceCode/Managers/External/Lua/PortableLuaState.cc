//--Base
#include "PortableLuaState.h"

//--Classes
//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

//#define PLS_DEBUG
#ifdef PLS_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPushNoLine(pFlag) DebugManager::Push(pFlag)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPrintNoLine(pString, ...) printf(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
    #define DebugPopNoLine() DebugManager::Pop()
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPushNoLine(pFlag) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPrintNoLine(pString, ...) ;
    #define DebugPop(pString, ...) ;
    #define DebugPopNoLine() ;
#endif

//=========================================== System ==============================================
PortableLuaState::PortableLuaState()
{
    //--System
    mOperationComplete = false;
    mLocalTableName = NULL;

    //--Storage
    mBaseTable = new PLSObject();
}
PortableLuaState::~PortableLuaState()
{
    free(mLocalTableName);
    delete mBaseTable;
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
int PortableLuaState::xTotalEntries = 0;
void PortableLuaState::OperateOn(lua_State *pLuaState, const char *pTableName)
{
    //--Heavy-lifting function. Reads across the table in the lua state and extracts all of its
    //  members, storing them. These objects are meant to only handle one table at a time, so this
    //  can't be used twice.
    if(mOperationComplete || !pLuaState || !pTableName) return;

    //--Flag.
    mOperationComplete = true;

    //--Statistics.
    PortableLuaState::xTotalEntries = 0;

    //--Store the table name.
    ResetString(mLocalTableName, pTableName);

    //--Current stack size. We need to know where the base table is going to get pushed.
    int tCurrentStackSize = lua_gettop(pLuaState);

    //--Push the named table.
    DebugPush(true, "== Pushing global for storage: %s ==\n", pTableName);
    lua_getglobal(pLuaState, pTableName);
    tCurrentStackSize ++;

    //--Not a table. Ignore it.
    if(lua_istable(pLuaState, tCurrentStackSize) == 0)
    {
        mOperationComplete = false;
        lua_pop(pLuaState, 1);
        DebugPop("== Error: PortableLuaState was ordered to save something other than a table. Failing. ==\n");
        return;
    }

    //--Base object creates its listing.
    PortableLuaState::xTotalEntries ++;
    mBaseTable->mIsBaseTable = true;
    mBaseTable->SetIdentifierString(pTableName);
    mBaseTable->BecomeTable();

    //--Object already pushed. Begin traversal.
    mBaseTable->Traverse(pLuaState);

    //--Pop the stack to clean up the Lua state.
    lua_pop(pLuaState, 1);
    tCurrentStackSize --;
    DebugPop("== Storage operation completed. Total entries: %i ==\n", PortableLuaState::xTotalEntries);

    //--Text file dump.
}
void PortableLuaState::WriteToFile(const char *pFileName)
{
    //--Write the PLS to a single binary file. Does nothing if the operation wasn't completed.
    if(!pFileName || !mOperationComplete) return;

    //--Write the whole thing to a single buffer.
    SugarAutoBuffer *tOutputBuffer = new SugarAutoBuffer();
    mBaseTable->RenderToBinary(tOutputBuffer);

    //--Write the buffer to a binary file.
    FILE *fDummy = fopen(pFileName, "wb");
    int tLength = tOutputBuffer->GetCursor();
    uint8_t *rRawData = tOutputBuffer->GetRawData();
    fwrite(rRawData, tLength, sizeof(uint8_t), fDummy);

    //--Clean.
    fclose(fDummy);
    delete tOutputBuffer;
}
void PortableLuaState::ReadFromFile(const char *pFileName)
{
    //--Given a file name, opens it as a virtual file and reconstructs a valid Portable Lua State.
    if(!pFileName || mOperationComplete) return;

    //--Open it.
    VirtualFile *fInfile = new VirtualFile(pFileName, false);
    if(!fInfile->IsReady()) return;

    //--Give the base entry the file. It's recursive, it'll do the rest.
    delete mBaseTable;
    mBaseTable = PLSObject::ReadFromBinary(fInfile);
    mBaseTable->mIsBaseTable = true;
    ResetString(mLocalTableName, "gzTextVar");

    //--Flag.
    mOperationComplete = true;

    //--Reconstruct the file.
    Reconstruct();

    //--Clear.
    delete fInfile;
}
void PortableLuaState::Reconstruct()
{
    //--Attempts to reconstruct the original table as if it was a series of lua commands. These commands
    //  are then printed to the provided file. If the file doesn't exist, does nothing.
    //--You are expected to open/close/manage the file, this just prints to it.
    if(!mOperationComplete) return;

    //--Order the table to disgorge its contents.
    SugarAutoBuffer *tOutBuffer = new SugarAutoBuffer();
    mBaseTable->RenderToBuffer(mLocalTableName, tOutBuffer);

    //--Now send it to the Lua Manager.
    uint8_t *rData = tOutBuffer->GetRawData();
    LuaManager::Fetch()->LoadLuaString((char *)rData);
    LuaManager::Fetch()->ExecuteLoadedFile();

    //--Clean.
    delete tOutBuffer;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//==================================== PLS Object Functions =======================================
void PLSObject::SetIdentifierNumeric(float pIdentifier)
{
    mUsesNumericIdentifer = true;
    mIdentifierNumeric = pIdentifier;
}
void PLSObject::SetIdentifierString(const char *pName)
{
    mUsesNumericIdentifer = false;
    mIdentifierNumeric = 0;
    ResetString(mIdentifierString, pName);
}
void PLSObject::BecomeNumeric(float pValue)
{
    mType = PLS_NUMERIC;
    mNumeric = pValue;
}
void PLSObject::BecomeString(const char *pValue)
{
    mType = PLS_STRING;
    ResetString(mString, pValue);
}
void PLSObject::BecomeTable()
{
    mType = PLS_TABLE;
    mTableData = new SugarLinkedList(true);
}
void PLSObject::BecomeBoolean(bool pBoolean)
{
    mType = PLS_BOOLEAN;
    mBoolean = pBoolean;
}
void PLSObject::Traverse(lua_State *pLuaState)
{
    //--Assuming that the table in question is on the stack, store all its elements. This includes
    //  running traverses on sub-tables recursively.
    if(mType != PLS_TABLE || !pLuaState) return;

    //--Traversal. Push the first key.
    lua_pushnil(pLuaState);
    while(lua_next(pLuaState, -2) != 0)
    {
        //--Due to lua_next(), the key-value pair is pushed. The key is pushed first. The stack goes down
        //  so the key is now at stack level -2, and the value is at -1.
        PortableLuaState::xTotalEntries ++;

        //--Create a new entry for this.
        PLSObject *nObject = new PLSObject();
        mTableData->AddElement("X", nObject, &PLSObject::DeleteThis);

        //--Now let's see the key.
        if(lua_isnumber(pLuaState, -2) == 1)
        {
            //--Debug Note: The majority of the time, indices are integers. Lua legally allows floating-point
            //  numbers. We print the integer for simplicity.
            DebugPrint("%i: ", lua_tointeger(pLuaState, -2));
            nObject->SetIdentifierNumeric(lua_tonumber(pLuaState, -2));
        }
        //--String Value:
        else if(lua_isstring(pLuaState, -2) == 1)
        {
            DebugPrint("%s: ", lua_tostring(pLuaState, -2));
            nObject->SetIdentifierString(lua_tostring(pLuaState, -2));
        }
        //--Other Value. Illegal!
        else
        {
            DebugManager::ForcePrint("== Warning - Unhandled key type! ==\n");
        }

        //--Boolean Value:
        if(lua_isboolean(pLuaState, -1) == 1)
        {
            DebugPrintNoLine("Bol.\n");
            nObject->BecomeBoolean(lua_toboolean(pLuaState, -1));
        }
        //--Numeric Value:
        else if(lua_isnumber(pLuaState, -1) == 1)
        {
            DebugPrintNoLine("Num: %f\n", lua_tonumber(pLuaState, -1));
            nObject->BecomeNumeric(lua_tonumber(pLuaState, -1));
        }
        //--String Value:
        else if(lua_isstring(pLuaState, -1) == 1)
        {
            DebugPrintNoLine("Str: %s\n", lua_tostring(pLuaState, -1));
            nObject->BecomeString(lua_tostring(pLuaState, -1));
        }
        //--Table. Needs to iterate!
        else if(lua_istable(pLuaState, -1) == 1)
        {
            //--Object becomes a table.
            DebugPrintNoLine("Table:\n");
            DebugPushNoLine(true);
            nObject->BecomeTable();
            nObject->Traverse(pLuaState);
            DebugPopNoLine();
        }
        //--Other Value. Assume these are nil. Nothing needs to be done.
        else
        {
            fprintf(stderr, "Unhandled type!\n");
        }

        //--Once we're done with it, pop the value off. The lua_next() function assumes the -1
        //  stack element is the 'key' for iterating purposes, which it is since it's at -2.
        lua_pop(pLuaState, 1);
    }
}

PLSObject *PLSObject::ReadFromBinary(VirtualFile *fInfile)
{
    //--[Documentation]
    //--Static function. Reads from the given autobuffer and reconstructs the data set.
    if(!fInfile) return NULL;

    //--Create a new object to handle things.
    PLSObject *nObject = new PLSObject();

    //--[Identifier Handling]
    //--Read a uint8_t to get the type.
    uint8_t tType = 0;
    fInfile->Read(&tType, sizeof(uint8_t), 1);

    //--Numeric index.
    if(tType == 1)
    {
        float tNumericIndicator = 0.0f;
        fInfile->Read(&tNumericIndicator, sizeof(float), 1);
        nObject->SetIdentifierNumeric(tNumericIndicator);
    }
    //--String index.
    else if(tType == 2)
    {
        //--Get length.
        int16_t tLength = 0;
        fInfile->Read(&tLength, sizeof(int16_t), 1);
        if(tLength < 1)
        {
            fprintf(stderr, "Warning: Identifier string was not longer enough.\n");
            delete nObject;
            return NULL;
        }

        //--Read.
        SetMemoryData(__FILE__, __LINE__);
        char *tBuffer = (char *)starmemoryalloc(sizeof(char) * (tLength + 1));
        fInfile->Read(tBuffer, sizeof(char), tLength);
        tBuffer[tLength] = '\0';

        //--Set.
        nObject->SetIdentifierString(tBuffer);

        //--Clean.
        free(tBuffer);
    }
    else
    {
        fprintf(stderr, "Warning: Unhandled identifier %i.\n", tType);
        delete nObject;
        return NULL;
    }

    //--[Data Handling]
    //--Read a uint8_t to get the data type.
    fInfile->Read(&tType, sizeof(uint8_t), 1);

    //--Boolean type.
    if(tType == 6)
    {
        bool tIsTrue = false;
        char tLetter = '0';
        fInfile->Read(&tLetter, sizeof(char), 1);
        if(tLetter == '1') tIsTrue = true;
        nObject->BecomeBoolean(tIsTrue);
    }
    //--Numeric type.
    else if(tType == 3)
    {
        float tValue = 0.0f;
        fInfile->Read(&tValue, sizeof(float), 1);
        nObject->BecomeNumeric(tValue);
    }
    //--String type.
    else if(tType == 4)
    {
        //--Get length.
        int16_t tLength = 0;
        fInfile->Read(&tLength, sizeof(int16_t), 1);

        //--A zero-length string is legal in lua, but it doesn't involve reading anything.
        if(tLength >= 1)
        {
            SetMemoryData(__FILE__, __LINE__);
            char *tBuffer = (char *)starmemoryalloc(sizeof(char) * (tLength + 1));
            fInfile->Read(tBuffer, sizeof(char), tLength);
            tBuffer[tLength] = '\0';
            nObject->BecomeString(tBuffer);
            free(tBuffer);
        }
        else
        {
            nObject->BecomeString("");
        }
    }
    //--Table. Needs to construct and iterate.
    else if(tType == 5)
    {
        //--Read how many entries we need.
        int32_t tListSize = 0;
        fInfile->Read(&tListSize, sizeof(int32_t), 1);

        //--Become a table object.
        nObject->BecomeTable();

        //--Iterate. Each becomes a subobject.
        for(int i = 0; i < tListSize; i ++)
        {
            PLSObject *nSubObject = PLSObject::ReadFromBinary(fInfile);
            nObject->mTableData->AddElement("X", nSubObject, &PLSObject::DeleteThis);
        }
    }
    else
    {
        fprintf(stderr, "Warning: Unhandled value.\n");
    }

    //--Pass back the object.
    return nObject;
}
void PLSObject::RenderToBuffer(const char *pLeadIn, SugarAutoBuffer *pBuffer)
{
    //--As above, reconstructs this object to the given outfile. Numeric and String objects just
    //  write themselves while tables will recursively iterate.
    //--The pLeadIn is the path of this object. In Lua, objects use [] and . to denote themselves
    //  as members of a table. The pLeadIn is just bolted on to the front to become part of whatever
    //  object owns it.
    if(!pLeadIn || !pBuffer) return;

    //--Construct a string from a buffer.
    char tLocalName[256];
    pBuffer->AppendStringWithoutNull(pLeadIn);
    strcpy(tLocalName, pLeadIn);

    //--If we are the base table, we don't append any identifier stuff.
    if(mIsBaseTable)
    {
    }
    //--If we are a numeric index:
    else if(mUsesNumericIdentifer)
    {
        //--Setup.
        char tNumericBuf[32];

        //--If the integer value is exactly equal to the floating-point value, we can simplify this a bit.
        //  This makes things a bit easier to read.
        if((int)mIdentifierNumeric == mIdentifierNumeric)
        {
            sprintf(tNumericBuf, "%i", (int)mIdentifierNumeric);
        }
        //--We need full accuracy.
        else
        {
            sprintf(tNumericBuf, "%f", mIdentifierNumeric);
        }

        //--Do the boring stuff.
        pBuffer->AppendStringWithoutNull("[");
        pBuffer->AppendStringWithoutNull(tNumericBuf);
        pBuffer->AppendStringWithoutNull("]");
        strcat(tLocalName, "[");
        strcat(tLocalName, tNumericBuf);
        strcat(tLocalName, "]");
    }
    //--String index.
    else
    {
        pBuffer->AppendStringWithoutNull(".");
        pBuffer->AppendStringWithoutNull(mIdentifierString);
        strcat(tLocalName, ".");
        strcat(tLocalName, mIdentifierString);
    }

    //--Boolean type.
    if(mType == PLS_BOOLEAN)
    {
        //--Common.
        pBuffer->AppendStringWithoutNull(" = ");

        //--Boolean cases.
        if(mBoolean)
        {
            pBuffer->AppendStringWithoutNull("true");
        }
        else
        {
            pBuffer->AppendStringWithoutNull("false");
        }

        //--Common.
        pBuffer->AppendStringWithoutNull("\n");

        //--Send the buffer.
        //fprintf(fOutfile, (char *)pBuffer->GetRawData());
    }
    //--Numeric type.
    else if(mType == PLS_NUMERIC)
    {
        //--Setup.
        char tNumericBuf[32];

        //--As above, if an integer will suffice, write an integer.
        if((int)mNumeric == mNumeric)
        {
            sprintf(tNumericBuf, "%i", (int)mNumeric);
        }
        //--We need full accuracy.
        else
        {
            sprintf(tNumericBuf, "%f", mNumeric);
        }

        //--Blah, blah.
        pBuffer->AppendStringWithoutNull(" = ");
        pBuffer->AppendStringWithoutNull(tNumericBuf);
        pBuffer->AppendStringWithoutNull("\n");

        //--Send the buffer.
        //fprintf(fOutfile, (char *)tBuffer->GetRawData());
    }
    //--String type.
    else if(mType == PLS_STRING)
    {
        pBuffer->AppendStringWithoutNull(" = \"");
        pBuffer->AppendStringWithoutNull(mString);
        pBuffer->AppendStringWithoutNull("\"\n");

        //--Send the buffer.
        //fprintf(fOutfile, (char *)pBuffer->GetRawData());
    }
    //--Table. Needs to construct and iterate.
    else if(mType == PLS_TABLE)
    {
        //--Construction. Add a construction case, then iterate across our local data and send it all.
        pBuffer->AppendStringWithoutNull(" = {}\n");

        //--Begin iteration. Note that this, too, can recurse.
        PLSObject *rSubObject = (PLSObject *)mTableData->PushIterator();
        while(rSubObject)
        {
            //--Render!
            rSubObject->RenderToBuffer(tLocalName, pBuffer);

            //--Next.
            rSubObject = (PLSObject *)mTableData->AutoIterate();
        }
    }
}
void PLSObject::RenderToBinary(SugarAutoBuffer *pBuffer)
{
    //--Renders to an autobuffer. These are much smaller than lua text files and can be more
    //  quickly constructed and used.
    if(!pBuffer) return;

    //--If we are the base table, append the name string.
    if(mIsBaseTable)
    {
        pBuffer->AppendUInt8(2);
        pBuffer->AppendStringWithLen(mIdentifierString);
    }
    //--If we are a numeric index:
    else if(mUsesNumericIdentifer)
    {
        pBuffer->AppendUInt8(1);
        pBuffer->AppendFloat(mIdentifierNumeric);
    }
    //--String index.
    else
    {
        pBuffer->AppendUInt8(2);
        pBuffer->AppendStringWithLen(mIdentifierString);
    }

    //--Boolean type.
    if(mType == PLS_BOOLEAN)
    {
        pBuffer->AppendUInt8(6);
        if(mBoolean)
            pBuffer->AppendCharacter('1');
        else
            pBuffer->AppendCharacter('0');
    }
    //--Numeric type.
    else if(mType == PLS_NUMERIC)
    {
        pBuffer->AppendUInt8(3);
        pBuffer->AppendFloat(mNumeric);
    }
    //--String type.
    else if(mType == PLS_STRING)
    {
        pBuffer->AppendUInt8(4);
        pBuffer->AppendStringWithLen(mString);
    }
    //--Table. Needs to construct and iterate.
    else if(mType == PLS_TABLE)
    {
        //--Construction. The buffer does not get sent directly, it is the lead-in.
        pBuffer->AppendUInt8(5);
        pBuffer->AppendInt32(mTableData->GetListSize());

        //--Begin iteration. Note that this, too, can recurse.
        PLSObject *rSubObject = (PLSObject *)mTableData->PushIterator();
        while(rSubObject)
        {
            //--Render!
            rSubObject->RenderToBinary(pBuffer);

            //--Next.
            rSubObject = (PLSObject *)mTableData->AutoIterate();
        }
    }
    else
    {
        fprintf(stderr, "Warning: Unhandled data type, somehow.\n");
    }
}

