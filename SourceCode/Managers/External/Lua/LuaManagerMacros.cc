//--Base
#include "LuaManager.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers

void LuaManager::CreateMacro(const char *pName, const char *pStartString)
{
    //--Creates and stores a new macro.  If the macro already exists, does nothing.
    if(!pName || !pStartString) return;

    //--Check for existence.
    void *rCheckPtr = mMacroList->GetElementByName(pName);
    if(rCheckPtr) return;

    //--Create a new macro.
    SetMemoryData(__FILE__, __LINE__);
    char **nPtr = (char **)starmemoryalloc(sizeof(char **));
    *nPtr = NULL;
    ResetString(*nPtr, pStartString);
    mMacroList->AddElement(pName, nPtr, &FreeDubChar);
}
void LuaManager::AppendMacro(const char *pName, const char *pAppendString)
{
    //--If the macro exists, appends the provided string onto its end.
    if(!pName || !pAppendString) return;

    //--Get existing macro.
    char **rMacro = (char **)mMacroList->GetElementByName(pName);
    if(!rMacro) return;

    //--Append it, adding a carriage return between the lines.
    char *rCurrentString = *rMacro;
    uint32_t tCurrentLen = strlen(rCurrentString);
    uint32_t tAppendLen = strlen(pAppendString);
    SetMemoryData(__FILE__, __LINE__);
    char *nNewString = (char *)starmemoryalloc(sizeof(char) * (tCurrentLen + tAppendLen + 1 + 1));
    strcpy(nNewString, rCurrentString);
    strcat(nNewString, "\n");
    strcat(nNewString, pAppendString);

    //--Clean and replace.
    free(rCurrentString);
    *rMacro = nNewString;
}
void LuaManager::CallMacro(const char *pName)
{
    //--If the requested macro exists, load it into Lua's memory and execute it.
    if(!pName) return;

    //--Get existing macro.
    char **rMacro = (char **)mMacroList->GetElementByName(pName);
    if(!rMacro) return;

    char *tArgumentAddedMacro = GetArgumentAddedMacro(*rMacro);

    //--Compile and Execute.
    CompileLuaFile(tArgumentAddedMacro);
    ExecuteLoadedFile();

    //--Clean up.
    free(tArgumentAddedMacro);
}
void LuaManager::ClearMacroArguments()
{
    //--Cleans off the argument list. Called before each macro call.
    mMacroArguments->ClearList();
}
void LuaManager::AppendMacroArgument(const char *pArgument)
{
    //--Appends a new argument to the end of the RLL. Garbage collection is handled automatically.
    SetMemoryData(__FILE__, __LINE__);
    char **nPtr = (char **)starmemoryalloc(sizeof(char **));
    *nPtr = NULL;
    ResetString(*nPtr, pArgument);
    mMacroArguments->AddElement("X", nPtr, &FreeDubChar);
}
char *LuaManager::GetArgumentAddedMacro(const char *pOriginalMacro)
{
    //--[Function]
    //--Returns a heap-allocated string which represents the macro, after all the %0's and %1's have
    //  been replaced with arguments. If an argument was not passed, "NULL" is put in its place.
    //--You are responsible for deallocating the resulting string. The original macro is unharmed.
    //--NULL is returned on failure.

    //--[Details]
    //--Arguments passed into the macro will replace instances of %0, %1, etc.
    //--The % rules are as follows: Once a % is spotted, all numerics afterwards are evaluated until
    //  a non-numeric is spotted, OR a '|' is spotted, which terminates the sequence.
    //--A backslash will ignore the % sign. Ex: "Ignore this \%1" -> "Ignore this %1"
    //--A double-backslash prints a backslash, as is the usual for printf-like functions.
    //--Arguments passed will all be strings regardless of their original types, as Lua doesn't
    //  care either way.
    if(!pOriginalMacro) return NULL;

    //--Setup. A single-pass technique is used for speed, as the returned string will usually be
    //  deallocated shortly after being executed.
    uint32_t tOriginalLen = strlen(pOriginalMacro);
    uint32_t tNewLen = tOriginalLen * 2;
    SetMemoryData(__FILE__, __LINE__);
    char *nNewMacro = (char *)starmemoryalloc(sizeof(char) * tNewLen);

    //--Other variables.
    bool tIgnoreNextChar = false;
    int tCurChar = 0;

    //--Begin parsing.
    for(uint16_t i = 0; i < tOriginalLen; i ++)
    {
        //--Check for special characters. '\' blocks special letters.
        if(pOriginalMacro[i] == '\\' && pOriginalMacro[i+1] == '%' && !tIgnoreNextChar)
        {
            tIgnoreNextChar = true;
        }
        //--Argument sequence.
        else if(pOriginalMacro[i] == '%' && !tIgnoreNextChar)
        {
            //--Scan ahead to determine the length of the escape sequence.
            uint32_t tLastLetter = 0;
            for(uint16_t p = i+1; p < tOriginalLen; p ++)
            {
                if(pOriginalMacro[p] < '0' || pOriginalMacro[p] > '9')
                {
                    tLastLetter = p;
                    //fprintf(stderr, " Last letter is in slot %i, '%c'\n", p, pOriginalMacro[p]);
                    break;
                }
            }

            //--We know how many letters to parse. Put these in a buffer.
            int tMacroSlot = 0;
            char tBuffer[32];
            for(uint16_t p = i+1; p < tLastLetter; p ++)
            {
                tBuffer[p-(i+1)+0] = pOriginalMacro[p];
                tBuffer[p-(i+1)+1] = '\0';
                //fprintf(stderr, " Copied %c into buffer, slot %i\n", tBuffer[p-(i+1)+0], p-(i+1)+0);
            }
            sscanf(tBuffer, "%i", &tMacroSlot);
            //fprintf(stderr, "The buffer |%s| is slot %i\n", tBuffer, tMacroSlot);

            //--Scan the original-string cursor ahead.
            i = tLastLetter-1;
            if(pOriginalMacro[i+1] == '|') i ++;

            //--Retrieve the argument.
            char **rArgument = (char **)mMacroArguments->GetElementBySlot(tMacroSlot);

            //--Argument was okay, insert it.
            if(rArgument && *rArgument)
            {
                char *rString = *rArgument;
                fprintf(stderr, "String was %s\n", rString);
                for(uint16_t p = 0; p < strlen(rString); p ++)
                {
                    nNewMacro[tCurChar+0] = rString[p];
                    nNewMacro[tCurChar+1] = '\0';
                    tCurChar ++;
                }
            }
            //--Argument was bad in some way, insert "NULL" instead.
            else
            {
                nNewMacro[tCurChar+0] = 'N';
                nNewMacro[tCurChar+1] = 'U';
                nNewMacro[tCurChar+2] = 'L';
                nNewMacro[tCurChar+3] = 'L';
                nNewMacro[tCurChar+4] = '\0';
                tCurChar += 4;
            }
        }
        //--No special characters, copy the letter and reset flags.
        else
        {
            nNewMacro[tCurChar+0] = pOriginalMacro[i];
            nNewMacro[tCurChar+1] = '\0';
            tCurChar ++;
            tIgnoreNextChar = false;
        }
    }

    //--Clean up.
    return nNewMacro;
}
