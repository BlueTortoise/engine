//--Base
#include "MapManager.h"

//--Classes
#include "RootLevel.h"

//--CoreClasses
//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "EntityManager.h"

int MapManager::xIncrementor = 1;
bool MapManager::GetClipAt(int pXPos, int pYPos, int pZPos)
{
    //--Returns whether or not the given position is "clipped", that is, Entities can or can't move
    //  through it.
    //--Default return value.  This is returned at edge cases, or if there is no map loaded.
    if(mActiveLevel) return mActiveLevel->GetClipAt(pXPos, pYPos, pZPos);
    return false;
}
bool MapManager::GetClipAt(int pXPos, int pYPos)
{
    //--Overload, assumes the Z Position is 0 or unused.
    return GetClipAt(pXPos, pYPos, 0);
}
bool MapManager::CanMoveToH(int pXPosition, int pTop, int pBot)
{
    //--Returns false if the line in questions has a clip on it, or true if it does not.
    //  This function checks a vertical line from (pXPosition, pTop) to (pXPosition, pBot).
    for(int y = pTop; y < pBot; y += xIncrementor)
    {
        if(GetClipAt(pXPosition, y)) return false;
    }
    return true;
}
bool MapManager::CanMoveToV(int pYPosition, int pLft, int pRgt)
{
    //--Returns false if the line in questions has a clip on it, or true if it does not.
    //  This function checks a horizontal line from (pLft, pYPosition) to (pRgt, pYPosition).
    for(int x = pLft; x < pRgt; x += xIncrementor)
    {
        if(GetClipAt(x, pYPosition)) return false;
    }
    return true;
}
bool MapManager::IsTouchingGround(int pYPosition, int pLft, int pRgt)
{
    //--Given the Entity's 'bottom', returns true if there's a clip along the bottom + 1.
    for(int x = pLft; x < pRgt; x += xIncrementor)
    {
        if(GetClipAt(x, pYPosition)) return true;
    }
    return false;
}
bool MapManager::IsTouchingGround(ThreeDimensionReal pDimensions)
{
    //--Overload that uses the positions directly.
    return IsTouchingGround(pDimensions.mBot, pDimensions.mLft, pDimensions.mRgt);
}

//--[Macros]
//--Quick-access functions that use the above CanMoveTo functions with presets.
bool MapManager::IsLftBlocked(ThreeDimensionReal pDimensions)
{
    return !CanMoveToH(pDimensions.mLft-1.0f, pDimensions.mTop, pDimensions.mBot);
}
bool MapManager::IsTopBlocked(ThreeDimensionReal pDimensions)
{
    return !CanMoveToV(pDimensions.mTop-1.0f, pDimensions.mLft, pDimensions.mRgt);
}
bool MapManager::IsRgtBlocked(ThreeDimensionReal pDimensions)
{
    return !CanMoveToH(pDimensions.mRgt+0, pDimensions.mTop, pDimensions.mBot);
}
bool MapManager::IsBotBlocked(ThreeDimensionReal pDimensions)
{
    return !CanMoveToV(pDimensions.mBot+0, pDimensions.mLft, pDimensions.mRgt);
}
