//--Base
#include "MapManager.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//========================================= Lua Hooking ===========================================
void MapManager::HookToLuaState(lua_State *pLuaState)
{
    /* MapM_SetOneGame(bFlag)
       Sets the only-one-game flag. Some title screens use this to exit the program directly. */
    lua_register(pLuaState, "MapM_SetOneGame", &Hook_MapM_SetOneGame);

    /* MapM_SetOverlayColor(fRed, fGreen, fBlue, fAlpha)
       Sets the color that is rendered over top of the map.  It's a quad of the matching color and
       has blending turned on.  Pass a negative fAlpha to turn overlays off altogether. */
    lua_register(pLuaState, "MapM_SetOverlayColor", &Hook_MapM_SetOverlayColor);

    /* MapM_Shake(fRadius, fSeconds)
       Shakes the screen with the provided radius for the provided duration. */
    lua_register(pLuaState, "MapM_Shake", &Hook_MapM_Shake);

    /* MapM_PushMenuStackHead()
       Pushes whatever is currently atop the Menu Stack onto the Activity Stack. Can legally push NULL! */
    lua_register(pLuaState, "MapM_PushMenuStackHead", &Hook_MapM_PushMenuStackHead);

    /* MapM_BackToTitle()
       Flags the program to return to the Title Screen. */
    lua_register(pLuaState, "MapM_BackToTitle", &Hook_MapM_BackToTitle);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_MapM_SetOneGame(lua_State *L)
{
    //MapM_SetOneGame(bFlag)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("MapM_SetOneGame");

    MapManager::xOnlyOneGame = lua_toboolean(L, 1);
    return 0;
}
int Hook_MapM_SetOverlayColor(lua_State *L)
{
    //MapM_SetOverlayColor(fRed, fGreen, fBlue, fAlpha)
    int tArgs = lua_gettop(L);
    if(tArgs != 4) return LuaArgError("MapM_SetOverlayColor");

    MapManager::Fetch()->SetOverlayColor(lua_tonumber(L, 1), lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    return 0;
}
int Hook_MapM_Shake(lua_State *L)
{
    //MapM_Shake(fRadius, fSeconds)
    int tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("MapM_Shake");

    MapManager::Fetch()->BeginShake(lua_tonumber(L, 1), lua_tonumber(L, 2) * SECONDSTOTICKS);
    return 0;
}
int Hook_MapM_PushMenuStackHead(lua_State *L)
{
    //MapM_PushMenuStackHead()
    void *rPtr = MapManager::Fetch()->GetMenuStackHead();
    DataLibrary::Fetch()->PushActiveEntity(rPtr);
    return 0;
}
int Hook_MapM_BackToTitle(lua_State *L)
{
    //MapM_BackToTitle()
    MapManager::Fetch()->mBackToTitle = true;
    return 0;
}
