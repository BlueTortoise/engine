//--[CutsceneManager]
//--Used exclusively for Adventure Mode, overrides control of the program while a cutscene is playing. Entities
//  will still update as normal but the player's controls are locked out and most entities will respond to
//  instructions instead. This Manager holds the instructions, it's up to the other entities (usually AdventureLevels)
//  to act them out and determine break conditions.
//--Note that while a CutsceneManager exists explicitly and can be safely Fetch()'d, this does not preclude the use
//  of additional CutsceneManagers to handle threaded events. NPCs can be made to path around using this technique,
//  but the engine provides no inherent support for it.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "SugarLinkedList.h"

//--[Local Structures]
typedef struct ParallelScriptPack
{
    //--Base.
    char *mRefresherScript;

    //--Events.
    SugarLinkedList *mEventList;

    //--Methods
    void Initialize()
    {
        mRefresherScript = NULL;
        mEventList = new SugarLinkedList(true);
    }
    static void DeleteThis(void *pPtr)
    {
        ParallelScriptPack *rStruct = (ParallelScriptPack *)pPtr;
        free(rStruct->mRefresherScript);
        delete rStruct->mEventList;
        free(rStruct);
    }
}ParallelScriptPack;

//--[Local Definitions]
//--[Classes]
class CutsceneManager
{
    private:
    //--System
    //--Storage
    SugarLinkedList *mEventList;

    //--Bucketed Addition
    bool mIsBucketedAddition;
    bool mBypassBucket;
    SugarLinkedList *mBucketedList;

    //--Drop List.
    SugarLinkedList *mDropList;

    //--Parallel Simultaneous Scripts
    bool mClearAllParallelLists;
    bool mSuspendActiveScript;
    bool mRemoveActiveScript;
    ParallelScriptPack *rActivePackage;
    SugarLinkedList *mParallelScriptList;
    SugarLinkedList *mSuspendedParallelScripts;
    SugarLinkedList *mParallelClearList;

    protected:

    public:
    //--System
    CutsceneManager();
    ~CutsceneManager();

    //--Public Variables
    static bool xFlag;

    //--Property Queries
    bool HasAnyEvents();

    //--Manipulators
    void CreateParallelScript(const char *pName, const char *pPath);
    void SuspendParallelScript(const char *pName);
    void SuspendActiveScript();
    void UnsuspendParallelScript(const char *pName);
    void RemoveParallelScript(const char *pName);
    void RemoveActiveScript();
    void ClearParallelScripts();

    //--Core Methods
    void RegisterEvent(RootEvent *pEvent);
    RootEvent *CreateBlocker();
    void DropAllEvents();

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();
    void UpdateParallelEvents();
    void UpdateParallelPackage(ParallelScriptPack *pPack);
    void DropListAtEndOfTick();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static CutsceneManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Cutscene_CreateEvent(lua_State *L);
int Hook_Cutscene_HandleParallel(lua_State *L);
