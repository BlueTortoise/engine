//--Base
#include "AudioPackage.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "AudioManager.h"
#include "DebugManager.h"

//=========================================== System ==============================================
AudioPackage::AudioPackage()
{
    //--[AudioPackage]
    //--System
    mIsReady = false;
    mLocalVolume = 1.0f;
    mGlobalVolume = 1.0f;

    //--Bass Only
    #if defined _BASS_AUDIO_
        mHandle = 0;
        mTempoHandle = 0;
    #endif

    //--FMOD Only
    #if defined _FMOD_AUDIO_
        mSound = NULL;
        rSoundToPlay = NULL;
        rChannel = NULL;
        mSubSoundsTotal = 0;
        mStartLoop = 0.0f;
        mEndLoop = 0.0f;
    #endif

    //--Sample
    mIsSample = false;

    //--Stream
    mIsStream = false;

    //--Extra Data
    mStartLoopTime = 0.0f;
    mEndLoopTime = 0.0f;
    mExtraData = NULL;
}
AudioPackage::~AudioPackage()
{
    Stop();
    free(mExtraData);
}
void AudioPackage::DeleteThis(void *pPtr)
{
    delete ((AudioPackage *)pPtr);
}

//====================================== Property Queries =========================================
float AudioPackage::GetDuration()
{
    //--Duration returned is in seconds.  For Streams, this is dynamic, and networked streams may
    //  report incorrect data.

    //--TODO
    return 0.0f;
}
bool AudioPackage::IsPlaying()
{
    //--[Common]
    if(!mIsReady) return false;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
    {
        //--Fail if the handle is not set.
        if(!mHandle) return false;

        //--Streams can only play once, and override each other.
        int tCheck = BASS_ACTIVE_STOPPED;
        if(mIsStream && mTempoHandle)
        {
            //--Get and check. We count paused as being stopped.
            tCheck = BASS_ChannelIsActive(mTempoHandle);

            //--Check the value. We consider paused to be stopped.
            if(tCheck == BASS_ACTIVE_STOPPED || tCheck == BASS_ACTIVE_PAUSED) return true;
        }
        //--Samples can play on multiple channels at once. If any one is playing, this is playing.
        else if(mIsSample)
        {
            //--Passing NULL returns how many channels are playing. If it's not zero, the sound is playing.
            HCHANNEL tHandleArray[SAMPLE_MAX_CHANNELS];
            int tPlayingChannels = BASS_SampleGetChannels(mHandle, tHandleArray);
            if(tPlayingChannels < 1) return false;

            //--If all channels are stopped, this is stopped. Otherwise it's playing.
            //fprintf(stderr, "%i channels playing\n", tPlayingChannels);
            for(int i = 0; i < tPlayingChannels; i ++)
            {
                tCheck = BASS_ChannelIsActive(tHandleArray[i]);
                if(tCheck != BASS_ACTIVE_STOPPED && tCheck != BASS_ACTIVE_PAUSED) return true;
            }
        }
    }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_
        //--Channel is null, so nothing is playing.
        if(!rChannel) return false;

        //--Otherwise, query the channel.
        FMOD_BOOL tIsPlaying = false;
        FMOD_Channel_IsPlaying(rChannel, &tIsPlaying);
        return tIsPlaying;
    #endif
    return false;
}
float AudioPackage::GetVolume()
{
    return mLocalVolume;
}
float AudioPackage::GetPosition()
{
    //--[Common]
    //--Package isn't ready.
    if(!mIsReady) return 0.0f;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
    {
        //--Fail if the handle is not set.
        if(!mHandle) return 0.0f;

        //--Streams can return their playback position.
        //int tCheck = BASS_ACTIVE_STOPPED;
        if(mIsStream && mTempoHandle)
        {
            //--Get position.
            int32_t tBytePosition = BASS_ChannelGetPosition(mTempoHandle, BASS_POS_BYTE);

            //--Translate.
            float tSecondsPosition = (float)BASS_ChannelBytes2Seconds(mTempoHandle, tBytePosition);
            return tSecondsPosition;
        }
        //--Samples always return 0.0f and will need special code.
        else if(mIsSample)
        {
            return 0.0f;
        }
    }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_
        //--Channel is null, so nothing is playing.
        if(!rChannel) return 0.0f;

        //--Otherwise, query the channel.
        unsigned int tPosition = 0;
        //uint32_t tResult = FMOD_Channel_GetPosition(rChannel, &tPosition, FMOD_TIMEUNIT_MS);
        FMOD_Channel_GetPosition(rChannel, &tPosition, FMOD_TIMEUNIT_MS);

        //--Translate to seconds.
        float tPositionAsSeconds = tPosition / 1000.0f;
        return tPositionAsSeconds;
    #endif

    //--No audio booted. Return 0.
    return 0.0f;
}
bool AudioPackage::IsLooping()
{
    if(mStartLoopTime == mEndLoopTime) return false;
    return true;
}
float AudioPackage::GetStartLoopTime()
{
    return mStartLoopTime;
}
float AudioPackage::GetEndLoopTime()
{
    return mEndLoopTime;
}

//========================================= Manipulators ==========================================
void AudioPackage::LoadSample(const char *pPath)
{
    //--Load the data and check it.
    if(!pPath) return;
    if(AudioManager::xBlockAllAudio) return;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
        mHandle = BASS_SampleLoad(false, pPath, 0, 0, SAMPLE_MAX_CHANNELS, BASS_SAMPLE_OVER_POS);
        if(mHandle)
        {
            mIsReady = true;
            mIsSample = true;
            DebugManager::Print("AudioManager:  Loaded %s to %u.\n", pPath, mHandle);
        }
        else
        {
            DebugManager::ForcePrint("AudioManager:  Error loading sample %s.\n", pPath);
        }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--Load.
        FMOD_System_CreateSound(AudioManager::xFMODSystem, pPath, FMOD_2D, NULL, &mSound);

        //--If there are subsounds, set the 0th subsoound as the target sound.
        FMOD_Sound_GetNumSubSounds(mSound, &mSubSoundsTotal);
        if(mSubSoundsTotal > 0)
        {
            FMOD_Sound_SetMode(mSound, FMOD_LOOP_OFF);
            FMOD_Sound_GetSubSound(mSound, 0, &rSoundToPlay);
        }
        else
        {
            rSoundToPlay = mSound;
            FMOD_Sound_SetMode(mSound, FMOD_LOOP_OFF);
        }

        //--Ready.
        mIsReady = true;

    #endif
}
void AudioPackage::LoadStream(const char *pPath)
{
    //--Attempt to open the file.
    if(!pPath) return;
    if(AudioManager::xBlockAllAudio) return;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
        mHandle = BASS_StreamCreateFile(false, pPath, 0, 0, BASS_STREAM_DECODE);
        mTempoHandle = BASS_FX_TempoCreate(mHandle, 0);
        if(mHandle && mTempoHandle)
        {
            mIsReady = true;
            mIsStream = true;
            DebugManager::Print("AudioManager:  Loaded %s to %u.\n", pPath, mHandle);
        }
        else
        {
            DebugManager::ForcePrint("AudioManager:  Error loading stream %s.\n", pPath);
        }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--Load.
        FMOD_System_CreateStream(AudioManager::xFMODSystem, pPath, FMOD_LOOP_NORMAL | FMOD_2D, 0, &mSound);

        //--If there are any subsounds, set the 0th subsound as the playing target.
        FMOD_Sound_GetNumSubSounds(mSound, &mSubSoundsTotal);
        if(mSubSoundsTotal > 0)
        {
            FMOD_Sound_SetMode(mSound, FMOD_LOOP_OFF);
            FMOD_Sound_GetSubSound(mSound, 0, &rSoundToPlay);
        }
        else
        {
            rSoundToPlay = mSound;
            FMOD_Sound_SetMode(mSound, FMOD_LOOP_OFF);
        }

        //--Ready.
        mIsReady = true;
    #endif
}
void AudioPackage::Play()
{
    //--[Common]
    if(AudioManager::xBlockAllAudio) return;
    if(!mIsReady) return;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        if(mIsStream && mTempoHandle)
        {
            BASS_ChannelPlay(mTempoHandle, true);
            BASS_ChannelSetAttribute(mTempoHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume);
        }
        else if(mIsSample)
        {
            uint32_t tChannel = BASS_SampleGetChannel(mHandle, false);
            if(tChannel)
            {
                BASS_ChannelPlay(tChannel, true);
                BASS_ChannelSetAttribute(tChannel, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume);
                //fprintf(stderr, "Volume is %f %f = %f\n", mLocalVolume, mGlobalVolume, mLocalVolume * mGlobalVolume);
            }
        }
    }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--Error check.
        if(!mIsReady || !rSoundToPlay) return;

        //--If looping, specify that here.
        if(mStartLoopTime < mEndLoopTime)
        {
            FMOD_Sound_SetLoopPoints(rSoundToPlay, mStartLoopTime * 1000.0f, FMOD_TIMEUNIT_MS, mEndLoopTime * 1000.0f, FMOD_TIMEUNIT_MS);
            FMOD_Sound_SetMode(rSoundToPlay, FMOD_LOOP_NORMAL | FMOD_2D);
            FMOD_Sound_SetLoopCount(rSoundToPlay, -1);
        }

        //--Play the sound.
        FMOD_System_PlaySound(AudioManager::xFMODSystem, rSoundToPlay, 0, false, &rChannel);
        FMOD_Channel_SetVolume(rChannel, mLocalVolume * mGlobalVolume);
    #endif
}
void AudioPackage::FadeIn(int pTicks)
{
    //--[Common]
    if(AudioManager::xBlockAllAudio) return;
    if(!mIsReady) return;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
        //--Sliding speed is in milliseconds for Bass and must be converted.
        if(mHandle)
        {
            if(mIsStream && mTempoHandle)
            {
                BASS_ChannelSetAttribute(mTempoHandle, BASS_ATTRIB_VOL, 0.0f);
                BASS_ChannelSlideAttribute(mTempoHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume, pTicks / 60.0f * 1000.0f);
            }
            else if(mIsSample)
            {
            }
        }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--Error check.
        if(!mIsReady || !rSoundToPlay) return;

        //--If looping, specify that here.
        if(mStartLoopTime < mEndLoopTime)
        {
            FMOD_Sound_SetLoopPoints(rSoundToPlay, mStartLoopTime * 1000.0f, FMOD_TIMEUNIT_MS, mEndLoopTime * 1000.0f, FMOD_TIMEUNIT_MS);
            FMOD_Sound_SetMode(rSoundToPlay, FMOD_LOOP_NORMAL | FMOD_2D);
            FMOD_Sound_SetLoopCount(rSoundToPlay, -1);
        }

        //--Play the sound but pause it immediately.
        FMOD_System_PlaySound(AudioManager::xFMODSystem, rSoundToPlay, 0, false, &rChannel);
        FMOD_Channel_SetPaused(rChannel, true);

        //--Get the mixing rate and current time clock.
        int tMixerRate = 0;
        long long unsigned int tDSPClock = 0;
        FMOD_System_GetSoftwareFormat(AudioManager::xFMODSystem, &tMixerRate, 0, 0);
        FMOD_Channel_GetDSPClock(rChannel, NULL, &tDSPClock);

        //--Set a 0 fade point here and a 1.0f fade point in the future.
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock, 0.0f);
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), mLocalVolume * mGlobalVolume);

        //--Now unpause it.
        FMOD_Channel_SetPaused(rChannel, false);

        //--Check loop points.
        int tLoopCount = 0;
        uint32_t tStart, tEnd, tMode;
        FMOD_Channel_GetLoopPoints(rChannel, &tStart, FMOD_TIMEUNIT_MS, &tEnd, FMOD_TIMEUNIT_MS);
        FMOD_Channel_GetMode(rChannel, &tMode);
        FMOD_Channel_GetLoopCount(rChannel, &tLoopCount);
    #endif
}
void AudioPackage::FadeOut(int pTicks)
{
    //--[Common]
    if(AudioManager::xBlockAllAudio) return;
    if(!mIsReady) return;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
        //--Sliding speed is in milliseconds for Bass and must be converted.
        if(mHandle)
        {
            if(mIsStream && mTempoHandle)
            {
                BASS_ChannelSlideAttribute(mTempoHandle, BASS_ATTRIB_VOL, 0.0f, pTicks / 60.0f * 1000.0f);
            }
            else if(mIsSample)
            {
            }
        }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--If there's no channel, fail.
        if(!rChannel) return;

        //--Get the mixing rate and current time clock.
        int tMixerRate = 0;
        long long unsigned int tDSPClock = 0;
        FMOD_System_GetSoftwareFormat(AudioManager::xFMODSystem, &tMixerRate, 0, 0);
        FMOD_Channel_GetDSPClock(rChannel, NULL, &tDSPClock);

        //--Set a volume fade point here and a 1.0f fade point in the future.
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock, mLocalVolume * mGlobalVolume);
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), 0.0f);

        //--Add a delayed pause.
        FMOD_Channel_SetDelay(rChannel, 0, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), false);
    #endif
}
void AudioPackage::FadeOutToStop(int pTicks)
{
    //--[Common]
    if(AudioManager::xBlockAllAudio) return;
    if(!mIsReady) return;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
    //--Sliding speed is in milliseconds for Bass and must be converted. The channel will stop after it fades out.
    if(mHandle)
    {
        if(mIsStream && mTempoHandle)
        {
            BASS_ChannelSlideAttribute(mTempoHandle, BASS_ATTRIB_VOL, -1.0f, pTicks / 60.0f * 1000.0f);
        }
        else if(mIsSample)
        {
        }
    }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--If there's no channel, fail.
        if(!rChannel) return;

        //--Get the mixing rate and current time clock.
        int tMixerRate = 0;
        long long unsigned int tDSPClock = 0;
        FMOD_System_GetSoftwareFormat(AudioManager::xFMODSystem, &tMixerRate, 0, 0);
        FMOD_Channel_GetDSPClock(rChannel, NULL, &tDSPClock);

        //--Set a volume fade point here and a 1.0f fade point in the future.
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock, mLocalVolume * mGlobalVolume);
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), 0.0f);

        //--Add a delayed stop.
        FMOD_Channel_SetDelay(rChannel, 0, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), true);
    #endif
}
void AudioPackage::Pause()
{
    //--[Common]
    if(AudioManager::xBlockAllAudio) return;
    if(!mIsReady) return;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        if(mIsStream && mTempoHandle)
        {
            BASS_ChannelPause(mTempoHandle);
        }
        else if(mIsSample)
        {
        }
    }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(!rChannel) return;
        FMOD_Channel_SetPaused(rChannel, true);
    #endif
}
void AudioPackage::Stop()
{
    //--[Common]
    if(AudioManager::xBlockAllAudio) return;
    if(!mIsReady) return;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        if(mIsStream && mTempoHandle)
        {
            BASS_ChannelStop(mTempoHandle);
        }
        else if(mIsSample)
        {
        }
    }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(!rChannel) return;
        FMOD_Channel_Stop(rChannel);
    #endif
}
void AudioPackage::SetLoop(bool pIsLooping)
{
    //--[Common]
    if(AudioManager::xBlockAllAudio) return;
    if(!mIsReady) return;

    //--[Bass Version]
    #if defined _BASS_AUDIO_

    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(!rChannel) return;
        if(!pIsLooping)
        {
            FMOD_Channel_SetLoopCount(rChannel, 0);
        }
        else
        {
            FMOD_Channel_SetLoopCount(rChannel, -1);
        }
    #endif
}
void AudioPackage::SetVolume(float pVolume)
{
    if(pVolume < 0.0f) pVolume = 0.0f;
    if(pVolume > 1.0f) pVolume = 1.0f;
    mLocalVolume = pVolume;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
    if(!mHandle) return;
    if(mTempoHandle)
        BASS_ChannelSetAttribute(mTempoHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume);
    else
        BASS_ChannelSetAttribute(mHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume);
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(!rChannel) return;
        FMOD_Channel_SetVolume(rChannel, mLocalVolume * mGlobalVolume);
    #endif
}
void AudioPackage::SetGlobalVolume(float pVolume)
{
    //--Modifies the volume playing without modifying the local volume.  That is:
    //  Total = Local * Global
    //  All three volumes are clamped between 0.0f and 1.0f.
    if(pVolume < 0.0f) pVolume = 0.0f;
    if(pVolume > 1.0f) pVolume = 1.0f;
    mGlobalVolume = pVolume;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
        if(!mHandle) return;
        if(mTempoHandle)
        {
            BASS_ChannelSlideAttribute(mTempoHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume, 1);
        }
        else
        {
            BASS_ChannelSlideAttribute(mHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume, 1);
        }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(rChannel)
        {
            FMOD_Channel_SetVolume(rChannel, mLocalVolume * mGlobalVolume);
        }
    #endif
}
void AudioPackage::SlideVolume(float pVolume, int pTicks)
{
    //--[Common]
    //--Sets the volume, but also slides it on BASS/FMOD.
    if(pVolume < 0.0f) pVolume = 0.0f;
    if(pVolume > 1.0f) pVolume = 1.0f;
    #if defined _FMOD_AUDIO_
    float tPreviousVolume = mLocalVolume;
    #endif
    mLocalVolume = pVolume;

    //--[Bass Version]
    #if defined _BASS_AUDIO_
        if(!mHandle) return;
        if(mTempoHandle)
            BASS_ChannelSlideAttribute(mTempoHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume, pTicks * TICKSTOSECONDS * 1000);
        else
            BASS_ChannelSlideAttribute(mHandle, BASS_ATTRIB_VOL, mLocalVolume * mGlobalVolume, pTicks * TICKSTOSECONDS * 1000);
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--If there's no channel, fail.
        if(!rChannel) return;

        //--Get the mixing rate and current time clock.
        int tMixerRate = 0;
        long long unsigned int tDSPClock = 0;
        FMOD_System_GetSoftwareFormat(AudioManager::xFMODSystem, &tMixerRate, 0, 0);
        FMOD_Channel_GetDSPClock(rChannel, NULL, &tDSPClock);

        //--Set a volume fade point here and a new fade point in the future.
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock, tPreviousVolume * mGlobalVolume);
        FMOD_Channel_AddFadePoint(rChannel, tDSPClock + (tMixerRate * (float)pTicks * (float)TICKSTOSECONDS), mLocalVolume * mGlobalVolume);
    #endif
}
void AudioPackage::SeekTo(float pSeconds)
{
    //--Only works for streams.
    #if defined _BASS_AUDIO_
    if(mHandle)
    {
        if(mIsStream && mTempoHandle)
        {
            int tBytes = BASS_ChannelSeconds2Bytes(mTempoHandle, pSeconds);
            BASS_ChannelSetPosition(mTempoHandle, tBytes, BASS_POS_BYTE);
        }
        else if(mIsSample)
        {
            //Does nothing.
        }
    }
    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_
        if(!rChannel) return;
        FMOD_Channel_SetPosition(rChannel, pSeconds * 1000.0f, FMOD_TIMEUNIT_MS);
    #endif
}
void AudioPackage::SetTempo(float pTempo)
{
    //--Only works for streams.
    #if defined _BASS_AUDIO_
    if(mHandle && mTempoHandle)
    {
        if(mIsStream)
        {
            BASS_ChannelSetAttribute(mTempoHandle, BASS_ATTRIB_TEMPO, pTempo);
            //int tErrorCode = BASS_ErrorGetCode();
            BASS_ErrorGetCode();
        }
        else if(mIsSample)
        {
            //Does nothing.
        }
    }
    #endif
}

//========================================= Core Methods ==========================================
void AudioPackage::AddLoop(float pStartLoop, float pEndLoop)
{
    //--[Common]
    //--Adds looping properties, arguments should be in seconds. Complex loops only work on streams
    //  and, presently, only in Bass.
    //--Pass an illogical start/end pair to remove looping.

    //--[Bass Version]
    #if defined _BASS_AUDIO_

        if(pStartLoop >= pEndLoop)
        {
            mStartLoopTime = 0.0f;
            mEndLoopTime = 0.0f;

            if(mTempoHandle)
                BASS_ChannelRemoveSync(mTempoHandle, BASS_SYNC_POS|BASS_SYNC_MIXTIME);
            else
                BASS_ChannelRemoveSync(mHandle, BASS_SYNC_POS|BASS_SYNC_MIXTIME);
            return;
        }

        //--If a loop already exists, remove it.
        if(pStartLoop < pEndLoop && mExtraData)
        {
            if(mTempoHandle)
                BASS_ChannelRemoveSync(mTempoHandle, BASS_SYNC_POS|BASS_SYNC_MIXTIME);
            else
                BASS_ChannelRemoveSync(mHandle, BASS_SYNC_POS|BASS_SYNC_MIXTIME);
            free(mExtraData);
            mExtraData = NULL;
        }

        //--Error check.
        if(!mHandle || !mIsStream) return;

        //--Store read-only values.
        mStartLoopTime = pStartLoop;
        mEndLoopTime = pEndLoop;

        //--Data package to be passed, stores the loop start time.
        SetMemoryData(__FILE__, __LINE__);
        AudioPackageLoopData *nData = (AudioPackageLoopData *)starmemoryalloc(sizeof(AudioPackageLoopData));

        if(mTempoHandle)
            nData->mStartInBytes = BASS_ChannelSeconds2Bytes(mTempoHandle, pStartLoop);
        else
            nData->mStartInBytes = BASS_ChannelSeconds2Bytes(mHandle, pStartLoop);
        mExtraData = nData;

        //--Loop end time.
        uint64_t tEndTime = 0;
        if(mTempoHandle)
            tEndTime = BASS_ChannelSeconds2Bytes(mTempoHandle, pEndLoop);
        else
            tEndTime = BASS_ChannelSeconds2Bytes(mHandle, pEndLoop);

        if(mTempoHandle)
            BASS_ChannelSetSync(mTempoHandle, BASS_SYNC_POS|BASS_SYNC_MIXTIME, tEndTime, &AudioManager::LoopCallback, mExtraData);
        else
            BASS_ChannelSetSync(mHandle, BASS_SYNC_POS|BASS_SYNC_MIXTIME, tEndTime, &AudioManager::LoopCallback, mExtraData);

    #endif

    //--[FMOD Version]
    #if defined _FMOD_AUDIO_

        //--Store loop times if no channel is playing.
        mStartLoopTime = pStartLoop;
        mEndLoopTime = pEndLoop;

        //--If no channel is playing, end here.
        if(!rChannel) return;

        //--If an illegal loop set is passed, disable looping.
        if(pStartLoop >= pEndLoop)
        {
            FMOD_Channel_SetLoopCount(rChannel, 0);
            return;
        }

        //--Set the loop points.
        //FMOD_Sound_SetMode(mSound, FMOD_LOOP_NORMAL);
        FMOD_Channel_SetLoopPoints(rChannel, pStartLoop * 1000.0f, FMOD_TIMEUNIT_MS, pEndLoop * 1000.0f, FMOD_TIMEUNIT_MS);
        FMOD_Channel_SetLoopCount(rChannel, -1);

    #endif
}

//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void AudioPackage::HookToLuaState(lua_State *pLuaState)
{
    /* AudioPackage_ActivateLoop(fStartLoop, fEndLoop)
       The last registered package gains looping properties.  Values are in seconds. */
    lua_register(pLuaState, "AudioPackage_ActivateLoop", &Hook_AudioPackage_ActivateLoop);

    /* AudioPackage_SetLocalVolume(fVolume)
       Sets the volume for this package, overriding the AudioManagers.  Auto-clamps 0 to 1. */
    lua_register(pLuaState, "AudioPackage_SetLocalVolume", &Hook_AudioPackage_SetLocalVolume);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
//--Audio packages do not print an error if they don't exist. This is typically because the audio
//  manager detected a re-registered audio package and nulled the pointer instead.
int Hook_AudioPackage_ActivateLoop(lua_State *L)
{
    //AudioPackage_ActivateLoop(fStartLoop, fEndLoop)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("AudioPackage_ActivateLoop");

    //--Get and Check
    AudioPackage *rCurrentPackage = AudioManager::Fetch()->rLastPackage;
    if(!rCurrentPackage) return 0;//LuaTypeError("AudioPackage_ActivateLoop");

    rCurrentPackage->AddLoop(lua_tonumber(L, 1), lua_tonumber(L, 2));

    return 0;
}
int Hook_AudioPackage_SetLocalVolume(lua_State *L)
{
    //AudioPackage_SetLocalVolume(fVolume)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs != 1) return LuaArgError("AudioPackage_SetLocalVolume");

    //--Get and Check
    AudioPackage *rCurrentPackage = AudioManager::Fetch()->rLastPackage;
    if(!rCurrentPackage) return 0;//LuaTypeError("AudioPackage_SetLocalVolume");

    rCurrentPackage->SetVolume(lua_tonumber(L, 1));

    return 0;
}
