//--[AudioPackage]
//--A wrapper class around an audio piece, be it a stream or sample.  The AudioManager deals
//  exclusively with these, and thus only AudioPackages need to be reprogrammed based on the API
//  in use.
//--The values mStartLoopTime and mEndLoopTime are *not* used by the looping software, they are
//  used only by property queries since getting the values once they are on the audio card is difficult.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--[Local Definitions]
#define SAMPLE_MAX_CHANNELS 5

class AudioPackage
{
    private:
    //--System
    bool mIsReady;
    float mLocalVolume;
    float mGlobalVolume;

    //--Bass Only
    #if defined _BASS_AUDIO_
        uint32_t mHandle;
        uint32_t mTempoHandle;
    #endif

    //--FMOD Only
    #if defined _FMOD_AUDIO_
        FMOD_SOUND *mSound;
        FMOD_SOUND *rSoundToPlay;
        FMOD_CHANNEL *rChannel;
        FMOD_RESULT mFMODResult;
        int mSubSoundsTotal;

        //--Looping properties.
        float mStartLoop;
        float mEndLoop;
    #endif

    //--Sample
    bool mIsSample;

    //--Stream
    bool mIsStream;

    //--Extra Data
    float mStartLoopTime; //Note: Read-only.
    float mEndLoopTime;   //Note: Read-only.
    void *mExtraData;

    protected:

    public:
    //--System
    AudioPackage();
    ~AudioPackage();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    float GetDuration();
    bool IsPlaying();
    float GetVolume();
    float GetPosition();
    bool IsLooping();
    float GetStartLoopTime();
    float GetEndLoopTime();

    //--Manipulators
    void LoadSample(const char *pPath);
    void LoadStream(const char *pPath);
    void Play();
    void FadeIn(int pTicks);
    void FadeOut(int pTicks);
    void FadeOutToStop(int pTicks);
    void Pause();
    void Stop();
    void SetLoop(bool pIsLooping);
    void SetVolume(float pVolume);
    void SetGlobalVolume(float pVolume);
    void SlideVolume(float pVolume, int pTicks);
    void SeekTo(float pSeconds);
    void SetTempo(float pTempo);

    //--Core Methods
    void AddLoop(float pStartLoop, float pEndLoop);

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AudioPackage_ActivateLoop(lua_State *L);
int Hook_AudioPackage_SetLocalVolume(lua_State *L);

