//--Base
#include "AudioManager.h"

//--Classes
#include "AudioPackage.h"

//--CoreClasses
#include "LoadInterrupt.h"
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
//--Managers

//=========================================== System ==============================================
AudioManager::AudioManager()
{
    //--[AudioManager]
    //--System
    //--Storage Lists
    mMusicList = new SugarLinkedList(true);
    mSoundList = new SugarLinkedList(true);
    mRandomSoundList = new SugarLinkedList(true);

    //--Music controllers
    rCurrentMusic = NULL;

    //--Voice controllers
    mCurrentSpeech = NULL;

    //--FMOD only
    #if defined _FMOD_AUDIO_
    #endif

    //--Public Variables
    rLastPackage = NULL;

    //--[Construction]
    //--Boot the sound system.
    if(!xBlockAllAudio)
    {
        bool tBootedSuccessfully = false;
        #if defined _BASS_AUDIO_
            #ifdef _TARGET_OS_MAC_
                tBootedSuccessfully = BASS_Init(-1, 44100, 0, NULL, NULL);
            #elif defined _TARGET_OS_LINUX_
                tBootedSuccessfully = BASS_Init(-1, 44100, 0, NULL, NULL);
            #else
                tBootedSuccessfully = BASS_Init(-1, 44100, 0, FindWindow(NULL, "Starlight Engine"), NULL);
                BASS_FX_GetVersion();
                //int tVersion = BASS_FX_GetVersion();
                //fprintf(stderr, "BASS FX VERSION: %i\n", tVersion);
            #endif
        #elif defined _FMOD_AUDIO_

            //--Initialize FMOD stuff.
            FMOD_System_Create(&xFMODSystem);
            FMOD_System_Init(xFMODSystem, 1024, FMOD_INIT_NORMAL, xExtraDriverData);

            //--Should be a successful boot.
            tBootedSuccessfully = true;

        #endif

        //--Error output
        if(!tBootedSuccessfully)
        {
            fprintf(stderr, "AudioManager:  Error booting sound system!\n");
            xBlockAllAudio = true;
        }
    }
}
AudioManager::~AudioManager()
{
    StopAllMusic();
    StopAllSound();
    delete mMusicList;
    delete mSoundList;
    delete mRandomSoundList;
    delete mCurrentSpeech;

    //--Clean up the audio from FMOD.
    #if defined _FMOD_AUDIO_
        FMOD_System_Close(xFMODSystem);
        FMOD_System_Release(xFMODSystem);
    #endif
}

//--[Public Statics]
bool AudioManager::xBlockAllAudio = false;
bool AudioManager::xBlockSound = false;
bool AudioManager::xBlockMusic = false;
float AudioManager::xMusicVolume = AM_DEFAULT_MUSIC_VOLUME;
float AudioManager::xSoundVolume = AM_DEFAULT_SOUND_VOLUME;
float AudioManager::xMasterVolume = 1.00f;

//--[FMOD Statics]
//--These only exist when using FMOD for Audio.
#if defined _FMOD_AUDIO_
    FMOD_SYSTEM *AudioManager::xFMODSystem = NULL;
    uint32_t AudioManager::xFMODVersion = 0;
    void *AudioManager::xExtraDriverData = NULL;
#endif

//====================================== Property Queries =========================================
bool AudioManager::MusicExists(const char *pName)
{
    //--Returns true if a music of the given name exists.
    return (mMusicList->GetSlotOfElementByName(pName) != -1);
}
bool AudioManager::SoundExists(const char *pName)
{
    //--Returns true if a sound of the given name exists.
    return (mSoundList->GetSlotOfElementByName(pName) != -1);
}
const char *AudioManager::GetNameOfMusicPack(void *pPtr)
{
    int tSlot = mMusicList->GetSlotOfElementByPtr(pPtr);
    if(tSlot == -1) return NULL;
    return mMusicList->GetNameOfElementBySlot(tSlot);
}

//========================================= Manipulators ==========================================
void AudioManager::RegisterMusic(const char *pName, AudioPackage *pMusicPack)
{
    //--Registers the AudioPackage as a piece of music.  All musics are mutually exclusive, playing
    //  any one will shut off all the others, barring the use of cross fading.
    if(!pName || !pMusicPack) return;
    mMusicList->AddElement(pName, pMusicPack, &AudioPackage::DeleteThis);
    rLastPackage = pMusicPack;
    if(SugarBitmap::xInterruptCall) SugarBitmap::xInterruptCall();
}
void AudioManager::RegisterSound(const char *pName, AudioPackage *pSoundPack)
{
    //--Registers the AudioPackage as a sound.  Sounds can play concurrently and do not support
    //  features like crossfading or looping.
    if(!pName || !pSoundPack) return;
    mSoundList->AddElement(pName, pSoundPack, &AudioPackage::DeleteThis);
    rLastPackage = pSoundPack;
    if(SugarBitmap::xInterruptCall) SugarBitmap::xInterruptCall();
}
void AudioManager::RegisterRandomSound(const char *pName, int pMaxSounds)
{
    //--Adds a RandomSound pack, which is just an integer. This can allow a sound to be picked at
    //  random instead of always playing the same one.
    if(!pName) return;
    SetMemoryData(__FILE__, __LINE__);
    int *nPtr = (int *)starmemoryalloc(sizeof(int));
    *nPtr = pMaxSounds;
    mRandomSoundList->AddElement(pName, nPtr, &FreeThis);
    if(SugarBitmap::xInterruptCall) SugarBitmap::xInterruptCall();
}
void AudioManager::ClearLastPackage()
{
    rLastPackage = NULL;
}
void AudioManager::ChangeMusicVolume(float pDelta)
{
    //--Change volume.
    xMusicVolume = xMusicVolume + pDelta;
    if(xMusicVolume > 1.0f) xMusicVolume = 1.0f;
    if(xMusicVolume < 0.0f) xMusicVolume = 0.0f;

    //--Inform all musics of the change.
    AudioPackage *rAudioPackage = (AudioPackage *)mMusicList->PushIterator();
    while(rAudioPackage)
    {
        rAudioPackage->SetGlobalVolume(xMusicVolume);
        rAudioPackage = (AudioPackage *)mMusicList->AutoIterate();
    }
}
void AudioManager::ChangeSoundVolume(float pDelta)
{
    //--Change volume.
    xSoundVolume = xSoundVolume + pDelta;
    if(xSoundVolume > 1.0f) xSoundVolume = 1.0f;
    if(xSoundVolume < 0.0f) xSoundVolume = 0.0f;

    //--Inform all sounds of the change.
    AudioPackage *rAudioPackage = (AudioPackage *)mSoundList->PushIterator();
    while(rAudioPackage)
    {
        rAudioPackage->SetGlobalVolume(xSoundVolume);
        rAudioPackage = (AudioPackage *)mSoundList->AutoIterate();
    }
}
void AudioManager::ChangeMusicVolumeTo(float pValue)
{
    //--Change volume.
    if(pValue > 1.0f) pValue = 1.0f;
    if(pValue < 0.0f) pValue = 0.0f;
    xMusicVolume = pValue;

    //--Inform all sounds of the change.
    AudioPackage *rAudioPackage = (AudioPackage *)mMusicList->PushIterator();
    while(rAudioPackage)
    {
        rAudioPackage->SetGlobalVolume(xMusicVolume);
        rAudioPackage = (AudioPackage *)mMusicList->AutoIterate();
    }
}
void AudioManager::ChangeSoundVolumeTo(float pValue)
{
    //--Change volume.
    if(pValue > 1.0f) pValue = 1.0f;
    if(pValue < 0.0f) pValue = 0.0f;
    xSoundVolume = pValue;

    //--Inform all sounds of the change.
    AudioPackage *rAudioPackage = (AudioPackage *)mSoundList->PushIterator();
    while(rAudioPackage)
    {
        rAudioPackage->SetGlobalVolume(xSoundVolume);
        rAudioPackage = (AudioPackage *)mSoundList->AutoIterate();
    }
}
void AudioManager::SeekMusicTo(float pSeconds)
{
    if(!rCurrentMusic) return;
    rCurrentMusic->SeekTo(pSeconds);
}
void AudioManager::AdvanceMusicToLoopPoint()
{
    //--Debugging tool. Advances the music to 2 seconds before its loop point. Useful for diagnosing
    //  looping time errors.
    //--Does nothing if no music is playing, obviously. Also does nothing if it's not looping.
    if(!rCurrentMusic || !rCurrentMusic->IsLooping()) return;

    //--Get the loop time and subtract 2 seconds. Clamp if needed.
    float tLoopPoint = rCurrentMusic->GetEndLoopTime() - 2.0f;
    if(tLoopPoint < 0.0f) tLoopPoint = 0.0f;

    //--Set.
    rCurrentMusic->SeekTo(tLoopPoint);
}
void AudioManager::ModifyLoopPointForward()
{
    //--Debugging tool. Moves the loop point on the current music forward by 0.010 seconds, and prints
    //  the loop time to the console so it can be edited in the Lua files.
    //--Does nothing if no music is playing, obviously. Also does nothing if it's not looping. Duh.
    if(!rCurrentMusic || !rCurrentMusic->IsLooping()) return;
    float tStartPoint = rCurrentMusic->GetStartLoopTime();
    float tEndPoint = rCurrentMusic->GetEndLoopTime();
    rCurrentMusic->AddLoop(tStartPoint, tEndPoint + 0.010f);
    fprintf(stderr, "Loop positions set to %f %f\n", tStartPoint, tEndPoint + 0.010f);
}
void AudioManager::ModifyLoopPointBackward()
{
    //--Debugging tool. Moves the loop point on the current music backward by 0.010 seconds, and prints
    //  the loop time to the console so it can be edited in the Lua files.
    //--Does nothing if no music is playing, obviously. Also does nothing if it's not looping. Duh.
    if(!rCurrentMusic || !rCurrentMusic->IsLooping()) return;
    float tStartPoint = rCurrentMusic->GetStartLoopTime();
    float tEndPoint = rCurrentMusic->GetEndLoopTime();
    rCurrentMusic->AddLoop(tStartPoint, tEndPoint - 0.010f);
    fprintf(stderr, "Loop positions set to %f %f\n", tStartPoint, tEndPoint - 0.010f);
}

//========================================= Core Methods ==========================================
void AudioManager::PlayMusic(const char *pName)
{
    //--Plays music.  Only one at a time.
    if(xBlockMusic) return;

    //--Special case:  Music was NULL, so just fade the old one out.
    if(!pName || !strcmp(pName, "NULL"))
    {
        if(rCurrentMusic) rCurrentMusic->FadeOutToStop(60);
        rCurrentMusic = NULL;
        return;
    }

    //--Locate the new music.
    AudioPackage *rPack = (AudioPackage *)mMusicList->GetElementByName(pName);

    //--Was it the same as the old music?  Do nothing.
    if(rPack == rCurrentMusic) return;

    //--Order the old music to stop.
    if(rCurrentMusic) rCurrentMusic->FadeOutToStop(60);

    //--Play the new stuff.
    if(rPack)
    {
        rPack->Play();
        rPack->FadeIn(120);
        rPack->SetLoop(true);
    }

    //--Become the new current music.
    rCurrentMusic = rPack;
}
void AudioManager::PlayMusicStartingAt(const char *pName, float pStartPoint)
{
    //--Plays the music starting at the requested position. It will still crossfade if needed.
    PlayMusic(pName);

    //--If the current music exists, change its position.
    if(rCurrentMusic)
    {
        rCurrentMusic->SeekTo(pStartPoint);
    }
}
void AudioManager::PlayMusicNoFade(const char *pName)
{
    //--Identical to PlayMusic, but doesn't fade the music track in.
    if(xBlockMusic) return;

    //--Special case:  Music was NULL, so just fade the old one out.
    if(!pName || !strcmp(pName, "NULL"))
    {
        if(rCurrentMusic) rCurrentMusic->FadeOutToStop(60);
        rCurrentMusic = NULL;
        return;
    }

    //--Locate the new music.
    AudioPackage *rPack = (AudioPackage *)mMusicList->GetElementByName(pName);

    //--Was it the same as the old music?  Do nothing.
    if(rPack == rCurrentMusic) return;

    //--Order the old music to stop.
    if(rCurrentMusic) rCurrentMusic->FadeOutToStop(60);

    //--Play the new stuff.
    if(rPack)
    {
        rPack->Play();
        rPack->SetVolume(xMusicVolume + 0.40f);
        rPack->SetLoop(true);
    }

    //--Become the new current music.
    rCurrentMusic = rPack;
}
void AudioManager::FadeMusic(int pTickDuration)
{
    //--Orders the music to fade out over the requested number of ticks.  The music is still
    //  'playing' in memory until the next stop/play call, and can be faded back in.
    //--Pass a negative tick value to cause the music to cease being the active music. It will
    //  fade out using the absolute value of the ticks. Passing 0 is the same as StopAllMusic().
    if(xBlockMusic || !rCurrentMusic) return;

    //--Normal case: Music fades out but stays in memory.
    if(pTickDuration > 0)
    {
        rCurrentMusic->FadeOutToStop(pTickDuration);
    }
    //--Zero case: Music stops immediately. Remains in memory.
    else if(pTickDuration == 0)
    {
        rCurrentMusic->FadeOut(0);
    }
    //--Negative case: Fade out, ceases being in memory.
    else
    {
        rCurrentMusic->FadeOutToStop(pTickDuration * -1);
        rCurrentMusic = NULL;
    }
}
void AudioManager::StopAllMusic()
{
    //--Order all music to stop playing.
    if(xBlockMusic) return;

    AudioPackage *rPack = (AudioPackage *)mMusicList->PushIterator();
    while(rPack)
    {
        rPack->Stop();
        rPack = (AudioPackage *)mMusicList->AutoIterate();
    }
    rCurrentMusic = NULL;
}
void AudioManager::PlaySound(const char *pName)
{
    //--Unlike PlayMusic, there is no exclusivity here.
    if(xBlockSound) return;
    AudioPackage *rPack = (AudioPackage *)mSoundList->GetElementByName(pName);
    if(!rPack) return;
    rPack->SetGlobalVolume(xSoundVolume * xMasterVolume);
    rPack->Play();
}
void AudioManager::StopSound(const char *pName)
{
    if(xBlockSound) return;
    AudioPackage *rPack = (AudioPackage *)mSoundList->GetElementByName(pName);
    if(!rPack) return;
    rPack->Stop();
}
void AudioManager::PlayRandomSound(const char *pName, int pMaxRoll)
{
    //--Plays a random sound effect of the pattern: "%s%i", pName, tRoll.
    //  The AudioManager can store certain maximums automatically. In those cases, use the overload
    //  which doesn't pass in a number.
    char tBuffer[64];
    sprintf(tBuffer, "%s%i", pName, rand() % pMaxRoll);
    PlaySound(tBuffer);
}
void AudioManager::PlayRandomSound(const char *pName)
{
    //--This version automatically resolves how many of a given sound there are in a random list.
    int *rMaxAmount = (int *)mRandomSoundList->GetElementByName(pName);
    if(!rMaxAmount) return;
    PlayRandomSound(pName, *rMaxAmount);
}
void AudioManager::StopAllSound()
{
    //--Order all sounds to stop playing.
    if(xBlockSound) return;
    AudioPackage *rPack = (AudioPackage *)mSoundList->PushIterator();
    while(rPack)
    {
        rPack->Stop();
        rPack = (AudioPackage *)mSoundList->AutoIterate();
    }
}
void AudioManager::PlaySpeech(const char *pPath)
{
    //--Unlike other audio sources, Speech only allows one instance to be in memory at a time.  If
    //  a new instance is loaded, the old one is automatically deleted.
    //--Passing NULL will deallocate the pack and not replace it.
    delete mCurrentSpeech;
    mCurrentSpeech = NULL;
    if(!pPath) return;

    mCurrentSpeech = new AudioPackage();
    mCurrentSpeech->LoadStream(pPath);
    mCurrentSpeech->Play();
}
bool AudioManager::StopSpeech()
{
    //--Stops the speech pack playing and nullifies it.  Does NOT flip over the dialogue hastening
    //  call, but this affects the return state.  If the speech had already stopped of its own accord
    //  then return false, if the speech was stopped by us return true.
    if(!mCurrentSpeech) return false;

    //--Sound will auto-stop on destructor call.
    bool tReturn = mCurrentSpeech->IsPlaying();
    delete mCurrentSpeech;
    mCurrentSpeech = NULL;
    return tReturn;
}

//--[Sounds]
void AudioManager::PauseSound(const char *pName)
{
    AudioPackage *rPack = (AudioPackage *)mSoundList->GetElementByName(pName);
    if(!rPack) return;
    rPack->Pause();
}

//============================================ Update =============================================
void AudioManager::Update()
{
    //--Stops any music package that is playing at zero volume.
    AudioPackage *rPack = (AudioPackage *)mMusicList->PushIterator();
    while(rPack)
    {
        if(rPack->IsPlaying() && rPack->GetVolume() < 0.01f)
        {
            rPack->Stop();
        }
        rPack = (AudioPackage *)mMusicList->AutoIterate();
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
AudioPackage *AudioManager::GetPlayingMusic()
{
    //--Note: Can legally return NULL.
    return rCurrentMusic;
}
AudioPackage *AudioManager::GetMusicPack(const char *pName)
{
    return (AudioPackage *)mMusicList->GetElementByName(pName);
}
AudioPackage *AudioManager::GetSoundPack(const char *pName)
{
    return (AudioPackage *)mSoundList->GetElementByName(pName);
}
AudioPackage *AudioManager::GetVoicePack()
{
    return mCurrentSpeech;
}

//====================================== Static Functions =========================================
AudioManager *AudioManager::Fetch()
{
    return Global::Shared()->gAudioManager;
}

//========================================= Lua Hooking ===========================================
void AudioManager::HookToLuaState(lua_State *pLuaState)
{
    /* AudioManager_Register(sName, "AsMusic", "AsStream", sPath)
       AudioManager_Register(sName, "AsMusic", "AsSample", sPath)
       AudioManager_Register(sName, "AsSound", "AsStream", sPath)
       AudioManager_Register(sName, "AsSound", "AsSample", sPath)
       AudioManager_Register(sName, "AsRandomSound", iTotalSounds)
       Registers the audio at the given path as a member of the given list.  Music is mutually
       exclusive:  Only one music may be playing at a time.  Sound is not so picky.
       Streams are streamed from the hard drive, samples are loaded all at once.  Which to use is
       based on how much RAM you have and want to use. */
    lua_register(pLuaState, "AudioManager_Register", &Hook_AudioManager_Register);

    /* AudioManager_PlayMusic(sName)
       Plays the named piece of music, if it exists.  Fails silently. */
    lua_register(pLuaState, "AudioManager_PlayMusic", &Hook_AudioManager_PlayMusic);

    /* AudioManager_PlaySound(sName)
       Plays the named sound, if it exists.  Fails silently. */
    lua_register(pLuaState, "AudioManager_PlaySound", &Hook_AudioManager_PlaySound);

    /* AudioManager_PlaySpeech(sPath)
       Plays the speech loaded right off the hard drive.  If another instance of speech is currently
       playing, it is stopped automatically.  Loading a new instance of Speech will delete the old
       one, never worry about memory management again! */
    lua_register(pLuaState, "AudioManager_PlaySpeech", &Hook_AudioManager_PlaySpeech);

    /* AudioManager_StopMusic()
       Instantly stops the playing music, without fading it. */
    lua_register(pLuaState, "AudioManager_StopMusic", &Hook_AudioManager_StopMusic);

    /* AudioManager_FadeMusic(iTickDuration)
       Fades the music out. Pass a negative to cause the music to lose its 'Is the current music' status. */
    lua_register(pLuaState, "AudioManager_FadeMusic", &Hook_AudioManager_FadeMusic);

    /* AudioManager_StopSound(sName)
       Stops the named sound effect if it's playing. */
    lua_register(pLuaState, "AudioManager_StopSound", &Hook_AudioManager_StopSound);

    /* AudioManager_GetProperty("Music Volume") (1 float)
       AudioManager_GetProperty("Sound Volume") (1 float)
       Gets and returns the requested property from the AudioManager. All getters are static. */
    lua_register(pLuaState, "AudioManager_GetProperty", &Hook_AudioManager_GetProperty);

    /* AudioManager_SetProperty("Music Volume", fVolume)
       AudioManager_SetProperty("Sound Volume", fVolume)
       Sets the requested property in the AudioManager. All setters are static. */
    lua_register(pLuaState, "AudioManager_SetProperty", &Hook_AudioManager_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AudioManager_Register(lua_State *L)
{
    //AudioManager_Register(sName, "AsMusic", "AsStream", sPath)
    //AudioManager_Register(sName, "AsMusic", "AsStream", sPath, fLoopStart, fLoopEnd)
    //AudioManager_Register(sName, "AsMusic", "AsSample", sPath)
    //AudioManager_Register(sName, "AsMusic", "AsSample", sPath, fLoopStart, fLoopEnd)
    //AudioManager_Register(sName, "AsSound", "AsStream", sPath)
    //AudioManager_Register(sName, "AsSound", "AsSample", sPath)
    //AudioManager_Register(sName, "AsRandomSound", iTotalSounds)
    int pArgs = lua_gettop(L);
    if(pArgs < 4) return LuaArgError("AudioManager_Register");

    //--If this is a Random Sound, just handle that here and ignore the rest.
    if(!strcmp(lua_tostring(L, 2), "AsRandomSound"))
    {
        AudioManager::Fetch()->RegisterRandomSound(lua_tostring(L, 1), lua_tointeger(L, 3));
        return 0;
    }

    //--Sound or Music?  Defaults to Sound.
    bool tIsMusic = false;
    if(!strcmp(lua_tostring(L, 2), "AsMusic"))
    {
        tIsMusic = true;
    }

    //--Sample or Stream?  Defaults to Sample.
    bool tIsStream = false;
    if(!strcmp(lua_tostring(L, 3), "AsStream"))
    {
        tIsStream = true;
    }

    //--If the package already exists, fail.
    if(tIsMusic)
    {
        if(AudioManager::Fetch()->MusicExists(lua_tostring(L, 1)))
        {
            AudioManager::Fetch()->ClearLastPackage();
            return 0;
        }
    }
    else
    {
        if(AudioManager::Fetch()->SoundExists(lua_tostring(L, 1)))
        {
            AudioManager::Fetch()->ClearLastPackage();
            return 0;
        }
    }

    //--Create
    AudioPackage *nPackage = new AudioPackage();
    if(!tIsStream)
    {
        nPackage->LoadSample(lua_tostring(L, 4));
    }
    else
    {
        nPackage->LoadStream(lua_tostring(L, 4));
    }

    //--Register
    if(!tIsMusic)
    {
        nPackage->SetGlobalVolume(AudioManager::xSoundVolume);
        AudioManager::Fetch()->RegisterSound(lua_tostring(L, 1), nPackage);
    }
    else
    {
        nPackage->SetGlobalVolume(AudioManager::xMusicVolume);
        AudioManager::Fetch()->RegisterMusic(lua_tostring(L, 1), nPackage);
    }

    //--If this is a music package, and there are 6 arguments, activate looping.
    if(tIsMusic && pArgs >= 6)
    {
        nPackage->AddLoop(lua_tonumber(L, 5), lua_tonumber(L, 6));
    }

    return 0;
}
int Hook_AudioManager_PlayMusic(lua_State *L)
{
    //AudioManager_PlayMusic(sName[])
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("AudioManager_PlayMusic");

    AudioManager::Fetch()->PlayMusic(lua_tostring(L, 1));
    return 0;
}
int Hook_AudioManager_PlaySound(lua_State *L)
{
    //AudioManager_PlaySound(sName[])
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("AudioManager_PlaySound");

    AudioManager::Fetch()->PlaySound(lua_tostring(L, 1));
    return 0;
}
int Hook_AudioManager_PlaySpeech(lua_State *L)
{
    //AudioManager_PlaySpeech(sPath[])
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("AudioManager_PlaySpeech");

    AudioManager::Fetch()->PlaySpeech(lua_tostring(L, 1));
    return 0;
}

//--[Stopping]
int Hook_AudioManager_StopMusic(lua_State *L)
{
    //AudioManager_StopMusic()

    AudioManager::Fetch()->StopAllMusic();
    return 0;
}
int Hook_AudioManager_FadeMusic(lua_State *L)
{
    //AudioManager_FadeMusic(iTickDuration)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("AudioManager_FadeMusic");

    AudioManager::Fetch()->FadeMusic(lua_tointeger(L, 1));
    return 0;
}
int Hook_AudioManager_StopSound(lua_State *L)
{
    //AudioManager_StopSound(sName[])
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("AudioManager_StopSound");

    AudioManager::Fetch()->StopSound(lua_tostring(L, 1));
    return 0;
}
int Hook_AudioManager_GetProperty(lua_State *L)
{
    //AudioManager_GetProperty("Music Volume") (1 float)
    //AudioManager_GetProperty("Sound Volume") (1 float)
    //AudioManager_GetProperty("Default Music Volume") (1 float)
    //AudioManager_GetProperty("Default Sound Volume") (1 float)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AudioManager_GetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Active object.
    AudioManager *rAudioManager = AudioManager::Fetch();

    //--Music Volume.
    if(!strcasecmp(rSwitchType, "Music Volume"))
    {
        lua_pushnumber(L, rAudioManager->xMusicVolume);
        return 1;
    }
    //--Sound Volume.
    else if(!strcasecmp(rSwitchType, "Sound Volume"))
    {
        lua_pushnumber(L, rAudioManager->xSoundVolume);
        return 1;
    }
    //--Default Music Volume.
    if(!strcasecmp(rSwitchType, "Default Music Volume"))
    {
        lua_pushnumber(L, AM_DEFAULT_MUSIC_VOLUME);
        return 1;
    }
    //--Default Sound Volume.
    else if(!strcasecmp(rSwitchType, "Default Sound Volume"))
    {
        lua_pushnumber(L, AM_DEFAULT_SOUND_VOLUME);
        return 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("AudioManager_GetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_AudioManager_SetProperty(lua_State *L)
{
    //AudioManager_SetProperty("Music Volume", fVolume)
    //AudioManager_SetProperty("Sound Volume", fVolume)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AudioManager_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Active object.
    AudioManager *rAudioManager = AudioManager::Fetch();

    //--Music Volume.
    if(!strcasecmp(rSwitchType, "Music Volume"))
    {
        rAudioManager->ChangeMusicVolume(lua_tonumber(L, 2) - rAudioManager->xMusicVolume);
    }
    //--Sound Volume.
    else if(!strcasecmp(rSwitchType, "Sound Volume"))
    {
        rAudioManager->ChangeSoundVolume(lua_tonumber(L, 2) - rAudioManager->xSoundVolume);
    }
    //--Error.
    else
    {
        LuaPropertyError("AudioManager_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
