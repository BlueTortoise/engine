//--[ControlManager]
//--Keeps track of all keys owned by the current instance of the keyboard, as well as all controls,
//  which are distinct from keys.  Controls are named values that are used by the player, such as
//  "Jump" and "Shoot", while keys are part of the keyboard or gamepads.
//--Mouse, joypad, and even theoretically touchscreen or networked controls are also routed through
//  here. Note that the mouse information is renormalized to the screen based on the GL information.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--[Local Definitions]
#define SPECIALKEY_LMOUSE -10
#define SPECIALKEY_RMOUSE -20

#define SPECIALCODE_BACKSPACE -1
#define SPECIALCODE_RETURN -2
#define SPECIALCODE_CANCEL -3

//--Size Definitions
#define CM_IMG_OFFSET_Y 3.0f

//--[Joypad Definitions]
//--Each joypad occupies JOYSTICK_OFFSET_PER units, allowing up to 10 sticks and 100 buttons per pad.
//  It is rare that more than that would be attached!
#define JOYSTICK_OFFSET_PER 200

//--First 100 are buttons, post-100 are sticks.
//--For Sticks:
//  A stick may allow up to 5 axes (for simplicity, most have 2-3)
//  The axes are defined as such:
//   Stick 0 Axes 0 Negative = 100
//   Stick 0 Axes 0 Positive = 101
//   Stick 0 Axes 1 Negative = 102
//   Stick 0 Axes 1 Positive = 103
//   Stick 1 Axes 0 Negative = 110
//   Stick 1 Axes 0 Positive = 111
//  And so on.
//  The sensitivity can be specified, by default it's 0.3f (stick is 30% in the direction requested)
#define JOYSTICK_OFFSET_STICK 100
#define JOYSTICK_STICK_CONSTANT 10
#define JOYSTICK_AXIS_CONSTANT 2

#define JOYSTICK_0_0_NEG (JOYSTICK_OFFSET_STICK + (JOYSTICK_STICK_CONSTANT * 0) + (JOYSTICK_AXIS_CONSTANT * 0) + 0)
#define JOYSTICK_0_0_POS (JOYSTICK_OFFSET_STICK + (JOYSTICK_STICK_CONSTANT * 0) + (JOYSTICK_AXIS_CONSTANT * 0) + 1)
#define JOYSTICK_0_1_NEG (JOYSTICK_OFFSET_STICK + (JOYSTICK_STICK_CONSTANT * 0) + (JOYSTICK_AXIS_CONSTANT * 1) + 0)
#define JOYSTICK_0_1_POS (JOYSTICK_OFFSET_STICK + (JOYSTICK_STICK_CONSTANT * 0) + (JOYSTICK_AXIS_CONSTANT * 1) + 1)

#if defined _ALLEGRO_PROJECT_
    #define KEYCODES_MAX ALLEGRO_KEY_MAX
#else
    #define KEYCODES_MAX SDL_NUM_SCANCODES
#endif

//--[Local Structures]
typedef struct
{
    bool mIsPress;
    int mScancode;
    int mMouseButton;
    int mJoypad;
    int mJoypadValue;
}KeypressPack;

//--ControlImagePack. Contains an image and a Keyboard/Mouse/Joypad index. If a key is needed to be shown
//  as an image, the matching package can be resolved and the image returned.
//  When possible, precache the control. Searching the list can be time-expensive as the list cannot be
//  meaningfully sorted between keyboard/mouse/joypad.
typedef struct ControlImagePack
{
    //--Members
    int mKeyCode;
    int mMouseCode;
    int mJoyCode;
    SugarBitmap *rImage;

    //--Functions
    void Initialize()
    {
        mKeyCode = -1;
        mMouseCode = -1;
        mJoyCode = -1;
        rImage = NULL;
    }
}ControlImagePack;

//--[Class]
class ControlManager
{
    private:
    //--System
    bool mEnabled;
    int mRefireRate;

    //--Profiles
    bool mIsInProfileMode;
    SugarLinkedList *rLastReggedProfile;
    SugarLinkedList *mProfileList;

    //--Flags
    bool mIsAnyKeyPressed;
    bool mIsAnyKeyDown;
    bool mIsAnyKeyReleased;

    //--Storage
    int mPressedKeyboardKey;
    int mPressedMouseButton;
    int mPressedJoypadButton;
    bool mDownKeys[KEYCODES_MAX];

    //--Input
    SugarLinkedList *mControlList;
    ControlState *mDummyState;

    //--SDL Input Buffer
    #if defined _SDL_PROJECT_
        SugarLinkedList *mInputBuffer;
    #endif

    //--Mouse
    int mMouseX;
    int mMouseY;
    int mMouseZ;

    //--SDL Joystick
    #if defined _SDL_PROJECT_
        SDL_Joystick *mJoystickHandle;
    #endif

    //--Names
    int mKeyboardNamesTotal;
    char **mKeyboardNames;
    int mMouseNamesTotal;
    char **mMouseNames;
    int mJoypadNamesTotal;
    char **mJoypadNames;

    //--Keyboard Input Buffering
    bool mIsShifted;
    SugarLinkedList *mKeyboardInputBuffer;
    Keycode mKeycodes[KEYCODES_MAX];

    //--Control Images
    int mControlImagePacksTotal;
    SugarBitmap *rErrorImage;
    ControlImagePack *mControlImagePacks;

    protected:

    public:
    //--System
    ControlManager();
    ~ControlManager();
    void SetupControls();

    //--Public Variables
    bool mShifted;
    bool mCaps;
    bool mCtrl;

    //--Allegro Variables
    #if defined _ALLEGRO_PROJECT_
    ALLEGRO_KEYBOARD_STATE *rKeyboardStatePtr;
    ALLEGRO_MOUSE_STATE *rMouseStatePtr;
    #endif

    //--Property Queries
    bool GetEnabledState();
    void GetMouseCoords(int &sMouseX, int &sMouseY, int &sMouseZ);
    void GetMouseCoordsF(float &sMouseX, float &sMouseY, float &sMouseZ);
    ControlState *GetControlState(const char *pName);
    bool IsAnyKeyPressed();
    bool IsAnyKeyDown();
    bool IsDown(const char *pName);
    bool IsFirstPress(const char *pName);
    bool IsFirstRelease(const char *pName);
    void GetKeyPressCodes(int &sKeyboard, int &sMouse, int &sJoypad);
    char PopKeyboardInputBuffer();

    //--Manipulators
    void SetEnabledState(bool pFlag);
    void SetMouseCoordsByDisplay(int pMouseX, int pMouseY, int pMouseZ);
    void SetMouseCoordsByCanvas(int pMouseX, int pMouseY, int pMouseZ);
    void SetMouseZRelative(int pMouseZChange);
    void BlankControls();
    void RegisterControl(const char *pName, ControlState *pState);
    void SetShiftFlag(bool pFlag);
    void ClearInputBuffer();
    void RegisterProfile(const char *pName);
    void PopProfileStack();
    void ActivateProfile(const char *pName);

    //--Core Methods
    SugarLinkedList *CopyControlList();
    bool IsJoystickActive(int pIndex);
    char *GetNameOfKeyIndex(int pIndex);
    char *GetNameOfKeyPrimary(const char *pName);
    char *GetNameOfKeySecondary(const char *pName);
    void RefreshMousePosition();
    void NullifyStateData();

    //--Hotkeys
    void CheckHotkeys();

    //--Images
    void AllocateImages(int pImages);
    void SetErrorImage(const char *pDLPath);
    void SetImage(int pSlot, int pKeyCode, int pMouseCode, int pJoyCode, const char *pDLPath);
    SugarBitmap *ResolveControlImage(const char *pControlName);

    //--Input
    void InformOfPress(int pKeycode, int pMouseButton, int pJoyButton);
    void InformOfRelease(int pKeycode, int pMouseButton, int pJoyButton);
    void InformOfJoyAxis(int pAxis, int16_t pResult);

    //--Key Binding
    void OverbindControl(const char *pName, bool pIsSecondary, int pKeyboard, int pMouse, int pJoystick);
    void RebindControl(const char *pName, const char *pPriCode, const char *pSecCode);
    void ResolveCode(const char *pCode, int &sKeyboard, int &sMouse, int &sJoystick);

    //--Lookups
    void BuildKeyNameLookups();

    //--Update
    bool IsControlDown(ControlState *pState, bool &sControlIsChangeable);
    void Update(bool pIsLogicTick, bool pIsPressEvent);
    void PostUpdate();
    void UpdatePaused(uint8_t pPauseFlags);

    //--File I/O
    //--Drawing
    //--Pointer Routing
    SugarLinkedList *GetControlList();

    //--Static Functions
    static ControlManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Profile_Begin(lua_State *L);
int Hook_Profile_End(lua_State *L);
int Hook_Profile_SetActive(lua_State *L);
int Hook_CM_Rebind(lua_State *L);
int Hook_CM_Overbind(lua_State *L);
int Hook_ControlManager_GetProperty(lua_State *L);
int Hook_ControlManager_SetProperty(lua_State *L);
