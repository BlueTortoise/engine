//--Base
#include "ControlManager.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers

void ControlManager::InformOfPress(int pKeycode, int pMouseButton, int pJoyButton)
{
    //--Events will inform of the press type.  Overrides previous scancode, and the codes are
    //  wiped at the end of every logic tick.
    mPressedKeyboardKey  = pKeycode;
    mPressedMouseButton  = pMouseButton;
    mPressedJoypadButton = pJoyButton;
    bool tWasAnyKeyPressed = mIsAnyKeyPressed;
    mIsAnyKeyPressed = true;

    //--[Allegro Version]
    //--If there was a keycode press, add it to the input buffer.
    #if defined _ALLEGRO_PROJECT_
        if(pKeycode > 0 && pKeycode < KEYCODES_MAX)
        {
            //--Store the keycode.
            char tKeycode = mKeycodes[pKeycode].mNormalChar;

            //--Check if the keycode is either Alt or Tab. If it is, it doesn't count as an any-key-pressed case.
            if(pKeycode == ALLEGRO_KEY_ALT || pKeycode == ALLEGRO_KEY_ALTGR || pKeycode == ALLEGRO_KEY_TAB)
            {
                mIsAnyKeyPressed = tWasAnyKeyPressed;
            }

            //--Check if the key is shifted.
            if(rKeyboardStatePtr && (al_key_down(rKeyboardStatePtr, ALLEGRO_KEY_LSHIFT) || al_key_down(rKeyboardStatePtr, ALLEGRO_KEY_RSHIFT)))
            {
                tKeycode = mKeycodes[pKeycode].mShiftedChar;
            }

            //--Only non-NULL keys get added.
            if(tKeycode != '\0')
            {
                SetMemoryData(__FILE__, __LINE__);
                char *nCode = (char *)starmemoryalloc(sizeof(char));
                *nCode = tKeycode;
                mKeyboardInputBuffer->AddElement("X", nCode, &FreeThis);
            }
        }

    //--[SDL Version]
    //--Unlike the Allegro version, which can store a pointer to the keyboard's whole state, we need
    //  to store individual keypresses and process them during the update tick.
    #elif defined _SDL_PROJECT_

        //--Package.
        SetMemoryData(__FILE__, __LINE__);
        KeypressPack *nPack = (KeypressPack *)starmemoryalloc(sizeof(KeypressPack));
        nPack->mIsPress = true;
        nPack->mScancode = pKeycode;
        nPack->mMouseButton = pMouseButton;
        nPack->mJoypad = pJoyButton;

        //--Register
        mInputBuffer->AddElement("X", nPack, &FreeThis);

        //--Check if the keycode is either Alt or Tab. If it is, it doesn't count as an any-key-pressed case.
        if(pKeycode == SDL_SCANCODE_LALT || pKeycode == SDL_SCANCODE_RALT || pKeycode == SDL_SCANCODE_TAB)
        {
            mIsAnyKeyPressed = tWasAnyKeyPressed;
        }

        //--Debug
        //fprintf(stderr, "Recorded key press: %i\n", pJoyButton);

    #endif
}
void ControlManager::InformOfRelease(int pKeycode, int pMouseButton, int pJoyButton)
{
    //--Called when a key is released. This is only used by SDL, as Allegro stores the state of
    //  the keyboard and just queries it.
    #if defined _SDL_PROJECT_

        //--Package.
        SetMemoryData(__FILE__, __LINE__);
        KeypressPack *nPack = (KeypressPack *)starmemoryalloc(sizeof(KeypressPack));
        nPack->mIsPress = false;
        nPack->mScancode = pKeycode;
        nPack->mMouseButton = pMouseButton;
        nPack->mJoypad = pJoyButton;

        //--Register
        mInputBuffer->AddElement("X", nPack, &FreeThis);

        //--Debug
        //fprintf(stderr, "Recorded key release: %i\n", pKeycode);

    #endif
}
void ControlManager::InformOfJoyAxis(int pAxis, int16_t pResult)
{
    //--SDL only. Informs of an axis event. Axex tend to have events a lot unless they're idle.
    #if defined _SDL_PROJECT_

        //--Clamp.
        if(abs(pResult) < 1500) pResult = 128;

        //--Flag reset.
        mPressedKeyboardKey  = -2;
        mPressedMouseButton  = -2;
        mPressedJoypadButton = -2;
        mIsAnyKeyPressed |= (abs(pResult) >= 1200);

        //--Package.
        SetMemoryData(__FILE__, __LINE__);
        KeypressPack *nPack = (KeypressPack *)starmemoryalloc(sizeof(KeypressPack));
        nPack->mIsPress = true;
        nPack->mScancode = -2;
        nPack->mMouseButton = -2;
        nPack->mJoypadValue = pResult;

        //--Start at the axis offset.
        nPack->mJoypad = JOYSTICK_OFFSET_STICK;

        //--Increase by axis count.
        nPack->mJoypad += pAxis * JOYSTICK_AXIS_CONSTANT;

        //--If the value was less than 0, flip the joypad value.
        if(pResult < -600) nPack->mJoypad *= -1;
        mPressedJoypadButton = nPack->mJoypad;

        //--Register
        mInputBuffer->AddElement("X", nPack, &FreeThis);

        //--Match pair: Create an opposite stop command so the controls don't jam if the stick suddenly changes.
        //if(abs(pResult) != 128)
        //{
            SetMemoryData(__FILE__, __LINE__);
            KeypressPack *nPack2 = (KeypressPack *)starmemoryalloc(sizeof(KeypressPack));
            nPack2->mIsPress = false;
            nPack2->mScancode = -2;
            nPack2->mMouseButton = -2;
            nPack2->mJoypad = nPack->mJoypad * -1;
            nPack2->mJoypadValue = 128;
            mInputBuffer->AddElement("X", nPack2, &FreeThis);
        //}

        //--Debug
        //fprintf(stderr, "Recorded axis event: %i %i\n", pAxis, pResult);
        //fprintf(stderr, " Data: %i - %i\n", nPack->mJoypad, nPack->mIsPress);
        //fprintf(stderr, " Rev:  %i - %i\n", nPack2->mJoypad, nPack2->mIsPress);

    #endif
}
