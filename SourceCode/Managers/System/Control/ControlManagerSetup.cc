//--Base
#include "ControlManager.h"

//--Classes
//--Definitions
#include "Global.h"

//--Generics
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--Local Functions
ControlState *BootControl()
{
    //--Returns a control with all its values set to neutrals.  Some of these are non-zero.
    SetMemoryData(__FILE__, __LINE__);
    ControlState *nControl = (ControlState *)starmemoryalloc(sizeof(ControlState));
    nControl->mWatchKeyPri = -1;
    nControl->mWatchKeySec = -1;
    nControl->mWatchMouseBtnPri = -1;
    nControl->mWatchMouseBtnSec = -1;
    nControl->mWatchJoyPri = -1;
    nControl->mWatchJoySec = -1;

    nControl->mIsFirstPress = false;
    nControl->mIsFirstRelease = false;
    nControl->mIsDown = false;
    nControl->mTicksSincePress = -1;

    nControl->mHasPendingRelease = false;
    return nControl;
}
ControlState *CreateControlWatchingKeyboard(int pPriCode, int pSecCode)
{
    //--Control watching zero, one, or two keyboard scancodes.
    ControlState *nControl = BootControl();
    nControl->mWatchKeyPri = pPriCode;
    nControl->mWatchKeySec = pSecCode;

    return nControl;
}
ControlState *CreateControlWatchingMouse(int pPriCode, int pSecCode)
{
    //--Control watching none, one of, or both of the mouse buttons.
    ControlState *nControl = BootControl();
    nControl->mWatchMouseBtnPri = pPriCode;
    nControl->mWatchMouseBtnSec = pSecCode;

    return nControl;
}
ControlState *CreateControlWatchingJoystick(int pPriCode, int pSecCode)
{
    //--Control watching none, one of, or both of the mouse buttons.
    ControlState *nControl = BootControl();
    nControl->mWatchJoyPri = pPriCode;
    nControl->mWatchJoySec = pSecCode;

    return nControl;
}

void ControlManager::SetupControls()
{
    //--[ ========================================= SDL ========================================= ]
    #if defined _SDL_PROJECT_

        //--[Joysticks]
        if(false)
        {
            fprintf(stderr, "Number of attached joysticks: %i\n", SDL_NumJoysticks());
            if(SDL_NumJoysticks() > 0)
            {
                mJoystickHandle = SDL_JoystickOpen(0);
                printf("Opened Joystick 0\n");
                printf("Name: %s\n", SDL_JoystickName(0));
                printf("Number of Axes: %d\n", SDL_JoystickNumAxes(mJoystickHandle));
                printf("Number of Buttons: %d\n", SDL_JoystickNumButtons(mJoystickHandle));
                printf("Number of Balls: %d\n", SDL_JoystickNumBalls(mJoystickHandle));
            }
        }

        //--[Standard keys]
        //--Mappings for directional movements.
        RegisterControl("Up"   ,   CreateControlWatchingKeyboard(SDL_SCANCODE_UP,    SDL_SCANCODE_W));
        RegisterControl("Down" ,   CreateControlWatchingKeyboard(SDL_SCANCODE_DOWN,  SDL_SCANCODE_S));
        RegisterControl("Left" ,   CreateControlWatchingKeyboard(SDL_SCANCODE_LEFT,  SDL_SCANCODE_A));
        RegisterControl("Right",   CreateControlWatchingKeyboard(SDL_SCANCODE_RIGHT, SDL_SCANCODE_D));
        RegisterControl("UpLevel", CreateControlWatchingKeyboard(SDL_SCANCODE_Q,     SDL_SCANCODE_PAGEUP));
        RegisterControl("DnLevel", CreateControlWatchingKeyboard(SDL_SCANCODE_E,     SDL_SCANCODE_PAGEDOWN));

        //--Action mappings.
        RegisterControl("Activate", CreateControlWatchingKeyboard(SDL_SCANCODE_Z,      -1));
        RegisterControl("Cancel",   CreateControlWatchingKeyboard(SDL_SCANCODE_X,      -1));
        RegisterControl("Run",      CreateControlWatchingKeyboard(SDL_SCANCODE_LSHIFT, -1));
        RegisterControl("Jump",     CreateControlWatchingKeyboard(SDL_SCANCODE_SPACE,  -1));

        //--Field Abilities
        RegisterControl("OpenFieldAbilityMenu", CreateControlWatchingKeyboard(SDL_SCANCODE_C,      -1));
        RegisterControl("FieldAbility0", CreateControlWatchingKeyboard(SDL_SCANCODE_1, -1));
        RegisterControl("FieldAbility1", CreateControlWatchingKeyboard(SDL_SCANCODE_2, -1));
        RegisterControl("FieldAbility2", CreateControlWatchingKeyboard(SDL_SCANCODE_3, -1));
        RegisterControl("FieldAbility3", CreateControlWatchingKeyboard(SDL_SCANCODE_4, -1));
        RegisterControl("FieldAbility4", CreateControlWatchingKeyboard(SDL_SCANCODE_5, -1));

        //--[Mouse Buttons]
        RegisterControl("MouseLft", CreateControlWatchingMouse(SDL_BUTTON_LEFT,  -1));
        RegisterControl("MouseRgt", CreateControlWatchingMouse(SDL_BUTTON_RIGHT, -1));

        //--[GUI Keys]
        RegisterControl("Shift",              CreateControlWatchingKeyboard(SDL_SCANCODE_LSHIFT, SDL_SCANCODE_RSHIFT));
        RegisterControl("Backspace",          CreateControlWatchingKeyboard(SDL_SCANCODE_BACKSPACE,               -1));
        RegisterControl("F1",                 CreateControlWatchingKeyboard(SDL_SCANCODE_F1,                      -1));
        RegisterControl("F2",                 CreateControlWatchingKeyboard(SDL_SCANCODE_F2,                      -1));
        RegisterControl("Enter",              CreateControlWatchingKeyboard(SDL_SCANCODE_RETURN,  SDL_SCANCODE_KP_ENTER));
        RegisterControl("Ctrl",               CreateControlWatchingKeyboard(SDL_SCANCODE_LCTRL,   SDL_SCANCODE_RCTRL));
        RegisterControl("Delete",             CreateControlWatchingKeyboard(SDL_SCANCODE_DELETE,                  -1));
        RegisterControl("Escape",             CreateControlWatchingKeyboard(SDL_SCANCODE_ESCAPE,                  -1));
        RegisterControl("ToggleFreeMovement", CreateControlWatchingKeyboard(SDL_SCANCODE_TAB,                     -1));
        RegisterControl("Quick Node",         CreateControlWatchingKeyboard(SDL_SCANCODE_F1,                      -1));
        RegisterControl("Reload Level",       CreateControlWatchingKeyboard(SDL_SCANCODE_F2,                      -1));
        RegisterControl("Build Lighting",     CreateControlWatchingKeyboard(SDL_SCANCODE_F3,                      -1));
        RegisterControl("Save Level Data",    CreateControlWatchingKeyboard(SDL_SCANCODE_F9,                      -1));
        RegisterControl("Load Level Data",    CreateControlWatchingKeyboard(SDL_SCANCODE_F10,                     -1));
        RegisterControl("ToggleFullscreen",   CreateControlWatchingKeyboard(SDL_SCANCODE_F11,       SDL_SCANCODE_F12));
        RegisterControl("PageUp",             CreateControlWatchingKeyboard(SDL_SCANCODE_PAGEUP,                  -1));
        RegisterControl("PageDn",             CreateControlWatchingKeyboard(SDL_SCANCODE_PAGEDOWN,                -1));
        RegisterControl("GUI|Up",             CreateControlWatchingKeyboard(SDL_SCANCODE_UP,                      -1));
        RegisterControl("GUI|Dn",             CreateControlWatchingKeyboard(SDL_SCANCODE_DOWN,                    -1));

        //--[Numeric Keys]
        RegisterControl("Key_1", CreateControlWatchingKeyboard(SDL_SCANCODE_1, -1));
        RegisterControl("Key_2", CreateControlWatchingKeyboard(SDL_SCANCODE_2, -1));
        RegisterControl("Key_3", CreateControlWatchingKeyboard(SDL_SCANCODE_3, -1));
        RegisterControl("Key_4", CreateControlWatchingKeyboard(SDL_SCANCODE_4, -1));
        RegisterControl("Key_5", CreateControlWatchingKeyboard(SDL_SCANCODE_5, -1));
        RegisterControl("Key_6", CreateControlWatchingKeyboard(SDL_SCANCODE_6, -1));
        RegisterControl("Key_7", CreateControlWatchingKeyboard(SDL_SCANCODE_7, -1));
        RegisterControl("Key_8", CreateControlWatchingKeyboard(SDL_SCANCODE_8, -1));
        RegisterControl("Key_9", CreateControlWatchingKeyboard(SDL_SCANCODE_9, -1));
        RegisterControl("Key_0", CreateControlWatchingKeyboard(SDL_SCANCODE_0, -1));

        //--[Debug Keys]
        //--Debug menu.
        RegisterControl("OpenDebug",    CreateControlWatchingKeyboard(SDL_SCANCODE_I,     -1));
        RegisterControl("CameraUnlock", CreateControlWatchingKeyboard(SDL_SCANCODE_LCTRL, -1));

        //--Shader Toggles
        RegisterControl("Shader0", CreateControlWatchingKeyboard(SDL_SCANCODE_0, -1));
        RegisterControl("Shader1", CreateControlWatchingKeyboard(SDL_SCANCODE_1, -1));
        RegisterControl("Shader2", CreateControlWatchingKeyboard(SDL_SCANCODE_2, -1));

        //--Loop Controls
        RegisterControl("LoopBack", CreateControlWatchingKeyboard(SDL_SCANCODE_LEFTBRACKET,  -1));
        RegisterControl("LoopForw", CreateControlWatchingKeyboard(SDL_SCANCODE_RIGHTBRACKET, -1));

        //--Kerning
        RegisterControl("RebuildKerning", CreateControlWatchingKeyboard(SDL_SCANCODE_F8, -1));

    //--[ ======================================= Allegro ======================================= ]
    #elif defined _ALLEGRO_PROJECT_

        //--[Standard keys]
        //--Mappings for directional movements.
        RegisterControl("Up"   ,   CreateControlWatchingKeyboard(ALLEGRO_KEY_UP,    ALLEGRO_KEY_W));
        RegisterControl("Down" ,   CreateControlWatchingKeyboard(ALLEGRO_KEY_DOWN,  ALLEGRO_KEY_S));
        RegisterControl("Left" ,   CreateControlWatchingKeyboard(ALLEGRO_KEY_LEFT,  ALLEGRO_KEY_A));
        RegisterControl("Right",   CreateControlWatchingKeyboard(ALLEGRO_KEY_RIGHT, ALLEGRO_KEY_D));
        RegisterControl("UpLevel", CreateControlWatchingKeyboard(ALLEGRO_KEY_Q,     ALLEGRO_KEY_PGUP));
        RegisterControl("DnLevel", CreateControlWatchingKeyboard(ALLEGRO_KEY_E,     ALLEGRO_KEY_PGDN));

        //--Action mappings.
        RegisterControl("Activate", CreateControlWatchingKeyboard(ALLEGRO_KEY_Z,      -1));
        RegisterControl("Cancel",   CreateControlWatchingKeyboard(ALLEGRO_KEY_X,      -1));
        RegisterControl("Run",      CreateControlWatchingKeyboard(ALLEGRO_KEY_LSHIFT, -1));
        RegisterControl("Jump",     CreateControlWatchingKeyboard(ALLEGRO_KEY_SPACE,  -1));

        //--Field Abilities
        RegisterControl("OpenFieldAbilityMenu", CreateControlWatchingKeyboard(ALLEGRO_KEY_C,      -1));
        RegisterControl("FieldAbility0", CreateControlWatchingKeyboard(ALLEGRO_KEY_1, -1));
        RegisterControl("FieldAbility1", CreateControlWatchingKeyboard(ALLEGRO_KEY_2, -1));
        RegisterControl("FieldAbility2", CreateControlWatchingKeyboard(ALLEGRO_KEY_3, -1));
        RegisterControl("FieldAbility3", CreateControlWatchingKeyboard(ALLEGRO_KEY_4, -1));
        RegisterControl("FieldAbility4", CreateControlWatchingKeyboard(ALLEGRO_KEY_5, -1));

        //--[Mouse Buttons]
        RegisterControl("MouseLft", CreateControlWatchingMouse(1, -1));
        RegisterControl("MouseRgt", CreateControlWatchingMouse(2, -1));

        //--[GUI Keys]
        RegisterControl("Shift",              CreateControlWatchingKeyboard(ALLEGRO_KEY_LSHIFT, ALLEGRO_KEY_RSHIFT));
        RegisterControl("Backspace",          CreateControlWatchingKeyboard(ALLEGRO_KEY_BACKSPACE,              -1));
        RegisterControl("F1",                 CreateControlWatchingKeyboard(ALLEGRO_KEY_F1,                     -1));
        RegisterControl("F2",                 CreateControlWatchingKeyboard(ALLEGRO_KEY_F2,                     -1));
        RegisterControl("Enter",              CreateControlWatchingKeyboard(ALLEGRO_KEY_ENTER,   ALLEGRO_KEY_PAD_ENTER));
        RegisterControl("Ctrl",               CreateControlWatchingKeyboard(ALLEGRO_KEY_LCTRL,   ALLEGRO_KEY_RCTRL));
        RegisterControl("Delete",             CreateControlWatchingKeyboard(ALLEGRO_KEY_DELETE,  ALLEGRO_KEY_BACKSPACE));
        RegisterControl("Escape",             CreateControlWatchingKeyboard(ALLEGRO_KEY_ESCAPE,                 -1));
        RegisterControl("ToggleFreeMovement", CreateControlWatchingKeyboard(ALLEGRO_KEY_TAB,                    -1));
        RegisterControl("Quick Node",         CreateControlWatchingKeyboard(ALLEGRO_KEY_F1,                     -1));
        RegisterControl("Reload Level",       CreateControlWatchingKeyboard(ALLEGRO_KEY_F2,                     -1));
        RegisterControl("Build Lighting",     CreateControlWatchingKeyboard(ALLEGRO_KEY_F3,                     -1));
        RegisterControl("Save Level Data",    CreateControlWatchingKeyboard(ALLEGRO_KEY_F4,                     -1));
        RegisterControl("Load Level Data",    CreateControlWatchingKeyboard(ALLEGRO_KEY_F10,                    -1));
        RegisterControl("ToggleFullscreen",   CreateControlWatchingKeyboard(ALLEGRO_KEY_F11,       ALLEGRO_KEY_F12));
        RegisterControl("PageUp",             CreateControlWatchingKeyboard(ALLEGRO_KEY_PGUP,                   -1));
        RegisterControl("PageDn",             CreateControlWatchingKeyboard(ALLEGRO_KEY_PGDN,                   -1));
        RegisterControl("GUI|Up",             CreateControlWatchingKeyboard(ALLEGRO_KEY_UP,                     -1));
        RegisterControl("GUI|Dn",             CreateControlWatchingKeyboard(ALLEGRO_KEY_DOWN,                   -1));

        //--[Numeric Keys]
        RegisterControl("Key_1", CreateControlWatchingKeyboard(ALLEGRO_KEY_1, -1));
        RegisterControl("Key_2", CreateControlWatchingKeyboard(ALLEGRO_KEY_2, -1));
        RegisterControl("Key_3", CreateControlWatchingKeyboard(ALLEGRO_KEY_3, -1));
        RegisterControl("Key_4", CreateControlWatchingKeyboard(ALLEGRO_KEY_4, -1));
        RegisterControl("Key_5", CreateControlWatchingKeyboard(ALLEGRO_KEY_5, -1));
        RegisterControl("Key_6", CreateControlWatchingKeyboard(ALLEGRO_KEY_6, -1));
        RegisterControl("Key_7", CreateControlWatchingKeyboard(ALLEGRO_KEY_7, -1));
        RegisterControl("Key_8", CreateControlWatchingKeyboard(ALLEGRO_KEY_8, -1));
        RegisterControl("Key_9", CreateControlWatchingKeyboard(ALLEGRO_KEY_9, -1));
        RegisterControl("Key_0", CreateControlWatchingKeyboard(ALLEGRO_KEY_0, -1));

        //--[Debug Keys]
        RegisterControl("OpenDebug",    CreateControlWatchingKeyboard(ALLEGRO_KEY_I,     -1));
        RegisterControl("CameraUnlock", CreateControlWatchingKeyboard(ALLEGRO_KEY_LCTRL, -1));
        RegisterControl("WalkSpeedDn",  CreateControlWatchingKeyboard(ALLEGRO_KEY_O, -1));
        RegisterControl("WalkSpeedUp",  CreateControlWatchingKeyboard(ALLEGRO_KEY_P, -1));
        RegisterControl("FrameSpeedUp", CreateControlWatchingKeyboard(ALLEGRO_KEY_K, -1));
        RegisterControl("FrameSpeedDn", CreateControlWatchingKeyboard(ALLEGRO_KEY_L, -1));

        //--Shader Toggles
        RegisterControl("Shader0", CreateControlWatchingKeyboard(ALLEGRO_KEY_0, -1));
        RegisterControl("Shader1", CreateControlWatchingKeyboard(ALLEGRO_KEY_1, -1));
        RegisterControl("Shader2", CreateControlWatchingKeyboard(ALLEGRO_KEY_2, -1));
        RegisterControl("RecompileShaders", CreateControlWatchingKeyboard(ALLEGRO_KEY_F9, -1));

        //--Kerning
        RegisterControl("RebuildKerning", CreateControlWatchingKeyboard(ALLEGRO_KEY_F8, -1));

        //--Loop Controls
        //RegisterControl("LoopBack", CreateControlWatchingKeyboard(ALLEGRO_KEY_LEFTBRACKET, -1));
        //RegisterControl("LoopForw", CreateControlWatchingKeyboard(ALLEGRO_KEY_RIGHTBRACKET, -1));

        //--[Joypad Depression]
        //--Creates controls watching all the keys on the joypad. These are for display purposes only
        //  and are not needed if the #define is not set.
        #ifdef DBG_SHOW_JOYPAD_DEPRESSION
        {
            ALLEGRO_JOYSTICK *rJoystick = al_get_joystick(0);
            if(rJoystick)
            {
                char tBuffer[32];
                for(int i = 0; i < al_get_joystick_num_buttons(rJoystick); i ++)
                {
                    sprintf(tBuffer, "DBGJoy%02i", i);
                    RegisterControl(tBuffer, CreateControlWatchingJoystick(i, -1));
                }
                for(int i = 0; i < al_get_joystick_num_sticks(rJoystick); i ++)
                {
                    for(int p = 0; p < 4; p ++)
                    {
                        int tIndex = JOYSTICK_OFFSET_STICK + (i * JOYSTICK_STICK_CONSTANT)  + p;
                        sprintf(tBuffer, "DBGStk%03i", tIndex);
                        RegisterControl(tBuffer, CreateControlWatchingJoystick(tIndex, -1));
                    }
                }
            }
        }
        #endif

    #endif
}
