//--[TransitionManager]
//--Manager responsible for transitioning between two things, where 'things' is nearly anything.
//  This manager causes the game to fade out, switch modes, then fade back in.
//--The execution of the switch is performed by an Executor-type class.  One should be made for
//  all the different logical transitions, and they are deleted once used.
//--Both AlphaDeltas should be POSITIVE.  The program will reject a call using negative alphas,
//  as the TransitionManager subtracts when fading in.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--Local Definitions
#define TM_TRANSITION_FAIL 0
#define TM_TRANSITION_GRAB 1
#define TM_TRANSITION_FADEOUT 2
#define TM_TRANSITION_SWITCH 3
#define TM_TRANSITION_FADEIN 4
#define TM_TRANSITION_RELEASE 5

#define TM_STANDARD_DELTA 0.10f

class TransitionManager
{
    private:
    //--System
    //--Executor
    bool mOwnsExecutor;
    RootExecutor *mExecutor;

    //--Fading
    int mTransitionStage;
    float mAlpha;
    float mAlphaDeltaOut;
    float mAlphaDeltaIn;

    //--Screen storage
    uint32_t mTextureHandle;

    protected:

    public:
    //--System
    TransitionManager();
    ~TransitionManager();

    //--Public Variables
    //--Property Queries
    bool IsTransitioning();

    //--Manipulators
    void SetFadeSpeeds(float pDeltaOut, float pDeltaIn);
    void SetExecutor(RootExecutor *pExecutor, bool pAcceptOwnership);
    void BeginTransition(RootExecutor *pExecutor, bool pAcceptOwnership);

    //--Core Methods
    //--Update
    void Update();
    void UpdatePaused(uint8_t pPauseFlags);

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    static TransitionManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *L);
};

//--Scripted Transition Controls
int Hook_TM_SetupTransition(lua_State *L);
int Hook_TM_ExecutorSetProperty(lua_State *L);
int Hook_TM_ExecTransition(lua_State *L);

