//--Base
#include "DisplayManager.h"

//--Classes
//--Definitions
#include "GlDfn.h"

//--Generics
#include "SugarLinkedList.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

//--<DOCUMENTATION>
//--Static definitions of the glFunctionDfn.  These are manually wrangled
//  functions because Allegro 5.0.0 does NOT support GLSL!
//--See GlDfn.h for more.

//--[GL Defines]
#define GL_FRAGMENT_SHADER                0x8B30
#define GL_VERTEX_SHADER                  0x8B31
#define GL_COMPILE_STATUS                 0x8B81
#define GL_INFO_LOG_LENGTH                0x8B84

//--A
GLATTACHSHADER_FUNC sglAttachShader;
GLACTIVETEXTURE_FUNC sglActiveTexture;

//--B
GLBINDBUFFER_FUNC sglBindBuffer;
GLBINDFRAMEBUFFER_FUNC sglBindFramebuffer;
GLBUFFERDATA_FUNC sglBufferData;
GLBLENDFUNCSEPARATE_FUNC sglBlendFuncSeparate;
GLBINDVERTEXARRAY_FUNC sglBindVertexArray;
GLBLENDEQUATION_FUNC sglBlendEquation;

//--C
GLCHECKFRAMEBUFFERSTATUS_FUNC sglCheckFramebufferStatus;
GLCLIENTACTIVETEXTURE_FUNC sglClientActiveTexture;
GLCOMPILESHADER_FUNC sglCompileShader;
GLCOMPRESSEDTEX2D_FUNC sglCompressedTexImage2D;
GLCREATEPROGRAM_FUNC sglCreateProgram;
GLCREATESHADER_FUNC sglCreateShader;

//--D
GLDELETEBUFFERS_FUNC sglDeleteBuffers;
GLDELETEPROGRAM_FUNC sglDeleteProgram;
GLDELETESHADER_FUNC sglDeleteShader;
GLDELETEVERTEXARRAYS_FUNC sglDeleteVertexArrays;
GLDRAWBUFFERS_FUNC sglDrawBuffers;

//--E
GLENABLEVERTEXATTRIBARRAY_FUNC sglEnableVertexAttribArray;

//--F
GLFRAMEBUFFERTEXTURE_FUNC sglFramebufferTexture;

//--G
GLGENERATEMIPMAP_FUNC sglGenerateMipmap;
GLGENBUFFERS_FUNC sglGenBuffers;
GLGENFRAMEBUFFERS_FUNC sglGenFramebuffers;
GLGENVERTEXARRAYS_FUNC sglGenVertexArrays;
GLGETSHADERIV_FUNC sglGetShaderiv;
GLGETSHADERINFOLOG_FUNC sglGetShaderInfoLog;
GLGETSHADERIV_FUNC sglGetProgramiv;
GLGETSHADERINFOLOG_FUNC sglGetProgramInfoLog;
GLGETUNIFORMLOCATION sglGetUniformLocation;

//--L
GLLINKPROGRAM_FUNC sglLinkProgram;

//--M
GLMULTITEXCOORD2F_FUNC sglMultiTexCoord2f;
GLMULTITEXCOORD3F_FUNC sglMultiTexCoord3f;

//--S
GLSHADERSOURCE_FUNC sglShaderSource;
WGLSWAPINTERVALEXT_FUNC swglSwapIntervalEXT;

//--U
GLUSEPROGRAM_FUNC sglUseProgram;
GLUNIFORM1I sglUniform1i;
GLUNIFORM1F sglUniform1f;
GLUNIFORM4F sglUniform4f;

//--V
GLVERTEXATTRIBPOINTER_FUNC sglVertexAttribPointer;

///--[Worker Functions]
void DisplayManager::ShaderWorker(SugarLinkedList *pNamesList, const char *pProgramName, const char *pVertPath, const char *pFragPath)
{
    //--Worker function which will register the program and its shader sub-components.
    if(!pNamesList || !pProgramName || !pFragPath || !pVertPath) return;
    RegisterProgram(pProgramName);

    //--Vertex shader.
    char tBuffer[128];
    sprintf(tBuffer, "%sVert", pProgramName);
    RegisterShader(tBuffer, pVertPath, true);
    AttachShaderToProgram(pProgramName, tBuffer);

    //--Fragment shader.
    sprintf(tBuffer, "%sFrag", pProgramName);
    RegisterShader(tBuffer, pFragPath, false);
    AttachShaderToProgram(pProgramName, tBuffer);

    //--Attach to the linking list.
    static int xDummyPtr = 0;
    pNamesList->AddElement(pProgramName, &xDummyPtr);
}

///--[System]
void DisplayManager::ShaderExec()
{
    //--Calls the lua file to build all shaders.
    DebugManager::PushPrint(true, "[GL Shaders] Begin\n");

    //--No lua path, do nothing.
    if(!xShaderBuildPath)
    {
        DebugManager::Print("Shader compilation cancelled, no path. Call DM_SetShaderPath() to specify the shader path.\n");
    }
    //--Execute.
    else
    {
        bool tOldFlag = LuaManager::xFailSilently;
        LuaManager::xFailSilently = false;
        LuaManager::Fetch()->ExecuteLuaFile(xShaderBuildPath);
        LuaManager::xFailSilently = tOldFlag;
    }

    //--Log Setup.
    int i = 0;
    GLuint *rProgramHandlePtr = (GLuint *)mProgramList->PushIterator();
    GLuint *rShaderHandlePtr = (GLuint *)mShaderList->PushIterator();

    //--Log.
    DebugManager::Print(" Shader report.\n");
    DebugManager::Print("  Shaders built: %i.\n", mShaderList->GetListSize());
    DebugManager::Print("  Programs built: %i.\n", mProgramList->GetListSize());
    DebugManager::Print("  Listing:\n");
    while(rProgramHandlePtr && rShaderHandlePtr)
    {
        DebugManager::Print("   %i: %s %p %p\n", i, mProgramList->GetIteratorName(), rProgramHandlePtr, rShaderHandlePtr);

        //--Next.
        i ++;
        rProgramHandlePtr = (GLuint *)mProgramList->AutoIterate();
        rShaderHandlePtr = (GLuint *)mShaderList->AutoIterate();
    }

    //--Finish up.
    DebugManager::PopPrint("[GL Shaders] Done\n");
}

///--[Manipulators]
void DisplayManager::RegisterProgram(const char *pName)
{
    //--Registers a new program, and stores the handle in the program list with
    //  the matching name.
    if(!pName) return;

    SetMemoryData(__FILE__, __LINE__);
    GLuint *nContainterHandle = (GLuint *)starmemoryalloc(sizeof(GLuint));
    *nContainterHandle = sglCreateProgram();
    mProgramList->AddElement(pName, nContainterHandle, &DisplayManager::DeleteProgram);
}
void DisplayManager::RegisterShader(const char *pName, const char *pFilePath, bool pIsVertex)
{
    //--Creates a new shader and registers it in the shader list with the name
    //  provided.  You must specify where to find the shader's sourcecode as
    //  well as whether or not it's a Vertex or Raster type shader.
    if(!pName || !pFilePath) return;

    //--Create the shader first, and get a handle for it.  The file does not need to exist at
    //  registration time, it can be procedurally generated if necessary, it only needs to exist
    //  when CompilePrograms is called.
    int tType = GL_FRAGMENT_SHADER;
    if(pIsVertex) tType = GL_VERTEX_SHADER;
    SetMemoryData(__FILE__, __LINE__);
    GLuint *nShaderHandle = (GLuint *)starmemoryalloc(sizeof(GLuint));
    *nShaderHandle = sglCreateShader(tType);

    //--Register it.
    mShaderList->AddElement(pName, nShaderHandle, &DisplayManager::DeleteShader);

    //--Get a string which is the provided file.  This needs to be freed after
    //  the source is uploaded.
    char *tFileDump = TextFileRead(pFilePath);

    //--Attach the file path to the shader object.  Other overloads allow multiple paths.
    sglShaderSource(*nShaderHandle, 1, (const GLchar **)(&tFileDump), NULL);

    //--Clean.
    free(tFileDump);
}
void DisplayManager::RegisterProgramAlias(const char *pAliasName, const char *pProgramName)
{
    //--Registers an alias for the named shader. The alias is what is looked for when activating shaders.
    if(!pAliasName || !pProgramName) return;

    //--Make sure the shader exists.
    void *rCheckPtr = mProgramList->GetElementByName(pProgramName);
    if(!rCheckPtr) return;

    //--Crossregister.
    mrProgramAliasList->AddElement(pAliasName, rCheckPtr);
}
void DisplayManager::AttachShaderToProgram(const char *pProgramName, const char *pShaderName)
{
    //--Attaches the named shader to the named program.
    if(!pProgramName || !pShaderName) return;

    GLuint *rProgramHandlePtr = (GLuint *)mProgramList->GetElementByName(pProgramName);
    GLuint *rShaderHandlePtr = (GLuint *)mShaderList->GetElementByName(pShaderName);
    if(!rShaderHandlePtr || !rProgramHandlePtr)
    {
        //fprintf(stderr, "%p %p\n", rProgramHandlePtr, rShaderHandlePtr);
        return;
    }
    sglAttachShader(*rProgramHandlePtr, *rShaderHandlePtr);
}
void DisplayManager::LinkProgram(const char *pName)
{
    //--Links the specified program, if it exists.
    if(!pName) return;
    GLuint *rProgramHandlePtr = (GLuint *)mProgramList->GetElementByName(pName);
    if(!rProgramHandlePtr) return;

    sglLinkProgram(*rProgramHandlePtr);
    PrintProgramInfoLog(*rProgramHandlePtr);
}
void DisplayManager::DeleteProgram(void *pPtr)
{
    //--A program is a GLHandle.  It also needs to be freed.
    GLuint *rHandle = (GLuint *)pPtr;
    sglDeleteProgram(*rHandle);
    free(pPtr);
}
void DisplayManager::DeleteShader(void *pPtr)
{
    //--A shader is a GLHandle.  It also needs to be freed.
    GLuint *rHandle = (GLuint *)pPtr;
    sglDeleteShader(*rHandle);
    free(pPtr);
}

///--[Core Methods]
void DisplayManager::CompileShaders()
{
    //--Compiles all shaders that require compilation on the shader list.  If a shader doesn't need it, it is skipped.
    //  All compiled shaders are then printed to the console if xFlag is true.
    DebugManager::PushPrint(true, "[DisplayManager] Compiling shaders\n");

    //--Setup
    GLint tReturnStatus = false;

    GLuint *rShaderHandlePtr = (GLuint *)mShaderList->PushIterator();
    while(rShaderHandlePtr)
    {
        //--Is the shader compiled yet?  If not, compile it.
        sglGetShaderiv(*rShaderHandlePtr, GL_COMPILE_STATUS, &tReturnStatus);
        if(tReturnStatus == GL_FALSE)
        {
            sglCompileShader(*rShaderHandlePtr);

            //--<DEBUG>
            sglGetShaderiv(*rShaderHandlePtr, GL_COMPILE_STATUS, &tReturnStatus);
            DebugManager::Print("Shader %i %s Status: %i\n", *rShaderHandlePtr, mShaderList->GetIteratorName(), tReturnStatus);
            if(tReturnStatus == GL_FALSE)
            {
                PrintShaderInfoLog(*rShaderHandlePtr);
            }
        }
        rShaderHandlePtr = (GLuint *)mShaderList->AutoIterate();
    }
    DebugManager::PopPrint("[DisplayManager] Finished shader compilation\n");
}
void DisplayManager::LinkPrograms()
{
    //--Links all programs.
    void *rDummyPtr = mProgramBuildList->PushIterator();
    while(rDummyPtr)
    {
        //--Get the name and link it.
        const char *rName = mProgramBuildList->GetIteratorName();
        LinkProgram(rName);

        //--Next.
        rDummyPtr = mProgramBuildList->AutoIterate();
    }

    //--Clear the build list.
    mProgramBuildList->ClearList();
}
void DisplayManager::ClearAllShaderData()
{
    //--Clears both the program and shader list. GL internally handles detachment to prevent conflicts,
    //  shaders will auto-delete when there are no programs holding them, and programs auto-detach all
    //  shaders when deleted. Much less of a headache for me!
    mProgramList->ClearList();
    mShaderList->ClearList();
    mrProgramAliasList->ClearList();
    mProgramBuildList->ClearList();
}

///--[Update/Rendering]
void DisplayManager::ActivateProgram(const char *pProgramName)
{
    //--Activates a specified program. Pass NULL to return GL's renderer to the factory-zero settings.
    if(!pProgramName)
    {
        sglUseProgram(0);
        return;
    }

    //--Check if the program handle exists. We search by alias. If not found, defaults to no program.
    mLastProgramHandle = 0;
    GLuint *rProgramHandlePtr = (GLuint *)mrProgramAliasList->GetElementByName(pProgramName);
    if(!rProgramHandlePtr) return;

    //--Activate it.
    sglUseProgram(*rProgramHandlePtr);
    mLastProgramHandle = *rProgramHandlePtr;
}

///--[Debug]
void DisplayManager::PrintShaderInfoLog(GLuint pShaderHandle)
{
    //--Debug function that spits all the shader's info log data to the
    //  console.  Requires the handle, but it will bark if the handle is
    //  invalid, so don't check it.
    int tInfoLogLength = 0;
    int tCharsWritten  = 0;
    char *tInfoLog = NULL;

    sglGetShaderiv(pShaderHandle, GL_INFO_LOG_LENGTH, &tInfoLogLength);
    if(tInfoLogLength > 0)
    {
        SetMemoryData(__FILE__, __LINE__);
        tInfoLog = (char *)starmemoryalloc(tInfoLogLength);
        sglGetShaderInfoLog(pShaderHandle, tInfoLogLength, &tCharsWritten, tInfoLog);
        fprintf(stderr, "%s\n",tInfoLog);
    }
    free(tInfoLog);
}
void DisplayManager::PrintProgramInfoLog(GLuint pProgramHandle)
{
    //--As above, but needs a program handle.
    int tInfoLogLength = 0;
    int tCharsWritten  = 0;
    char *tInfoLog = NULL;

    sglGetProgramiv(pProgramHandle, GL_INFO_LOG_LENGTH, &tInfoLogLength);
    if(tInfoLogLength > 0)
    {
        SetMemoryData(__FILE__, __LINE__);
        tInfoLog = (char *)starmemoryalloc(tInfoLogLength);
        sglGetProgramInfoLog(pProgramHandle, tInfoLogLength, &tCharsWritten, tInfoLog);
        fprintf(stderr, "%s\n",tInfoLog);
    }
    free(tInfoLog);
}
