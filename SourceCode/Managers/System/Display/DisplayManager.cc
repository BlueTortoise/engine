//--Base
#include "DisplayManager.h"

//--Classes
#include "VisualLevel.h"

//--CoreClasses
#include "SugarLinkedList.h"
#include "SugarBitmap.h"

//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "OptionsManager.h"

//=========================================== System ==============================================
DisplayManager::DisplayManager()
{
    //--[DisplayManager]
    //--System
    mIsReady = true;
    mTakeScreenshot = false;
    mIsScreenshotReady = false;

    //--Sorted Rendering
    mUseSortedRendering = true;
    mRenderingList = new SugarLinkedList(false);

    //--Shaders
    mProgramList = new SugarLinkedList(true);
    mShaderList = new SugarLinkedList(true);
    mProgramBuildList = new SugarLinkedList(false);
    mrProgramAliasList = new SugarLinkedList(false);

    //--Fullscreen and Sizing
    mIsFullscreen = false;

    //--Public variables
    mLastProgramHandle = 0;

    //--Display modes
    mActiveDisplaySlot = 0;
    mDisplayModesTotal = 0;
    mDisplayModes = NULL;

    //--Mouse commands
    mHasObtainedMouse = false;
    mIsMouseVisible = true;
    mIsMouseLocked = false;

    //--[SDL Only]
    #if defined _SDL_PROJECT_
    mWindow = NULL;
    mGLContext = NULL;
    mActiveCanvas = NULL;
    #endif

    //--[Type-Specific Construction]
    //--The following functions can be found in a different subfile depending on which library
    //  is handling the video.
    SaveDisplayModes();
    ConstructorTail();
}
DisplayManager::~DisplayManager()
{
    delete mProgramList;
    delete mShaderList;
    delete mProgramBuildList;
    delete mrProgramAliasList;
    for(int i = 0; i < mDisplayModesTotal; i ++)
    {
        free(mDisplayModes[i].mDetails);
    }
    free(mDisplayModes);

    //--[SDL Destruction]
    #if defined _SDL_PROJECT_
        SDL_GL_DeleteContext(mGLContext);
        SDL_DestroyWindow(mWindow);
    #endif
}
bool DisplayManager::xUseHardLetterbox = true;
int DisplayManager::xHorizontalOffset = 0;
int DisplayManager::xVerticalOffset = 0;
int DisplayManager::xViewportW = 0;
int DisplayManager::xViewportH = 0;
float DisplayManager::xViewportScaleX = 1.0f;
float DisplayManager::xViewportScaleY = 1.0f;
float DisplayManager::xScaledOffsetX = 0.0f;
float DisplayManager::xScaledOffsetY = 0.0f;
float DisplayManager::xMouseLockX = 0.0f;
float DisplayManager::xMouseLockY = 0.0f;
bool DisplayManager::xHasHadRenderPassSinceFullscreen = false;
bool DisplayManager::xDontSetViewport = false;
bool DisplayManager::xLowDefinitionFlag = false;

//--Path to the shader compilation file. At program startup, this file is called to build all shaders.
//  Shaders can be recompiled during program execution, which re-calls this file.
char *DisplayManager::xShaderBuildPath = NULL;

//====================================== Property Queries =========================================
bool DisplayManager::IsFullscreen()
{
    return mIsFullscreen;
}
bool DisplayManager::IsUsingSortedRendering()
{
    return mUseSortedRendering;
}
int DisplayManager::GetTotalDisplayModes()
{
    return mDisplayModesTotal;
}
DisplayInfo DisplayManager::GetDisplayMode(int pIndex)
{
    if(pIndex < 0 || pIndex >= mDisplayModesTotal) return mDummyDisplayInfo;
    return mDisplayModes[pIndex];
}
DisplayInfo DisplayManager::GetActiveDisplayMode(int &sIndex)
{
    //--Also returns the index the active display mode was in!
    sIndex = mActiveDisplaySlot;
    if(mActiveDisplaySlot < 0 || mActiveDisplaySlot >= mDisplayModesTotal)
    {
        return mDummyDisplayInfo;
    }
    return mDisplayModes[mActiveDisplaySlot];
}

//========================================= Manipulators ==========================================
void DisplayManager::FlipFullscreen()
{
    //--Flips from windowed to fullscreen, or fullscreen to windowed, as a toggle.  May take a few
    //  seconds, during which play may or may not be suspended (based on the driver).
    mHasObtainedMouse = 0;
    xHasHadRenderPassSinceFullscreen = false;
    mIsFullscreen = !mIsFullscreen;

    //--[Allegro Version]
    #if defined _ALLEGRO_PROJECT_

        //--Setup.
        GLOBAL *rGlobal = Global::Shared();

        //--Reset the various flags.  We cannot be guaranteed that Allegro did not modify them at some
        //  point after the last display creation.
        if(!mIsFullscreen)
        {
            al_set_display_flag(rGlobal->gDisplay, ALLEGRO_FULLSCREEN_WINDOW, false);
        }
        else
        {
            al_set_display_flag(rGlobal->gDisplay, ALLEGRO_FULLSCREEN_WINDOW, true);
        }

        //--Store the window sizes.  The virtual canvas doesn't change size, but the window may have.
        rGlobal->gWindowWidth  = al_get_display_width (rGlobal->gDisplay);
        rGlobal->gWindowHeight = al_get_display_height(rGlobal->gDisplay);
        xMouseLockX = VIRTUAL_CANVAS_X / 2.0f;
        xMouseLockY = VIRTUAL_CANVAS_Y / 2.0f;

    //--[SDL Version]
    #elif defined _SDL_PROJECT_

        //--Turning off fullscreen...
        if(!mIsFullscreen)
        {
            SDL_SetWindowFullscreen(mWindow, 0);
        }
        //--Turning on fullscreen...
        else
        {
            SDL_SetWindowFullscreen(mWindow, SDL_WINDOW_FULLSCREEN_DESKTOP);
        }

        //--Storage.
        GLOBAL *rGlobal = Global::Shared();
        SDL_GetWindowSize(mWindow, &rGlobal->gWindowWidth, &rGlobal->gWindowHeight);
        xMouseLockX = VIRTUAL_CANVAS_X / 2.0f;
        xMouseLockY = VIRTUAL_CANVAS_Y / 2.0f;

    #endif

    //--Letterboxing: Reset the offsets before any rendering occurs.
    //fprintf(stderr, "Fullscreen: %i - %i %i\n", mIsFullscreen, rGlobal->gWindowWidth, rGlobal->gWindowHeight);
    LetterboxOffsets();
    //fprintf(stderr, " %i %i, %i %i\n", xHorizontalOffset, xVerticalOffset, xViewportW, xViewportH);

    //--Renormalize the mouse lock coordinates. They need to be set to integers, though.
    //NormalizeMouse(xMouseLockX, xMouseLockY);
    xMouseLockX = (int)xMouseLockX;
    xMouseLockY = (int)xMouseLockY;

    //--The VisualLevel, if present, responds to the resize by moving its UI around.
    VisualLevel *rVisualLevel = VisualLevel::Fetch();
    if(rVisualLevel) rVisualLevel->SetUIConstants();

    //--Store the change in the options.
    OptionsManager::Fetch()->mIsFullscreen = mIsFullscreen;
}
void DisplayManager::LetterboxOffsets()
{
    //--Offsets the virtual canvas such that it maintains a 1:1 aspect ratio with the window.
    //  This will leave black borders at the top and bottom of the screen if they weren't 1:1 to
    //  begin with, otherwise it will have no effect.
    GLOBAL *rGlobal = Global::Shared();

    //--Reset some flags.
    rGlobal->gRenderedPixelsW = (int)rGlobal->gWindowWidth;
    rGlobal->gRenderedPixelsH = (int)rGlobal->gWindowHeight;

    //--Calculate the aspect ratio of the virtual canvas.
    if(rGlobal->gScreenWidthPixels == 0.0f) return;
    float tVCAspectRatio = (float)VIRTUAL_CANVAS_Y / (float)VIRTUAL_CANVAS_X;

    //--If the screen is smaller than the virtual canvas, ignore the aspect ratio flag.
    bool tIgnoreAspectRatioFlag = false;
    if(rGlobal->gRenderedPixelsH < VIRTUAL_CANVAS_Y || rGlobal->gRenderedPixelsW < VIRTUAL_CANVAS_X)
    {
        tIgnoreAspectRatioFlag = true;
    }

    //--Normal behavior, match the aspect ratio of the window to the virtual canvas.
    if(!OptionsManager::Fetch()->GetOptionB("ForceDirectAspectRatios") || tIgnoreAspectRatioFlag)
    {
        //--Calculate the aspect ratio of the window.
        if(rGlobal->gWindowWidth == 0.0f) return;
        float tWinAspectRatio = (float)rGlobal->gWindowHeight / (float)rGlobal->gWindowWidth;

        //--If they're identical, then we can stop right now.
        xHorizontalOffset = 0;
        xVerticalOffset = 0;
        xViewportW = 0;
        xViewportH = 0;
        //fprintf(stderr, "Aspect ratios: %f %f\n", tVCAspectRatio, tWinAspectRatio);
        if(tVCAspectRatio == tWinAspectRatio)
        {
            xViewportW = rGlobal->gWindowWidth;
            xViewportH = rGlobal->gWindowHeight;
            return;
        }

        //--The screen is wider than the aspect ratio allows, letterbox the left and right sides.
        if(tWinAspectRatio < tVCAspectRatio)
        {
            float tExpandedX = (float)VIRTUAL_CANVAS_X * (rGlobal->gWindowHeight / (float)VIRTUAL_CANVAS_Y);
            xHorizontalOffset = (rGlobal->gWindowWidth - tExpandedX) / 2;
            xViewportW = tExpandedX;
            xViewportH = rGlobal->gWindowHeight;
            //fprintf(stderr, "Screen is wider, letterboxing sides.\n");
            //fprintf(stderr, " Window: %i %i\n", rGlobal->gWindowWidth, rGlobal->gWindowHeight);
            //fprintf(stderr, " Viewport: %i %i\n", xViewportW, xViewportH);
            //fprintf(stderr, " Offsets: %i %i\n", xHorizontalOffset, xVerticalOffset);


            //xHorizontalOffset = ((rGlobal->gWindowWidth) - (rGlobal->gWindowHeight / tVCAspectRatio)) / 2;
            //xViewportW = rGlobal->gWindowWidth - (xHorizontalOffset * 2);
            //xViewportH = rGlobal->gWindowHeight;
            rGlobal->gRenderedPixelsW = xViewportW;
            rGlobal->gRenderedPixelsH = xViewportH;
        }
        //--The screen is taller than the aspect ratio allows, letterbox the top and bottom.
        else
        {
            xVerticalOffset = ((rGlobal->gWindowHeight) - (rGlobal->gWindowWidth * tVCAspectRatio)) / 2;
            xViewportW = rGlobal->gWindowWidth;
            xViewportH = rGlobal->gWindowHeight - (xVerticalOffset * 2);
            rGlobal->gRenderedPixelsW = xViewportW;
            rGlobal->gRenderedPixelsH = xViewportH;
            /*
            fprintf(stderr, "Vertical offset: %i\n", xVerticalOffset);
            fprintf(stderr, " Viewport: %i\n", xViewportH);
            fprintf(stderr, " Screen size: %i\n", rGlobal->gWindowHeight);
            fprintf(stderr, " Virtual canvas Y: %i\n", (int)VIRTUAL_CANVAS_Y);
            fprintf(stderr, " End Point: %i\n", (int)xViewportH + xVerticalOffset + xVerticalOffset);*/
        }
    }
    //--Strict-sizing behavior.  Select the next-lowest direct multiple of the virtual canvas size
    //  to fit the current window, and letterbox off the rest.
    else
    {
        //--Determine how many ratios can fit.
        float tMaxX = rGlobal->gScreenWidthPixels;
        float tMaxY = rGlobal->gScreenHeightPixels;
        bool tBreachedMax = false;
        while(!tBreachedMax)
        {
            //--Double the X/Y, check against window.
            if(tMaxX + rGlobal->gScreenWidthPixels < rGlobal->gWindowWidth && tMaxY + rGlobal->gScreenHeightPixels < rGlobal->gWindowHeight)
            {
                tMaxX = tMaxX + rGlobal->gScreenWidthPixels;
                tMaxY = tMaxY + rGlobal->gScreenHeightPixels;
            }
            else
            {
                break;
            }
        }

        //--Debug
        //fprintf(stderr, "The largest size available is %f %f\n", tMaxX, tMaxY);

        //--It is always the case that both edges will need to be letterboxed, though that value
        //  may equate to zero.  Simply center the screen.
        xHorizontalOffset = (rGlobal->gWindowWidth - tMaxX) / 2.0f;
        xVerticalOffset   = (rGlobal->gWindowHeight - tMaxY) / 2.0f;
        xViewportW = tMaxX;
        xViewportH = tMaxY;
    }
}
void DisplayManager::NormalizeMouse(float &sRawX, float &sRawY)
{
    //--Taking in a raw mouse position (passed from Allegro's event handler), modifies it such that
    //  it matches the virtual canvas positions.
    //GLOBAL *rGlobal = Global::Shared();

    //--Horiztonal.
    sRawX = sRawX - xHorizontalOffset;
    sRawX = sRawX / xViewportW;
    sRawX = sRawX * VIRTUAL_CANVAS_X;
    //sRawX = sRawX / xViewportScaleX;

    //--Vertical.
    sRawY = sRawY - xVerticalOffset;
    sRawY = sRawY / xViewportH;
    sRawY = sRawY * VIRTUAL_CANVAS_Y;
    //sRawY = sRawY / xViewportScaleY;
}
#include "Startup.h"
void DisplayManager::SwitchDisplayModes(int pIndex)
{
    //--Attempts to switch to the given display mode. Auto-fails if the index is out of range or
    //  if the index was currently in use.
    if(pIndex < 0 || pIndex >= mDisplayModesTotal || pIndex == mActiveDisplaySlot) return;

    //--Setup.

    //--[Allegro Version]
    #if defined _ALLEGRO_PROJECT_

        //--Setup.
        GLOBAL *rGlobal = Global::Shared();

        //--Resize!
        int tOldDisplaySlot = mActiveDisplaySlot;
        mActiveDisplaySlot = pIndex;
        rGlobal->gWindowWidth = mDisplayModes[mActiveDisplaySlot].mWidth;
        rGlobal->gWindowHeight = mDisplayModes[mActiveDisplaySlot].mHeight;
        bool tSuccess = al_resize_display(rGlobal->gDisplay, rGlobal->gWindowWidth, rGlobal->gWindowHeight);

        //--If it failed, snap back to the old index.
        if(!tSuccess)
        {
            mActiveDisplaySlot = tOldDisplaySlot;
            rGlobal->gWindowWidth = mDisplayModes[mActiveDisplaySlot].mWidth;
            rGlobal->gWindowHeight = mDisplayModes[mActiveDisplaySlot].mHeight;
        }

    //--[SDL Version]
    #elif defined _SDL_PROJECT_

        //--Setup.
        GLOBAL *rGlobal = Global::Shared();

        //--Resize!
        mActiveDisplaySlot = pIndex;
        rGlobal->gWindowWidth = mDisplayModes[mActiveDisplaySlot].mWidth;
        rGlobal->gWindowHeight = mDisplayModes[mActiveDisplaySlot].mHeight;
        SDL_SetWindowSize(mWindow, rGlobal->gWindowWidth, rGlobal->gWindowHeight);

    #endif
}

//========================================= Core Methods ==========================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void DisplayManager::ActivateMaskRender(int pStencilCode)
{
    //--Causes the program to not render color/depth, but write to the stencil buffer with the given code.
    glEnable(GL_STENCIL_TEST);
    glColorMask(false, false, false, false);
    glDepthMask(false);
    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, pStencilCode, 0xFF);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
}
void DisplayManager::ActivateCountermaskRender()
{
    //--Causes the program to not render color/depth, but write zeroes to the stencil buffer.
    glEnable(GL_STENCIL_TEST);
    glColorMask(false, false, false, false);
    glDepthMask(false);
    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, 0, 0xFF);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
}
void DisplayManager::ActivateStencilRender(int pStencilCode)
{
    //--Causes the program to render color/depth, but only on stencilled pixels.
    glEnable(GL_STENCIL_TEST);
    glColorMask(true, true, true, true);
    glDepthMask(true);
    glStencilMask(0xFF);
    glStencilFunc(GL_EQUAL, pStencilCode, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
}
void DisplayManager::DeactivateStencilling()
{
    //--Turn off stencil rendering.
    glDisable(GL_STENCIL_TEST);
}

//======================================= Pointer Routing =========================================
SugarLinkedList *DisplayManager::GetRenderingList()
{
    return mRenderingList;
}
SugarLinkedList *DisplayManager::GetProgramConstructionList()
{
    return mProgramBuildList;
}

//======================================== SDL Functions ==========================================
#if defined _SDL_PROJECT_
SDL_Window *DisplayManager::GetWindow()
{
    return mWindow;
}
SDL_Surface *DisplayManager::GetSurface()
{
    return mActiveCanvas;
}
#endif

//====================================== Static Functions =========================================
DisplayManager *DisplayManager::Fetch()
{
    return Global::Shared()->gDisplayManager;
}
SugarLinkedList *DisplayManager::FetchRenderingList()
{
    return Global::Shared()->gDisplayManager->GetRenderingList();
}

//========================================= Lua Hooking ===========================================
void DisplayManager::HookToLuaState(lua_State *pLuaState)
{
    ///--[System]
    /* DM_GetResolution() (2 integers)
       Returns 2 integers representing the X/Y size of the screen. */
    lua_register(pLuaState, "DM_GetResolution", &Hook_DM_GetResolution);

    /* DM_GetDisplayModeInfo("Current Mode") (1 integer)
       DM_GetDisplayModeInfo("Total Modes") (1 integer)
       DM_GetDisplayModeInfo("Data", iModeIndex) (3 integers)
       Gets the requested information about the display mode. */
    lua_register(pLuaState, "DM_GetDisplayModeInfo", &Hook_DM_GetDisplayModeInfo);

    /* DM_GetEnumeration(sName)
       Returns an integer representing the value of an enumeration stored in the DM.  See
       DisplayManagerGLEnumerations.cc for a list. */
    lua_register(pLuaState, "DM_GetEnumeration", &Hook_DM_GetEnumeration);

    /* DM_SetDisplayMode(iIndex)
       Sets the display mode to the one in the index. Fails if the index is illegal or in use. */
    lua_register(pLuaState, "DM_SetDisplayMode", &Hook_DM_SetDisplayMode);

    ///--[Shaders]
    /* DM_SetShaderPath(sPath)
       Sets the path used to compile shaders, both during startup and re-compile calls. */
    lua_register(pLuaState, "DM_SetShaderPath", &Hook_DM_SetShaderPath);

    /* DM_BuildShader(sInternalName, sVertexPath, sFragmentPath)
       Builds a shader with the given name using the provided vertex and fragment code. The sInternalName
       is what is looked for in DM_AddShaderAlias().*/
    lua_register(pLuaState, "DM_BuildShader", &Hook_DM_BuildShader);

    /* DM_AddShaderAlias(sShaderName, sAlias)
       Registers an alias to the named shader program. The alias is what the program looks for. If
       the alias already exists, it is overwritten with the new shader. */
    lua_register(pLuaState, "DM_AddShaderAlias", &Hook_DM_AddShaderAlias);

    /* DM_CompileShaders()
       Compiles and links all shaders/programs once setup is complete. */
    lua_register(pLuaState, "DM_CompileShaders", &Hook_DM_CompileShaders);

    /* DM_ClearShaders()
       Wipes all shader data, including aliases. */
    lua_register(pLuaState, "DM_ClearShaders", &Hook_DM_ClearShaders);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
///--[System]
int Hook_DM_GetResolution(lua_State *L)
{
    //DM_GetResolution()
    lua_pushnumber(L, Global::Shared()->gWindowWidth);
    lua_pushnumber(L, Global::Shared()->gWindowHeight);

    return 2;
}
int Hook_DM_GetDisplayModeInfo(lua_State *L)
{
    //DM_GetDisplayModeInfo("Current Mode") (1 integer)
    //DM_GetDisplayModeInfo("Total Modes") (1 integer)
    //DM_GetDisplayModeInfo("Data", iModeIndex) (3 integers)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("DM_GetDisplayModeInfo");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Current display mode.
    if(!strcasecmp(rSwitchType, "Current Mode") && tArgs == 1)
    {
        int tActiveIndex;
        DisplayManager::Fetch()->GetActiveDisplayMode(tActiveIndex);
        lua_pushinteger(L, tActiveIndex);
        return 1;
    }
    //--How many modes there are total.
    else if(!strcasecmp(rSwitchType, "Total Modes") && tArgs == 1)
    {
        lua_pushinteger(L, DisplayManager::Fetch()->GetTotalDisplayModes());
        return 1;
    }
    //--Information about a display mode. X/Y/Refresh.
    else if(!strcasecmp(rSwitchType, "Data") && tArgs == 2)
    {
        DisplayInfo tInfo = DisplayManager::Fetch()->GetDisplayMode(lua_tointeger(L, 2));
        lua_pushinteger(L, tInfo.mWidth);
        lua_pushinteger(L, tInfo.mHeight);
        lua_pushinteger(L, tInfo.mRefreshRate);
        return 3;
    }
    //--Unknown request.
    else
    {
        LuaPropertyError("DM_GetDisplayModeInfo", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_DM_GetEnumeration(lua_State *L)
{
    //DM_GetEnumeration(sName)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs != 1)
    {
        LuaArgError("DM_GetEnumeration");
        lua_pushinteger(L, 0);
        return 1;
    }

    //--Get the value.
    uint32_t tValue = DisplayManager::GetEnumeration(lua_tostring(L, 1));
    lua_pushinteger(L, tValue);
    return 1;
}
int Hook_DM_SetDisplayMode(lua_State *L)
{
    //DM_SetDisplayMode(iIndex)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("DM_SetDisplayMode");

    DisplayManager::Fetch()->SwitchDisplayModes(lua_tointeger(L, 1));
    return 0;
}

///--[Shaders]
int Hook_DM_SetShaderPath(lua_State *L)
{
    //DM_SetShaderPath(sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("DM_SetShaderPath");

    //--Set.
    ResetString(DisplayManager::xShaderBuildPath, lua_tostring(L, 1));
    fprintf(stderr, "==== Set shader path to %s\n", DisplayManager::xShaderBuildPath);
    return 0;
}
int Hook_DM_BuildShader(lua_State *L)
{
    //DM_BuildShader(sInternalName, sVertexPath, sFragmentPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 3) return LuaArgError("DM_BuildShader");

    //--Set.
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    SugarLinkedList *rConstructionList = rDisplayManager->GetProgramConstructionList();
    rDisplayManager->ShaderWorker(rConstructionList, lua_tostring(L, 1), lua_tostring(L, 2), lua_tostring(L, 3));
    return 0;
}
int Hook_DM_AddShaderAlias(lua_State *L)
{
    //DM_AddShaderAlias(sShaderName, sAlias)
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("DM_AddShaderAlias");

    //--Set.
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    rDisplayManager->RegisterProgramAlias(lua_tostring(L, 2), lua_tostring(L, 1));
    return 0;
}
int Hook_DM_CompileShaders(lua_State *L)
{
    //DM_CompileShaders()
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    rDisplayManager->CompileShaders();
    rDisplayManager->LinkPrograms();
    return 0;
}
int Hook_DM_ClearShaders(lua_State *L)
{
    //DM_ClearShaders()
    DisplayManager *rDisplayManager = DisplayManager::Fetch();
    rDisplayManager->ClearAllShaderData();
    return 0;
}
