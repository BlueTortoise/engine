//--Base
#include "DisplayManager.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"
#include "SugarBitmap.h"

//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "OptionsManager.h"

//--[Allegro Version]
//--This is meant to be used in Allegro projects. Once the DisplayManager has constructed itself,
//  this function will be called to build the display.
#if defined _ALLEGRO_PROJECT_
void DisplayManager::ConstructorTail()
{
    //--Function is called immediately after the DisplayManager has called its setup functions.
    //  It will create the display.
    DebugManager::PushPrint(true, "[DisplayManager] Creating display...\n");

    //--Setup.
    GLOBAL *rGlobal = Global::Shared();
    OptionsManager *rOptionsManager = OptionsManager::Fetch();

    //--Fullscreen or Windowed Flag
    DebugManager::Print("Handling fullscreen/windowed flags.\n");
    bool tUseFullscreen = rOptionsManager->GetOptionB("Fullscreen");
    if(!tUseFullscreen)
    {
        al_set_new_display_flags(ALLEGRO_OPENGL | ALLEGRO_WINDOWED);
        mIsFullscreen = false;
    }
    else
    {
        al_set_new_display_flags(ALLEGRO_OPENGL | ALLEGRO_FULLSCREEN_WINDOW);
        mIsFullscreen = true;
    }

    //--Sets up the Depth and Stencil Buffer stuff.  These can be blocked for debug purposes
    //  by the options.
    DebugManager::Print("Checking depth buffer flag.\n");
    bool tBlockDepthBuffer = rOptionsManager->GetOptionB("BlockDepthBuffer");
    if(!tBlockDepthBuffer)
    {
        al_set_new_display_option(ALLEGRO_DEPTH_SIZE, 16, ALLEGRO_SUGGEST);
    }

    DebugManager::Print("Checking stencil buffer flag.\n");
    bool tBlockStencilBuffer = rOptionsManager->GetOptionB("BlockStencilBuffer");
    if(!tBlockStencilBuffer)
    {
        al_set_new_display_option(ALLEGRO_STENCIL_SIZE, 8, ALLEGRO_SUGGEST);
    }

    //--Activate VSync.
    al_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);

    //--Get the location to spawn the window.  Only executes if either is non
    //  zero for compatibility reasons.
    DebugManager::Print("Checking window location flags.\n");
    int tWindowX = rOptionsManager->GetOptionI("StartWinX");
    int tWindowY = rOptionsManager->GetOptionI("StartWinY");
    if(tWindowX || tWindowY)
    {
        al_set_new_window_position(tWindowX, tWindowY);
    }

    //--Flag for stereo buffers. Can be switched off.
    DebugManager::Print("Checking block-double-buffer flag.\n");
    if(!rOptionsManager->GetOptionB("BlockDoubleBuffering"))
    {
        al_set_new_display_option(ALLEGRO_STEREO, 0, ALLEGRO_SUGGEST);
    }

    //--Create the Display and test it.
    rGlobal->gWindowWidth = rOptionsManager->GetOptionI("WinSizeX");
    rGlobal->gWindowHeight = rOptionsManager->GetOptionI("WinSizeY");

    DebugManager::ForcePrint("\n");
    DebugManager::ForcePrint("If the program crashes at this point, then your display could not be created.\n");
    DebugManager::ForcePrint("This means your OGL drivers are not updated, or the program is unable to run.\n");
    rGlobal->gDisplay = al_create_display(rGlobal->gWindowWidth, rGlobal->gWindowHeight);
    if(!rGlobal->gDisplay)
    {
        DebugManager::ForcePrint("Display of size %i by %i failed to create.\n", rGlobal->gWindowWidth, rGlobal->gWindowHeight);
        DebugManager::ForcePrint("With no display, the program will now exit\n");
        rGlobal->gQuit = true;
        return;
    }

    //--Find the display mode that worked and set it as active in the DisplayInfo list.
    DebugManager::Print("Storing active display mode.\n");
    for(int i = 0; i < mDisplayModesTotal; i ++)
    {
        if(mDisplayModes[i].mWidth == rGlobal->gWindowWidth && mDisplayModes[i].mHeight == rGlobal->gWindowHeight)
        {
            mActiveDisplaySlot = i;
            break;
        }
    }

    //--Standard GL Settings (GL Stack pops back to these)
    DebugManager::Print("Setting OpenGL standards.\n");
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0);

    //--Setup default OGL stuff
    DebugManager::Print("Setting OpenGL standards.\n");
    al_set_target_backbuffer(rGlobal->gDisplay);

    //--Post-setup
    DebugManager::Print("Setting window title.\n");
    al_set_window_title(rGlobal->gDisplay, "Starlight Engine");

    //--Static setup.
    DebugManager::Print("Building enumerations list.\n");
    ConstructEnumerationList();

    //--Store GL statistics.
    DebugManager::Print("Getting GL statistics.\n");
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &SugarBitmap::xMaxTextureSize);
    SugarBitmap::xAtlasMaxSize = SugarBitmap::xMaxTextureSize / 2;
    //SugarBitmap::xMaxTextureSize = 4096;

    //--These may not be the same if fullscreen was set, so store them.
    DebugManager::Print("Refreshing screen sizes.\n");
    rGlobal->gWindowWidth  = al_get_display_width (rGlobal->gDisplay);
    rGlobal->gWindowHeight = al_get_display_height(rGlobal->gDisplay);
    xMouseLockX = rGlobal->gWindowWidth  / 2.0f;
    xMouseLockY = rGlobal->gWindowHeight / 2.0f;

    //--Clean up.
    DebugManager::PopPrint("[DisplayManager] Display setup complete.\n");
}
void DisplayManager::SaveDisplayModes()
{
    //--Checks the available display modes and stores their data inside a library-independent structure.
    //--Collects a list of all available display modes. We are only interested in unique modes:
    //  two modes with the same W/H but different refresh rates are ignored, we only want modes
    //  with refresh rates of 60.0 fps. Likewise, only 32bit formats are acceptable.
    int tRawModesTotal = al_get_num_display_modes();
    SetMemoryData(__FILE__, __LINE__);
    ALLEGRO_DISPLAY_MODE *tModeList = (ALLEGRO_DISPLAY_MODE *)starmemoryalloc(sizeof(ALLEGRO_DISPLAY_MODE) * tRawModesTotal);
    for(int i = 0; i < tRawModesTotal; i ++)
    {
        //--Get the raw mode. Allegro passes NULL back on failure.
        if(!al_get_display_mode(i, &tModeList[i])) continue;

        //--Check if the mode is applicable to our program. Count if it is.
        if(tModeList[i].refresh_rate == 60 && tModeList[i].format == 23)
        {
            mDisplayModesTotal ++;
        }
    }

    //--Allocate space for all the modes that matched.
    char tBuffer[64];
    int tScans = 0;
    SetMemoryData(__FILE__, __LINE__);
    mDisplayModes = (DisplayInfo *)starmemoryalloc(sizeof(DisplayInfo) * mDisplayModesTotal);
    for(int i = 0; i < mDisplayModesTotal; i ++)
    {
        //--Find the next applicable resolution and store it. It's logically impossible for a
        //  particular structure to not be initialized, but I NULL the string here for safety anyway.
        mDisplayModes[i].mDetails = NULL;

        for(int p = tScans; p < tRawModesTotal; p ++)
        {
            if(tModeList[p].refresh_rate != 60 || tModeList[p].format != 23) continue;

            //--Save for later.
            tScans = p+1;

            //--Copy data.
            mDisplayModes[i].mWidth = tModeList[p].width;
            mDisplayModes[i].mHeight = tModeList[p].height;
            mDisplayModes[i].mRefreshRate = tModeList[p].refresh_rate;

            //--Create a string which has this as simple, human-readable data.
            sprintf(tBuffer, "%ix%ix%i", mDisplayModes[i].mWidth, mDisplayModes[i].mHeight, mDisplayModes[i].mRefreshRate);
            ResetString(mDisplayModes[i].mDetails, tBuffer);
            break;
        }
    }

    //--Dummy display mode. Do not deallocate until destructor!
    mDummyDisplayInfo.mWidth = 0;
    mDummyDisplayInfo.mHeight = 0;
    mDummyDisplayInfo.mRefreshRate = 0;
    mDummyDisplayInfo.mDetails = NULL;
    ResetString(mDummyDisplayInfo.mDetails, "ERROR");

    //--Clean.
    free(tModeList);
}
#endif
