//--Base
#include "DisplayManager.h"

//--Classes
//--CoreClasses
//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "ControlManager.h"

//--Handles mouse control. While the position of the mouse is polled by the ControlManager, the mouse itself
//  can be hid, locked (forced to stay in the same spot), and have its image changed here. These are all hardware
//  dependent, so they might not work on some systems.

//--[Common]
bool DisplayManager::HasGrabbedMouse()
{
    return (mHasObtainedMouse >= 3 && xHasHadRenderPassSinceFullscreen);
}
bool DisplayManager::IsHardwareMouseVisible()
{
    return mIsMouseVisible;
}
bool DisplayManager::IsMouseLocked()
{
    return mIsMouseLocked;
}
void DisplayManager::LockMouse()
{
    if(!mIsMouseLocked) mHasObtainedMouse = false;
    mIsMouseLocked = true;
}
void DisplayManager::UnlockMouse()
{
    mIsMouseLocked = false;
}

//--[Allegro Version]
#if defined _ALLEGRO_PROJECT_
void DisplayManager::ShowHardwareMouse()
{
    mIsMouseVisible = true;
    al_show_mouse_cursor(Global::Shared()->gDisplay);
}
void DisplayManager::HideHardwareMouse()
{
    mIsMouseVisible = false;
    al_hide_mouse_cursor(Global::Shared()->gDisplay);
}
void DisplayManager::UpdateMouseLock()
{
    //--Do nothing if the mouse is not locked.
    if(!mIsMouseLocked) return;

    //--Special: Do *NOT* attempt to grab and lock the mouse if it's not over the display, or is
    //  currently in a click. The user might be doing something on their desktop!
    if(!mHasObtainedMouse)
    {
        //--Get the data.
        ALLEGRO_MOUSE_STATE tMouseState;
        al_get_mouse_state(&tMouseState);

        //--Outside of the window?
        if(tMouseState.x <= 0 || tMouseState.x >= Global::Shared()->gWindowWidth || tMouseState.y <= 0 || tMouseState.y >= Global::Shared()->gWindowHeight) return;
    }

    //--Count the ticks we've held the mouse for.
    mHasObtainedMouse ++;

    //--Force reposition.
    al_set_mouse_xy(Global::Shared()->gDisplay, xMouseLockX, xMouseLockY);

    //--Make sure the ControlManager knows, since it won't update from the display unless it receives an event.
    //  We need to preserve the Z position since it doesn't get locked, so pass -700.0f.
    ControlManager *rControlManager = ControlManager::Fetch();
    rControlManager->SetMouseCoordsByCanvas(xMouseLockX, xMouseLockY, -700.0f);
}

//--[SDL Version]
#elif defined _SDL_PROJECT_
void DisplayManager::ShowHardwareMouse()
{
    //TODO
}
void DisplayManager::HideHardwareMouse()
{
    //TODO
}
void DisplayManager::UpdateMouseLock()
{

}

#endif

