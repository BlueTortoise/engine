//--[DisplayManager]
//--Controls the fetching of the display, shaders, openGL standard procedures, and letterboxing.
//  This is ultimately the manager in charge of everything that gets rendered, though not all
//  render code necessarily runs through it.

#pragma once

#include "Definitions.h"

//--[Local Structures]
typedef struct
{
    int mWidth;
    int mHeight;
    int mRefreshRate;
    char *mDetails;
}DisplayInfo;

//--[Local Definitions]
#define VIRTUAL_CANVAS_X 1366.0f
#define VIRTUAL_CANVAS_Y  768.0f
#define CANX (VIRTUAL_CANVAS_X)
#define CANY (VIRTUAL_CANVAS_Y)

class DisplayManager
{
    private:
    //--System
    bool mIsReady;
    bool mTakeScreenshot;
    bool mIsScreenshotReady;

    //--Sorted Rendering
    bool mUseSortedRendering;
    SugarLinkedList *mRenderingList;

    //--Shaders
    SugarLinkedList *mProgramList; //GLuint *, master
    SugarLinkedList *mShaderList; //GLuint *, master
    SugarLinkedList *mProgramBuildList; //Dummyvar
    SugarLinkedList *mrProgramAliasList; //GLuint *, ref

    //--Fullscreen and Sizing
    bool mIsFullscreen;

    //--Enumerations.
    static SugarLinkedList *xEnumerationList;

    //--Display modes.
    int mActiveDisplaySlot;
    int mDisplayModesTotal;
    DisplayInfo *mDisplayModes;
    DisplayInfo mDummyDisplayInfo;

    //--Mouse commands
    int mHasObtainedMouse;
    bool mIsMouseVisible;
    bool mIsMouseLocked;

    //--[SDL Only]
    #if defined _SDL_PROJECT_
    SDL_Window *mWindow;
    SDL_GLContext mGLContext;
    SDL_Surface *mActiveCanvas;
    #endif

    public:
    //System
    DisplayManager();
    ~DisplayManager();

    //--Public Variables
    GLint mLastProgramHandle;
    static bool xUseHardLetterbox;
    static int xHorizontalOffset;
    static int xVerticalOffset;
    static int xViewportW;
    static int xViewportH;
    static float xViewportScaleX;
    static float xViewportScaleY;
    static float xScaledOffsetX;
    static float xScaledOffsetY;
    static float xMouseLockX;
    static float xMouseLockY;
    static bool xHasHadRenderPassSinceFullscreen;
    static bool xDontSetViewport;
    static bool xLowDefinitionFlag;
    static char *xShaderBuildPath;

    //--Property Queries
    bool IsFullscreen();
    bool IsUsingSortedRendering();
    int GetTotalDisplayModes();
    DisplayInfo GetDisplayMode(int pIndex);
    DisplayInfo GetActiveDisplayMode(int &sIndex);

    //--Manipulators
    void FlipFullscreen();
    static void LetterboxOffsets();
    static void NormalizeMouse(float &sRawX, float &sRawY);
    void SwitchDisplayModes(int pIndex);

    //--Core Methods
    //--Initialization (Allegro or SDL)
    void ConstructorTail();
    void SaveDisplayModes();

    //--Dummy Functions
    static void dsglAttachShader(GLuint, GLuint);
    static void dsglActiveTexture(GLenum);
    static void dsglBindBuffer(GLenum, GLuint);
    static void dsglBindFramebuffer(GLenum, GLuint);
    static void dsglBindVertexArray(GLuint);
    static void dsglBlendFuncSeparate(GLenum, GLenum, GLenum, GLenum);
    static void dsglBufferData(GLenum, GLsizeiptr, const GLvoid *, GLenum);
    static void dsglBlendEquation(GLenum);
    static GLenum dsglCheckFramebufferStatus(GLenum);
    static void dsglClientActiveTexture(GLenum);
    static void dsglCompileShader(GLuint);
    static void dsglCompressedTexImage2D(GLenum, GLint, GLenum, GLsizei, GLsizei, GLint, GLsizei, const GLvoid *);
    static GLuint dsglCreateProgram();
    static GLuint dsglCreateShader(GLenum);
    static void dsglDeleteBuffers(GLsizei, const GLuint *);
    static void dsglDeleteProgram(GLuint);
    static void dsglDeleteShader(GLuint);
    static void dsglDeleteVertexArrays(GLsizei, const GLuint *);
    static void dsglDrawBuffers(GLsizei, const GLenum *);
    static void dsglEnableVertexAttribArray(GLuint);
    static void dsglFramebufferTexture(GLenum, GLenum, GLuint, GLint);
    static void dsglGenerateMipmap(GLenum);
    static void dsglGenBuffers(GLsizei, GLuint *);
    static void dsglGenFramebuffers(GLsizei, GLuint *);
    static void dsglGenVertexArrays(GLsizei, GLuint *);
    static void dsglGetShaderiv(GLuint, GLenum, GLint *);
    static void dsglGetShaderInfoLog(GLuint, GLsizei, GLsizei *, GLchar *);
    static void dsglGetProgramiv(GLuint, GLenum, GLint *);
    static void dsglGetProgramInfoLog(GLuint, GLsizei, GLsizei *, GLchar *);
    static GLint dsglGetUniformLocation(GLuint, const GLchar *);
    static void dsglLinkProgram(GLuint);
    static void dsglMultiTexCoord2f(GLenum, GLfloat, GLfloat);
    static void dsglMultiTexCoord3f(GLenum, GLfloat, GLfloat, GLfloat);
    static void dsglShaderSource(GLuint, GLsizei, const GLchar **, const GLint *);
    static void dswglSwapIntervalEXT(GLint);
    static void dsglUseProgram(GLuint);
    static void dsglUniform4f(GLint, GLfloat, GLfloat, GLfloat, GLfloat);
    static void dsglUniform1i(GLint, GLint);
    static void dsglVertexAttribPointer(GLuint, GLint, GLenum, GLboolean, GLsizei, const GLvoid *);

    //--GL Standards
    static void StdFrustum();
    static void CleanFrustum();
    static void StdOrtho(int pXSize, int pYSize);
    static void StdOrtho();
    static void CleanOrtho();
    static void StdModelPush();
    static void StdModelPop();
    static void StdAttribPush();
    static void StdAttribPop();
    static void Project(float pX, float pY, float pZ, float *pReturns);
    static void Unproject(float pX, float pY, float pZ, float *pReturns);

    //--Mouse Commands
    bool HasGrabbedMouse();
    bool IsHardwareMouseVisible();
    bool IsMouseLocked();
    void ShowHardwareMouse();
    void HideHardwareMouse();
    void LockMouse();
    void UnlockMouse();
    void UpdateMouseLock();

    //--GL Enumerations.
    static void ConstructEnumerationList();
    static uint32_t GetEnumeration(const char *pName);

    //--Shaders
    void ShaderWorker(SugarLinkedList *pNamesList, const char *pProgramName, const char *pVertPath, const char *pFragPath);
    void ShaderExec();
    void RegisterProgram(const char *pName);
    void RegisterShader(const char *pName, const char *pFilePath, bool pIsVertex);
    void RegisterProgramAlias(const char *pAliasName, const char *pProgramName);
    void AttachShaderToProgram(const char *pProgramName, const char *pShaderName);
    void LinkProgram(const char *pName);
    static void DeleteProgram(void *pPtr);
    static void DeleteShader(void *pPtr);
    void CompileShaders();
    void LinkPrograms();
    void ActivateProgram(const char *pProgramName);
    void ClearAllShaderData();
    void PrintShaderInfoLog(GLuint pShaderHandle);
    void PrintProgramInfoLog(GLuint pProgramHandle);

    //--Wrangling GL Extensions
    static void *WrangleSubroutine(const char *pFuncName, void *pAltPtr);
    static void WrangleExec();

    //--Update
    //--File I/O
    char *TextFileRead(const char *pFilePath);

    //--Drawing
    static void ActivateMaskRender(int pStencilCode);
    static void ActivateCountermaskRender();
    static void ActivateStencilRender(int pStencilCode);
    static void DeactivateStencilling();

    //--Pointer Routing
    SugarBitmap *GetBackbufferWrapper();
    SugarLinkedList *GetRenderingList();
    SugarLinkedList *GetProgramConstructionList();

    //--SDL Functions
    #if defined _SDL_PROJECT_
    SDL_Window *GetWindow();
    SDL_Surface *GetSurface();
    #endif

    //--Static Functions
    static DisplayManager *Fetch();
    static SugarLinkedList *FetchRenderingList();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_DM_GetResolution(lua_State *L);
int Hook_DM_GetDisplayModeInfo(lua_State *L);
int Hook_DM_GetEnumeration(lua_State *L);
int Hook_DM_SetDisplayMode(lua_State *L);
int Hook_DM_SetShaderPath(lua_State *L);
int Hook_DM_BuildShader(lua_State *L);
int Hook_DM_AddShaderAlias(lua_State *L);
int Hook_DM_CompileShaders(lua_State *L);
int Hook_DM_ClearShaders(lua_State *L);
