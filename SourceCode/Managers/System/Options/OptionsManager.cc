//--Base
#include "OptionsManager.h"

//--Classes
#include "VisualLevel.h"
#include "FlexMenu.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
OptionsManager::OptionsManager()
{
    //--[OptionsManager]
    //--System
    mErrorFlag = false;

    //--Storage
    mOptionList    = new SugarLinkedList(false); //Set to false for safety.
    mDebugFlagList = new SugarLinkedList(false);

    //--Options
    mIsFullscreen = false;
    mIsMemoryMode = false;
    mWinSpawnX = 0;
    mWinSpawnY = 0;
    mWinSizeX = 800;
    mWinSizeY = 600;
    mMusicVolume = 75;
    mSoundVolume = 100;

    //--Hardware Configuration
    mForcePowerOfTwo = false;
    mBlockDepthBuffer = false;
    mBlockStencilbuffer = false;
    mBlockDoubleBuffering = false;
    mUseRAMLoading = false;
    mRequestedAccelerateVisual = 1;
    mRequestedOpenGLMajor = 4;
    mRequestedOpenGLMinor = 5;

    //--Engine Options
    mNoAudioSuppression = false;
    mForceDirectAspectRatios = false;

    //--Debug Variables
    for(int i = 0; i < OM_DEBUGVARS; i ++) mDebugVariables[i] = 0.0f;
    mDebugVariables[2] = 1.25;
    mDebugVariables[3] = 90;
    mDebugVariables[4] = 25;

    //--Set the console command list to be case-insensitive.
    xConsoleCommandList->SetCaseSensitivity(false);

    //--Configuration File List
    mConfigFilePaths = new SugarLinkedList(true);
}
OptionsManager::~OptionsManager()
{
    delete mOptionList;
    delete mDebugFlagList;
    delete mConfigFilePaths;
}

//--[Public Statics]
//--System
bool OptionsManager::xIsDefiningOption = false;
bool OptionsManager::xErrorFlag = false;
SugarLinkedList *OptionsManager::xConsoleCommandList = new SugarLinkedList(false);

//--Program
bool OptionsManager::xAutoLaunch3DMode = false;

//--Pairanormal
bool OptionsManager::xAllowFlashingImages = true;

//====================================== Property Queries =========================================
bool OptionsManager::GetErrorFlag()
{
    bool tOrigFlag = mErrorFlag;
    mErrorFlag = false;
    return tOrigFlag;
}
bool OptionsManager::GetDebugFlag(const char *pName)
{
    //--Returns the named debug flag from the RLL.
    bool *rReturnFlag = (bool *)mDebugFlagList->GetElementByName(pName);
    if(!rReturnFlag) return false;
    return *rReturnFlag;
}
OptionPack *OptionsManager::GetOption(const char *pName)
{
    return (OptionPack *)mOptionList->GetElementByName(pName);
}
int OptionsManager::GetOptionType(const char *pName)
{
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!rPack) return POINTER_TYPE_FAIL;
    return rPack->mTypeFlag;
}
int OptionsManager::GetOptionI(const char *pName)
{
    //--Check.
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_INT))
    {
        //--If it's not an int or a float, fail.
        if(!CheckPtr(rPack, POINTER_TYPE_FLOAT))
        {
            return OM_ERROR_INT;
        }
        //--If it's a float, return it casted.
        else
        {
            float *rPtr = (float *)rPack->rPtr;
            int tIntValue = (int)(*rPtr);
            return tIntValue;
        }
    }
    int *rPtr = (int *)rPack->rPtr;
    return *rPtr;
}
float OptionsManager::GetOptionF(const char *pName)
{
    //--Check.
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_FLOAT))
    {
        //--If it's not a float or an int, fail.
        if(!CheckPtr(rPack, POINTER_TYPE_INT))
        {
            return OM_ERROR_FLOAT;
        }
        //--If it's an int, return it casted.
        else
        {
            int *rPtr = (int *)rPack->rPtr;
            float tFloatValue = (float)(*rPtr);
            return tFloatValue;
        }
    }
    float *rPtr = (float *)rPack->rPtr;
    return *rPtr;
}
char *OptionsManager::GetOptionS(const char *pName)
{
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_STRING)) return OM_ERROR_STRING;
    char *rPtr = (char *)rPack->rPtr;
    return rPtr;
}
bool OptionsManager::GetOptionB(const char *pName)
{
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_BOOL)) return OM_ERROR_BOOL;
    bool *rPtr = (bool *)rPack->rPtr;
    return *rPtr;
}
float OptionsManager::GetDebugVar(int pSlot)
{
    if(pSlot < 0 || pSlot >= OM_DEBUGVARS) return 0.0f;
    return mDebugVariables[pSlot];
}
int OptionsManager::GetOrbitCode()
{
    return OM_ORBITCODE_UNKNOWN;
}

//========================================= Manipulators ==========================================
OptionPack *OptionsManager::AddOption(const char *pName, void *pPtr, int pType, const char *pPath)
{
    if(!pName || !pPtr || pType == POINTER_TYPE_FAIL || !pPath) return NULL;
    SetMemoryData(__FILE__, __LINE__);
    OptionPack *nPack = (OptionPack *)starmemoryalloc(sizeof(OptionPack));
    nPack->Initialize();
    nPack->rPtr = pPtr;
    nPack->mTypeFlag = pType;
    nPack->mIsProgramLocal = false;
    nPack->mIsDefinedExternally = xIsDefiningOption;
    strncpy(nPack->mFilePath, pPath, sizeof(nPack->mFilePath));
    mOptionList->AddElement(pName, nPack);
    return nPack;
}
void OptionsManager::SetOptionI(const char *pName, int pValue)
{
    //--Sets an option to an integer value. If it's a float, it still works.
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_INT))
    {
        //--We can still set the value if it's a float.
        if(CheckPtr(rPack, POINTER_TYPE_FLOAT))
        {
            float *rPtr = (float *)rPack->rPtr;
            *rPtr = (float)pValue;
            rPack->mWasChanged = true;
        }

        //--Return here.
        return;
    }

    //--Dereference, set.
    int *rPtr = (int *)rPack->rPtr;
    *rPtr = pValue;
    rPack->mWasChanged = true;
}
void OptionsManager::SetOptionF(const char *pName, float pValue)
{
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_FLOAT))
    {
        //--We can still set the value if it's an int.
        if(CheckPtr(rPack, POINTER_TYPE_INT))
        {
            int *rPtr = (int *)rPack->rPtr;
            *rPtr = (int)pValue;
            rPack->mWasChanged = true;
        }

        //--Return here.
        return;
    }

    //--Deteference, set.
    float *rPtr = (float *)rPack->rPtr;
    *rPtr = pValue;
    rPack->mWasChanged = true;
}
void OptionsManager::SetOptionS(const char *pName, const char *pValue)
{
    //--Retrieve the value.
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_STRING)) return;

    //--Clean, reset.
    free(rPack->rPtr);
    rPack->rPtr = InitializeString(pValue);
    rPack->mWasChanged = true;
}
void OptionsManager::SetOptionB(const char *pName, bool pValue)
{
    //--Retrieve the value.
    OptionPack *rPack = (OptionPack *)mOptionList->GetElementByName(pName);
    if(!CheckPtr(rPack, POINTER_TYPE_BOOL)) return;

    //--Set.
    bool *rPtr = (bool *)rPack->rPtr;
    *rPtr = pValue;
    rPack->mWasChanged = true;
}
void OptionsManager::RemOption(const char *pName)
{
    mOptionList->RemoveElementS(pName);
}
void OptionsManager::SetDebugVar(int pSlot, float pValue)
{
    if(pSlot < 0 || pSlot >= OM_DEBUGVARS) return;
    mDebugVariables[pSlot] = pValue;
}
void OptionsManager::RegisterConfigPath(const char *pPath)
{
    if(!pPath) return;
    mConfigFilePaths->AddElement("X", InitializeString(pPath), &FreeThis);
}

//===================================== Private Core Methods ======================================
//========================================= Core Methods ==========================================
void OptionsManager::SetupOptionList()
{
    //--Configuration
    AddOption("Fullscreen",                &mIsFullscreen,              POINTER_TYPE_BOOL, "Config_Engine.lua");
    AddOption("MemoryMode",                &mIsMemoryMode,              POINTER_TYPE_BOOL, "Config_Engine.lua");
    AddOption("BlockDepthBuffer",          &mBlockDepthBuffer,          POINTER_TYPE_BOOL, "Config_Engine.lua");
    AddOption("BlockStencilBuffer",        &mBlockStencilbuffer,        POINTER_TYPE_BOOL, "Config_Engine.lua");
    AddOption("StartWinX",                 &mWinSpawnX,                 POINTER_TYPE_INT,  "Config_Engine.lua");
    AddOption("StartWinY",                 &mWinSpawnY,                 POINTER_TYPE_INT,  "Config_Engine.lua");
    AddOption("WinSizeX",                  &mWinSizeX,                  POINTER_TYPE_INT,  "Config_Engine.lua");
    AddOption("WinSizeY",                  &mWinSizeY,                  POINTER_TYPE_INT,  "Config_Engine.lua");
    AddOption("ForcePowerOfTwo",           &mForcePowerOfTwo,           POINTER_TYPE_BOOL, "Config_Engine.lua");
    AddOption("MusicVolume",               &mMusicVolume,               POINTER_TYPE_INT,  "Config_Engine.lua");
    AddOption("SoundVolume",               &mSoundVolume,               POINTER_TYPE_INT,  "Config_Engine.lua");

    //--Engine Options
    AddOption("AccelVisual",               &mRequestedAccelerateVisual, POINTER_TYPE_INT,  "Config_Engine.lua");
    AddOption("OpenGLMajor",               &mRequestedOpenGLMajor,      POINTER_TYPE_INT,  "Config_Engine.lua");
    AddOption("OpenGLMinor",               &mRequestedOpenGLMinor,      POINTER_TYPE_INT,  "Config_Engine.lua");
    AddOption("NoAudioSuppression",        &mNoAudioSuppression,        POINTER_TYPE_BOOL, "Config_Engine.lua");
    AddOption("ForceDirectAspectRatios",   &mForceDirectAspectRatios,   POINTER_TYPE_BOOL, "Config_Engine.lua");
    AddOption("UseRAMLoading",             &mUseRAMLoading,             POINTER_TYPE_BOOL, "Config_Engine.lua");

    //--Command Line
    OptionPack *rAutoRunPack = AddOption("AutoRun3DMode", &xAutoLaunch3DMode, POINTER_TYPE_BOOL, "Config_Engine.lua");
    if(rAutoRunPack) rAutoRunPack->mIsProgramLocal = true;

    //--UI Options
    AddOption("UseAlternate3DUI", &VisualLevel::xUseAlternateUI, POINTER_TYPE_BOOL, "Config_Classic.lua");
    AddOption("UseClassic2DUI",   &FlexMenu::xUseClassicUI,      POINTER_TYPE_BOOL, "Config_Classic.lua");

    //--Pandemonium Level Render Options
    AddOption("ActorsAnimateMove",      &PandemoniumLevel::xActorIconsMove,      POINTER_TYPE_BOOL, "Config_Classic.lua");
    AddOption("ActorsAnimateMoveTicks", &PandemoniumLevel::xActorMoveTicks,      POINTER_TYPE_INT,  "Config_Classic.lua");
    AddOption("ShowDamageAnimations",   &PandemoniumLevel::xAnimateDamage,       POINTER_TYPE_BOOL, "Config_Classic.lua");
    AddOption("Show2DUnderlays",        &PandemoniumLevel::xShow2DUnderlays,     POINTER_TYPE_BOOL, "Config_Classic.lua");
    AddOption("UseSmallConsoleFont",    &PandemoniumLevel::xUseSmallConsoleFont, POINTER_TYPE_BOOL, "Config_Classic.lua");
    AddOption("DisableDynamicFog",      &PandemoniumLevel::xDisableDynamicFog,   POINTER_TYPE_BOOL, "Config_Classic.lua");

    //--Visual Level Render Options
    AddOption("AllowDamageOverlay", &VisualLevel::xAllowDamageOverlay, POINTER_TYPE_BOOL, "Config_Classic.lua");

    //--Adventure Mode Render Options
    AddOption("LowResAdventureMode", &DisplayManager::xLowDefinitionFlag, POINTER_TYPE_BOOL,     "Config_Adventure.lua");
    AddOption("DisallowPerlinNoise", &Global::Shared()->gDisallowPerlinNoise, POINTER_TYPE_BOOL, "Config_Adventure.lua");
}
bool OptionsManager::CheckPtr(OptionPack *pPack, int pExpectedType)
{
    //--Returns true if the Pack is of the expected type
    //  Returns false if it is NULL, or is the wrong type, and sets the error
    //  flag accordingly.
    if(!pPack || pPack->mTypeFlag != pExpectedType)
    {
        mErrorFlag = true;
        return false;
    }
    return true;
}
void OptionsManager::PostProcess()
{
    //--Set the filtering option for SugarBitmaps.
    SugarBitmap::xForcePowerOfTwo = mForcePowerOfTwo;

    //--Clear any lingering flags.
    ClearUpdateFlags();
}
void OptionsManager::PrintReport()
{
    //--Prints a report of data pulled from the game's state after setup is completed. Sometimes the
    //  data set in the options will not match due to driver errors, so this report is useful for
    //  diagnostics.
    int32_t tAuxBuffers = 0;

    //--Get the values.
    glGetIntegerv(GL_AUX_BUFFERS, &tAuxBuffers);

    //--GL Version.
    int tGLMajor = 0;
    int tGLMinor = 0;
    int r=0, g=0, b=0;
    #if defined _SDL_PROJECT_
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &tGLMajor);
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &tGLMinor);

        //--Color properties.
        SDL_GL_GetAttribute(SDL_GL_RED_SIZE, &r);
        SDL_GL_GetAttribute(SDL_GL_GREEN_SIZE, &g);
        SDL_GL_GetAttribute(SDL_GL_BLUE_SIZE, &b);
    #endif

    //--Print.
    fprintf(stderr, "\n== Program Status Report: ==\n");
    fprintf(stderr, " Expected OpenGL Version %i.%i\n", 4, 5);
    fprintf(stderr, " Received OpenGL Version %i.%i\n", tGLMajor, tGLMinor);
    fprintf(stderr, " Bit sizes: %i %i %i\n", r, g, b);
    fprintf(stderr, " Total auxillary draw buffers: %i\n", tAuxBuffers);
    fprintf(stderr, " Max texture size: %i\n", SugarBitmap::xMaxTextureSize);
    fprintf(stderr, "==     End of Report      ==\n\n");
}
void OptionsManager::RunConfigFiles()
{
    //--Runs all registered configuration files. This builds all variables needed by all sub-games.
    LuaManager *rLuaManager = LuaManager::Fetch();
    LuaManager::xFailSilently = true;
    char *rConfigPath = (char *)mConfigFilePaths->PushIterator();
    while(rConfigPath)
    {
        rLuaManager->ExecuteLuaFile(rConfigPath);
        rConfigPath = (char *)mConfigFilePaths->AutoIterate();
    }
    LuaManager::xFailSilently = true;

    //--After running the configs, all flags are cleared.
    ClearUpdateFlags();
}
void OptionsManager::ClearUpdateFlags()
{
    //--Sets all options pack flags back to "unmodified".
    OptionPack *rPackage = (OptionPack *)mOptionList->PushIterator();
    while(rPackage)
    {
        rPackage->mWasChanged = false;
        rPackage = (OptionPack *)mOptionList->AutoIterate();
    }
}

//============================================ Update =============================================
//=========================================== File I/O ============================================
void OptionsManager::WriteConfigFiles()
{
    //--[Documentation and Setup]
    //--Writes the config file to the given path, using all the options currently within the OM that
    //  are specified. An option flagged as "Program-Local" will not be written.
    if(mConfigFilePaths->GetListSize() < 1) return;

    //--Setup.
    char tFunctionBuffer[256];
    char tNameBuffer[256];
    char tTypeBuffer[8];
    char tValueBuffer[256];

    //--List of files that have at least one edit done to them.
    int tDummyVar = 3;
    SugarLinkedList *tAtLeastOneEdit = new SugarLinkedList(false);//char *, reference

    //--[Determine Which Files to Write]
    //--Iterate across all variables. If a variable was edited, it needs to write its config file.
    OptionPack *rPackage = (OptionPack *)mOptionList->PushIterator();
    while(rPackage)
    {
        //--If edited:
        if(rPackage->mWasChanged)
        {
            //--If the tAtLeastOneEdit list already has the entry, do nothing:
            void *rCheckEntry = tAtLeastOneEdit->GetElementByName(rPackage->mFilePath);
            if(rCheckEntry)
            {
            }
            //--Entry does not exist. Create it.
            else
            {
                tAtLeastOneEdit->AddElement(rPackage->mFilePath, &tDummyVar);
            }
        }

        //--Next.
        rPackage = (OptionPack *)mOptionList->AutoIterate();
    }

    //--If there are zero files with at least one edit, we don't need to write any config files.
    if(tAtLeastOneEdit->GetListSize() < 1)
    {
        delete tAtLeastOneEdit;
        return;
    }

    //--Create a linked-list that is parallel with the config files. This will contain FILE pointers. The path must be on the
    //  tAtLeastOneEdit list, or it is not created.
    SugarLinkedList *tFileList = new SugarLinkedList(false);
    char *rConfigPath = (char *)mConfigFilePaths->PushIterator();
    while(rConfigPath)
    {
        //--Check if this path exists on the edit list. If not, skip it.
        void *rCheckPtr = tAtLeastOneEdit->GetElementByName(rConfigPath);
        if(rCheckPtr)
        {
            FILE *fOutfile = fopen(rConfigPath, "wb");
            tFileList->AddElementAsTail(rConfigPath, fOutfile);
        }

        //--Next.
        rConfigPath = (char *)mConfigFilePaths->AutoIterate();
    }

    //--[Write Options]
    //--Begin writing.
    OptionPack *rOption = (OptionPack *)mOptionList->PushIterator();
    while(rOption)
    {
        //--If flagged, ignore this option.
        if(rOption->mIsProgramLocal)
        {
            rOption = (OptionPack *)mOptionList->AutoIterate();
            continue;
        }

        //--Name of the option.
        const char *rOptionName = mOptionList->GetIteratorName();
        sprintf(tNameBuffer, "\"%s\"", rOptionName);

        //--If the name begins with "ExpectedLoad_" then don't write it.
        if(!strncasecmp(rOptionName, "ExpectedLoad_", 13))
        {
            rOption = (OptionPack *)mOptionList->AutoIterate();
            continue;
        }

        //--Get the file this variable writes to. If the outfile does not exist, it may be because no variables
        //  in that file updated this pulse.
        FILE *fOutfile = (FILE *)tFileList->GetElementByName(rOption->mFilePath);
        if(!fOutfile)
        {
            //fprintf(stderr, "== Warning, no configuration file %s registered for option %s. ==\n", rOption->mFilePath, mOptionList->GetIteratorName());
            rOption = (OptionPack *)mOptionList->AutoIterate();
            continue;
        }

        //--Resolve the type.
        if(rOption->mTypeFlag == POINTER_TYPE_BOOL)
        {
            //--Type setter.
            strcpy(tTypeBuffer, "\"B\"");

            //--Value goes without quotes.
            bool *rValuePtr = (bool *)rOption->rPtr;
            if(*rValuePtr)
            {
                strcpy(tValueBuffer, "true");
            }
            else
            {
                strcpy(tValueBuffer, "false");
            }
        }
        else if(rOption->mTypeFlag == POINTER_TYPE_FLOAT)
        {
            //--Type setter.
            strcpy(tTypeBuffer, "\"N\"");

            //--Value goes in quotes.
            float *rValuePtr = (float *)rOption->rPtr;
            sprintf(tValueBuffer, "\"%f\"", *rValuePtr);
        }
        else if(rOption->mTypeFlag == POINTER_TYPE_INT)
        {
            //--Type setter.
            strcpy(tTypeBuffer, "\"I\"");

            //--Value goes in quotes.
            int *rValuePtr = (int *)rOption->rPtr;
            sprintf(tValueBuffer, "\"%i\"", *rValuePtr);
        }
        else if(rOption->mTypeFlag == POINTER_TYPE_STRING)
        {
            //--Type setter.
            strcpy(tTypeBuffer, "\"S\"");

            //--Value goes in quotes.
            char *rValuePtr = (char *)rOption->rPtr;
            sprintf(tValueBuffer, "\"%s\"", rValuePtr);
        }

        //--If the option is internal, use this format.
        if(!rOption->mIsDefinedExternally)
        {
            sprintf(tFunctionBuffer, "OM_SetOption(%s, %s)", tNameBuffer, tValueBuffer);
        }
        //--Otherwise, the option is an external one. Use this format.
        else
        {
            sprintf(tFunctionBuffer, "OM_DefineOption(%s, %s, %s, \"%s\")", tNameBuffer, tTypeBuffer, tValueBuffer, rOption->mFilePath);
        }

        //--Write the final copy.
        fprintf(fOutfile, "%s\n", tFunctionBuffer);

        //--Next.
        rOption = (OptionPack *)mOptionList->AutoIterate();
    }

    //--[Clean]
    //--Close all the files we opened.
    FILE *fOutfilePtr = (FILE *)tFileList->PushIterator();
    while(fOutfilePtr)
    {
        fclose(fOutfilePtr);
        fOutfilePtr = (FILE *)tFileList->AutoIterate();
    }
    delete tFileList;
    delete tAtLeastOneEdit;

    //--Clear update flags. All variables are now up to date.
    ClearUpdateFlags();
}
void OptionsManager::WriteLoadFile(const char *pPath)
{
    //--Identical to writing the configuration file, but only writes variables which are used for the
    //  load interrupt counters. These change frequently as the game is updated but are not real
    //  configuration variables.
    //--In order to be written, the variable name needs to begin with "ExpectedLoad_"
    FILE *fOutfile = fopen(pPath, "w");
    if(!fOutfile) return;

    //--Setup.
    char tFunctionBuffer[256];
    char tNameBuffer[256];
    char tTypeBuffer[8];
    char tValueBuffer[256];

    //--Begin writing.
    OptionPack *rOption = (OptionPack *)mOptionList->PushIterator();
    while(rOption)
    {
        //--If flagged, ignore this option.
        if(rOption->mIsProgramLocal)
        {

        }
        //--Write the option.
        else
        {
            //--Name of the option.
            const char *rOptionName = mOptionList->GetIteratorName();
            sprintf(tNameBuffer, "\"%s\"", rOptionName);

            //--If the name does not begin with "ExpectedLoad_" then don't write it.
            if(strncasecmp(rOptionName, "ExpectedLoad_", 13))
            {
                rOption = (OptionPack *)mOptionList->AutoIterate();
                continue;
            }

            //--Resolve the type.
            if(rOption->mTypeFlag == POINTER_TYPE_BOOL)
            {
                //--Type setter.
                strcpy(tTypeBuffer, "\"B\"");

                //--Value goes without quotes.
                bool *rValuePtr = (bool *)rOption->rPtr;
                if(*rValuePtr)
                {
                    strcpy(tValueBuffer, "true");
                }
                else
                {
                    strcpy(tValueBuffer, "false");
                }
            }
            else if(rOption->mTypeFlag == POINTER_TYPE_FLOAT)
            {
                //--Type setter.
                strcpy(tTypeBuffer, "\"N\"");

                //--Value goes in quotes.
                float *rValuePtr = (float *)rOption->rPtr;
                sprintf(tValueBuffer, "\"%f\"", *rValuePtr);
            }
            else if(rOption->mTypeFlag == POINTER_TYPE_INT)
            {
                //--Type setter.
                strcpy(tTypeBuffer, "\"N\"");

                //--Value goes in quotes.
                int *rValuePtr = (int *)rOption->rPtr;
                sprintf(tValueBuffer, "\"%i\"", *rValuePtr);
            }
            else if(rOption->mTypeFlag == POINTER_TYPE_STRING)
            {
                //--Type setter.
                strcpy(tTypeBuffer, "\"S\"");

                //--Value goes in quotes.
                char *rValuePtr = (char *)rOption->rPtr;
                sprintf(tValueBuffer, "\"%s\"", rValuePtr);
            }

            //--If the option is internal, use this format.
            if(!rOption->mIsDefinedExternally)
            {
                sprintf(tFunctionBuffer, "OM_SetOption(%s, %s)", tNameBuffer, tValueBuffer);
            }
            //--Otherwise, the option is an external one. Use this format.
            else
            {
                sprintf(tFunctionBuffer, "OM_DefineOption(%s, %s, %s, \"%s\")", tNameBuffer, tTypeBuffer, tValueBuffer, rOption->mFilePath);
            }

            //--Write the final copy.
            fprintf(fOutfile, "%s\n", tFunctionBuffer);
        }

        //--Next.
        rOption = (OptionPack *)mOptionList->AutoIterate();
    }

    //--Clean.
    fclose(fOutfile);
}

//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
OptionsManager *OptionsManager::Fetch()
{
    return Global::Shared()->gOptionsManager;
}

//========================================= Lua Hooking ===========================================
void OptionsManager::HookToLuaState(lua_State *pLuaState)
{
    /* OM_RegisterConfigPath(sPath)
       Registers a configuration file's path. All configuration files that are detected are run to
       build variables. */
    lua_register(pLuaState, "OM_RegisterConfigPath", &Hook_OM_RegisterConfigPath);

    /* OM_DefineOption(sName, "B", bValue)
       OM_DefineOption(sName, "N", fValue)
       OM_DefineOption(sName, "S", sString)
       Defines an option of the specified type, and sets its initial value. This option can then
       be set or queried elsewhere. The Starlight engine has a list of option names and types it
       uses by default, all of which are defined in Configuration.lua at least once. */
    lua_register(pLuaState, "OM_DefineOption", &Hook_OM_DefineOption);

    lua_register(pLuaState, "OM_GetOption", Hook_OM_GetOption);
    lua_register(pLuaState, "OM_SetOption", Hook_OM_SetOption);
    lua_register(pLuaState, "OM_WriteConfigFiles", Hook_OM_WriteConfigFiles);
    lua_register(pLuaState, "OM_WriteLoadFile", Hook_OM_WriteLoadFile);
}

//======================================= Command Hooking =========================================
void OptionsManager::RegisterCommand(const char *pName, ConsoleFunctionPtr pFuncPtr)
{
    //--Registers the command onto the list.
    xConsoleCommandList->AddElement(pName, (void *)pFuncPtr);
}
void OptionsManager::ExecuteCommand(const char *pCommandLine)
{
    //--Executes the requested command.  Everything up to the first space is parsed, and, if an
    //  error is thrown or the command is not found, an error is barked.
    if(!pCommandLine || pCommandLine[0] == '\0') return;

    //--Buffer out the command.
    char tBuffer[256];
    int tLen = strlen(pCommandLine);
    for(int i = 0; i < tLen; i ++)
    {
        tBuffer[i] = pCommandLine[i];
        tBuffer[i+1] = '\0';
        if(tBuffer[i] == ' ')
        {
            tBuffer[i] = '\0';
            break;
        }
    }

    //--Special cases:
    //--First, if /Macroname is passed, we switch this out to be "LM_Call Macroname"
    //  Any other arguments are tacked on the end.
    char tUseBuffer[256];
    if(tBuffer[0] == '/')
    {
        //--Assemble a "new" command line.
        uint32_t tLastLetter = 0;
        strcpy(tUseBuffer, "LM_Call \"");
        for(int i = 1; i < (int)strlen(tBuffer); i ++)
        {
            tUseBuffer[i+9-1] = tBuffer[i];
            tUseBuffer[i+9+0] = '\0';
            tLastLetter = i+2;
            if(tBuffer[i+1] == ' ') break;
        }

        //--Tack on the rest of the arguments.
        strcat(tUseBuffer, "\" ");
        strcat(tUseBuffer, &pCommandLine[tLastLetter]);
        //fprintf(stderr, "Repaired buffer |%s|\n", tUseBuffer);

        //--Modify the buffer to be just "LM_Call"
        strcpy(tBuffer, "LM_Call");
        pCommandLine = tUseBuffer;
    }

    //--Find it on the list.
    ConsoleFunctionPtr rFuncPtr = (ConsoleFunctionPtr)xConsoleCommandList->GetElementByName(tBuffer);
    //fprintf(stderr, "Ptr %p\n", rFuncPtr);

    //--Command not found.
    if(!rFuncPtr)
    {
        DebugManager::ForcePrint("Failed to execute command %s\n", tBuffer);
        return;
    }

    //--Attempt to execute the command.  If it throws an error, reset the flag and bark the error.
    rFuncPtr(pCommandLine);
    if(xErrorFlag)
    {
        xErrorFlag = false;
        DebugManager::ForcePrint("Error in command: %s\n", pCommandLine);
    }
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_OM_RegisterConfigPath(lua_State *L)
{
    //OM_RegisterConfigPath(sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("OM_RegisterConfigPath");

    OptionsManager::Fetch()->RegisterConfigPath(lua_tostring(L, 1));
    return 0;
}
int Hook_OM_DefineOption(lua_State *L)
{
    //OM_DefineOption(sName, "B", bValue,  sConfigPath)
    //OM_DefineOption(sName, "I", iValue,  sConfigPath)
    //OM_DefineOption(sName, "N", fValue,  sConfigPath)
    //OM_DefineOption(sName, "S", sString, sConfigPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 4) return LuaArgError("OM_DefineOption");

    //--Setup
    OptionsManager *rOptionsManager = OptionsManager::Fetch();
    const char *pName = lua_tostring(L, 1);
    const char *pType = lua_tostring(L, 2);
    const char *pPath = lua_tostring(L, 4);

    //--Static flag.
    OptionsManager::xIsDefiningOption = true;

    //--Boolean
    if(!strcmp(pType, "B"))
    {
        SetMemoryData(__FILE__, __LINE__);
        bool *nPtr = (bool *)starmemoryalloc(sizeof(bool));
        *nPtr = lua_toboolean(L, 3);
        rOptionsManager->AddOption(pName, nPtr, POINTER_TYPE_BOOL, pPath);
    }
    //--Integer
    else if(!strcmp(pType, "I"))
    {
        SetMemoryData(__FILE__, __LINE__);
        int *nPtr = (int *)starmemoryalloc(sizeof(int));
        *nPtr = lua_tointeger(L, 3);
        rOptionsManager->AddOption(pName, nPtr, POINTER_TYPE_INT, pPath);
    }
    //--Floating Point.
    else if(!strcmp(pType, "N") || !strcmp(pType, "F"))
    {
        SetMemoryData(__FILE__, __LINE__);
        float *nPtr = (float *)starmemoryalloc(sizeof(float));
        *nPtr = lua_tonumber(L, 3);
        rOptionsManager->AddOption(pName, nPtr, POINTER_TYPE_FLOAT, pPath);
    }
    //--String
    else if(!strcmp(pType, "S"))
    {
        char *nStringCopy = InitializeString("%s", lua_tostring(L, 3));
        rOptionsManager->AddOption(pName, nStringCopy, POINTER_TYPE_STRING, pPath);
    }

    //--Unset static flag.
    OptionsManager::xIsDefiningOption = false;

    return 0;
}
int Hook_OM_GetOption(lua_State *L)
{
    //--Returns the option requested. Lua will coerce the type for you.
    //OM_GetOption(sName)
    if(lua_gettop(L) != 1)
    {
        fprintf(stderr, "OM_GetOption:  Failed, invalid argument list\n");
        lua_pushnumber(L, OM_ERROR_INT);
        return 1;
    }

    //--Setup.
    OptionsManager *rManager = OptionsManager::Fetch();
    int tOptionType = rManager->GetOptionType(lua_tostring(L, 1));
    if(tOptionType == POINTER_TYPE_FAIL)
    {
        fprintf(stderr, "OM_GetOption:  Failed, option %s not found\n", lua_tostring(L, 1));
        lua_pushnumber(L, OM_ERROR_INT);
        return 1;
    }

    //--Returns.
    if(tOptionType == POINTER_TYPE_INT)
    {
        lua_pushinteger(L, rManager->GetOptionI(lua_tostring(L, 1)));
    }
    else if(tOptionType == POINTER_TYPE_FLOAT)
    {
        lua_pushnumber(L, rManager->GetOptionF(lua_tostring(L, 1)));
    }
    else if(tOptionType == POINTER_TYPE_STRING)
    {
        lua_pushstring(L, rManager->GetOptionS(lua_tostring(L, 1)));
    }
    else if(tOptionType == POINTER_TYPE_BOOL)
    {
        lua_pushboolean(L, rManager->GetOptionB(lua_tostring(L, 1)));
    }
    else
    {
        lua_pushinteger(L, 0);
    }
    return 1;
}
int Hook_OM_SetOption(lua_State *L)
{
    //--Sets the option requested. Second argument should match the type!
    //OM_SetOption(sName, SecondArg)
    if(lua_gettop(L) != 2) return fprintf(stderr, "OM_SetOption:  Failed, invalid argument list\n");

    //--Setup.
    OptionsManager *rManager = OptionsManager::Fetch();
    int tOptionType = rManager->GetOptionType(lua_tostring(L, 1));
    if(tOptionType == POINTER_TYPE_FAIL)
    {
        fprintf(stderr, "OM_SetOption:  Failed, option %s not found\n", lua_tostring(L, 1));
        lua_pushnumber(L, OM_ERROR_INT);
        return 1;
    }

    //--Setters.
    if(tOptionType == POINTER_TYPE_INT)
    {
        rManager->SetOptionI(lua_tostring(L, 1), lua_tointeger(L, 2));
    }
    else if(tOptionType == POINTER_TYPE_FLOAT)
    {
        rManager->SetOptionF(lua_tostring(L, 1), lua_tonumber(L, 2));
    }
    else if(tOptionType == POINTER_TYPE_STRING)
    {
        rManager->SetOptionS(lua_tostring(L, 1), lua_tostring(L, 2));
    }
    else if(tOptionType == POINTER_TYPE_BOOL)
    {
        rManager->SetOptionB(lua_tostring(L, 1), lua_toboolean(L, 2));
    }

    return 0;
}
int Hook_OM_WriteConfigFiles(lua_State *L)
{
    //OM_WriteConfigFiles()

    OptionsManager::Fetch()->WriteConfigFiles();
    return 0;
}
int Hook_OM_WriteLoadFile(lua_State *L)
{
    //OM_WriteLoadFile(sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("OM_WriteLoadFile");

    OptionsManager::Fetch()->WriteLoadFile(lua_tostring(L, 1));
    return 0;
}
