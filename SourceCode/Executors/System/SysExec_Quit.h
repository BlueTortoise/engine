//--[SysExec_Quit]
//--Quits the game when executed.

#pragma once

#include "Definitions.h"
#include "Structures.h"
#include "RootExecutor.h"
#include "Global.h"

class SysExec_Quit : public RootExecutor
{
    private:

    protected:

    public:
    //--System
    SysExec_Quit()
    {

    }
    virtual ~SysExec_Quit()
    {

    }
    virtual void Execute()
    {
        Global::Shared()->gQuit = true;
    }
};
