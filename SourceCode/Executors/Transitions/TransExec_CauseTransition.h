//--[TransExec_CauseTransition]
//--Executor which causes the TransitionManager to activate.  Stores another Executor which it
//  will pass to the TransitionManager.

#pragma once

#include "Definitions.h"
#include "Structures.h"
#include "RootExecutor.h"

#include "TransitionManager.h"

class TransExec_CauseTransition : public RootExecutor
{
    private:
    RootExecutor *mPassExecutor;
    float mFadeOutSpeed;
    float mFadeInSpeed;

    protected:

    public:
    //--System
    TransExec_CauseTransition()
    {
        mPassExecutor = NULL;
        mFadeOutSpeed = 0.0f;
        mFadeInSpeed = 0.0f;
    }
    virtual ~TransExec_CauseTransition()
    {
    }

    //--Manipulators
    void GiveExecutor(RootExecutor *pExecutor)
    {
        mPassExecutor = pExecutor;
    }
    void SetFadeSpeeds(float pFadeOut, float pFadeIn)
    {
        mFadeOutSpeed = pFadeOut;
        mFadeInSpeed = pFadeIn;
    }

    //--Executor
    virtual void Execute()
    {
        //--Begin the transition.
        TransitionManager::Fetch()->SetFadeSpeeds(mFadeOutSpeed, mFadeInSpeed);
        TransitionManager::Fetch()->BeginTransition(mPassExecutor, false);
    }
};
