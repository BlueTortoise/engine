//--[RootExecutor]
//--A class which contains an overridable logic function, essentially this is a class wrapper around
//  a piece of logic.  Examples of things that use Executors are Buttons or script events.  The
//  Executor should be setup when it is created, and stored as a RootExecutor exclusively.
//--This class is abstract, it must be inherited.

#pragma once

#include "Definitions.h"
#include "Structures.h"

class RootExecutor : public RootObject
{
    private:

    protected:

    public:
    //--System
    RootExecutor()
    {
        //--[RootObject]
        mType = POINTER_TYPE_EXECUTOR_ROOT;
    }
    virtual ~RootExecutor()
    {

    }
    virtual void Execute() = 0;
    virtual void ExecuteClass(void *pCaller)
    {
        //--Calls the Execute function with the pCaller set as an owner.  Not all classes use this,
        //  in which case Execute is called by default.
        //--Inherit and Override if needed.
        Execute();
    }
    static void DeleteThis(void *pPtr)
    {
        delete ((RootExecutor *)pPtr);
    }
};

