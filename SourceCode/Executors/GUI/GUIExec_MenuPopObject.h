//--[GUIExec_MenuPopObject]
//--Pops the top of the menu stack off.  Couldn't be simpler.  If a string was provided, removes the
//  requested object (which may not be at the top).
//--That second part is not implemented yet.

#pragma once

#include "Definitions.h"
#include "Structures.h"
#include "RootExecutor.h"
#include "GUI.h"
#include "GUIBaseObject.h"

class GUIExec_MenuPopObject : public RootExecutor
{
    private:
    char *mObjectName;

    protected:

    public:
    //--System
    GUIExec_MenuPopObject()
    {
        mObjectName = NULL;
    }
    virtual ~GUIExec_MenuPopObject()
    {
        free(mObjectName);
        mObjectName = NULL;
    }

    //--Manipulators
    void SetName(const char *pName)
    {
        ResetString(mObjectName, pName);
    }

    //--Executor
    virtual void Execute()
    {
        GraphicalUserInterface::Fetch()->PopMenuStack();
    }
};


