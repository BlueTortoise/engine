//--[GUIExec_HideEverything]
//--Hides everything on the UI.

#pragma once

#include "Definitions.h"
#include "Structures.h"
#include "RootExecutor.h"

#include "GUI.h"

class GUIExec_HideEverything : public RootExecutor
{
    private:

    protected:

    public:
    //--System
    GUIExec_HideEverything()
    {

    }
    virtual ~GUIExec_HideEverything()
    {

    }
    virtual void Execute()
    {
        GraphicalUserInterface::Fetch()->HideEverything();
    }
};
