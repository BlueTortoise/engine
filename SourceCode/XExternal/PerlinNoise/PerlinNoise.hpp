//----------------------------------------------------------------------------------------
//
//	siv::PerlinNoise
//	Perlin noise library for modern C++
//
//	Copyright (C) 2013-2018 Ryo Suzuki <reputeless@gmail.com>
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files(the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions :
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//
//----------------------------------------------------------------------------------------

# pragma once
//# include <cstdint>
# include <numeric>
# include <algorithm>
//# include <random>

namespace siv
{
	class PerlinNoise
	{
	private:

		uint8_t p[512];

		static double Fade(double t)
		{
			return t * t * t * (t * (t * 6 - 15) + 10);
		}

		static double Lerp(double t, double a, double b)
		{
			return a + t * (b - a);
		}

		static double Grad(uint8_t hash, double x, double y, double z)
		{
			const uint8_t h = hash & 15;
			const double u = h < 8 ? x : y;
			const double v = h < 4 ? y : h == 12 || h == 14 ? x : z;
			return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
		}

	public:

		explicit PerlinNoise(uint32_t seed)
		{
			reseed(seed);
		}

		template <class URNG>
		explicit PerlinNoise(URNG& urng)
		{
			reseed(urng);
		}

		void reseed(uint32_t seed)
		{
		    //--Baseline.
			for(int i = 0; i < 256; ++i)
			{
				p[i] = i;
			}

			//--Pseudorandom randomization. Generate 256 random numbers using the seed value.
			int tScatterVals[256];
			for(int i = 0; i < 256; i ++)
            {
                tScatterVals[i] = (seed * 1103515245 + 12345) & RAND_MAX;
                seed = tScatterVals[i];
            }

            //--Randomly shuffle the first 256 elements of p.
            for(int i = 0; i < 256; i ++)
            {
                //--Roll a random slot to swap with.
                int tRandomSlot = tScatterVals[i] % 256;
                if(tRandomSlot == i) continue;

                //--Swap 'em.
                uint8_t tTemp = p[i];
                p[i] = p[tRandomSlot];
                p[tRandomSlot] = tTemp;
            }

            //--Elements of p past 256 are just duplicates.
			for(int i = 0; i < 256; ++i)
			{
				p[256 + i] = p[i];
			}
		}

		double noise(double x) const
		{
			return noise(x, 0.0, 0.0);
		}

		double noise(double x, double y) const
		{
			return noise(x, y, 0.0);
		}

		double noise(double x, double y, double z) const
		{
			const int32_t X = static_cast<int32_t>(floor(x)) & 255;
			const int32_t Y = static_cast<int32_t>(floor(y)) & 255;
			const int32_t Z = static_cast<int32_t>(floor(z)) & 255;

			x -= floor(x);
			y -= floor(y);
			z -= floor(z);

			const double u = Fade(x);
			const double v = Fade(y);
			const double w = Fade(z);

			const int32_t A = p[X] + Y, AA = p[A] + Z, AB = p[A + 1] + Z;
			const int32_t B = p[X + 1] + Y, BA = p[B] + Z, BB = p[B + 1] + Z;

			return Lerp(w, Lerp(v, Lerp(u, Grad(p[AA], x, y, z),
				Grad(p[BA], x - 1, y, z)),
				Lerp(u, Grad(p[AB], x, y - 1, z),
				Grad(p[BB], x - 1, y - 1, z))),
				Lerp(v, Lerp(u, Grad(p[AA + 1], x, y, z - 1),
				Grad(p[BA + 1], x - 1, y, z - 1)),
				Lerp(u, Grad(p[AB + 1], x, y - 1, z - 1),
				Grad(p[BB + 1], x - 1, y - 1, z - 1))));
		}

		double octaveNoise(double x, int32_t octaves) const
		{
			double result = 0.0;
			double amp = 1.0;

			for (int32_t i = 0; i < octaves; ++i)
			{
				result += noise(x) * amp;
				x *= 2.0;
				amp *= 0.5;
			}

			return result;
		}

		double octaveNoise(double x, double y, int32_t octaves) const
		{
			double result = 0.0;
			double amp = 1.0;

			for (int32_t i = 0; i < octaves; ++i)
			{
				result += noise(x, y) * amp;
				x *= 2.0;
				y *= 2.0;
				amp *= 0.5;
			}

			return result;
		}

		double octaveNoise(double x, double y, double z, int32_t octaves) const
		{
			double result = 0.0;
			double amp = 1.0;

			for (int32_t i = 0; i < octaves; ++i)
			{
				result += noise(x, y, z) * amp;
				x *= 2.0;
				y *= 2.0;
				z *= 2.0;
				amp *= 0.5;
			}

			return result;
		}

		double noise0_1(double x) const
		{
			return noise(x) * 0.5 + 0.5;
		}

		double noise0_1(double x, double y) const
		{
			return noise(x, y) * 0.5 + 0.5;
		}

		double noise0_1(double x, double y, double z) const
		{
			return noise(x, y, z) * 0.5 + 0.5;
		}

		double octaveNoise0_1(double x, int32_t octaves) const
		{
			return octaveNoise(x, octaves) * 0.5 + 0.5;
		}

		double octaveNoise0_1(double x, double y, int32_t octaves) const
		{
			return octaveNoise(x, y, octaves) * 0.5 + 0.5;
		}

		double octaveNoise0_1(double x, double y, double z, int32_t octaves) const
		{
			return octaveNoise(x, y, z, octaves) * 0.5 + 0.5;
		}
	};
}
