include(../plugin.pri)

DEFINES += SLF_LIBRARY

SOURCES += slfplugin.cpp \
    SugarLumpFile.cpp \
    RootLump.cpp \
    imagelump.cpp \
    layerlump.cpp \
    mapinfolump.cpp
HEADERS += slfplugin.h\
    slf_global.h \
    SugarLumpFile.h \
    RootLump.h \
    imagelump.h \
    layerlump.h \
    mapinfolump.h
