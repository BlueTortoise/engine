#include "SugarLumpFile.h"
#include "RootLump.h"
#include <QList>
#include <stdio.h>
#include <string.h>


//=========================================== System ==============================================
SugarLumpFile::SugarLumpFile()
{
    //--[SugarLumpFile]
    //--System
    mHeader = (char *)malloc(sizeof(char) * (strlen("SugarLumpFile100") + 1));
    strcpy(mHeader, "SugarLumpFile100");
    mHeader[13] = ((SugarLumpFile::cFileVersion / 100) % 10) + '0';
    mHeader[14] = ((SugarLumpFile::cFileVersion /  10) % 10) + '0';
    mHeader[15] = ((SugarLumpFile::cFileVersion /   1) % 10) + '0';

    //--Chunks
    mLumpsTotal = 0;
    mLumpsList = NULL;

    //--Public Variables
    mWritePath = NULL;
}
SugarLumpFile::~SugarLumpFile()
{
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void SugarLumpFile::RegisterLump(const char *pName, RootLump *pLump)
{
    //--Registers the Lump to the file.  Lumps do not contain internal names, their name is whatever
    //  they are on the SLF's list.
    if(!pName || !pLump) return;
    pLump->mName = (char *)malloc(sizeof(char) * (strlen(pName) + 1));
    strcpy(pLump->mName, pName);

    //--Add space for the lump list.
    mLumpsTotal ++;
    mLumpsList = (RootLump **)realloc(mLumpsList, sizeof(RootLump *) * mLumpsTotal);

    //--Store the lump.
    mLumpsList[mLumpsTotal-1] = pLump;
    fprintf(stdout, "Registered lump %s.\n", pName);
}

//========================================= Core Methods ==========================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
void QListQuickSort(RootLump **pList, int pLft, int pRgt)
{
    //--Runs a quicksort on the provided QList assuming it uses RootLump objects.
    int i = pLft, j = pRgt;
    RootLump *rTemp = NULL;
    RootLump *rPivot = pList[(pLft + pRgt) / 2];

    //--Partition the list.
    while (i <= j)
    {
        //--Locate the I pivot.
        RootLump *rLumpI = pList[i];
        while(strcmp(rLumpI->mName, rPivot->mName) < 0)
        {
            i++;
            rLumpI = pList[i];
        }

        //--Locate the J pivot.
        RootLump *rLumpJ = pList[j];
        while(strcmp(rLumpJ->mName, rPivot->mName) > 0)
        {
            j--;
            rLumpJ = pList[j];
        }

        //--Swap the elements.
        if (i <= j)
        {
            rTemp = pList[i];
            pList[i] = pList[j];
            pList[j] = rTemp;
            i++;
            j--;
        }
    }

    //--Split the list and redo the algorithm.
    if (pLft < j)
    {
        QListQuickSort(pList, pLft, j);
    }
    if (i < pRgt)
    {
        QListQuickSort(pList, i, pRgt);
    }
}
void SugarLumpFile::Write(const char *pPath)
{
    //--Write the SugarLumpFile to the provided path.  Most of the hard logic is done by the Lumps
    //  themselves, in overridden functions.
    //--Note:  Overwrite is ON!
    fprintf(stdout, "Writing SLF with %i lumps.\n", mLumpsTotal);
    if(!pPath) return;

    //--Open and check for write protection
    FILE *fOutfile = fopen(pPath, "wb");
    if(!fOutfile)
    {
        fprintf(stdout, "SugarLumpFile - Error opening %s for writing\n", pPath);
        return;
    }

    //--Write the header to the file.
    uint64_t tFileCursor = 0;
    fwrite(mHeader, sizeof(char), 16, fOutfile);
    tFileCursor += (sizeof(char) * 16);

    //--Write total number of Lumps.
    uint32_t tTotalChunks = mLumpsTotal;
    fwrite(&tTotalChunks, sizeof(uint32_t), 1, fOutfile);
    tFileCursor += (sizeof(uint32_t) * 1);
    fprintf(stdout, "Wrote header and lumps.\n");

    //--Sort the Lumps by name.
    QListQuickSort(mLumpsList, 0, mLumpsTotal - 1);

    //--Write the name of each Lump, and leave space for it's 64-bit address in the file.
    uint64_t tDummy = 0;
    for(int i = 0; i < mLumpsTotal; i ++)
    {
        //--Get the lump.
        RootLump *rLump = (RootLump *)mLumpsList[i];

        //--Get the name
        const char *rName = rLump->mName;
        uint8_t tLen = strlen(rName);

        //--Write it!
        fwrite(&tLen, sizeof(uint8_t), 1, fOutfile);
        fwrite(rName, sizeof(char), tLen, fOutfile);
        tFileCursor += (sizeof(uint8_t) * 1) + (sizeof(char) * tLen);

        //--Write a blank 64-bit integer, and tell the Lump where it is.
        rLump->SetAddressOfAddress(tFileCursor);
        fwrite(&tDummy, sizeof(uint64_t), 1, fOutfile);
        tFileCursor += (sizeof(uint64_t) * 1);
    }

    //--Write the Lumps to the file.
    for(int i = 0; i < mLumpsTotal; i ++)
    {
        //--Get the lump.
        RootLump *rLump = (RootLump *)mLumpsList[i];

        //--Lump has an overridden write function for this.
        rLump->WriteTo(tFileCursor, fOutfile);
    }

    //--Go back and write the addresses in the header.
    for(int i = 0; i < mLumpsTotal; i ++)
    {
        //--Get the lump.
        RootLump *rLump = (RootLump *)mLumpsList[i];

        //--Lump stores the address of its... address... internally.
        rLump->WriteAddress(fOutfile);
    }

    //--Close and cleanup.
    fclose(fOutfile);
}

