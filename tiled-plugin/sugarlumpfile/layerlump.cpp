//--Base
#include "layerlump.h"

//--System
#include <stdio.h>
#include <stdint.h>
#include <QString>
#include <QVariantMap>

//--Classes
#include "layer.h"
#include "tilelayer.h"
#include "tile.h"
#include "gidmapper.h"
#include "mapinfolump.h"
#include "tileset.h"
#include "objectgroup.h"
#include "properties.h"
#include "qmap.h"
#include "mapobject.h"
#include "imagelayer.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

//--System
LayerLump::LayerLump()
{
    //--[LayerLump]
    //--System
    //--Type
    strcpy(mLayerType, "No Type");

    //--Common Data
    strcpy(mLayerName, "Unnamed");
    mPropertiesTotal = 0;
    mProperties = NULL;

    //--Tile Data
    mX = 0;
    mY = 0;
    mW = 0;
    mH = 0;
    mTileData = NULL;
    mFlipData = NULL;

    //--Image Data
    mImageLumpName = NULL;
}
LayerLump::~LayerLump()
{
    free(mImageLumpName);
}

//--Property Queries
char *LayerLump::GetName()
{
    return mLayerName;
}
char *LayerLump::GetImagePath()
{
    return mImageLumpName;
}

//--Manipulators
//--Core Methods
int LayerLump::GetUniversalID(Tiled::TileLayer *pLayer, int pX, int pY)
{
    //--Returns an integer that represents the universal tile lookup according to the MapInfoLump's
    //  storage of tileset data. -1 IS LEGAL, it represents "No Tile" and is transparent.
    if(!pLayer || pX < 0 || pY < 0) return -1;
    if(pX >= pLayer->width() || pY >= pLayer->height()) return -1;

    //--Get the cell which has the tile in it.
    Tiled::Cell cCellData = pLayer->cellAt(pX, pY);
    if(cCellData.tile() == NULL) return -1;

    //--Request the tile's tileset and ID.
    int tRelativeID = cCellData.tile()->id();
    Tiled::Tileset *rTileset = cCellData.tile()->tileset();

    //--Search the MapInfoLump's tileset data and add that to the relative ID.
    int tFinalID = tRelativeID;
    for(int i = 0; i < MapInfoLump::xTilesetsTotal; i ++)
    {
        QByteArray tData = rTileset->name().toLocal8Bit();
        const char *rTilesetName = tData.data();
        if(!strcmp(MapInfoLump::xTilesetPacks[i].mTilesetName, rTilesetName))
        {
            tFinalID += MapInfoLump::xTilesetPacks[i].mTilesToThisPoint;
            break;
        }
    }

    return tFinalID;
}
int LayerLump::GetFlippingID(Tiled::TileLayer *pLayer, int pX, int pY)
{
    //--Given a tile layer and position, returns an integer which represents how the tile is flipped.
    //  The Sugar^3 standard is the 0x01 flag is horizontal, and the 0x02 flag is vertical.
    if(!pLayer || pX < 0 || pY < 0) return 0;
    if(pX >= pLayer->width() || pY >= pLayer->height()) return 0;

    //--Get the cell which has the tile in it.
    Tiled::Cell cCellData = pLayer->cellAt(pX, pY);
    if(cCellData.tile() == NULL) return 0;

    //--Storage
    int tReturn = 0;

    //--Simple flips.
    if(cCellData.flippedHorizontally()) tReturn = tReturn | 0x01;
    if(cCellData.flippedVertically()) tReturn = tReturn | 0x02;
    if(cCellData.flippedAntiDiagonally()) tReturn = tReturn | 0x04;

    //--All done, pass it back.
    return tReturn;
}

char ***LayerLump::PopulateProperties(int &sPropertiesTotal, QVariantMap pProperties)
{
    //--Given a set of properties, returns a declared list of key/value pairs representing them.
    //  Modifies sPropertiesTotal to report the max number of properties.
    sPropertiesTotal = pProperties.count();
    if(sPropertiesTotal < 1) return NULL;

    //--Storage
    QList<QString> tKeyList = pProperties.keys();
    QList<QVariant> tValList = pProperties.values();

    char ***nProperties = (char ***)malloc(sizeof(char **) * sPropertiesTotal);
    for(int i = 0; i < sPropertiesTotal; i ++)
    {
        //--Properties are always two strings.
        nProperties[i] = (char **)malloc(sizeof(char *) * 2);

        //--Translate.
        QByteArray tDataKey = tKeyList[i].toLocal8Bit();
        const char *rKey = tDataKey.data();
        QByteArray tDataVal = tValList[i].toByteArray();
        const char *rVal = tDataVal.data();

        //--Key string.
        nProperties[i][0] = (char *)malloc(sizeof(char) * (strlen(rKey) + 1));
        strcpy(nProperties[i][0], rKey);
        fprintf(stderr, "   Property key is %s\n", rKey);

        //--Value string.
        nProperties[i][1] = (char *)malloc(sizeof(char) * (strlen(rVal) + 1));
        strcpy(nProperties[i][1], rVal);
        fprintf(stderr, "   Property value is %s\n", rVal);
    }

    //--Done.
    return nProperties;
}

void LayerLump::PopulateTileData(Tiled::TileLayer *pLayer)
{
    //--Stores the data that the lump will need as local copies. These are not scrapped if the
    //  layer type is changed, but will not be written.
    if(!pLayer) return;

    //--Switch the layer type.
    strcpy(mLayerType, "Tile");

    //--Common Data.
    QByteArray tNameData = pLayer->name().toLocal8Bit();
    const char *rNameKey = tNameData.data();
    strcpy(mLayerName, rNameKey);

    //--X/Y/W/D
    mX = pLayer->x();
    mY = pLayer->y();
    mW = pLayer->width();
    mH = pLayer->height();

    //--Properties.
    mProperties = PopulateProperties(mPropertiesTotal, pLayer->properties());

    //--Tile data.
    mTileData = (int16_t **)malloc(sizeof(int16_t *) * mW);
    for(int x = 0; x < mW; x ++)
    {
        mTileData[x] = (int16_t *)malloc(sizeof(int16_t) * mH);
        for(int y = 0; y < mH; y ++)
        {
            mTileData[x][y] = GetUniversalID(pLayer, x, y);
        }
    }

    //--Flip data.
    mFlipData = (int16_t **)malloc(sizeof(int16_t *) * mW);
    for(int x = 0; x < mW; x ++)
    {
        mFlipData[x] = (int16_t *)malloc(sizeof(int16_t) * mH);
        for(int y = 0; y < mH; y ++)
        {
            mFlipData[x][y] = GetFlippingID(pLayer, x, y);
        }
    }
}
void LayerLump::PopulateObjectData(Tiled::ObjectGroup *pObjectLayer)
{
    //--Populates object data based on the passed in layer.
    if(!pObjectLayer) return;

    //--Switch the layer type.
    strcpy(mLayerType, "Object");

    //--Common Data.
    QByteArray tNameData = pObjectLayer->name().toLocal8Bit();
    const char *rNameKey = tNameData.data();
    strcpy(mLayerName, rNameKey);

    //--Properties.
    mProperties = PopulateProperties(mPropertiesTotal, pObjectLayer->properties());

    //--Actual object data.
    QList<Tiled::MapObject *> rObjectList = pObjectLayer->objects();
    mObjectsTotal = rObjectList.count();
    mObjectData = (LocalObjectData *)malloc(sizeof(LocalObjectData) * mObjectsTotal);

    for(int i = 0; i < mObjectsTotal; i ++)
    {
        //--Map object.
        Tiled::MapObject *rObject = rObjectList[i];

        //--Name.
        QByteArray tDataKey = rObject->name().toLocal8Bit();
        const char *rKey = tDataKey.data();
        mObjectData[i].mName = (char *)malloc(sizeof(char) * (strlen(rKey) + 1));
        strcpy(mObjectData[i].mName, rKey);

        //--Name and Type.
        tDataKey = rObject->type().toLocal8Bit();
        rKey = tDataKey.data();
        mObjectData[i].mType = (char *)malloc(sizeof(char) * (strlen(rKey) + 1));
        strcpy(mObjectData[i].mType, rKey);

        //--Sizing.
        mObjectData[i].mX = rObject->x();
        mObjectData[i].mY = rObject->y();
        mObjectData[i].mW = rObject->width();
        mObjectData[i].mH = rObject->height();

        //--Tile ID. May be -1.
        mObjectData[i].mTileID = -1;
        Tiled::Cell cCellData = rObject->cell();

        //--Not all objects have a tile associated.
        if(cCellData.tile() == NULL)
        {
            mObjectData[i].mTileID = -1;
        }
        //--Only use the relative ID.
        else
        {
            //--Base ID.
            mObjectData[i].mTileID = cCellData.tile()->id();

            //--Extra: If the tileset is Pastables C2, then add 90 to the ID.
            Tiled::Tileset *rTileset = cCellData.tile()->tileset();
            QByteArray tData = rTileset->name().toLocal8Bit();
            const char *rTilesetName = tData.data();

            if(!strcmp(rTilesetName, "Pastables C2"))
            {
                mObjectData[i].mTileID += 100;
            }
        }


        //--Properties
        mObjectData[i].mProperties = PopulateProperties(mObjectData[i].mPropertiesTotal, rObject->properties());
    }
}
void LayerLump::PopulateImageData(Tiled::ImageLayer *pImageLayer)
{
    //--Takes in an ImageLayer and stores the local name of its image. Not a lot else to it.
    if(!pImageLayer) return;

    //--Switch the layer type.
    strcpy(mLayerType, "Image");

    //--Common Data.
    QByteArray tNameData = pImageLayer->name().toLocal8Bit();
    const char *rNameKey = tNameData.data();
    strcpy(mLayerName, rNameKey);

    //--Properties.
    mProperties = PopulateProperties(mPropertiesTotal, pImageLayer->properties());

    //--Basic name.
    QByteArray tData = pImageLayer->imageSource().toEncoded();
    const char *rKey = tData.data();
    fprintf(stdout, "=> Encoded %s\n", rKey);


    mImageLumpName = (char *)malloc(sizeof(char) * (strlen(rKey) + 1));
    strcpy(mImageLumpName, rKey);

    //--X/Y
    mX = pImageLayer->x();
    mY = pImageLayer->y();
}

//--Update
//--File I/O
void LayerLump::WriteData(uint64_t &sCounter, FILE *pOutfile)
{
    //--Overloaded member. Write the local data to the file.
    if(!pOutfile) return;

    //--Write the Lump's type as a 10-byte string.
    const char tString[11] = "LAYERDATA1";
    fwrite(tString, sizeof(char), 10, pOutfile);
    sCounter += (sizeof(char) * 10);

    //--Write the layer's type.
    uint8_t tLen = (uint8_t)strlen(mLayerType);
    fwrite(&tLen, sizeof(uint8_t), 1, pOutfile);
    fwrite(mLayerType, sizeof(char), tLen, pOutfile);
    sCounter += (sizeof(uint8_t) * 1) + (sizeof(char) * tLen);

    //--Write the layer's name.
    tLen = (uint8_t)strlen(mLayerName);
    fwrite(&tLen, sizeof(uint8_t), 1, pOutfile);
    fwrite(mLayerName, sizeof(char), tLen, pOutfile);
    sCounter += (sizeof(uint8_t) * 1) + (sizeof(char) * tLen);

    //--Properties.
    WriteProperties(mPropertiesTotal, mProperties, sCounter, pOutfile);

    //--Tile layers write tile data.
    if(!strcmp(mLayerType, "Tile"))
    {
        WriteTileData(sCounter, pOutfile);
    }
    //--Object layers write object data.
    else if(!strcmp(mLayerType, "Object"))
    {
        WriteObjectData(sCounter, pOutfile);
    }
    //--Image layers only store a pathname.
    else if(!strcmp(mLayerType, "Image"))
    {
        WriteImageData(sCounter, pOutfile);
    }
}
void LayerLump::WriteProperties(int pPropertiesTotal, char ***pProperties, uint64_t &sCounter, FILE *pOutfile)
{
    //--Writes a set of properties to the hard drive.
    uint8_t tLen;
    fwrite(&pPropertiesTotal, sizeof(int16_t), 1, pOutfile);
    sCounter += (sizeof(int16_t) * 1);
    for(int i = 0; i < pPropertiesTotal; i ++)
    {
        //--Key string.
        tLen = (uint8_t)strlen(pProperties[i][0]);
        fwrite(&tLen, sizeof(uint8_t), 1, pOutfile);
        fwrite(pProperties[i][0], sizeof(char), tLen, pOutfile);
        sCounter += (sizeof(uint8_t) * 1) + (sizeof(char) * tLen);

        //--Value string.
        tLen = (uint8_t)strlen(pProperties[i][1]);
        fwrite(&tLen, sizeof(uint8_t), 1, pOutfile);
        fwrite(pProperties[i][1], sizeof(char), tLen, pOutfile);
        sCounter += (sizeof(uint8_t) * 1) + (sizeof(char) * tLen);
    }
}

void LayerLump::WriteTileData(uint64_t &sCounter, FILE *pOutfile)
{
    //--Writes the lump as if it's a TileLayer type.
    fwrite(&mX, sizeof(int16_t), 1, pOutfile);
    fwrite(&mY, sizeof(int16_t), 1, pOutfile);
    fwrite(&mW, sizeof(int16_t), 1, pOutfile);
    fwrite(&mH, sizeof(int16_t), 1, pOutfile);
    sCounter += (sizeof(int16_t) * 4);

    //--Tile data.
    for(int x = 0; x < mW; x ++)
    {
        for(int y = 0; y < mH; y ++)
        {
            fwrite(&mTileData[x][y], sizeof(int16_t), 1, pOutfile);
            sCounter += (sizeof(int16_t) * 1);
        }
    }

    //--Flip data.
    for(int x = 0; x < mW; x ++)
    {
        for(int y = 0; y < mH; y ++)
        {
            fwrite(&mFlipData[x][y], sizeof(int16_t), 1, pOutfile);
            sCounter += (sizeof(int16_t) * 1);
        }
    }
}
void LayerLump::WriteObjectData(uint64_t &sCounter, FILE *pOutfile)
{
    //--Total number of objects.
    fwrite(&mObjectsTotal, sizeof(int16_t), 1, pOutfile);
    sCounter += (sizeof(int16_t) * 1);

    //--Write the tile data structures.
    uint8_t tLen;
    for(int i = 0; i < mObjectsTotal; i ++)
    {
        //--Data.
        LocalObjectData *rData = &mObjectData[i];

        //--Name.
        tLen = (uint8_t)strlen(rData->mName);
        fwrite(&tLen, sizeof(uint8_t), 1, pOutfile);
        fwrite(rData->mName, sizeof(char), tLen, pOutfile);
        sCounter += (sizeof(uint8_t) * 1) + (sizeof(char) * tLen);

        //--Type.
        tLen = (uint8_t)strlen(rData->mType);
        fwrite(&tLen, sizeof(uint8_t), 1, pOutfile);
        fwrite(rData->mType, sizeof(char), tLen, pOutfile);
        sCounter += (sizeof(uint8_t) * 1) + (sizeof(char) * tLen);

        //--Simple parts.
        fwrite(&rData->mX, sizeof(float), 1, pOutfile);
        fwrite(&rData->mY, sizeof(float), 1, pOutfile);
        fwrite(&rData->mW, sizeof(float), 1, pOutfile);
        fwrite(&rData->mH, sizeof(float), 1, pOutfile);
        sCounter += (sizeof(float) * 4);

        //--Tile ID. This is local to the enemies sheet, it is NOT a global ID number!
        fwrite(&rData->mTileID, sizeof(int16_t), 1, pOutfile);
        sCounter += (sizeof(int16_t) * 1);

        //--Properties
        WriteProperties(rData->mPropertiesTotal, rData->mProperties, sCounter, pOutfile);
    }
}
void LayerLump::WriteImageData(uint64_t &sCounter, FILE *pOutfile)
{
    //--All the image data is actually in a different lump. This one only needs X/Y and properties!
    fwrite(&mX, sizeof(float), 1, pOutfile);
    fwrite(&mY, sizeof(float), 1, pOutfile);
    sCounter += (sizeof(float) * 2);
}

//--Drawing
//--Pointer Routing
//--Static Functions
//--Lua Hooking
