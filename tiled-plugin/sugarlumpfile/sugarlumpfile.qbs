import qbs 1.0

TiledPlugin {
    cpp.defines: base.concat(["SLF_LIBRARY"])

    files: [
        "RootLump.cpp",
        "RootLump.h",
        "SugarLumpFile.cpp",
        "SugarLumpFile.h",
        "imagelump.cpp",
        "imagelump.h",
        "layerlump.cpp",
        "layerlump.h",
        "mapinfolump.cpp",
        "mapinfolump.h",
        "plugin.json",
        "slf_global.h",
        "slfplugin.cpp",
        "slfplugin.h",
    ]
}
