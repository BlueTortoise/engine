//--[SLF Exporter Plugin]
//--SaltyJustice, 2020. Written for Starlight Engine integration.
//  Contact: saltyjustice@starlightstudios.org
//  Redistribution follows CC 3.0.
//--This is largely modified from the format of the lua plugin.

#include "slfplugin.h"

#include "gidmapper.h"
#include "grouplayer.h"
#include "imagelayer.h"
#include "map.h"
#include "mapobject.h"
#include "objectgroup.h"
#include "objecttemplate.h"
#include "properties.h"
#include "savefile.h"
#include "terrain.h"
#include "tile.h"
#include "tilelayer.h"
#include "tileset.h"

#include <QCoreApplication>
#include <QDir>
#include <QFile>

#include "SugarLumpFile.h"
#include "RootLump.h"
#include "mapinfolump.h"
#include "imagelump.h"
#include "layerlump.h"

/**
 * See below for an explanation of the different formats. One of these needs
 * to be defined.
 */
#define POLYGON_FORMAT_FULL
//#define POLYGON_FORMAT_PAIRS
//#define POLYGON_FORMAT_OPTIMAL

using namespace Tiled;

namespace Slf {

void SlfPlugin::initialize()
{
    addObject(new SlfMapFormat(this));
}

bool SlfMapFormat::write(const Map *map,
                         const QString &fileName,
                         Options options)
{
    //--Orders Tiled to save the file before attempting to export it.
    SaveFile file(fileName);

    //--Make sure the file can be opened for writing.
    /*if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        mError = QCoreApplication::translate("File Errors", "Could not open file for writing.");
        return false;
    }*/

    //--Actually write the file here.
    mMapDir = QFileInfo(fileName).path();
    ExportTo(fileName.toStdString().c_str(), map);

    /*
    //--Check if the file actually got written to.
    if (file.error() != QFileDevice::NoError) {
        mError = file.errorString();
        return false;
    }
    if (!file.commit()) {
        mError = file.errorString();
        return false;
    }*/

    //--All checks passed.
    return true;
}

//-------------------------------------- Property Queries -----------------------------------------
QString SlfMapFormat::nameFilter() const
{
    return tr("SLF Binary Files (*.slf)");
}

QString SlfMapFormat::shortName() const
{
    return QLatin1String("slf");
}

QString SlfMapFormat::errorString() const
{
    return mError;
}
//------------------------------------------ File I/O ---------------------------------------------
void SlfMapFormat::ExportTo(const char *pPath, const Tiled::Map *pMap)
{
    //--[Debug]
    fprintf(stdout, "Writing file to path: %s\n", pPath);

    //--[Setup]
    //--SugarLumpFile creation.
    SugarLumpFile *fFile = new SugarLumpFile();

    //--MapInfoLump. Always named the same thing for brevity.
    fprintf(stdout, "Assembling map info lump - \n");
    MapInfoLump *nMapInfoLump = new MapInfoLump();
    nMapInfoLump->BootTilesetData(pMap);
    fFile->RegisterLump("AMapInfo", nMapInfoLump);
    fprintf(stdout, "done\n");

    //--Image lumps for all unique tilesets.
    char tTilesetName[64];
    for(int i = 0; i < MapInfoLump::xTilesetsTotal; i ++)
    {
        //--Get the path.
        const char *rPath = MapInfoLump::xTilesetPacks[i].mTilesetPath;

        //--Create a lump around it.
        ImageLump *nImageLump = new ImageLump();
        nImageLump->RipFromFile(rPath);

        //--Register.
        sprintf_s(tTilesetName, "Tileset|%s", MapInfoLump::xTilesetPacks[i].mTilesetName);
        fFile->RegisterLump(tTilesetName, nImageLump);
    }

    //--Create a layer lump for each layer on the map.
    int tWrittenLayerCount = 0;
    char tLayerName[32];
    fprintf(stderr, "Assembling layer lumps - \n");
    for(int i = 0; i < pMap->layerCount(); i ++)
    {
        //--Retrieve Layer.
        Tiled::Layer *pLayer = pMap->layerAt(i);
        sprintf_s(tLayerName, "Layer %02i", tWrittenLayerCount);
        fprintf(stderr, " Layer Name: %s\n", tLayerName);

        //--Retrieve the true name.
        QByteArray tTrueNameData = pLayer->name().toLocal8Bit();
        const char *rTrueName = tTrueNameData.data();

        //--Skip if the name is "CAMERAPROTOTYPE".
        if(!strcmp(rTrueName, "CAMERAPROTOTYPE")) continue;

        //--If a given layer is not visible it will not be saved!
        if(!pLayer->isVisible()) continue;
        tWrittenLayerCount ++;

        //--Build a lump around it.
        LayerLump *nLump = new LayerLump();

        //--Tile Layer.
        if(pLayer->isTileLayer())
        {
            nLump->PopulateTileData((Tiled::TileLayer *)pLayer);
        }
        //--Object Layer
        if(pLayer->isObjectGroup())
        {
            nLump->PopulateObjectData((Tiled::ObjectGroup *)pLayer);
        }
        //--Image Layer
        if(pLayer->isImageLayer())
        {
            //--Image lump will store X/Y/Offset information.
            nLump->PopulateImageData((Tiled::ImageLayer *)pLayer);

            //--Path to the image the layer has.
            char *rPath = nLump->GetImagePath();

            //--Store an image lump with the same name.
            ImageLump *nImageLump = new ImageLump();
            nImageLump->RipFromFile(rPath);

            //--Register.
            char tBuffer[80];
            sprintf_s(tBuffer, "Image|%s", nLump->GetName());
            fFile->RegisterLump(tBuffer, nImageLump);
        }

        //--Register it.
        fFile->RegisterLump(tLayerName, nLump);
    }
    fprintf(stderr, "done\n");

    //--[Writing]
    //--All done. Write it.
    fprintf(stdout, "Writing to hard drive - \n");
    fFile->Write(pPath);
    delete fFile;
    fprintf(stdout, "done\n");

    //--[Debug]
    fprintf(stdout, "Finished writing slf file.\n", pPath);
}

}//Namespace Slf
