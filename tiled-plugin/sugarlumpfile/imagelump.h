//--[ImageLump]
//--Lump holding an image. Code is shared between Sugar^3 projects.

#ifndef _IMAGELUMP_H_
#define _IMAGELUMP_H_

#define _CRT_SECURE_NO_WARNINGS

#include "RootLump.h"

class ImageLump : public RootLump
{
    //--[Constants]
    public:
    static const uint8_t cInvalidCompression = 0;
    static const uint8_t cNoCompression = 1;
    static const uint8_t cPNGCompression = 2;
    static const uint8_t cDXT1Compression = 3;
    static const uint8_t cDXT1CompressionWithAlpha = 4;
    static const uint8_t cDXT3Compression = 5;
    static const uint8_t cDXT5Compression = 6;
    static const uint8_t cIsMonoColor = 7;
    static const uint8_t cSupportedCompressions = 8;
    static const int cGLLookups[4];

    private:
    //--System
    uint8_t mLocalCompression;

    //--Storage
    int mDataWidth;
    int mDataHeight;
    int mTrueWidth;
    int mTrueHeight;
    int mXOffset;
    int mYOffset;
    uint32_t mDataArraySize;
    uint8_t *mDataArray;

    public:
    //--System
    ImageLump();
    virtual ~ImageLump();

    //--Public Variables

    //--Property Queries
    //--Manipulators
    //--Core Methods
    void RipFromFile(const char *pPath);

    //--Update
    //--File I/O
    virtual void WriteData(uint64_t &sCounter, FILE *pOutfile);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

#endif // _LAYERLUMP_H_
