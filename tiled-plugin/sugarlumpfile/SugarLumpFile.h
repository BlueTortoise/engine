#pragma once

#define _CRT_SECURE_NO_WARNINGS
#include <QList>
#include <stdint.h>

class RootLump;

class SugarLumpFile
{
    private:
    //--System
    static const int cFileVersion = 100;
    char *mHeader;

    //--Chunks
    int mLumpsTotal;
    RootLump **mLumpsList;

    protected:

    public:
    //--System
    SugarLumpFile();
    ~SugarLumpFile();

    //--Public Variables
    char *mWritePath;

    //--Property Queries
    //--Manipulators
    void RegisterLump(const char *pName, RootLump *pLump);

    //--Core Methods
    //--Update
    //--File I/O
    void Write(const char *pPath);
};

