--[ ======================================== Shader Exec ======================================== ]
--At program startup, builds and registers all shaders. If you want to use custom shaders, this
-- is where you build them.

--Resolve if this is the OSX build. If so, change the path.
local sShaderPath = "Shaders/WinLin/"
if(LM_GetSystemOS() == "OSX") then sShaderPath = "Shaders/OSX/" end

--[ ========================================== Builder ========================================== ]
--Clear any existing shader data.
DM_ClearShaders()

--Build all the shaders.
DM_BuildShader("Lighting2D",               sShaderPath .. "Lighting2D.vert",               sShaderPath .. "Lighting2D.frag")
DM_BuildShader("ColorOutline",             sShaderPath .. "ColorOutline.vert",             sShaderPath .. "ColorOutline.frag")
DM_BuildShader("ColorInvert",              sShaderPath .. "ColorInvert.vert",              sShaderPath .. "ColorInvert.frag")
DM_BuildShader("PaletteSwap",              sShaderPath .. "PaletteSwap.vert",              sShaderPath .. "PaletteSwap.frag")
DM_BuildShader("RenderAtlas",              sShaderPath .. "RenderAtlas.vert",              sShaderPath .. "RenderAtlas.frag")
DM_BuildShader("RenderAtlasWhite",         sShaderPath .. "RenderAtlasWhite.vert",         sShaderPath .. "RenderAtlasWhite.frag")
DM_BuildShader("RenderAtlasLightmap",      sShaderPath .. "RenderAtlasLightmap.vert",      sShaderPath .. "RenderAtlasLightmap.frag")
DM_BuildShader("RenderAtlasLightmapWhite", sShaderPath .. "RenderAtlasLightmapWhite.vert", sShaderPath .. "RenderAtlasLightmapWhite.frag")
DM_BuildShader("RenderAtlasLightmapZero",  sShaderPath .. "RenderAtlasLightmapZero.vert",  sShaderPath .. "RenderAtlasLightmapZero.frag")
DM_BuildShader("Underwater",               sShaderPath .. "Underwater.vert",               sShaderPath .. "Underwater.frag")

--[ ========================================== Aliases ========================================== ]
--Set aliases for the shaders. Multiple aliases can fit the same shader, allowing re-use. These are
-- categorized by class.

--TilemapActor. Entities that appear in Adventure Mode's levels, including NPCs and the player.
DM_AddShaderAlias("ColorOutline", "TilemapActor ColorOutline")

--Pairanormal classes. The dialogue is the text box, the level is the background.
DM_AddShaderAlias("ColorInvert",  "PairanormalDialogue ColorInvert")
DM_AddShaderAlias("ColorInvert",  "PairanormalLevel ColorInvert")

--AdventureLevel, the map files used in Adventure Mode.
DM_AddShaderAlias("Underwater",   "AdventureLevel Underwater")
DM_AddShaderAlias("Lighting2D",   "AdventureLevel Lighting")

--WADFile. Map files used for Classic 3D.
DM_AddShaderAlias("RenderAtlas",              "WADFile RenderAtlas")
DM_AddShaderAlias("RenderAtlasWhite",         "WADFile RenderAtlasWhite")
DM_AddShaderAlias("RenderAtlasZero",          "WADFile RenderAtlasZero")
DM_AddShaderAlias("RenderAtlasLightmap",      "WADFile RenderAtlasLightmap")
DM_AddShaderAlias("RenderAtlasLightmapWhite", "WADFile RenderAtlasLightmapWhite")
DM_AddShaderAlias("RenderAtlasLightmapZero",  "WADFile RenderAtlasLightmapZero")

--SugarPalette, a built-in palette format.
DM_AddShaderAlias("PaletteSwap", "SugarPalette PaletteSwap")

--[ ========================================== Compile ========================================== ]
--Compile the shaders so the program can use them.
DM_CompileShaders()