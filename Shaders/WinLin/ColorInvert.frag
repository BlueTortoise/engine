//--[Color Inversion]
//--Shader used for Pairanormal Glitch cases. Inverts the colors. Looks creepy.
#version 120
#if __VERSION__ < 130
    #define TextureFunc texture2D
#else
    #define TextureFunc texture
#endif
uniform sampler2D xTexSampler;

void main(void)
{
    //--Retrieve the texel.
    vec4 cTexelColor = TextureFunc(xTexSampler, gl_TexCoord[0].st);
    gl_FragColor = vec4(1.0 - cTexelColor.r, 1.0 - cTexelColor.g, 1.0 - cTexelColor.b, cTexelColor.a);
}
