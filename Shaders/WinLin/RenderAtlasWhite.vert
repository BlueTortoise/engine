//--Fragment half of the rendering shader. Requires the UV clamps from the texture, and modifies the passed
//  in UV values to be within those clamps. This repeats along the edges.
void main(void)
{
    //--Provide the current texture coordinates for the fragment half of the shader;
    gl_FrontColor = gl_Color;
    gl_BackColor = gl_Color;
    gl_TexCoord[0] = gl_MultiTexCoord0;

    //--Standard transformation.
    gl_Position = ftransform();
}
