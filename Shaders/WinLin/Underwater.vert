//--Vertex half of the lighting shader.
void main(void)
{
    //--Fetch texturing information for both textures.
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_FrontColor = gl_Color;
    gl_BackColor = gl_Color;

    //--Standard transformation.
    gl_Position = ftransform();
}
