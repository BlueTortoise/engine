//--Fragment half of the lighting shader. Lighting information must be passed in, where each light
//  is a vertex (XYZ) position. For now assume they are all white and the same color.
uniform sampler2D uTexture;
varying vec3 vPosition; //Retrieved from the vertex shader.

void main (void)
{
    //--Get the original texture color.
    vec4 tColor = texture(uTexture, gl_TexCoord[0].st);

    //--Fixed lighting
    vec3 tLightPos = vec3(50.0, -1147.0, -156.0);
    //vec3 tLightPos = vec3(0, 0, 0);

    //--Invert the distance.
    //float tDistance = 1.0 - sqrt( pow(tLightPos.x - vPosition.x, 2) + pow(tLightPos.y - vPosition.y, 2) + pow(tLightPos.z - vPosition.z, 2)) * 0.0005;
    float tDistance = 1.0 - length(tLightPos - vPosition) * 0.0010;
    //float tDistance = length(tLightPos - vPosition) * 0.0001;

    gl_FragColor = vec4(tColor.r * tDistance, tColor.g * tDistance, tColor.b * tDistance, 1.0);
    //gl_FragColor = vec4(1.0 - tDistance, 1.0 - tDistance, 1.0 - tDistance, 1.0);


    //vec4 tOriginalColor = texture2D(uTexture, gl_TexCoord[0].st);
    //gl_FragColor = tOriginalColor;
    //gl_FragColor = vec4(gl_FragCoord.b, gl_FragCoord.b, gl_FragCoord.b, gl_FragCoord.b);
}
