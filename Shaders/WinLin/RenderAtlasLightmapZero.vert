//--Fragment half of the rendering shader. Requires the UV clamps from the texture, and modifies the passed
//  in UV values to be within those clamps. This repeats along the edges.
void main(void)
{
    //--Provide the current texture coordinates for the fragment half of the shader;
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_TexCoord[1] = gl_MultiTexCoord1;

    //--Standard transformation.
    gl_Position = ftransform();
}
