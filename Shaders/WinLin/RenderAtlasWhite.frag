//--Fragment half of the rendering shader. Requires the UV clamps from the texture, and modifies the passed
//  in UV values to be within those clamps. This repeats along the edges.
#version 120
uniform sampler2D xTexSampler;

//--These are the clamping variables. The x/y are the lft/rgt, and the z/w are the top/bot.
uniform int uTotalTextures;
uniform vec4 uClamps[128];

void main(void)
{
    //--The texture index will be passed in as the third value for the texture. Only the s/t position
    //  is used for rendering.
    float cLft, cTop, cRgt, cBot;
    int tIndex = int(gl_TexCoord[0].z);
    if(tIndex < 0 || tIndex >= uTotalTextures)
    {
        return;
    }
    else
    {
        cLft = uClamps[tIndex].x;
        cRgt = uClamps[tIndex].y;
        cTop = uClamps[tIndex].z;
        cBot = uClamps[tIndex].w;
    }

    //--Get the dif values.
    float cDifX = cRgt - cLft;
    float cDifY = cBot - cTop;

    //--Original location.
    vec2 tTexelLocation = gl_TexCoord[0].st;

    //--Both differences must be more than 0.0f.
    if(cDifX > 0.0 && cDifY > 0.0)
    {
        tTexelLocation.s = cLft + mod(tTexelLocation.s - cLft, cDifX);
        tTexelLocation.t = cTop + mod(tTexelLocation.t - cTop, cDifY);
    }

    //--Compute the fog behavior.
    float tFogFactor = (gl_FragCoord.w * 5.0);
    tFogFactor = clamp(tFogFactor, 0.0, 1.0);

    //--Retrieve the texel.
    vec4 tColor = texture2D(xTexSampler, tTexelLocation);
    //gl_FragColor = tColor * vec4(tFogFactor, tFogFactor, tFogFactor, 1.0);
    gl_FragColor = vec4(tFogFactor, tFogFactor, tFogFactor, 1.0);
}
