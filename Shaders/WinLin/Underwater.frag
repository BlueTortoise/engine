//--Underwater shader. Adds a shimmer effect.
#version 150 compatibility
uniform sampler2D xTexSampler;
uniform int uTimer;

//--Function.
void main (void)
{
    //--Resolve exact X/Y positions.
    float cPixelSizeX = 1.0 / 1366.0;
    float cPixelSizeY = 1.0 /  768.0;
    vec2 tCoordinates;
    tCoordinates.x = gl_TexCoord[0].x;
    tCoordinates.y = gl_TexCoord[0].y;

    //--Time, offsets the starting point.
    float cWavePeriod = 300.0;
    float cTimeOffset = float(uTimer % int(cWavePeriod)) / cWavePeriod;

    //--Offset the X position based on the Y position.
    float cAmplitude = 2.0 * cPixelSizeX;
    float cWaves = 1.0;
    float cYSin = sin((tCoordinates.y + cTimeOffset) * 3.1415926 * 2.0 * cWaves);
    tCoordinates.x = tCoordinates.x + (cYSin * cAmplitude);

    //--Offset the Y position based on the X position.
    //float cXSin = 1.0 - cos((gl_TexCoord[0].x + cTimeOffset) * 3.1415926 * 2.0 * cWaves);
    //tCoordinates.y = tCoordinates.y + (cXSin * cAmplitude);

    //--Get the base color from the resolved position.
    vec4 tColor = texture2D(xTexSampler, tCoordinates);

    //--Color it blue, too.
    tColor.r = tColor.r * 0.45;
    tColor.g = tColor.g * 0.45;
    tColor.b = tColor.b * 1.00;

    //--Upload the final color.
    gl_FragColor = tColor;
}
