============================================== Readme =============================================
Pandemonium Adventure v2.0

Primary website: https://hopremastered.wordpress.com
Contact email: Pandemonium AT starlightstudios DAWT org

--[[ Common System Questions ]]--
1) Where are the saves and my configuration stored?
   
   Your savefiles are stored in the Saves/ folder locally. Your control configurations are also saved
   there. Engine configurations are stored in Configuration.lua.

2) What are the different executables for?
 
   These are here for players using older hardware. Sometimes, Allegro or SDL are unable to acquire
   the screen due to driver issues and the game cannot start. If this happens, use one of the 
   other executables and hope that the issue is fixed. Most players report that one or the other
   executable works, and both are otherwise identical.
   The SDLFMOD executable uses FMOD instead of Bass for audio, and should only be used if the
   program's audio fails to boot.

3) What are the different .bat files for?

   Once again, these are for compatibility on older machines, but they only concern the drivers 
   for 3D mode. If you'd like to try out Classic 3D mode, the batch files attempt to inform the 
   engine of what sort of hardware acceleration you'd like to try out. This is because the 
   OpenGL drivers often fail to inform the engine of its version correctly and I have not had
   the time to go through and manually set the detectors.
   The batch files do nothing special if you're playing Adventure Mode.
   
4) I'm stuck at X, what do I do?

   The best place to get help is at our official forum thread: 
    https://www.tfgamessite.com/phpbb3/viewtopic.php?f=6&t=7476
   
   We also have a rough content guide:
    https://hopremastered.wordpress.com/adventure-content-guide/
   
   And #1 Fan/Overall Great Person Klaysee even wrote a game guide!
   https://drive.google.com/file/d/1if3SFz_H6wCka83NzGmOhNV_KYvsTmYG/view (Chapter 1)
   https://drive.google.com/file/d/1rNx4ttCSP-pRNlVuo_bUp2_xCJJY3XDw/view (Chapter 5)

--[[ Version History]]
Adventure v1.0 - Chapter 1 Completed
Adventure v2.0 - Chapter 5 Completed