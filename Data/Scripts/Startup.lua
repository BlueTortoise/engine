--[ ========================================== Startup ========================================== ]
--Script that executes during program startup.  Load all resources here.
Debug_PushPrint(false, "Executing Startup.lua\n")
gbIsOneGameMode = false

--[Debug Version Barker]
--io.write("Lua reports it is ",_VERSION,"!\n")

--Set version string.
Debug_SetVersion("Starlight Eng v2.1 Adv v5-5f StrTy v1.3 Cls v2.0 Pair v2.0")

--[Global Lua Functions and Variables]
Debug_Print("Executing System and Pathing scripts.\n")
LM_ExecuteScript("Data/Scripts/Functions/System.lua") --This one always goes first. Other functions may depend on it.
LM_ExecuteScript("Data/Scripts/Functions/Pathing.lua")
LM_ExecuteScript("Data/Scripts/Functions/Menu.lua")
LM_ExecuteScript("Data/Scripts/Functions/Loading.lua")
local sBasePath = fnResolvePath()

--[ ======================================= Path Listing ======================================== ]
--IMPORTANT: These path listings are used in some scenarios. For example, AdventureMode uses the
-- ElectrospriteAdventure path. So, don't edit them!
--There are several possible paths for each mode, to ease splitting them into smaller projects.
-- The "Active" path is just the first one found. By default, this is the "Games" folder version.

--The active path can also be found by using VM_GetVar(). A list of the paths follows. This list is
-- mirrored in DataLibrary.cc.
--"Root/Paths/System/Startup/sClassicModePath"
--"Root/Paths/System/Startup/sClassicMode3DPath"
--"Root/Paths/System/Startup/sCorrupterModePath"
--"Root/Paths/System/Startup/sAdventurePath"
--"Root/Paths/System/Startup/sDollManorPath"
--"Root/Paths/System/Startup/sElectrospriteAdventurePath"
--"Root/Paths/System/Startup/sMOTFPath"
--"Root/Paths/System/Startup/sLevelGeneratorPath"
--"Root/Paths/System/Startup/sPairanormalPath"

--[Path Listings for Starlight Games]
--Adventure Mode
fnAddGameEntry("Adventure Mode", "Root/Paths/System/Startup/sAdventurePath", "Play Adventure Mode", "120 LaunchAdventureMode.lua", -12)
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "Games/AdventureMode/ZLaunch.lua")
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "../adventure/Games/AdventureMode/ZLaunch.lua")
VM_SetVar("Root/Paths/System/Startup/sAdventurePath", "S", string.sub(gsaGameEntries[giGamesTotal].sActivePath, 1, -12))

--String Tyrant
fnAddGameEntry("Doll Manor", "Root/Paths/System/Startup/sDollManorPath", "Play String Tyrant", "121 LaunchTextAdventureMode.lua", -23)
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "Games/DollManor/000 Launcher Script.lua")
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "../dollmanor/Games/DollManor/000 Launcher Script.lua")
local iStringTyrantEntry = giGamesTotal

--String Tyrant Demo
fnAddGameEntry("Doll Manor Demo", "Root/Paths/System/Startup/sDollManorDemoPath", "Play String Tyrant Demo", "121 LaunchTextAdventureModeDemo.lua", -23)
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "Games/DollManorDemo/000 Launcher Script.lua")
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "../dollmanor/Games/DollManorDemo/000 Launcher Script.lua")
local iStringTyrantDemoEntry = giGamesTotal

--Monsters of the Forest
fnAddGameEntry("MOTF", "Root/Paths/System/Startup/sMOTFPath", "Play Monsters of the Forest", "123 LaunchMonstersOfTheForest.lua", -23)
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "Games/MonstersOfTheForest/000 Launcher Script.lua")
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "../monstersoftheforest/Games/MonstersOfTheForest/000 Launcher Script.lua")

--Pairanormal
fnAddGameEntry("Pairanormal", "Root/Paths/System/Startup/sPairanormalPath", "Play Pairanormal",  "125 LaunchPairanormal.lua", -12)
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "Games/Pairanormal/ZLaunch.lua")
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "../pairanormal/Games/Pairanormal/ZLaunch.lua")

--Electrosprite Text Adventure: Only appears if the player unlocked it in Adventure Mode.
local fUnlockElectrosprites = OM_GetOption("Unlock Electrosprites")
if(fUnlockElectrosprites == 1.0) then
    fnAddGameEntry("Electrosprite Adventure", "Root/Paths/System/Startup/sElectrospriteAdventurePath", "Play Electrosprite Adventure", "122 LaunchElectrospriteAdventure.lua", -23)
    fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
    fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "../Adventure/Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
else
    fnAddGameEntry("Electrosprite Adventure", "Root/Paths/System/Startup/sElectrospriteAdventurePath", "DO NOT ADD", "122 LaunchElectrospriteAdventure.lua", -23)
    fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
    fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "../Adventure/Games/ElectrospriteTextAdventure/000 Launcher Script.lua")
end

--[Path Listing for Classic Mode Games]
--Classic Mode
fnAddGameEntry("Classic Mode", "Root/Paths/System/Startup/sClassicModePath", "Play Classic Mode", "100 LaunchClassicMode.lua", -12)
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "Games/ClassicMode/ZLaunch.lua")
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "../classic/Games/ClassicMode/ZLaunch.lua")

--Classic Mode 3D
fnAddGameEntry("Classic Mode 3D", "Root/Paths/System/Startup/sClassicMode3DPath", "Play Classic Mode 3D", "101 LaunchClassic3DMode.lua", -12)
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "Games/ClassicMode3D/ZLaunch.lua")
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "../classic/Games/ClassicMode3D/ZLaunch.lua")

--Corrupter Mode
fnAddGameEntry("Corrupter Mode", "Root/Paths/System/Startup/sCorrupterModePath", "Play Corrupter Mode",  "110 LaunchCorrupterMode.lua", -12)
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "Games/CorrupterMode/ZLaunch.lua")
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "../classic/Games/CorrupterMode/ZLaunch.lua")

--[Tools]
--Level Generator
fnAddGameEntry("Level Generator", "Root/Paths/System/Startup/sLevelGeneratorPath", "Show Level Generator", "124 LaunchGenerator.lua", -10)
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "Games/AdventureLevelGenerator/Grids.lua")
fnAddGamePath(gsaGameEntries[giGamesTotal].sSearchPaths, "../adventure/Games/AdventureLevelGenerator/Grids.lua")


--[Script Hunter]
--The automated debugger Script Hunter searches directories for errors in lua files. You can specify 
-- which directories it should search by adding a path set to this list. Note that this list is not
-- autogenerated, since it's meant for debuggers.
--The Script Hunter is activated from the Debug Menu in Adventure Mode. You will then be able to 
-- select which file tree to scan.
ADebug_SetProperty("Clear Script Hunter")
ADebug_SetProperty("Add Script Hunter Path", "Games/AdventureLevelGenerator/")
ADebug_SetProperty("Add Script Hunter Path", "../adventure/Games/AdventureLevelGenerator/")
ADebug_SetProperty("Add Script Hunter Path", "Games/AdventureMode/")
ADebug_SetProperty("Add Script Hunter Path", "../adventure/Games/AdventureMode/")
ADebug_SetProperty("Add Script Hunter Path", "Games/ClassicMode/")
ADebug_SetProperty("Add Script Hunter Path", "../classic/Games/ClassicMode/")
ADebug_SetProperty("Add Script Hunter Path", "Games/ClassicMode3D/")
ADebug_SetProperty("Add Script Hunter Path", "../classic/Games/ClassicMode3D/")
ADebug_SetProperty("Add Script Hunter Path", "Games/CorrupterMode/")
ADebug_SetProperty("Add Script Hunter Path", "../classic/Games/CorrupterMode/")
ADebug_SetProperty("Add Script Hunter Path", "Games/DollManor/")
ADebug_SetProperty("Add Script Hunter Path", "../dollmanor/Games/DollManor/")
ADebug_SetProperty("Add Script Hunter Path", "Games/ElectrospriteTextAdventure/")
ADebug_SetProperty("Add Script Hunter Path", "../adventure/Games/ElectrospriteTextAdventure/")

--[Set Active Paths]
--All modes get scanned. If they exist, their active paths get set. This is needed for interoperability.
-- Control reconfigurations, for example, rely on Adventure Mode's path.
local bOnlyStringTyrant = true
local bOnlyStringTyrantDemo = true
DL_AddPath("Root/Paths/System/Startup/")
for i = 1, giGamesTotal, 1 do
    
    --Check which path is active for this game.
    local iPathIndex = fnScanPaths(gsaGameEntries[i].sSearchPaths)
    
    --Path index was zero. Game was not found.
    if(iPathIndex == 0) then
        --io.write("Game " .. gsaGameEntries[i].sName .. " was not found.\n")
    
    --Path index was nonzero. Set the path.
    else
    
        --Debug.
        --io.write("Game " .. gsaGameEntries[i].sName .. " was found with path " .. gsaGameEntries[i].sSearchPaths[iPathIndex] .. ".\n")
        
        --Set this as the active path.
        if(gsaGameEntries[i].sName ~= "Doll Manor")      then bOnlyStringTyrant     = false end
        if(gsaGameEntries[i].sName ~= "Doll Manor Demo") then bOnlyStringTyrantDemo = false end
        gsaGameEntries[i].sActivePath = gsaGameEntries[i].sSearchPaths[iPathIndex]
        
        --Store the active path in the DataLibrary.
        VM_SetVar(gsaGameEntries[i].sVarPath, "S", string.sub(gsaGameEntries[i].sActivePath, 1, gsaGameEntries[i].iNegativeLen))
        --io.write(" Stored path as " .. VM_GetVar(gsaGameEntries[i].sVarPath, "S") .. "\n")
        
    end
end


--[ ===================================== Global Variables ====================================== ]
--[Globals]
Debug_Print("Setting display enumerations.\n")
giCurrentDisplayMode = DM_GetDisplayModeInfo("Current Mode")
gbAntiAliasCharacters = true
gsAutoExec = nil
gbClearMenu = true

--[Color Constants]
gfColorWhite = {}
gfColorWhite.fR = 0.70
gfColorWhite.fG = 0.00
gfColorWhite.fB = 0.00
gfColorWhite.fA = 1.00

gfColorRed = {}
gfColorRed.fR = 0.70
gfColorRed.fG = 0.00
gfColorRed.fB = 0.00
gfColorRed.fA = 1.00

gfColorGreen = {}
gfColorGreen.fR = 0.00
gfColorGreen.fG = 0.00
gfColorGreen.fB = 0.40
gfColorGreen.fA = 1.00

gfColorViolet = {}
gfColorViolet.fR = 0.50
gfColorViolet.fG = 0.30
gfColorViolet.fB = 0.90
gfColorViolet.fA = 1.00

--[Classic Globals]
--Difficulty. The startup scripts for each scenario use this to extrapolate.
gsDifficulty = "Normal"

--[Object Types]
--These are internal C++ constants. Do not edit them.
xciType_InventoryItem = 21
xciType_WorldContainer = 22
xciType_Actor = 1002

--[Rendering Enumerations]
--These are derived from the GL State.
giLinear         = DM_GetEnumeration("GL_LINEAR")
giNearest        = DM_GetEnumeration("GL_NEAREST")
giLinearLinear   = DM_GetEnumeration("GL_LINEAR_MIPMAP_LINEAR")
giLinearNearest  = DM_GetEnumeration("GL_LINEAR_MIPMAP_NEAREST")
giNearestLinear  = DM_GetEnumeration("GL_NEAREST_MIPMAP_LINEAR")
giNearestNearest = DM_GetEnumeration("GL_NEAREST_MIPMAP_NEAREST")

--Set default flags
ALB_SetTextureProperty("Default MinFilter", giLinearLinear)
ALB_SetTextureProperty("Default MagFilter", giLinearLinear)
ALB_SetTextureProperty("Restore Defaults")

--[String Tyrant Only]
--If *only* String Tyrant was detected as a playable game, switch to it immediately and change the load handler.
LI_SetSuppression(false)
if(bOnlyStringTyrant or bOnlyStringTyrantDemo) then
    
    --Mark the loader so it does not re-boot in the launcher.
    gbHasBootedLoader = true
    gbIsOneGameMode = true
    
    --Resolve the path.
    local p = iStringTyrantEntry
    if(bOnlyStringTyrantDemo) then p = iStringTyrantDemoEntry end
    local sPath = string.sub(gsaGameEntries[p].sActivePath, 1, gsaGameEntries[p].iNegativeLen-1)
    
    --Reboot the loader.
    LI_BootStringTyrant(sPath .. "Datafiles/STLoad.slf")
    if(bOnlyStringTyrant) then
        LI_Reset(36 + 316)
    else
        LI_Reset(36 + 217)
    end
    
    --Flag.
    MapM_SetOneGame(true)
end

--[Resource Loading]
--Setup.
Debug_Print("Loading boot graphics.\n")
LM_ExecuteScript("Data/Scripts/Loading/Graphics.lua")

--[Audio Loading]
Debug_Print("Loading audio.\n")
LM_SetSilenceFlag(true)
	LM_ExecuteScript("Data/Audio/ZRouting.lua")
LM_SetSilenceFlag(false)

--[Debug]
Debug_PopPrint("Completed Startup.lua\n")

--[String Tyrant Only]
--When in String Tyrant, boot directly in.
gbAutobootStringTyrant = false
gbAutobootStringTyrantDemo = false
if(bOnlyStringTyrant == true) then
    gbAutobootStringTyrant = true
elseif(bOnlyStringTyrantDemo == true) then
    gbAutobootStringTyrantDemo = true

--Otherwise, finish loading.
else
    LI_Finish()
end
