--[ ====================================== Menu Functions ======================================= ]
--Variables needed by these functions.
giGamesTotal = 0
gsaGameEntries = {}

--[Entry Adder]
--Used to add a new game entry to the list of playable games. The iNegativeLen value indicates how many
-- letters to trim off the end to get the basic path. Example:
-- "Games/AdventureMode/ZLaunch.lua"
-- string.sub("Games/AdventureMode/ZLaunch.lua", 1, -12)
-- Result: "Games/AdventureMode/"
-- Thus, the iNegativeLen value for adventure mode is -12.
fnAddGameEntry = function(sName, sVarPath, sButtonText, sFileLaunchPath, iNegativeLen)
    
    --Error check.
    if(sName == nil) then return end
    if(sVarPath == nil) then return end
    if(sButtonText == nil) then return end
    if(sFileLaunchPath == nil) then return end
    if(iNegativeLen == nil) then return end
    
    --Add one.
    giGamesTotal = giGamesTotal + 1
    
    --Set!
    gsaGameEntries[giGamesTotal] = {}
    gsaGameEntries[giGamesTotal].sName = sName
    gsaGameEntries[giGamesTotal].sVarPath = sVarPath
    gsaGameEntries[giGamesTotal].sButtonText = sButtonText
    gsaGameEntries[giGamesTotal].sFileLaunchPath = sFileLaunchPath
    gsaGameEntries[giGamesTotal].sActivePath = "Null"
    gsaGameEntries[giGamesTotal].sSearchPaths = {}
    gsaGameEntries[giGamesTotal].iNegativeLen = iNegativeLen
    
end

--[Path Adder]
--Adds a new path to the list of game paths provided. Does not dupe check!
fnAddGamePath = function(saPathList, sPathName)

    --Arg check.
    if(saPathList == nil) then return end
    if(sPathName  == nil) then return end
    
    --Length.
    local iLen = #saPathList
    saPathList[iLen+1] = sPathName

end

--[Index Finder]
--Returns the index of a playable game of the same name, or 0 if the game is not found.
fnGetGameIndex = function(sName)

    --Error if the variables were not initialized.
    if(giGamesTotal == nil or gsaGameEntries == nil) then return 0 end

    --Scan.
    for i = 1, giGamesTotal, 1 do
        if(gsaGameEntries[i].sName == sName) then
            return i
        end
    end
    
    --Not found.
    return 0
end

--[Path Checker]
--Scans available paths, returns lowest index of a usable path. Returns 0 if no paths match.
-- The path checks for the existince of the launching file, but not the rest of the datafiles.
fnScanPaths = function(saPathList)
    if(saPathList == nil) then return 0 end
    for i = 1, #saPathList, 1 do
        if(FS_Exists(saPathList[i])) then
            return i
        end
    end
    return 0
end
