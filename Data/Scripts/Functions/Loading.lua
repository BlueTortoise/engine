--[ ===================================== Loading Functions ===================================== ]
--Global Variables
gsLoadSequence = "None"

--[Begin Load Sequence]
--Issue a load reset, using the provided name to determine the load counter.
function fnIssueLoadReset(sName)
	
	--Arg check.
	if(sName == nil) then return end
	
	--Warning.
	if(gsLoadSequence ~= "None") then
		io.write("Warning: A load reset " .. sName .. " was issued while " .. gsLoadSequence .. " was already in progress.\n")
	end
	
	--Issue.
	local iExpectedLoads = OM_GetOption("ExpectedLoad_" .. sName)
	LI_Reset(iExpectedLoads)
	
	--Store.
	gsLoadSequence = sName
	
end

--[Complete Load Sequence]
--After a loading sequence is completed, ends the loading sequence and stores its load count.
function fnCompleteLoadSequence()
	
	--If there was no active load sequence, do nothing.
	if(gsLoadSequence == "None") then return end
	
	--Order the LI to finish. This causes it to render one last time, and print reports if desired.
	LI_Finish()
	
	--Get the interrupt call count.
	local iInterruptCalls = LI_GetLastInterruptCount()
	
	--Send it to the OptionsManager.
	OM_SetOption("ExpectedLoad_" .. gsLoadSequence, iInterruptCalls)
	
	--Order the OptionsManager to write the load counter file. The program libraries sometimes vary in counts.
    -- This is always local to the executable, not the game directory.
	local sProgramLibrary = LM_GetSystemLibrary()
	if(sProgramLibrary == "Allegro") then
		OM_WriteLoadFile("LoadCountersAL.lua")
	else
		OM_WriteLoadFile("LoadCountersSDL.lua")
	end
	
	--Reset.
	gsLoadSequence = "None"
	
end