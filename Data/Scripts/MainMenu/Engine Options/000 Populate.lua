--[Setup Options Menu]
--Sets up the global options menu. Note that some of these changes may require a program restart to
-- take full effect.
local sBasePath = fnResolvePath()

--[Setup]
--Local vars.
local iAscendingPriority = 0

--Length.
local sLongestString = "Use Low Res for Adventure Mode"

--[Header]
FlexMenu_SetHeaderSize(2)
FlexMenu_SetHeaderLine(0, "Engine and Display Options")
FlexMenu_SetHeaderLine(1, "Some changes may require a program restart to take effect.", 0.6)

--[Set Resolution]
--Cycles through legal resolutions.
local iResX, iResY, iResR = DM_GetDisplayModeInfo("Data", giCurrentDisplayMode)
FlexMenu_RegisterButton("Resolution")

	--Description.
	FlexButton_SetProperty("Description", "Changes the resolution the game renders at. Resolutions are determined by your graphics drivers.\nRequires a program restart to take effect.")

	--Get how many modes there are.
	local iTotalDisplayModes = DM_GetDisplayModeInfo("Total Modes")
	
	--Upload the data.
	FlexButton_SetProperty("Option Count", iTotalDisplayModes)
	for i = 1, iTotalDisplayModes, 1 do
		local iResX, iResY, iResR = DM_GetDisplayModeInfo("Data", i-1)
		FlexButton_SetProperty("Option Text", i-1, iResX .. "x" .. iResY)
	end
	
	--Set the cursor to the current display mode.
	local iCurrentMode = DM_GetDisplayModeInfo("Current Mode")
	FlexButton_SetProperty("Current Option", iCurrentMode)
	
	--Other flags.
	SugarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

--[Toggle Mipmapping]
--Mipmapping causes things to antialias when they are magnified or minified.
FlexMenu_RegisterButton("Disallow Mipmapping")

	--Description.
	FlexButton_SetProperty("Description", "Toggles antialiasing on distant or extremely close textures. Some graphics cards may crash if they don't support mipmapping, otherwise you can ignore this.")

	--Upload the data.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	
	--Set to current mode.
	local bDisallowMipmapping = OM_GetOption("DisallowMipmapping")
	if(bDisallowMipmapping) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	SugarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

--[Mouse Debug]
--Shows a green cursor under the mouse. Useful for debugging resolution changes.
FlexMenu_RegisterButton("Show Mouse Debug Cursor")

	--Description.
	FlexButton_SetProperty("Description", "Shows a small cursor underneath the mouse cursor, or at least, where the program thinks the mouse cursor is after scaling. Use this only if the program seems to be clicking where your cursor isn't.")

	--Upload the data.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	
	--Set to current mode.
	local bShowMouseDebug = OM_GetOption("ShowMouseDebug")
	if(bShowMouseDebug) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	SugarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

--[Font Options]
FlexMenu_RegisterButton("Use Small Console Font")

	--Description.
	FlexButton_SetProperty("Description", "Uses a smaller font in the console, allowing it to display more information. May look bad at low resolutions.")
	
	--Uploads.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	local bUseSmallConsoleFont = OM_GetOption("UseSmallConsoleFont")
	if(bUseSmallConsoleFont) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	SugarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

--[Low Resolution Mode]
FlexMenu_RegisterButton("Use Low Res for Adventure Mode")

	--Description.
	FlexButton_SetProperty("Description", "Uses half-sized portraits and scene images in Adventure Mode. Only needed for integrated graphics cards.")
	
	--Uploads.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	local bUseLowResMode = OM_GetOption("LowResAdventureMode")
	if(bUseLowResMode) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	SugarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

--[Disallow Perlin Noise]
FlexMenu_RegisterButton("Disallow Perlin Noise Engine")

	--Description.
	FlexButton_SetProperty("Description", "Disables Perlin Noise generation. Makes Random Level Generation unusable.")
	
	--Uploads.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	local bDisallowNoise = OM_GetOption("DisallowPerlinNoise")
	if(bDisallowNoise) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	SugarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

--[RAM Loading]
FlexMenu_RegisterButton("Use RAM Loading")

	--Description.
	FlexButton_SetProperty("Description", "Causes the engine to use extra RAM to load files faster. May cause instability on older machines.")
	
	--Uploads.
	FlexButton_SetProperty("Option Count", 2)
	FlexButton_SetProperty("Option Text", 0, "True")
	FlexButton_SetProperty("Option Text", 1, "False")
	local bUseRAMLoading = OM_GetOption("UseRAMLoading")
	if(bUseRAMLoading) then
		FlexButton_SetProperty("Current Option", 0)
	else
		FlexButton_SetProperty("Current Option", 1)
	end
	
	--Other flags.
	SugarButton_SetProperty("Priority", iAscendingPriority)
	iAscendingPriority = iAscendingPriority + 1
	FlexButton_SetProperty("Lying Length", sLongestString)
DL_PopActiveObject()

--[Save]
--Save changes and go back to the main menu.
FlexMenu_RegisterButton("Save changes")

	--Description.
	FlexButton_SetProperty("Description", "Save changes and return to the main menu.")

	SugarButton_SetProperty("Priority", 10000)
	SugarButton_SetProperty("NewLuaExec", 0, sBasePath .. "100 Exit And Save.lua", 0)
DL_PopActiveObject()

--[Back]
--Go back to the main menu.
FlexMenu_RegisterButton("Exit without saving")

	--Description.
	FlexButton_SetProperty("Description", "Return to the main menu without saving changes.")

	SugarButton_SetProperty("Priority", 10001)
	SugarButton_SetProperty("NewLuaExec", 0, sBasePath .. "101 Exit No Save.lua", 0)
DL_PopActiveObject()
