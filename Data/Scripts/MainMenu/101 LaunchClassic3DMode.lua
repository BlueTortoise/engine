--[Launch Classic 3D Mode]
--Launches classic mode using the 3D renderer. Same gameplay, brand new look and feel. Modern!
local iGameIndex = fnGetGameIndex("Classic Mode 3D")
if(iGameIndex == 0 or gsaGameEntries[iGameIndex].sActivePath == "Null") then return end

--Run the setup scripts.
LM_ExecuteScript(gsaGameEntries[iGameIndex].sActivePath)
