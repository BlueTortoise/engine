--[Back to Main Menu]
--Closes the existing menu and returns to the main menu.
MapM_PushMenuStackHead()
FlexMenu_Clear()

--Run the setup script. This is elsewhere.
LM_ExecuteScript("Data/Scripts/MainMenu/000 PopulateMainMenu.lua")

--Clean Up.
DL_PopActiveObject()