--[ ========================================= Main Menu ========================================= ]
--This menu executes before anything else in the game actually happens. It allows the user to select
-- which game they'd like to play, set options, or quit. It can be expanded to incorporate other
-- features, like mods or cheats.
--Resetting the game returns to this menu after cleaning up unused resources.
local sBasePath = fnResolvePath()

--[Default Paths]
--Used for the treasure counting debug options.
DL_AddPath("Root/Paths/System/Startup/")
VM_SetVar("Root/Paths/System/Startup/sDebugAdventurePath", "S", "Games/AdventureMode/")

--[Autorun 3D]
--Used for testing 3D mode many times, saves you a few clicks.
if(OM_GetOption("AutoRun3DMode")) then
	LM_ExecuteScript("Games/ClassicMode3D/ZLaunch.lua")
	return
end

--[Autorun String Tyrant]
--This one-off flag autoboots String Tyrant.
if(gbAutobootStringTyrant == true) then
    LM_ExecuteScript(fnResolvePath() .. "121 LaunchTextAdventureMode.lua")
    return
    
--Demo version.
elseif(gbAutobootStringTyrantDemo == true) then
    LM_ExecuteScript(fnResolvePath() .. "121 LaunchTextAdventureModeDemo.lua")
    return
end

--[Auto-Exec]
--If there's anything in the auto-exec, use it here.
if(gsAutoExec ~= nil) then
	LM_ExecuteScript(gsAutoExec)
	gsAutoExec = nil
	return
end

--[Setup]
Debug_PushPrint(false, "Populating main menu.\n")

--Intro.
Debug_DropEvents()
FlexMenu_ActivateIntro()

--[Header]
FlexMenu_SetHeaderSize(1)
FlexMenu_SetHeaderLine(0, "Main Menu")

--[ ====================================== Mode Selectors ======================================= ]
--We use the filesystem to resolve whether or not specific files exist. These files are indicators
-- that mean the rest of the files are there, or at least are capable of doing something when
-- selected. Later we might want to use a metadata file or somesuch.
--The list of paths is built in Startup.lua.
local iPriorityCounter = 100

--For each game entry: Scan the available paths. If one is found, add a button to play that game.
for i = 1, giGamesTotal, 1 do
    
    --Scan across
    local iPathIndex = fnScanPaths(gsaGameEntries[i].sSearchPaths)
    if(iPathIndex ~= 0) then
        gsaGameEntries[i].sActivePath = gsaGameEntries[i].sSearchPaths[iPathIndex]
        
        
        --If the name is "DO NOT ADD" then skip it.
        if(gsaGameEntries[i].sButtonText == "DO NOT ADD") then
        else
            FlexMenu_RegisterButton(gsaGameEntries[i].sButtonText)
                SugarButton_SetProperty("Priority", iPriorityCounter)
                SugarButton_SetProperty("NewLuaExec", 0, sBasePath .. gsaGameEntries[i].sFileLaunchPath, 0)
            DL_PopActiveObject()
            iPriorityCounter = iPriorityCounter + 1
        end
    end
end


--[ ========================================== Options ========================================== ]
--Sets options that are independent of the game that's currently running. This includes things like
-- resolution, color schemes, and control layouts.
--Always exists, never checks a path.
FlexMenu_RegisterButton("Global Options")
	SugarButton_SetProperty("Priority", 7000)
    --FlexButton_SetProperty("Description", "Change program options that affect all game modes.")
	SugarButton_SetProperty("NewLuaExec", 0, sBasePath .. "190 GlobalOptions.lua", 0)
DL_PopActiveObject()

--[ ======================================= Quit Messages ======================================= ]
--Roll which message to show for this.
local sQuitMessage = "Quit the program."
local iRoll = LM_GetRandomNumber(0, 9)
if(iRoll == 0) then
	sQuitMessage = "Please don't leave, there's more monstergirls to toast!"
elseif(iRoll == 1) then
	sQuitMessage = "I wouldn't leave if I were you. Your desktop is much worse."
elseif(iRoll == 2) then
	sQuitMessage = "Go ahead and leave, see if I care."
elseif(iRoll == 3) then
	sQuitMessage = "Why would you quit this great game?"
elseif(iRoll == 4) then
	sQuitMessage = "Go back to your boring programs, then."
elseif(iRoll == 5) then
	sQuitMessage = "Leaving? When you come back, I'll be waiting with a bat."
elseif(iRoll == 6) then
	sQuitMessage = "Wait, don't go! We haven't run out of monstergirls yet!"
elseif(iRoll == 7) then
	sQuitMessage = "So you're saying you want to play other games? That's okay..."
elseif(iRoll == 8) then
	sQuitMessage = "I didn't give you permission to quit, get back here!"
elseif(iRoll == 9) then
	sQuitMessage = "If you're going to go look at cat photos, then I don't want you here either!"
end

--Quits the game. Always appears last.
FlexMenu_RegisterButton("Quit")
	SugarButton_SetProperty("Priority", 10000)
    --FlexButton_SetProperty("Description", sQuitMessage)
	SugarButton_SetProperty("NewLuaExec", 0, sBasePath .. "191 Quit.lua", 0)
DL_PopActiveObject()

--Debug.
Debug_PopPrint("Completed populating main menu.\n")