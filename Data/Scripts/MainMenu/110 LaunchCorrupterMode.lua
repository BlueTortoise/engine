--[Launch Corrupter Mode]
--Launches corrupter mode, the new game type. Exciting!
local iGameIndex = fnGetGameIndex("Corrupter Mode")
if(iGameIndex == 0 or gsaGameEntries[iGameIndex].sActivePath == "Null") then return end

--Run the setup scripts.
LM_ExecuteScript(gsaGameEntries[iGameIndex].sActivePath)
