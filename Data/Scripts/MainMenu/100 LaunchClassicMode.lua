--[Launch Classic Mode]
--Launches classic mode, which has the original HoP with no frills.
local iGameIndex = fnGetGameIndex("Classic Mode")
if(iGameIndex == 0 or gsaGameEntries[iGameIndex].sActivePath == "Null") then return end

--Run the setup scripts.
LM_ExecuteScript(gsaGameEntries[iGameIndex].sActivePath)
