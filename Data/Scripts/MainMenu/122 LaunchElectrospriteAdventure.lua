--[Launch Electrosprite Text Adventure Mode]
--Luanches the Electrosprite Text Adventure. This is the one used in Adventure Mode.
local iGameIndex = fnGetGameIndex("Electrosprite Adventure")
if(iGameIndex == 0 or gsaGameEntries[iGameIndex].sActivePath == "Null") then return end

--Go to system boot.
MapM_PushMenuStackHead()
	FlexMenu_FlagClose()
DL_PopActiveObject()

--Run the setup scripts.
bIsBootedFromMenu = true
bQuitsToMenu = true
LM_ExecuteScript(gsaGameEntries[iGameIndex].sActivePath)
