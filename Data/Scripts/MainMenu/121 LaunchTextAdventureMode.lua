--[Launch Text Adventure Mode]
--Become a doll! Or not.
local iGameIndex = fnGetGameIndex("Doll Manor")
if(iGameIndex == 0 or gsaGameEntries[iGameIndex].sActivePath == "Null") then return end

--Go to system boot.
MapM_PushMenuStackHead()
	FlexMenu_FlagClose()
DL_PopActiveObject()

--Run the setup scripts.
LM_ExecuteScript(gsaGameEntries[iGameIndex].sActivePath)
