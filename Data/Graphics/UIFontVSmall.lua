--[UI Font: Small]
--Edging script.

--Local Functions
local fnCreateLetter = function(sLetterString, iLft, iTop, iWid, iHei, iStdSpacing)
	
	--Edge case, go to next row.
	if(iLft >= 1008) then
		iLft = iLft - 1008
		iTop = iTop + 23
	end

	iWid = iWid + 2
	iHei = iHei + 2
	SugarFont_SetBitmapCharacter(string.byte(sLetterString), iLft, iTop, iLft + iWid, iTop + iHei, iWid + iStdSpacing)
end

--Setup
local iLft = 576
local iCharWid = 18

--Letters
fnCreateLetter("A", iLft + (iCharWid *  0), 0, 11, 11, 1)
fnCreateLetter("B", iLft + (iCharWid *  1), 0,  8, 11, 1)
fnCreateLetter("C", iLft + (iCharWid *  2), 0,  8, 11, 1)
fnCreateLetter("D", iLft + (iCharWid *  3), 0, 10, 11, 1)
fnCreateLetter("E", iLft + (iCharWid *  4), 0,  9, 11, 1)
fnCreateLetter("F", iLft + (iCharWid *  5), 0,  7, 11, 1)
fnCreateLetter("G", iLft + (iCharWid *  6), 0, 10, 11, 1)
fnCreateLetter("H", iLft + (iCharWid *  7), 0, 12, 11, 1)
fnCreateLetter("I", iLft + (iCharWid *  8), 0,  5, 11, 1)
fnCreateLetter("J", iLft + (iCharWid *  9), 0,  7, 14, 1)
fnCreateLetter("K", iLft + (iCharWid * 10), 0, 10, 11, 1)
fnCreateLetter("L", iLft + (iCharWid * 11), 0,  8, 11, 1)
fnCreateLetter("M", iLft + (iCharWid * 12), 0, 13, 11, 1)
fnCreateLetter("N", iLft + (iCharWid * 13), 0, 11, 11, 1)
fnCreateLetter("O", iLft + (iCharWid * 14), 0,  9, 11, 1)
fnCreateLetter("P", iLft + (iCharWid * 15), 0,  8, 11, 1)
fnCreateLetter("Q", iLft + (iCharWid * 16), 0, 10, 14, 1)
fnCreateLetter("R", iLft + (iCharWid * 17), 0, 10, 11, 1)
fnCreateLetter("S", iLft + (iCharWid * 18), 0,  6, 11, 1)
fnCreateLetter("T", iLft + (iCharWid * 19), 0, 10, 11, 1)
fnCreateLetter("U", iLft + (iCharWid * 20), 0, 11, 11, 1)
fnCreateLetter("V", iLft + (iCharWid * 21), 0, 11, 11, 1)
fnCreateLetter("W", iLft + (iCharWid * 22), 0, 14, 11, 1)
fnCreateLetter("X", iLft + (iCharWid * 23), 0, 11, 11, 1)
fnCreateLetter("Y", iLft + (iCharWid * 24), 0,  9, 11, 1)
fnCreateLetter("Z", iLft + (iCharWid * 25), 0,  8, 11, 1)

iLft = 144
fnCreateLetter("a", iLft + (iCharWid *  0), 23,  6, 11, 1)
fnCreateLetter("b", iLft + (iCharWid *  1), 23,  7, 11, 1)
fnCreateLetter("c", iLft + (iCharWid *  2), 23,  5, 11, 1)
fnCreateLetter("d", iLft + (iCharWid *  3), 23,  6, 11, 1)
fnCreateLetter("e", iLft + (iCharWid *  4), 23,  5, 11, 1)
fnCreateLetter("f", iLft + (iCharWid *  5), 23,  6, 11, 1)
fnCreateLetter("g", iLft + (iCharWid *  6), 23,  8, 15, 1)
fnCreateLetter("h", iLft + (iCharWid *  7), 23,  8, 11, 1)
fnCreateLetter("i", iLft + (iCharWid *  8), 23,  3, 11, 1)
fnCreateLetter("j", iLft + (iCharWid *  9), 23,  4, 15, 1)
fnCreateLetter("k", iLft + (iCharWid * 10), 23,  8, 11, 1)
fnCreateLetter("l", iLft + (iCharWid * 11), 23,  3, 11, 1)
fnCreateLetter("m", iLft + (iCharWid * 12), 23, 13, 11, 1)
fnCreateLetter("n", iLft + (iCharWid * 13), 23,  8, 11, 1)
fnCreateLetter("o", iLft + (iCharWid * 14), 23,  6, 11, 1)
fnCreateLetter("p", iLft + (iCharWid * 15), 23,  7, 15, 1)
fnCreateLetter("q", iLft + (iCharWid * 16), 23,  6, 15, 1)
fnCreateLetter("r", iLft + (iCharWid * 17), 23,  5, 11, 1)
fnCreateLetter("s", iLft + (iCharWid * 18), 23,  5, 11, 1)
fnCreateLetter("t", iLft + (iCharWid * 19), 23,  4, 11, 1)
fnCreateLetter("u", iLft + (iCharWid * 20), 23,  8, 11, 1)
fnCreateLetter("v", iLft + (iCharWid * 21), 23,  8, 11, 1)
fnCreateLetter("w", iLft + (iCharWid * 22), 23, 11, 11, 1)
fnCreateLetter("x", iLft + (iCharWid * 23), 23,  8, 11, 1)
fnCreateLetter("y", iLft + (iCharWid * 24), 23,  8, 16, 1)
fnCreateLetter("z", iLft + (iCharWid * 25), 23,  6, 11, 1)

--Numbers
iLft = 270
fnCreateLetter("0", iLft + (iCharWid *  0), 0,  7, 11, 1)
fnCreateLetter("1", iLft + (iCharWid *  1), 0,  5, 11, 1)
fnCreateLetter("2", iLft + (iCharWid *  2), 0,  6, 11, 1)
fnCreateLetter("3", iLft + (iCharWid *  3), 0,  6, 11, 1)
fnCreateLetter("4", iLft + (iCharWid *  4), 0,  7, 11, 1)
fnCreateLetter("5", iLft + (iCharWid *  5), 0,  6, 11, 1)
fnCreateLetter("6", iLft + (iCharWid *  6), 0,  7, 11, 1)
fnCreateLetter("7", iLft + (iCharWid *  7), 0,  8, 11, 1)
fnCreateLetter("8", iLft + (iCharWid *  8), 0,  6, 11, 1)
fnCreateLetter("9", iLft + (iCharWid *  9), 0,  6, 11, 1)

--Misc sequence 1
iLft = 0
fnCreateLetter("!",  iLft + (iCharWid *  0), 0,  3, 11, 1)
fnCreateLetter("\"", iLft + (iCharWid *  1), 0, 11, 15, 1)
fnCreateLetter("#",  iLft + (iCharWid *  2), 0,  8, 11, 1)
fnCreateLetter("$",  iLft + (iCharWid *  3), 0,  6, 11, 1)
fnCreateLetter("%",  iLft + (iCharWid *  4), 0, 11, 12, 1)
fnCreateLetter("&",  iLft + (iCharWid *  5), 0, 10, 11, 1)
fnCreateLetter("'",  iLft + (iCharWid *  6), 0,  2, 11, 1)
fnCreateLetter("(",  iLft + (iCharWid *  7), 0,  4, 11, 1)
fnCreateLetter(")",  iLft + (iCharWid *  8), 0,  4, 11, 1)
fnCreateLetter("*",  iLft + (iCharWid *  9), 0,  4, 11, 1)
fnCreateLetter("+",  iLft + (iCharWid * 10), 0,  6, 11, 1)
fnCreateLetter(",",  iLft + (iCharWid * 11), 0,  2, 21, 1)
fnCreateLetter("-",  iLft + (iCharWid * 12), 0,  4, 11, 1)
fnCreateLetter(".",  iLft + (iCharWid * 13), 0,  1, 17, 1)
fnCreateLetter("/",  iLft + (iCharWid * 14), 0,  4, 11, 1)

--Misc sequence 2
iLft = 450
fnCreateLetter(":",  iLft + (iCharWid *  0), 0,  1, 11, 1)
fnCreateLetter(";",  iLft + (iCharWid *  1), 0,  2, 11, 1)
fnCreateLetter("<",  iLft + (iCharWid *  2), 0,  7, 11, 1)
fnCreateLetter("=",  iLft + (iCharWid *  3), 0,  8, 11, 1)
fnCreateLetter(">",  iLft + (iCharWid *  4), 0,  7, 11, 1)
fnCreateLetter("?",  iLft + (iCharWid *  5), 0,  5, 11, 1)

--Misc sequence 3
iLft = 36
fnCreateLetter("[",  iLft + (iCharWid *  0), 23,  3, 11, 1)
fnCreateLetter("\\", iLft + (iCharWid *  1), 23,  3, 11, 1)
fnCreateLetter("]",  iLft + (iCharWid *  2), 23,  3, 11, 1)
fnCreateLetter("^",  iLft + (iCharWid *  3), 23,  6, 11, 1)
fnCreateLetter("_",  iLft + (iCharWid *  4), 23,  5, 11, 1)

--Special
fnCreateLetter(" ",  612, 23, 3, 11, 1)









