--[UI Font: Small]
--Set lookup information for the large half of the font.

--Local Functions
local fnCreateLetter = function(sLetterString, iLft, iTop, iWid, iHei, iStdSpacing)
	
	--Edge case, go to next row.
	if(iLft >= 1020) then
		iLft = iLft - 1020
		iTop = iTop + 20
	end
		
	
	iWid = iWid + 2
	iHei = iHei + 1
	SugarFont_SetBitmapCharacter(string.byte(sLetterString), iLft, iTop, iLft + iWid, iTop + iHei, iWid + iStdSpacing)
end

--Constants
local ciLftEdge = 659
local ciHei = 19

--Letters
fnCreateLetter("A", 1299 - ciLftEdge, 0, 12, ciHei, 1)
fnCreateLetter("B", 1319 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("C", 1339 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("D", 1359 - ciLftEdge, 0, 11, ciHei, 1)
fnCreateLetter("E", 1379 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("F", 1399 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("G", 1419 - ciLftEdge, 0, 11, ciHei, 1)
fnCreateLetter("H", 1439 - ciLftEdge, 0, 12, ciHei, 1)
fnCreateLetter("I", 1459 - ciLftEdge, 0,  4, ciHei, 1)
fnCreateLetter("J", 1479 - ciLftEdge, 0,  5, ciHei+1, 1)
fnCreateLetter("K", 1499 - ciLftEdge, 0, 11, ciHei, 1)
fnCreateLetter("L", 1519 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("M", 1539 - ciLftEdge, 0, 14, ciHei, 1)
fnCreateLetter("N", 1559 - ciLftEdge, 0, 11, ciHei, 1)
fnCreateLetter("O", 1579 - ciLftEdge, 0, 11, ciHei, 1)
fnCreateLetter("P", 1599 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("Q", 1619 - ciLftEdge, 0, 12, ciHei+1, 1)
fnCreateLetter("R", 1639 - ciLftEdge, 0, 11, ciHei, 1)
fnCreateLetter("S", 1659 - ciLftEdge, 0,  8, ciHei, 1)
fnCreateLetter("T", 1679 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("U", 1699 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("V", 1719 - ciLftEdge, 0, 12, ciHei, 1)
fnCreateLetter("W", 1739 - ciLftEdge, 0, 18, ciHei, 1)
fnCreateLetter("X", 1759 - ciLftEdge, 0, 11, ciHei, 1)
fnCreateLetter("Y", 1779 - ciLftEdge, 0, 12, ciHei, 1)
fnCreateLetter("Z", 1799 - ciLftEdge, 0,  9, ciHei, 1)

fnCreateLetter("a", 1939 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("b", 1959 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("c", 1979 - ciLftEdge, 0,  7, ciHei, 1)
fnCreateLetter("d", 1999 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("e", 2019 - ciLftEdge, 0,  8, ciHei, 1)
fnCreateLetter("f", 2039 - ciLftEdge, 0,  7, ciHei, 1)
fnCreateLetter("g", 2059 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("h", 2079 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("i", 2099 - ciLftEdge, 0,  4, ciHei, 1)
fnCreateLetter("j", 2119 - ciLftEdge, 0,  4, ciHei, 1)
fnCreateLetter("k", 2139 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("l", 2159 - ciLftEdge, 0,  4, ciHei, 1)
fnCreateLetter("m", 2179 - ciLftEdge, 0, 15, ciHei, 1)
fnCreateLetter("n", 2199 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("o", 2219 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("p", 2239 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("q", 2259 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("r", 2279 - ciLftEdge, 0,  7, ciHei, 1)
fnCreateLetter("s", 2299 - ciLftEdge, 0,  7, ciHei, 1)
fnCreateLetter("t", 2319 - ciLftEdge, 0,  6, ciHei, 1)
fnCreateLetter("u", 2339 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("v", 2359 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("w", 2379 - ciLftEdge, 0, 15, ciHei, 1)
fnCreateLetter("x", 2399 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("y", 2419 - ciLftEdge, 0, 10, ciHei, 1)
fnCreateLetter("z", 2439 - ciLftEdge, 0,  7, ciHei, 1)

--Numbers
fnCreateLetter("0",  959 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("1",  979 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("2",  999 - ciLftEdge, 0,  8, ciHei, 1)
fnCreateLetter("3", 1019 - ciLftEdge, 0,  8, ciHei, 1)
fnCreateLetter("4", 1039 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("5", 1059 - ciLftEdge, 0,  8, ciHei, 1)
fnCreateLetter("6", 1079 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("7", 1099 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("8", 1119 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("9", 1139 - ciLftEdge, 0,  9, ciHei, 1)

--Misc
fnCreateLetter(" ",  780, 20,  4,  1, 1)
fnCreateLetter("/",  280,  0,  8, ciHei, 1)
fnCreateLetter("'",  121,  0,  4, ciHei, 1)
fnCreateLetter("\"", 679 - ciLftEdge, 0,  5, ciHei, 1)
fnCreateLetter(";", 1179 - ciLftEdge, 0,  3, ciHei, 1)
fnCreateLetter(":", 1159 - ciLftEdge, 0,  2, ciHei, 1)
fnCreateLetter("[", 1819 - ciLftEdge, 0,  5, ciHei, 1)
fnCreateLetter("]", 1859 - ciLftEdge, 0,  5, ciHei, 1)
fnCreateLetter("-",  899 - ciLftEdge, 0,  5, ciHei, 1)
fnCreateLetter("+",  859 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("_",  780,            20,  7, ciHei, 1)
fnCreateLetter("<",  540,             0,  11, ciHei, 1)
fnCreateLetter("=", 1219 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter(">",  580,             0,  12, ciHei, 1)
fnCreateLetter("!",  659 - ciLftEdge, 0,  2, ciHei, 1)
fnCreateLetter("@", 1279 - ciLftEdge, 0, 15, ciHei, 1)
fnCreateLetter("#",  699 - ciLftEdge, 0,  9, ciHei, 1)
fnCreateLetter("$",  719 - ciLftEdge, 0,  8, ciHei, 1)
fnCreateLetter("%",  739 - ciLftEdge, 0, 16, ciHei, 1)
fnCreateLetter("^", 1879 - ciLftEdge, 0,  7, ciHei, 1)
fnCreateLetter("&",  759 - ciLftEdge, 0, 13, ciHei, 1)
fnCreateLetter("*",  839 - ciLftEdge, 0,  6, ciHei, 1)
fnCreateLetter("(",  799 - ciLftEdge, 0,  6, ciHei, 1)
fnCreateLetter(")",  819 - ciLftEdge, 0,  6, ciHei, 1)
fnCreateLetter("?", 1259 - ciLftEdge, 0,  6, ciHei, 1)

fnCreateLetter(".",  919 - ciLftEdge, 0,  2, ciHei, 1)
fnCreateLetter(",",  879 - ciLftEdge, 0,  3, ciHei, 1)

